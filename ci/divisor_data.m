freeze;

/////////////////////////////////////////////////////////////////////////
// divisor_data.m
/////////////////////////////////////////////////////////////////////////
// Calculates and validates the divisor data for a toric complete
// intersection.
/////////////////////////////////////////////////////////////////////////

import "validate.m": validate_toric_data, validate_weight_data;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Given a smooth toric variety X, returns the toric divisors, nef cone, and
// Mori cone.
function divisor_data_from_variety(X)
    phi:=WeilToClassLatticesMap(X);
    return phi(Basis(Domain(phi))),NefCone(X),MoriCone(X);
end function;

// Given weights W and optional ample divisor, returns the toric divisors, nef
// cone, and Mori cone. Assumes that W defines a smooth toric Fano.
function divisor_data_from_weights(W : ample:=false)
    // Create the weight matrix
    weight_matrix:=Matrix(W);
    // Extract the toric divisors
    L:=ToricLattice(#W);
    D:=ColumnSequence(weight_matrix);
    // If no ample divisor is given, use -K
    if ample cmpeq false then
        ample:=LatticeVector(Eltseq(&+Columns(weight_matrix)));
    else
        ample:=LatticeVector(ample);
    end if;
    L:=Parent(ample);
    ChangeUniverse(~D,L);
    // Calculate the minimal anticones
    D_without_repeats := SetToSequence(SequenceToSet(D));
    cones:=[];
    for idx in Subsets({1..#D_without_repeats},#W) do
        C:=Cone([L | D_without_repeats[j] : j in idx]);
        if ample in C then
            Append(~cones,C);
        end if;
    end for;
    // Calculate the nef cone and the Mori code
    nef:=&meet cones;
    mori:=Dual(nef);
    // Return the data
    return D,nef,mori;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic DivisorData( X::TorVar : allow_weak_fano:=false )
    -> SeqEnum[TorLatElt],TorCon,TorCon
{Given a smooth toric Fano variety X, computes the toric divisors, nef cone, and Mori cone. Set 'allow_weak_fano' to true (default: false) to allow weak Fanos.}
    // Sanity check
    bool,msg:=validate_toric_data(X : allow_weak_fano:=allow_weak_fano);
    require bool: msg;
    // Calculate the basic geometric data
    D,NC,MC:=divisor_data_from_variety(X);
    return D,NC,MC;
end intrinsic;

intrinsic DivisorData( W::SeqEnum[SeqEnum[RngIntElt]] : ample:=false,
    define_fan:=false, allow_weak_fano:=false )
    -> SeqEnum[TorLatElt],TorCon,TorCon
{Given weights W defining a smooth toric Fano variety, computes the toric divisors, nef cone, and Mori cone. The optional parameter 'ample' can be used to specify the ample divisor to use. If the optional parameter 'define_fan' (default: false) is set to true then the verification that the data defines a smooth toric Fano variety is skipped. Set 'allow_weak_fano' to true (default: false) to allow weak Fanos.}
    // Sanity check
    bool,ample:=validate_weight_data(W : ample:=ample, define_fan:=define_fan,
                                              allow_weak_fano:=allow_weak_fano);
    require bool: ample;
    // Calculate the geometric data
    D,NC,MC:=divisor_data_from_weights(W : ample:=ample);
    return D,NC,MC;
end intrinsic;

freeze;

/////////////////////////////////////////////////////////////////////////
// validate.m
/////////////////////////////////////////////////////////////////////////
// Validation functions for the c.i. data.
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Validates the target dimensions for c.i. in an ambient toric Fano of given
// dimension. Returns true followed by the dimensions on success, false followed
// by an error message otherwise.
function validate_dimensions(ambient_dim,dimensions)
    if not(Type(dimensions) eq SeqEnum or Type(dimensions) eq SetEnum) then
        return false,Sprintf("Parameter 'dimensions' must be a set or sequence of integers in the range 1..%o.",ambient_dim - 1);
    end if;
    if Type(dimensions) eq SeqEnum then
        dimensions:=SequenceToSet(dimensions);
    end if;
    bool,dimensions:=CanChangeUniverse(dimensions,Integers());
    if not bool then
        return false,Sprintf("Parameter 'dimensions' must be a set or sequence of integers in the range 1..%o.",ambient_dim - 1);
    end if;
    if #dimensions ne 0 then
        if not(Min(dimensions) ge 1 and Max(dimensions) lt ambient_dim) then
            return false,Sprintf("Parameter 'dimensions' must be a set or sequence of integers in the range 1..%o.",ambient_dim - 1);
        end if;
    end if;
    return true,dimensions;
end function;

// Validates the arguments for weight data. Returns true followed by ample on
// success, false followed by an error message otherwise.
function validate_weight_data(W : ample:=false, define_fan:=false,
  allow_weak_fano:=false)
    if not (#W ne 0 and #W[1] ne 0) then
        return false, "The fan must be defined by at least one weight.";
    end if;
    d:=#W[1];
    if not &and[#wt eq d : wt in W] then
        return false, "The weights must all be of the same length.";
    end if;
    if not Type(define_fan) eq BoolElt then
        return false, "The parameter 'define_fan' must be a boolean.";
    end if;
    if not Type(allow_weak_fano) eq BoolElt then
        return false, "Parameter 'allow_weak_fano' must be a boolean.";
    end if;
    if not ample cmpeq false then
        if not (Type(ample) eq SeqEnum and #ample eq #W) then
            return false, Sprintf("The parameter 'ample' must be a sequence of integers of length %o.",#W);
        end if;
        bool,ample:=CanChangeUniverse(ample,Integers());
        if not bool then
    	    return false, "The parameter 'ample' must be a sequence of integers.";
    	end if;
    end if;
    if not define_fan then
        // Try to create the associated fan
        try
            F:=FanWithWeights(W : ample:=ample);
        catch e
            return false,"The weight data does not define a fan.";
        end try;
        // Is this a complete fan?
        if not IsComplete(F) then
            return false,"The fan is not complete.";
        end if;
        // Do we allow this ambient?
        X:=ToricVariety(Rationals(),F);
        if not IsWeightedProjectiveSpace(X) then
            if not IsNonSingular(X) then
                return false,"The toric variety is not nonsingular.";
            end if;
            if allow_weak_fano then
                if not IsWeakFano(X) then
                    return false,"The toric variety must be a weak Fano variety.";
                end if;
            else
                if not IsFano(X) then
                    return false,"The toric variety must be a smooth Fano variety.";
                end if;
            end if;
        end if;
    end if;
    return true,ample;
end function;

// Validates the arguments for a toric variety. Returns true on success, false
// followed by an error message otherwise.
function validate_toric_data(X : allow_weak_fano:=false)
    if Type(allow_weak_fano) ne BoolElt then
        return false,"Parameter 'allow_weak_fano' must be a boolean.";
    end if;
    if not IsWeightedProjectiveSpace(X) then
        if allow_weak_fano then
            if not IsNonSingular(X) or not IsWeakFano(X) then
                return false,"The toric variety must be a weighted projective space or a smooth weak Fano variety.";
            end if;
        else
            if not IsNonSingular(X) or not IsFano(X) then
                return false,"The toric variety must be a weighted projective space or a smooth Fano variety.";
            end if;
        end if;
    end if;
    return true,_;
end function;

// Validates the arguments for the weight data and bundles. Returns true
// followed by ample on success, false followed by an error message otherwise.
function validate_weight_data_and_bundles(W,K : ample:=false, define_fan:=false,
                                                         allow_weak_fano:=false)
    bool,ample:=validate_weight_data(W : ample:=ample, define_fan:=define_fan,
                                              allow_weak_fano:=allow_weak_fano);
    if not bool then
        return false,ample;
    end if;
    d:=#W;
    if not &and[#s eq d : s in K] then
        return false, Sprintf("The intersection data must be a sequence of sequences of integers, each of length %o.",d);
    end if;
    return true,ample;
end function;

// Validates the arguments for a toric variety and bundles. Returns true if
// valid, false otherwise. If true, also returns the weights W; if false, also
// returns an error message.
function validate_toric_data_and_bundles(X,K : allow_weak_fano:=false)
    bool,msg:=validate_toric_data(X : allow_weak_fano:=allow_weak_fano);
    if not bool then
        return false,msg;
    end if;
    W:=Gradings(CoxRing(X));
    d:=#W;
    if not &and[#s eq d : s in K] then
        return false, Sprintf("The intersection data must be a sequence of sequences of integers, each of length %o.",d);
    end if;
    return true,W;
end function;

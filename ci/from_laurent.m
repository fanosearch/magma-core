freeze;

/////////////////////////////////////////////////////////////////////////
// from_laurent.m
/////////////////////////////////////////////////////////////////////////
// Given a Laurent polynomial, attempts to find a complete intersection
// in a toric manifold or toric Deligne--Mumford stack which gives
// rise to this Laurent polynomial via the Przyjalkowski trick
/////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// Let us imagine, given a Laurent polynomial f, trying to generate all possible
// toric complete intersection models obtained from f by inverting the
// Pryzjalkowski trick.  Let us say that a "scaffolding" of f is a way of
// writing f as l_1/m_1 + ... + l_r/m_r + M_1 + ... + M_s where:
// * l_1,...,l_r are products of basic pieces of the form (1+u_1+...+u_k)^s
//   where the u_i are monomials;
// * m_1,...,m_r and M_1,...,M_s are monomials
// * exponents of the monomials {M_1,...,M_s} \cup {u_i that occur in the l_j}
//   form a basis for the lattice ZZ^N, where N is the number of variables in f
//
// We proceed as follows.  Observe that the Newton polytope of l_i is a product
// Q of standard simplices.  Let us call such an irreducible k-simplex factor of
// an l_i a "strut".  The Newton polytope of a strut is a dilation of a standard
// simplex.  We first single out a set of possible struts by either
// (a) looking for all possible standard sub-simplices of P:=Newt(f); or
// (b) assuming that each strut that occurs arises by nominating a vertex v of P
//     together with k edges incident to v, taking the first lattice points
//     along each of these k edges to define a k-simplex, and dilating if
//     necessary.
// Option (a) is exhaustive but, because there are then so many cases to check,
// makes things slower; option (b) is in practice a good choice and, as it
// produces a smaller set of possible struts, is quicker.
//
// For each set of struts we then:
// * compute the number s of uneliminated variables needed;
// * choose a set of uneliminated variables M_1,...,M_s compatible with the
//   struts, in the sense that the collection of all monomials involved forms a
//   co-ordinate system on \Cstar^{N-r}.
// * assemble the struts into all possible pieces, as follows.  For each
//      lattice point p in P, work out the largest possible dilation of each
//      chosen strut that we can base at p such that the dilation fits inside P.
//      Now take all products of our chosen struts, using at most this number of
//      factors, and add those that fit inside P to the list of possible pieces.
// * walk the possible scaffoldings of f, as follows.  Nominate a number e of
//      eliminated variables.  This is bounded by the sum of the coefficients
//      of f minus the number of uneliminated variables.  Choose an e-tuple of
//      pieces and check if the corresponding Laurent polynomial differs from f
//      by a constant.  If so, generate complete intersection data and see if
//      it works.
////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

import "divisor_data.m": divisor_data_from_weights;


////////////////////////////////////////////////////////////////////////////////
// Let F be the fan corresponding to the GIT problem with weights given by W
// and linearization given by ample.  Returns:
// true, F   if F is a simplicial fan, i.e. corresponds to a toric
//           Deligne--Mumford stack, and
// false, _  otherwise
// The optional parameter 'ample' defaults to the sum of the weights.
// It is guaranteed that the rays of the fan are in the same order as the
// columns of the weight matrix.  The code checks that the resulting rays are
// primitive and distinct (this will fail if the GIT presentation of the toric
// variety is redundant) and fails with an error if not.  It checks that the
// resulting fan is complete (i.e. corresponds to a compact toric variety) and
// fails with an error if not.
////////////////////////////////////////////////////////////////////////////////
function make_fan_from_weights(W : ample:=false)
    // Sanity checks
    error if not (#W ne 0 and #W[1] ne 0), "The fan must be defined by at least one weight.";
    n:=#W[1];
    error if not &and[#wt eq n : wt in W], "The weights must all be of the same length.";
    if not ample cmpeq false then
        error if not (Type(ample) eq SeqEnum and #ample eq #W),
	      Sprintf("The parameter 'ample' must be a sequence of integers of length %o.",#W);
        bool, ample:=CanChangeUniverse(ample,Integers());
        error if not bool, "The parameter 'ample' must be a sequence of integers.";
    end if;
    // Create the weight matrix
    wts:=Matrix(Integers(),W);
    // Generate the sequence of toric divisors
    divisors:=RowSequence(Transpose(wts));
    D:=ToricLattice(#W);
    ChangeUniverse(~divisors,D);
    // If no ample divisor is given, use -K as our ample divisor
    if ample cmpeq false then
        ample:=&+divisors;
    else
        ample:=D ! ample;
    end if;
    // check if the ample divisor is in the interior of a chamber for the GIT problem
    walls := { {D|divisors[i] : i in idxs} : idxs in Subsets({1..#divisors},NumberOfRows(wts)-1) };
    for wall in walls do
	if ample in Cone(wall) then
	    vprintf Fanosearch: "The ample class is not in the interior of a chamber.\nThe ample class %o is on the wall spanned by %o\n", ample, wall;
	    return false, _;
	end if;
    end for;
    // Calculate the rays and maximal cone indices
    rays:=RowSequence(Transpose(KernelMatrix(Transpose(wts))));
    error if not #rays ne 0, "The weights have empty kernel.";
    error if not &and[GCD(v) eq 1 : v in rays], "One or more of the rays is non-primitive";
    error if not #rays eq #SequenceToSet(rays), "There are duplicate rays.  Does W, ample give an irredundant GIT presentation of the toric variety?";
    cone_idxs:=[SetToSequence(sigma) : sigma in Subsets({1..#rays},#rays[1]) |
		ample in Cone([D | divisors[k] : k in [1..#rays] | not k in sigma])];
    // Create the cones
    L:=ToricLattice(#rays[1]);
    ChangeUniverse(~rays,L);
    cones:=[Cone([L | rays[i] : i in I]) : I in cone_idxs];
    // All cones should be of maximum dimension, because we have already checked that the quotient is an orbifold
    error if not &and[Dimension(C) eq Dimension(L) : C in cones], "There is a 'maximal' cone of non-maximal dimension, so something is wrong.";
    F := Fan(rays, cone_idxs : max_cones:=true);   // this checks that the cones really do define a fan
    if not IsComplete(F) then
	// The weights do not define a complete fan, so the corresponding toric variety is not compact (i.e. is not proper)
	return false, _;
    end if;
    error if not &and[IsSimplicial(C) : C in Cones(F)], "The weights do not define a simplicial fan, so (W, ample) does not define a toric orbifold?";
    return true, F;
end function;

////////////////////////////////////////////////////////////////////////////////
// analyze the toric complete intersection defined by:
// * the GIT problem with weights given by W and linearization given by ample;
// * the line bundles specified by bundles
// Here ample is an optional parameter, which defaults to the sum of the weights
// If the verbose flag Fanosearch is set to true, or if the optional parameter
// verbose is set to true (default: false) then it displays:
// * whether or not the ambient toric variety (defined by W and ample) is
//   smooth;
// * whether or not the line bundles are nef; (it throws an error if not)
// * generators for the isotropy groups;
// * the toric subvarieties and line bundles that these isotropy groups fix.
// it returns:
// ambient_is_compact_orbifold, ambient_is_smooth, ambient_is_Fano,
// bundles_are_nef, ci_is_obviously_Fano, bundles_are_pullbacks
// where:
// ambient_is_compact_orbifold is a boolean, true iff the ambient toric variety
//    is a smooth and proper Deligne--Mumford stack;
// ambient_is_smooth is a boolean, true iff the ambient toric variety is smooth;
// ambient_is_Fano is a boolean, true iff the ambient toric variety is Fano;
// bundles_are_nef is a sequence of booleans, one per line bundle, each of which
//    is true iff the corresponding line bundle is nef on the ambient variety;
// ci_is_obviously_Fano is a boolean, true iff -K_Y-sum of c1's of the bundles is
//    ample on the ambient variety Y;
// bundles_are_pullbacks is a sequence of booleans, one per line bundle, each of
//    which is true iff the corresponding line bundle is pulled back from the
//    coarse moduli space of the ambient toric orbifold
////////////////////////////////////////////////////////////////////////////////
function get_orbifold_data(W,bundles : ample:=false, verbose:=false)
    ////////////////////////////////////////////////////////////////////////////////
    // Sanity checks
    ////////////////////////////////////////////////////////////////////////////////
    error if not (#W ne 0 and #W[1] ne 0), "The fan must be defined by at least one weight.";
    n:=#W[1];
    error if not &and[#wt eq n : wt in W], "The weights must all be of the same length.";
    if not ample cmpeq false then
        error if not (Type(ample) eq SeqEnum and #ample eq #W),
	      Sprintf("The parameter 'ample' must be a sequence of integers of length %o.",#W);
        bool, ample:=CanChangeUniverse(ample,Integers());
        error if not bool, "The parameter 'ample' must be a sequence of integers.";
    end if;
    if not &and[#s eq #W : s in bundles] then
       error Sprintf("The intersection data must be a sequence of sequences of integers, each of length %o.",#W);
    end if;
    error if not Type(verbose) eq BoolElt, "Optional parameter 'verbose' should be a boolean.";
    if FanosearchIsVerbose() then
	verbose := true;
    end if;
    ////////////////////////////////////////////////////////////////////////////////
    // make the fan.
    // This will fail if the ambient toric variety is not an orbifold or manifold
    ////////////////////////////////////////////////////////////////////////////////
    ambient_is_compact_orbifold, F := make_fan_from_weights(W : ample:=ample);
    if not ambient_is_compact_orbifold then
	if verbose then
	    printf "The ambient toric variety is not a Deligne--Mumford stack.\n";
	end if;
	return false, _, _, _, _, _;
    end if;
    ////////////////////////////////////////////////////////////////////////////////
    // get divisor data etc
    ////////////////////////////////////////////////////////////////////////////////
    weight_matrix := Matrix(W);
    D, NC, MC := divisor_data_from_weights(W);
    //////////////////////////////////////////////////////////////////////
    // is the ambient variety Fano?
    //////////////////////////////////////////////////////////////////////
    ambient_is_Fano:=IsFano(ToricVariety(Rationals(),F));
    if not ambient_is_Fano then
	if verbose then
	    printf "The ambient toric orbifold is not Fano.\n";
	end if;
    end if;
    //////////////////////////////////////////////////////////////////////
    // is the ambient Fano variety smooth?
    //////////////////////////////////////////////////////////////////////
    ambient_is_smooth := IsNonSingular(F);
    if ambient_is_smooth then
	if verbose then
	    printf "The ambient toric variety is a Fano manifold with weight matrix:\n";
	end if;
    else
	if verbose then
	    printf "The ambient toric variety is a Fano orbifold with weight matrix:\n";
	end if;
    end if;
    if verbose then
	printf "%o\nand anticanonical class %o.\n", weight_matrix, &+D;
    end if;
    L_dual := Ambient(NC);  // L_dual is the lattice of characters of the torus K, where X = C^N//K
    L := Ambient(MC);       // L is the lattice of subgroups of K
    ChangeUniverse(~bundles,L_dual);     // line bundles live in L_dual = Pic
    ////////////////////////////////////////////////////////////////////////////////
    // check that the line bundles are nef
    ////////////////////////////////////////////////////////////////////////////////
    bundles_are_nef := [];
    for c1 in bundles do
	if c1 in NC then
	    Append(~bundles_are_nef,true);
	else
	    Append(~bundles_are_nef,false);
	    if verbose then
		printf "The bundle %o is not nef on the ambient toric variety.\n", c1;
	    end if;
	end if;
    end for;
    if #bundles gt 0 and &and bundles_are_nef then
	if verbose then
	    printf "The line bundles are all nef.\n";
	end if;
    end if;
    //////////////////////////////////////////////////////////////////////
    // check if the complete intersection X is obviously Fano, i.e. if
    // -K_X is the restriction of an ample class on the ambient variety
    //////////////////////////////////////////////////////////////////////
    ci_is_obviously_Fano := IsInInterior(&+D-&+bundles,NC);
    ////////////////////////////////////////////////////////////////////////////////
    // walk the isotropy subgroups
    ////////////////////////////////////////////////////////////////////////////////
    if IsNonSingular(F) then
	bundles_are_pullbacks := [true : bundle in bundles];
	return ambient_is_compact_orbifold, ambient_is_smooth, ambient_is_Fano,
	       bundles_are_nef, ci_is_obviously_Fano, bundles_are_pullbacks;
    else
	if verbose then
	    printf "The isotropy groups are as follows: \n======\n";
	end if;
	maximal_cone_indices := [ConeIndices(F,C) : C in Cones(F,Dimension(Ambient(F)))];
	singular_maximal_cone_indices := [ConeIndices(F,C) : C in Cones(F,Dimension(Ambient(F))) | not IsNonSingular(C)];
	minimal_anticone_indices := [{1..NumberOfColumns(weight_matrix)} diff idxs : idxs in maximal_cone_indices];
	singular_minimal_anticone_indices := [{1..NumberOfColumns(weight_matrix)} diff idxs : idxs in singular_maximal_cone_indices];
	isotropy_submatrices := [Submatrix(weight_matrix,[1..NumberOfRows(weight_matrix)],Sort(SetToSequence(idxs))) : idxs in singular_minimal_anticone_indices];
	// walk the isotropy subgroups and check if they move each line bundle.
	// a line bundle is pulled back from the coarse moduli space iff it is fixed by each
	// isotropy subgroup
	bundles_are_pullbacks := [true : bundle in bundles];
	for M in isotropy_submatrices do
	    phi := LatticeMap(L,ToricLattice(NumberOfRows(M)),M);
	    isotropy_generators := [Preimage(phi,Codomain(phi).k) : k in [1..Dimension(Codomain(phi))]];
	    fixed_indices := [idx : idx in [1..#D] | &and[D[idx]*gen in Integers() : gen in isotropy_generators]];
	    assert &or[idx subset SequenceToSet(fixed_indices) : idx in minimal_anticone_indices]; // the locus didn't get deleted, so it survives in the quotient C^N//K
	    if verbose then
		printf "Isotropy generators: %o\nFixed indices: %o\nDimension of fixed set: %o\n", isotropy_generators, fixed_indices, #fixed_indices - NumberOfColumns(M);
		printf "Fixed line bundles: %o\n======\n", [c1 : c1 in bundles | &and[c1*gen in Integers() : gen in isotropy_generators]];
	    end if;
	    for bundle_idx in [1..#bundles] do
		if not &and[bundles[bundle_idx]*gen in Integers() : gen in isotropy_generators] then
		    bundles_are_pullbacks[bundle_idx] := false;
		end if;
	    end for;
	end for;
	return ambient_is_compact_orbifold, ambient_is_smooth, ambient_is_Fano,
	       bundles_are_nef, ci_is_obviously_Fano, bundles_are_pullbacks;
    end if;
 end function;

////////////////////////////////////////////////////////////////////////////////
// Given a Laurent polynomial f, use the heuristic discussed at the top of the
// code to compute a set of possible struts for f
////////////////////////////////////////////////////////////////////////////////
function all_struts_cheap(f)
    //////////////////////////////////////////////////////////////////////
    // Sanity checks
    //////////////////////////////////////////////////////////////////////
    error if not IsLaurent(f), "Argument must be a Laurent polynomial";
    P := NewtonPolytope(f) ;
    number_of_variables := Rank(Parent(f));
    // THINK: I am not sure if I actually need the origin to be in P,
    // but it seems safer to assume it
    error if not Zero(Ambient(P)) in P,
	  "The Newton polytope of f must contain the origin";
    // THINK:  I am not sure if I need P to be full-dimensional,
    // but (again) it seems safer to assume it
    error if not Dimension(P) eq Dimension(Ambient(P)),
	  "The Newton polytope of f should be full-dimensional.";
    //////////////////////////////////////////////////////////////////////
    // now walk through each vertex of P in turn, nominate a number k of
    // edges incident to v, take the primitive lattice points along these
    // edges, and use these to build a possible k-simplex
    //////////////////////////////////////////////////////////////////////
    known_struts := [Parent(f)|];
    edge_indices := EdgeIndices(P);
    for vertex_number in [1..NumberOfVertices(P)] do
	v := Vertices(P)[vertex_number];
	vprintf Fanosearch: "Analyzing vertex %o\n", v;
	other_vertices := [Vertices(P)[idx] :
			   idx in [1..NumberOfVertices(P)] | {vertex_number,idx} in edge_indices];
	for k in [1..number_of_variables] do
	    vprintf Fanosearch: "Looking for %o-simplices\n", k;
	    for chosen_vertices in Subsets(SequenceToSet(other_vertices),k) do
		Q := Polytope([v] cat [v + PrimitiveLatticeVector(w-v) : w in chosen_vertices]);
		if k gt 1 then
		    // is Q equivalent to a standard k-simplex?
		    // if not, skip it
		    // the condition automatically holds if k eq 1
		    if Volume(Q) ne 1 then
			vprintf Fanosearch: "Skipped a non-standard %o-simplex with vertices %o\n", k, Vertices(Q);
			continue;
		    end if;
		end if;
		possible_strut := PolytopeToLaurent(Q,[1 : i in [1..k+1]]);  // there are exactly k+1 points in Q
		// did we see this strut already?
		already_known := false;
		_, es := CoefficientsAndExponents(possible_strut) ;
		for exponent in es do
		    if possible_strut/Monomial(Parent(f),exponent) in known_struts then
			already_known := true;
			vprintf Fanosearch: "Found strut %o again.  Skipping it.\n", possible_strut;
			break;
		    end if;
		end for;
		if not already_known then
		    Append(~known_struts,possible_strut/Monomial(Parent(f),es[1]));
		    vprintf Fanosearch: "Found a new strut:  %o\n", possible_strut;
		end if;
	    end for;
	end for;
    end for;
    return known_struts;
end function;

////////////////////////////////////////////////////////////////////////////////
// Given a Laurent polynomial f, return all_struts, all_unelim_vars
// where:
// * all_struts is a sequence of all possible struts for a scaffolding
//   of f, in decreasing order by dimension of Newton polytope;
// * all_unelim_vars is a sequence of possible exponents of uneliminated
//   variables;
// This works by exhaustive search for all standard sub-simplices of Newt(f)
////////////////////////////////////////////////////////////////////////////////
function all_struts_and_uneliminated_variables(f)
    //////////////////////////////////////////////////////////////////////
    // Sanity checks
    //////////////////////////////////////////////////////////////////////
    error if not IsLaurent(f), "Argument must be a Laurent polynomial";
    P := NewtonPolytope(f) ;
    number_of_variables := Rank(Parent(f));
    // THINK: I am not sure if I actually need the origin to be in P,
    // but it seems safer to assume it
    error if not Zero(Ambient(P)) in P,
	  "The Newton polytope of f must contain the origin";
    // THINK:  I am not sure if I need P to be full-dimensional,
    // but (again) it seems safer to assume it
    error if not Dimension(P) eq Dimension(Ambient(P)),
	  "The Newton polytope of f should be full-dimensional.";
    //////////////////////////////////////////////////////////////////////
    // now find all possible sub-simplices of P that are standard
    // simplices.  We build these dimension-by-dimension, starting with
    // the zero-simplices
    //////////////////////////////////////////////////////////////////////
    found_simplices := [Polytope([p]) : p in Points(P)];
    current_index := 1;
    while current_index le #found_simplices do
	current_simplex := found_simplices[current_index];
	current_dimension := Dimension(current_simplex);
	if current_dimension eq number_of_variables then
	    // this is already a simplex of maximal dimension
	    // so we cannot add a point to it and get a non-degenerate simplex
	    current_index +:=1;
	    continue;
	end if;
	for pt in Points(P) do
	    Q := Polytope(Vertices(current_simplex) cat [pt]);
	    // only add standard simplices that we haven't seen before
	    if Volume(Q) eq 1 and not Q in found_simplices then
		Append(~found_simplices,Q);
	    end if;
	end for;
	current_index +:=1;
    end while;
    //////////////////////////////////////////////////////////////////////
    // reverse the sequence, so that simplices of larger dimension occur
    // first
    //////////////////////////////////////////////////////////////////////
    Reverse(~found_simplices);
    //////////////////////////////////////////////////////////////////////
    // now remove translates and, at the same time, check whether
    // simplices contain zero and, if they do, record the non-zero
    // vertices as a possible collection of uneliminated variables
    //////////////////////////////////////////////////////////////////////
    known_simplices := [];
    known_unelim := [];
    zero := Zero(Ambient(P));
    for Q in found_simplices do
	// does this give us a new possible collection of uneliminated
	// variables?
	if zero in Vertices(Q) then
	    possible_unelim := Sort([Ambient(Q) | v : v in Vertices(Q) | not IsZero(v)]);
	    if not possible_unelim in known_unelim then
		Append(~known_unelim,possible_unelim);
	    end if;
	end if;
	// did we see a translate of this polytope already?
	found_already := false;
	for v in Vertices(Q) do
	    if Q-v in known_simplices then
		found_already := true;
		break;
	    end if;
	end for;
	if not found_already and Dimension(Q) gt 0 then
	    Append(~known_simplices,Q-Vertices(Q)[1]);
	end if;
    end for;
    //////////////////////////////////////////////////////////////////////
    // now make the simplices into struts and return
    //////////////////////////////////////////////////////////////////////
    known_struts := [PolytopeToLaurent(Q,[1 : i in [1..Dimension(Q)+1]]) :
		     Q in known_simplices];
    return known_struts, known_unelim;
end function;

////////////////////////////////////////////////////////////////////////////////
// given:
// * struts, a set or sequence of struts called struts; and
// * uneliminated_variables, a set or sequence of lattice points
//   (representing the exponents of uneliminated variables)
// checks if the total collection of variables involved forms a
// co-ordinate system on \Cstar^{N-r}.  This is a necessary condition for
// being able to invert the Przyjalkowski trick using these struts and
// uneliminated variables
////////////////////////////////////////////////////////////////////////////////
function is_compatible(struts,uneliminated_variables)
    //////////////////////////////////////////////////////////////////////
    // grab the exponents of the monomials involved
    //////////////////////////////////////////////////////////////////////
    exponents := uneliminated_variables;
    for g in struts do
	_, es := CoefficientsAndExponents(g);
	exponents cat:= [Universe(uneliminated_variables) | xx :
							    xx in es | not IsZero(xx)];
    end for;
    e_mat := Matrix(Integers(),exponents);
    if not NumberOfRows(e_mat) eq NumberOfColumns(e_mat) or
       not Determinant(e_mat) in {1,-1} then
	return false;
    else
	return true;
    end if;
end function;

////////////////////////////////////////////////////////////////////////////////
// given:
// * f, a Laurent polynomial;
// * struts, a sequence of struts; and
// return all possible pieces that can occur in a scaffolding of f that
// uses only struts from the specified set
//
// The return value is { tup_1, tup_2, ..., tup_k } where tup_j is a
// tuple <g,[n_1,...n_r],m> with g a Laurent polynomial ("a piece"),
// n_1,...,n_r the non-negative integers, and m the lattice point
// such that g is the monomial x^m times strut_1^{n_1}*...*strut_r^{n_r}
////////////////////////////////////////////////////////////////////////////////
function all_pieces(f,struts)
    //////////////////////////////////////////////////////////////////////
    // Sanity checks
    //////////////////////////////////////////////////////////////////////
    error if not IsLaurent(f), "f should be a Laurent polynomial";
    error if not Type(struts) eq SeqEnum and Universe(struts) eq Parent(f),
	  "struts should be a sequence of struts";
    //////////////////////////////////////////////////////////////////////
    // basic definitions
    //////////////////////////////////////////////////////////////////////
    P := NewtonPolytope(f);
    number_of_struts := #struts;
    //////////////////////////////////////////////////////////////////////
    // monomials are always legitimate pieces
    //////////////////////////////////////////////////////////////////////
    cs, es := CoefficientsAndExponents(f);
    known_pieces := [ <Monomial(Parent(f),e),
		       [0 : i in [1..number_of_struts]],
		       Ambient(P)!e> :
		      e in es ];
    //////////////////////////////////////////////////////////////////////
    // we will need the Newton polytopes of the struts
    //////////////////////////////////////////////////////////////////////
    strut_polytopes := [NewtonPolytope(strut) : strut in struts];
    //////////////////////////////////////////////////////////////////////
    // now walk the lattice points p in P, moving each strut to p (recall
    // that a strut has the form 1 + monomials) and then working out how
    // much I can dilate the strut and stay inside P
    // then compute all possible products of struts that can stay inside
    // P when based at p, and add these to the possible pieces
    //////////////////////////////////////////////////////////////////////
    for p in Points(P) do
	maximum_dilations := [Integers()|];
	for Q in strut_polytopes do
	    dilation := 1;
	    while p+dilation*Q subset P do
		dilation +:=1;
	    end while;
	    Append(~maximum_dilations,dilation-1);
	end for;
	// at this point maximum_dilations contains a sequence of the
	// maximum dilations of factors based at p
	// we walk over all the possible products of struts
	for dilation in CartesianProduct([PowerSequence(Integers())|[0..k] : k in maximum_dilations]) do
	    // we already added all the monomials, so skip the zero dilation
	    if IsZero([xx : xx in dilation]) then continue; end if;
	    Q := p + &+[dilation[k]*strut_polytopes[k] : k in [1..number_of_struts]];
	    if Q subset P then
		result := <Monomial(Parent(f),p)*
				      &*[struts[k]^dilation[k] : k in [1..number_of_struts]],
				      [xx : xx in dilation],
			   p>;
		Append(~known_pieces,result);
	    end if;
	end for;
    end for;
    return known_pieces;
end function;

////////////////////////////////////////////////////////////////////////////////
// given:
// * struts, a sequence of struts;
// * pieces, a sequence of pieces (as in all_pieces);
// * unelim_variables, a sequence of exponents representing uneliminated
//   monomials;
// return:
// weights, bundles, bundle_columns
// where:
// * weights is a sequence of k integer sequences of length N, being the row
//   sequences of a weight matrix;
// * bundles is a sequence of c integer sequences of length r, being the characters of an
//   r-dimensional torus T;
// * bundle_columns is a sequence of c 0/1 sequences of length N, indicating how
//   the bundles are obtained as sums of the columns of the weight matrix
// The weight matrix defines a toric variety X = C^N//T, where we use the
// linearization given by the sum of the columns of the weight matrix
// (i.e. -K_X) and the sequence bundles defines a collection of line bundles.
// The weights, bundles, and bundle_columns are obtained by inverting the Przjalkowski
// trick using the given collections of struts, pieces, and uneliminated monomials.
////////////////////////////////////////////////////////////////////////////////
function make_ci_data(struts, pieces, unelim_variables)
    //////////////////////////////////////////////////////////////////////
    // Sanity checks
    //////////////////////////////////////////////////////////////////////
    error if not Type(struts) eq SeqEnum,
	  "struts must be a sequence of struts";
    error if not Type(pieces) eq SeqEnum,
	  "pieces must be a sequence of pieces";
    error if not (Type(unelim_variables) eq SeqEnum and Universe(unelim_variables) eq PowerSequence(Integers())),
	  "unelim_variables must be a sequence of integer sequences (i.e. of exponents of monomials)";
    //////////////////////////////////////////////////////////////////////
    // the weight matrix starts with an identity block,
    // of size equal to the number of eliminated variables
    //////////////////////////////////////////////////////////////////////
    identity_block := RowSequence(IdentityMatrix(Integers(),#pieces));
    //////////////////////////////////////////////////////////////////////
    // a line bundle piece with k variables then gives
    // k+1 columns, one of which is distinguished
    // We put all the undistinguished columns first,
    // line bundle by line bundle, and then the columns for
    // uneliminated variables, and then all the
    // distinguished columns from line bundles at the end, line bundle by
    // line bundle
    //////////////////////////////////////////////////////////////////////
    // first build the basis of monomials found in the struts and
    // uneliminated variables
    //////////////////////////////////////////////////////////////////////
    monomials := [PowerSequence(Integers())|];
    for strut in struts do
	_,es := CoefficientsAndExponents(strut);
	monomials cat:= [exp : exp in es | not IsZero(exp)];
    end for;
    monomials cat:= unelim_variables;
    exponent_matrix := Matrix(monomials);
    //////////////////////////////////////////////////////////////////////
    // Sanity check: this should give a basis of monomials
    //////////////////////////////////////////////////////////////////////
    error if not Determinant(exponent_matrix) in {1,-1},
	  "Something is wrong. The collection of monomials is supposed to be a basis.";
    //////////////////////////////////////////////////////////////////////
    // Now build the sub-block corresponding to non-distinguished line
    // bundle variables and uneliminated variables.  This can be read off
    // from the monomial shift of the pieces
    //////////////////////////////////////////////////////////////////////
    target_matrix := Matrix([-piece[3] : piece in pieces]);
    main_block, kernel := Solution(exponent_matrix, target_matrix);
    error if not Dimension(kernel) eq 0, "Something is wrong: there should be an unambiguous solution to this linear system.";
    main_block := RowSequence(main_block);
    //////////////////////////////////////////////////////////////////////
    // build the trailing block
    //////////////////////////////////////////////////////////////////////
    trailing_block := [PowerSequence(Integers())|];
    for piece_idx in [1..#pieces] do
	piece := pieces[piece_idx];
	this_row := [Integers()|];
	idx_shift := 0;
	for strut_idx in [1..#struts] do
	    strut := struts[strut_idx];
	    num_vars_in_strut := Integers()!Evaluate(strut,[1 : i in [1..Rank(Parent(strut))]]) - 1;
	    Append(~this_row,piece[2][strut_idx]-&+[main_block[piece_idx][idx_shift+k] : k in [1..num_vars_in_strut]]);
	    idx_shift +:= num_vars_in_strut;
	end for;
	Append(~trailing_block,this_row);
    end for;
    //////////////////////////////////////////////////////////////////////
    // now we know the weights
    //////////////////////////////////////////////////////////////////////
    wts := [identity_block[k] cat main_block[k] cat trailing_block[k] :
	    k in [1..#pieces]];
    //////////////////////////////////////////////////////////////////////
    // and we can also build the bundles
    //////////////////////////////////////////////////////////////////////
    weight_matrix := Matrix(wts);
    bundles := [PowerSequence(Integers())|];
    bundle_columns := [PowerSequence(Integers())|];
    idx_shift := #pieces;
    for strut_idx in [1..#struts] do
	strut := struts[strut_idx];
    	num_vars_in_strut := Integers()!Evaluate(strut,[1 : i in [1..Rank(Parent(strut))]]) - 1;
	// initialize the bundle data
	this_bundle := Zero(Universe(Columns(weight_matrix)));
	this_bundle_columns := ZeroSequence(Integers(),NumberOfColumns(weight_matrix));
	// this is the part from the columns corresponding to non-distinguished variables
	for col_idx in [1+idx_shift..num_vars_in_strut+idx_shift] do
	    this_bundle +:= Columns(weight_matrix)[col_idx];
	    this_bundle_columns[col_idx] := 1;
	end for;
	// the columns for distinguished variables are at the end
	this_bundle +:= Columns(weight_matrix)[NumberOfColumns(weight_matrix) - #struts + strut_idx];
	this_bundle_columns[NumberOfColumns(weight_matrix) - #struts + strut_idx] := 1;
	// add this to the sequence of bundles
	Append(~bundles, Eltseq(this_bundle));
	Append(~bundle_columns, this_bundle_columns);
	idx_shift +:= num_vars_in_strut;
    end for;
    //////////////////////////////////////////////////////////////////////
    // return
    //////////////////////////////////////////////////////////////////////
    return wts, bundles, bundle_columns;
end function;

////////////////////////////////////////////////////////////////////////////////
// the default termination behaviour for invert_Przjalkowski() is to return
// the first model with ambient a compact toric Fano manifold, nef bundles, and
// such that the complete intersection is obviously Fano
////////////////////////////////////////////////////////////////////////////////
function is_good_model(wts, bundles, bundle_columns)
    // We need the ambient toric variety to be a compact Deligne--Mumford stack
    bool, _ := make_fan_from_weights(wts);
    if not bool then
	// the ambient toric variety is not of Deligne--Mumford type
	return false;
    end if;
    ambient_is_compact_orbifold, ambient_is_smooth, ambient_is_Fano,
    bundles_are_nef, ci_is_obviously_Fano, bundles_are_pullbacks := get_orbifold_data(wts,bundles);
    assert ambient_is_compact_orbifold;
    return ambient_is_smooth and
	   ambient_is_Fano and
	   &and bundles_are_nef and
	   ci_is_obviously_Fano;
end function;

// given:
// * f, a Laurent polynomial;
// * struts, a set of struts;
// * all_unelim, a list of uneliminated variables;
// * terminate_func, a function that specifies what is meant by a good model,
//   as documented below in the preamble to the function invert_Przjalkowski()
// walks through all possible scaffoldings of f that use only the struts
// from 'struts'.  Returns true, wts, bundles, bundle_columns if it finds a
// scaffold that gives a good model; returns false, _, _ if none of the
// scaffolds give a good model.
// The optional parameter 'ranks' restricts the ranks of the toric ambients
// that are considered.
function walk_all_scaffolds(f,struts,all_unelim,terminate_func :
			    ranks:=false)
    //////////////////////////////////////////////////////////////////////
    // basic data
    //////////////////////////////////////////////////////////////////////
    number_of_variables := Rank(Parent(f));
    //////////////////////////////////////////////////////////////////////
    // walk over the compatible sets of uneliminated variables
    //////////////////////////////////////////////////////////////////////
    all_pieces_with_these_struts := SequenceToSet(all_pieces(f,struts));
    for unelim_variables in all_unelim do
	if not is_compatible(struts,unelim_variables) then
	    continue;
	end if;
	unelim_monomials := [Parent(f)| Monomial(Parent(f),e) :
					e in unelim_variables];
	vprintf Fanosearch: "======\nStruts: %o\nUneliminated monomials: %o\n", struts,
		Join([Sprintf("%o",xx) : xx in unelim_monomials],", ");
	//////////////////////////////////////////////////////////////////////
	// set the maximum number of pieces, that is, of eliminated variables
	//////////////////////////////////////////////////////////////////////
	// the Evaluate(...) term here gives the sum of the coefficients of f
	number_of_pieces_bound := Evaluate(f,[1 : i in [1..number_of_variables]]) -
				  #unelim_variables;
	if ranks cmpeq false then
	    these_ranks := [1..number_of_pieces_bound];
	else
	    these_ranks := [xx : xx in [1..number_of_pieces_bound] | xx in ranks];
	end if;
	for number_of_pieces in these_ranks do
	    for pieces in Multisets(all_pieces_with_these_struts,number_of_pieces) do
		scaffold := &+[xx[1] : xx in pieces] +
			    &+unelim_monomials;
		if not IsConstant(f - scaffold) then
		    // this is not a scaffolding
		    continue;
		end if;
		//////////////////////////////////////////////////////////////////////
		// at this point we have found a scaffolding
		// so compute the corresponding c.i. data and see if it gives a good
		// model (i.e. a nef complete intersection in a smooth toric)
		//////////////////////////////////////////////////////////////////////
		vprintf Fanosearch: "Found a scaffolding : %o %o\n", pieces, unelim_monomials;
		wts, bundles, bundle_columns := make_ci_data(struts, [xx : xx in pieces], [PowerSequence(Integers()) | Eltseq(xx) : xx in unelim_variables]);
		if terminate_func(wts, bundles, bundle_columns) then
		    // we found a satisfactory model, so return
		    return true, wts, bundles, bundle_columns;
		end if;
	    // otherwise continue searching
	    end for;
	end for;
    end for;
    // There was no good model, so return
    return false, _, _, _;
end function;

////////////////////////////////////////////////////////////////////////////////
// given:
//
// f, a Laurent polynomial;
//
// try to invert the Przjalkowski trick.  The optional parameter
// ranks specifies the allowed Picard ranks of the ambient toric
// variety, i.e. the allowed number of eliminated variables.  The
// optional parameter codimensions specifies the allowed number of
// line bundles; the default is [0..the number of variables in f].
// The optional parameter use_cheap_heuristic controls whether to
// use a cheaper heuristic (when true) or an exhaustive search
// (when false) to find the possible line bundle pieces.  The
// exhaustive search for possible line bundles is performed in each
// case, because it also finds the possible collections of
// uneliminated variables, but the cheaper version finds fewer
// candidate struts (=line bundles) and then the later search for
// scaffolds is faster but non-exhaustive.  The default is
// use_cheap_heuristic:=true.
// Setting the optional parameter quiet to false (defalut=true)
// turns on the display of progress information.
// The optional parameter terminate_func is a function which is
// passed:
//
// wts,
// bundles
// bundle_columns
//
// as documented in make_ci_data() above.
// The function terminate_func should return true if wts, bundles is a good
// model and false otherwise.  If terminate_func returns true
// invert_Przjalkowski() terminates and returns true, wts, bundles.
// If no good model is found then it returns false, _, _
////////////////////////////////////////////////////////////////////////////////
function invert_Przjalkowski(f : ranks:=false, codimensions:=false,
			     use_cheap_heuristic:=true,
			     terminate_func:=is_good_model, quiet:=true)
    //////////////////////////////////////////////////////////////////////
    // basic definitions
    //////////////////////////////////////////////////////////////////////
    P := NewtonPolytope(f);
    number_of_variables := Rank(Parent(f));
    //////////////////////////////////////////////////////////////////////
    // set the maximum codimension
    //////////////////////////////////////////////////////////////////////
    if codimensions cmpeq false then
	codimensions := [0..number_of_variables];
    else
	codimensions := Sort(SetToSequence({xx : xx in codimensions}));
    end if;
    //////////////////////////////////////////////////////////////////////
    // compute the possible struts and uneliminated variables
    //////////////////////////////////////////////////////////////////////
    if use_cheap_heuristic then
	if not quiet then printf "Computing possible struts heuristically ... "; cpu_t := Cputime(); end if;
	all_struts := all_struts_cheap(f);
	if not quiet then printf "done (%o in %os)\n", #all_struts, Cputime(cpu_t); end if;
	all_uneliminated_variables := AssociativeArray(Integers());
    	if not quiet then printf "Computing possible uneliminated variables ... "; cpu_t := Cputime(); end if;
	all_unelim := [[Ambient(P)|]];
	for k in [1..Dimension(P)-Min(codimensions)] do
	    all_unelim cat:= [ SetToSequence(S) : S in Subsets(Points(P),k) | Volume(Polytope(S)) eq 1];
	end for;
	if not quiet then printf "done (%o in %os)\n", #all_unelim, Cputime(cpu_t); end if;
    else
	if not quiet then printf "Computing all possible struts and uneliminated variables ... "; cpu_t := Cputime(); end if;
	all_struts, all_unelim := all_struts_and_uneliminated_variables(f);
	if not quiet then printf "done (%o, %o in %os)\n", #all_struts, #all_unelim, Cputime(cpu_t); end if;
    end if;
    //////////////////////////////////////////////////////////////////////
    // now walk over all potential scaffoldings and see if any of them
    // are actually scaffoldings of f
    //////////////////////////////////////////////////////////////////////
    for codim in [0..Max(codimensions)] do
	//////////////////////////////////////////////////////////////////////
	// we begin by building the collection of sets of struts that:
	// - are compatible with each other; and
	// - have size codim.
	//////////////////////////////////////////////////////////////////////
	if codim eq 0 then
	    collections_of_struts := [{Universe(all_struts)|}];
	elif codim eq 1 then
	    collections_of_struts := [{xx} : xx in all_struts];
	elif codim eq 2 then
	    if not quiet then cpu_t := Cputime(); printf "Building the legitimate pairs of struts ... "; end if;
	    legitimate_pairs := [PowerSet(Universe(all_struts))|];
	    for S in Subsets(SequenceToSet(all_struts),2) do
		exponents := [];
		for strut in S do
		    _, es := CoefficientsAndExponents(strut);
		    exponents cat:= es;
		end for;
		exponent_polytope := Polytope(exponents);
		if Volume(exponent_polytope) eq 1 and Dimension(exponent_polytope) eq #exponents - #S then
		    Append(~legitimate_pairs,S);
		end if;
	    end for;
	    if not quiet then printf "done (%o in %os)\n", #legitimate_pairs, Cputime(cpu_t); end if;
	    collections_of_struts := legitimate_pairs;
	else
	    if not quiet then cpu_t := Cputime(); printf "Building the legitimate %o-tuples of struts ... ", codim; end if;
	    // to each existing collection add one strut, in all possible
	    // ways such that every pair of struts in the new collection is
	    // legitimate
	    bigger_sets := { Include(S,strut) : strut in all_struts, S in collections_of_struts | &and[{strut,xx} in legitimate_pairs : xx in S]};
	    // some of the resulting collections are bogus, so remove those
	    collections_of_struts := [PowerSet(Universe(all_struts))|];
	    for S in bigger_sets do
		exponents := [];
		for strut in S do
		    _, es := CoefficientsAndExponents(strut);
		    exponents cat:= es;
		end for;
		exponent_polytope := Polytope(exponents);
		if Volume(exponent_polytope) eq 1 and Dimension(exponent_polytope) eq #exponents - #S then
		    Append(~collections_of_struts,S);
		end if;
	    end for;
	    if not quiet then printf "done (%o in %os)\n", #collections_of_struts, Cputime(cpu_t); end if;
	end if;
	//////////////////////////////////////////////////////////////////////
	// skip this codimension, if we don't need to do the computation
	//////////////////////////////////////////////////////////////////////
	if not codim in codimensions then
	    continue;
	end if;
	//////////////////////////////////////////////////////////////////////
	// look at the scaffoldings that can be built from legitimate
	// collections of struts
	//////////////////////////////////////////////////////////////////////
	vprintf Fanosearch: "======================================================================\nCodim: %o\n",codim;
	if not quiet then strut_count := 0; strut_total:=#collections_of_struts; end if;
	for struts in collections_of_struts do
	    if not quiet then
		strut_count+:=1;
		printf "Codimension %o: scaffold type %o of %o ... ",codim,strut_count,strut_total;
		cpu_t := Cputime();
	    end if;
	    bool, wts, bundles, bundle_columns := walk_all_scaffolds(f,SetToSequence(struts),
								     all_unelim,terminate_func :
								     ranks:=ranks);
	    //////////////////////////////////////////////////////////////////////
	    // did we find a good model?  if so, return it
	    //////////////////////////////////////////////////////////////////////
	    if bool then
		if not quiet then printf "done (%os)\n", Cputime(cpu_t); end if;
		return true, wts, bundles, bundle_columns;
	    end if;
	    //////////////////////////////////////////////////////////////////////
	    // we didn't find a good model, so continue the search
	    //////////////////////////////////////////////////////////////////////
	    if not quiet then printf "done (%os)\n", Cputime(cpu_t); end if;
	end for;
    end for;
    return false, _, _, _;
end function;


////////////////////////////////////////////////////////////////////////////////
// Intrinsics
////////////////////////////////////////////////////////////////////////////////


intrinsic CompleteIntersectionForLaurent( f::FldFunRatMElt : ranks:=false,
					  codimensions:=false,
					  use_cheap_heuristic:=true,
					  terminate_func:=is_good_model,
					  quiet:=true) -> SeqEnum[SeqEnum[RngIntElt]], SeqEnum[SeqEnum[RngIntElt]]
{ Given a Laurent polynomial f, returns wts, bundles where the pair (wts, bundles) is GIT data for a 'good' toric complete intersection X such that f is a mirror to X.  Here 'wts' is a sequence of r integer sequences, each of length N, representing the r-by-N weight matrix for a GIT presentation C^N/T of a toric variety, where T is the standard split torus of dimension r;  'bundles' is a sequence of c integer sequences, each of length r, representing characters of T and hence line bundles on the quotient Y = C^N/T.  Note that 'bundles' can be the empty sequence, in which case X will be a toric variety.  By default, 'good' means that X is a Fano manifold, that the ambient toric variety Y is a Fano manifold, that the line bundles L_1,...,L_c specified by 'bundles' are all nef on Y, and that -K_Y - L_1 - ... - L_c is ample on Y.  The meaning of 'good' can be changed via the optional parameter 'terminate_func', which should be a function that accepts the arguments: wts, bundles, bundle_columns.  Here 'wts' and 'bundles' are as described above, and bundle_columns is a sequence of c 0/1 sequences, each of length N, indicating how the bundles are realized as sums of columns of 'wts'.  The function terminate_func should return true if wts, bundles, bundle_columns is a good model and false otherwise.  If terminate_func returns true then CompleteIntersectionForLaurent() terminates and returns wts, bundles, bundle_columns; if no good model is found then an error is raised.  The optional parameter 'ranks' should be a set or sequence of positive integers, specifying the ranks of the ambient toric manifolds Y that should be considered; the default is to consider all possible ranks.  The optional parameter 'codimensions' should be a set or sequence of non-negative integers, specifying the numbers of possible line bundles that should be searched for; the default is [0..k] where k is the number of variables in the Laurent polynomial f, i.e. where k is Rank(Parent(f)).  The optional parameter 'quiet' controls whether progress messages should be displayed (false) or suppressed (true); the default is true.  The optional parameter 'use_cheap_heuristic' should be set to true to use a heuristic to prune the search (this restricts the possible struts that can be used to form a scaffolding of f); the default is true.}
    //////////////////////////////////////////////////////////////////////
    // sanity checks
    //////////////////////////////////////////////////////////////////////
    if not ranks cmpeq false then
	require #ranks gt 0 and
		&and[xx in Integers() : xx in ranks] and
		&and[xx gt 0 : xx in ranks]:
		"Optional parameter 'ranks' must be a non-empty sequence of positive integers.";
    end if;
    if not codimensions cmpeq false then
	require #codimensions gt 0 and
		&and[xx in Integers() : xx in codimensions] and
		&and[xx ge 0 : xx in codimensions]:
		"Optional parameter 'codimensions' must be a non-empty sequence of non-negative integers.";
    end if;
    require Type(use_cheap_heuristic) eq BoolElt:
	    "Optional parameter 'use_cheap_heuristic' must be a boolean.";
    require Type(quiet) eq BoolElt:
	    "Optional parameter 'quiet' must be a boolean.";
    require IsLaurent(f): "Argument 'f' must be a Laurent polynomial";
    //////////////////////////////////////////////////////////////////////
    // check if a good model exists and, if so, return it
    //////////////////////////////////////////////////////////////////////
    bool, wts, bundles, bundle_columns := invert_Przjalkowski(f : ranks:=ranks,
				  			          codimensions:=codimensions,
						                  use_cheap_heuristic:=use_cheap_heuristic,
						                  terminate_func:=terminate_func,
						                  quiet:=quiet);
    require bool: "With these parameter settings, no good complete intersection model for f exists";
    return wts, bundles, bundle_columns;
end intrinsic;

intrinsic HasCompleteIntersectionModel( f::FldFunRatMElt : ranks:=false,
					codimensions:=false,
					use_cheap_heuristic:=true,
					terminate_func:=is_good_model,
					quiet:=true) -> BoolElt, SeqEnum[SeqEnum[RngIntElt]], SeqEnum[SeqEnum[RngIntElt]]
{ Attempts the computation as documented for CompleteIntersectionForLaurent(), with the same optional parameters.  Returns true, wts, bundles, bundle_columns on success and false, _, _ otherwise.  See the documentation for the intrinsic CompleteIntersectionForLaurent().
}
    //////////////////////////////////////////////////////////////////////
    // sanity checks
    //////////////////////////////////////////////////////////////////////
    if not ranks cmpeq false then
	require #ranks gt 0 and
		&and[xx in Integers() : xx in ranks] and
		&and[xx gt 0 : xx in ranks]:
		"Optional parameter 'ranks' must be a non-empty sequence of positive integers.";
    end if;
    if not codimensions cmpeq false then
	require #codimensions gt 0 and
		&and[xx in Integers() : xx in codimensions] and
		&and[xx ge 0 : xx in codimensions]:
		"Optional parameter 'codimensions' must be a non-empty sequence of non-negative integers.";
    end if;
    require Type(use_cheap_heuristic) eq BoolElt:
	    "Optional parameter 'use_cheap_heuristic' must be a boolean.";
    require Type(quiet) eq BoolElt:
	    "Optional parameter 'quiet' must be a boolean.";
    require IsLaurent(f): "Argument 'f' must be a Laurent polynomial";
    //////////////////////////////////////////////////////////////////////
    // do the computation
    //////////////////////////////////////////////////////////////////////
    return invert_Przjalkowski(f : ranks:=ranks, codimensions:=codimensions,
				   use_cheap_heuristic:=use_cheap_heuristic,
				   terminate_func:=terminate_func,
				   quiet:=quiet);
end intrinsic;

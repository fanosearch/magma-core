freeze;

//////////////////////////////////////////////////////////////////////
// normal_form.m
//////////////////////////////////////////////////////////////////////
// Computes a normal form for complete intersection data
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

function validate_input(wts, bundles)
    if Type(wts) ne SeqEnum or Universe(wts) ne PowerSequence(Integers()) then
	return false, "The weights must be a sequence of integer sequences";
    elif #bundles ne 0 and (Type(bundles) ne SeqEnum or Universe(bundles) ne PowerSequence(Integers())) then
	return false, "The bundles must be a sequence of integer sequences";
    elif #wts eq 0 then
	return false, "There must be a non-zero number of weights";
    elif #{#w : w in wts} ne 1 then
	return false, "Every entry in wts must have the same length";
    elif #wts[1] eq 0 then
    	return false, "There must be a non-zero number of weights";
    elif not &and[#b eq #wts : b in bundles] then
	return false, "Every entry in bundles must be of length equal to #wts";
    end if;
    return true, _;
end function;

function normal_form(wts, bundles)
    // build the weight matrix
    m := Matrix(wts);
    cols := Columns(m);
    // record the indices of duplicate columns
    idxs := {[i : i in [1..#cols] | cols[i] eq v] : v in SequenceToSet(cols)};
    // build the permutation group of the columns
    S := Sym(#cols);
    // record the subgroup that leaves the matrix invariant
    stab := sub<S| &cat [[S| (L[1],v) : v in Remove(L,1)] : L in idxs]> ;
    // find the minimum Hermite normal form, after column permutations
    W := HermiteForm(m);
    B := Sort(bundles);
    gg := Identity(S);
    TT := IdentityMatrix(Integers(), #wts);
    P := TransversalProcess(S, stab);
    while TransversalProcessRemaining(P) gt 0 do
	g := TransversalProcessNext(P);
	H, T := HermiteForm(m*PermutationMatrix(Integers(),g));
	if H lt m then
	    W := H;
	    B := Sort([Eltseq(T*Transpose(Matrix([b]))) : b in bundles]);
	    gg := g;
	    TT := T;
	end if;
    end while;
    return RowSequence(W), B, gg, TT;
end function;

//////////////////////////////////////////////////////////////////////
// Intrinsics
//////////////////////////////////////////////////////////////////////

intrinsic NormalForm( W::SeqEnum[SeqEnum[RngIntElt]], B::SeqEnum[SeqEnum[RngIntElt]])
    -> SeqEnum[SeqEnum[RngIntElt]], SeqEnum[SeqEnum[RngIntElt]], GrpPermElt, AlgMatElt
    {Given weights W and bundle data B, compute a normal form for (W, B) under the left action of GL(N,Z) and column permutations.  Here N is the number of rows of W.  Return W', B', g, T where W' is the minimal Hermite normal form, B' is the corresponding bundle data (sorted), and g and T are such that T*W*PermutationMatrix(g) = W'.}
    // Sanity checks
    bool, msg := validate_input(W, B);
    require bool: msg;
    // Do the computation
    return normal_form(W,B);
end intrinsic;

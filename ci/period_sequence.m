freeze;

/////////////////////////////////////////////////////////////////////////
// period_sequence.m
/////////////////////////////////////////////////////////////////////////
// Given a complete intersection data in a smooth toric Fano variety,
// calculates the period sequence.
/////////////////////////////////////////////////////////////////////////

import "divisor_data.m": divisor_data_from_weights, divisor_data_from_variety;
import "validate.m": validate_toric_data_and_bundles, validate_weight_data_and_bundles;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Computes the n-th coefficient of the I-function for the given geometric data
function nth_term_I_function(D,NC,MC,degrees_to_sum,
  line_bundles,n)
    // Compute the basic geometric data
    mK:=&+D;
    // Ensure that the line bundles are in the correct ambient
    ChangeUniverse(~line_bundles,Ambient(NC));
    // We force the computation of the Hilbert basis for "degrees_to_sum" using
    // the "Hemmecke" algorithm
    _:=HilbertBasis(degrees_to_sum : algorithm:="Hemmecke");
    // Calculate the coefficient of the n-th term of the I-function for the
    // complete intersection
    c1:=&+line_bundles;
    coeff:=Rationals() ! 0;
    for d in Points(degrees_to_sum,mK - c1,n) do
        twist_factor:=&*[Integers() |
            Factorial(Integers() ! (rho * d)) : rho in line_bundles];
        ambient_factor:=&*[Integers() |
            Factorial(Integers() ! (D[i] * d)) : i in [1..#D]];
        coeff +:= twist_factor / ambient_factor;
    end for;
    return coeff;
end function;

// Returns the specified coefficients of the I-function for the
// complete intersection specified by line_bundles (a sequence of elements of
// the nef cone NC). D is the divisor data, NC and MC are the nef and Mori
// cones, degrees_to_sum is the intersection of MC with the half-spaces
// D[i] ge 0. "start" and "finish" are indexed from zero, i.e. they correspond
// to the power of t in the I-function. The resulting sequence is defined
// over the rationals.
function I_function(D,NC,MC,degrees_to_sum,line_bundles,
  start,finish)
    // Compute the basic geometric data
    mK:=&+D;
    // Ensure that the line bundles are in the correct ambient
    ChangeUniverse(~line_bundles,Ambient(NC));
    // We force the computation of the Hilbert basis for "degrees_to_sum" using
    // the "Hemmecke" algorithm
    _:=HilbertBasis(degrees_to_sum : algorithm:="Hemmecke");
    // Calculate the I-function for the complete intersection
    c1:=&+line_bundles;
    I:=[Rationals()|];
    for h in [start..finish] do
        coeff:=Rationals() ! 0;
        for d in Points(degrees_to_sum,mK - c1,h) do
            twist_factor:=&*[Integers() |
                Factorial(Integers() ! (rho * d)) : rho in line_bundles];
            ambient_factor:=&*[Integers() |
                Factorial(Integers() ! (D[i] * d)) : i in [1..#D]];
            coeff +:= twist_factor / ambient_factor;
        end for;
        Append(~I,coeff);
    end for;
    // Return what we have
    return I;
end function;

// Takes the coefficients [c0,c1,c2,...] of an I-function and applies the mirror
// map so as to zero c1, and then regularises. Attempts to cast the resulting
// sequence into the integers before returning.
function mirror_map_and_regularize(I_coeffs)
    // Create the ring for the quantum period to live in
    R:=PolynomialRing(Rationals());
    t:=R.1;
    I:=R ! I_coeffs;
    // Do the "mirror map", which just amounts to multiplying by exp(at) so as
    // to zero the coefficient of t in exp(at)*I
    n:=#I_coeffs - 1;
    if Coefficient(I,1) eq 0 then
        I_after_mirror_map:=I;
    else
        a := -Coefficient(I,1);
        exp_factor:=&+[R | a^k * t^k / Factorial(k) : k in [0..n]];
        I_after_mirror_map:=I * exp_factor;
        // This is only correct up to degree n, so truncate it
        I_after_mirror_map:=R ! (Coefficients(I_after_mirror_map)[1..n + 1]);
    end if;
    // Regularise
    cs:=Coefficients(I_after_mirror_map);
    I_reg:=R ! [Universe(cs) | cs[k] * Factorial(k - 1) : k in [1..#cs]];
    // Rebuild the period sequence
    ps:=Coefficients(I_reg);
    if #ps lt n + 1 then
        ps cat:= ZeroSequence(Universe(ps),n + 1 - #ps);
    end if;
    // Attempt to cast the sequence into the integers before returning
    bool,intps:=CanChangeUniverse(ps,Integers());
    if bool then ps:=intps; end if;
    return ps;
end function;

// Returns n + 1 terms of the regularised period sequence for the complete
// intersection specified by line_bundles (a sequence of elements of the nef
// cone NC). D is the divisor data, NC and MC are the nef and Mori cones,
// degrees_to_sum is the intersection of MC with the half-spaces D[i] ge 0.
function period_sequence(D,NC,MC,degrees_to_sum,
  line_bundles,n)
    return mirror_map_and_regularize(
        I_function(D,NC,MC,degrees_to_sum,line_bundles,0,n));
end function;

/////////////////////////////////////////////////////////////////////////
// I-function coefficient process
/////////////////////////////////////////////////////////////////////////

// Given an I-function coefficient process P, returns true followed by
// the next coefficient
function I_function_next_value(P)
    // Recover the data for this process. The format for the data is:
    //  [* D,NC,degrees_to_sum,line_bundles,final_idx,next_idx,step_size *]
    I:=GetUserProcessData(P);
    if Type(I[5]) eq RngIntElt and I[6] gt I[5] then
        return false,_;
    end if;
    // Calculate the coefficient
    coeff:=nth_term_I_function(I[1],I[2],Dual(I[2]),I[3],I[4],I[6]);
    // Update the data and return
    I[6] +:= I[7];
    SetUserProcessData(P,I);
    return true,coeff;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic PeriodSequenceForCompleteIntersection( X::TorVar,
    K::SeqEnum[SeqEnum[RngIntElt]], n::RngIntElt : allow_weak_fano:=false )
    -> SeqEnum[RngIntElt]
{The first n+1 terms of the period sequence corresponding to the given complete intersection in the smooth toric Fano variety (or weighted projective space) X. Set 'allow_weak_fano' to true (default: false) to allow weak Fano ambients.}
    // Sanity check
    require n gt 0: "The number of terms to compute must be a positive integer";
    bool,msg:=validate_toric_data_and_bundles(X,K :
                                              allow_weak_fano:=allow_weak_fano);
    require bool: msg;
    // Calculate the basic geometric data
    D,NC,MC:=divisor_data_from_variety(X);
    degrees_to_sum:=ConeWithInequalities(D);
    if not degrees_to_sum subset MC then
        degrees_to_sum:=MC meet degrees_to_sum;
    end if;
    // Calculate the period sequence
    ps:=period_sequence(D,NC,MC,degrees_to_sum,K,n);
    require Universe(ps) cmpeq Integers():
        "Unable to recast period sequence as a sequence of integers. Perhaps the input data was invalid?";
    return ps;
end intrinsic;

intrinsic PeriodSequenceForCompleteIntersection( D::SeqEnum, NC::TorCon,
    K::SeqEnum, n::RngIntElt : degrees_to_sum:=false ) -> SeqEnum[RngIntElt]
{The first n+1 terms of the period sequence corresponding to the given complete intersection in the smooth toric Fano variety (or weighted projective space) X generated by the given divisor data. By default 'degrees_to_sum' is the intersection of the Mori cone with the half-spaces D[i] ge 0. 'start' and 'finish' index from zero.}
    // Sanity check
    require n gt 0: "The number of terms to compute must be a positive integer";
    L:=Ambient(NC);
    bool,D:=CanChangeUniverse(D,L);
    require bool: "Argument 1 must lie in the same toric lattice as argument 2";
    bool,K:=CanChangeUniverse(K,L);
    require bool: "Argument 3 must lie in the same toric lattice as argument 2";
    MC:=Dual(NC);
    require (Type(degrees_to_sum) eq BoolElt and degrees_to_sum eq false) or
        (Type(degrees_to_sum) eq TorCon and
        Ambient(degrees_to_sum) eq Dual(L) and degrees_to_sum subset MC):
        "Parameter 'degrees_to_sum' must be a subcone of the Mori cone";
    // If necessary, create the degrees to sum
    if Type(degrees_to_sum) eq BoolElt then
        degrees_to_sum:=ConeWithInequalities(D);
        if not degrees_to_sum subset MC then
            degrees_to_sum:=MC meet degrees_to_sum;
        end if;
    end if;
    // Calculate the period sequence
    ps:=period_sequence(D,NC,MC,degrees_to_sum,K,n);
    require Universe(ps) cmpeq Integers():
        "Unable to recast period sequence as a sequence of integers. Perhaps the input data was invalid?";
    return ps;
end intrinsic;

intrinsic PeriodSequenceForCompleteIntersection( W::SeqEnum[SeqEnum[RngIntElt]],
    K::SeqEnum[SeqEnum[RngIntElt]], n::RngIntElt : ample:=false,
    define_fan:=false, allow_weak_fano:=false ) -> SeqEnum[RngIntElt]
{The first n+1 terms of the period sequence corresponding to the given complete intersection in the smooth toric Fano variety (or weighted projective space) X generated by the given weight data. The optional parameter 'ample' can be used to specify the ample divisor to use. If the optional parameter 'define_fan' (default: false) is set to true then the verification that the data defines a smooth toric Fano variety is skipped. Set 'allow_weak_fano' to true (default: false) to allow weak Fano ambients.}
    // Sanity check
    require n gt 0: "The number of terms to compute must be a positive integer";
    bool,ample:=validate_weight_data_and_bundles(W,K : ample:=ample,
                      define_fan:=define_fan, allow_weak_fano:=allow_weak_fano);
    require bool: ample;
    // Calculate the basic geometric data
    D,NC,MC:=divisor_data_from_weights(W : ample:=ample);
    degrees_to_sum:=ConeWithInequalities(D);
    if not degrees_to_sum subset MC then
        degrees_to_sum:=MC meet degrees_to_sum;
    end if;
    // Calculate the period sequence
    ps:=period_sequence(D,NC,MC,degrees_to_sum,K,n);
    require Universe(ps) cmpeq Integers():
        "Unable to recast period sequence as a sequence of integers. Perhaps the input data was invalid?";
    return ps;
end intrinsic;

intrinsic IFunctionCoefficientsForCompleteIntersection( X::TorVar,
    K::SeqEnum[SeqEnum[RngIntElt]], start::RngIntElt, finish::RngIntElt :
    allow_weak_fano:=false ) -> SeqEnum[RngIntElt]
{The coefficients in the range [start..finish] for the I-function corresponding to the given complete intersection in the smooth toric Fano variety (or weighted projective space) X. Set 'allow_weak_fano' to true (default: false) to allow weak Fano ambients. 'start' and 'finish' index from zero.}
    // Sanity check
    require start ge 0 and finish ge start:
        "The 'start' and 'finish' terms must be non-negative integers, and 'finish' must be at least equal to 'start'";
    bool,msg:=validate_toric_data_and_bundles(X,K :
                                              allow_weak_fano:=allow_weak_fano);
    require bool: msg;
    // Calculate the basic geometric data
    D,NC,MC:=divisor_data_from_variety(X);
    degrees_to_sum:=ConeWithInequalities(D);
    if not degrees_to_sum subset MC then
        degrees_to_sum:=MC meet degrees_to_sum;
    end if;
    // Calculate the coefficients
    return I_function(D,NC,MC,degrees_to_sum,K,start,finish);
end intrinsic;

intrinsic IFunctionCoefficientsForCompleteIntersection( D::SeqEnum,
    NC::TorCon, K::SeqEnum, start::RngIntElt, finish::RngIntElt :
    degrees_to_sum:=false ) -> SeqEnum[FldRatElt]
{The coefficients in the range [start..finish] for the I-function corresponding to the given complete intersection in the smooth toric Fano variety (or weighted projective space) X generated by the given divisor data. By default 'degrees_to_sum' is the intersection of the Mori cone with the half-spaces D[i] ge 0. 'start' and 'finish' index from zero.}
    // Sanity check
    require start ge 0 and finish ge start:
        "The 'start' and 'finish' terms must be non-negative integers, and 'finish' must be at least equal to 'start'";
    L:=Ambient(NC);
    bool,D:=CanChangeUniverse(D,L);
    require bool: "Argument 1 must lie in the same toric lattice as argument 2";
    bool,K:=CanChangeUniverse(K,L);
    require bool: "Argument 3 must lie in the same toric lattice as argument 2";
    MC:=Dual(NC);
    require (Type(degrees_to_sum) eq BoolElt and degrees_to_sum eq false) or
        (Type(degrees_to_sum) eq TorCon and
        Ambient(degrees_to_sum) eq Dual(L) and degrees_to_sum subset MC):
        "Parameter 'degrees_to_sum' must be a subcone of the Mori cone";
    // If necessary, create the degrees to sum
    if Type(degrees_to_sum) eq BoolElt then
        degrees_to_sum:=ConeWithInequalities(D);
        if not degrees_to_sum subset MC then
            degrees_to_sum:=MC meet degrees_to_sum;
        end if;
    end if;
    // Calculate the coefficients
    return I_function(D,NC,MC,degrees_to_sum,K,start,finish);
end intrinsic;

intrinsic IFunctionCoefficientsForCompleteIntersection(
    W::SeqEnum[SeqEnum[RngIntElt]], K::SeqEnum[SeqEnum[RngIntElt]],
    start::RngIntElt, finish::RngIntElt : ample:=false, define_fan:=false,
    allow_weak_fano:=false ) -> SeqEnum[FldRatElt]
{The coefficients in the range [start..finish] for the I-function corresponding to the given complete intersection in the smooth toric Fano variety (or weighted projective space) X generated by the given weight data. The optional parameter 'ample' can be used to specify the ample divisor to use. If the optional parameter 'define_fan' (default: false) is set to true then the verification that the data defines a smooth toric Fano variety is skipped. Set 'allow_weak_fano' to true (default: false) to allow weak Fano ambients. 'start' and 'finish' index from zero.}
    // Sanity check
    require start ge 0 and finish ge start:
        "The 'start' and 'finish' terms must be non-negative integers, and 'finish' must be at least equal to 'start'";
    bool,ample:=validate_weight_data_and_bundles(W,K : ample:=ample,
                      define_fan:=define_fan, allow_weak_fano:=allow_weak_fano);
    require bool: ample;
    // Calculate the basic geometric data
    D,NC,MC:=divisor_data_from_weights(W : ample:=ample);
    degrees_to_sum:=ConeWithInequalities(D);
    if not degrees_to_sum subset MC then
        degrees_to_sum:=MC meet degrees_to_sum;
    end if;
    // Calculate the coefficients
    return I_function(D,NC,MC,degrees_to_sum,K,start,finish);
end intrinsic;

intrinsic IFunctionCoefficientsProcessForCompleteIntersection(
    W::SeqEnum[SeqEnum[RngIntElt]], K::SeqEnum[SeqEnum[RngIntElt]] :
    ample:=false, define_fan:=false, allow_weak_fano:=false, start:=0,
    finish:=false, step:=1 ) -> Process
{A process to generate sucessive coefficients of the I-function corresponding to the given complete intersection in the smooth toric Fano variety (or weighted projective space) X generated by the given weight data. The optional parameter 'ample' can be used to specify the ample divisor to use. If the optional parameter 'define_fan' (default: false) is set to true then the verification that the data defines a smooth toric Fano variety is skipped. Set 'allow_weak_fano' to true (default: false) to allow weak Fano ambients. The optional parameter 'start' can be used to specify the initial term to calculate (indexed from zero), and 'step' can adjust the step size. The optional parameter 'finish' can be used to specify the final term to calculate; the default is for the process to be infinite.}
    // Sanity check
    require Type(start) eq RngIntElt and start ge 0:
        "Parameter 'start' must be a non-negative integer";
    require (Type(finish) eq BoolElt and finish eq false) or
        (Type(finish) eq RngIntElt and finish ge start):
        "Parameter 'finish' must be a non-negative integer at least equal to 'start'";
    require Type(step) eq RngIntElt and step ge 1:
        "Parameter 'step' must be a positive integer";
    bool,ample:=validate_weight_data_and_bundles(W,K : ample:=ample,
                      define_fan:=define_fan, allow_weak_fano:=allow_weak_fano);
    require bool: ample;
    // Calculate the basic geometric data
    D,NC,MC:=divisor_data_from_weights(W : ample:=ample);
    degrees_to_sum:=ConeWithInequalities(D);
    if not degrees_to_sum subset MC then
        degrees_to_sum:=MC meet degrees_to_sum;
    end if;
    // Return the process
    return UserProcess("I-function Coefficients",I_function_next_value,
              I_function_next_value,[* D, NC, degrees_to_sum, K, finish, start,step *]);
end intrinsic;

intrinsic MirrorMapAndRegularize( cs::SeqEnum[FldRatElt] )
    -> SeqEnum[RngIntElt]
{Applies the mirror map to and then regularises the sequence cs of I-function coefficients. In other words, applies an exponential shift so as to zero the first term in cs, and then does the trick with factorials.}
    require #cs ge 2: "Argument must have length at least 2.";
    ps:=mirror_map_and_regularize(cs);
    require Universe(ps) cmpeq Integers():
        "Unable to recast period sequence as a sequence of integers. Perhaps the input data was invalid?";
    return ps;
end intrinsic;

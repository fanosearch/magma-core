freeze;

/////////////////////////////////////////////////////////////////////////
// secondary_fan.m
/////////////////////////////////////////////////////////////////////////
// Calculates the secondary fan of a compact toric variety X from a
// weight matrix for X
/////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// Returns the toric lattice containing the columns of the weight
// matrix given by wts, the toric divisors (i.e. the columns), and
// the possible minimal anticones
function basic_data(wts);
    // make the lattice containing the toric divisors
    L := ToricLattice(#wts);
    // make the divisors
    D := {L | c : c in Columns(Matrix(wts))};
    // make the support of the secondary fan
    support := Cone(SetToSequence(D));
    // make the possible minimal anticones
    anticones := [];
    for S in Subsets(D,Dimension(L)) do
	C := Cone(S);
	if Dimension(C) eq Dimension(L) then
	    Append(~anticones, C);
	end if;
    end for;
    return L, support, anticones;
end function;

// Returns true followed by the largest t such that the point
// a + t(b-a) on the line from a to b lies in the cone C, if such a
// t exists, and false, _ otherwise.
function exit_time(C,a,b)
    // sanity checks
    error if Type(C) ne TorCon, "C must be a cone";
    L := Ambient(C);
    error if not (a in L and b in L), "a and b must lie in the same lattice as C";
    // the relevant inequalities are those that are negative
    // on the direction vector of the line
    ineqs := [ v : v in Inequalities(C) | v * (b-a) lt 0 ];
    // if there are no such inequalities, the line either
    // never enters the cone or never leaves it
    if #ineqs eq 0 then
	return false, _;
    end if;
    crossing_times := [ -(v*a)/(v*(b-a)) : v in ineqs ];
    min := Min(crossing_times);
    return true, min;
end function;

// Returns true followed by the smallest t such that the point
// a + t(b-a) on the line from a to b lies in the cone C, if such a
// t exists, and false, _ otherwise.
function entry_time(C,a,b)
    // sanity checks
    error if Type(C) ne TorCon, "C must be a cone";
    L := Ambient(C);
    error if not (a in L and b in L), "a and b must lie in the same lattice as C";
    // the relevant inequalities are those that are positive
    // on the direction vector of the line
    ineqs := [ v : v in Inequalities(C) | v * (b-a) gt 0 ];
    // if there are no such inequalities, the line either
    // never enters the cone or never leaves it
    if #ineqs eq 0 then
	return false, _;
    end if;
    crossing_times := [ -(v*a)/(v*(b-a)) : v in ineqs ];
    max := Max(crossing_times);
    return true, max;
end function;

// Returns the ok, C if the stability condition A lies in the
// interior of a chamber C, and false, _ otherwise
function chamber(anticones, A)
    // we start with the full lattice
    result := FullCone(Parent(A));
    // empty is true iff no cone covers A
    empty := true;
    for C in anticones do
	if IsInInterior(A,C) then
	    result meet:= C;
	    empty := false;
	elif A in C then
	    // A lies in the boundary of C, so A cannot lie
	    // in the interior of any chamber
	    return false, _;
	end if;
    end for;
    if empty then
	return false, _;
    end if;
    return true, result;
end function;

// Returns the nef cone associated to a random point in support.
// This is guaranteed to be of full dimension.
function random_nef_cone(support, anticones)
    // build a height function;
    h := &+Rays(Dual(support));
    // start at height 5 and work upwards
    height := 5;
    repeat
	for p in Points(support, h, height) do
	    if not IsInInterior(p, support) then
		continue;
	    end if;
	    ok, C := chamber(anticones, p);
	    if ok then
		assert Dimension(C) eq Dimension(Ambient(C));
		return C;
	    end if;
	end for;
	height +:=1;
    // we loop forever
    until false;
end function;

// Updates the cone and facet data.  If C is already in cones then this
// does nothing.  Otherwise it adds C to cones, adds any facets of C
// that are not in facets or walls_of_support to facets (and facet_idxs)
// and removes any facets of C that are already in facets (together with
// their facet indices)
procedure add_cone_and_facets(C, ~cones, ~facets, ~facet_idxs, walls_of_support)
    // Check if C is new
    if C in cones then
	return;
    end if;
    // Add C to cones and grab the facets of C
    Append(~cones, C);
    Fs := Facets(C);
    // Process each facet in turn
    for F in Fs do
	idx := Index(facets, F);
	if idx eq 0 then
	    // the facet is new
	    if not &or[F subset W : W in walls_of_support] then
		// F doesn't lie on the boundary of the support
		// so we add it
		Append(~facets, F);
		Append(~facet_idxs, #cones);
	    end if;
	else
	    // sanity check: the two cones should meet along F
	    x := SupportingHyperplane(C,F);
	    y := SupportingHyperplane(cones[facet_idxs[idx]],F);
	    assert IsZero(x+y);
	    // remove F from the list of facets we need to process
	    Remove(~facets, idx);
	    Remove(~facet_idxs, idx);
	end if;
    end for;
end procedure;

// Returns a point in the relative interior of the cone C
function point_in_interior(C)
    return &+Rays(C);
end function;

// Grows the collection of cones by adding the cone on the opposite side
// of a facet in facets, one at a time.  Returns when there are no more
// facets to process.
//
// cones records the cones that we have found so far
// facets records the facets that we still need to cross
// facet_idxs[i] records the index in cones of the cone that has
//                  facets[i] as a facet
// support is the support of the secondary fan (which is a cone)
// walls_of_support is the facets of support
// anticones is the sequence of possible minimal anticones
procedure grow_secondary_fan(~cones, ~facets, ~facet_idxs, walls_of_support, support, anticones)
    while #facets ne 0 do
	vprintf Fanosearch: "Growing secondary fan.  #facets remaining=%o, #cones=%o.\n", #facets, #cones;
	// grab a facet and a point that contains it
	F := facets[#facets];
	C := cones[facet_idxs[#facets]];
	a := point_in_interior(F);
	b := point_in_interior(C);
	// we look at the line from a to b, and choose a stability condition
	// on this line
	ok, t := entry_time(support,a,b);
	if not ok then
	    t := -2;
	end if;
	while F in facets do
	    t := t/2;
	    ok, newC := chamber(anticones, a+t*(b-a));
	    while not ok do
		t := t/2;
		ok, newC := chamber(anticones, a+t*(b-a));
	    end while;
	    add_cone_and_facets(newC, ~cones, ~facets, ~facet_idxs, walls_of_support);
	end while;
    end while;
end procedure;

// Returns the maximal cones of the secondary fan.  Assumes that wts is a non-empty sequence of integer sequences of equal lengths.  Will raise a RuntimeError if the secondary fan defined by the weights is not strictly convex, or is not of full dimension.
function secondary_fan(wts)
    // L is the lattice containing columns of the weight matrix
    // support is the support of the secondary fan (which is a cone)
    // anticones is the possible minimal anticones
    L, support, anticones := basic_data(wts);
    // sanity checks
    // THINK this might actually be completely OK when the secondary
    // fan is not convex, or equivalently, when the toric varieties
    // are non-compact
    error if not IsStrictlyConvex(support),
	  "The weights define a secondary fan with non-convex support.";
    error if not Dimension(support) eq Dimension(Ambient(support)),
	  "The weights define a secondary fan which is not of full dimension.";
    // walls_of_support is the boundary of the support of the secondary fan
    walls_of_support := Facets(support);
    // cones records the cones that we have found so far
    cones := [];
    // facets records the facets that we still need to cross
    facets := [];
    // facet_idxs[i] records the index in cones of the cone that has
    // facets[i] as a facet
    facet_idxs := [];
    // start by choosing a random stability condition
    C := random_nef_cone(support, anticones);
    add_cone_and_facets(C, ~cones, ~facets, ~facet_idxs, walls_of_support);
    // grow the secondary fan, by adding the cone on the other side of a
    // facet one facet at a time
    grow_secondary_fan(~cones, ~facets, ~facet_idxs, walls_of_support, support, anticones);
    vprintf Fanosearch: "Finished:  #facets remaining=%o, #cones=%o.\n", #facets, #cones;
    return cones;
end function;

//////////////////////////////////////////////////////////////////////
// Intrinsics
//////////////////////////////////////////////////////////////////////

intrinsic SecondaryFanWithWeights( W::SeqEnum[SeqEnum[RngIntElt]] : define_fan:=false)
    -> TorFan
{Given weights W defining a toric variety, computes the secondary fan.  An error will be raised unless the secondary fan is full-dimensional (that is, unless the weight data defines a toric Deligne--Mumford stack) and the support of the secondary fan is strictly convex (that is, unless the toric varieties defined by the weights are compact). The optional parameter 'define_fan' (default: false) can be set to true to skip the verification that the cones calculated do indeed form a fan.}
    // Sanity check
    require #W gt 0: "The weights must be non-empty";
    require #{#w : w in W} eq 1: "The weights must be a sequence of integer sequences of the same length.";
    require #W[1] gt 0: "The weights must be non-empty";
    // Do the computation
    return Fan(secondary_fan(W) : define_fan:=define_fan);
end intrinsic;

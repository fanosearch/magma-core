freeze;

/////////////////////////////////////////////////////////////////////////
// ci.m
/////////////////////////////////////////////////////////////////////////
// Given a smooth toric Fano variety X, compute all period sequences of
// complete intersections in X corresponding to the that are cut out by
// direct sums of non-trivial nef line bundles.
/////////////////////////////////////////////////////////////////////////

import "divisor_data.m": divisor_data_from_variety, divisor_data_from_weights;
import "period_sequence.m": period_sequence;
import "validate.m": validate_dimensions, validate_toric_data, validate_weight_data;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// All decompositions of n into k non-negative parts (in any order)
function non_negative_decompositions(n,k)
    partitions:=[[xx - 1 : xx in yy] : yy in Partitions(n + k,k)];
    return SetToSequence(&join[{[xx[j^g] : j in [1..#xx]] : xx in partitions} :
                                                                  g in Sym(k)]);
end function;

// Given Q, a sequence of sequences of integers of the same length, and w,
// another sequence of integers of that same length, returns an iterator for the
// points in the positive orthant corresponding to the coefficients of linear
// relations in Q that achieve w.
function toric_knapsack_process(Q,w)
    P:=ToricKnapsack(Q,w);
    error if not IsPolytope(P),
        "Solution space has a non-zero dimensional kernel";
    return ModifyProcess(PointProcess(P),Eltseq);
end function;

// Recursively searches for a simplicial cone (using the given inequalities).
// Returns the first such cone found.
function search_for_simplicial_subcone(ineqs,cone_ineqs,d)
    if #cone_ineqs eq d then
    	C:=Cone(cone_ineqs);
    	if Dimension(C) eq d and IsStrictlyConvex(C) then
    	    return true,C;
    	end if;
    	return false,_;
    end if;
    for i in [1..#ineqs] do
    	bool,C:=$$(ineqs[i + 1..#ineqs],Append(cone_ineqs,ineqs[i]),d);
    	if bool then
    	    return true,C;
    	end if;
    end for;
    return false,_;
end function;

// Given a point pt in a bounding box with top right corner v and bottom left
// corner the origin, returns true followed by the next point along in lex
// order, if such a point exists, and returns false otherwise. v and pt are
// sequences of integers. On call, i should be set equal to #pt.
function next_point(pt,v,i)
    pt[i] +:= 1;
    if pt[i] gt v[i] then
    	if i eq 1 then
    	    return false,_;
    	else
    	    return $$(pt,v,i-1);
    	end if;
    end if;
    for j in [i+1..#pt] do
    	pt[j]:=0;
    end for;
    return true,pt;
end function;

// Let NC be a cone in d dimensional space defined by inequalities ineqs. We
// define H to be the matrix given by the pruned inequalities,
//      H = RemoveRow(Transpose(Matrix(ineqs)),d)
//  and final_H to be the sequence corresponding to the row that was pruned,
//      final_H = [I.d : I in ineqs].
// Let pt be a point on a codimension - 1 face of a bounding box, pt cat [0]
// and pt cat [a] the endpoints of the interval in which we will search. This
// returns all the points v along this interval (including endpoints) for which
// "v in NC" and "mK - v in Interior(NC)".
function scan_along_ray(pt,a,H,final_H,mK)
    // Make a note of the ambient lattice
    L:=Dual(Parent(mK));
    d:=Dimension(L);
    // We begin by evaluating pt and Prune(mK) - v at all the inequalities
    // (suitably pruned).
    pt_eval:=Eltseq(Vector(pt) * H);
    mKpt:=Prune(Eltseq(mK));
    mKpt:=[Integers() | mKpt[i] - pt[i] : i in [1..#pt]];
    mKpt_eval:=Eltseq(Vector(mKpt) * H);
    // We need to know all 0 <= t <= a such that:
    //  pt_eval[i] + t * final_H[i] >= 0 and
    //  mKpt_eval[i] + (mK[d] - t) * final_H[i] > 0.
    // We begin by absorbing mK[d] * final_H[i] into mKpt_eval[i].
    mKd:=Eltseq(mK)[d];
    for i in [1..#final_H] do
        mKpt_eval[i] +:= mKd * final_H[i];
    end for;
    // The equations we need to satisfy are now:
    //  0 <= t <= a,
    //  pt_eval[i] + t * final_H[i] >= 0,
    //  mKpt_eval[i] - t * final_H[i] > 0.
    // So if final_H[i] > 0:
    //  Ceiling(-pt_eval[i] / final_H[i]) <= t <= Ceiling(mKpt_eval[i] / final_H[i]) - 1
    // If final_H[i] < 0:
    //  Floor(mKpt_eval[i] / final_H[i]) + 1 <= t <= Floor(-pt_eval[i] / final_H[i])
    // If final_H[i] = 0 then we need pt_eval[i] >= 0 and mKpt_eval[i] > 0.
    mint:=0;
    maxt:=a;
    for i in [1..#final_H] do
        if final_H[i] gt 0 then
            mint:=Max(mint,Ceiling(-pt_eval[i] / final_H[i]));
            maxt:=Min(maxt,Ceiling(mKpt_eval[i] / final_H[i]) - 1);
            if mint gt maxt then return {L|}; end if;
        elif final_H[i] lt 0 then
            mint:=Max(mint,Floor(mKpt_eval[i] / final_H[i]) + 1);
            maxt:=Min(maxt,Floor(-pt_eval[i] / final_H[i]));
            if mint gt maxt then return {L|}; end if;
        elif pt_eval[i] lt 0 or mKpt_eval[i] le 0 then
            return {L|};
        end if;
    end for;
    return {L | Append(pt,c) : c in [mint..maxt]};
end function;

// Here NC is a cone (the nef cone) and mK is a lattice point (minus K). Returns
//      {Ambient(NC) | pt : pt in Points(semipositive_cone meet NC) |
//                                    IsInInterior(pt,semipositive_cone)}
// where semipositive_cone := -NC + mK
function first_Chern_classes_for_nef_ci(NC,mK)
    // We begin by computing a change of basis that will map NC into the
    // positive orthant. We do this by finding a dual basis contained in the
    // dual cone.
    ineqs:=SetToSequence(SequenceToSet(Inequalities(NC)));
    mK_heights:=[Integers() | h * mK : h in ineqs];
    ParallelSort(~mK_heights,~ineqs);
    d:=Dimension(Ambient(NC));
    bool,SC:=search_for_simplicial_subcone(ineqs,[Universe(ineqs)|],d);
    error if not bool, "Nef cone not of maximum dimension in ambient lattice.";
    B:=Matrix(LatticeBasisInCone(SC));
    ineqs:=RowSequence(Matrix(ineqs) * B^-1);
    ChangeUniverse(~ineqs,Ambient(Dual(NC)));
    mK:=mK * Transpose(B);
    // Calculate the pruned inequality matrix and the final inequality column.
    H:=Transpose(Matrix(ineqs));
    RemoveRow(~H,Nrows(H));
    final_H:=[Integers() | I.d : I in ineqs];
    // Extract the top-right corner of the bounding box of NC \cap (mK - NC)
    // (by construction the bottom-left corner is the origin) and prune the
    // final coefficient
    v:=Eltseq(mK);
    a:=v[d];
    Prune(~v);
    // Start scanning through the bounding box for points
    pt:=ZeroSequence(Integers(),d-1);
    if #pt ne 0 then
    	pts:={Dual(Parent(mK))|};
    	repeat
    	    pts join:= scan_along_ray(pt,a,H,final_H,mK);
    	    bool,pt:=next_point(pt,v,#pt);
    	until not bool;
    else
    	pts:=scan_along_ray(pt,a,H,final_H,mK);
    end if;
    // Finally undo the change of basis and return
    B:=Transpose(B^-1);
    return {Ambient(NC) | v * B : v in pts};
end function;

// Returns a sequence of tuples <K,ps> where K is the intersection data
// corresponding to the line bundles, and ps is the first number_of_terms + 1
// terms of the corresponding period sequence.
function complete_intersections_and_period_sequences(D,NC,MC,number_of_terms :
  dimensions:={Integers()|})
    // Compute the basis geometric data
    max_dim:=#D - Dimension(Universe(D));
    mK:=&+D;
    // Construct c_1 such that the corresponding complete intersection is Fano
    amples:=first_Chern_classes_for_nef_ci(NC,mK);
    // We should remove zero, too as the trivial line bundle defines an empty
    // hypersurface (or, more accurately, as at some point in the Quantum
    // Lefschetz process I need the map "multiply by the Euler class of the
    // line bundle" to  give an injection H^0 -> H^2, i.e. I need the Euler
    // class of the line bundle to be non-zero).
    Exclude(~amples,Zero(Universe(amples)));
    // Is there anything to do?
    results:={};
    if #amples eq 0 then return results; end if;
    // We need to sum over the part of the Mori cone which is non-negative on
    // each toric divisor (and of  degree at most number_of_terms on -KX -c1,
    // but we will impose that later)
    if number_of_terms gt 0 then
        degrees_to_sum:=ConeWithInequalities(D);
        if not degrees_to_sum subset MC then
            degrees_to_sum:=MC meet degrees_to_sum;
        end if;
    end if;
    // Now walk through the available c1s, computing complete intersections
    // with that c1 and calculating the quantum period for each complete
    // intersection.
    hilbert_basis:=HilbertBasis(NC : algorithm:="Hemmecke");
    for c1 in amples do
        // First decompose c1 as a sum of non-trivial nef line bundles. If the
        // optional argument dimensions is set then we know how many line
        // bundles to take.
        decompositions:=toric_knapsack_process([Eltseq(b) :
                                        b in hilbert_basis], Eltseq(c1));
        // This is all ways of writing c1 as a LC of Hilbert basis generators
        // so divide it up as a sum of first Chern classes of nef line bundles
        complete_intersections:={};
        for decomposition in decompositions do
            for k in [1..max_dim - 1] do
                // Make sure that, if dimension is set, we only use the correct
                // number of line bundles
                if (#dimensions eq 0) or (max_dim - k in dimensions) then
                    poss:=[non_negative_decompositions(u,k):u in decomposition];
                    for choice in CartesianProduct(poss) do
                        line_bundles:=Sort([&+[hilbert_basis[i] * choice[i][j] :
                                              i in [1..#poss]]  : j in [1..k]]);
                        // Exclude trivial summands, because in the quantum
                        // Lefschetz process we need the Euler class of each
                        // line-bundle to be non-zero (see comment above)
                        if not Zero(Universe(amples)) in line_bundles then
                            Include(~complete_intersections,line_bundles);
                        end if;
                    end for;
                end if;
            end for;
        end for;
        // Now iterate over all the complete intersections with this c1,
        // calculating the quantum period
        if number_of_terms gt 0 then
            for line_bundles in complete_intersections do
                ps:=period_sequence(D,NC,MC,degrees_to_sum,line_bundles,
                                                               number_of_terms);
                Include(~results,<[Eltseq(L) : L in line_bundles],ps>);
            end for;
        else
            one:=[Integers()|1];
            results join:= {<[Eltseq(L) : L in line_bundles],one> :
                                        line_bundles in complete_intersections};
        end if;
    end for;
    // Return the results
    return results;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic CompleteIntersectionsAndPeriodSequences( X::TorVar, n::RngIntElt :
    allow_weak_fano:=false, dimensions:={Integers()|} ) -> SetEnum
{Computes all complete intersections in the smooth toric Fano variety (or weighted projective space) X, cut out by direct sums of non-trivial nef line bundles. The data is returned as a sequence of tuples <K,ps> where K is the intersection data corresponding to the line bundles, and ps is the first n+1 terms of the corresponding period sequence. If the optional parameter 'dimensions' is equal to a set or sequence of integers, only period sequences for complete intersections of those dimensions will be calculated. Set 'allow_weak_fano' to true (default: false) to allow weak Fano ambients.}
    // Sanity checks
    bool,msg:=validate_toric_data(X : allow_weak_fano:=allow_weak_fano);
    require bool: msg;
    bool,dimensions:=validate_dimensions(Dimension(X),dimensions);
    require bool: dimensions;
    require n ge 0:
        "The number of terms to compute must be a non-negative integer.";
    // Calculate the basic geometric data
    D,NC,MC:=divisor_data_from_variety(X);
    // Return the result
    return complete_intersections_and_period_sequences(D,NC,MC,n :
                                                        dimensions:=dimensions);
end intrinsic;

intrinsic CompleteIntersectionsAndPeriodSequences(
    W::SeqEnum[SeqEnum[RngIntElt]], n::RngIntElt : ample:=false,
    define_fan:=false, allow_weak_fano:=false, dimensions:={Integers()|} )
    -> SetEnum
{Computes all complete intersections in the smooth toric Fano variety (or weighted projective space) X with spanning fan generated by the given weight data, cut out by direct sums of non-trivial nef line bundles. The data is returned as a sequence of tuples <K,ps> where K is the intersection data corresponding to the line bundles, and ps is the first n+1 terms of the corresponding period sequence. The optional parameter 'ample' can be used to specify the ample divisor to use. If the optional parameter 'define_fan' (default: false) is set to true then the verification that the data defines a smooth toric Fano variety is skipped. If the optional parameter 'dimensions' is equal to a set or sequence of integers, only period sequences for complete intersections of those dimensions will be calculated. Set 'allow_weak_fano' to true (default: false) to allow weak Fano ambients.}
    // Sanity checks
    bool,ample:=validate_weight_data(W : ample:=ample, define_fan:=define_fan,
                                              allow_weak_fano:=allow_weak_fano);
    require bool: ample;
    bool,dimensions:=validate_dimensions(#W[1] - #W,dimensions);
    require bool: dimensions;
    require n ge 0:
        "The number of terms to compute must be a non-negative integer.";
    // Calculate the basic geometric data
    D,NC,MC:=divisor_data_from_weights(W : ample:=ample);
    // Return the result
    return complete_intersections_and_period_sequences(D,NC,MC,n :
                                                        dimensions:=dimensions);
end intrinsic;

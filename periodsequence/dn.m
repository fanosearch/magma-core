freeze;

/////////////////////////////////////////////////////////////////////////
// dn.m
/////////////////////////////////////////////////////////////////////////
// Functions for working with Dn operators.
/////////////////////////////////////////////////////////////////////////

import "../utilities/laurent.m": seq_to_monomial;

/////////////////////////////////////////////////////////////////////////
// Database functions
/////////////////////////////////////////////////////////////////////////

// Attempts to read the next polynomial (in R) from the file F. Returns false
// followed by an error message if the file is corrupted. Returns true,false if
// the eof was encountered. Returns true,true,f if a polynomial was read.
function read_polynomial_from_db_file(F,R)
	// The data comes in pairs -- the coefficients, and the exponents
	S1:=Gets(F);
	if IsEof(S1) then
		return true,false,_;
	end if;
	S2:=Gets(F);
	if IsEof(S2) then
		return false,"Unexpected end of file whilst attempting to read exponents",_;
	end if;
	// Convert the first line to a sequence of rational coefficients
	try
		cs:=CommaSeparatedStringToRationalSequence(S1);
	catch e
		return false,"Unable to convert coefficients to a sequence of rationals",_;
	end try;
	// Convert the second line into a sequence of exponents
	try
		exs:=CommaSeparatedStringToIntegerSequence(S2);
	catch e
		return false,"Unable to convert exponents to a sequence of integers",_;
	end try;
	n:=Rank(R);
	if #exs ne n * #cs then
		return false,"Incompatible number of exponents";
	end if;
	ex:=[PowerSequence(Integers())|];
	for i in [1..#cs] do
		Append(~ex,exs[1 + (i - 1) * n..i * n]);
	end for;
	// Rebuild the polynomial
	return true,true,&+[R | cs[i] * seq_to_monomial(R,ex[i]) : i in [1..#cs]];
end function;

// Attempts to read the equations from the given database file. Returns true
// followed by the equations on success, false followed by an error message
// otherwise.
// The code to generate a database file for a sequence of equations is trivial:
/*
	fprintf path,"%o\n",Rank(Universe(eqns));
	for f in eqns do
	    cs,ex:=CoefficientsAndExponents(Numerator(f));
	    fprintf path,"%o\n%o\n",ToString(cs),ToString(&cat(ex));
	    cs,ex:=CoefficientsAndExponents(Denominator(f));
	    fprintf path,"%o\n%o\n",ToString(cs),ToString(&cat(ex));
	end for;
*/
function read_db_file(name)
	// First we create the path to the file
	try
        root:=FanosearchGetDbRoot();
    catch e
        return false,e`Object;
    end try;
	path:=Sprintf("%o/%o",root,name);
	// Try to open the file for reading
	try
		F:=Open(path,"r");
	catch e
		return false,"Unable to open database file " cat path;
	end try;
	// The first line of the file is the rank of the rational function field
	S:=Gets(F);
	if IsEof(S) then
		return false,"Unexpected end of file when attempting to read the rank of the function field";
	end if;
	bool,n:=IsStringInteger(S);
	if not bool then
		return false,"Unable to convert the rank to an integer";
	end if;
	if n lt 1 then
		return false,"The rank is must be a positive integer";
	end if;
	// Create the rational function field
	R:=RationalFunctionField(Rationals(),n);
	// Now we start building the equations
	eqns:=[R|];
	while true do
		// Read in the numerator
		bool,success,num:=read_polynomial_from_db_file(F,R);
		if not bool then return false,success; end if;
		if not success then return true,eqns; end if;
		// Read in the denominator
		bool,success,den:=read_polynomial_from_db_file(F,R);
		if not bool then
			return false,success;
		end if;
		if not success then
			return false,"Unexpected end of file whilst attempting to read the denominator";
		end if;
		if IsZero(den) then
			return false,"Zero denominator";
		end if;
		// Record the equation
		Append(~eqns,num / den);
	end while;
end function;

/////////////////////////////////////////////////////////////////////////
// Dn functions
/////////////////////////////////////////////////////////////////////////

// Given a polynomial f in K[x_1,...,x_k][y_1,...,y_n] and a polynomial ring
// RR = K[Y_1,...,Y_n][X_1,...,X_k], returns f as a polynomial in RR given
// by x_j \mapsto X_j, y_i \mapsto Y_i.
function swap_rings(f,RR)
	// Sanity check
	R:=Parent(f);
	K:=CoefficientRing(R);
	error if Rank(K) ne Rank(RR), "The rank of the coefficient ring of argument 1 must equal the rank of argument 2";
	KK:=CoefficientRing(RR);
	error if Rank(R) ne Rank(KK), "The rank of the parent of argument 1 must equal the rank of the coefficient ring of argument 2";
	// Build the map sending the K to RR
	phi:=hom<K -> RR | [RR.i : i in [1..Rank(RR)]]>;
	// Extract the coefficients and exponents of f
	cs,ex:=CoefficientsAndExponents(f);
	// Rebuild f in RR
	return &+[RR | seq_to_monomial(KK,ex[i]) * phi(cs[i]) : i in [1..#cs]];
end function;

// Given a linear polynomial f = a x_i + b in K[x_1,...,x_n], for some i,
// returns -b / a in K, and the corresponding value of i.
function solve_linear(f)
	// Extract the coefficients and exponents
	cs,ex:=CoefficientsAndExponents(f);
	error if #cs eq 0 or #cs gt 2, "Argument 1 is not linear in one variable.";
	// Recover the index of the constant term b
	zero_idx:={Integers() | i : i in [1..#ex] | IsZero(ex[i])};
	if #zero_idx eq 0 then
		error if #cs ne 1, "Argument 1 is not linear in one variable.";
		b_idx:=0;
	else
		error if #cs ne 2, "Argument 1 is not linear in one variable.";
		b_idx:=Representative(zero_idx);
	end if;
	// Recover the index of coefficient a and the value of i
	a_idx:=Representative({1..#ex} diff zero_idx);
	i:=Index(RowSequence(IdentityMatrix(Integers(),Rank(Parent(f)))),ex[a_idx]);
	error if i eq 0, "Argument 1 is not linear in one variable.";
	// Return the solution
	if b_idx eq 0 then
		return Universe(cs) ! 0,i;
	end if;
	return -cs[b_idx] / cs[a_idx],i;
end function;

// Given a polynomial f = a x_i + g(x_1,...,x_{i-1},x_{i+1},...,x_n) in
// K[x_1,...,x_n], returns -g / a.
function solve_linear_for_i(f,i)
	// Extract the coefficients and exponents
	cs,ex:=CoefficientsAndExponents(f);
	// Find the term solving for x_i
	idx:=0;
	for j in [1..#ex] do
		if ex[j][i] ne 0 then
			error if idx ne 0 or ex[j][i] ne 1,
				"Argument 1 is not linear in x_i.";
			idx:=j;
		end if;
	end for;
	// Return the solution
	R:=Parent(f);
	return -(f - cs[idx] * R.i) / cs[idx];
end function;

// Returns the equations for the coefficients of the given operator L.
function equations_for_coeffs(L)
	// Note the coefficient ring
	K:=CoefficientRing(Parent(L));
	// Extract the equations satisfied by the first few terms of the period
	// sequence
	R1:=PolynomialRing(K,Rank(K) + 1);
	cs:=ApplyPicardFuchsOperator(L,[R1.i : i in [1..Rank(R1)]]);
	// We rebuild these equations with the coefficient rings and monomials
	// exchanged
	K2:=RationalFunctionField(Rationals(),Rank(R1));
	R2:=PolynomialRing(K2,Rank(K),"glex");
	CS:=[swap_rings(c,R2) : c in cs];
	// The Groebner basis for the corresponding ideal allows us to recover
	// equations for the coefficients in terms of the coeffs of the period
	// sequence.
	eqns:=ZeroSequence(K2,Rank(R2));
	for f in GroebnerBasis(ideal<R2 | CS>) do
		g,i:=solve_linear(f);
		eqns[i]:=g;
	end for;
	return eqns;
end function;

// Attempts to evaluate f(ps). If this is 0/0, tries to apply L'Hopital's rule
// in each coordinate direction. Returns true followed by the value on success,
// false followed by an error message otherwise.
function can_evaluate(f,ps)
	ps:=ps[1..Rank(Parent(f))];
	den:=Denominator(f);
	if not IsZero(Evaluate(den,ps)) then
		return true,Evaluate(f,ps);
	end if;
	num:=Numerator(f);
	if IsZero(Evaluate(num,ps)) then
		for i in [1..Rank(Parent(f))] do
			d:=Evaluate(Derivative(den,i),ps);
			n:=Evaluate(Derivative(num,i),ps);
			if not IsZero(d) then
				return true,n / d;
			elif IsZero(n) then
				d:=Derivative(den,i);
				if not IsZero(d) then
					bool,val:=$$(Derivative(num,i) / d,ps);
					if bool then
						return true,val;
					end if;
				end if;
			end if;
		end for;
	end if;
	return false,"Division by zero";
end function;

// Calculates the first k+1 terms of the period sequence for the given Dn
// Picard-Fuchs operator L. The coefficients of L can be expressed in terms of
// the first few coefficients of the period sequence via "eqns".
function period_sequence(k,L,eqns)
	// If k is small enough this is trivial
	if k lt Rank(Universe(eqns)) then
		K:=RationalFunctionField(Rationals(),k + 1);
		return [K | K.i : i in [1..Rank(K)]];
	end if;
	// Calculate the equations for the next k + 1 terms of the period sequence
	K:=CoefficientRing(Parent(L));
	R1:=PolynomialRing(K,k + 1);
	cs:=ApplyPicardFuchsOperator(L,[R1.i : i in [1..Rank(R1)]]);
	fs:=[solve_linear_for_i(cs[i],i) : i in [Rank(K) + 2..Rank(R1)]];
	// Build the polynomial rings we need
	K2:=RationalFunctionField(Rationals(),Rank(R1));
	R2:=PolynomialRing(K2,#eqns);
	// Map each f into R2 and substitute in the equations found above to give us
	// the equations in K2
	phi:=hom<Universe(eqns) -> K2 | [K2.i : i in [1..Rank(Universe(eqns))]]>;
	eqns:=[phi(e) : e in eqns];
	ps:=[K2 | K2.i : i in [1..Rank(K) + 1]] cat
		[K2 | Evaluate(swap_rings(f,R2),eqns) : f in fs];
	// This is in one too-many variables still, so reduce the rank by one
	K3:=RationalFunctionField(Rationals(),Rank(K2) - 1);
	phi:=hom<K2 -> K3 | Append([K3.i : i in [1..Rank(K3)]],0)>;
	return [K3 | phi(c) : c in ps];
end function;

// Restricts the period sequence ps to the first k+1 coefficients. This assumes
// that #ps >= k+1, and that k is sufficiently large so that the restricted
// period sequence isn't completely free.
function restrict_period_sequence(k,ps)
	K:=RationalFunctionField(Rationals(),k);
	rk:=Rank(Universe(ps));
	if k eq rk then
		return ChangeUniverse(ps,K);
	else
		phi:=hom<Universe(ps) -> K | [K.i : i in [1..k]] cat
									 ZeroSequence(K,rk - k)>;
		return [K | phi(ps[i]) : i in [1..k + 1]];
	end if;
end function;

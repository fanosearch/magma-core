freeze;

/////////////////////////////////////////////////////////////////////////
// weyl.m
/////////////////////////////////////////////////////////////////////////
// The Weyl algebra W<D,t> implemented as a quotient of the free algebra
// in two variables by the idea <tD + t - Dt>.
/////////////////////////////////////////////////////////////////////////
// Note that, in the intrinsics and functions below which deal with
// Groebner bases in the Weyl algebra, we use the term order where
// t^a D^b is ordered by [b,a] in lexicographic order
// cf. p5 of [SST] := Saito--Sturmfels--Takayama, "Gr\"obner Deformations
// of Hypergeometric Differential Equations"
// although note that they have d = d/dt whereas we have D = t d/dt
/////////////////////////////////////////////////////////////////////////

// The local cache in which we store the Weyl algebra
weyl_cache:=NewStore();

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Returns the common polynomial ring \QQ[t,D].
function polynomial_ring_2()
    bool,R:=StoreIsDefined(weyl_cache,"polyring");
    if not bool then
        R:=PolynomialRing(Rationals(),2);
        StoreSet(weyl_cache,"polyring",R);
    end if;
    return R;
end function;


// returns the leading term of an element of the Weyl algebra, where
// t^a D^b < t^c D^d iff b<d or b=d and a<c
// if the leading term is c t^a D^b, returns c, a, b
function leading_coefficient_and_exponents(L)
    cs, es := CoefficientsAndExponents(L);
    error if #cs eq 0, "Zero doesn't have a well-defined leading term";
    max_ex, idx := Maximum([ex : ex in es]);
    return cs[idx], max_ex[2], max_ex[1];
end function;

// computes the S-pair of two elements of the Weyl algebra [SST] p7
function s_pair(f,g)
    cf, alpha, beta := leading_coefficient_and_exponents(f);
    cg, a, b := leading_coefficient_and_exponents(g);
    alpha_prime := Max(alpha,a)-alpha;
    beta_prime := Max(beta,b)-beta;
    a_prime := Max(alpha,a)-a;
    b_prime := Max(beta,b)-b;
    t := Parent(f).2;
    D := Parent(f).1;
    return t^alpha_prime*D^beta_prime*f - (cf/cg)*t^a_prime*D^b_prime*g;
end function;

// computes the normal form of an element of the Weyl algebra [SST] p7
function normal_form(f,gs)
    // get the trivial case out of the way
    if f eq 0 then
	return f;
    end if;
    // otherwise, follow [SST] p7
    r := f;
    cr,leading_ex_for_r_t,leading_ex_for_r_D := leading_coefficient_and_exponents(r);
    for g in gs do
	_,leading_ex_for_g_t,leading_ex_for_g_D := leading_coefficient_and_exponents(g);
	while leading_ex_for_g_t le leading_ex_for_r_t and
	      leading_ex_for_g_D le leading_ex_for_r_D do
	    r := s_pair(r,g);
	    if r eq 0 then
		return r;
	    end if;
	    cr,leading_ex_for_r_t,leading_ex_for_r_D := leading_coefficient_and_exponents(r);
	end while;
    end for;
    initial_term_r := cr*(Parent(f).2)^leading_ex_for_r_t*(Parent(f).1)^leading_ex_for_r_D;
    r := initial_term_r + normal_form(r - initial_term_r,gs);
    return r;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic InternalIsWeylAlgebra( W::AlgFP ) -> BoolElt,Map,Map
{True iff W is the Weyl algebra W<D,t> (or W<t,D>). If true, also returns the map W -> K[t,D] and the map K[t,D] -> W.}
	// Verify the rank
	if Rank(W) ne 2 then
		return false,_,_;
	end if;
	// Perhaps this is our cached copy of the Weyl algebra
	WW,phi,psi:=WeylAlgebra();
	if W cmpeq WW then
		return true,phi,psi;
	end if;
	// We need to look more carefully
	B:=Basis(DivisorIdeal(W));
	if #B ne 1 then
		return false,_,_;
	end if;
	// Is the quotient relation correct?
	I:=Universe(B);
	if B[1] eq I.1 * I.2 - I.2 * I.1 - I.2 then
		// This looks like W<D,t>
		RR:=PolynomialRing(CoefficientRing(W),2);
		phi:=hom<W -> RR | [RR.2,RR.1]>;
		psi:=hom<RR -> W | [W.2,W.1]>;
		return true,phi,psi;
	elif B[1] eq I.1 * I.2 - I.2 * I.1 + I.1 then
		// This looks like W<t,D>
		RR:=PolynomialRing(CoefficientRing(W),2);
		phi:=hom<W -> RR | [RR.1,RR.2]>;
		psi:=hom<RR -> W | [W.1,W.2]>;
		return true,phi,psi;
	end if;
	// If we're here then it wasn't W<D,t> or W<t,D>
	return false,_,_;
end intrinsic;

intrinsic WeylAlgebra( K::Rng ) -> AlgFP,Map,Map
{The Weyl algebra W<D,t>, over the field K, in two variables such that Dt = tD + t. Also gives the map W<D,t> -> K[t,D] and the map K[t,D] -> W<D,t> (note the necessary change of variable ordering).}
	// Sanity check
	require IsField(K): "Argument 1 is not a field";
	// If we've been passed the rationals we make use of the cache
	if K cmpeq Rationals() then
		return WeylAlgebra();
	end if;
	// Create the Weyl algebra W<D,t>
	R:=FreeAlgebra(K,2);
	W:=R / ideal< R | R.2 * R.1 + R.2 - R.1 * R.2>;
	// Create the maps between W<D,t> and the polynomial ring K[t,D]
    RR:=PolynomialRing(K,2);
    phi:=hom<W -> RR | [RR.2,RR.1]>;
    psi:=hom<RR -> W | [W.2,W.1]>;
	// Return the Weyl algebra and the maps
	return W,phi,psi;
end intrinsic;

intrinsic WeylAlgebra() -> AlgFP,Map,Map
{The Weyl algebra W<D,t>, over the rationals, in two variables such that Dt = tD + t. Also gives the map W<D,t> -> \QQ[t,D] and the map \QQ[t,D] -> W<D,t> (note the necessary change of variable ordering).}
    bool,W:=StoreIsDefined(weyl_cache,"weyl");
    if not bool then
        // Create the Weyl algebra W<D,t>
        R:=FreeAlgebra(Rationals(),2);
        W:=R / ideal< R | R.2 * R.1 + R.2 - R.1 * R.2>;
        // Create the maps between W<D,t> and the polynomial ring RR[t,D]
        RR:=polynomial_ring_2();
        phi:=hom<W -> RR | [RR.2,RR.1]>;
        psi:=hom<RR -> W | [W.2,W.1]>;
        // Cache the data
        StoreSet(weyl_cache,"weyl",W);
        StoreSet(weyl_cache,"weyl_to_polyring",phi);
        StoreSet(weyl_cache,"polyring_to_weyl",psi);
    else
        // Extract the remaining data from the cache
        phi:=StoreGet(weyl_cache,"weyl_to_polyring");
        psi:=StoreGet(weyl_cache,"polyring_to_weyl");
    end if;
    // Return the Weyl algebra and the maps
    return W,phi,psi;
end intrinsic;

intrinsic Symbol( f::AlgFPElt ) -> RngUPolElt
{The symbol of the differential operator f, where f an element of the Weyl algebra W<D,t>. The symbol will be given as a polynomial in K[t], where K is the coefficient ring of W.}
    // Sanity check
    bool,phi:=InternalIsWeylAlgebra(Parent(f));
    require bool: "Argument must be an element of the Weyl algebra W<D,t>";
    // Extract the symbol and map it into \QQ[t]
    ff:=phi(f);
    symbol:=Coefficient(ff,2,Degree(ff,2));
    R:=PolynomialRing(CoefficientRing(Codomain(phi)));
    pi:=hom<Parent(symbol) -> R | [R.1,0]>;
    return pi(symbol);
end intrinsic;

intrinsic GroebnerBasisInWeylAlgebra( fs::SeqEnum[AlgFPElt] ) -> SeqEnum[AlgFPElt]
{ Given a sequence [f1,...,fk] of elements of the Weyl algebra W, returns [g1,...,gl] where g1,...,gl is a reduced Groebner basis for the left ideal of W generated by [f1,...,fk], with respect to the term order t^a D^b < t^c D^d iff b<d or b=d and a<c. }
    // Sanity check
    bool:=InternalIsWeylAlgebra(Universe(fs));
    require bool: "Argument must be a sequence of elements of the Weyl algebra W<D,t>";
    // run the algorithm from [SST] p9.
    // we construct a Groebner basis G
    pairs := [ {fs[i],fs[j]} : i in [1..#fs], j in [1..#fs] | i lt j and fs[i] ne fs[j]];
    G := fs;
    while #pairs gt 0 do
	// prune the list of S-pairs using the Chain Criterion.  See:
	// http://www.math.rwth-aachen.de/~levandov/filez/dmod0708/nc+gb.pdf
	// slide 20
	for this_pair in pairs do
	    fi, fj := Explode(SetToSequence(this_pair));
	    _,deg_t_i,deg_D_i := leading_coefficient_and_exponents(fi);
	    _,deg_t_j,deg_D_j := leading_coefficient_and_exponents(fj);
	    for other_pair in pairs do
		if fj in other_pair and not fi in other_pair then
		    fk := Representative(other_pair diff {fj});
		    _,deg_t_k,deg_D_k := leading_coefficient_and_exponents(fk);
		    if deg_t_j le Max(deg_t_i,deg_t_k) and
		       deg_D_j le Max(deg_D_i,deg_D_k) then
			// the Chain Criterion says that we can delete {fi,fk}
			idx := Index(pairs,{fi,fk});
			if idx ne 0 then
			    Remove(~pairs,idx);
			end if;
		    end if;
		end if;
	    end for;
	end for;
	if #pairs eq 0 then break; end if;
	f,f_prime := Explode(SetToSequence(pairs[#pairs]));
	Remove(~pairs,#pairs);
	h := s_pair(f,f_prime);
	r := normal_form(h,G);
	if r ne 0 then
	    pairs cat:= [{g,r} : g in G | g ne r];
	    Append(~G,r);
	end if;
    end while;
    // at this point G is a Groebner basis but not a reduced Groebner basis.
    // we turn it into a reduced Groebner basis as follows (cf. [CLS] :=
    // Cox, Little, O'Shea, "Ideals, Varieties, and Algorithms:..." pp.91--92)
    // first note that if LT(g_1),...,LT(g_n) generate in(I) then g_1,...,g_n in
    // fact generate I.
    // second deduce that if [g_1,...,g_n] is a Groebner basis and in(g_k) is
    // divisible by any of in(g_j), j ne k, then [g_1,...,g_n] with g_k omitted
    // is also a Groebner basis
    // third use the algorithm in the proof of [CLS] Proposition 6 on page 92.
    delete_me := [false : k in [1..#G]];
    for k1 in [1..#G] do
	if delete_me[k1] then
	    continue;
	end if;
	g1 := G[k1];
	for k2 in [k : k in [1..#G] | delete_me[k] eq false and k ne k1] do
	    g2 := G[k2];
	    _,g1_t,g1_D := leading_coefficient_and_exponents(g1);
	    _,g2_t,g2_D := leading_coefficient_and_exponents(g2);
	    if g1_t le g2_t and g1_D le g2_D then
		delete_me[k2] := true;
	    end if;
	end for;
    end for;
    minimal_G := [G[k] : k in [1..#G] | delete_me[k] eq false];
    // now rescale so that leading coefficients equal 1
    for k in [1..#minimal_G] do
	lc,_,_ := leading_coefficient_and_exponents(minimal_G[k]);
	minimal_G[k] *:= 1/lc;
    end for;
    // now use the algorithm in the proof of [CLS] Proposition 6
    for k in [1..#minimal_G] do
	minimal_G[k] := normal_form(minimal_G[k],Remove(minimal_G,k));
    end for;
    return minimal_G;
end intrinsic;



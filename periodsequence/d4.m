freeze;

/////////////////////////////////////////////////////////////////////////
// d4.m
/////////////////////////////////////////////////////////////////////////
// Intrinsics for working with D4 operators. The general shape of a D4
// operator is:
// L = D^4 +
//     t(a_4(D + 1/2)^4 + a_2(D + 1/2)^2 + a_0) +
//     t^2(D + 1)(b_3(D + 1)^3 + b_1(D + 1)) +
//     t^3(D + 1)(D + 2)(c_2(D + 3/2)^2 + c_0) +
//     t^4(D + 1)(D + 2)(D + 3)d_1(D + 2) +
//     t^5(D + 1)(D + 2)(D + 3)(D + 4)e_0
/////////////////////////////////////////////////////////////////////////

import "../core.m": fetch_data, save_data;
import "dn.m": can_evaluate, read_db_file, equations_for_coeffs,
	 	       period_sequence, restrict_period_sequence;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Attempts to build the D4 coefficients a0,a2,etc. given by evaluating
// eqns[1](ps),eqns[2](ps),etc. Returns true followed by the coefficients on
// success, false followed by an error message otherwise.
function can_recover_D4_coeffs(eqns,ps)
	cs:=[];
	for g in eqns do
		bool,val:=can_evaluate(g,ps);
		if not bool then
			return false,val;
		end if;
		Append(~cs,val);
	end for;
	return true,cs;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic D4Operator( cs::SeqEnum[RngElt] ) -> AlgFPElt
{The D4 operator in the Weyl algebra W<D,t> with coefficients [a0, a2, a4, b1, b3, c0, c2, d1, e0] given by the sequence cs.}
	// Sanity check
	require not IsNull(cs): "Illegal null sequence";
	require #cs eq 9: "A D4 operator requires exactly 9 coefficients";
	// Build and return the D4 operator
	W:=WeylAlgebra(FieldOfFractions(Universe(cs)));
	D:=W.1;
	t:=W.2;
	L:=D^4 +
	   t * (D + 1/2)^4 * Numerator(cs[3]) / Denominator(cs[3]) +
	   t * (D + 1/2)^2 * Numerator(cs[2]) / Denominator(cs[2]) +
	   t * Numerator(cs[1]) / Denominator(cs[1]) +
	   t^2 * (D + 1) * (D + 1)^3 * Numerator(cs[5]) / Denominator(cs[5]) +
	   t^2 * (D + 1) * (D + 1) * Numerator(cs[4]) / Denominator(cs[4]) +
	   t^3 * (D + 1) * (D + 2) * (D + 3/2)^2 *
	   		Numerator(cs[7]) / Denominator(cs[7]) +
	   t^3 * (D + 1) * (D + 2) * Numerator(cs[6]) / Denominator(cs[6]) +
	   t^4 * (D + 1) * (D + 2) * (D + 3) * (D + 2) *
	   		Numerator(cs[8]) / Denominator(cs[8]) +
	   t^5 * (D + 1) * (D + 2) * (D + 3) * (D + 4) *
	   		Numerator(cs[9]) / Denominator(cs[9]);
	return L;
end intrinsic;

intrinsic D4Coefficients() -> SeqEnum[FldFunRatMElt]
{}
	// Perhaps the equations are in the cache?
	bool,eqns:=fetch_data("D4_coefficients");
	if bool then return eqns; end if;
	// Perhaps the equations are stored in a database file?
	bool,eqns:=read_db_file("d4.txt");
	if bool then
		save_data("D4_coefficients",eqns);
		return eqns;
	end if;
	// First we build the D4 operator
	K:=RationalFunctionField(Rationals(),9);
	L:=D4Operator([K.i : i in [1..Rank(K)]]);
	// Calculate the equations for the coefficients
	eqns:=equations_for_coeffs(L);
	// Cache the equations and return
	save_data("D4_coefficients",eqns);
	return eqns;
end intrinsic;

intrinsic D4Coefficients( ps::SeqEnum[RngElt] ) -> SeqEnum[RngElt]
{A sequence [a0, a2, a4, b1, b3, c0, c2, d1, e0] defining the coefficients of a D4 operator in terms of the first 10 coefficients of the period sequence.}
	// Sanity check
	require not IsNull(ps): "Illegal null sequence";
	// Fetch the equations for the coefficients
	eqns:=D4Coefficients();
	n:=Rank(Universe(eqns));
	require #ps ge n:
		Sprintf("The first %o terms of the period sequence are required.",n);
	// Evaluate the equations and return
	bool,cs:=can_recover_D4_coeffs(eqns,ps);
	require bool: cs;
	return cs;
end intrinsic;

intrinsic D4PeriodSequence( k::RngIntElt ) -> SeqEnum[FldFunRatMElt]
{The first k+1 terms of the period sequence associated with a general D4 operator. The first 10 period coefficients are free, after which the (n+1)-th period coefficient is given as a rational function involving the previous n period coefficients.}
	// Sanity check
	require k gt 0: "The number of terms to compute must be a positive integer";
	// If k is small enough this is trivial
	if k le 9 then
		K:=RationalFunctionField(Rationals(),k + 1);
		return [K | K.i : i in [1..Rank(K)]];
	end if;
	// Perhaps we have enough terms in the cache?
	bool,ps:=fetch_data("D4_period_sequence");
	if bool and #ps gt k then
		return restrict_period_sequence(k,ps);
	end if;
	// Build a general D4 operator and equations for the general coefficients
	K:=RationalFunctionField(Rationals(),9);
	L:=D4Operator([K.i : i in [1..Rank(K)]]);
	eqns:=D4Coefficients();
	// Calculate the period sequence
	ps:=period_sequence(k,L,eqns);
	// Update the cache and return
	save_data("D4_period_sequence",ps);
	return ps;
end intrinsic;

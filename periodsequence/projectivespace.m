freeze;

/////////////////////////////////////////////////////////////////////////
// projectivespace.m
/////////////////////////////////////////////////////////////////////////
// Compute the period sequence for products of prohective space.
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Returns the unregularised form of the given regularised period sequence.
function unreg(ps)
    return [Rationals() | ps[i] / Factorial(i-1) : i in [1..#ps]];
end function;

// Returns the regularised form of the given unregularised period sequence.
function reg(ps)
    ps:=[Rationals() | ps[i] * Factorial(i-1) : i in [1..#ps]];
    ok,pps:=CanChangeUniverse(ps,Integers());
    return ok select pps else ps;
end function;

/////////////////////////////////////////////////////////////////////////
// Instrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic PeriodSequenceForProjectiveSpace(n::RngIntElt,k::RngIntElt) -> SeqEnum
{The first k+1 terms of the period sequence for P^n.}
    // Sanity check
    require n ge 1: "The dimension must be a positive integer";
    require k gt 0: "The number of terms to compute must be a positive integer";
    // Handle the trivial case
    if k eq 1 then
        return [Integers() | 1,0];
    end if;
    // Build the Picard--Fuchs operator for P^n
    W:=WeylAlgebra();
    D:=W.1;
    t:=W.2;
    L:=D^n - (n+1)^(n+1)*t^(n+1)*&*[W | D + i : i in [1..n]];
    // Calculate the period sequence
    return PeriodSequenceForPicardFuchsOperator(L,k,[Integers() | 1,0]);
end intrinsic;

intrinsic PeriodSequenceForProductProjectiveSpace(N::SeqEnum[RngIntElt],k::RngIntElt) -> SeqEnum
{The first k+1 terms of the period sequence for the product of #N projectives spaces whose dimensions are N.}
    // Sanity check
    require #N ne 0: "Argument 1 must not be empty";
    require &and[n ge 1 : n in N]: "The dimensions must be positive integers";
    require k gt 0: "The number of terms to compute must be a positive integer";
    // Handle the trivial case
    if k eq 1 then
        return [Integers() | 1,0];
    end if;
    // We work in a power series ring
    R:=PowerSeriesRing(Rationals(),k+1);
    // Compute the unregularised period sequence
    ps:=R ! 1;
    for n in SequenceToSet(N) do
        num:=#[1 : i in N | i eq n];
        ps *:= (R ! unreg(PeriodSequenceForProjectiveSpace(n,k)))^num;
    end for;
    ps:=Coefficients(ps);
    ps cat:= ZeroSequence(Rationals(),k + 1 - #ps);
    // Regularise it and return it
    return reg(ps);
end intrinsic;

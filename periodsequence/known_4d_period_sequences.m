freeze;

/////////////////////////////////////////////////////////////////////////
// known_4d_period_sequences.m
/////////////////////////////////////////////////////////////////////////
// Lookup tables for period sequences of known 4-dim. Fano manifolds
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Return the directory that contains the period sequence key-value
// databases.  It raises a runtime error if the environment variable
// FSROOT is not set
function get_db_dir()
    fs_root:=GetEnv("FSROOT");
    error if #fs_root eq 0, "The environment variable FSROOT must be set.";
    return fs_root cat "/db/known_periods";
end function;

// Read the key-value database Fano_N
function read_Fano_db(N)
    // W is the Weyl algebra that contains the Picard--Fuchs operators
    W<D,t> := WeylAlgebra();
    // R is the ring that holds the Laurent polynomial mirrors
    case N:
    when 1:
	R<x> := RationalFunctionField(Rationals(),1);
    when 2:
	R<x,y> := RationalFunctionField(Rationals(),2);
    when 3:
	R<x,y,z> := RationalFunctionField(Rationals(),3);
    when 4:
	R<x,y,z,w> := RationalFunctionField(Rationals(),4);
    end case;
    // Build the conversion functions
    keys := {"period", "names", "PF", "proven", "proof", "f"};
    conversion_funcs := AssociativeArray();
    for k in keys do
	conversion_funcs[k] := func<xx|eval(xx)>;
    end for;
    // Read the database and return
    input_file := Sprintf("%o/Fano_%o.txt", get_db_dir(), N);
    return [xx : xx in KeyValueFileProcess(input_file, keys : skip_unknown:=false, require_all_keys:=false, conversion_funcs:=conversion_funcs)];
end function;

// Return a boolean indicating success or otherwise, followed by k+1 terms of the period sequence for M.  This will fail if k is too large and the Picard--Fuchs operator for M is not known.
function build_period(M, k)
    ps := M["period"];
    // do we know enough terms of the period sequence?
    if k + 1 le #ps then
	return true, ps[1..k+1];
    end if;
    // do we know the Picard--Fuchs operator?
    ok, PF := IsDefined(M,"PF");
    if not ok then
	return false, _;
    end if;
    return PeriodSequenceForPicardFuchsOperator(PF, k, ps);
end function;

////////////////////////////////////////////////////////////////////////////////
// intrinsics
////////////////////////////////////////////////////////////////////////////////


intrinsic Known4DPeriodSequences(:number_of_terms := 15) -> Assoc, Assoc
{
Two lookup tables lookup_F, lookup_R.  The first return value, lookup_F, is an associative array with keys given by the names (as strings) of known 4-dimensional Fano varieties and values given by the first number_of_terms+1 terms of their regularized quantum period sequence.  The second return value, lookup_R, contains the same data but the keys are the period sequences and the values are the names.
}
    // read in the data
    Fano4 := read_Fano_db(4);
    // build the lookup tables
    results := AssociativeArray();
    reverse := AssociativeArray();
    for M in Fano4 do
	names := M["names"];
	ok, ps := build_period(M, number_of_terms);
	if not ok then
	    msg := Sprintf("number_of_terms must be at most %v, as we do not know the Picard--Fuchs operator for the Fano manifold with names %o and we only know %o terms of the period sequence", #M["period"], names, #M["period"]);
	    error msg;
	end if;
	for n in names do
	    results[n] := ps;
	end for;
	reverse[ps] := names;
    end for;
    return results, reverse;
end intrinsic;

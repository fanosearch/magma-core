freeze;

/////////////////////////////////////////////////////////////////////////
// period_sequence_db.m
/////////////////////////////////////////////////////////////////////////

import "../../core.m": save_data, fetch_data;

/////////////////////////////////////////////////////////////////////////
// Constants
/////////////////////////////////////////////////////////////////////////

// The database name
DB_NAME:="knownperiods.db";

// The number of terms stored in the database.
// Note: If you change this value you *MUST* delete and remake your database.
NUM_TERMS:=21;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Returns true followed by the path to the period sequence database, false
// followed by an error message otherwise.
function db_path()
    try
        path:=FanosearchGetDbRoot() cat "/" cat DB_NAME;
    catch e
        return false,"Unable to deduce the path to the Fanosearch root directory";
    end try;
    return true,path;
end function;

// Create the database table (if it doesn't exist).
procedure db_make_table(db)
    // Is there anything to do?
    bool,table_exists:=fetch_data("psdb_table_" cat DB_NAME);
    if bool and table_exists then return; end if;
    // Create the period sequence table
    if not SqliteTableExists(db,"period_sequences") then
        SqliteExecute(db,"CREATE TABLE period_sequences (c2 INTEGER NOT NULL, c3 INTEGER NOT NULL, c4 INTEGER NOT NULL, c5 INTEGER NOT NULL, ps TEXT NOT NULL, hash INTEGER NOT NULL, dim INTEGER NOT NULL, name TEXT NOT NULL)");
        SqliteExecute(db,"CREATE INDEX c2_idx ON period_sequences(c2)");
        SqliteExecute(db,"CREATE INDEX c3_idx ON period_sequences(c3)");
        SqliteExecute(db,"CREATE INDEX c4_idx ON period_sequences(c4)");
        SqliteExecute(db,"CREATE INDEX c5_idx ON period_sequences(c5)");
        SqliteExecute(db,"CREATE INDEX hash_idx ON period_sequences(hash)");
        SqliteExecute(db,"CREATE INDEX dim_idx ON period_sequences(dim)");
    end if;
    // Update the cache
    save_data("psdb_table_" cat DB_NAME,true);
end procedure;

// Returns true followed by connection to the database, false followed by an
// error message otherwise.
function db_connect()
    bool,path:=db_path();
    if not bool then return false,path; end if;
    db:=SqliteConnect(path);
    try
        db_make_table(db);
    catch e
        return false,e`Object;
    end try;
    return true,db;
end function;

// Escapes the string.
function escape_string(S)
    to_escape:={"\\","\"","'","*"};
    T:=[];
    for i in [1..#S] do
        if S[i] in to_escape then
            Append(~T,"\\");
        end if;
        Append(~T,S[i]);
    end for;
    return &cat T;
end function;

// Unescapes the string.
function unescape_string(S)
    T:=[];
    in_escape:=false;
    for i in [1..#S] do
        if not in_escape and S[i] eq "\\" then
            in_escape:=true;
        else
            in_escape:=false;
            Append(~T,S[i]);
        end if;
    end for;
    return &cat T;
end function;

// ps_to_string returns a string representation of the given period sequence.
function ps_to_string(S)
    strS:=[IntegerToString(c) : c in S];
    return Join(strS,",");
end function;

// ps_hash returns a hash value for the given period sequence.
function ps_hash(S)
    hash:=1;
    for c in S do
        k:=(7 * c) mod 101;
        hash:=(((3^k) mod 65521) * (hash + 1)) mod 65521;
    end for;
    return hash;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic PSDBAdd( d::RngIntElt, S::SeqEnum[RngIntElt], name::MonStgElt )
{Adds the period sequence S of dimension d to the period sequence database using the given name. If there already exists a period sequence of dimension d with this name, then it will be replaced.}
    // Sanity check
    require d ge 1: "Argument 1 must be a positive integer";
    require #S ge NUM_TERMS:
        Sprintf("Argument 2 must be of length at least %o",NUM_TERMS);
    require #Trim(name) ne 0: "Argument 3 must be non-empty";
    // Escape the name
    name:=escape_string(name);
    // Truncate the period sequence, build the string representation, and
    // calculate the hash value
    S:=S[1..NUM_TERMS];
    strS:=ps_to_string(S);
    hash:=ps_hash(S);
    // Establish a connection to the database
    bool,db:=db_connect();
    require bool: db;
    // Is there anything to do?
    sql:=Sprintf("SELECT COUNT(*) FROM period_sequences WHERE hash = %o AND dim = %o AND name = \"%o\" AND c2 = %o AND c3 = %o AND c4 = %o AND c5 = %o AND ps = \"%o\"",hash,d,name,S[3],S[4],S[5],S[6],strS);
    res:=SqliteQuery(db,sql);
    num:=StringToInteger(res[1][1][2]);
    if num ne 0 then return; end if;
    // Delete any existing copy of this name
    sql:=Sprintf("DELETE FROM period_sequences WHERE dim = %o AND name = \"%o\"",d,name);
    SqliteExecute(db,sql);
    // Add this period sequence
    sql:=Sprintf("INSERT INTO period_sequences (c2,c3,c4,c5,ps,hash,dim,name) VALUES (%o,%o,%o,%o,\"%o\",%o,%o,\"%o\")",S[3],S[4],S[5],S[6],strS,hash,d,name);
    SqliteExecute(db,sql);
end intrinsic;

intrinsic PSDBAdd( f::FldFunRatMElt, name::MonStgElt : algorithm:="auto",
    interpolate:=false )
{Adds the period sequence for f to the period sequence database using the given name. If there already exists a period sequence of dimension d with this name, then it will be replaced.}
    // Sanity check
    require IsLaurent(f): "Argument 1 must be a Laurent polynomial";
    require #Trim(name) ne 0: "Argument 2 must be non-empty";
    P:=NewtonPolytope(f);
    require IsMaximumDimensional(P):
        "The Newton polytope of argument 1 must be maximum dimensional";
    require ContainsZero(P):
        "The Newton polytope of argument 1 must contain the origin in its (strict) interior";
    // Note the dimension
    d:=Rank(Parent(f));
    // Calculate the period sequence
    S:=PeriodSequence(f,NUM_TERMS - 1 :
                 algorithm:=algorithm, interpolate:=interpolate);
    // Add the period sequence
    PSDBAdd(d,S,name);
end intrinsic;

intrinsic PSDBAdd( W::SeqEnum[SeqEnum[RngIntElt]],
    K::SeqEnum[SeqEnum[RngIntElt]], name::MonStgElt : ample:=false,
    define_fan:=false, allow_weak_fano:=false )
{Adds the period sequence for the given weight data W and bundles K to the period sequence database using the given name. If there already exists a period sequence of dimension d with this name, then it will be replaced.}
    // Sanity check
    require #W ne 0: "Argument 1 must be a non-empty sequence of weight data";
    n:=#W[1];
    require &and[#w eq n : w in W]:
        "Argument 1 must be a sequence of sequences of integers, all of the same length";
    require #Trim(name) ne 0: "Argument 3 must be non-empty";
    // Note the dimension
    d:=n - #W - #K;
    // Calculate the period sequence
    S:=PeriodSequenceForCompleteIntersection(W,K,NUM_TERMS - 1 : ample:=ample,
                      define_fan:=define_fan, allow_weak_fano:=allow_weak_fano);
    // Add the period sequence
    PSDBAdd(d,S,name);
end intrinsic;

intrinsic PSDBNames( d::RngIntElt ) -> SetEnum[MonStgElt]
{The set of names of all period sequences of dimension d in the database.}
    // Sanity check
    require d ge 1: "Argument must be a positive integer";
    // Establish a connection to the database
    bool,db:=db_connect();
    require bool: db;
    // Perform the query
    sql:=Sprintf("SELECT name FROM period_sequences WHERE dim = %o",d);
    res:=SqliteQuery(db,sql);
    // Extract the results
    names:={Universe({"x"})|};
    for row in res do
        Include(~names,unescape_string(row[1][2]));
    end for;
    // Return the results
    return names;
end intrinsic;

intrinsic PSDBAllPeriodSequences( d::RngIntElt ) -> SetEnum[SeqEnum[RngIntElt]]
{The set of all period sequences of dimension d in the database.}
    // Sanity check
    require d ge 1: "Argument must be a positive integer";
    // Establish a connection to the database
    bool,db:=db_connect();
    require bool: db;
    // Perform the query
    sql:=Sprintf("SELECT ps FROM period_sequences WHERE dim = %o",d);
    res:=SqliteQuery(db,sql);
    // Extract the results
    ps:={PowerSequence(Integers())|};
    for row in res do
        Include(~ps,CommaSeparatedStringToIntegerSequence(row[1][2]));
    end for;
    // Return the results
    return ps;
end intrinsic;

intrinsic PSDBIsPeriodSequence( d::RngIntElt, S::SeqEnum[RngIntElt] )
    -> BoolElt, SetEnum[MonStgElt]
{True iff the period sequence S of dimension d exists in the period sequence database. If it does exist, the set of associated names is also returned.}
    // Sanity check
    require d ge 1: "Argument 1 must be a positive integer";
    // Establish a connection to the database
    bool,db:=db_connect();
    require bool: db;
    // What we search on depends on how many terms we have
    if #S ge NUM_TERMS then
        // Truncate the period sequence, build the string representation, and
        // calculate the hash value
        S:=S[1..NUM_TERMS];
        strS:=ps_to_string(S);
        hash:=ps_hash(S);
        // Build the sql query
        sql:=Sprintf("SELECT ps,name FROM period_sequences WHERE hash = %o AND dim = %o AND c2 = %o AND c3 = %o AND c4 = %o AND c5 = %o AND ps = \"%o\"",hash,d,S[3],S[4],S[5],S[6],strS);
    else
        // Build the sql query
        sql:=Sprintf("SELECT ps,name FROM period_sequences WHERE dim = %o",d);
        cs:=[Sprintf("c%o = %o",i,S[i + 1]) : i in [2..Min(5,#S - 1)]];
        if #cs ne 0 then
            sql cat:= " AND " cat Join(cs," AND ");
        end if;
    end if;
    // Perform the query
    res:=SqliteQuery(db,sql);
    // Extract the results
    names:={};
    for row in res do
        ps:=CommaSeparatedStringToIntegerSequence(row[1][2]);
        if ps[1..#S] eq S then
            Include(~names,unescape_string(row[2][2]));
        end if;
    end for;
    // Return the results
    if #names eq 0 then return false,_; end if;
    return true,names;
end intrinsic;

intrinsic PSDBPeriodSequence( d::RngIntElt, name::MonStgElt )
    -> BoolElt, SeqEnum[RngIntElt]
{The period sequence of dimension d with given name in the period sequence database.}
    // Sanity check
    require d ge 1: "Argument 1 must be a positive integer";
    require #Trim(name) ne 0: "Argument 2 must be non-empty";
    // Escape the name
    name:=escape_string(name);
    // Establish a connection to the database
    bool,db:=db_connect();
    require bool: db;
    // Perform the query
    sql:=Sprintf("SELECT ps FROM period_sequences WHERE dim = %o AND name = \"%o\"",d,name);
    res:=SqliteQuery(db,sql);
    // Extract and return the results
    require #res ne 0:
        Sprintf("The name does not correspond to a %o-dimensional period sequence contained in the database",d);
    return CommaSeparatedStringToIntegerSequence(res[1][1][2]);
end intrinsic;

intrinsic PSDBDelete( d::RngIntElt, name::MonStgElt )
{Deletes the period sequence of dimension d with given name from the period sequence database, if it exists.}
    // Sanity check
    require d ge 1: "Argument 1 must be a positive integer";
    require #Trim(name) ne 0: "Argument 2 must be non-empty";
    // Escape the name
    name:=escape_string(name);
    // Establish a connection to the database
    bool,db:=db_connect();
    require bool: db;
    // Delete any matching entries
    sql:=Sprintf("DELETE FROM period_sequences WHERE dim = %o AND name = \"%o\"",d,name);
    SqliteExecute(db,sql);
end intrinsic;

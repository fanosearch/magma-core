freeze;

/////////////////////////////////////////////////////////////////////////
// periodsequence.m
/////////////////////////////////////////////////////////////////////////
// Tools for computing the period sequence.
/////////////////////////////////////////////////////////////////////////

import "db.m": fetch_period_sequence,save_period_sequence;

// The power of f used when constructing the list of prime numbers to be used
// with the Chinese Remainder Theorem
HWH_POWER:=32;

/////////////////////////////////////////////////////////////////////////
// Algorithm validation and choice
/////////////////////////////////////////////////////////////////////////

// Returns true if the algorithm is valid, false followed by an error message
// otherwise.
function validate_algorithm(name)
    if Type(name) eq MonStgElt and (name eq "auto" or name eq "cone" or
        name eq "naive" or name eq "crt" or name eq "hartwoodhouse") then
        return true,_;
    end if;
    return false,"Parameter 'algorithm' should be either \"naive\", \"crt\", \"cone\", \"hartwoodhouse\", or \"auto\" [default]";
end function;

// Returns true if the interpolation flag is valid, false followed by an
// error message otherwise.
function validate_interpolate(alg,interp)
    if Type(interp) ne BoolElt then
        return false,"Parameter 'interpolate' must be a boolean";
    end if;
    if interp eq true and alg eq "cone" then
        return false,"Parameter 'interpolate' must be false when the algorithm is set to \"cone\"";
    end if;
    return true,_;
end function;

// Makes a guess at the most appropriate algorithm to use, based on the number
// of terms k + 1 to calculate and either the Laurent polynomial.
function choose_algorithm(k,f)
    // Note the coefficient ring.
    K:=CoefficientRing(Parent(f));
    // If the coefficients come from a finite field, we use the naive method.
    if IsFinite(K) then return "naive"; end if;
    // If the coefficients come from the rationals or integers, we'll see if
    // the Chinese Remainder Theorem is a good bet.
    if k gt HWH_POWER and (K cmpeq Integers() or K cmpeq Rationals()) then
        cs:=CoefficientsAndExponents(f);
        if CanChangeUniverse(cs,Integers()) and &and[c ge 0 : c in cs] then
            return "crt";
        end if;
    end if;
    // We default we fall back on the naive method of successive powers.
    return "naive";
end function;

/////////////////////////////////////////////////////////////////////////
// Hart-Woodhouse interface
/////////////////////////////////////////////////////////////////////////

// Return an array of coefficients for the Laurent polynomial f.
function encode_coefficients(f)
    cs,exponents:=CoefficientsAndExponents(Numerator(f));
    bool,cs:=CanChangeUniverse(cs,Integers());
    assert bool;
    ParallelSort(~exponents,~cs);
    return cs;
end function;

// Returns an array of exponents for the Laurent polynomial f, encoded as a
// sequence of integers.
function encode_exponents(f)
    ebits:=Floor(64 / Rank(Parent(f)));
    _,exponents:=CoefficientsAndExponents(Numerator(f));
    Sort(~exponents);
    vect:=[Integers()|];
    for e in exponents do
        exp:=0;
        for i in e do
            exp *:= 2^ebits;
            exp +:= i;
        end for;
        Append(~vect,exp);
    end for;
    return vect;
end function;

// Returns the denominator of the Laurent polynomial f, encoded as an integer.
function encode_denominator(f)
    ebits:=Floor(64 / Rank(Parent(f)));
    res:=0;
    for i in Exponents(Denominator(f)) do
        res *:= 2^ebits;
        res +:= i;
    end for;
    return res;
end function;

// Returns true followed by the path to the Hart-Woodhouse function. May also
// return false followed by an error message.
function hart_woodhouse_path()
    try
        base:=FanosearchGetRoot() cat "/bin/periodSequence";
    catch e
        return false,e`Object;
    end try;
    return true,base;
end function;

// Performs the calculation using the Hart-Woodhouse code for the given Laurent
// polynomial f and number of terms k. On success, returns true followed by
// the sequence of coefficients. May also return false followed by an error.
forward estimate_primes;
function hart_woodhouse_command(f,k)
    // Sanity check
    error if not IsLaurent(f),
        "Argument 1 must be a Laurent polynomial";
    error if not Type(k) eq RngIntElt or k le 0,
        "Power must be a positive integer";
    // Fetch the path to the Hart-Woodhouse script
    bool,cmd:=hart_woodhouse_path();
    if not bool then return false,cmd; end if;
    // Calculate the required number of primes
    vprintf Fanosearch: "Estimating primes...\n";
    M:=estimate_primes(f,k);
    vprintf Fanosearch: "Will require %o primes:\n%o\n",#M,M;
    // Encode the Laurent polynomial
    cs:=encode_coefficients(f);
    es:=encode_exponents(f);
    den:=encode_denominator(f);
    // Build the argument string
    args:=Sprintf("%o %o %o %o %o",Rank(Parent(f)),k,#cs,#M,den);
    for c in cs do
        args cat:= " " cat IntegerToString(c);
    end for;
    for e in es do
        args cat:= " " cat IntegerToString(e);
    end for;
    for p in M do
        args cat:= " " cat IntegerToString(p);
    end for;
    // Execute the command
    vprintf Fanosearch: "Executing command:\n%o %o\n",cmd,args;
    bool,res:=PipeCommand(cmd cat " " cat args);
    if not bool then
        return false,"Unable to execute Hart-Woodhouse command. Check that the path is valid:\n   " cat cmd;
    end if;
    vprintf Fanosearch: "Recieved result:\n%o\n",res;
    // Return the sequence of coefficients
    try
        S:=eval res;
    catch e
        return false,"Unable to parse output from Hart-Woodhouse script";
    end try;
    if Type(S) ne SeqEnum then
        return false,"Unable to parse output from Hart-Woodhouse script";
    end if;
    return true,[Integers() | 1] cat S;
end function;

/////////////////////////////////////////////////////////////////////////
// Algorithm implementations
/////////////////////////////////////////////////////////////////////////

// Returns g^k using successive multiplication g. For some reason this is faster
// that computing g^k by an order of magnitude!
function poly_to_the_k(g,k)
    // Sanity check
    error if Type(k) ne RngIntElt or k le 0, "Power must be a positive integer";
    // Do the multiplication
    gg:=Parent(g) ! 1;
    for i in [1..k] do
        gg *:= g;
    end for;
    return gg;
end function;

// Returns a guess at enough primes in order to compute the constant terms of
// f^i, 0 <= i <= k, using the Chinese Remainder Theorem. Note that this is
// both the most crucial step of this algorithm, and also the weakest part.
// Any improvment on these bounds will yield massive improvements!
function estimate_primes(f,k : max_prime:=2^52)
    // Sanity check
    error if not IsLaurent(f),
        "Argument 1 must be a Laurent polynomial over the integers";
    error if not Type(k) eq RngIntElt or k le 0,
        "Power must be a positive integer";
    // We only want to work with the numerator
    f:=Numerator(f);
    // To get started, extract the maximum coefficient of f^HWH_POWER
    cs:=Coefficients(poly_to_the_k(f,HWH_POWER));
    bool,cs:=CanChangeUniverse(cs,Integers());
    error if not bool,
        "Argument 1 must be a Laurent polynomial over the integers";
    upperLim:=Maximum([Integers() | Abs(c) : c in cs]);
    numOfTerms:=#cs;
    // Extend the upper limit based on the power k
    p:=HWH_POWER;
    while p lt k do
        upperLim:=2 * numOfTerms * upperLim^2;
        numOfTerms:=numOfTerms^2;
        p *:= 2;
    end while;
    // Now extract enough large primes
    prod:=1;
    i:=max_prime;
    primes:=[Integers()|];
    while prod lt upperLim * 4 do
        if IsPrime(i) then
            Append(~primes,i);
            prod *:= i;
        end if;
        i -:= 1;
    end while;
    // Return the primes
    return primes;
end function;

// Returns the constant terms of f^i mod q for 0 <= i <= k, where q is a prime
// power, and f is a Laurent polynomial with integer coefficients. The resulting
// sequence will be coerced back into the Integers().
function first_k_terms_mod_q(f,q,k)
    // Sanity check
    error if not IsLaurent(f),
        "Argument 1 must be a Laurent polynomial";
    error if not Type(q) eq RngIntElt or not IsPrimePower(q),
        "Argument 2 must be a prime power";
    error if not Type(k) eq RngIntElt or k le 0,
        "Power must be a positive integer";
    // Extract the denominator and numerator
    den:=Denominator(f);
    if den in CoefficientRing(Parent(f)) then
        den:=Parent(f) ! 1;
    else
        den:=Monomials(den)[1];
    end if;
    f:=Numerator(f);
    // Cast them into FF_q[x1,...,xj]
    RR:=Parent(f);
    F:=FiniteField(q);
    R:=PolynomialRing(F,Rank(RR));
    phi:=hom<RR -> R | [R.i : i in [1..Rank(R)]]>;
    f:=phi(f);
    den:=phi(den);
    // Now we can compute the constant terms
    vprintf Fanosearch: "Computing period sequence over FF_%o...\n",q;
    t:=Cputime();
    ff:=R ! 1;
    dd:=R ! 1;
    S:=[F | 1];
    for i in [1..k] do
        ff *:= f;
        dd *:= den;
        cs,ms:=CoefficientsAndMonomials(ff);
        idx:=Index(ms,dd);
        c:=idx ne 0 select cs[idx] else F ! 0;
        Append(~S,c);
    end for;
    vprintf Fanosearch: "Time: %os\n",Cputime(t);
    // Coerce the sequence back into the integers and return
    bool,S:=CanChangeUniverse(S,Integers());
    error if not bool, "Unable to coerce coefficients back into the integers";
    return S;
end function;

// Applies the Chinese Remainder Theorem to the given sequence S of sequences
// of integers S[i], lifting with respect to the sequence of moduli M. Returns
// a sequence X such that X[i] = S[j][i] mod M[j] for each i,j.
function chinese_remainder_theorem(S,M)
    // Sanity check
    error if #S eq 0, "Argument 1 must not be empty";
    error if #M ne #S,
        "Arguments 1 and 2 must be of the same length";
    n:=#S[1];
    error if n eq 0 or not &and[#s eq n : s in S] or
        not &and[Universe(s) cmpeq Integers() : s in S],
        "Argument 1 must be a sequence of non-empty integer sequences all of the same length";
    // Compute the result
    SS:=[Integers()|];
    for i in [1..n] do
        c:=ChineseRemainderTheorem([Integers() | s[i] : s in S],M);
        error if c eq -1, "No solution exists";
        Append(~SS,c);
    end for;
    // Return the sequence
    return SS;
end function;

// Returns the constant terms of f^i for 0 <= i <= k, where f is a Laurent
// polynomial.
function first_k_terms(f,k)
    // Sanity check
    error if not IsLaurent(f),
        "Argument 1 must be a Laurent polynomial";
    error if not Type(k) eq RngIntElt or k le 0,
        "Power must be a positive integer";
    // Extract the denominator and numerator
    den:=Denominator(f);
    if den in CoefficientRing(Parent(f)) then
        den:=Parent(f) ! 1;
    else
        den:=Monomials(den)[1];
    end if;
    f:=Numerator(f);
    // Record the polynomial ring and coefficient ring
    R:=Parent(f);
    F:=CoefficientRing(R);
    // Now we can compute the constant terms
    ff:=R ! 1;
    dd:=R ! 1;
    S:=[F | 1];
    for i in [1..k] do
        vprintf Fanosearch: "Calculating f^%o...\n",i;
        t:=Cputime();
        ff *:= f;
        dd *:= den;
        cs,ms:=CoefficientsAndMonomials(ff);
        idx:=Index(ms,dd);
        c:=idx ne 0 select cs[idx] else F ! 0;
        Append(~S,c);
        vprintf Fanosearch: "Time: %os\n",Cputime(t);
    end for;
    // Return the sequence
    return S;
end function;

// Computes the first k + 1 coefficients of the period sequence calculating
// successive powers of f using the Chinese Remainder Theorem.
function successive_powers_crt(f,k)
    // Sanity check
    error if not IsLaurent(f),
        "Argument 1 must be a Laurent polynomial";
    error if not Type(k) eq RngIntElt or k le 0,
        "Power must be a positive integer";
    error if not CanChangeUniverse(Coefficients(Numerator(f)),Integers()),
        "The coefficients of the Laurent polynomial must be integeral";
    error if not &and[c ge 0 : c in Coefficients(Numerator(f))],
        "The coefficients of the Laurent polynomial must be positive";
    // Perform the computation
    vprintf Fanosearch: "Estimating primes...\n";
    M:=estimate_primes(f,k);
    vprintf Fanosearch: "Will require %o primes:\n%o\n",#M,M;
    S:=[first_k_terms_mod_q(f,q,k) : q in M];
    cs:=chinese_remainder_theorem(S,M);
    // Return the coefficients
    return cs;
end function;

// Computes the first k + 1 coefficients of the period sequence by naively
// calculating successive powers of f.
function successive_powers_naive(f,k)
    // Sanity check
    error if not IsLaurent(f),
        "Argument 1 must be a Laurent polynomial";
    error if not Type(k) eq RngIntElt or k le 0,
        "Power must be a positive integer";
    // Perform the computation
    cs:=first_k_terms(f,k);
    // Return the coefficients
    if Universe(cs) cmpeq Rationals() then
        bool,newcs:=CanChangeUniverse(cs,Integers());
        if bool then cs:=newcs; end if;
    end if;
    return cs;
end function;

// Computes the first k + 1 coefficients of the period sequence using the
// kernel cone.
function cone_of_kernel(S,coeffs,k)
    // Sanity check
    d:=#S;
    error if d eq 0,
        "Argument 1 must be a non-empty sequence of points";
    error if #coeffs ne d,
        "Arguments 1 and 2 must be in bijective correspondence";
    error if not Type(k) eq RngIntElt or k le 0,
        "Power must be a positive integer";
    // Create the transposed sequence
    ineqs:=ColumnSequence(Matrix(S));
    // Construct the cone
    posorth:=RowSequence(IdentityMatrix(Rationals(),d));
    C:=ConeWithInequalities(ineqs cat [[-c : c in ineq] : ineq in ineqs]
                                                                   cat posorth);
    // Start constructing the terms
    cs:=[Universe(coeffs) | 1];
    n:=Dual(Ambient(C)) ! [1 : i in [1..d]];
    for i in [1..k] do
        vprintf Fanosearch: "Calculating points at height %o...\n",i;
        t:=Cputime();
        c:=Universe(coeffs) ! 0;
        for pt in Points(C,n,i) do
            eltpt:=Eltseq(pt);
            c+:=&*[coeffs[j]^eltpt[j] : j in [1..d]] * Multinomial(i,eltpt);
        end for;
        Append(~cs,c);
        vprintf Fanosearch: "Time: %os\n",Cputime(t);
    end for;
    // Return the coefficients
    bool,newcs:=CanChangeUniverse(cs,Integers());
    if bool then cs:=newcs; end if;
    return cs;
end function;

// Computes the first k + 1 coefficients of the period sequence of f via
// interpolation, using the specified algorithm for the sub-period sequence
// calculations.
function interpolate_period_sequence(f,k : algorithm:="naive")
    // Sanity check
    error if not IsLaurent(f),
        "Argument 1 must be a Laurent polynomial";
    error if not Type(k) eq RngIntElt or k le 0,
        "Power must be a positive integer";
    // Split f into a numerator and denominator and note the degrees we need
    num:=Numerator(f);
    maxj:=Maximum([Reverse(Exponents(m))[1] : m in Monomials(num)]);
    den:=Denominator(f);
    denj:=Reverse(Exponents(den))[1];
    // Extract the base ring and check it
    RR:=Parent(num);
    F:=CoefficientRing(RR);
    error if not F cmpeq Integers() and not IsField(F),
        "The coefficient ring must be either the integers or a field";
    // Construct the new polynomial ring and map the denominator into it
    R:=PolynomialRing(F,Rank(RR) - 1);
    phi:=hom<RR -> R | [R.i : i in [1..Rank(R)]] cat [1]>;
    den:=phi(den);
    // Create the sequence of points for interpolation
    pts:=[F | i : i in [1..k * maxj]];
    // Calculate the sub-period sequences
    SS:=[PowerSequence(F)|];
    for i in [1..#pts] do
        vprintf Fanosearch: "Period sequence %o of %o...\n", i, #pts;
        t:=Cputime();
        phi:=hom<RR -> R | [R.j : j in [1..Rank(R)]] cat [pts[i]]>;
        ff:=phi(num) / den;
        if algorithm eq "naive" then
            Append(~SS,successive_powers_naive(ff,k));
        elif algorithm eq "crt" then
            Append(~SS,successive_powers_crt(ff,k));
        elif algorithm eq "hartwoodhouse" then
            bool,cs:=hart_woodhouse_command(ff,k);
            error if not bool, cs;
            Append(~SS,cs);
        end if;
        vprintf Fanosearch: "Total time: %os\n", Cputime(t);
    end for;
    // Perform interpolation to recover the period sequence
    if F cmpeq Integers() then
        F:=Rationals();
        ChangeUniverse(~pts,Rationals());
        ChangeUniverse(~SS,PowerSequence(Rationals()));
    end if;
    cs:=[F | 1];
    for i in [1..k] do
        g:=Interpolation(pts,[F | S[i + 1] : S in SS]);
        Append(~cs,Coefficient(g,i * denj));
    end for;
    // Return the result
    bool,newcs:=CanChangeUniverse(cs,Integers());
    if bool then cs:=newcs; end if;
    return cs;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic PeriodSequence( S::SeqEnum[TorLatElt], coeffs::SeqEnum, k::RngIntElt :
    algorithm:="auto", interpolate:=false, cache:=true ) -> SeqEnum
{}
    // Sanity check
    require k gt 0: "The number of terms to compute must be a positive integer";
    require #S ne 0: "Illegal empty sequence";
    require &and[IsIntegral(pt) : pt in S]: "The points must all be integral";
    require #coeffs eq #S:
        "The coefficients should be in bijective correspondence with the points";
    bool,err:=validate_algorithm(algorithm);
    require bool: err;
    require Type(cache) eq BoolElt: "Parameter 'cache' must be a boolean";
    // Build the Laurent polynomial and choose the algorithm
    f:=LaurentPolynomial(S,coeffs);
    if algorithm eq "auto" then algorithm:=choose_algorithm(k,f); end if;
    bool,err:=validate_interpolate(algorithm,interpolate);
    require bool: err;
    // Perhaps the period sequence is in the database?
    if cache then
        bool,S:=fetch_period_sequence(f,k);
        if bool then return S; end if;
    end if;
    // Compute the period sequence
    if interpolate then
        cs:=interpolate_period_sequence(f,k : algorithm:=algorithm);
    elif algorithm eq "cone" then
        cs:=cone_of_kernel(S,coeffs,k);
    elif algorithm eq "naive" then
        cs:=successive_powers_naive(f,k);
    elif algorithm eq "crt" then
        cs:=successive_powers_crt(f,k);
    elif algorithm eq "hartwoodhouse" then
        bool,cs:=hart_woodhouse_command(f,k);
        require bool: cs;
    end if;
    // Save the result in the database
    if cache then save_period_sequence(f,cs); end if;
    // Return the result
    return cs;
end intrinsic;

intrinsic PeriodSequence( P::TorPol, coeffs::SeqEnum, k::RngIntElt :
    sortpts:=false, algorithm:="auto", interpolate:=false, cache:=true )
    -> SeqEnum
{}
    // Sanity check
    require k gt 0: "The number of terms to compute must be a positive integer";
    require IsPolytope(P) and IsIntegral(P):
        "The polyhedron must be an integral polytope";
    if sortpts cmpeq false then
        sortpts:=Sort(Points(P));
    else
        require Type(sortpts) eq SeqEnum and Universe(sortpts) eq Ambient(P):
            "Parameter 'sortpts' should be a sequence of distinct points in the polytope";
        tmp:=SequenceToSet(sortpts);
        require #tmp eq #sortpts and tmp subset Points(P):
            "Parameter 'sortpts' should be a sequence of distinct points in the polytope";
    end if;
    require #coeffs eq #sortpts:
        "The coefficients should be in bijective correspondence with the points";
    bool,err:=validate_algorithm(algorithm);
    require bool: err;
    require Type(cache) eq BoolElt: "Parameter 'cache' must be a boolean";
    // Build the Laurent polynomial and choose the algorithm
    f:=LaurentPolynomial(sortpts,coeffs);
    if algorithm eq "auto" then algorithm:=choose_algorithm(k,f); end if;
    bool,err:=validate_interpolate(algorithm,interpolate);
    require bool: err;
    // Perhaps the period sequence is in the database?
    if cache then
        bool,S:=fetch_period_sequence(f,k);
        if bool then return S; end if;
    end if;
    // Compute the period sequence
    if interpolate then
        cs:=interpolate_period_sequence(f,k : algorithm:=algorithm);
    elif algorithm eq "cone" then
        cs:=cone_of_kernel(sortpts,coeffs,k);
    elif algorithm eq "naive" then
        cs:=successive_powers_naive(f,k);
    elif algorithm eq "crt" then
        cs:=successive_powers_crt(f,k);
    elif algorithm eq "hartwoodhouse" then
        bool,cs:=hart_woodhouse_command(f,k);
        require bool: cs;
    end if;
    // Save the result in the database
    if cache then save_period_sequence(f,cs); end if;
    // Return the result
    return cs;
end intrinsic;

intrinsic PeriodSequence( f::FldFunRatMElt, k::RngIntElt :
    algorithm:="auto", interpolate:=false, cache:=true ) -> SeqEnum
{The first k+1 terms of the period sequence. The optional parameter 'algorithm' can be set to either "naive", "crt", "cone", "hartwoodhouse", or "auto" [default]. Set 'interpolate' to true to reduce the dimension by one via interpolation (this has the effect of decreasing maximum memory usage whilst increasing runtime).}
    // Sanity check
    require k gt 0: "The number of terms to compute must be a positive integer";
    require IsLaurent(f): "The rational function must be a Laurent polynomial";
    bool,err:=validate_algorithm(algorithm);
    require bool: err;
    require Type(cache) eq BoolElt: "Parameter 'cache' must be a boolean";
    // Choose the algorithm
    if algorithm eq "auto" then algorithm:=choose_algorithm(k,f); end if;
    bool,err:=validate_interpolate(algorithm,interpolate);
    require bool: err;
    // Perhaps the period sequence is in the database?
    if cache then
        bool,S:=fetch_period_sequence(f,k);
        if bool then return S; end if;
    end if;
    // Compute the period sequence
    if interpolate then
        cs:=interpolate_period_sequence(f,k : algorithm:=algorithm);
    elif algorithm eq "cone" then
        cs,monos:=CoefficientsAndExponents(f);
        monos:=[LatticeVector(v) : v in monos];
        cs:=cone_of_kernel(monos,cs,k);
    elif algorithm eq "naive" then
        cs:=successive_powers_naive(f,k);
    elif algorithm eq "crt" then
        cs:=successive_powers_crt(f,k);
    elif algorithm eq "hartwoodhouse" then
        bool,cs:=hart_woodhouse_command(f,k);
        require bool: cs;
    end if;
    // Save the result in the database
    if cache then save_period_sequence(f,cs); end if;
    // Return the result
    return cs;
end intrinsic;

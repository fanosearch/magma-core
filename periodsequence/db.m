freeze;

/////////////////////////////////////////////////////////////////////////
// db.m
/////////////////////////////////////////////////////////////////////////
// Functions for accessing the period sequence databases.
/////////////////////////////////////////////////////////////////////////

import "../core.m": save_data, fetch_data;
import "../utilities/laurent.m": laurent_normal_form;

// The power of f used when constructing the list of prime numbers to be used
// with the Chinese Remainder Theorem
HWH_POWER:=32;

// The cache table name.
LOOKUP_TABLE_NAME:="period_sequence_lookup";
CACHE_TABLE_NAME:="period_sequence_cache";

// The timeout to use for communication.
TIMEOUT:=10;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Converts the given sequence of strings to a string "a","b",...,"c".
function stringseq_to_string(S)
    return Join([Strings() | "\"" cat s cat "\"" : s in S],",");
end function;

// Validates the database arguments.
procedure assert_arguments(monos,coeffs,hash,k)
    error if Type(monos) ne SeqEnum,
        "Monomial exponents must be given by a sequence.";
    error if Type(coeffs) ne SeqEnum,
        "Coefficients must be given by a sequence.";
    error if Type(hash) ne RngIntElt or hash lt 0 or hash ge 16777216,
        "Hash value must be an integer in the range 0..16777215.";
    error if Type(k) ne RngIntElt or k le 0,
        "Number of terms must be a positive integer.";
end procedure;

/////////////////////////////////////////////////////////////////////////
// Normal form and hash value functions
/////////////////////////////////////////////////////////////////////////

// Attempts to compute a normal form for the given Laurent polynomial f.
// Returns true followed by a sequence of monomial exponents and a sequence of
// coefficients upon success, false otherwise.
function normal_form(f)
    // Ultimately we require the coefficients to be integral, so make an easy
    // check of the parent ring
    if not (CoefficientRing(Parent(f)) cmpeq Integers() or
            CoefficientRing(Parent(f)) cmpeq Rationals()) then
       return false,_,_;
    end if;
    // Calculate the normal form
    bool,_,_,monos,coeffs:=laurent_normal_form(f : return_coeffs:=true);
    if not bool then return false,_,_; end if;
    // Check that we can move the coefficients into the integers
    bool,coeffs:=CanChangeUniverse(coeffs,Integers());
    if not bool then return false,_,_; end if;
    // Return the data
    return true,monos,coeffs;
end function;

// Given the normal form of f (as returned by normal_form above), returns the
// hash value. Note that we do NOT use a hash function provided by Magma here,
// since we intend this hash to be easily computable by other systems.
function hash_value(monos,coeffs)
    // Sanity check
    error if Type(monos) ne SeqEnum or #monos eq 0 or
        not Universe(monos) cmpeq PowerSequence(Integers()),
        "Argument 1 must be a non-empty sequence of sequences of integers.";
    d:=#monos[1];
    error if d eq 0 or &or[#m ne d : m in monos],
        "Subsequences of argument 1 must all be of the same non-zero length.";
    error if Type(coeffs) ne SeqEnum or #coeffs eq 0 or
        not Universe(coeffs) cmpeq Integers(),
        "Argument 2 must be a non-empty sequence of integers.";
    error if #monos ne #coeffs,
        "Arguments 1 and 2 must be of the same length.";
    // Compute the hash value
    hash:=1;
    for i in [1..#coeffs] do
        monohash:=1;
        for j in [1..d] do
            monohash:=(monohash + monos[i][j] * NthPrime(j)) mod 541;
        end for;
        hash:=(hash + monohash * coeffs[i] * NthPrime(i)) mod 16777216;
    end for;
    // Return the hash
    return hash;
end function;

/////////////////////////////////////////////////////////////////////////
// Database access functions
/////////////////////////////////////////////////////////////////////////

// Creates the tables.
procedure db_make_tables(db)
    // List the tables
    names:=ListTables(db : Timeout:=TIMEOUT);
    // Do we need to create LOOKUP_TABLE_NAME?
    if not LOOKUP_TABLE_NAME in names then
        // Create the table
        template:=AssociativeArray(
            ["ulid","hash","num_terms","num_coeffs"],
            [* "",0,0,0 *]);
        CreateTable(db,LOOKUP_TABLE_NAME,template : Timeout:=TIMEOUT);
        // Add the indices
        AddIndex(db,LOOKUP_TABLE_NAME,"ulid" : Timeout:=TIMEOUT);
        AddIndex(db,LOOKUP_TABLE_NAME,"hash" : Timeout:=TIMEOUT);
    end if;
    // Do we need to create CACHE_TABLE_NAME?
    if not CACHE_TABLE_NAME in names then
        // Create the table
        template:=AssociativeArray(
            ["ulid","length","exponents","coefficients","sequence"],
            [* "",0,"","","" *]);
        CreateTable(db,CACHE_TABLE_NAME,template : Timeout:=TIMEOUT);
        // Add the indices
        AddIndex(db,CACHE_TABLE_NAME,"ulid" : Timeout:=TIMEOUT);
        AddIndex(db,CACHE_TABLE_NAME,"length" : Timeout:=TIMEOUT);
    end if;
end procedure;

// Returns true followed by a connection to the database, false otherwise.
function db_connect()
    bool,db:=fetch_data("periodsequence_db");
    if not bool then
        try
            db:=KeyValueDbConnect("cache");
            db_make_tables(db);
        catch e
            return false,_;
        end try;
        save_data("periodsequence_db",db);
    end if;
    return true,db;
end function;

// This function will check the database for the period sequence. The arguments
// are the normal form of f and hash value (as returned by normal_form and
// hash_value respectively), and the required number of terms k + 1 of the
// period sequence. Returns true followed by the period sequence and database
// ULID on success, false otherwise.
function db_fetch_period_sequence(monos,coeffs,hash,k)
    // Sanity check
    assert_arguments(monos,coeffs,hash,k);
    // Establish a connection to the database
    bool,db:=db_connect();
    if not bool then return false,_,_; end if;
    // Create the selector to recover the candidate period sequence ULIDs
    selector:=AssociativeArray(
        ["hash","num_terms","num_coeffs"],
        [* hash, #[1 : c in coeffs | c ne 0], #coeffs *]);
    // Perform the query
    itr:=Select(db,LOOKUP_TABLE_NAME,{"ulid"},selector : Timeout:=TIMEOUT);
    ulids:=[Strings()|];
    while Next(itr : Timeout:=TIMEOUT) do
        A:=Current(itr);
        Append(~ulids,A["ulid"]);
    end while;
    Close(itr);
    if #ulids eq 0 then return false,_,_; end if;
    // Create the selector to hunt for a matching period sequence
    sql:=Sprintf("WHERE length >= %o AND exponents = \"%o\" AND coefficients = \"%o\" AND ulid ",
        k+1,ToGob(&cat monos),ToGob(coeffs));
    if #ulids eq 1 then
        sql cat:= "= \"" cat ulids[1] cat "\"";
    else
        sql cat:= "IN (" cat stringseq_to_string(ulids) cat ")";
    end if;
    // Perform the query
    A,ok:=SelectOne(db,CACHE_TABLE_NAME,{"ulid","sequence"},sql :
                                                        Timeout:=TIMEOUT);
    if not ok then
        return false,_,_;
    end if;
    // Return the match
    ps:=GobToIntegerSequence(A["sequence"]);
    return true,ps[1..k+1],A["ulid"];
end function;

// Attempts to save the given period sequence to the database. Returns true
// followed by the database ULID on success, false otherwise.
function db_save_period_sequence(monos,coeffs,hash,S)
    // Sanity check
    assert_arguments(monos,coeffs,hash,1);
    bool,S:=CanChangeUniverse(S,Integers());
    if not bool then return false,_; end if;
    // Establish a connection to the database
    bool,db:=db_connect();
    if not bool then return false,_,_; end if;
    // We'll need to gob encode the monomials, coefficients, and period sequence
    monosstr:=ToGob(&cat monos);
    coeffsstr:=ToGob(coeffs);
    Sstr:=ToGob(S);
    numterms:=#[1 : c in coeffs | c ne 0];
    // Create the selector to recover the candidate period sequence ULIDs
    selector:=AssociativeArray(
        ["hash","num_terms","num_coeffs"],
        [* hash,numterms,#coeffs *]);
    // Perform the query
    itr:=Select(db,LOOKUP_TABLE_NAME,{"ulid"},selector : Timeout:=TIMEOUT);
    ulids:=[Strings()|];
    while Next(itr : Timeout:=TIMEOUT) do
        A:=Current(itr);
        Append(~ulids,A["ulid"]);
    end while;
    Close(itr);
    // If there aren't any matching ULIDs from the core table, add an entry.
    // Note that there's a race condition here, but it's nothing terrible. At
    // worst there will be duplicate entries in the cache.
    if #ulids eq 0 then
        // Generate a ULID
        ulid:=ULIDNew();
        // Add the data -- this requires two inserts
        A:=AssociativeArray(
            ["ulid","hash","num_terms","num_coeffs"],
            [* ulid,hash,numterms,#coeffs *]);
        _:=Insert(db,LOOKUP_TABLE_NAME,A);
        A:=AssociativeArray(
            ["ulid","length","exponents","coefficients","sequence"],
            [* ulid,#S,monosstr,coeffsstr *]);
        _:=Insert(db,CACHE_TABLE_NAME,A);
        // Return success
        return true,ulid;
    end if;
    // Create the selector to hunt for a matching period sequence
    sql:=Sprintf("WHERE exponents = \"%o\" AND coefficients = \"%o\" AND ulid ",
        monosstr,coeffsstr);
    if #ulids eq 1 then
        sql cat:= "= \"" cat ulids[1] cat "\"";
    else
        sql cat:= "IN (" cat stringseq_to_string(ulids) cat ")";
    end if;
    // Perform the query
    template:=AssociativeArray(Strings(),[* "ulid","" *],[* "length",0 *]);
    A,ok:=SelectOne(db,CACHE_TABLE_NAME,template,sql:Timeout:=TIMEOUT);
    if ok then
        ulid:=A["ulid"];
        k:=A["length"];
        // Is there anything to do?
        if k ge #S then return true,ulid; end if;
        // We need to update the period sequence
        selector:=AssociativeArray(
            ["ulid","exponents","coefficients"],
            [* ulid,monosstr,coeffsstr *]);
        replacement:=AssociativeArray(
            ["length","sequence"],
            [* #S,Sstr *]);
        _:=Update(db,CACHE_TABLE_NAME,selector,replacement : Timeout:=TIMEOUT);
        // Return success
        return true,ulid;
    end if;
    // It's not there, so add the data -- we can use any of the found ULIDs
    A:=AssociativeArray(
        ["ulid","length","exponents","coefficients","sequence"],
        [* ulids[1],#S,monosstr,coeffsstr,Sstr *]);
    _:=Insert(db,CACHE_TABLE_NAME,A);
    // Return the ulid
    return true,ulids[1];
end function;

/////////////////////////////////////////////////////////////////////////
// Main access functions
/////////////////////////////////////////////////////////////////////////

// Saves the given period sequence for f in the databases.
procedure save_period_sequence(f,S)
    // If we're not running pcas-magma then there's nothing to do
    if not IsPcasMagma() then return; end if;
    // Compute the normal form and hash value of f
    bool,monos,coeffs:=normal_form(f);
    if not bool then return; end if;
    hash:=hash_value(monos,coeffs);
    // Record the data in the database
    try _:=db_save_period_sequence(monos,coeffs,hash,S);
    catch e;
    end try;
end procedure;

// Checks the databases for the first k + 1 terms in the period sequence for f.
// Returns true followed by the period sequence on success, false otherwise.
function fetch_period_sequence(f,k)
    // If we're not running pcas-magma then there's nothing to do
    if not IsPcasMagma() then return false,_; end if;
    // Compute the normal form and hash value of f
    bool,monos,coeffs:=normal_form(f);
    if not bool then return false,_; end if;
    hash:=hash_value(monos,coeffs);
    // Perhaps the database has the answer?
    try
        bool,S:=db_fetch_period_sequence(monos,coeffs,hash,k);
    catch e
        bool:=false;
    end try;
    if bool then return true,S; end if;
    // No luck
    return false,_;
end function;

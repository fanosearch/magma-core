freeze;

/////////////////////////////////////////////////////////////////////////
// ramification.m
/////////////////////////////////////////////////////////////////////////
// Tools for computing the ramification data.
/////////////////////////////////////////////////////////////////////////
// Example:
//  K<x,y> := RationalFunctionField(Rationals(),2);
//  f := x+y+x^(-1)*y^(-1);
//  ps := PeriodSequence(f,100);
//  PF := PicardFuchsOperator(ps);
//  IsExtremal(PF);         // true
//  RamificationData(PF);   // the local ramification data
/////////////////////////////////////////////////////////////////////////

// The local data cache
ramification_cache:=NewStore();

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Returns the polynomial ring QQ[t][D].
function polynomial_ring()
    bool,S:=StoreIsDefined(ramification_cache,"polyring");
    if not bool then
        S:=PolynomialRing(PolynomialRing(Rationals()));
        StoreSet(ramification_cache,"polyring",S);
    end if;
    return S;
end function;

// Given PF an element of the Weyl algebra W<D,t>, maps it into the polynomial
// ring QQ[t][D].
function change_ring(PF)
    bool,phi:=StoreIsDefined(ramification_cache,"polymap");
    if not bool then
        S:=polynomial_ring();
        phi:=hom<Parent(PF) -> S | [S.1,CoefficientRing(S).1]>;
        StoreSet(ramification_cache,"polymap",phi);
    end if;
    return phi(PF);
end function;

// Given a matrix over kk[[t]] or kk[t] or kk(t), return the eigenvalues of that
// matrix after setting t=0. These eigenvalues are explicitly coerced into QQ,
// to avoid lots of confusion caused by the fact that the ordering of rationals
// inside AC is NOT the same as the ordering of rationals inside QQ.
function exponents(M)
    return [Rationals() | e[1] : e in Eigenvalues(Evaluate(M,0))];
end function;

// Rewrite the PF operator PF in k[t][D_t] in terms of s := t-a and return it
// as an element of k[s][D_s]
function PF_shift(PF,a)
    // Notation: s = t - a, tau = a s^(-1), so then Dt = (1+tau) Ds.
    // Create the target polynomial ring k[s][Ds].
    C:=Parent(a) cmpeq Integers() select Rationals() else Parent(a);
    R<s>:=PolynomialRing(C);
    RR<Ds>:=PolynomialRing(R);
    // Create the target "corrected" Weyl algebra
    WR:=FreeAlgebra(Rationals(),2);
    W<Dss,tau>:=WR / ideal< WR | WR.2 * WR.1 - WR.2 - WR.1 * WR.2>;
    // Now we need the powers Dt^k
    N:=Degree(PF);
    powers:=[((1+tau)*Dss)^k : k in [1..N]];
    // Now take (t-a)^N D^k and write it out in RR
    bumpedpowers:=[RR|];
    for pow in powers do
        cs,es:=CoefficientsAndExponents(pow);
        bp:=RR ! 0;
        for i in [1..#cs] do
            bp +:= cs[i] * a^es[i][2] * s^(N - es[i][2]) * Ds^es[i][1];
        end for;
        Append(~bumpedpowers,bp);
    end for;
    // Finally multiply the powers by the functions in s
    phi:=hom<CoefficientRing(Parent(PF)) -> R | [s + a]>;
    return &+[phi(Coefficient(PF,k)) * bumpedpowers[k] : k in [1..N]] +
                                              RR ! phi(Coefficient(PF,0)) * s^N;
end function;

// Returns the number of basic gauge transformations that we need in order to
// give the matrix mm prepared eigenvalues prepared means in the half-open
// interval [0,1) and each basic gauge transformation shifts an eigenvalue by 1
// so we just look at the eigenvalues and count.
function number_of_steps(mm)
    // Extract the eigenvalues and corresponding multiplicities
    eigenvalues_and_multiplicities:=Eigenvalues(mm);
    eigenvalues:=[I[1] : I in eigenvalues_and_multiplicities];
    multiplicities:=[I[2] : I in eigenvalues_and_multiplicities];
    // Fuchsian theory implies that all eigenvalues are rational
    bool,eigenvalues:=CanChangeUniverse(eigenvalues,Rationals());
    assert bool;
    assert (&+multiplicities) eq NumberOfRows(mm);
    result:=0;
    eigenvalues:=SequenceToSet(eigenvalues);
    max_eval:=Max(eigenvalues);
    while max_eval ge 1 do
        Exclude(~eigenvalues,max_eval);
        Include(~eigenvalues,max_eval - 1);
        result:=result + 1;
        max_eval:=Max(eigenvalues);
    end while;
    min_eval:=Min(eigenvalues);
    while min_eval lt 0 do
        Exclude(~eigenvalues,min_eval);
        Include(~eigenvalues,min_eval + 1);
        result:=result + 1;
        min_eval:=Min(eigenvalues);
    end while;
    return result;
end function;

// Perform a basic gauge transformation on M so as to increase or decrease the
// eigenvalue lambda (increase if plusminus eq +1; decrease if plusminus eq -1).
// M is a matrix with entries in kk((t)).
function prepare(M,lambda,plusminus)
    // Sanity check
    assert Abs(plusminus) eq 1;
    assert Type(BaseRing(M)) eq RngSerLaur;
    // Now get the Jordan decomposition of the leading term of nn
    leading_term:=Evaluate(M,0);
    jnf,T:=JordanForm(leading_term);
    vprintf Fanosearch: "Jordan form of M_0:\n%o\n", jnf;
    // We want to gauge as U^{-1}.A.U not T.A.T^{-1}. We will make our gauge
    // transformation U as
    //   U = Ubase * Uextra
    // where Ubase is the t-independent transformation that puts the leading
    // term into Jordan normal form, and Uextra is the t-dependent part which
    // shifts the generalized eigenspace for lambda into the generalized
    // eigenspace for lambda plusminus 1
    Ubase:=T^-1;
    gen:=BaseRing(M).1;
    N:=NumberOfColumns(jnf);
    Uextra:=IdentityMatrix(BaseRing(M),N);
    for j in [1..N] do
        if jnf[j,j] eq lambda then
            if plusminus eq 1 then
                Uextra[j,j]:=gen;
            elif plusminus eq -1 then
                Uextra[j,j]:=gen^(-1);
            end if;
        end if;
    end for;
    U:=Ubase * Uextra;
    vprintf Fanosearch: "Gauging by U:\n%o\n", U;
    derivative_matrix:=ZeroMatrix(BaseRing(M),N,N);
    for j in [1..N] do
        for k in [1..N] do
            derivative_matrix[j,k]:=gen * Derivative(U[j,k]);
        end for;
    end for;
    return U^(-1) * (M * U + derivative_matrix);
end function;

// The Jordan normal form of the local log-monodromy of the Picard--Fuchs
// system defined by PF about the point 'a'. PF should be an element of
// QQ[t][D], and 'a' should be an element in the AlgebraicClosure() ring.
function local_monodromy_at_point(PF,a)
    // Shift PF to t=a
    PF:=PF_shift(PF,a);
    vprintf Fanosearch: "shifted PF operator: %o\n", PF;
    // Now turn it into a matrix. We want to work over k[[t]] so we need to know
    // how many powers of t we need this depends on the eigenvalues of M(0),
    // where the PF operator is equivalent to the system of first order ODEs
    //      (d/dt)(v) = v M(t)   (v = row vector).
    // So we start off by calculating M(0).
    PF_coefficients:=Coefficients(PF);
    PF_degree:=#PF_coefficients - 1;
    leading_coefficient:=PF_coefficients[#PF_coefficients];
    Prune(~PF_coefficients);
    // We assert that M is holomorphic at t=0. Note that if this fails this is
    // not actually a contradiction, as the PF equations could still have a
    // regular singular point at t=0, but it will stop the rest of the code
    // from working.
    error if not &and[Valuation(c) ge Valuation(leading_coefficient) :
        c in PF_coefficients], "Not holomorphic at the given point";
    // Now make M(0)
    R:=CoefficientRing(CoefficientRing(PF));
    rows_of_matrix:=[PowerSequence(R) | [R | j eq k+1 select 1 else 0 :
                                j in [1..PF_degree]] : k in [1..PF_degree - 1]];
    relevant_coefficient:=Valuation(leading_coefficient);
    Append(~rows_of_matrix,
             [R | (-1) * Coefficient(c,relevant_coefficient) /
                         Coefficient(leading_coefficient,relevant_coefficient) :
                         c in PF_coefficients]);
    Mzero:=Transpose(Matrix(rows_of_matrix));
    // At this point we know M(0), so we know how many basic gauge
    // transformations we will need and hence what order we need to work to in
    // our Laurent series ring.
    vprintf Fanosearch: "Leading term is:\n%o\n", Mzero;
    vprintf Fanosearch: "Eigenvalues are: %o\n",[e[1] : e in Eigenvalues(Mzero)];
    num_of_steps:=number_of_steps(Mzero);
    vprintf Fanosearch: "To prepare the exponents, need %o basic gauge transformations.\n", num_of_steps;
    R:=LaurentSeriesRing(CoefficientRing(CoefficientRing(PF)) :
                                                   Precision:=num_of_steps + 1);
    ChangeUniverse(~PF_coefficients,R);
    // Now make the matrix M where the PF operator is equivalent to the system
    // of first order ODEs
    //      (d/dt)(v) = v M(t)   (v = row vector).
    // We work in the LaurentSeriesRing R, because we only need to know the
    // first num_of_steps + 1 coefficients in M in order to prepare the
    // eigenvalues.
    rows_of_matrix:=[PowerSequence(R) | [R | j eq k + 1 select 1 else 0 :
                                  j in [1..PF_degree]] : k in [1..PF_degree-1]];
    denom:=1 / (R ! leading_coefficient);
    Append(~rows_of_matrix,[R | -1 * c * denom :  c in PF_coefficients]);
    mm:=Transpose(Matrix(rows_of_matrix));
    // Now prepare the eigenvalues
    max_eval:=Max(exponents(mm));
    while max_eval ge 1 do
        mm:=prepare(mm,max_eval,-1);
        vprintf Fanosearch: "New exponents: %o\n", exponents(mm);
        max_eval:= Max(exponents(mm));
    end while;
    min_eval:=Min(exponents(mm));
    while min_eval lt 0 do
        mm:=prepare(mm,min_eval,1);
        vprintf Fanosearch: "New exponents: %o\n", exponents(mm);
        min_eval:=Min(exponents(mm));
    end while;
    // Finally, compute the Jordan normal form
    jnf:=JordanForm(Evaluate(mm,0));
    return jnf;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic LocalMonodromyAtPoint( PF::AlgFPElt, a::FldACElt ) -> ModMatFldElt
{The Jordan normal form of the local log-monodromy of the Picard-Fuchs system defined by PF about the point a, where PF is assumed to be an element of the Weyl algebra W<D,t>.}
    // Sanity check
    require Parent(PF) cmpeq WeylAlgebra(): "Argument must be an element of the Weyl algebra W<D,t> defined over the rationals.";
    // Map PF into QQ[t][D]
    PF:=change_ring(PF);
    // Return the monodromy at a
    return local_monodromy_at_point(PF,a);
end intrinsic;

intrinsic RamificationData( PF::AlgFPElt : max_degree := false) -> SeqEnum
{The ramification data of the Picard-Fuchs operator PF. The data is presented in the form [[*f1,M1*],...,[*fk,Mk*]], where the fi are the irreducible factors of the principal symbol of PF, and the Mi are the Jordan normal forms of local log-monodromies about a root (hence about any root) of fi. We also include the local ramification at 0 and infinity where fi = t for zero and fi = Infinity for infinity. PF is assumed to be an element of the Weyl algebra W<D,t>.  The optional parameter max_degree can be set to N to skip local monodromy calculations for all roots defined over number fields of order gt N, thereby assuming that any such are apparent singularities.}
    // Sanity check
    require Parent(PF) cmpeq WeylAlgebra(): "Argument must be an element of the Weyl algebra W<D,t> defined over the rationals.";
    // Map PF into QQ[t][D]
    PF:=change_ring(PF);
    R:=BaseRing(PF);
    // We compute the local monodromy at t=0 and t=any root of the principal
    // symbol of PF
    result:=[];
    // The following is the list of polynomials whose roots we need to consider
    if Evaluate(LeadingCoefficient(PF),0) eq 0 then
        list_of_factors:=[R| f[1] : f in Factorization(LeadingCoefficient(PF))];
    else
        list_of_factors:=[R.1] cat
                         [R| f[1] : f in Factorization(LeadingCoefficient(PF))];
    end if;
    for f in list_of_factors do
	if (not (max_degree cmpeq false)) and Degree(f) gt max_degree then
	    // skip these roots
	    printf "Degree %o: skipping\n", Degree(f);
	    continue;
	end if;
	printf "Degree %o: computing\n", Degree(f);
        vprintf Fanosearch: "Computing monodromy about roots of %o\n",f;
        AC:=AlgebraicClosure();
        root_of_f:=Roots(f,AC: Max:=1)[1][1];
        Include(~result,[* f,local_monodromy_at_point(PF,root_of_f) *]);
    end for;
    // Now add the local ramification at infinity
    max_t_degree:=Max([Degree(c) : c in Coefficients(PF)]);
    t:=R.1;
    D:=Parent(PF).1;
    coefficients_at_infinity:=[R | t^max_t_degree * Evaluate(c, 1 / t) :
                                                         c in Coefficients(PF)];
    PF_at_infinity:=&+[Parent(PF) | coefficients_at_infinity[i] * (-D)^(i - 1) :
                                           i in [1..#coefficients_at_infinity]];
    Include(~result,
           [* Infinity(),local_monodromy_at_point(PF_at_infinity,AlgebraicClosure() ! 0) *]);
    // Return the result
    return result;
end intrinsic;

intrinsic RamificationDefect( PF::AlgFPElt : ramification_data:=false, max_degree:=false) -> SeqEnum
{The ramification defect of the Picard-Fuchs PF, i.e. the quantity ramification(V) - 2 * rank(V) where V is the local system corresponding to PF. If this is zero and V is irreducible then PF is extremal. PF is assumed to be an element of the Weyl algebra W<D,t>.  The optional argument ramification_data can be set equal to the output from RamificationData(), to avoid recomputing the ramification data if it is already known. The optional parameter max_degree can be set to N to skip local monodromy calculations for all roots defined over number fields of order gt N, thereby assuming that any such are apparent singularities.}
    // Sanity checks
    require Parent(PF) cmpeq WeylAlgebra(): "Argument must be an element of the Weyl algebra W<D,t> defined over the rationals.";
    if not ramification_data cmpeq false then
	require max_degree cmpeq false: "The optional parameters ramification_data and max_degree cannot both be set.";
    end if;
    // Compute the ramification data, if necessary
    if ramification_data cmpeq false then
	rd:=RamificationData(PF : max_degree := max_degree);
    else
	rd := ramification_data;
    end if;
    irreducible_factors:=[* xx[1] : xx in rd *];
    log_monodromy_matrices:=[* xx[2] : xx in rd *];
    rk:=NumberOfColumns(log_monodromy_matrices[1]);
    // Recover the coefficient ring
    R:=CoefficientRing(polynomial_ring());
    // Treat the ramification at zero separately
    defect:=-2 * rk;
    for k in [1..#rd] do
        if (irreducible_factors[k] cmpeq R.1) or
           (irreducible_factors[k] cmpeq Infinity()) then
            defect:=defect + Rank(log_monodromy_matrices[k]);
        else
            defect:=defect + Rank(log_monodromy_matrices[k]) *
                                                 Degree(irreducible_factors[k]);
        end if;
    end for;
    // Return the defect
    return defect;
end intrinsic;

intrinsic IsExtremal( PF::AlgFPElt : ramification_data:=false) -> BoolElt
{True iff the Picard-Fuchs operator PF is extremal, where PF is assumed to be an element of the Weyl algebra W<D,t>.  The local system corresponding to PF is assumed to be irreducible.  The optional argument ramification_data can be set equal to the output from RamificationData(), to avoid recomputing the ramification data if it is already known.}
    // Sanity check
    require Parent(PF) cmpeq WeylAlgebra(): "Argument must be an element of the Weyl algebra W<D,t> defined over the rationals.";
    // The defect must be zero
    return RamificationDefect(PF : ramification_data:=ramification_data) eq 0;
end intrinsic;

intrinsic JordanStructure( M:Mtrx ) -> SeqEnum
{Given a matrix M with rational entries, distinct rational eigenvalues e_1,e_2,... in increasing order, and Jordan blocks for e_i of sizes n_(i,1),...,n_(i,k), returns the sequence with i-th entry <e_i,[n_(i,1),...,n_(i,k)]> with the n_(i_j) sorted into increasing order.}
    // Sanity checks
    require NumberOfRows(M) eq NumberOfColumns(M) : "M must be a square matrix";
    bool, M := CanChangeRing(M,Rationals());
    require bool: "M must be a matrix of rational numbers";
    require &and[lambda[1] in Rationals() : lambda in Eigenvalues(ChangeRing(M,AlgebraicClosure()))] : "M must have rational eigenvalues";
    // order the eigenvalues
    eigenvalues := Sort([xx[1] : xx in Eigenvalues(M)]);
    // compute the minimal polynomials for each Jordan block
    block_data := PrimaryInvariantFactors(M);
    results := [];
    for lambda in eigenvalues do
	// grab the polynomials with the appropriate roots
	these_block_data := [xx : xx in block_data | Evaluate(xx[1],lambda) eq 0];
	Append(~results,<lambda,Sort([xx[2] : xx in these_block_data])>);
    end for;
    return results;
end intrinsic;

freeze;

/////////////////////////////////////////////////////////////////////////
// blockwriter.m
/////////////////////////////////////////////////////////////////////////
// Provides intrinsics for creating block files.
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Constants
/////////////////////////////////////////////////////////////////////////

// The default maximum size of the memory cache
DEFAULT_CACHE_SIZE:=5000;

//////////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////////

// Returns true iff the given store is a block writer.
function is_bw(F)
    try
        bool,t:=StoreIsDefined(F,"type");
        success:=bool and t cmpeq "block_writer";
    catch e
        success:=false;
    end try;
    return success;
end function;

// Creates a new block writer file.
procedure bw_new_file(F)
    id:=StoreGet(F,"block_id") + 1;
    dest_dir:=StoreGet(F,"dest_dir");
    name:=Sprintf("%o/%o.txt",dest_dir,id);
    fh:=Open(name,"w");
    StoreSet(F,"block_id",id);
    StoreSet(F,"fh",fh);
    StoreSet(F,"line_num",0);
end procedure;

// Writes the cache to the block writer file.
procedure bw_write_data(F)
    lines_per_file:=StoreGet(F,"lines_per_file");
    cache:=StoreGet(F,"cache");
    while #cache ne 0 do
        fh:=StoreGet(F,"fh");
        line_num:=StoreGet(F,"line_num");
        chunk_size:=Min(#cache,lines_per_file - line_num);
        str:="";
        for i in [1..chunk_size] do
            str cat:= cache[i] cat "\n";
        end for;
        Write(fh,str);
        cache:=cache[chunk_size + 1..#cache];
        line_num +:= chunk_size;
        if line_num eq lines_per_file then
            bw_new_file(F);
        else
            StoreSet(F,"line_num",line_num);
        end if;
    end while;
    StoreSet(F,"cache",[]);
end procedure;

//////////////////////////////////////////////////////////////////////////
// Intrinsics
//////////////////////////////////////////////////////////////////////////

// Initialize the block writer.
intrinsic BWNew( dir::MonStgElt : lines_per_file:=5000,
    initial_block_id:=1 ) -> Rec
{Initialise a new block writer. The destination block directory must be writable. Important: You must call BWFlush when finished with the block writer to ensure that any cached data is written to disk.}
    // Sanity check
    bool,lines_per_file:=IsCoercible(Integers(),lines_per_file);
    require bool and lines_per_file gt 0:
        "Parameter 'lines_per_file' must be a positive integer.";
    bool,initial_block_id:=IsCoercible(Integers(),initial_block_id);
    require bool and initial_block_id ge 0:
        "Parameter 'initial_block_id' must be a non-negative integer.";
    // Create the new data store
    F:=NewStore();
    StoreSet(F,"type","block_writer");
    StoreSet(F,"dest_dir",dir);
    StoreSet(F,"block_id",initial_block_id - 1);
    StoreSet(F,"lines_per_file",lines_per_file);
    StoreSet(F,"cache",[]);
    StoreSet(F,"cache_size",DEFAULT_CACHE_SIZE);
    // Ensure that the directory exists
    cmd:="mkdir -p " cat QuotePath(dir);
    _:=PipeCommand(cmd);
    // Start a new block file
    bw_new_file(F);
    // Return the data
    return F;
end intrinsic;

intrinsic BWWriteLine( F::Rec, S::MonStgElt )
{Writes the line S to the block writer F.}
    // Sanity check
    require is_bw(F): "Argument 1 must be a block writer record.";
    // Fetch the data we need
    cache:=StoreGet(F,"cache");
    cache_size:=StoreGet(F,"cache_size");
    // Add the string to the cache and, if necessary, flush the cache
    Append(~cache,S);
    StoreSet(F,"cache",cache);
    if #cache ge cache_size then bw_write_data(F); end if;
end intrinsic;

intrinsic BWFlush( F::Rec )
{Flushes any cached data in the block writer F to the block files.}
    // Sanity check
    require is_bw(F): "Argument must be a block writer record.";
    // Flush the cache
    bw_write_data(F);
end intrinsic;

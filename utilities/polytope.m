freeze;

/////////////////////////////////////////////////////////////////////////
// polytope.m
/////////////////////////////////////////////////////////////////////////
// Intrinsics for working with polytopes, polyhedra, and toric lattices.
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic IsTerminal(P::TorPol) -> BoolElt
{True iff the Fano polytope P corresponds to a terminal toric variety.}
    require IsFano(P): "Argument must be a Fano polytope.";
    return NumberOfPoints(P) eq NumberOfVertices(P) + 1;
end intrinsic;

intrinsic IsCanonical(P::TorPol) -> BoolElt
{True iff the Fano polytope P corresponds to a canonical toric variety.}
    require IsFano(P): "Argument must be a Fano polytope.";
    return NumberOfInteriorPoints(P) eq 1;
end intrinsic;

intrinsic DefaultToricLattice(n::RngIntElt) -> TorLatElt
{The standard toric lattice of rank n.}
    return Parent(LatticeVector(ZeroSequence(Integers(),n)));
end intrinsic;

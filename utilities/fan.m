freeze;

/////////////////////////////////////////////////////////////////////////
// fan.m
/////////////////////////////////////////////////////////////////////////
// Intrinsics for working with cones and fans.
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

// Alias for IsNonsingular
intrinsic IsSmooth(C::TorCon) -> BoolElt
{True iff the affine variety associated to the cone C is nonsingular.}
    return IsNonsingular(C);
end intrinsic;

// Alias for IsNonsingular
intrinsic IsSmooth(F::TorFan) -> BoolElt
{True iff all cones of the fan F are nonsingular.}
    return IsNonsingular(F);
end intrinsic;

// Alias for IsNonsingular
intrinsic IsSmooth(X::TorVar) -> BoolElt
{True iff X is non-singular.}
    return IsNonsingular(X);
end intrinsic;

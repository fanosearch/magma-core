freeze;

/////////////////////////////////////////////////////////////////////////
// partitions.m
/////////////////////////////////////////////////////////////////////////

// we follow the comment of user rici in [https://stackoverflow.com/questions/30893292/generate-all-partitions-of-a-set]

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Return the partition of S corresponding to p.  Here S is a sequence.
function recover_partition(S, p)
    k := Max(p);
    return {PowerSet(Universe(S))| {Universe(S)| S[i] : i in [1..#p] | p[i] eq j} : j in [1..k] };
end function;


// Calculates the initial partition for the given partitions process P. Returns
// true followed by the partition on success, false otherwise.
function partitions_init(P)
    // Recover the data for this process
    data:=GetUserProcessData(P);
    S:=data[1]; // The set (as a sequence)
    n := #S;
    // Is there anything to do?
    if n eq 0 then
        return false, _;
    end if;
    // Initialise the process
    p := [1 : i in [1..n]];
    m := [1 : i in [1..n]];
    m[1] := 0;
    SetUserProcessData(P,[* S, p , m *]);
    return true, recover_partition(S, p);
end function;

// Advance the given partitions process P. Returns true followed by the next
// partition on success, false otherwise.
function partitions_next(P)
    // Recover the data for this process
    data:=GetUserProcessData(P);
    S := data[1];
    n := #S;
    p := data[2];
    m := data[3];
    // Find the right-most incrementable entry in p
    success := false;
    for i in [n..1 by -1] do
        if p[i] le m[i] then
            success := true;
            idx := i;
            break;
        end if;
    end for;
    // There might not be an incrementable entry
    if not success then
        return false, _;
    end if;
    // The right-most incrementable entry is idx
    // So increment it
    p[idx] +:= 1;
    x := Max(m[idx],p[idx]);
    for i in [idx+1..n] do
        p[i] := 1;
        m[i] := x;
    end for;
    SetUserProcessData(P,[* S, p, m *]);
    return true, recover_partition(S, p);
end function;

// Recursively computes partitions of S. Here T is S as a sequence.
function partitions_recurse(S,T)
    if #S eq 0 then
        return {PowerSet(PowerSet(Universe(S)))|};
    elif #S eq 1 then
        return {PowerSet(PowerSet(Universe(S))) | {S}};
    elif #S eq 2 then
        a,b:=Explode(T);
        return {PowerSet(PowerSet(Universe(S))) | {S}, {{a},{b}}};
    end if;
    a:=T[#T];
    Prune(~T);
    Exclude(~S,a);
    parts:={PowerSet(PowerSet(Universe(S)))|};
    for U in Subsets(S) do
        Ua:=Include(U,a);
        TT:=[Universe(T) | c : c in T | not c in U];
        if #TT eq 0 then
            Include(~parts,{Ua});
        else
            for subparts in $$(S diff U,TT) do
                Include(~parts,Include(subparts,Ua));
            end for;
        end if;
    end for;
    return parts;
end function;

/////////////////////////////////////////////////////////////////////////
// Create a new partitions process
/////////////////////////////////////////////////////////////////////////

intrinsic PartitionsProcess( S::SetEnum ) -> Process
{A process for iterating over partitions of S.}
    S:=SetToSequence(S);
    return UserProcess("Partitions",partitions_init,partitions_next,[* S *]);
end intrinsic;

intrinsic Partitions(S::SetEnum) -> SetEnum
{All partitions of S}
    require not IsNull(S): "illegal empty set";
    return partitions_recurse(S,SetToSequence(S));
end intrinsic;

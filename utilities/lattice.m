freeze;

/////////////////////////////////////////////////////////////////////////
// lattice.m
/////////////////////////////////////////////////////////////////////////
// Intrinsics for working with toric lattice points.
/////////////////////////////////////////////////////////////////////////

// The quadrants, sorted in anti-clockwise order
quadrants:=[[1,0],[1,1],[0,1],[-1,1],[-1,0],[-1,-1],[0,-1],[1,-1]];

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Compares the two points x and y, using the anti-clockwise sort order, with
// the cut taken along the positive x-axis. Returns -1 if x < y, 0 if x == y,
// and +1 if x > y. Assumes that the arguments are two-dimensional toric
// lattice points, contained in the same parent toric lattice.
function cmp(x,y)
    // Get the  case where x == y and the origin out of the way
    if x eq y then
        return 0;
    elif IsZero(x) then
        return -1;
    elif IsZero(y) then
        return 1;
    end if;
    // Perform the comparison
    x:=Eltseq(x);
    y:=Eltseq(y);
    quadx:=[Sign(c) : c in x];
    quady:=[Sign(c) : c in y];
    if quadx ne quady then
        // x and y lie in different quadrants, so this is easy
        return Index(quadrants,quadx) lt Index(quadrants,quady) select
            -1 else 1;
    elif quadx[1] eq 0 then
        // x and y lie on the y-axis
        return Abs(x[2]) lt Abs(y[2]) select -1 else 1;
    elif quadx[2] eq 0 then
        // x and y lie on the x-axis
        return Abs(x[1]) lt Abs(y[1]) select -1 else 1;
    end if;
    // We project x and y onto the line y = 1 and the image for comparison
    projx:=x[1] / x[2];
    projy:=y[1] / y[2];
    if projx eq projy then
        return &+[c^2 : c in x] lt &+[c^2 : c in y] select -1 else 1;
    end if;
    return projx gt projy select -1 else 1;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic AnticlockwiseCmp( x::TorLatElt, y::TorLatElt ) -> RngIntElt
{Compares two 2-dimensional toric lattice points x and y, where x < y if x is in the anti-clockwise direction away from y. The cut is taken to be the positive x-axis. Returns -1 if x < y, 0 if x == y, and +1 if x > y.}
    // Sanity check
    require Parent(x) cmpeq Parent(y):
        "Arguments must lie in the same toric lattice.";
    require Dimension(Parent(x)) eq 2:
        "Arguments must lie in a two-dimensional toric lattice.";
    // Return the comparison value
    return cmp(x,y);
end intrinsic;

intrinsic AnticlockwiseSort( S::SeqEnum[TorLatElt] ) -> SeqEnum[TorLatElt]
{}
    // Sanity check
    if IsNull(S) then return S; end if;
    require Dimension(Universe(S)) eq 2:
        "The sequence must contain two-dimensional toric lattice points.";
    // Sort the points
    return Sort(S,cmp);
end intrinsic;

intrinsic AnticlockwiseSort( ~S::SeqEnum[TorLatElt] )
{Sorts the sequence S of two-dimensional toric lattice points in anti-clockwise order, where the cut is taken to be the positive x-axis.}
    // Sanity check
    if IsNull(S) then return; end if;
    require Dimension(Universe(S)) eq 2:
        "The sequence must contain two-dimensional toric lattice points.";
    // Sort the points
    Sort(~S,cmp);
end intrinsic;

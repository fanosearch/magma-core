freeze;

/////////////////////////////////////////////////////////////////////////
// eseq.m
/////////////////////////////////////////////////////////////////////////
// A collection of utilities for working with sequences, sets, etc.
/////////////////////////////////////////////////////////////////////////

import "strings.m": is_string_integer,is_string_rational;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Recursively converts a set, sequence, etc. to string. 'l' and 'r' should be
// the left- and right-deliminators to use.
function list_to_str(S,l,r)
    if #S eq 0 then
        if #l ne 0 and l[#l] eq " " and #r ne 0 and r[1] eq " " then
            return l cat r[2..#r];
        end if;
        return l cat r;
    end if;
    str:=l;
    for s in S do
        t:=Type(s);
        if t eq RngIntElt then
            str cat:= IntegerToString(s) cat ",";
        elif t eq SeqEnum then
            str cat:= list_to_str(s,"[","]") cat ",";
        elif t eq SetEnum then
            str cat:= list_to_str(s,"{","}") cat ",";
        elif t eq List then
            str cat:= list_to_str(s,"[* "," *]") cat ",";
        elif t eq Tup then
            str cat:= list_to_str(s,"<",">") cat ",";
        else
            str cat:= Sprintf("%o,",s);
        end if;
    end for;
    return str[1..#str - 1] cat r;
end function;

// Given parallel sequences vals and nums, which should be read to mean "vals[i]
// is repeated nums[i] times", decreases the value of nums[idx] by 1. If
// nums[idx] becomes zero, then the corresponding entry in vals and nums is
// removed. Returns the new parallel sequences vals and nums.
function remove_one(vals,nums,idx)
   if nums[idx] ne 1 then
       nums[idx] -:= 1;
   else
       vals:=[Universe(vals) | vals[i] : i in [1..#vals] | i ne idx];
       nums:=[Integers() | nums[i] : i in [1..#nums] | i ne idx];
   end if;
   return vals,nums;
end function;

// The recursive heart of Permutations(S) below. Here "base" is the permutation
// that has so-far been constructed. "vals" and "nums" are parallel sequences,
// where vals contains the distinct elements in S and nums[i] records how many
// occurrences of vals[i] are available to use. Returns the sequence of all
// distinct permutations which start with "base".
function distinct_permutations(base,vals,nums)
   if #vals eq 1 then
       return [base cat [vals[1] : i in [1..nums[1]]]];
   end if;
   results:=[PowerSequence(Universe(vals))|];
   for idx in [1..#vals] do
       newvals,newnums:=remove_one(vals,nums,idx);
       results cat:= $$(Append(base,vals[idx]),newvals,newnums);
   end for;
   return results;
end function;

/*
    Hs:=[ PowerSequence(Integers()) |
            [ 1, 1, 2, 1, 1, 1, 3, 1, 3, 1 ],
            [ 3, 4, 7, 4, 4, 4, 10, 4, 11, 4 ],
            [ 7, 9, 16, 10, 10, 10, 23, 10, 25, 10 ],
            [ 13, 17, 31, 20, 20, 20, 44, 20, 48, 20 ],
            [ 22, 29, 52, 34, 35, 35, 74, 35, 81, 35 ],
            [ 34, 45, 81, 53, 55, 56, 116, 56, 126, 56 ],
            [ 50, 66, 119, 78, 81, 83, 170, 84, 186, 84 ],
            [ 70, 93, 167, 110, 114, 117, 239, 119, 261, 120 ],
            [ 95, 126, 227, 150, 155, 159, 324, 162, 354, 164 ],
            [ 125, 166, 299, 198, 205, 210, 427, 214, 467, 217 ],
            [ 161, 214, 385, 255, 265, 271, 550, 276, 601, 280 ],
            [ 203, 270, 486, 322, 335, 343, 694, 349, 759, 354 ],
            [ 252, 335, 603, 400, 416, 427, 862, 434, 942, 440 ],
            [ 308, 410, 738, 490, 509, 523, 1054, 532, 1152, 539 ],
            [ 372, 495, 891, 592, 615, 632, 1273, 644, 1392, 652 ],
            [ 444, 591, 1064, 707, 735, 755, 1520, 770, 1662, 780 ],
            [ 525, 699, 1258, 836, 870, 893, 1797, 911, 1965, 924 ],
            [ 615, 819, 1474, 980, 1020, 1047, 2106, 1068, 2303, 1084 ],
            [ 715, 952, 1714, 1140, 1186, 1218, 2448, 1242, 2677, 1261 ],
            [ 825, 1099, 1978, 1316, 1369, 1407, 2826, 1434, 3090, 1456 ],
            [ 946, 1260, 2268, 1509, 1570, 1614, 3240, 1645, 3543, 1670 ] ];
    cs:=[ Integers() | 3, 12, 28, 54, 93, 146, 216, 306, 416, 549, 708, 894,
            1110, 1359, 1642, 1962, 2322, 2722, 3165, 3654, 4190 ];
    hyperplane_knapsack(Hs,cs);
    Output:
    0-dimensional polytope with one vertex:
        (0, 2, 0, 0, 0, 0, 0, 0, 0, 1)
*/
// Given a sequence of sequences of integers Hs, all #Hs[i] = d, and a target
// sequence cs of integers such that #cs = #Hs, returns a polyhedron P in a
// d-dimensional ambient such that the lattice points pts in P give a solution
//      &+[pts[i] * H[i] : H in Hs] eq cs[i], i in [1..#cs].
function hyperplane_knapsack(Hs,cs)
    // Note the dimension
    d:=#Hs[1];
    // Move to the graded lattice
    D:=GradedToricLattice(Dual(Parent(LatticeVector(Hs[1]))));
    Hs:=[D | [-cs[i]] cat Hs[i] : i in [1..#Hs]];
    // We start with the positive orthant
    C:=ConeWithInequalities([D.i : i in [2..d + 1]]);
    // Intersect with the hyperplanes as necessary
    for H in Hs do
        if &or[H * r ne 0 : r in MinimalRGenerators(C : algorithm:="FM")] then
            C:=ConeWithInequalities(Inequalities(C) cat [H,-H]);
        end if;
    end for;
    // Finally, return the polyhedron
    return Polyhedron(C);
end function;

// The recursive heart of Knapsack(Q::SetEnum,k,n) below. Q is a sequence of
// integers, n is the target sum, and k is the number of integers used to
// score the target. The integers are taken from Q, with repeats allowed.
function knapsack_with_repeats_recurse(Q,n,k)
    results:={PowerSequence(Integers())|};
    if k ne 1 and #Q gt 0 then
        smallest_sum:=Q[#Q] * (k-1);
        Q:=[a : a in Q | a + smallest_sum le n];
        newQ:=Q;
        for a in Q do
            partial := $$(newQ,n-a,k-1);
            for Wini in partial do
                Include(~results,[a] cat Wini);
            end for;
            Remove(~newQ,1);
        end for;
    elif k eq 1 and n in Q then
        Include(~results,[n]);
    end if;
    return results;
end function;

// The recursive heart of Knapsack(Q::SeqEnum,k,n) below. Q is a sequence of
// integers, Qdist is the sequence of distinct integers of Q sorted in
// decreasing order, n is the target sum, and k is the number of elements of Q
// to use to score the target.
function knapsack_recurse(Q,Qdist,n,k)
    results:={PowerSequence(Integers())|};
    if k ne 1 and #Q ge k then
        smallest_sum:=&+[Integers() | Q[i] : i in [#Q - k + 2..#Q]];
        greatest_sum:=&+[Integers() | Q[i] : i in [1..k - 1]];
        Q:=[Integers() | a : a in Q | (a + smallest_sum le n) and
                                                       (a + greatest_sum ge n)];
        if #Q gt k then
            Qdist:=[Integers() | a : a in Qdist | a in Q];
            for a in Qdist do
                Remove(~Q,Index(Q,a));
                partial:=$$(Q,Qdist,n - a,k - 1);
                Q:=[Integers() | b : b in Q | b ne a];
                for Wini in partial do
                    Include(~results,[Integers() | a] cat Wini);
                end for;
            end for;
        elif #Q eq k and &+Q eq n then
            Include(~results,Q);
        end if;
    elif k eq 1 and n in Q then
        Include(~results,[Integers() | n]);
    end if;
    return results;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic CommaSeparatedStringToIntegerSequence( S::MonStgElt )
    -> SeqEnum[RngIntElt]
{Attempts to converts the string S into a sequence of integers. The elements are assumed by be separated by a comma.}
    // Begin by moving to the first non-white-space character
    idx:=IndexOfNonWhiteSpace(S);
    if idx eq 0 then return [Integers()|]; end if;
    // Is this an opening bracket?
    btype:=Index("[{(<",S[idx]);
    if btype ne 0 then
        idx:=IndexOfNonWhiteSpace(S,idx + 1);
    end if;
    // We're in a position to start parsing the string into a sequence
    vals:=[Integers()|];
    require_value:=false;
    finished:=idx eq 0;
    while not finished do
        bool,next_idx,val:=is_string_integer(S,idx);
        if bool then
            Append(~vals,val);
            idx:=next_idx;
            if idx eq 0 or S[idx] ne "," then
                finished:=true;
            else
                require_value:=true;
                idx +:= 1;
            end if;
        else
            require not require_value: "Unable to parse string";
            finished:=true;
        end if;
    end while;
    // We need to check we've ended up where we expected
    if idx eq 0 then
        require btype eq 0: "Unbalanced or mismatched brackets";
    else
        if btype eq 0 then
            require Index("]})>",S[idx]) eq 0:
                "Unbalanced or mismatched brackets";
            require false: "Unable to parse string";
        end if;
        // We'd better be pointing the the closing bracket, with nothing but
        // white space to our right
        require S[idx] eq "]})>"[btype]: "Unbalanced or mismatched brackets";
        require IndexOfNonWhiteSpace(S,idx + 1) eq 0: "Unable to parse string";
    end if;
    // Return the sequence
    return vals;
end intrinsic;

intrinsic CommaSeparatedStringToRationalSequence( S::MonStgElt )
    -> SeqEnum[FldRatElt]
{Attempts to converts the string S into a sequence of rationals. The elements are assumed by be separated by a comma.}
    // Begin by moving to the first non-white-space character
    idx:=IndexOfNonWhiteSpace(S);
    if idx eq 0 then return [Rationals()|]; end if;
    // Is this an opening bracket?
    btype:=Index("[{(<",S[idx]);
    if btype ne 0 then
        idx:=IndexOfNonWhiteSpace(S,idx + 1);
    end if;
    // We're in a position to start parsing the string into a sequence
    vals:=[Rationals()|];
    require_value:=false;
    finished:=idx eq 0;
    while not finished do
        bool,next_idx,num,den:=is_string_rational(S,idx);
        if bool then
            require den ne 0: "Illegal zero denominator";
            Append(~vals,num / den);
            idx:=next_idx;
            if idx eq 0 or S[idx] ne "," then
                finished:=true;
            else
                require_value:=true;
                idx +:= 1;
            end if;
        else
            require not require_value: "Unable to parse string";
            finished:=true;
        end if;
    end while;
    // We need to check we've ended up where we expected
    if idx eq 0 then
        require btype eq 0: "Unbalanced or mismatched brackets";
    else
        if btype eq 0 then
            require Index("]})>",S[idx]) eq 0:
                "Unbalanced or mismatched brackets";
            require false: "Unable to parse string";
        end if;
        // We'd better be pointing the the closing bracket, with nothing but
        // white space to our right
        require S[idx] eq "]})>"[btype]: "Unbalanced or mismatched brackets";
        require IndexOfNonWhiteSpace(S,idx + 1) eq 0: "Unable to parse string";
    end if;
    // Return the sequence
    return vals;
end intrinsic;

intrinsic ToString( S::SeqEnum ) -> MonStgElt
{}
    return list_to_str(S,"[","]");
end intrinsic;

intrinsic ToString( S::List ) -> MonStgElt
{}
    return list_to_str(S,"[* "," *]");
end intrinsic;

intrinsic ToString( S::Tup ) -> MonStgElt
{}
    return list_to_str(S,"<",">");
end intrinsic;

intrinsic ToString( S::SetEnum ) -> MonStgElt
{A string representation of S, presented in a compact form with unnecessary white space between the elements omitted.}
    return list_to_str(S,"{","}");
end intrinsic;

intrinsic IndexOfFirstUndefined( S::SeqEnum ) -> RngIntElt
{The index of the first undefined element in S, or 0 if all element are defined.}
    for i in [1..#S] do
        if not IsDefined(S,i) then return i; end if;
    end for;
    return 0;
end intrinsic;

intrinsic Sort( S::SetEnum ) -> SeqEnum
{Sort the sequence corresponding to the set S}
    S:=SetToSequence(S);
    Sort(~S);
    return S;
end intrinsic;

intrinsic Sort( S::SetEnum, compare::UserProgram ) -> SeqEnum
{Sort the sequence corresponding to the set S using the given comparison function}
    S:=SetToSequence(S);
    Sort(~S,compare);
    return S;
end intrinsic;

intrinsic Representative( M::List ) -> .
{A representative element of M}
    require #M ne 0: "Argument 1 is not non-empty";
    return M[1];
end intrinsic;

intrinsic Representative( M::Tup ) -> .
{A representative element of M}
    require #M ne 0: "Argument 1 is not non-empty";
    return M[1];
end intrinsic;

intrinsic ColumnSequence( M::Mtrx ) -> SeqEnum
{A sequence of sequences representing columns.}
    return RowSequence(Transpose(M));
end intrinsic;

intrinsic Columns( X::Mtrx ) -> SeqEnum
{The columns of matrix X as a sequence.}
    return Transpose(X)[1..Ncols(X)];
end intrinsic;

intrinsic Columns( X::Mtrx, Q::SeqEnum[RngIntElt] ) -> SeqEnum
{The columns given by Q of X as a sequence.}
    if #Q ne 0 then
        require Min(Q) ge 1 and Max(Q) le Ncols(X):
        Sprintf("Argument 2 must take values in the range [1..%o]\n",Ncols(X));
    end if;
    return Transpose(X)[Q];
end intrinsic;

intrinsic Eltseq( S::SetMulti ) -> SeqEnum
{The elements of S as a sequence.}
    if IsNull(S) then return []; end if;
    return &cat[PowerSequence(Universe(S)) |
        [Universe(S) | s : i in [1..Multiplicity(S,s)]] :
        s in MultisetToSet(S)];
end intrinsic;

intrinsic Permutations( S::SeqEnum ) -> SetEnum
{The set of permutations (as sequences) of elements of S.}
    if #S le 1 then
        return {S};
    end if;
    vals:=SequenceToSet(S);
    if #vals eq 1 then
        return {S};
    elif #vals eq #S then
        return Permutations(vals);
    end if;
    vals:=SetToSequence(vals);
    nums:=[Integers() | #[i : i in [1..#S] | S[i] eq val] : val in vals];
    return SequenceToSet(distinct_permutations([Universe(vals)|],vals,nums));
end intrinsic;

/*
> ToricKnapsack([[2,0,0,0,2],[1,1,0,0,2],[0,0,1,1,2]],[2,2,2,2,8]);
0-dimensional polytope with one vertex:
    (0, 2, 2)
*/
intrinsic ToricKnapsack(Q::SeqEnum[SeqEnum[RngIntElt]],w::SeqEnum[RngIntElt])
  -> TorPol
{The polyhedron P in the positive orthant whose lattice points are the coefficients of linear relations between the Q that achieve w. That is, for each point v in P, &+[v[i] * Q[i] : i in [1..#Q]] eq w.}
    // Get the degenerate case out of the way
	if #Q eq 0 then return EmptyPolyhedron(#w); end if;
	// Sanity check
	d:=#w;
	require d ne 0: "Argument 2 must be a nonempty integer sequence";
	require &and[#H eq d : H in Q]:
        "Argument 1 must be a sequence of integer sequences, each having the same length as argument 2";
	bool,Q:=CanChangeUniverse(Q,PowerSequence(Integers()));
	require bool:
        "Argument 1 must be a sequence of integer sequences, each having the same length as argument 2";
    bool,w:=CanChangeUniverse(w,Integers());
    require bool: "Argument 2 must be a nonempty integer sequence";
    // OK, let the hyperplane knapsack do the work
	return hyperplane_knapsack(ColumnSequence(Matrix(Q)),w);
end intrinsic;

/*
> BooleanKnapsack([[2,0,0,0,2],[1,1,0,0,2],[0,0,1,1,2]],[2,2,2]);
{
    [ false, false, false, false, true ],
    [ true, true, true, true, false ]
}
*/
intrinsic BooleanKnapsack(Q::SeqEnum[SeqEnum[RngIntElt]],w::SeqEnum[RngIntElt])
  -> SetEnum
{Q is a sequence of sequences of integers, with each Q[i] of length d. w is a target sequence of non-negative integers of length equal to Q. Returns a set of all sequences S, where S is a sequence of booleans of length d, such that &+[Q[i][j] : S[j]] eq w[i] for each i in [1..#w].}
    // Sanity check
    require #w eq #Q: "Argument 2 must be of length equal to argument 1.";
    if #Q eq 0 then return {PowerSequence(Booleans())|}; end if;
    d:=#Q[1];
    require &and[#q eq d : q in Q]:
        "Argument 1 must be a sequence of sequences, each having the same length.";
    bool,Q:=CanChangeUniverse(Q,PowerSequence(Integers()));
    require bool: "Argument 1 must be a sequence of sequences of integers.";
    bool,w:=CanChangeUniverse(w,Integers());
    require bool and &and[c ge 0 : c in w]:
        "Argument 2 must be a non-empty sequence of non-negative integers";
    // Begin by counting and removing duplicates
    Q:=ColumnSequence(Matrix(Q));
    distinct:=SetToSequence(SequenceToSet(Q));
    Hs:=ColumnSequence(Matrix(distinct));
    copies:=[#[1 : q in Q | q eq r] : r in distinct];
    d:=#Hs[1];
    // Move to the graded lattice
    D:=GradedToricLattice(Dual(Parent(LatticeVector(Hs[1]))));
    Hs:=[D | [-w[i]] cat Hs[i] : i in [1..#Hs]];
    // We start with the positive orthant
    C:=ConeWithInequalities([D.i : i in [2..d + 1]]);
    // Intersect with the hyperplanes as necessary
    for H in Hs do
        if &or[H * r ne 0 : r in MinimalRGenerators(C : algorithm:="FM")] then
            C:=ConeWithInequalities(Inequalities(C) cat [H,-H]);
        end if;
    end for;
    // Bound the number of copies
    for i in [1..#copies] do
        H:=copies[i] * D.1 - D.(i + 1);
        if &or[H * r lt 0 : r in MinimalRGenerators(C : algorithm:="FM")] then
            C:=ConeWithInequalities(Append(Inequalities(C),H));
        end if;
    end for;
    // Create the polytope
    P:=Polyhedron(C);
    assert IsPolytope(P);
    // Now we need to distribute the solutions over the copies
    poss:=[{Integers() | i : i in [1..#Q] | Q[i] eq q} : q in distinct];
    solns:=[PowerSequence(Booleans())|];
    for v in Points(P) do
        u:=Eltseq(v);
        idxs:=[Subsets(poss[i],u[i]) : i in [1..#u] | u[i] ne 0];
        for I in CartesianProduct(idxs) do
            sol:=[Booleans() | false : i in [1..#Q]];
            for i in &join[I[i] : i in [1..#I]] do
                sol[i]:=true;
            end for;
            Append(~solns,sol);
        end for;
    end for;
    // Return the solutions
    return SequenceToSet(solns);
end intrinsic;

intrinsic Knapsack(Q::SetEnum[RngIntElt],n::RngIntElt,k::RngIntElt) -> SetEnum
{The set of all sequences S of elements taken from Q (where elements may be repeated), distinct up to permutation, such that the sum of the elements in S equals n (i.e. &+S eq n) and the number of elements in S is equal to k (i.e. #S eq k).}
    if #Q eq 0 then return {PowerSequence(Integers())|}; end if;
    Q:=Reverse(Sort(Q));
    return knapsack_with_repeats_recurse(Q,n,k);
end intrinsic;

intrinsic Knapsack(Q::SeqEnum[RngIntElt],n::RngIntElt,k::RngIntElt) -> SetEnum
{The set of all subsequences S of Q, distinct up to permutation, such that the sum of the elements in S equals n (i.e. &+S eq n) and the number of elements in S is equal to k (i.e. #S eq k).}
    if #Q eq 0 then return {PowerSequence(Integers())|}; end if;
    Sort(~Q);
    Reverse(~Q);
    Qdist:=Sort(SequenceToSet(Q));
    Reverse(~Qdist);
    return knapsack_recurse(Q,Qdist,n,k);
end intrinsic;

intrinsic AssociativeArray(keys::SeqEnum, values::List) -> Assoc
{Create the associative array with the given key-value pairs.}
    // Sanity check
    require #keys eq #values: "Argument 1 and argument 2 must be of the same length.";
    // Deal with the null case
    if IsNull(keys) then
        return AssociativeArray();
    end if;
    // Create the associative array
    A:=AssociativeArray(Universe(keys));
    for i in [1..#keys] do
        A[keys[i]]:=values[i];
    end for;
    return A;
end intrinsic;

intrinsic AssociativeArray(I::Str, pairs::Any,...) -> Assoc
{Create the associative array with index universe I and the given key-value pairs. Each element of pairs must be of the form [* key, value *], with key an element of I.}
    // Create the associative array, validating as we go
    A:=AssociativeArray(I);
    for i in [1..#pairs] do
        J:=pairs[i];
        require Type(J) cmpeq List: Sprintf("Argument %o must be a List.",i+1);
        require #J eq 2: Sprintf("Argument %o must be a List of length 2.",i+1);
        k:=J[1];
        require Parent(k) cmpeq I:
            Sprintf("Entry 1 of argument %o must be an element of argument 1.",i+1);
        A[k]:=J[2];
    end for;
    return A;
end intrinsic;

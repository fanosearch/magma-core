freeze;

/////////////////////////////////////////////////////////////////////////
// laurent.m
/////////////////////////////////////////////////////////////////////////
// Intrinsics for working with Laurent polynomials.
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Given a pair (cs,monos) and a (finite) group acting on monos, extends the
// two sequences (by adding points to monos and zeros to cs) until monos
// is closed under the group action. The resulting (cs,monos) pair is parallel
// sorted and returned.
function close_cs(cs,monos,G)
    L:=Universe(monos);
    monos:=[Vector(Eltseq(v)) : v in monos];
    gens:=SequenceToSet(monos);
    while #gens ne 0 do
        m:=Representative(gens);
        for g in G do
            n:=m * g;
            Exclude(~gens,n);
            if not n in monos then
                Append(~monos,n);
                Append(~cs,0);
            end if;
        end for;
    end while;
    ParallelSort(~monos,~cs);
    return cs,[L | Eltseq(v) : v in monos];
end function;

// Parallel sorts the pair (monos,cs), and then returns the resulting
// coefficients.
function reorder_cs(monos,cs)
    ParallelSort(~monos,~cs);
    return cs;
end function;

// Given a sequence S of integers and a function field R, returns the
// corresponding monomial. Note: This assumes that the rank is compatible with
// the length of S.
function seq_to_monomial(R,S)
    m:=R ! 1;
    for i in [1..#S] do
        if S[i] gt 0 then
            m *:= R.i^S[i];
        elif S[i] lt 0 then
            m *:= 1/(R.i^-S[i]);
        end if;
    end for;
    return m;
end function;

// Returns a sequence of coefficients, and a sequence of monomial exponents
// for the given Laurent polynomial.
function coefficients_and_exponents(f)
    coeffs,monos:=CoefficientsAndMonomials(Numerator(f));
    monos:=[PowerSequence(Integers()) | Exponents(m) : m in monos];
    d:=Coefficients(Denominator(f))[1];
    R:=Parent(f);
    K:=CoefficientRing(R);
    if not Denominator(f) in K then
        den:=Exponents(Monomials(Denominator(f))[1]);
        monos:=[PowerSequence(Integers()) |
                      [Integers() | m[i] - den[i] : i in [1..#m]] : m in monos];
    elif Type(R) cmpeq Type(K) and Rank(R) eq Rank(K) then
        // There is an unfortunate issue with Magma casting into K:
        /*
        > K<a,b>:=RationalFunctionField(Rationals(),2);
        > R<x,y>:=RationalFunctionField(K,2);
        > f:=1/(x*y);
        > Denominator(f) in K;
        True
        */
        // To work around this problem, our logic here is that any elements of K
        // in the denominator would have been moved up into the coefficients of
        // the numerator (since K is a rational function field):
        /*
        > f:=1/(3*a*x*y);
        > Denominator(f);
        x*y
        > Numerator(f);
        1/3/a
        */
        // So we can safely assume that the denominator is a monomial in R.
        den:=Exponents(Monomials(Denominator(f))[1]);
        if not IsZero(den) then
            monos:=[PowerSequence(Integers()) |
                      [Integers() | m[i] - den[i] : i in [1..#m]] : m in monos];
        end if;
    end if;
    coeffs:=[FieldOfFractions(Universe(coeffs)) | c / d : c in coeffs];
    return coeffs,monos;
end function;

// laurent_normal_form - Attempts to compute a normal form for the given Laurent
// polynomial f. On success returns true followed by the Newton polytope of
// the normal form, a map (as a matrix) sending f to its normal form, a sequence
// of monomials, and a sequence of coefficients. Otherwise returns false
// followed by an error message. By default the sequence of monomials and
// coefficients returned will be the non-zero values only. If you need the
// complete sequence for every point in Sort(SetToSequence(Points(Q))), then
// set 'return_coeffs' to true.
function laurent_normal_form(f : return_coeffs:=false)
    // Covert f to a Newton polytope
    P,cs:=NewtonPolytope(f : return_coeffs:=return_coeffs);
    if not IsMaximumDimensional(P) then
        return false,"Unable to compute normal form (the Newton polytope must be of maximum dimension).",_,_,_;
    end if;
    // Compute the normal form of P and recover the change of basis
    Qverts,perm:=NormalForm(P);
    H:=Transpose(Solution(Transpose(Matrix(PermuteSequence(Vertices(P),perm))),
                          Transpose(Matrix(Qverts))));
    // Extract the coefficients and monomials, and ensure that they're closed
    // under the action of the automorphism group of P
    G:=AutomorphismGroup(P);
    if return_coeffs then
        Pmonos:=Sort(Points(P));
    else
        cs,Pmonos:=CoefficientsAndExponents(f);
        cs,Pmonos:=close_cs(cs,Pmonos,G);
    end if;
    Pmonos:=Matrix(Pmonos);
    // Work through the automorphism group to find the lex-smallest ordering
    // for the coefficients (w.r.t. the change of basis)
    min_cs:=false;
    M:=false;
    for g in G do
        new_M:=g * H;
        new_cs:=reorder_cs(Rows(Pmonos * new_M),cs);
        if Type(min_cs) eq BoolElt then
            min_cs:=new_cs;
            M:=new_M;
        elif new_cs lt min_cs then
            min_cs:=new_cs;
            M:=new_M;
        end if;
    end for;
    // Now we recover the data needed for the return values
    monos:=Sort(RowSequence(Pmonos * H));
    Q:=Polytope(Qverts : areVertices:=true);
    return true,Q,M,monos,min_cs;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic '^'( f::FldFunRatMElt, M::Mtrx ) -> FldFunRatMElt
{The action of the square matrix M on the exponents of the Laurent polynomial f}
    // Sanity check
    require IsLaurent(f): "Argument 1 must be a Laurent polynomial";
    R:=Parent(f);
    d:=Rank(R);
    require NumberOfRows(M) eq NumberOfColumns(M) and NumberOfRows(M) eq d:
        Sprintf("Argument 2 must be a %o x %o (square) matrix",d,d);
    bool,M:=CanChangeRing(M,Integers());
    require bool: "Argument 2 must have integer entries";
    // Split f into coefficients and exponents, and apply M to the exponents
    cs,ex:=CoefficientsAndExponents(f);
    ex:=[Eltseq(Vector(e) * M) : e in ex];
    ChangeUniverse(~cs,R);
    // Reassemble the Laurent polynomial and return
    return &+[R | cs[i] * seq_to_monomial(R,ex[i]) : i in [1..#cs]];
end intrinsic;

intrinsic PrintLaurent( f::FldFunRatMElt ) -> MonStgElt
{A string representation of the Laurent polynomial f}
    // Sanity check
    require IsLaurent(f): "Argument must be a Laurent polynomial";
    // Extract the coefficients and monomials as parallel sequences
    cs,monos:=CoefficientsAndMonomials(f);
    // Create string representations of the terms
    terms:=[Sprintf("%o",(Numerator(cs[i]) * monos[i]) / Denominator(cs[i])) :
                                                                 i in [1..#cs]];
    // Join the terms
    if #terms eq 0 then return "0"; end if;
    str:=terms[1];
    for i in [2..#terms] do
        if terms[i][1] eq "-" then
            str cat:= " - " cat terms[i][2..#terms[i]];
        else
            str cat:= " + " cat terms[i];
        end if;
    end for;
    // Return the string
    return str;
end intrinsic;

intrinsic IsMonomial( f::RngUPolElt ) -> BoolElt
{True iff f is a monomial}
    return #Terms(f) le 1;
end intrinsic;

intrinsic Monomials( F::FldFunRatMElt ) -> SeqEnum
{The monomials of F as a sequence}
    require IsLaurent(F): "Argument must be a Laurent polynomial";
    _,monos:=CoefficientsAndMonomials(Numerator(F));
    R:=Parent(F);
    K:=CoefficientRing(R);
    if not Denominator(F) in K then
        den:=Monomials(Denominator(F))[1];
        monos:=[R | m / den : m in monos];
    elif Type(R) cmpeq Type(K) and Rank(R) eq Rank(K) then
        den:=Monomials(Denominator(F))[1];
        if not IsZero(den) then
            monos:=[R | m / den : m in monos];
        end if;
    end if;
    return monos;
end intrinsic;

intrinsic CoefficientsAndMonomials( F::RngUPolElt ) -> SeqEnum, SeqEnum
{}
    allms:=Monomials(F);
    allcs:=Coefficients(F);
    ms:=[Universe(allms) | allms[i] : i in [1..#allms] | not IsZero(allcs[i])];
    cs:=[Universe(allcs) | allcs[i] : i in [1..#allcs] | not IsZero(allcs[i])];
    return cs,ms;
end intrinsic;

intrinsic CoefficientsAndMonomials( F::FldFunRatMElt ) -> SeqEnum, SeqEnum
{The coefficients and monomials of F as parallel sequences}
    require IsLaurent(F): "Argument must be a Laurent polynomial";
    coeffs,monos:=CoefficientsAndMonomials(Numerator(F));
    d:=Coefficients(Denominator(F))[1];
    R:=Parent(F);
    K:=CoefficientRing(R);
    if not Denominator(F) in K then
        den:=Monomials(Denominator(F))[1];
        monos:=[R | m / den : m in monos];
    elif Type(R) cmpeq Type(K) and Rank(R) eq Rank(K) then
        den:=Monomials(Denominator(F))[1];
        if not IsZero(den) then
            monos:=[R | m / den : m in monos];
        end if;
    end if;
    coeffs:=[FieldOfFractions(Universe(coeffs)) | c / d : c in coeffs];
    return coeffs,monos;
end intrinsic;

intrinsic CoefficientsAndExponents( F::RngUPolElt ) -> SeqEnum, SeqEnum
{}
    cs,ex:=CoefficientsAndMonomials(F);
    ex:=[PowerSequence(Integers()) | [Degree(t)] : t in ex];
    return cs,ex;
end intrinsic;

intrinsic CoefficientsAndExponents( F::FldFunRatMElt ) -> SeqEnum, SeqEnum
{}
    require IsLaurent(F): "Argument must be a Laurent polynomial";
    cs,ex:=coefficients_and_exponents(F);
    return cs,ex;
end intrinsic;

intrinsic CoefficientsAndExponents( F::GenMPolBGElt ) -> SeqEnum, SeqEnum
{The coefficients and exponents of F as parallel sequences}
    try
        cs,ms:=CoefficientsAndMonomials(F);
    catch e;
        cs:=Coefficients(F);
        ms:=Monomials(F);
    end try;
    try
        ex:=[PowerSequence(Integers()) | Exponents(m) : m in ms];
    catch e;
        ex:=[PowerSequence(Integers()) |
                           [Degree(m,i) : i in [1..Rank(Parent(F))]] : m in ms];
    end try;
    return cs,ex;
end intrinsic;

intrinsic Facets( f::FldFunRatMElt ) -> SeqEnum[FldFunRatMElt]
{The sequence of Laurent polynomials f_i given by restricting f to the i-th facet of the corresponding Newton polytope}
    // Sanity check
    require IsLaurent(f): "The argument must be a Laurent polynomial";
    // Recover the Newton polytope
    P:=NewtonPolytope(f);
    // Build a map from the exponents of f to the coefficients
    cs,ex:=CoefficientsAndExponents(f);
    ChangeUniverse(~ex,Ambient(P));
    A:=AssociativeArray(Universe(ex));
    for i in [1..#ex] do
        A[ex[i]]:=cs[i];
    end for;
    // Restrict f to each facet of P in turn
    R:=Parent(f);
    facet_laurents:=[R|];
    for F in Facets(P) do
        g:=R ! 0;
        for v in Points(F) do
            bool,c:=IsDefined(A,v);
            if bool then
                g +:= c * seq_to_monomial(R,Eltseq(v));
            end if;
        end for;
        Append(~facet_laurents,g);
    end for;
    // Return the restricted Laurent polynomials
    return facet_laurents;
end intrinsic;

intrinsic LaurentPolynomial( ex::SeqEnum[SeqEnum[RngIntElt]], cs::SeqEnum )
    -> FldFunRatMElt
{}
    // Sanity check
    require #cs eq #ex:
        "Arguments 1 and 2 must be sequences of the same length";
    require not IsNull(cs): "Argument 2: Illegal null sequence";
    bool,ex:=CanChangeUniverse(ex,PowerSequence(Integers()));
    require bool:
        "Argument 1 must be a sequence of sequences of integers, each of the same length";
    rk:=#ex eq 0 select 0 else #ex[1];
    require &and[#e eq rk : e in ex]:
        "Argument 1 must be a sequence of sequences of integers, each of the same length";
    // Return the Laurent polynomial
    R:=FieldOfFractions(PolynomialRing(Universe(cs),rk));
    return &+[R | cs[i] * seq_to_monomial(R,ex[i]) : i in [1..#cs]];
end intrinsic;

intrinsic LaurentPolynomial( R::FldFunRat, ex::SeqEnum[SeqEnum[RngIntElt]],
    cs::SeqEnum ) -> FldFunRatMElt
{The Laurent polynomial with coefficients 'cs' and exponents 'ex'}
    // Sanity check
    require #cs eq #ex:
        "Arguments 2 and 3 must be sequences of the same length";
    bool,cs:=CanChangeUniverse(cs,CoefficientRing(R));
    require bool: "Argument 3 is incompatible with the coefficient ring";
    rk:=Rank(R);
    bool,ex:=CanChangeUniverse(ex,PowerSequence(Integers()));
    require bool:
        Sprintf("Argument 2 must be a sequence of sequences of integers, each of length %o",rk);
    require &and[#e eq rk : e in ex]:
        Sprintf("Argument 2 must be a sequence of sequences of integers, each of length %o",rk);
    // Return the Laurent polynomial
    return &+[R | cs[i] * seq_to_monomial(R,ex[i]) : i in [1..#cs]];
end intrinsic;

intrinsic LaurentNormalForm( f::FldFunRatMElt : return_coeffs:=false )
    -> FldFunRatMElt,TorPol,Mtrx,SeqEnum
{A normal form g for the Laurent polynomial f. Also gives the corresponding Newton polytope P, and the matrix M such that f^M = g. By construction, P eq NormalForm(P). Set 'return_coeffs' to true to also return the coefficients associated with the points of P. The coefficients are placed in bijection with Sort(SetToSequence(Points(P))).}
    // Sanity check
    require IsLaurent(f): "Argument must be a Laurent polynomial";
    require Type(return_coeffs) eq BoolElt:
        "Parameter 'return_coeffs' must be a boolean";
    // Calculate the normal form
    bool,Q,M,monos,cs:=laurent_normal_form(f : return_coeffs:=return_coeffs);
    require bool: Q;
    // Do we need to compute all the coefficients?
    if return_coeffs then
        return LaurentPolynomial(Parent(f),monos,cs),Q,M,cs;
    else
        return LaurentPolynomial(Parent(f),monos,cs),Q,M,_;
    end if;
end intrinsic;

intrinsic LaurentAffineNormalForm( f::FldFunRatMElt : return_coeffs:=false )
    -> FldFunRatMElt,TorPol,SeqEnum
{An affine normal form for the Laurent polynomial f. This normal form is unique up to multiplication of f by a monomial, and change of basis. Also gives the corresponding Newton polytope P. By construction, P eq AffineNormalForm(P). Set 'return_coeffs' to true to also return the coefficients associated with the points of P. The coefficients are placed in bijection with Sort(SetToSequence(Points(P))).}
    // Sanity check
    require IsLaurent(f): "Argument must be a Laurent polynomial";
    require Type(return_coeffs) eq BoolElt:
        "Parameter 'return_coeffs' must be a boolean";
    // Extract the Newton polytope
    P,cs:=NewtonPolytope(f : return_coeffs:=return_coeffs);
    require IsMaximumDimensional(P): "Unable to compute normal form (the Newton polytope must be of maximum dimension)";
    // Compute the affine normal form of P and recover the transformation
    Qverts,perm:=AffineNormalForm(P);
    assert IsZero(Qverts[1]);
    u:=Vertices(P)[1^perm];
    vtrans:=[v - u : v in Vertices(P)];
    H:=Transpose(Solution(Transpose(Matrix(PermuteSequence(vtrans,perm))),
                          Transpose(Matrix(Qverts))));
    // Map the coefficients and monomials to the affine normal form
    if return_coeffs then
        monos:=Sort(Points(P));
    else
        cs,monos:=CoefficientsAndExponents(f);
        ChangeUniverse(~monos,Ambient(P));
    end if;
    monos:=[PowerSequence(Integers()) | Eltseq((v - u) * H) : v in monos];
    // Now we embed Q in L x Z at height 1 and take closure under Aut(Q)
    Q:=Polytope([Append(Eltseq(v),1) : v in Qverts] : areVertices:=true);
    G:=AutomorphismGroup(Q);
    pts:=[PowerSequence(Integers()) | Append(v,1) : v in monos];
    if not return_coeffs then
        cs,pts:=close_cs(cs,pts,G);
        monos:=[PowerSequence(Integers()) | Prune(Eltseq(pt)) : pt in pts];
    end if;
    pts:=Matrix(pts);
    // Work through the automorphism group to find the lex-smallest ordering
    // for the coefficients (w.r.t. the change of basis)
    cs:=Min([reorder_cs(Rows(pts * g),cs) : g in G]);
    // Return the data
    Q:=Polytope(Qverts : areVertices:=true);
    if return_coeffs then
        return LaurentPolynomial(Parent(f),monos,cs),Q,cs;
    else
        return LaurentPolynomial(Parent(f),monos,cs),Q,_;
    end if;
end intrinsic;

intrinsic LaurentAutomorphismGroup( f::FldFunRatMElt ) -> GrpMat
{The automorphism group of the Laurent polynomial f}
    // Sanity check
    require IsLaurent(f): "Argument must be a Laurent polynomial";
    // Extract the Newton polytope P and corresponding automorphism group G
    P:=NewtonPolytope(f : return_coeffs:=false);
    G:=AutomorphismGroup(P);
    // Ensure that the monomials and coefficients are closed under G, and then
    // convert the monomials to a matrix
    cs,monos:=CoefficientsAndExponents(f);
    cs,monos:=close_cs(cs,monos,G);
    M:=Matrix(monos);
    // We build the automorphism group of f by restriction of G
    autf:=sub<G | Identity(G)>;
    for g in G do
        if not g in autf then
            newcs:=cs;
            newpts:=RowSequence(M * g);
            ParallelSort(~newpts,~newcs);
            if newcs eq cs then
                autf:=sub<G | autf,g>;
            end if;
        end if;
    end for;
    // Return the automorphism group
    return autf;
end intrinsic;

intrinsic ConstantTerm( f::FldFunRatMElt ) -> FldRatElt
    {The constant term of the Laurent polynomial f}
    // Sanity check
    require IsLaurent(f): "Argument must be a Laurent polynomial";
    // Extract the coefficient of the zero exponent
    cs, es := CoefficientsAndExponents(f);
    zero := [0 : i in [1..Rank(Parent(f))]];
    idx := Index(es, zero);
    if idx eq 0 then
	return 0;
    else
	return cs[idx];
    end if;
end intrinsic;

intrinsic Face(f::FldFunRatMElt, w::TorLatElt) -> FldFunRatElt, FldRatElt
{The Laurent polynomial supported on the face of Newton polytope of f that of minimum height with respect to w. Also returns that minimum height.}
    // Sanity checks
    require IsLaurent(f): "Argument 1 must be a Laurent polynomial";
    require not IsZero(f): "Argument 1 must be non-zero";
    require not IsZero(w): "Argument 2 must be non-zero";
    require Dimension(Parent(w)) eq Rank(Parent(f)): "Argument 2 must lie in a lattice of the same dimension as the rank of the ring that contains argument 1.";
    // Find the heights of the exponents
    L := Dual(Parent(w));
    cs, es := CoefficientsAndExponents(f);
    ChangeUniverse(~es, L);
    hts := [w*e : e in es];
    min_ht := Min(hts);
    idxs := [i : i in [1..#hts] | hts[i] eq min_ht];
    // Rebuild the Laurent polynomial
    g := LaurentPolynomial(Parent(f), [Eltseq(es[i]) : i in idxs], [cs[i] : i in idxs]);
    return g, min_ht;
end intrinsic;

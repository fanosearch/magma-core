freeze;

/////////////////////////////////////////////////////////////////////////
// strings.m
/////////////////////////////////////////////////////////////////////////
// A collection of useful string utilities.
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Local constants
/////////////////////////////////////////////////////////////////////////

// A string containing the digits 0-9
numbers:="1234567890";

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Attempts to chomp as much of the string 'S', starting at index 'idx',
// into an integer. Returns true if something exists to be converted, false
// otherwise. In either case, the index of the first character that will not
// form part of an integer is returned as the second value. If the end of the
// string was reached, this index is 0. When true, the third return value is
// the integer.
function is_string_integer(S,idx)
    // Skip over any white space
    idx:=IndexOfNonWhiteSpace(S,idx);
    if idx eq 0 then return false,0,_; end if;
    start:=idx;
    // The first value is allowed to be a negative sign, and this is allowed
    // to have white-space after it
    if S[idx] eq "-" then
        new_idx:=IndexOfNonWhiteSpace(S,idx + 1);
        if new_idx eq 0 or Index(numbers,S[new_idx]) eq 0 then
            return false,idx,_;
        end if;
        idx:=new_idx;
    // Otherwise the first value must be an integer
    elif Index(numbers,S[idx]) eq 0 then
        return false,idx,_;
    end if;
    // We loop until we stop encountering numbers
    n:=#S;
    while idx + 1 le n do
        idx +:= 1;
        if Index(numbers,S[idx]) eq 0 then
            return true,IndexOfNonWhiteSpace(S,idx),
                        StringToInteger(S[start..idx - 1]);
        end if;
    end while;
    // If we're here then we successfully parsed to the end of the string
    return true,0,StringToInteger(S[start..#S]);
end function;

// Attempts to chomp as much of the string 'S', starting at index 'idx',
// into a rational. Returns true if something exists to be converted, false
// otherwise. In either case, the index of the first character that will not
// form part of a rational is returned as the second value. If the end of the
// string was reached, this index is 0. When true, the third return value is
// the numerator and the fourth is the denominator (which may be zero!).
function is_string_rational(S,idx)
    // First we attempt to read in a numerator
    bool,next_idx,num:=is_string_integer(S,idx);
    if not bool then return false,next_idx,_,_; end if;
    // Is there a denominator?
    if next_idx eq 0 or S[next_idx] ne "/" then
        return true,next_idx,num,1;
    end if;
    idx:=next_idx;
    bool,next_idx,den:=is_string_integer(S,idx + 1);
    if not bool then return true,idx,num,1; end if;
    return true,next_idx,num,den;
end function;

// Given a polynomial f and power k, returns a string representation of f^k.
// This will bracket f as appropriate for a component of the string representing
// a factorisation.
function factor_to_string(f,k)
	if k eq 1 then
		if f eq Parent(f).1 then
			return Sprintf("%o",f);
		end if;
		return Sprintf("(%o)",f);
	elif f eq Parent(f).1 then
		return Sprintf("%o^%o",f,k);
	end if;
	return Sprintf("(%o)^%o",f,k);
end function;

// Given a polynomial f and power k, returns a string representation of f^k.
function one_factor(c,f,k)
	if c eq 1 then
		if k eq 1 then
			return Sprintf("%o",f);
		elif f eq Parent(f).1 then
			return Sprintf("%o^%o",f,k);
		end if;
		return Sprintf("(%o)^%o",f,k);
	elif c eq -1 then
		if k eq 1 then
			if f eq Parent(f).1 then
				return Sprintf("-%o",f);
			end if;
			return Sprintf("-(%o)",f);
		elif f eq Parent(f).1 then
			return Sprintf("-%o^%o",f,k);
		end if;
		return Sprintf("-(%o)^%o",f,k);
	end if;
	return Sprintf("%o*%o",c,factor_to_string(f,k));
end function;

// Given a string representation of an ideal over the rationals of rank n,
// returns the ideal.
function string_to_ideal(S,n,name)
	// Sanity check that this is a sequence
	S:=Trim(S);
	if S[1] ne "[" or S[#S] ne "]" then
		return false,"Expected a sequence of generators for the ideal";
	end if;
	// Sanity check for characters that could indicate dodgy business)
	if Index(S,";") ne 0 or Index(S,"%") ne 0 or Index(S,"!") ne 0 then
		return false,"The string contains an illegal character";
	end if;
	// Remove all white space
	S:=SubstituteString(S,"\n","");
	S:=SubstituteString(S," ","");
	// Parse the generators
	genidx:=0;
	len:=#name;
	R:=n eq 1 select PolynomialRing(Rationals())
				else PolynomialRing(Rationals(),n);
	gens:=[R|];
	for L in Split(S[2..#S - 1],",") do
		genidx +:= 1;
		// Sanity check on the generator
		idx:=Index(L,name);
		while idx ne 0 do
			if idx gt #L - len - 1 or L[idx + len] ne "." then
				return false,Sprintf("Unable to parse generator %o",genidx);
			end if;
			endidx:=idx + len + 1;
			endfound:=false;
			while not endfound and endidx le #L do
				if Index(numbers,L[endidx]) eq 0 then
					endfound:=true;
				else
					endidx +:= 1;
				end if;
			end while;
			if endidx le idx + len + 1 then
				return false,Sprintf("Unable to parse generator %o",genidx);
			end if;
			k:=StringToInteger(L[idx + len + 1..endidx - 1]);
			if k gt n then
				return false,Sprintf("Expected an ideal of rank %o, but found rank %o",n,k);
			end if;
			idx:=Index(L,name,endidx);
		end while;
		// Attempt to evaluate the generator
		try
			gen:=eval "R ! " cat SubstituteString(L,name,"R");
		catch e;
			return false,Sprintf("Unable to parse generator %o",genidx);
		end try;
		Append(~gens,gen);
	end for;
	// Return the ideal
	I:=ideal<R | gens>;
	return true,I;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic GetVersionString() -> MonStgElt
{The current version of Magma, in the form x.y-z}
    return Sprintf("%o.%o-%o",a,b,c) where a,b,c:=GetVersion();
end intrinsic;

intrinsic IsStringInteger( S::MonStgElt ) -> BoolElt,RngIntElt
{True iff the given string S could be successfully parsed into an integer. If true, also returns the value of the integer.}
    bool,idx,val:=is_string_integer(S,1);
    if not bool or idx ne 0 then return false,_; end if;
    return true,val;
end intrinsic;

intrinsic IsStringRational( S::MonStgElt ) -> BoolElt,FldRatElt
{True iff the given string S could be successfully parsed into a rational. If true, also returns the value of the rational.}
    bool,idx,num,den:=is_string_rational(S,1);
    if not bool or idx ne 0 or den eq 0 then return false,_; end if;
    return true,num / den;
end intrinsic;

intrinsic FactorizationToString( f::RngUPolElt ) -> MonStgElt
{}
	return FactorisationToString(f);
end intrinsic;

intrinsic FactorisationToString( f::RngUPolElt ) -> MonStgElt
{A string representation of the factorization of f into irreducible factors}
	// Get the zero polynomial out of the way
	if IsZero(f) then
		return "0";
	end if;
	// Factorise the polynomial
	F,c:=Factorisation(f);
	// If there's only one factor, we need to handle this seperately
	if #F eq 1 then
		return one_factor(c,F[1][1],F[1][2]);
	end if;
	// Build the coefficient string
	if c eq 1 then
		S:="";
	elif c eq -1 then
		S:="-";
	else
		S:=Sprintf("%o*",c);
	end if;
	// Convert the factors into strings and return
	return S cat Join([factor_to_string(I[1],I[2]) : I in F],"*");
end intrinsic;

intrinsic StringToIdeal( S::MonStgElt, n::RngIntElt : name:="$" ) -> RngMPol
{The ideal I defined over the rationals of rank n given by the string S. Here S is assumed to describe a sequence of generators of I, with the indeterminants appearing in the form "$.k" (where "$" can be modified using the parameter 'name').}
	// Sanity check
	require n gt 0: "Argument 2 must be a positive integer";
	require Type(name) eq MonStgElt:
		"Parameter 'name' must be a non-empty string";
	name:=Trim(name);
	require #name ne 0: "Parameter 'name' must be a non-empty string";
	// Attempt to convert the string to an ideal
	bool,I:=string_to_ideal(S,n,name);
	require bool: I;
	return I;
end intrinsic;

intrinsic StringToBoolean( s::MonStgElt ) -> BoolElt
{The boolean corresponding to s}
    t:=ToLower(Trim(s));
	if t eq "true" or t eq "t" or t eq "yes" or t eq "y" or t eq "1" then
        return true;
    elif t eq "false" or t eq "f" or t eq "no" or t eq "n" or t eq "0" then
        return false;
    end if;
    require false: Sprintf("String \"%o\" does not represent a boolean value",s);
end intrinsic;

intrinsic ToString(A::Assoc) -> MonStgElt
{A string representation of the associative array A}
    if #A eq 0 then
        return "";
    end if;
    S:=[];
    for k in Sort(Keys(A)) do
		v:=A[k];
		if Type(v) eq MonStgElt then
			Append(~S,Sprintf("%o: \"%o\"",k,v));
		else
			Append(~S,Sprintf("%o: %o",k,v));
		end if;
    end for;
    return Join(S,"\n");
end intrinsic;

intrinsic ToString(n::RngIntElt) -> MonStgElt
{A string representation of the integer n.}
	return IntegerToString(n);
end intrinsic;

intrinsic ToString(r::FldRatElt) -> MonStgElt
{A string representation of the rational number r.}
	return Sprintf("%o", r);
end intrinsic;

intrinsic ToString(s::MonStgElt) -> MonStgElt
{The string s.}
	return s;
end intrinsic;


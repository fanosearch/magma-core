freeze;

/////////////////////////////////////////////////////////////////////////
// regression.m
/////////////////////////////////////////////////////////////////////////
// Intrinsics for linear regression
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////


intrinsic LinearRegression(x::SeqEnum[SeqEnum[FldReElt]], y::SeqEnum[FldReElt]) -> SeqEnum[FldReElt], FldReElt
{The least-squares coefficients for sequences x = [ x_1, ..., x_N ] and y = [ y_1, ..., y_N ], where x_i = [ x_i1, ..., x_ir ]. Returns [ beta_1, ..., beta_r ] and rsq where y_i ~= beta_1 x_i1 + ... + beta_r x_ir and rsq is the residual (R^2 coefficient) for the least squares fit.}
    // Sanity checks
    require #x eq #y: "Arguments 1 and 2 should have the same length";
    require #x gt 0: "Arguments 1 and 2 should be non-empty";
    x_repr := Representative(x);
    RR := Universe(x_repr);
    require #x_repr gt 0: "Argument 1 must contain only non-empty sequences";
    require &and[#z eq #x_repr : z in x]: "Argument 1 must contain sequences of the same length";
    require &and[Universe(z) cmpeq RR : z in x]: "Argument 1 must contain sequences with the same universe";
    require Universe(y) cmpeq RR: "Argument 2 must have the same universe as the sequences in argument 1";
    // Turn x and y into matrices
    X := Matrix(RR, x);
    y := Matrix(RR, #y, 1, y);
    // Find the least-squares coefficients
    XT := Transpose(X);
    beta := Inverse(XT*X)*XT*y;
    delete XT;
    // Compute r^2
    ymXb := y-X*beta;
    delete X;
    rss := Transpose(ymXb)*(ymXb);
    delete ymXb;
    mean := &+[ y[i,1] : i in [1..NumberOfRows(y)] ]/NumberOfRows(y);
    tot := &+[ (y[i,1] - mean)^2 : i in [1..NumberOfRows(y)] ];
    rsq := 1 - rss[1,1]/tot;
    // Return the coefficients and the r-squared
    return Eltseq(beta), rsq;
end intrinsic;

intrinsic LinearRegression(x::SeqEnum[FldReElt], y::SeqEnum[FldReElt]) -> FldReElt, FldReElt, FldReElt
{The least-squares coefficients for sequences x = [ x_1, ..., x_N ] and y = [ y_1, ..., y_N ]. Returns m, c, rsq where y_i ~= m x_i + c and rsq is the residual (R^2 coefficient) for the least squares fit.}
    require #x eq #y: "Arguments 1 and 2 should have the same length";
    require #x gt 0: "Arguments 1 and 2 should be non-empty";
    require Universe(x) cmpeq Universe(y): "Arguments 1 and 2 should have the same universe";
    beta, rsq := LinearRegression([[z,1] : z in x], y);
    return beta[1], beta[2], rsq;
end intrinsic;

freeze;

/////////////////////////////////////////////////////////////////////////
// newton.m
/////////////////////////////////////////////////////////////////////////
// Intrinsics for working with Newton polytopes.
/////////////////////////////////////////////////////////////////////////

import "patch.m": get_jmol;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic ViewWithJmol( f::FldFunRatMElt :
                        path:=get_jmol(),
                        title:="",
                        background:=[255,255,255],
                        transparency:=4,
                        face_color:=[255,215,0],
                        edge_color:=[50,50,50],
                        edge_width:=3,
                        point_color:=[255,0,0],
                        point_size:=10,
                        vertex_color:=[255,0,0],
                        vertex_size:=10,
                        open_in_background:=false )
{View the Newton polytope of the Laurent polynomial f using Jmol. The dimension of the ambient lattice must be at most three. The lattice points in the Newton polytope are labelled by the coefficients of f.}
    // Sanity checks
    require IsLaurent(f): "Argument must be a Laurent polynomial";
    require Rank(Parent(f)) le 3: "The Laurent polynomial must be in 3 or fewer variables";
    require not InternalIsPC(): "Operation not supported on Windows";
    P:=NewtonPolytope(f);
    // Create the point labels
    cs,es:=CoefficientsAndExponents(f);
    point_labels:=ZeroSequence(Universe(cs),NumberOfPoints(P));
    sorted_points:=Sort(SetToSequence(Points(P)));
    for i in [1..#cs] do
    	idx:=Index(sorted_points,es[i]);
    	if idx ne 0 then
    	    point_labels[idx]:=cs[i];
    	end if;
    end for;
    // View the Newton polytope
    ViewWithJmol( P : path:=path,
                      title:=title,
                      background:=background,
                      transparency:=transparency,
                      face_color:=face_color,
                      edge_color:=edge_color,
                      edge_width:=edge_width,
                      point_color:=point_color,
                      point_size:=point_size,
                      point_labels:=point_labels,
                      vertex_color:=vertex_color,
                      vertex_size:=vertex_size,
                      open_in_background:=open_in_background );
end intrinsic;

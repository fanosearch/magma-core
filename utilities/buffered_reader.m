freeze;

/////////////////////////////////////////////////////////////////////////
// buffered_reader.m
/////////////////////////////////////////////////////////////////////////
// Provides a buffered line-by-line reader to simplify parsing files.
/////////////////////////////////////////////////////////////////////////
/* Example:
> infile:="/path/to/my/file";
> F:=NewBufferedReader(infile);
> while not IsEmpty(F) do
>   L:=Gets(F);
>   // Parse the next line...
>   ...
>   // Push some unparsed data back to the reader
>   Ungets(F,L);
> end while;
*/

declare type BufRdr;
declare attributes BufRdr:
    path,       // The path to the input file
    fh,         // The file pointer
    buf,        // The line buffer
    eof;        // Are we at EOF?

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Returns true along with a file pointer to the requested file upon success,
// false and an error string otherwise.
function open_file(F)
    try
        fh:=Open(F,"r");
    catch e
        // The real work is to create a sensible error message
        err:="";
        if assigned e`Object and Type(e`Object) eq MonStgElt then
            idx:=Index(e`Object,":");
            if idx gt 0 then
                err:=e`Object[idx + 1..#e`Object];
                err:=SubstituteString(err,"\\\n","");
                err:=SubstituteString(err,"\n"," ");
                err:=Trim(err);
                err:=SubstituteString(err,"  "," ");
                if #err ne 0 and err[#err] eq "." then Prune(~err); end if;
            end if;
        end if;
        if #err eq 0 then
            err := "Unable to open file " cat F;
        end if;
        return false, err;
    end try;
    return true,fh;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic NewBufferedReader(path::MonStgElt) -> BufRdr
{Create a new buffered reader for the given file}
    // Sanity check
    path:=Trim(path);
    require #path gt 0: "The path cannot be empty.";
    // Open the file for reading
    bool,fh:=open_file(path);
    require bool: fh;
    // Create and return the buffered reader
    F:=New(BufRdr);
    F`path:=path;
    F`fh:=fh;
    F`buf:=[];
    F`eof:=false;
    return F;
end intrinsic;

intrinsic Path(F::BufRdr) -> MonStgElt
{The path to the file wrapped by the buffered reader}
    return F`path;
end intrinsic;

intrinsic Print(F::BufRdr)
{The description of the buffered reader F}
    printf "%o",Path(F);
end intrinsic;

intrinsic IsEmpty(F::BufRdr) -> BoolElt
{True iff the given file buffer is empty}
    if #F`buf ne 0 then
        return false;
    elif Type(F`eof) ne BoolElt then
        return true;
    end if;
    S:=Gets(F`fh);
    if IsEof(S) then
        F`eof:=S;
        return true;
    end if;
    Append(~F`buf,S);
    return false    ;
end intrinsic;

intrinsic Gets(F::BufRdr) -> MonStgElt
{Get and return one more line from the buffered reader F (or EOF marker); the newline is not included}
    n:=#F`buf;
    if n ne 0 then
        S:=F`buf[1];
        F`buf:=n eq 1 select [] else F`buf[2..n];
        return S;
    elif Type(F`eof) ne BoolElt then
        return F`eof;
    end if;
    S:=Gets(F`fh);
    if IsEof(S) then
        F`eof:=S;
    end if;
    return S;
end intrinsic;

intrinsic Ungets(F::BufRdr,L::MonStgElt)
{Pushes the given line L back onto the buffered reader F. The next call to Gets will return the line L.}
    F`buf:=[L] cat F`buf;
    F`eof:=false;
end intrinsic;

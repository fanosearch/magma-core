freeze;

/////////////////////////////////////////////////////////////////////////
// subsets.m
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Subsets process functions
/////////////////////////////////////////////////////////////////////////

// Calculates the initial subset for the given subset process P. Returns true
// followed by the subset on success, false otherwise.
function ss_initial_value(P)
    // Recover the data for this process
    data:=GetUserProcessData(P);
    S:=data[1]; // The set (as a sequence)
    k:=data[2]; // The size of the subsets
    // Is there anything to do?
    if k lt 0 or k gt #S then return false,_; end if;
    // Decide on our approach
    if Binomial(#S,k) lt 2000000 then
        // We let Subsets do the work
        subsets:=SetToSequence(Subsets(SequenceToSet(S),k));
        SS:=subsets[#subsets];
        Prune(~subsets);
        SetUserProcessData(P,[* true,subsets *]);
    else
        // This is too large to use Subsets, so we branch
        subS:=SequenceToSet(S[2..#S]);
        childP1:=SubsetsProcess(subS,k);
        childP2:=SubsetsProcess(subS,k - 1);
        // Get the first result from our children
        if IsEmpty(childP1) then
            if IsEmpty(childP2) then
                return false,_; // Actually this really shouldn't happen
            end if;
            SS:=Current(childP2);
            Include(~SS,S[1]);
            Advance(~childP2);
            SetUserProcessData(P,[* false,S[1],childP2 *]);
        else
            SS:=Current(childP1);
            Advance(~childP1);
            SetUserProcessData(P,[* false,S[1],childP1,childP2 *]);
        end if;
    end if;
    return true,SS;
end function;

// Calculate the next subset for the given subset process P. Returns true
// followed by the subset on success, false otherwise.
function ss_next_value(P)
    // Recover the data for this process
    data:=GetUserProcessData(P);
    // Are we simply a light-weight wrapper around Subsets?
    if data[1] then
        // We already own all the subsets S, as computed by Subsets
        subsets:=data[2];
        if #subsets eq 0 then return false,_; end if;
        SS:=subsets[#subsets];
        Prune(~subsets);
        SetUserProcessData(P,[* true,subsets *]);
    else
        // We need to call our child process
        childP:=data[3];
        if #data eq 4 then
            if IsEmpty(childP) then
                childP:=data[4];
                if IsEmpty(childP) then return false,_; end if;
                SS:=Current(childP);
                Include(~SS,data[2]);
                Advance(~childP);
                SetUserProcessData(P,[* false,data[2],childP *]);
            else
                SS:=Current(childP);
                Advance(~childP);
            end if;
        else
            if IsEmpty(childP) then return false,_; end if;
            SS:=Current(childP);
            Include(~SS,data[2]);
            Advance(~childP);
        end if;
    end if;
    return true,SS;
end function;

/////////////////////////////////////////////////////////////////////////
// Create a new subset process
/////////////////////////////////////////////////////////////////////////

intrinsic SubsetsProcess( S::SetEnum, k::RngIntElt ) -> Process
{A process for iterating over the k-element subsets of S.}
    require k ge 0: "Argument 2 should be a non-negative integer";
    S:=SetToSequence(S);
    return UserProcess("Subsets",ss_initial_value,ss_next_value,[* S,k *]);
end intrinsic;

freeze;

cache:=NewStore();

// Returns a (cached) polynomial ring over the given coefficient ring, in the given number of variables, using the given monomial order.
// Here the coefficient ring must be one of QQ, ZZ, or FF_p.
function get_polynomial_ring(K,n,order)
    // First we recover a key for the coefficient ring
    if K cmpeq Rationals() then
	coeff_key:="Q";
    elif K cmpeq Integers() then
	coeff_key:="Z";
    elif Type(K) eq FldFin then
	coeff_key:="F_" cat IntegerToString(#K);
    end if;
    // Now make the key for this polynomial ring
    if n eq 1 then
	key:=coeff_key cat ":1:any";
    else
	key:=coeff_key cat ":" cat IntegerToString(n) cat ":" cat order;
    end if;
    // Is this polynomial ring in the cache?
    bool,R:=StoreIsDefined(cache,key);
    if not bool then
	R:=n eq 1 select PolynomialRing(K) else PolynomialRing(K,n,order);
	StoreSet(cache,key,R);
    end if;
    return R;
end function;

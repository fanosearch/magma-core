freeze;

/////////////////////////////////////////////////////////////////////////
// graphs.m
/////////////////////////////////////////////////////////////////////////
// Utility intrinsics for working with graphs.
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic Eltseq( E::GrphEdge ) -> SeqEnum[RngIntElt]
{A sequence of indices of vertices describing the edge E.}
    return [Integers() | Index(InitialVertex(E)),Index(TerminalVertex(E))];
end intrinsic;

intrinsic IsEdge( u::GrphVert, v::GrphVert ) -> BoolElt, GrphEdge
{True iff there exists an edge starting at vertex u and terminating at vertex v. If true, the edge is given as the second return value.}
    // Sanity check
    G:=ParentGraph(u);
    require G cmpeq ParentGraph(v):
        "The vertices must come from the same graph";
    // Are these vertices connected by an edge?
    if u adj v then
        if ISA(Type(G),GrphMult) then
            m:=EdgeMultiplicity(u,v);
            E:=IsDirected(G) select EdgeSet(G) ! <[u,v],m>
                               else EdgeSet(G) ! <{u,v},m>;
        else
            E:=IsDirected(G) select EdgeSet(G) ! [u,v]
                               else EdgeSet(G) ! {u,v};
        end if;
        return true,E;
    else
        return false,_;
    end if;
end intrinsic;

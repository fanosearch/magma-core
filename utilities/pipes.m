freeze;

/////////////////////////////////////////////////////////////////////////
// pipes.m
/////////////////////////////////////////////////////////////////////////
// A collection of useful command-line interfaces.
/////////////////////////////////////////////////////////////////////////

import "../core.m": save_data, fetch_data;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic QuotePath( path::MonStgElt ) -> MonStgElt
{Quotes the given path. Normally this will simply escape any quotes in the path and encase it with quotation marks. However, if the initial character is a tilde then it tries to get it correct.}
    path:=EscapeString(path);
    if #path gt 0 and path[1] eq "~" then
        idx:=Index(path,"/");
        if idx eq 0 then idx:=#path; end if;
        initial:=path[1..idx];
        final:=path[idx + 1..#path];
        initial:=SubstituteString(initial," ","\\\ ");
        initial:=SubstituteString(initial,";","\\\;");
        initial:=SubstituteString(initial,"&","\\\&");
        initial:=SubstituteString(initial,"|","\\\|");
        if #final gt 0 then
            return initial cat "\"" cat final cat "\"";
        else
            return initial;
        end if;
    else
        return "\"" cat path cat "\"";
    end if;
end intrinsic;

intrinsic EscapeString( str::MonStgElt ) -> MonStgElt
{Escapes the characters ", `, \, and $ in the given string}
    str:=SubstituteString(str,"\\","\\\\");
    str:=SubstituteString(str,"\"","\\\"");
    str:=SubstituteString(str,"`","\\\`");
    str:=SubstituteString(str,"$","\\\$");
    return str;
end intrinsic;

intrinsic PipeCommand( C::MonStgElt ) -> BoolElt,MonStgElt
{Given a shell command line C, executes C and returns true followed by the output on success, false otherwise.}
    if InternalIsPC() then return false,_; end if;
    try
        res:=Pipe(C cat " 2>&1 || echo \"_123FAILED\"","");
        res:=Trim(res);
    catch e;
        res:="_123FAILED";
    end try;
    if Index(res,"_123FAILED") ne 0 then
        return false,_;
    else
        return true,res;
    end if;
end intrinsic;

intrinsic Sleep( n::RngIntElt )
{Sleep for n seconds}
    require n ge 0: "Argument must be a non-negative integer";
    require not InternalIsPC(): "Operation not supported on Windows";
    if n gt 0 then
        _:=PipeCommand("sleep " cat IntegerToString(n));
    end if;
end intrinsic;

intrinsic Date( format::MonStgElt ) -> MonStgElt
{The current date and time, where 'format' is the display format used by the UNIX date command}
    require #format ne 0: "Illegal empty format string";
    require not InternalIsPC(): "Operation not supported on Windows";
    bool,date:=PipeCommand("date \"+" cat EscapeString(format) cat "\"");
    return bool select date else "";
end intrinsic;

intrinsic Date() -> MonStgElt
{The current date and time}
    require not InternalIsPC(): "Operation not supported on Windows";
    bool,date:=PipeCommand("date");
    return bool select date else "";
end intrinsic;

intrinsic SetHostname( hostname::MonStgElt )
{Set the hostname for this machine}
    save_data("hostname",Trim(hostname));
end intrinsic;

intrinsic Hostname() -> MonStgElt
{The hostname assigned to this machine}
    bool,hostname:=fetch_data("hostname");
    if not bool then
        bool,hostname:=PipeCommand("hostname");
        if not bool then hostname:=""; end if;
        if #hostname gt 0 then save_data("hostname",hostname); end if;
    end if;
    return hostname;
end intrinsic;

intrinsic SetCurlPath( path::MonStgElt )
{Set the path for curl. This can also be specified via the environment variable CURL_PATH.}
    path:=Trim(path);
    require #path ne 0: "Illegal empty string";
    save_data("curl",path);
end intrinsic;

intrinsic CurlPath() -> MonStgElt
{The path for curl}
    bool,curl:=fetch_data("curl");
    if not bool then
        curl:=Trim(GetEnv("CURL_PATH"));
        if #curl eq 0 then curl:="curl"; end if;
        save_data("curl",curl);
    end if;
    return curl;
end intrinsic;

intrinsic Curl( url::MonStgElt : username:="", password:="", data:="",
                                 contenttype:="" ) -> MonStgElt
{Send a curl request to the given url}
    // Sanity check
    require not InternalIsPC(): "Operation not supported on Windows";
    require #url ne 0: "The url must not be empty";
    require Type(data) eq MonStgElt: "The 'data' must be a string";
    require Type(username) eq MonStgElt: "The 'username' must be a string";
    require Type(password) eq MonStgElt: "The 'password' must be a string";
    if Position(password, ":") ne 0 then
        "WARNING: Your password contains a colon -- no idea what will happen!";
	end if;
    // Construct the command
    cmd:=CurlPath() cat
         " -sS --compressed --stderr /dev/null -X POST";
    if #contenttype gt 0 then
        cmd cat:= " -H \"Content-Type: " cat EscapeString(contenttype) cat "\"";
    end if;
    if #data gt 0 then
        cmd cat:= " --data \"" cat EscapeString(data) cat "\"";
    end if;
    if #username gt 0 or #password gt 0 then
        cmd cat:= " -u \"" cat EscapeString(username) cat ":" cat
                               EscapeString(password) cat "\"";
    end if;
    cmd cat:= " " cat url;
    // Execute command
    return Pipe(cmd,"");
end intrinsic;

intrinsic IsLocalhost( address::MonStgElt ) -> BoolElt
{True iff the given address looks like the localhost's address}
    // Is this easy?
    address:=Trim(address);
    if address eq "127.0.0.1" or address eq "localhost" then
        return true;
    end if;
    // Resolve the given address if we can
    bool,ip:=PipeCommand("host \"" cat EscapeString(address) cat "\"");
    if bool then
        ip:=Trim(ip);
        idx:=Index(ip,"address");
        ip:=idx eq 0 select address else ip[idx + 8..#ip];
    else
        ip:=address;
    end if;
    // Get the IP address(es) of the localhost
    ips:=["127.0.0.1"];
    bool,listing:=PipeCommand("host `hostname`");
    if bool then
        for res in Split(listing) do
            idx:=Index(res,"address");
            if idx ne 0 then
                Append(~ips,Trim(res[idx + 8..#res]));
            end if;
        end for;
    end if;
    // Is the ip address a local one?
    return ip in ips;
end intrinsic;

intrinsic ListDir( dir::MonStgElt ) -> SeqEnum[MonStgElt]
{A sequence containing the names of the files and directories in the given directory.}
    bool,res:=PipeCommand("ls " cat QuotePath(dir));
    require bool: "Unable to fetch directory listing.";
    return Split(res);
end intrinsic;

intrinsic ListDir() -> SeqEnum[MonStgElt]
{A sequence containing the names of the files and directories in the working directory.}
    bool,res:=PipeCommand("ls");
    require bool: "Unable to fetch directory listing.";
    return Split(res);
end intrinsic;

intrinsic Replace(F::MonStgElt, S::MonStgElt)
{Writes the string S to the file F. If F already exists then its contents will be overwritten.}
    Write(F,S : Overwrite:=true);
    // There is a small "gotcha" here -- Magma insists on adding a newline to
    // the end of S. For example, if S:="bar" then four bytes will be written:
    // the three bytes of S followed by '\n'. I don't know how to work around
    // this.
end intrinsic;

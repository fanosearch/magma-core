freeze;

/////////////////////////////////////////////////////////////////////////
// gzip.m
/////////////////////////////////////////////////////////////////////////
// Interfaces to the gzip and gunzip command line functions.
/////////////////////////////////////////////////////////////////////////

import "../core.m": save_data, fetch_data;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Fetches the path to the 'gzip' command line function. This can be specified
// via the GZIP_PATH environment variable. Defaults to 'gzip'.
function gzip_path()
    bool,gzip:=fetch_data("gzip");
    if not bool then
        gzip:=Trim(GetEnv("GZIP_PATH"));
        if #gzip eq 0 then gzip:="gzip"; end if;
        save_data("gzip",gzip);
    end if;
    return gzip;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic GzipFile( path::MonStgElt : level:=6 )
{}
    // Sanity check
    require not InternalIsPC(): "Operation not supported on Windows";
    require #path ne 0: "The path must not be empty";
    require Type(level) eq RngIntElt and level ge 1 and level le 9:
        "Parameter 'level' must be an integer in the range 1..9";
    // Construct the command
    cmd:=gzip_path() cat " -q" cat IntegerToString(level) cat " " cat
         QuotePath(path);
    // Execute the command
    require PipeCommand(cmd): "An error occurred whilst compressing the file";
end intrinsic;

intrinsic GzipFile( path::MonStgElt, dest::MonStgElt : level:=6 )
{Compresses the file using gzip. Unless a destination path is given, the file is replaced by one with the extension .gz, whilst keeping the same ownership modes, access, and modification times when possible. Only regular files will be compressed; in particular, symbolic links will be ignored. Optional parameter 'level' can be used to specify the compression level. This should be an integer between 1 (fastest/less compression) and 9 (slowest/best compression). The default compression level is 6.}
    // Sanity check
    require not InternalIsPC(): "Operation not supported on Windows";
    require #path ne 0: "The path must not be empty";
    require #dest ne 0: "The destination path must not be empty";
    require Type(level) eq RngIntElt and level ge 1 and level le 9:
        "Parameter 'level' must be an integer in the range 1..9";
    // Construct the command
    cmd:=gzip_path() cat " -qc" cat IntegerToString(level) cat " " cat
         QuotePath(path) cat " > " cat QuotePath(dest);
    // Execute the command
    require PipeCommand(cmd): "An error occurred whilst compressing the file";
end intrinsic;

intrinsic GunzipFile( path::MonStgElt )
{}
    // Sanity check
    require not InternalIsPC(): "Operation not supported on Windows";
    require #path ne 0: "The path must not be empty";
    // Construct the command
    cmd:=gzip_path() cat " -qd " cat QuotePath(path);
    // Execute the command
    require PipeCommand(cmd): "An error occurred whilst decompressing the file";
end intrinsic;

intrinsic GunzipFile( path::MonStgElt, dest::MonStgElt )
{Decompresses the file using gunzip. Unless a destination path is given, the archive will be replaced with the decompressed file, whilst keeping the same ownership modes, access, and modification times when possible.}
    // Sanity check
    require not InternalIsPC(): "Operation not supported on Windows";
    require #path ne 0: "The path must not be empty";
    require #dest ne 0: "The destination path must not be empty";
    // Construct the command
    cmd:=gzip_path() cat " -qdc " cat QuotePath(path) cat " > " cat
         QuotePath(dest);
    // Execute the command
    require PipeCommand(cmd): "An error occurred whilst decompressing the file";
end intrinsic;

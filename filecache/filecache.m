freeze;

/////////////////////////////////////////////////////////////////////////
// filecache.m
/////////////////////////////////////////////////////////////////////////
// Provides intrinsics for working with file-based set data.
/////////////////////////////////////////////////////////////////////////

// The record defining a file cache
fc_rec:=recformat<
    type: MonStgElt,               // A flag to indicate this is a file cache
    dir: MonStgElt,                // The directory for the bucket files
    uid: MonStgElt,                // The unique id for this file cache
    creation: MonStgElt,           // The creation date
    version: SeqEnum,              // The version number
	num_buckets: Integers(),       // The number of buckets
    max_size: Integers(),          // The maximum number of elements to cache
	hash: Program,                 // The hash function (if defined)
	to_string: Program,            // The "to string" function (if defined)
	from_string: Program >;        // The "from string" function (if defined)

/////////////////////////////////////////////////////////////////////////
// Constants
/////////////////////////////////////////////////////////////////////////

// The default number of buckets
DEFAULT_NUM_BUCKETS:=9973;

// The default maximum size of the memory cache
DEFAULT_CACHE_SIZE:=10000;

// The salt used for validating the uids -- note that changing this will prevent
// existing uids from validating!
UID_SALT:="jxwydmzg";

/////////////////////////////////////////////////////////////////////////
// Data store
/////////////////////////////////////////////////////////////////////////

// The file cache data store. Do NOT access directly!
fc_store:=NewStore();

// Returns the file cache data for the given file cache.
function fc_get_cache(fc)
    bool,cache:=StoreIsDefined(fc_store,fc`uid);
    return bool select cache else AssociativeArray(Integers());
end function;

// Updates the file cache data for the given file cache.
procedure fc_set_cache(fc,cache)
    StoreSet(fc_store,fc`uid,cache);
end procedure;

// Clears the file cache data for the given file cache.
procedure fc_clear_cache(fc)
    StoreRemove(fc_store,fc`uid);
end procedure;

/////////////////////////////////////////////////////////////////////////
// Helper functions
/////////////////////////////////////////////////////////////////////////

// Reformats the error message.
function reformat_error(e)
    err:=Sprintf("%o",e`Object);
    idx:=Index(err,":");
    if idx ne 0 then err:=err[idx + 1..#err]; end if;
    err:=Trim(err);
    err:=SubstituteString(err,"\\\n","");
    err:=SubstituteString(err,"\n"," ");
    err:=Trim(err);
    while Index(err,"  ") ne 0 do
        err:=SubstituteString(err,"  "," ");
    end while;
    return err;
end function;

// Returns true followed by the version sequence [major,minor,patch] on success,
// false followed by an error message otherwise.
function version_string_to_sequence(str)
    idx1:=Index(str,".");
    idx2:=Index(str,"-");
    if idx1 eq 0 or idx2 eq 0 or idx1 gt idx2 then
        return false,"Incorrect format";
    end if;
    major:=str[1..idx1 - 1];
    minor:=str[idx1 + 1..idx2 - 1];
    patch:=str[idx2 + 1..#str];
    bool,major:=IsStringInteger(major);
    if not bool then
        return false,"Unable to extract major version number";
    end if;
    if major ne 2 then
        return false,"Invalid major version number";
    end if;
    bool,minor:=IsStringInteger(minor);
    if not bool then
        return false,"Unable to extract minor version number";
    end if;
    if minor lt 18 then
        return false,"The minimum valid version is V2.18-4";
    end if;
    bool,patch:=IsStringInteger(patch);
    if not bool then
        return false,"Unable to extract patch version number";
    end if;
    if minor eq 18 and patch lt 4 then
        return false,"The minimum valid version is V2.18-4";
    end if;
    return true,[major,minor,patch];
end function;

// Converts the given non-negative integer to base 16 and returns the result
// as a string.
function convert_base16(n)
    error if n lt 0, "Argument must be a non-negative integer";
    if n eq 0 then return "0"; end if;
    i:=1;
    res:=[];
    S:="0123456789abcdef";
    while n ne 0 do
        x:=n mod 16;
        Append(~res,S[x + 1]);
        n:=(n - x) div 16;
    end while;
    Reverse(~res);
    return &cat res;
end function;

// Generates a uid for the given directory.
function generate_uid(dir)
    // Generate a string of base 16 values
    str:=convert_base16(StringToInteger(Date("%s"))) cat
         convert_base16(Abs(Hash(dir))) cat
         convert_base16(Getpid());
    // Padding if necessary, and split the string into four blocks of length 8,
    // where the final block is a hash
    while #str lt 24 do
        str cat:= convert_base16(Random(0,2^30));
    end while;
    str:=str[1..8] cat "-" cat str[9..16] cat "-" cat str[17..24];
    hash:=convert_base16(Abs(Hash(UID_SALT cat str)));
    while #hash lt 8 do
        hash:="0" cat hash;
    end while;
    return str cat "-" cat hash[1..8];
end function;

// Validates the uid. Returns true on success, false otherwise.
function validate_uid(uid)
    // Validate the format of the string
    if #uid ne 35 then return false; end if;
    hex:=&cat["[0-9a-f]" : i in [1..8]];
    R:="^" cat hex cat "-" cat hex cat "-" cat hex cat "-" cat hex cat "$";
    if not Regexp(R,uid) then return false; end if;
    // Now validate the hash
    hash:=convert_base16(Abs(Hash(UID_SALT cat uid[1..26])));
    while #hash lt 8 do
        hash:="0" cat hash;
    end while;
    return hash eq uid[28..#uid];
end function;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Returns the indices of the elements in S that change.
function step_indices(S)
    steps:=[1];
    val:=S[1];
    for i in [2..#S] do
        newval:=S[i];
        if newval ne val then
            Append(~steps,i);
            val:=newval;
        end if;
    end for;
    return steps;
end function;

// Returns true iff the given object is a file cache record.
function is_fc(x)
    return Type(x) cmpeq Rec and "type" in Names(x) and assigned x`type and
        x`type cmpeq "filecache";
end function;

// Attempts to read (or write) the settings from the settings file. Returns true
// followed by an associative array of settings on success, false followed by an
// error message otherwise.
function fc_settings(dir,numBuckets)
    // Ensure that the directory exists
    cmd:="mkdir -p " cat QuotePath(dir);
    _:=PipeCommand(cmd);
    // Open the settings file
    file:=dir cat "/settings.txt";
    try
        fh:=Open(file,"a+");
        Seek(fh,0,0);
        success:=true;
    catch e
        // The hard part is working out why we're here -- if its because the
        // directory doesn't exist, we should tell the user
        err:=reformat_error(e);
        if Index(err,"(No such file") eq 0 then
            err:=Sprintf("Unable to open settings file: %o",reformat_error(e));
        else
            err:=Sprintf("Unable to write to the directory %o (try creating the directory from the command line first)",dir);
        end if;
        success:=false;
    end try;
    if not success then return false,err; end if;
    // First we read any existing data in from the file
    A:=AssociativeArray();
    l:=1;
    S:=Gets(fh);
    while not IsEof(S) do
        // Split this line into key and value
        idx:=Index(S,":");
        if idx eq 0 then
            delete fh;
            return false,Sprintf("The settings file is corrupted (file %o, line %o).",file,l);
        end if;
        key:=StringToLower(Trim(S[1..idx - 1]));
        value:=Trim(S[idx + 1..#S]);
        // Process and cache the data
        if key eq "uid" then
            // Check that the uid validates
            value:=StringToLower(value);
            if not validate_uid(value) then
                delete fh;
                return false,Sprintf("The uid in the settings file does not validate (file %o, line %o).",file,l);
            end if;
        elif key eq "num_buckets" then
            // Convert the value into a (positive) integer
            bool,value:=IsStringInteger(value);
            if not bool or value le 0 then
                delete fh;
                return false,Sprintf("The value of %o in the settings file should be a positive integer (file %o, line %o).",key,file,l);
            end if;
        elif key eq "version" then
            // Convert the version string into a sequence
            bool,value:=version_string_to_sequence(value);
            if not bool then
                delete fh;
                return false,Sprintf("The version in the settings file does not validate (file %o, line %o): %o",key,file,l,value);
            end if;
        end if;
        A[key]:=value;
        // Move on
        S:=Gets(fh);
        l +:= 1;
    end while;
    // If we read nothing at all, that's fine -- it just means that this is a
    // new file cache
    if #Keys(A) eq 0 then
        // Set the data we need
        A["uid"]:=generate_uid(dir);
        A["creation"]:=Date("%Y-%m-%d %H:%M:%S");
        A["version"]:=[a,b,c] where a,b,c:=GetVersion();
        A["num_buckets"]:=numBuckets cmpeq false select DEFAULT_NUM_BUCKETS
                                                   else numBuckets;
        // Write the settings to the file
        for key in Keys(A) do
            if key eq "version" then
                Puts(fh,Sprintf("%o: %o",key,GetVersionString()));
            else
                Puts(fh,Sprintf("%o: %o",key,A[key]));
            end if;
        end for;
    end if;
    // Close the settings file
    delete fh;
    // Verify that the settings we require exist
    for key in {"uid","num_buckets","version","creation"} do
        if not key in Keys(A) then
            return false,Sprintf("The settings file is corrupted (file %o). The entry for %o is missing.",file,key);
        end if;
    end for;
    // Verify that the value of numBuckets is compatible with the settings
    if not numBuckets cmpeq false and numBuckets ne A["num_buckets"] then
        return false,Sprintf("The number of buckets must be set to %o (or left unset)",A["num_buckets"]);
    end if;
    // Return the settings
    return true,A;
end function;

// Returns true followed by the hash value for the given object, or false
// followed by an error message.
function fc_hash(fc,x)
    // Recover the hash function
    hash:=assigned fc`hash select fc`hash else Hash;
    // Calculate the hash value
	try
		val:=hash(x);
		success:=true;
	catch e
		err:=Sprintf("Error calling hash function: %o",reformat_error(e));
        success:=false;
	end try;
    if not success then return false,err; end if;
    if not Type(val) cmpeq RngIntElt then
        return false,Sprintf("Error calling hash function:\nExpected return type RngIntElt but got type %o.",Type(val));
    end if;
    // Reduce the hash value modulo the bucket size and return
	return true,(val mod fc`num_buckets);
end function;

// Returns true followed by a sequence of hash values for the objects in the
// given sequence, or false followed by an error message.
function fc_hashes_for_seq(fc,S)
    // Recover the hash function
    hash:=assigned fc`hash select fc`hash else Hash;
    // Calculate the hash values
    num_buckets:=fc`num_buckets;
	try
        hashes:=[Integers() | hash(x) mod num_buckets : x in S];
		success:=true;
	catch e
		err:=Sprintf("Error calling hash function: %o",reformat_error(e));
        success:=false;
	end try;
    if not success then return false,err; end if;
    // Return the sequence of (reduced) hashes
	return true,hashes;
end function;

// Returns the path to the bucket file for the given hash.
function fc_bucket_file(fc,hashval)
    return Sprintf("%o/%o.txt",fc`dir,hashval);
end function;

// Returns true,true,_ if the end of file has been reached; true,false,
// and the next object in the file; otherwise returns false,error message,_.
function fc_from_bucket_file(fh,from_string)
    // Fetch the next row from the file
    S:=Gets(fh);
    if IsEof(S) then return true,true,_; end if;
    // Determine how many additional lines we need to fetch
    idx:=Index(S,":");
    if idx le 1 then return false,"The file is corrupted.",_; end if;
    try
        n:=StringToInteger(S[1..idx - 1]);
        success:=true;
    catch e
        success:=false;
    end try;
    if not success then return false,"The file is corrupted.",_; end if;
    // Read in the additional lines
    str:=S[idx + 1..#S];
    for i in [2..n] do
        S:=Gets(fh);
        if IsEof(S) then return false,"Unexpected end of file.",_; end if;
        str cat:= S cat "\n";
    end for;
    // Convert the string into an object
    try
        if from_string cmpeq false then
            x:=eval str;
        else
            x:=from_string(str);
        end if;
        success:=true;
    catch e
        err:=Sprintf("Error converting string to object: %o",reformat_error(e));
        success:=false;
    end try;
    if not success then return false,err,_; end if;
    // Return the object
    return true,false,x;
end function;

// Opens the indicated bucket file for reading. Returns true followed by the
// file handle on success, false followed by false if the bucket file doesn't
// exist, and false followed by an error message otherwise.
function fc_open_bucket_file_read(fc,hashval)
    // Open the bucket file with a read lock to prevent editing whilst we're
    // working with it
    try
        fh:=Open(fc_bucket_file(fc,hashval),"r");
        success:=true;
    catch e
        // The hard part if working out why we're here -- if its just because
        // the file doesn't exist, that's OK.
        err:=reformat_error(e);
        fatal:=Index(err,"(No such file") eq 0;
        success:=false;
    end try;
    if not success then
        if fatal then
            return false,Sprintf("Error opening bucket file: %o",err);
        else
            return false,false;
        end if;
    end if;
    // Looks good
    return true,fh;
end function;

// Determines whether the object x, with given hash value, is in the
// corresponding bucket file. Returns true followed by the result on success,
// false followed by an error message otherwise.
function fc_is_in_bucket_file(fc,x,hashval)
    // Recover the "from string" function
    from_string:=assigned fc`from_string select fc`from_string else false;
    // Open the bucket file for reading
    bool,fh:=fc_open_bucket_file_read(fc,hashval);
    if not bool then
        if Type(fh) eq BoolElt then
            return true,false;
        end if;
        return false,fh;
    end if;
    // Start working through the bucket file looking for a match
    repeat
        // Fetch the next object from the file
        bool,is_eof,xx:=fc_from_bucket_file(fh,from_string);
        if not bool then
            delete fh;
            return false,is_eof;
        end if;
        // Is this the object we're looking for?
        if not is_eof and xx cmpeq x then
            delete fh;
            return true,true;
        end if;
    until is_eof;
    // If we're here then the object isn't in the bucket file
    delete fh;
    return true,false;
end function;

// Attempts to load the given bucket file. Returns true followed by the bucket
// on success, false followed by an error message otherwise.
function fc_load_bucket(fc,hashval)
    // Recover the "from string" function
    from_string:=assigned fc`from_string select fc`from_string else false;
    // Open the bucket file for reading
    bool,fh:=fc_open_bucket_file_read(fc,hashval);
    if not bool then
        if Type(fh) eq BoolElt then
            return true,{};
        end if;
        return false,fh;
    end if;
    // Start importing the data from the bucket file
    bucket:={};
    repeat
        // Fetch the next object from the file
        bool,is_eof,x:=fc_from_bucket_file(fh,from_string);
        if not bool then
            delete fh;
            return false,is_eof;
        end if;
        if not is_eof then
            Include(~bucket,x);
        end if;
    until is_eof;
    // Close the file and return the bucket
    delete fh;
    return true,bucket;
end function;

// Outputs the bucket to the bucket file. Returns true on success, false
// followed by an error message otherwise.
function fc_bucket_to_file(fc,bucket,hashval)
    // Recover the "to string" and "from string" functions
    to_string:=assigned fc`to_string select fc`to_string else false;
    from_string:=assigned fc`from_string select fc`from_string else false;
    // Open the bucket file with a write lock to prevent editing whilst we're
    // working with it
    try
        fh:=Open(fc_bucket_file(fc,hashval),"a+");
        Seek(fh,0,0);
        success:=true;
    catch e
        err:=Sprintf("Error opening bucket file: %o",reformat_error(e));
        success:=false;
    end try;
    if not success then return false,err; end if;
    // Start working through the bucket file to remove duplicates
    repeat
        // Fetch the next object from the file
        bool,is_eof,x:=fc_from_bucket_file(fh,from_string);
        if not bool then
            delete fh;
            return false,x;
        end if;
        // Does the object lie in the bucket cache? If so, remove it
        if not is_eof and x in bucket then
            Exclude(~bucket,x);
            // Is there anything to do?
            if #bucket eq 0 then
                delete fh;
                return true,_;
            end if;
        end if;
    until is_eof;
    // Start outputting the objects
    for x in bucket do
        // Convert the object to a string
        if to_string cmpeq false then
            str:=Sprintf("%m",x);
        else
            try
                str:=to_string(x);
                success:=true;
            catch e
                err:=Sprintf("Error converting object to string: %o",
                                                             reformat_error(e));
                success:=false;
            end try;
            if not success then
                delete fh;
                return false,err;
            end if;
            if not Type(str) cmpeq MonStgElt then
                delete fh;
                return false,Sprintf("Error converting object to string:\nExpected return type MonStgElt but got type %o.",Type(str));
            end if;
        end if;
        // Write the string to the file
        try
            n:=#Split(str);
            Puts(fh,IntegerToString(n) cat ":" cat str);
            success:=true;
        catch e
            err:=Sprintf("Error writing object to file: %o",reformat_error(e));
            success:=false;
        end try;
        if not success then
            delete fh;
            return false,err;
        end if;
    end for;
    // Close the file and return success
    delete fh;
    return true,_;
end function;

// Adds the given object to the correct hash bucket.
procedure fc_add_to_hash(fc,x,hashval)
	// Recover the current cache
	cache:=fc_get_cache(fc);
	// Add the object
	bool,S:=IsDefined(cache,hashval);
	if bool then
        Include(~S,x);
	else
		S:={x};
	end if;
	cache[hashval]:=S;
	// Update the data
    fc_set_cache(fc,cache);
    // If the cache has grown too large, flush it
    size:=&+[Integers() | #cache[k] : k in Keys(cache)];
    if size gt fc`max_size then FCFlush(fc); end if;
end procedure;

// Adds the objects in the given set to the given hash bucket.
procedure fc_join_to_hash(fc,X,hashval)
	// Recover the current cache
	cache:=fc_get_cache(fc);
	// Add the objects
	bool,S:=IsDefined(cache,hashval);
	if bool then
        S join:= X;
	else
		S:=X;
	end if;
	cache[hashval]:=S;
	// Update the data
    fc_set_cache(fc,cache);
    // If the cache has grown too large, flush it
    size:=&+[Integers() | #cache[k] : k in Keys(cache)];
    if size gt fc`max_size then FCFlush(fc); end if;
end procedure;

/////////////////////////////////////////////////////////////////////////
// Process functions
/////////////////////////////////////////////////////////////////////////

// Returns true iff the bucket file process has finished.
function pbfc_is_empty(info)
    return info[1];
end function;

// Increments the bucket file process.
procedure pbfc_advance(~info)
    // Sanity check
    error if info[1], "The process has finished.";
    // Extract the data we need
    fh:=info[2];
    bucket:=info[4];
    // Have we finished walking through the file?
    if fh cmpeq false then
        // Fetch the next object from the bucket
        if #bucket eq 0 then
            info:=<true>;
        else
            x:=Representative(bucket);
            Exclude(~bucket,x);
            info:=<false,false,false,bucket,x,info[6] + 1>;
        end if;
    else
        // Fetch the next object from the file
        bool,is_eof,x:=fc_from_bucket_file(fh,info[3]);
        error if not bool, x;
        // Have we reached the end of the file?
        if is_eof then
            if #bucket eq 0 then
                info:=<true>;
            else
                x:=Representative(bucket);
                Exclude(~bucket,x);
                info:=<false,false,false,bucket,x,info[6] + 1>;
            end if;
        else
            Exclude(~bucket,x);
            info:=<false,fh,info[3],bucket,x,info[6] + 1>;
        end if;
    end if;
end procedure;

// Returns the bucket file's current object.
function pbfc_current_value(info)
    error if info[1], "The process has finished.";
    return info[5];
end function;

// Returns a label for the bucket file's current object.
function pbfc_current_label(info)
    error if info[1], "The process has finished.";
    return info[6];
end function;

// Returns true followed by a bucket file process for the given hash value; true
// followed by false if there is no bucket file for that hash value (or the file
// is empty); false followed by an error message otherwise.
function pbfc_create_process(fc,hashval,bucket)
    // Recover the "from string" function
    from_string:=assigned fc`from_string select fc`from_string else false;
    // Open the bucket file for reading
    bool,fh:=fc_open_bucket_file_read(fc,hashval);
    if not bool then
        if Type(fh) ne BoolElt then
            return false,fh;
        end if;
        is_eof:=true;
    else
        bool,is_eof,x:=fc_from_bucket_file(fh,from_string);
        if not bool then
            delete fh;
            return false,is_eof;
        end if;
    end if;
    // If we're at the end of the file we consult the bucket
    if is_eof then
        if #bucket eq 0 then return true,false; end if;
        x:=Representative(bucket);
        Exclude(~bucket,x);
        info:=<false,false,false,bucket,x,1>;
    else
        Exclude(~bucket,x);
        info:=<false,fh,from_string,bucket,x,1>;
    end if;
    // Return the new process
    return true,InternalCreateProcess("Bucket File",info,pbfc_is_empty,
                            pbfc_advance,pbfc_current_value,pbfc_current_label);
end function;

// Returns true iff the cache file process has finished.
function pfc_is_empty(info)
    return info[1];
end function;

// Increments the cache file process.
procedure pfc_advance(~info)
    // Sanity check
    error if info[1], "The process has finished.";
    // Recover the data we need
    fc:=info[2];
    hashval:=info[3];
    P:=info[4];
    cache:=fc_get_cache(fc);
    // Attempt to advance the bucket file process
    if not IsEmpty(P) then Advance(~P); end if;
    // Has the bucket file process finished?
    if IsEmpty(P) then
        // Try advancing the hash value
        success:=false;
        hashval +:= 1;
        while not success and hashval lt fc`num_buckets do
            bool,bucket:=IsDefined(cache,hashval);
            if not bool then bucket:={}; end if;
            bool,P:=pbfc_create_process(fc,hashval,bucket);
            error if not bool, P;
            if P cmpeq false then
                hashval +:= 1;
            else
                success:=true;
            end if;
        end while;
        // Any luck?
        if not success then
            info:=<true>;
            return;
        end if;
    end if;
    // Save the values
    info:=<false,fc,hashval,P,Current(P),info[6] + 1>;
end procedure;

// Returns the cache file's current object.
function pfc_current_value(info)
    error if info[1], "The process has finished.";
    return info[5];
end function;

// Returns a label for the cache file's current object.
function pfc_current_label(info)
    error if info[1], "The process has finished.";
    return info[6];
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic FCCreate( dir::MonStgElt : maxMemCacheSize:=false, hash:=false,
	numBuckets:=false, toString:=false, fromString:=false ) -> Rec
{Create a new file cache. The optional parameters 'hash', 'toString', and 'fromString' can be set to, respectively, a hash function, an "object to string" function, and a "string to object" function for this file cache. Important: You must call FCFlush when  finished with the file cache to ensure that any changes are written to disk.}
    // Sanity check
    require maxMemCacheSize cmpeq false or
        (Type(maxMemCacheSize) cmpeq RngIntElt and maxMemCacheSize gt 0):
        "Parameter 'maxMemCacheSize' must be a positive integer";
    require numBuckets cmpeq false or (Type(numBuckets) cmpeq RngIntElt and
        numBuckets gt 0 and IsPrime(numBuckets)):
        "Parameter 'numBuckets' must be a (positive) prime number";
    require hash cmpeq false or ISA(Type(hash),Program):
        "Parameter 'hash' must be a Program";
    require toString cmpeq false or ISA(Type(toString),Program):
        "Parameter 'toString' must be a Program";
    require fromString cmpeq false or ISA(Type(fromString),Program):
        "Parameter 'fromString' must be a Program";
    // Recover the settings for this file cache (if any)
    bool,A:=fc_settings(dir,numBuckets);
    require bool: A;
    // Set the default settings
    if maxMemCacheSize cmpeq false then
        maxMemCacheSize:=DEFAULT_CACHE_SIZE;
    end if;
    // Initialize the file cache record
    fc:=rec< fc_rec | type:="filecache",
                      uid:=A["uid"],
                      creation:=A["creation"],
                      version:=A["version"],
                      num_buckets:=A["num_buckets"],
                      dir:=dir,
                      max_size:=maxMemCacheSize >;
    if not hash cmpeq false then fc`hash:=hash; end if;
    if not toString cmpeq false then fc`to_string:=toString; end if;
    if not fromString cmpeq false then fc`from_string:=fromString; end if;
    // Return the file cache record
    return fc;
end intrinsic;

intrinsic FCNumBuckets( fc::Rec ) -> RngIntElt
{The number of buckets in the file cache fc}
    require is_fc(fc): "Argument must be a file cache";
    return fc`num_buckets;
end intrinsic;

intrinsic FCContains( fc::Rec, x::Any ) -> BoolElt
{True iff the file cache fc contains x}
    // Sanity check
    require is_fc(fc): "Argument 1 must be a file cache";
    // Calculate the hash
    bool,hashval:=fc_hash(fc,x);
    require bool: hashval;
    // Perhaps x lies in the local cache?
    cache:=fc_get_cache(fc);
    bool,bucket:=IsDefined(cache,hashval);
    if bool and x in cache[hashval] then return true; end if;
    // No luck - we need to check the bucket file
    success,bool:=fc_is_in_bucket_file(fc,x,hashval);
    require success: bool;
    return bool;
end intrinsic;

intrinsic FCInclude( fc::Rec, x::Any )
{Adds x to the file cache fc}
    // Sanity check
    require is_fc(fc): "Argument 1 must be a file cache";
    // Calculate the hash
    bool,hashval:=fc_hash(fc,x);
    require bool: hashval;
    // Add this data to the cache
    fc_add_to_hash(fc,x,hashval);
end intrinsic;

intrinsic FCJoin( fc::Rec, S::SetEnum )
{Adds the objects in the set S to the file cache fc}
    // Sanity check
    require is_fc(fc): "Argument 1 must be a file cache";
    if #S eq 0 then return; end if;
    // Pick our algorithm
    if #S le 10000 then
        // Partition by hash value
        hashS:=AssociativeArray(Integers());
        for x in S do
            bool,hashval:=fc_hash(fc,x);
            require bool: hashval;
            bool,B:=IsDefined(hashS,hashval);
            if bool then
                Include(~B,x);
            else
                B:={x};
            end if;
            hashS[hashval]:=B;
        end for;
        // Add the data to the cache
        for hashval in Keys(hashS) do
            fc_join_to_hash(fc,hashS[hashval],hashval);
        end for;
    else
        // Calculate the hash values
        S:=SetToSequence(S);
        bool,hashes:=fc_hashes_for_seq(fc,S);
        require bool: hashes;
        // Sort the data
        ParallelSort(~hashes,~S);
        // Build the step indices
        steps:=step_indices(hashes);
        // Work through the ranges
        for i in [1..#steps] do
            start:=steps[i];
            finish:=i eq #steps select #hashes else steps[i + 1] - 1;
            hashval:=hashes[start];
            // Add the data to the cache and move on
            fc_join_to_hash(fc,SequenceToSet(S[start..finish]),hashval);
        end for;
    end if;
end intrinsic;

intrinsic FCFlush( fc::Rec )
{Flushes the file cache fc}
    // Sanity check
    require is_fc(fc): "Argument must be a file cache";
    // Start writing the data to the files
    cache:=fc_get_cache(fc);
    for hashval in Keys(cache) do
        bool,err:=fc_bucket_to_file(fc,cache[hashval],hashval);
        require bool: err;
    end for;
    // Finally clear the cache
    fc_clear_cache(fc);
end intrinsic;

// Note: Calling FCInclude or FCFlush whilst owning a process for the file
// cache can cause undocumented behaviour, which includes locking.
intrinsic FCProcess( fc::Rec ) -> Process
{A process for interating over the file cache fc}
    // Sanity check
    require is_fc(fc): "Argument must be a file cache";
    // Work through the hash values looking for the first value
    cache:=fc_get_cache(fc);
    success:=false;
    hashval:=0;
    while not success and hashval lt fc`num_buckets do
        bool,bucket:=IsDefined(cache,hashval);
        if not bool then bucket:={}; end if;
        bool,P:=pbfc_create_process(fc,hashval,bucket);
        require bool: P;
        if P cmpeq false then
            hashval +:= 1;
        else
            success:=true;
        end if;
    end while;
    // Create the info data
    if not success then
        info:=<true>;
    else
        info:=<false,fc,hashval,P,Current(P),1>;
    end if;
    // Return the new process
    return InternalCreateProcess("Cache File",info,pfc_is_empty,pfc_advance,
                                           pfc_current_value,pfc_current_label);
end intrinsic;

intrinsic FCProcess( fc::Rec, n::RngIntElt ) -> Process
{A process for interating over the n-th bucket of the file cache fc}
    // Sanity check
    require is_fc(fc): "Argument 1 must be a file cache";
    require n ge 0 and n lt fc`num_buckets:
        Sprintf("The bucket must be in the range 0..%o",fc`num_buckets - 1);
    // Recover the cache and create a process for this bucket
    cache:=fc_get_cache(fc);
    bool,cache_bucket:=IsDefined(cache,n);
    if not bool then cache_bucket:={}; end if;
    bool,P:=pbfc_create_process(fc,n,cache_bucket);
    require bool: P;
    // If we didn't get a process back (maybe because there was nothing to do)
    // we return an empty process.
    if P cmpeq false then
        return InternalCreateProcess("Bucket File",<true>,pbfc_is_empty,
                            pbfc_advance,pbfc_current_value,pbfc_current_label);
    else
        return P;
    end if;
end intrinsic;

intrinsic FCBucket( fc::Rec, n::RngIntElt ) -> SetEnum
{The n-th bucket of the file cache fc}
    // Sanity check
    require is_fc(fc): "Argument 1 must be a file cache";
    require n ge 0 and n lt fc`num_buckets:
        Sprintf("The bucket must be in the range 0..%o",fc`num_buckets - 1);
    // Recover the cached version of the bucket
    cache:=fc_get_cache(fc);
    bool,cache_bucket:=IsDefined(cache,n);
    if not bool then cache_bucket:={}; end if;
    // Load the bucket from the bucket file
    bool,file_bucket:=fc_load_bucket(fc,n);
    require bool: file_bucket;
    // Return the join
    return cache_bucket join file_bucket;
end intrinsic;

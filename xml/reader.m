freeze;

/////////////////////////////////////////////////////////////////////////
// reader.m
/////////////////////////////////////////////////////////////////////////
// Functions for reading basic XML documents.
// Important limitations:
//  * Probably exist strange ways to format your XML that it doesn't understand
//  * Does not understand any character encodings
//  * Does not handle schemas or namespaces
/////////////////////////////////////////////////////////////////////////

// The XML node record structure
XMLNodeRec:=recformat< type:PowerStructure(MonStgElt),
                       name:PowerStructure(MonStgElt),
                       attributes:PowerStructure(Assoc),
                       text:PowerStructure(MonStgElt),
                       children:PowerSequence(PowerStructure(Rec)) >;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Unescapes any special characters
function unescape_string(str)
    str:=SubstituteString(str,"&gt;",">");
    str:=SubstituteString(str,"&lt;","<");
    str:=SubstituteString(str,"&apos;","'");
    str:=SubstituteString(str,"&quot;","\"");
    str:=SubstituteString(str,"&amp;","&");
    return str;
end function;

// Returns a new XML node from the given data. "children" should be either
// a sequence of child node, or a string (indicating that this is a text node).
function create_XML_node(name,attributes,children)
    attrs:=AssociativeArray(PowerStructure(MonStgElt));
    for attr in Keys(attributes) do
        attrs[attr]:=unescape_string(attributes[attr]);
    end for;
    if Type(children) eq MonStgElt then
        text:=unescape_string(children);
        return rec< XMLNodeRec | type:="xmlnode",
                                 name:=name,
                                 attributes:=attrs,
                                 text:=text >;
    else
        return rec< XMLNodeRec | type:="xmlnode",
                                 name:=name,
                                 attributes:=attrs,
                                 children:=children >;
    end if;
end function;

// Returns the index of the close to the node that opens at the current index,
// or 0 if this wasn't a node afterall.
function node_close_index(str,idx)
    idx+:=1;
    while idx le #str and str[idx] ne "<" do
        if str[idx] eq ">" then return idx; end if;
        idx+:=1;
    end while;
    return 0;
end function;

// Returns true iff the given index marks the start of an XML comment. If true,
// also returns the index of the close to the comment.
function is_XML_comment(str,idx)
    len:=#str;
    if idx + 6 gt len then return false,_; end if;
    if str[idx..idx + 3] ne "<!--" then return false,_; end if;
    endidx:=node_close_index(str,idx + 3);
    if endidx lt idx + 6 then return false,_; end if;
    if str[endidx - 2..endidx] ne "-->" then return false,_; end if;
    return true,endidx;
end function;

// Returns true iff the given index marks the start of a <!DOCTYPE ...>
// node. If true, also returns the index of the end of the node.
function is_XML_doctype(str,idx)
    len:=#str;
    if idx + 10 gt len then return false,_; end if;
    if str[idx..idx + 9] ne "<!DOCTYPE " then return false,_; end if;
    endidx:=node_close_index(str,idx + 9);
    if endidx lt idx + 10 then return false,_; end if;
    return true,endidx;
end function;

// Returns true iff the string begins with an XML header. If it is true, then
// skips over the header and returns the index of the first node.
function is_XML_header(str)
    startidx:=IndexOfNonWhiteSpace(str);
    if #str-startidx lt 30 then return false,_; end if;
    if str[startidx..startidx+5] eq "<?xml " then
        // Scan forward to find the closing tag
        endidx:=node_close_index(str,startidx);
        if endidx ne 0 then
            // We're not going to seriously check this is well formed. We'll
            // just check that it ended with '?>' rather than '>'.
            if str[endidx-1] eq "?" then
                // OK, for our purposes this is fine. Let's return success and
                // the index of the first (non-comment, non-"<!DOCTYPE...") node
                startidx:=IndexOfNonWhiteSpace(str,endidx+1);
                while startidx ne 0 do
                    if str[startidx] eq "<" then
                        bool,endidx:=is_XML_comment(str,startidx);
                        if not bool then
                            bool,endidx:=is_XML_doctype(str,startidx);
                            if not bool then
                                endidx:=node_close_index(str,startidx);
                                if endidx ne 0 then
                                    return true,startidx;
                                end if;
                            else
                                startidx:=IndexOfNonWhiteSpace(str,endidx+1);
                            end if;
                        else
                            startidx:=IndexOfNonWhiteSpace(str,endidx+1);
                        end if;
                    else
                        return false,_;
                    end if;
                end while;
            end if;
        end if;
    end if;
    // If we're here then it didn't work out
    return false,_;
end function;

// Extracts the attributes from the given string of attributes.
function extract_XML_attrubutes(str)
    // Start extracting the attributes
    attrs:=AssociativeArray(PowerStructure(MonStgElt));
    startidx:=IndexOfNonWhiteSpace(str);
    while startidx ne 0 do
        // Scan forwards for the equals sign
        eqidx:=Index(str,"=",startidx);
        if eqidx eq 0 or eqidx eq startidx then return false; end if;
        // Extract the attribute name
        name:=Trim(str[startidx..eqidx-1]);
        // The first non-white space after the equals should be "
        eqidx:=IndexOfNonWhiteSpace(str,eqidx+1);
        if eqidx eq 0 or str[eqidx] ne "\"" then return false; end if;
        // Extract the value
        closeidx:=Index(str,"\"",eqidx+1);
        if closeidx eq 0 then return false; end if;
        value:=str[eqidx+1..closeidx-1];
        // Record this pair
        attrs[name]:=value;
        // Move on
        startidx:=IndexOfNonWhiteSpace(str,closeidx+1);
    end while;
    return attrs;
end function;

// Extracts the child nodes from the given node, or, if it's a text node, the
// text. If the XML document isn't well formed, returns false.
forward extract_XML_node;
function extract_XML_children(str,startidx,tag)
    // Skip over any white space and comment nodes
    rem:=true;
    while rem do
    	startidx:=IndexOfNonWhiteSpace(str,startidx);
    	if startidx eq 0 then return false,_; end if;
    	if str[startidx] eq "<" then
    		rem,endidx:=is_XML_comment(str,startidx);
    		if rem then startidx:=endidx + 1; end if;
    	else
    		rem:=false;
    	end if;
    end while;
    // Is this a text node?
    if str[startidx] ne "<" or node_close_index(str,startidx) eq 0 then
    	text:="";
    	done:=false;
    	while not done do
			// Find the end of the text
			foundend:=false;
			endidx:=startidx;
			while not foundend do
				endidx:=Index(str,"<",endidx+1);
				if endidx eq 0 then return false,_; end if;
				if node_close_index(str,endidx) ne 0 then
					foundend:=true;
				end if;
			end while;
			// Extract the text
			text cat:= str[startidx..endidx-1];
			// Is it just a comment node?
			rem,tmpidx:=is_XML_comment(str,endidx);
			if rem then
				startidx:=tmpidx + 1;
				if startidx gt #str then return false,_; end if;
			else
				done:=true;
			end if;
		end while;
        // The next node had better be the closing node for our tag
        startidx:=endidx;
        if str[startidx+1] ne "/" then return false,_; end if;
        endidx:=node_close_index(str,startidx);
        if tag ne Trim(str[startidx+2..endidx-1]) then
            return false,_; end if;
        endidx+:=1;
        // Record the text
		children:=Trim(text);
    else
        // This isn't a text node -- read in the child nodes
        closed:=false;
        endidx:=startidx;
        children:=[PowerStructure(Rec)|];
        while endidx le #str and not closed do
            // Skip over any white space
            startidx:=IndexOfNonWhiteSpace(str,endidx);
            if startidx eq 0 then return false,_; end if;
            // This had better be the start of a node
            if str[startidx] ne "<" then return false,_; end if;
            // Is this a comment node?
            rem,endidx:=is_XML_comment(str,startidx);
            if rem then
            	endidx+:=1;
            else
				endidx:=node_close_index(str,startidx);
				if endidx eq 0 then return false,_; end if;
				// Perhaps this is the closing node?
				if str[startidx+1] eq "/" then
					if tag ne Trim(str[startidx+2..endidx-1]) then
						return false,_; end if;
					closed:=true;
					endidx+:=1;
				else
					// This is a child node
					child,endidx:=extract_XML_node(str,startidx);
					if Type(child) eq BoolElt then return false,_; end if;
					if endidx eq 0 then return false,_; end if;
					Append(~children,child);
				end if;
			end if;
        end while;
        // Did we find the closing tag?
        if not closed then return false,_; end if;
    end if;
    // Return the children and the final index
    return children,endidx;
end function;

// Extracts the next node from the given XML string. If the XML document isn't
// well formed, returns false.
function extract_XML_node(str,startidx)
    // Get the node length
    endidx:=node_close_index(str,startidx);
    if endidx eq 0 then return false,_; end if;
    // We need to be a little careful -- is this node self-closing?
    if str[endidx-1] eq "/" then
        selfclosing:=true;
        strend:=endidx-2;
        endidx+:=1;
    else
        selfclosing:=false;
        strend:=endidx-1;
    end if;
    // Extract the node substring
    nodestr:=Trim(str[startidx+1..strend]);
    if #nodestr eq 0 then return false,_; end if;
    // Extract the tag name
    idx:=IndexOfFirstWhiteSpace(nodestr);
    tag:=idx eq 0 select nodestr else nodestr[1..idx-1];
    // Are there any attributes?
    if idx ne 0 then
        attributes:=extract_XML_attrubutes(nodestr[idx..#nodestr]);
        if Type(attributes) eq BoolElt then return false,_; end if;
    else
        attributes:=AssociativeArray(PowerStructure(MonStgElt));
    end if;
    // Extract the node's children (if any)
    if selfclosing then
        children:=[PowerStructure(Rec)|];
    else
        children,endidx:=extract_XML_children(str,endidx + 1,tag);
        if Type(children) eq BoolElt then return false,_; end if;
    end if;
    // Skip over any white space and check the next node looks good
    if endidx ne 0 then
        endidx:=IndexOfNonWhiteSpace(str,endidx);
        if endidx ne 0 then
            if str[endidx] ne "<" then return false,_; end if;
            if node_close_index(str,endidx) eq 0 then return false,_; end if;
        end if;
    end if;
    // Return the node and the next node index
    return create_XML_node(tag,attributes,children),endidx;
end function;

/////////////////////////////////////////////////////////////////////////
// Main functions
/////////////////////////////////////////////////////////////////////////

// True iff this is an XML node
intrinsic IsXMLNode( x::Any ) -> BoolElt
{True iff the argument is an XML node}
    return Type(x) cmpeq Rec and
           "type" in Names(x) and
           assigned x`type and
           x`type eq "xmlnode";
end intrinsic;

intrinsic XMLParseString( str::MonStgElt ) -> Rec
{Attempts to parse the XML string into an XML document}
    // Is this an XML string?
    bool,startidx:=is_XML_header(str);
    require bool: "Not valid XML";
    require startidx gt 0: "Not valid XML";
    // Parse the string into an XML node
    node,endidx:=extract_XML_node(str,startidx);
    require Type(node) eq Rec: "Not valid XML";
    // There should only be one root node, but there may be comment nodes
    while endidx ne 0 do
    	require str[endidx] eq "<": "Not valid XML";
    	rem,endidx:=is_XML_comment(str,endidx);
    	require rem: "Not valid XML";
    	endidx:=IndexOfNonWhiteSpace(str,endidx+1);
    end while;
    return node;
end intrinsic;

intrinsic XMLNodeName( node::Rec ) -> MonStgElt
{The name of the given XML node}
    // Sanity check
    require IsXMLNode(node): "Argument must be an XML node";
    // Return the node name
    return node`name;
end intrinsic;

intrinsic XMLAttributes( node::Rec ) -> SetEnum[MonStgElt]
{A set of attributes for the given XML node}
    // Sanity check
    require IsXMLNode(node): "Argument must be an XML node";
    // Return the attribute keys
    return Keys(node`attributes);
end intrinsic;

intrinsic XMLIsAttribute( node::Rec, attr::MonStgElt ) -> BoolElt,MonStgElt
{True iff the given XML node has the given attribute. If so, also gives the value.}
    // Sanity check
    require IsXMLNode(node): "Argument 1 must be an XML node";
    // Is the attribute defined? If so, return the value
    bool,value:=IsDefined(node`attributes,attr);
    if bool then
        return true,value;
    else
        return false,_;
    end if;
end intrinsic;

intrinsic XMLAttributeValue( node::Rec, attr::MonStgElt ) -> MonStgElt
{The given XML node's attribute value (if any)}
    // Sanity check
    require IsXMLNode(node): "Argument 1 must be an XML node";
    // Return the attribute value
    bool,value:=IsDefined(node`attributes,attr);
    return bool select value else "";
end intrinsic;

intrinsic XMLNumberOfChildren( node::Rec ) -> RngIntElt
{The number of children of the given XML node}
    // Sanity check
    require IsXMLNode(node): "Argument must be an XML node";
    // Return the number of child nodes
    return assigned node`children select #node`children else 0;
end intrinsic;

intrinsic XMLNodeChildren( node::Rec ) -> SeqEnum[Rec]
{The child XML nodes of the given XML node as a sequence}
    // Sanity check
    require IsXMLNode(node): "Argument must be an XML node";
    // Return the child nodes (if any)
    return assigned node`children select node`children
                                    else [PowerStructure(Rec)|];
end intrinsic;

intrinsic XMLNodeValue( node::Rec ) -> MonStgElt
{The given XML node's text value (if any)}
    // Sanity check
    require IsXMLNode(node): "Argument must be an XML node";
    // Return the text (if any)
    return assigned node`text select node`text else "";
end intrinsic;

intrinsic XMLIsTextNode( node::Rec ) -> BoolElt
{True iff the given XML node is a text node}
    // Sanity check
    require IsXMLNode(node): "Argument must be an XML node";
    // Is this a text node?
    return assigned node`text;
end intrinsic;

intrinsic XMLIsEmptyNode( node::Rec ) -> BoolElt
{True iff the given XML node is empty}
    // Sanity check
    require IsXMLNode(node): "Argument must be an XML node";
    // Does this node have any content?
    return (assigned node`children and #node`children eq 0) or
           (assigned node`text and #node`text eq 0);
end intrinsic;

intrinsic XMLGetNodesByName( node::Rec, name::MonStgElt ) -> SeqEnum[Rec]
{A sequence of all XML nodes in the tree with the given name}
    // Sanity check
    require IsXMLNode(node): "Argument 1 must be an XML node";
    // Extract the matching nodes
    results:=[PowerStructure(Rec)|];
    if node`name eq name then
        Append(~results,node);
    elif assigned node`children then
        for child in node`children do
            results cat:= XMLGetNodesByName(child,name);
        end for;
    end if;
    return results;
end intrinsic;

freeze;

/////////////////////////////////////////////////////////////////////////
// writer.m
/////////////////////////////////////////////////////////////////////////
// Functions for outputting basic XML documents.
// Important limitations:
//  * Does not attempt to implement any text encoding
//  * Does not handle schemas or namespaces
//  * Does not allow you to include comments in the document
/////////////////////////////////////////////////////////////////////////

import "reader.m": create_XML_node;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Escapes any special characters
function escape_string(str)
    str:=SubstituteString(str,"&","&amp;");
    str:=SubstituteString(str,"\"","&quot;");
    str:=SubstituteString(str,"'","&apos;");
    str:=SubstituteString(str,"<","&lt;");
    str:=SubstituteString(str,">","&gt;");
    return str;
end function;

// Returns true if the proposed node name is valid
function valid_node_name(tag)
    return Index(tag," ") eq 0 and
           Index(tag,"&") eq 0 and
           Index(tag,"\"") eq 0 and
           Index(tag,"'") eq 0 and
           Index(tag,"<") eq 0 and
           Index(tag,">") eq 0;
end function;

// Converts the given node's attributes to a string
function attributes_to_string(node)
    str:="";
    for attr in XMLAttributes(node) do
        str cat:= attr cat "=\"" cat escape_string(XMLAttributeValue(node,attr))
                                 cat "\" ";
    end for;
    return #str gt 0 select str[1..#str - 1] else "";
end function;

// Converts the given node to a string
function node_to_string(node : formatted:=false, indent:="")
    // Create the start of the opening node
    tag:=XMLNodeName(node);
    attrs:=attributes_to_string(node);
    str:=#attrs eq 0 select indent cat "<" cat tag
                       else indent cat "<" cat tag cat " " cat attrs;
    // Has this node got any content?
    if XMLIsEmptyNode(node) then
        return str cat " />";
    end if;
    str cat:= ">";
    close:="</" cat tag cat ">";
    // Add the content
    if XMLIsTextNode(node) then
        return str cat escape_string(XMLNodeValue(node)) cat close;
    else
        newindent:=formatted select indent cat "  " else indent;
        for child in XMLNodeChildren(node) do
            if formatted then str cat:= "\n"; end if;
            str cat:= $$(child : formatted:=formatted, indent:=newindent);
        end for;
        return formatted select str cat "\n" cat indent cat close
                           else str cat close;
    end if;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic XMLToString( node::Rec : formatted:=false ) -> MonStgElt
{Outputs the given XML document as an XML string}
    // Sanity check
    require IsXMLNode(node): "Argument must be an XML node";
    require Type(formatted) cmpeq BoolElt: "Optional parameter 'formatted' must be a boolean";
    // Create the XML string
    header:="<?xml version=\"1.0\"?>\n";
    return header cat node_to_string(node : formatted:=formatted);
end intrinsic;

intrinsic XMLCreateTextNode( name::MonStgElt, text::MonStgElt ) -> Rec
{Create a new XML text node}
    // Sanity check
    require valid_node_name(name): "Invalid node name";
    // Create the node
    attr:=AssociativeArray(PowerStructure(MonStgElt));
    return create_XML_node(name,attr,text);
end intrinsic;

intrinsic XMLCreateNode( name::MonStgElt ) -> Rec
{Create a new XML node}
    // Sanity check
    require valid_node_name(name): "Invalid node name";
    // Create the node
    attr:=AssociativeArray(PowerStructure(MonStgElt));
    children:=[PowerStructure(Rec)|];
    return create_XML_node(name,attr,children);
end intrinsic;

intrinsic XMLAppendChild( ~node::Rec, child::Rec )
{Appends the child XML node to the end of the parent XML node}
    // Sanity check
    require IsXMLNode(node): "Argument 1 must be an XML node";
    require IsXMLNode(child): "Argument 2 must be an XML node";
    require assigned node`children: "Argument 1 is a text node";
    // Append the child node
    Append(~node`children,child);
end intrinsic;

intrinsic XMLInsertChild( ~node::Rec, i::RngIntElt, child::Rec )
{Inserts the child XML node into position i in the parent XML node's list of children}
    // Sanity check
    require IsXMLNode(node): "Argument 1 must be an XML node";
    require IsXMLNode(child): "Argument 3 must be an XML node";
    require assigned node`children: "Argument 1 is a text node";
    require i gt 0: "Argument 2 must be positive";
    require i le #node`children + 1:
        Sprintf("Argument 2 must be in the range [1..%o]",#node`children + 1);
    // Insert the child node
    Insert(~node`children,i,child);
end intrinsic;

intrinsic XMLDeleteChild( ~node::Rec, idx::RngIntElt )
{Removes the indicated child from the given XML node}
    // Sanity check
    require IsXMLNode(node): "Argument 1 must be an XML node";
    require assigned node`children: "Argument 1 is a text node";
    require idx ge 1 and idx le #node`children:
        Sprintf("Argument 2 must be in the range [1..%o]",#node`children);
    // Remove the child node
    Remove(~node`children,idx);
end intrinsic;

intrinsic XMLSetAttribute( ~node::Rec, attr::MonStgElt, value::MonStgElt )
{Sets given XML node's attribute value. If the attribute is already set, then the value will be overwritten.}
    // Sanity check
    require IsXMLNode(node): "Argument 1 must be an XML node";
    // Set the attribute value
    node`attributes[attr]:=value;
end intrinsic;

intrinsic XMLDeleteAttribute( ~node::Rec, attr::MonStgElt )
{Removes the given attribute from the given XML node. If the attribute is not set, does nothing.}
    // Sanity check
    require IsXMLNode(node): "Argument 1 must be an XML node";
    // Remove the attribute
    Remove(~node`attributes,attr);
end intrinsic;

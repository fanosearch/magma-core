freeze;

/////////////////////////////////////////////////////////////////////////
// polynomials.m
/////////////////////////////////////////////////////////////////////////
// Intrinsics for outputting polynomials in LaTeX.
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Returns true followed by the string names if the user has assigned names to
// the variables of the given field/ring R. This is an all-or-nothing test: if
// the user has only assigned partial names, or no names, then returns false.
function has_user_assigned_names(R)
    vars:=[];
    for i in [1..Rank(R)] do
        s:=Sprintf("%o",R.i);
        if #s ge 3 and s[1] eq "$" and s[2] eq "." and
                                             s[3..#s] eq IntegerToString(i) then
            return false,_;
        else
            Append(~vars,s);
        end if;
    end for;
    return true,vars;
end function;

// Given a field/ring R, returns string names for the variables.
function default_names(R)
    // Check to see if the user has assigned names to R
    bool,vars:=has_user_assigned_names(R);
    if bool then return vars; end if;
    // Nope -- create the default variable names
    rk:=Rank(R);
    if rk le 7 then
        vars:=["x","y","z","t","u","v","w"][1..rk];
    else
        vars:=[];
        for i in [1..rk] do
            if i lt 10 then
                Append(~vars,"x_" cat IntegerToString(i));
            else
                Append(~vars,"x_{" cat IntegerToString(i) cat "}");
            end if;
        end for;
    end if;
    return vars;
end function;

// Given a sequence of exponents and a sequence of variable names, returns the
// LaTeX for the corresponding monomial. This is done in a format suitable for
// in-line code, so negative exponents will appear as negative exponents, and
// not in a denominator of a fraction.
function monomial_to_latex(ex,vars)
    str:="";
    for i in [1..#ex] do
        e:=ex[i];
        if e ne 0 then
            if e eq 1 then
                str cat:= vars[i];
            elif e gt 0 and e lt 10 then
                str cat:= vars[i] cat "^" cat IntegerToString(e);
            else
                str cat:= vars[i] cat "^{" cat IntegerToString(e) cat "}";
            end if;
        end if;
    end for;
    return #str eq 0 select "1" else str;
end function;

// Given a sequence of exponents and a sequence of variable names, returns the
// LaTeX for the corresponding monomial. This is done in a format suitable for
// display code, so negative exponents will appear as in the denominator of
// a fraction.
function monomial_to_latex_display(ex,vars)
    numstr:="";
    denstr:="";
    for i in [1..#ex] do
        e:=ex[i];
        if e ne 0 then
            if e gt 0 then
                if e eq 1 then
                    numstr cat:= vars[i];
                elif e lt 10 then
                    numstr cat:= vars[i] cat "^" cat IntegerToString(e);
                else
                    numstr cat:= vars[i] cat "^{" cat IntegerToString(e) cat "}";
                end if;
            else
                if e eq -1 then
                    denstr cat:= vars[i];
                elif e gt -10 then
                    denstr cat:= vars[i] cat "^" cat IntegerToString(-e);
                else
                    denstr cat:= vars[i] cat "^{" cat IntegerToString(-e) cat "}";
                end if;
            end if;
        end if;
    end for;
    if #numstr eq 0 then
        if #denstr eq 0 then
            return "1";
        else
            return "\\frac{1}{" cat denstr cat "}";
        end if;
    else
        if #denstr eq 0 then
            return numstr;
        else
            return "\\frac{" cat numstr cat "}{" cat denstr cat "}";
        end if;
    end if;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic MonomialToLaTeX( m::FldFunRatElt :
    names:=false, display:=false ) -> MonStgElt
{A LaTeX representation of the given monomial m}
    // Sanity check
    require IsMonomial(m): "Argument must be a monomial";
    require Type(display) eq BoolElt:
        "Parameter 'display' must be a boolean";
    rk:=Rank(Parent(m));
    require names cmpeq false or
        (Type(names) eq SeqEnum and #names eq rk and
        (#names eq 0 or ExtendedCategory(names) eq SeqEnum[MonStgElt])):
        Sprintf("Parameter 'names' must be a sequence of strings of length %o",rk);
    // If necessary, create the default variable names
    if names cmpeq false then
        names:=default_names(Parent(m));
    end if;
    // Extract the exponents
    ex:=Exponents(Numerator(m));
    den:=Exponents(Denominator(m));
    for i in [1..rk] do
        ex[i] -:= den[i];
    end for;
    // Return the monomial
    if display then
        return monomial_to_latex_display(ex,names);
    else
        return monomial_to_latex(ex,names);
    end if;
end intrinsic;

intrinsic MonomialToLaTeX( m::RngMPolElt :
    names:=false, display:=false ) -> MonStgElt
{A LaTeX representation of the given monomial m}
    // Sanity check
    require IsMonomial(m): "Argument must be a monomial";
    require Type(display) eq BoolElt:
        "Parameter 'display' must be a boolean";
    rk:=Rank(Parent(m));
    require names cmpeq false or
        (Type(names) eq SeqEnum and #names eq rk and
        (#names eq 0 or ExtendedCategory(names) eq SeqEnum[MonStgElt])):
        Sprintf("Parameter 'names' must be a sequence of strings of length %o",rk);
    // If necessary, create the default variable names
    if names cmpeq false then
        names:=default_names(Parent(m));
    end if;
    // Return the monomial
    return monomial_to_latex(Exponents(m),names);
end intrinsic;

intrinsic ToLaTeX( f::RngMPolElt :
    names:=false, subfactor:=false, display:=false ) -> MonStgElt
{A LaTeX representation of the given polynomial f. If the optional parameter 'subfactor' is set to true, then try to make the resulting string shorter by factoring sub-summands.}
    // Sanity check
    require CoefficientRing(Parent(f)) cmpeq Rationals() or
            CoefficientRing(Parent(f)) cmpeq Integers():
        "The polynomial must be defined over either the integers or the rationals";
    require Type(display) eq BoolElt:
        "Parameter 'display' must be a boolean";
    require Type(subfactor) eq BoolElt:
        "Parameter 'subfactor' must be a boolean";
    rk:=Rank(Parent(f));
    require names cmpeq false or
        (Type(names) eq SeqEnum and #names eq rk and
        (#names eq 0 or ExtendedCategory(names) eq SeqEnum[MonStgElt])):
        Sprintf("Parameter 'names' must be a sequence of strings of length %o",rk);
    // If necessary, create the default variable names
    if names cmpeq false then
        names:=default_names(Parent(f));
    end if;
    // Get the constant case out of the way
    if IsZero(f) then
        return "0";
    elif Degree(f) eq 0 then
        return ToLaTeX(Coefficients(f)[1] : display:=display);
    end if;
    // Initialize and factorise over the rationals
    str:="";
    R:=PolynomialRing(Rationals(),Rank(Parent(f)));
    f:=R ! f;
    F,c:=Factorisation(f);
    // Clear out as many denominators as we can
    if IsIntegral(c) then
        c:=Integers() ! c;
        for i in [1..#F] do
            den:=Denominator(F[i][1])^F[i][2];
            if IsDivisibleBy(c,den) then
                c:=c div den;
                F[i][1] *:= Denominator(F[i][1]);
            end if;
        end for;
    else
        for i in [1..#F] do
            den:=Denominator(F[i][1]);
            F[i][1] *:= den;
            c /:= den^F[i][2];
        end for;
    end if;
    // Extract and collate all the monomial terms
    newF:=[Universe(F)|];
    mono:=Parent(f) ! 1;
    for I in F do
        cs,ms:=CoefficientsAndMonomials(I[1]);
        if #cs eq 1 then
            assert cs[1] eq 1;
            mono *:= I[1]^I[2];
        else
            Append(~newF,I);
        end if;
    end for;
    // See if we can get rid of the -'ve term
    if c eq -1 and mono eq 1 and #newF eq 1 and newF[1][2] eq 1 then
        c:=1;
        newF[1][1]:=-newF[1][1];
    end if;
    // Create the string for the monomial term
    if mono ne 1 then
        str:=monomial_to_latex(Exponents(mono),names);
    end if;
    // Do the terms need brackets?
    if #newF gt 1 then
        terms_brackets:=true;
    elif mono eq 1 then
        terms_brackets:=false;
    else
        terms_brackets:=true;
    end if;
    // Build the rest of the polynomial
    for I in newF do
        cs,ms:=CoefficientsAndMonomials(I[1]);
        term:="";
        if subfactor then
            // Walk through the subsummands with at least three terms, trying to
            // factor them. We choose to factor the subsummand with as many
            // terms as possible, breaking ties among those subsummands of the
            // same length by choosing the subsummand that gives the shortest
            // resulting string of LaTeX.
            subsummand_term := "";
            for k in [1..#cs - 3] do
                factor_successes:=[];
                for set in SetToSequence(Subsets({1..#cs},#cs - k)) do
                    subsummand:=&+[cs[i] * ms[i] : i in set];
                    // If the subsummand factors non-trivially, then store it as
                    // subsummand_term in factored form
                    if #Factorization(subsummand) gt 1 or
                       Factorization(subsummand)[1][2] gt 1 then
                        subsummand_term:=Sprintf("+%o",$$(subsummand :
                             names:=names, subfactor:=false, display:=display));
                        // The sign may wrong here if any of the coefficients
                        // are negative, so make sure they are not
                        assert &and[cs[i] gt 0 : i in [1..#cs]];
                        Append(~factor_successes,<set,subsummand_term>);
                    end if;
                end for;
                // Did we manage to factor any of them? If so, pick the best one
                // and continue.
                if #factor_successes gt 0 then
                    // Pick the best one
                    _,best_one:=Min([#xx[2] : xx in factor_successes]);
                    set:=factor_successes[best_one][1];
                    subsummand_term:=factor_successes[best_one][2];
                    // Remove the subsummand from cs, ms
                    cs:=[cs[i] : i in [1..#cs] | not i in set];
                    ms:=[ms[i] : i in [1..#ms] | not i in set];
                    break;
                end if;
                // Otherwise try to factor subsummands with fewer terms
            end for;
        end if;
        first:=true;
        for i in [1..#cs] do
            if Abs(cs[i]) eq 1 then
                ex:=Exponents(ms[i]);
                if cs[i] lt 0 then
                    term cat:= "-" cat monomial_to_latex(ex,names);
                elif first then
                    term cat:= monomial_to_latex(ex,names);
                else
                    term cat:= "+" cat monomial_to_latex(ex,names);
                end if;
            elif ms[i] eq 1 then
                if first or cs[i] lt 0 then
                    term cat:= ToLaTeX(cs[i] : display:=display);
                else
                    term cat:= "+" cat ToLaTeX(cs[i] : display:=display);
                end if;
            else
                ex:=Exponents(ms[i]);
                if first or cs[i] lt 0 then
                    term cat:= ToLaTeX(cs[i] : display:=display) cat
                                                    monomial_to_latex(ex,names);
                else
                    term cat:= "+" cat ToLaTeX(cs[i] : display:=display) cat
                                                    monomial_to_latex(ex,names);
                end if;
            end if;
            first:=false;
        end for;
        if subfactor and #subsummand_term gt 0 then
            term cat:= subsummand_term;
        end if;
        if terms_brackets or I[2] gt 1 then
            term:="(" cat term cat ")";
            if I[2] gt 1 then
                if I[2] lt 10 then
                    term cat:= "^" cat IntegerToString(I[2]);
                else
                    term cat:= "^{" cat IntegerToString(I[2]) cat "}";
                end if;
            end if;
        end if;
        str cat:= term;
    end for;
    // Fix up the constant term and return
    if Abs(Rationals()!c) eq 1 then
        if c eq 1 then
            return str;
        else
            if not terms_brackets then
                return "-\\left(" cat str cat "\\right)";
            else
                return "-" cat str;
            end if;
        end if;
    else
        if not terms_brackets then
            return ToLaTeX(c : display:=display) cat
                                "\\left(" cat str cat "\\right)";
        else
            return ToLaTeX(c : display:=display) cat str;
        end if;
    end if;
end intrinsic;

intrinsic ToLaTeX( f::FldFunRatElt :
    names:=false, subfactor:=false, display:=false ) -> MonStgElt
{A LaTeX representation of the given rational function f. If the optional parameter 'subfactor' is set to true, then try to make the resulting string shorter by factoring sub-summands.}
    // Sanity check
    require CoefficientRing(Parent(f)) cmpeq Rationals() or
            CoefficientRing(Parent(f)) cmpeq Integers():
        "The rational function must be defined over either the integers or the rationals";
    require Type(display) eq BoolElt:
        "Parameter 'display' must be a boolean";
    require Type(subfactor) eq BoolElt:
        "Parameter 'subfactor' must be a boolean";
    rk:=Rank(Parent(f));
    require names cmpeq false or
        (Type(names) eq SeqEnum and #names eq rk and
        (#names eq 0 or ExtendedCategory(names) eq SeqEnum[MonStgElt])):
        Sprintf("Parameter 'names' must be a sequence of strings of length %o",rk);
    // If necessary, create the default variable names
    if names cmpeq false then
        names:=default_names(Parent(f));
    end if;
    // Extract the numerator and denominator
    num:=Numerator(f);
    den:=Denominator(f);
    // Create the corresponding strings
    if Degree(den) eq 0 then
        assert den eq 1;
        str:=ToLaTeX(num : names:=names,subfactor:=subfactor,display:=display);
    else
        sign:=Sign(Coefficients(LeadingTerm(num))[1]);
        num *:= sign;
        str:="\\frac{" cat
           ToLaTeX(num : names:=names,subfactor:=subfactor,display:=display) cat
           "}{" cat
           ToLaTeX(den : names:=names,subfactor:=subfactor,display:=display) cat
           "}";
        if sign lt 0 then
            str:="-" cat str;
        end if;
    end if;
    return str;
end intrinsic;

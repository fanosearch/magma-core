freeze;

/////////////////////////////////////////////////////////////////////////
// numbers.m
/////////////////////////////////////////////////////////////////////////
// Intrinsics for outputting numbers in LaTeX.
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Returns the string for the given rational value, in \frac{a}{b} notation
// if appropriate.
function rational_to_latex(c)
    if IsIntegral(c) then return IntegerToString(Integers() ! c); end if;
    if c lt 0 then
        c:=Abs(c);
        return "-\\frac{" cat IntegerToString(Numerator(c)) cat "}{" cat
                              IntegerToString(Denominator(c)) cat "}";
    else
        return "\\frac{" cat IntegerToString(Numerator(c)) cat "}{" cat
                             IntegerToString(Denominator(c)) cat "}";
    end if;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic ToLaTeX( c::RngIntElt : display:=false ) -> MonStgElt
{A LaTeX representation of the given value c}
    require Type(display) eq BoolElt: "Parameter 'display' must be a boolean";
    return IntegerToString(c);
end intrinsic;

intrinsic ToLaTeX( c::FldRatElt : display:=false ) -> MonStgElt
{A LaTeX representation of the given value c}
    require Type(display) eq BoolElt: "Parameter 'display' must be a boolean";
    return display select rational_to_latex(c) else Sprintf("%o",c);
end intrinsic;

intrinsic ToLaTeX( c::FldReElt : display:=false ) -> MonStgElt
{A LaTeX representation of the given value c}
    require Type(display) eq BoolElt: "Parameter 'display' must be a boolean";
    return Sprintf("%o",c);
end intrinsic;

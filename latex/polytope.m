freeze;

/////////////////////////////////////////////////////////////////////////
// polytope.m
/////////////////////////////////////////////////////////////////////////
// Intrinsics for outputting polytopes in LaTeX.
/////////////////////////////////////////////////////////////////////////

import "numbers.m": rational_to_latex;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Returns the given polytope in LaTeX notation.
function polytope_to_latex(P,display)
    return "\\mathrm{conv}\\left\\{" cat
           Join([ToLaTeX(v : display:=display) : v in Vertices(P)],",") cat
           "\\right\\}";
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic ToLaTeX( P::TorPol : display:=false ) -> MonStgElt
{A LaTeX representation of the given polytope P}
    require Type(display) eq BoolElt: "Parameter 'display' must be a boolean";
    if IsPolytope(P) then
        return polytope_to_latex(P,display);
    else
        return polytope_to_latex(CompactPart(P),display) cat "+" cat
               ToLaTeX(InfinitePart(P) : display:=display);
    end if;
end intrinsic;

intrinsic ToLaTeX( C::TorCon : display:=false ) -> MonStgElt
{A LaTeX representation of the given cone C}
    require Type(display) eq BoolElt: "Parameter 'display' must be a boolean";
    return "\\mathrm{cone}\\left\\{" cat
           Join([ToLaTeX(v : display:=display) :
                              v in MinimalRGenerators(C)],",") cat "\\right\\}";
end intrinsic;

intrinsic ToLaTeX( v::TorLatElt : display:=false ) -> MonStgElt
{A LaTeX representation of the given lattice point v}
    require Type(display) eq BoolElt: "Parameter 'display' must be a boolean";
    if IsIntegral(v) then
        return "(" cat Join([IntegerToString(c) : c in Eltseq(v)],",") cat ")";
    end if;
    if display then
        return "\\left(" cat Join([rational_to_latex(c) : c in Eltseq(v)],",")
                                                                 cat "\\right)";
    else
        return "(" cat Join([Sprintf("%o",c) : c in Eltseq(v)],",") cat ")";
    end if;
end intrinsic;

freeze;

/////////////////////////////////////////////////////////////////////////
// matrix.m
/////////////////////////////////////////////////////////////////////////
// Intrinsics for outputting matrices in LaTeX.
/////////////////////////////////////////////////////////////////////////

import "numbers.m": rational_to_latex;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Validates the matrix bracket type. Returns true on success, false followed
// by an error message otherwise.
function check_matrix_bracket_type(brackets)
    if Type(brackets) ne MonStgElt then
        return false,"Bracket style must be a string.";
    end if;
    if brackets ne "[" and brackets ne "(" and brackets ne "{" and
       brackets ne "|" and brackets ne "" and brackets ne " " then
        return false,"Bracket style is unknown. Should be one of \"[\", \"(\", \"{\", \"|\", or \"\" for no brackets.";
    end if;
    return true,_;
end function;

// Returns true followed by the matrix type for the given bracket style, or
// false followed by an error message.
function get_matrix_brackets(brackets)
    // Sanity check
    bool,msg:=check_matrix_bracket_type(brackets);
    if not bool then return false,msg; end if;
    // Compute the left and right brackets
    idx := Index(["[","(","{","|",""," "],brackets);
    if idx eq 0 then return false,"Bracket style is unknown"; end if;
    return true,["bmatrix","pmatrix","Bmatrix","vmatrix","matrix","matrix"][idx];
end function;

// Returns a string describing the body of the given integer matrix.
function integer_matrix_to_latex(M)
    rows:=[Join([IntegerToString(c) : c in row],"&") : row in RowSequence(M)];
    return Join(rows,"\\\\\n");
end function;

// Returns a string describing the body of the given rational matrix.
function rational_matrix_to_latex(M,display)
    if display then
        rows:=[Join([rational_to_latex(c) : c in row],"&") :
                                                        row in RowSequence(M)];
    else
        rows:=[Join([Sprintf("%o",c) : c in row],"&") : row in RowSequence(M)];
    end if;
    return Join(rows,"\\\\\n");
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic ToLaTeX( M::ModMatRngElt : type:="(", display:=false ) -> MonStgElt
{}
    // Sanity check
    require Type(display) eq BoolElt: "Parameter 'display' must be a boolean";
    bool,type:=get_matrix_brackets(type);
    require bool: type;
    // Return the matrix
    return "\\begin{" cat type cat "}\n" cat
           integer_matrix_to_latex(M) cat
           "\n\\end{" cat type cat "}";
end intrinsic;

intrinsic ToLaTeX( M::ModMatFldElt : type:="(", display:=false ) -> MonStgElt
{}
    // Sanity check
    require Type(display) eq BoolElt: "Parameter 'display' must be a boolean";
    bool,type:=get_matrix_brackets(type);
    require bool: type;
    // Return the matrix
    return "\\begin{" cat type cat "}\n" cat
           rational_matrix_to_latex(M,display) cat
           "\n\\end{" cat type cat "}";
end intrinsic;

intrinsic ToLaTeX( M::Mtrx : type:="(", display:=false ) -> MonStgElt
{A LaTeX representation of the given matrix M. The optional parameter 'type' can be set to one of "[", "(" [default], "{", "|", or "" for no brackets.}
    // Sanity check
    require Type(display) eq BoolElt: "Parameter 'display' must be a boolean";
    bool,type:=get_matrix_brackets(type);
    require bool: type;
    // Build the coefficients
    if CoefficientRing(M) cmpeq Integers() then
        str:=integer_matrix_to_latex(M);
    elif CoefficientRing(M) cmpeq Rationals() then
        str:=rational_matrix_to_latex(M,display);
    else
        bool,M:=CanChangeRing(M,Rationals());
        require bool: "The matrix must be defined over either the integers or the rationals.";
        str:=rational_matrix_to_latex(M,display);
    end if;
    // Return the matrix
    return "\\begin{" cat type cat "}\n" cat
           str cat "\n\\end{" cat type cat "}";
end intrinsic;

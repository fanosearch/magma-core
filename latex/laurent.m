freeze;

/////////////////////////////////////////////////////////////////////////
// numbers.m
/////////////////////////////////////////////////////////////////////////
// Intrinsics for outputting Laurent polynomials in LaTeX.
/////////////////////////////////////////////////////////////////////////

import "polynomials.m": default_names, monomial_to_latex;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Given a Laurent polynomial f, returns a LaTeX representation of f suitable
// for inclusion in display-mode ($$...$$) LaTeX code.
function laurent_to_latex_display(f,names)
    // Check whether this is secretly a polynomial
    den:=Denominator(f);
    num:=Numerator(f);
    if Degree(den) eq 0 then
        assert IsOne(den);
        return ToLaTeX(num : names:=names, display:=true);
    end if;
    // Start building the string, term by term
    str:="";
    first:=true;
    for t in Terms(num) do
        tt:=t / den;
        sign:=Sign(Coefficients(LeadingTerm(t))[1]);
        tstr:=ToLaTeX(tt : names:=names, display:=true);
        if sign gt 0 and not first then
            tstr:="+" cat tstr;
        end if;
        str cat:= tstr;
        first:=false;
    end for;
    return str;
end function;

// Given a Laurent polynomial f, returns a LaTeX representation of f suitable
// for inclusion in in-line ($...$) LaTeX code.
function laurent_to_latex_inline(f,names)
    // Extract the coefficient and exponents
    cs, es := CoefficientsAndExponents(f);
    assert &and[not IsZero(c) : c in cs];
    // Deal with the zero case
    if #cs eq 0 then
        return "0";
    end if;
    // Start building the string, term by term
    result:="";
    for n in [1..#cs] do
        if Sign(cs[n]) ge 0 then
            sign:=n gt 1 select "+" else "";
        else
            sign:="-";
        end if;
        coeff:=Abs(cs[n]) eq 1 select "" else Sprintf("%o",Abs(cs[n]));
        if &and[IsZero(e) : e in es[n]] and coeff ne "" then
            // The unit monomial gets absorbed into the coefficient
            mono:="";
        else
            mono:=monomial_to_latex(es[n],names);
        end if;
        result cat:= sign cat coeff cat mono;
    end for;
    return result;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic LaurentToLaTeX( f::FldFunRatMElt : names:=false, display:=false )
    -> MonStgElt
{A LaTeX representation of the given Laurent polynomial f}
    // Sanity check
    require IsLaurent(f): "Argument must be a Laurent polynomial";
    require Type(display) eq BoolElt: "Parameter 'display' must be a boolean";
    rk:=Rank(Parent(f));
    require names cmpeq false or
        (Type(names) eq SeqEnum and #names eq rk and
        (#names eq 0 or ExtendedCategory(names) eq SeqEnum[MonStgElt])):
        Sprintf("Parameter 'names' must be a sequence of strings of length %o",rk);
    // If necessary, create the default variable names
    if names cmpeq false then
        names:=default_names(Parent(f));
    end if;
    // Return the string
    return display select laurent_to_latex_display(f,names)
                     else laurent_to_latex_inline(f,names);
end intrinsic;

freeze;

/////////////////////////////////////////////////////////////////////////
// sequences.m
/////////////////////////////////////////////////////////////////////////
// Intrinsics for outputting sequences, sets, etc. in LaTeX.
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Attempts to convert the given sequence (or set, etc.) of objects into a
// string by calling ToLaTeX on each object. Returns true followed by the
// string on success, false followed by an error message otherwise.
function items_to_latex(S,display)
    strs:=[];
    for s in S do
        if Type(s) eq SeqEnum or Type(s) eq SetEnum or Type(s) eq SetIndx or
           Type(s) eq List or Type(s) eq Tup then
            success,c:=$$(s,display);
            if not success then return false,c; end if;
        else
            try
                c:=ToLaTeX(s : display:=display);
            catch e;
                return false, "Unable to convert an element of type " cat
                                                        Sprint(Type(s)) cat ".";
            end try;
        end if;
        Append(~strs,c);
    end for;
    case Type(S):
        when SeqEnum:
            open:="\\left[";
            close:="\\right]";
        when SetEnum:
            open:="\\left\\{";
            close:="\\right\\}";
        when SetIndx:
            open:="\\left\\{";
            close:="\\right\\}";
        when List:
            open:="\\left[";
            close:="\\right]";
        when Tup:
            open:="\\left<";
            close:="\\right>";
    end case;
    return true,open cat Join(strs,",") cat close;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic ToLaTeX( S::SeqEnum : display:=false ) -> MonStgElt
{A LaTeX representation of S}
    require Type(display) eq BoolElt: "Parameter 'display' must be a boolean";
    success,str:=items_to_latex(S,display);
    require success: str;
    return str;
end intrinsic;

intrinsic ToLaTeX( S::SetEnum : display:=false ) -> MonStgElt
{A LaTeX representation of S}
    require Type(display) eq BoolElt: "Parameter 'display' must be a boolean";
    success,str:=items_to_latex(S,display);
    require success: str;
    return str;
end intrinsic;

intrinsic ToLaTeX( S::SetIndx : display:=false ) -> MonStgElt
{A LaTeX representation of S}
    require Type(display) eq BoolElt: "Parameter 'display' must be a boolean";
    success,str:=items_to_latex(S,display);
    require success: str;
    return str;
end intrinsic;

intrinsic ToLaTeX( S::List : display:=false ) -> MonStgElt
{A LaTeX representation of S}
    require Type(display) eq BoolElt: "Parameter 'display' must be a boolean";
    success,str:=items_to_latex(S,display);
    require success: str;
    return str;
end intrinsic;

intrinsic ToLaTeX( S::Tup : display:=false ) -> MonStgElt
{A LaTeX representation of S}
    require Type(display) eq BoolElt: "Parameter 'display' must be a boolean";
    success,str:=items_to_latex(S,display);
    require success: str;
    return str;
end intrinsic;

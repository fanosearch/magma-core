freeze;

/////////////////////////////////////////////////////////////////////////
// point.m
/////////////////////////////////////////////////////////////////////////
// Functions for converting toric lattice points to/from XML.
/////////////////////////////////////////////////////////////////////////

import "primitives.m": integer_to_xml, rational_to_xml, xml_to_integer, xml_to_rational;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Returns a sequence of the given type
function create_sequence(type)
    if type eq "integer" then
        return [Integers()|];
    elif type eq "rational" then
        return [Rationals()|];
    end if;
    error "Illegal point type: " cat type;
end function;

/////////////////////////////////////////////////////////////////////////
// Conversion functions
/////////////////////////////////////////////////////////////////////////

// Convert the given XML node to a lattice point
function xml_to_point(node)
    // Do we know the type?
    bool,type:=XMLIsAttribute(node,"type");
    S:=bool select create_sequence(type) else false;
    // Start adding the elements
    for child in XMLNodeChildren(node) do
        name:=XMLNodeName(child);
        if S cmpeq false then
            type:=name;
            S:=create_sequence(type);
        end if;
        error if name ne type,
            Sprintf("Point coefficient type (%o) does not agree with expected type (%o)",name,type);
        if type eq "integer" then
            Append(~S,xml_to_integer(child));
        else
            Append(~S,xml_to_rational(child));
        end if;
    end for;
    // Check for an untyped 0-dimensional point
    error if S cmpeq false, "Illegal untyped 0-dimensional point";
    // Verify the dimension agrees
    bool,str:=XMLIsAttribute(node,"dimension");
    if bool then
        bool,dim:=IsStringInteger(str);
        error if not bool, "Unable to parse point dimension: " cat str;
        error if dim ne #S,
            Sprintf("Point dimension (%o) does not agree with expected dimension (%o)",#S,dim);
    end if;
    // Looks good
    return LatticeVector(S);
end function;

// Converts the given point to an XML node
function point_to_xml(x)
    error if not Type(x) cmpeq TorLatElt, "Argument must be a point";
    nd:=XMLCreateNode("point");
    XMLSetAttribute(~nd,"dimension",IntegerToString(Dimension(Parent(x))));
    if IsIntegral(x) then
        XMLSetAttribute(~nd,"type","integer");
        for c in Eltseq(x) do XMLAppendChild(~nd,integer_to_xml(c)); end for;
    else
        XMLSetAttribute(~nd,"type","rational");
        for c in Eltseq(x) do XMLAppendChild(~nd,rational_to_xml(c)); end for;
    end if;
    return nd;
end function;

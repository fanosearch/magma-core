freeze;

/////////////////////////////////////////////////////////////////////////
// sequence.m
/////////////////////////////////////////////////////////////////////////
// Functions for converting sequences to/from XML.
/////////////////////////////////////////////////////////////////////////

import "types.m": object_to_xml, xml_to_object;
import "primitives.m": rational_to_xml;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Returns a sequence to contain the given type.
function type_to_sequence(type)
    return case <type |
        "integer":  [Integers()|],
        "rational": [Rationals()|],
        "boolean":  [Booleans()|],
        "string":   [PowerStructure(MonStgElt)|],
        "polytope": [PowerStructure(TorPol)|],
        "error":    [PowerStructure(Rec)|],
        "job":      [PowerStructure(Rec)|],
        default:    [* *] >;
end function;

// Returns a string description of the item type, or false.
function type_to_string(c)
    if IsErrorRecord(c) then return "error"; end if;
    if IsFanosearchJobDetails(c) then return "job"; end if;
    if IsXMLNode(c) then return XMLNodeName(c); end if;
    return case<Type(c) |
        RngIntElt:     "integer",
        FldRatElt:     "rational",
        BoolElt:       "boolean",
        TorLatElt:     "point",
        TorPol:        "polytope",
        SeqEnum:       "sequence",
        SetEnum:       "sequence",
        List:          "sequence",
        Tup:           "sequence",
        RngUPolElt:    "laurent",
        RngMPolElt:    "laurent",
        FldFunRatUElt: "laurent",
        FldFunRatMElt: "laurent",
        ModMatRngElt:  "matrix",
        ModMatFldElt:  "matrix",
        AlgMatElt:     "matrix",
        MonStgElt:     "string",
        Err:           "error",
        default:       false >;
end function;

// Returns a string describing the universe, or false.
function universe_to_string(S)
    if Type(Universe(S)) cmpeq PowStr and #S ne 0 then
        return type_to_string(Representative(S));
    end if;
    return case <Type(Universe(S)) |
        RngInt:       "integer",
        FldRat:       "rational",
        Bool:         "boolean",
        TorLat:       "point",
        PowSeqEnum:   "sequence",
        PowSetEnum:   "sequence",
        PowStr:       "sequence",
        SetCart:      "sequence",
        RngUPol:      "laurent",
        RngMPol:      "laurent",
        FldFunRat:    "laurent",
        ModMatRng:    "matrix",
        ModMatFld:    "matrix",
        AlgMat:       "matrix",
        MonStg:       "string",
        default:      false >;
end function;

// Returns a string description of an appropriate universe, or false.
function common_universe_type(x)
    if Type(x) cmpeq SetEnum then
        type:=type_to_string(Representative(x));
    else
        type:=type_to_string(x[1]);
    end if;
    if Type(type) ne MonStgElt then return false; end if;
    for c in x do
        ctype:=type_to_string(c);
        if Type(ctype) ne MonStgElt then return false; end if;
        if ctype ne type then
            if (ctype eq "rational" and type eq "integer") or
               (ctype eq "integer" and type eq "rational") then
                type:="rational";
            else
                return false;
            end if;
        end if;
    end for;
    return type;
end function;

// Returns true iff there is a common universe containing the elements of the
// given list (without coercing!). If true, also returns the list as a
// sequence.
function has_common_universe(S)
    if #S eq 0 then return false,_; end if;
    type:=Type(S[1]);
    if type cmpeq List or type cmpeq Tup then return false,_; end if;
    if type cmpeq SeqEnum or type cmpeq SetEnum then
        seq:=true;
        cgry:=ExtendedCategory(S[1]);
    else
        seq:=false;
    end if;
    for x in S do
        xtype:=Type(x);
        if xtype cmpeq List or xtype cmpeq Tup then return false,_; end if;
        if xtype ne type then return false,_; end if;
        if seq then
            xcgry:=ExtendedCategory(x);
            if xcgry ne cgry then return false,_; end if;
        end if;
    end for;
    try
        newS:=[x : x in S];
        success:=true;
    catch e
        success:=false;
    end try;
    if success then
        return true,newS;
    else
        return false,_;
    end if;
end function;

// Returns true iff every entry in the sequence is assigned
function all_assigned(S)
    return &and[IsDefined(S,i) : i in [1..#S]];
end function;

/////////////////////////////////////////////////////////////////////////
// Conversion functions
/////////////////////////////////////////////////////////////////////////

// Convert the given XML node to a sequence
function xml_to_sequence(node)
    error if XMLIsTextNode(node), "Invalid sequence node";
    // Create a containing sequence
    bool,type:=XMLIsAttribute(node,"type");
    S:=bool select type_to_sequence(type) else [* *];
    // Start adding the elements
    for child in XMLNodeChildren(node) do
        error if bool and XMLNodeName(child) ne type,
            Sprintf("Sequence entry type (%o) does not agree with expected type (%o)",XMLNodeName(child),type);
        x:=xml_to_object(child);
        Append(~S,x);
    end for;
    // Verify the length agrees
    bool,str:=XMLIsAttribute(node,"length");
    if bool then
        bool,len:=IsStringInteger(str);
        error if not bool, "Unable to parse sequence length: " cat str;
        error if len ne #S,
            Sprintf("Sequence length (%o) does not agree with expected length (%o)",#S,len);
    end if;
    // Is there a common universe for this sequence?
    if Type(S) cmpeq List then
        bool,newS:=has_common_universe(S);
        if bool then S:=newS; end if;
    end if;
    // Looks good
    return S;
end function;

// Convert the given sequence to an XML node
function sequence_to_xml(x)
    // Sanity checks
    error if not Type(x) cmpeq SeqEnum, "Argument must be a sequence";
    error if IsNull(x), "Illegal null sequence";
    error if #x ge 65536, "Sequence nodes must have length less than 65536";
    error if not all_assigned(x),
        "Every element in the sequence must be defined";
    type:=universe_to_string(x);
    error if not Type(type) cmpeq MonStgElt,
        Sprintf("Unsupported sequence universe: %o",Type(Universe(x)));
    // Create the node
    nd:=XMLCreateNode("sequence");
    XMLSetAttribute(~nd,"length",IntegerToString(#x));
    XMLSetAttribute(~nd,"type",type);
    for c in x do
        XMLAppendChild(~nd,object_to_xml(c));
    end for;
    return nd;
end function;

// Convert the given set to an XML node
function set_to_xml(x)
    // Sanity checks
    error if not Type(x) cmpeq SetEnum, "Argument must be a set";
    error if IsNull(x), "Illegal null set";
    error if #x ge 65536, "Sequence nodes must have length less than 65536";
    type:=universe_to_string(x);
    error if not Type(type) cmpeq MonStgElt,
        Sprintf("Unsupported sequence universe: %o",Type(Universe(x)));
    // Create the node
    nd:=XMLCreateNode("sequence");
    XMLSetAttribute(~nd,"length",IntegerToString(#x));
    XMLSetAttribute(~nd,"type",type);
    for c in x do
        XMLAppendChild(~nd,object_to_xml(c));
    end for;
    return nd;
end function;

// Convert the given list to an XML node
function list_to_xml(x)
    // Sanity checks
    error if not Type(x) cmpeq List, "Argument must be a list";
    error if #x eq 0, "Illegal empty list";
    error if #x ge 65536, "Sequence nodes must have length less than 65536";
    type:=common_universe_type(x);
    error if not Type(type) cmpeq MonStgElt, "Invalid list content";
    // Create the node
    nd:=XMLCreateNode("sequence");
    XMLSetAttribute(~nd,"length",IntegerToString(#x));
    XMLSetAttribute(~nd,"type",type);
    for c in x do
        if type eq "rational" and Type(c) cmpeq RngIntElt then
            XMLAppendChild(~nd,rational_to_xml(c));
        else
            XMLAppendChild(~nd,object_to_xml(c));
        end if;
    end for;
    return nd;
end function;

// Convert the given tuple to an XML node
function tuple_to_xml(x)
    // Sanity checks
    error if not Type(x) cmpeq Tup, "Argument must be a tuple";
    error if #x eq 0, "Illegal empty tuple";
    error if #x ge 65536, "Sequence nodes must have length less than 65536";
    type:=common_universe_type(x);
    error if not Type(type) cmpeq MonStgElt, "Invalid tuple content";
    // Create the node
    nd:=XMLCreateNode("sequence");
    XMLSetAttribute(~nd,"length",IntegerToString(#x));
    XMLSetAttribute(~nd,"type",type);
    for c in x do
        if type eq "rational" and Type(c) cmpeq RngIntElt then
            XMLAppendChild(~nd,rational_to_xml(c));
        else
            XMLAppendChild(~nd,object_to_xml(c));
        end if;
    end for;
    return nd;
end function;

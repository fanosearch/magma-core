freeze;

/////////////////////////////////////////////////////////////////////////
// term.m
/////////////////////////////////////////////////////////////////////////
// Functions for converting Laurent polynomial terms to/from XML.
// (A "term" is a purely formal objec representing a (coefficient,monomial)
// pair in a polynomial/Laurent polynomial.)
/////////////////////////////////////////////////////////////////////////

import "../core.m": delete_data, save_data, fetch_data;
import "primitives.m": integer_to_xml, rational_to_xml, xml_to_integer, xml_to_rational;
import "../periodsequence/weyl.m": polynomial_ring_2;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Returns true iff there exists an element s of S with s < 0.
function has_lt_zero(S)
    for s in S do
        if s lt 0 then return true; end if;
    end for;
    return false;
end function;

// Returns the polynomial ring over \ZZ or \QQ in k variables.
function polynomial_ring(F,k)
    if F cmpeq Integers() then
        key:="Z" cat IntegerToString(k);
    elif F cmpeq Rationals() then
        key:="Q" cat IntegerToString(k);
    else
        error "Invalid coefficient ring";
    end if;
    bool,R:=fetch_data(key);
    if not bool then
        if k eq 1 then
            R:=PolynomialRing(F);
        elif k eq 2 and F cmpeq Rationals() then
            R:=polynomial_ring_2();
        else
            R:=PolynomialRing(F,k);
        end if;
        save_data(key,R);
    end if;
    return R;
end function;

// Converts the (coeff,mono) data into a (Laurent) polynomial term
function data_to_term(coeff,mono)
    rk:=#mono;
    R:=polynomial_ring(Parent(coeff),rk);
    if has_lt_zero(mono) then
        R:=FieldOfFractions(R);
        coeff:=R ! coeff;
        f:=coeff * &*[R | R.i^mono[i] : i in [1..rk] | mono[i] gt 0] *
                   &*[R | 1/(R.i^-mono[i]) : i in [1..rk] | mono[i] lt 0];
    else
        coeff:=R ! coeff;
        f:=coeff * &*[R | R.i^mono[i] : i in [1..rk] | mono[i] ne 0];
    end if;
    return f;
end function;

// Extracts the monimial represented as a sequence
function extract_monomial_sequence(node)
    // Do we know the type? If so, it had better be "integer"
    bool,type:=XMLIsAttribute(node,"type");
    error if bool and type ne "integer",
        "Illegal monomial sequence type: " cat type;
    // Extract the values
    S:=[Integers()|];
    for child in XMLNodeChildren(node) do
        type:=XMLNodeName(child);
        error if type ne "integer",
            Sprintf("Sequence entry type (%o) does not agree with expected type (integer)",type);
        Append(~S,xml_to_integer(child));
    end for;
    // Verify the length agrees
    bool,str:=XMLIsAttribute(node,"length");
    if bool then
        bool,len:=IsStringInteger(str);
        error if not bool, "Unable to parse sequence length: " cat str;
        error if len ne #S,
            Sprintf("Sequence length (%o) does not agree with expected length (%o)",#S,len);
    end if;
    // Return the sequence
    return S;
end function;

// Converts the given XML node to a coefficient and monomial sequence
function xml_to_term_pair(node)
    // Walk the child nodes looking for a coefficient and monomial
    coeff:=false;
    mono:=false;
    for child in XMLNodeChildren(node) do
        name:=XMLNodeName(child);
        if name eq "integer" then
            error if not coeff cmpeq false, "Duplicate coefficient node";
            coeff:=xml_to_integer(child);
        elif name eq "rational" then
            error if not coeff cmpeq false, "Duplicate coefficient node";
            coeff:=xml_to_rational(child);
        elif name eq "sequence" then
            error if not mono cmpeq false, "Duplicate monomial node";
            mono:=extract_monomial_sequence(child);
        end if;
    end for;
    // Did we get all the data we require?
    error if coeff cmpeq false, "Missing coefficient node";
    error if mono cmpeq false, "Missing monomial node";
    // Return the pair
    return coeff,mono;
end function;

// Connverts the given coefficient and monomial sequence to an XML node
function term_pair_to_xml(coeff,mono)
    // Sanity check
    error if #mono eq 0, "Illegal empty monomial sequence";
    error if not Universe(mono) cmpeq Integers(), "Invalid monomial sequence";
    K:=Parent(coeff);
    if K cmpeq Integers() then
        type:="integer";
    elif K cmpeq Rationals() then
        type:="rational";
    else
        error Sprintf("Invalid term coefficient ring: %o",K);
    end if;
    // Create the <term> node
    nd:=XMLCreateNode("term");
    // Add the coefficient
    if type eq "integer" then
        XMLAppendChild(~nd,integer_to_xml(coeff));
    else
        XMLAppendChild(~nd,rational_to_xml(coeff));
    end if;
    // Create the monomial sequence
    seq:=XMLCreateNode("sequence");
    XMLSetAttribute(~seq,"length",IntegerToString(#mono));
    XMLSetAttribute(~seq,"type","integer");
    for c in mono do
        XMLAppendChild(~seq,integer_to_xml(c));
    end for;
    XMLAppendChild(~nd,seq);
    // Return the node
    return nd;
end function;

/////////////////////////////////////////////////////////////////////////
// Conversion functions
/////////////////////////////////////////////////////////////////////////

// Convert the given XML node to a term
function xml_to_term(node)
    // Fetch the data
    coeff,mono:=xml_to_term_pair(node);
    // Convert the data into a term
    return data_to_term(coeff,mono);
end function;

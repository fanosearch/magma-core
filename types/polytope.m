freeze;

/////////////////////////////////////////////////////////////////////////
// polytope.m
/////////////////////////////////////////////////////////////////////////
// Functions for converting Polytopes to/from XML.
/////////////////////////////////////////////////////////////////////////

import "primitives.m": integer_to_xml, rational_to_xml, boolean_to_xml, xml_to_integer, xml_to_rational, xml_to_boolean;
import "sequence.m": sequence_to_xml, set_to_xml, xml_to_sequence;

// The polytope record structure
PolytopeRec:=recformat< type:PowerStructure(MonStgElt),
                        vertices:SeqEnum,
                        generators:SeqEnum,
                        points:SetEnum,
                        boundary_points:SetEnum,
                        interior_points:SetEnum,
                        inequalities:SeqEnum,
                        facet_normals:SeqEnum,
                        facet_indices:PowerSequence(PowerSet(Integers())),
                        ehrhart_delta_vector:PowerSequence(Integers()),
                        ehrhart_coefficients:PowerSequence(Integers()),
                        hilbert_delta_vector:PowerSequence(Integers()),
                        hilbert_coefficients:PowerSequence(Integers()),
                        f_vector:PowerSequence(Integers()),
                        volume:Rationals(),
                        boundary_volume:Rationals(),
                        dimension:Integers(),
                        ambient_dimension:Integers(),
                        number_of_points:Integers(),
                        number_of_vertices:Integers(),
                        number_of_boundary_points:Integers(),
                        number_of_interior_points:Integers(),
                        picard_rank:Integers(),
                        gorenstein_index:Integers(),
                        is_integral:Booleans(),
                        is_reflexive:Booleans(),
                        is_terminal:Booleans(),
                        is_canonical:Booleans(),
                        is_smooth:Booleans(),
                        is_simplicial:Booleans(),
                        is_fano:Booleans() >;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Returns true followed by the first matching child node, false if the node
// can't be found.
function xml_find_child(node,name)
    for child in XMLNodeChildren(node) do
        if XMLNodeName(child) eq name then return true,child; end if;
    end for;
    return false,_;
end function;

// Searches the child nodes for a boolean. Returns the boolean value.
function xml_child_to_boolean(node)
    bool,child:=xml_find_child(node,"boolean");
    error if not bool,
        Sprintf("A \"%o\" node must contain a boolean",XMLNodeName(node));
    return xml_to_boolean(child);
end function;

// Searches the child nodes for an integer. Returns the integer value.
function xml_child_to_integer(node)
    bool,child:=xml_find_child(node,"integer");
    error if not bool,
        Sprintf("A \"%o\" node must contain an integer",XMLNodeName(node));
    return xml_to_integer(child);
end function;

// Searches the child nodes for a rational. Returns the rational value.
function xml_child_to_rational(node)
    bool,child:=xml_find_child(node,"rational");
    error if not bool,
        Sprintf("A \"%o\" node must contain a rational",XMLNodeName(node));
    return xml_to_rational(child);
end function;

// Searches the child nodes for an integer sequence. Returns the sequence.
function xml_child_to_int_sequence(node)
    for child in XMLNodeChildren(node) do
        if XMLNodeName(child) eq "sequence" then
            seq:=xml_to_sequence(child);
            if #seq eq 0 then return [Integers()|]; end if;
            if Type(seq) cmpeq SeqEnum and Universe(seq) cmpeq Integers() then
                return seq;
            end if;
        end if;
    end for;
    error Sprintf("A \"%o\" node must contain a sequence of integers",
                                                             XMLNodeName(node));
end function;

// Searches the child nodes for a sequence of numbers. Returns the sequence.
function xml_child_to_num_sequence(node)
    for child in XMLNodeChildren(node) do
        if XMLNodeName(child) eq "sequence" then
            seq:=xml_to_sequence(child);
            if #seq eq 0 then return [Integers()|]; end if;
            if Type(seq) cmpeq SeqEnum and (Universe(seq) cmpeq Integers() or
                Universe(seq) cmpeq Rationals()) then
                return seq;
            end if;
        end if;
    end for;
    error Sprintf("A \"%o\" node must contain a sequence of numbers",
                                                             XMLNodeName(node));
end function;

// Searches the child nodes for a sequence of integer sequences. Returns the
// sequence.
function xml_child_to_int_seq_sequence(node)
    for child in XMLNodeChildren(node) do
        if XMLNodeName(child) eq "sequence" then
            seq:=xml_to_sequence(child);
            if #seq eq 0 then return [PowerSequence(Integers())|]; end if;
            if Type(seq) cmpeq SeqEnum then
                S:=seq[1];
                if Type(S) cmpeq SeqEnum and Universe(S) cmpeq Integers() then
                    return seq;
                end if;
            end if;
        end if;
    end for;
    error Sprintf("A \"%o\" node must contain a sequence of integer sequences",
                                                             XMLNodeName(node));
end function;

// Searches the child nodes for a sequence of points. Returns the sequence.
// If you require that the points are integral, set 'integral' to true.
function xml_child_to_point_sequence(node : integral:=false)
    for child in XMLNodeChildren(node) do
        if XMLNodeName(child) eq "sequence" then
            seq:=xml_to_sequence(child);
            if #seq eq 0 then return []; end if;
            if Type(seq) cmpeq SeqEnum and Type(Universe(seq)) cmpeq TorLat then
                if integral then
                    if &and[IsIntegral(pt) : pt in seq] then
                        return seq;
                    end if;
                else
                    return seq;
                end if;
            end if;
        end if;
    end for;
    type:=integral select " integral" else "";
    error Sprintf("A \"%o\" node must contain a sequence of%o points",
                                                        XMLNodeName(node),type);
end function;

// Searches the child nodes for a sequence of points and a sequence of integers
// or rationals. The two sequences must be of the same length. Returns a
// sequence of tuples.
function xml_child_to_normals(node)
    vs:=xml_child_to_point_sequence(node);
    error if #vs eq 0,
        Sprintf("The sequences in a \"%o\" node mist not be empty",
                                                             XMLNodeName(node));
    hs:=xml_child_to_num_sequence(node);
    error if #vs ne #hs,
        Sprintf("The sequences in a \"%o\" node must be of the same length",
                                                             XMLNodeName(node));
    // We also ensure that the heights are all integral
    D:=Dual(Universe(vs));
    norms:=[CartesianProduct(D,Integers())|];
    for i in [1..#hs] do
        h:=Numerator(hs[i]);
        v:=vs[i] * Denominator(hs[i]);
        k:=Denominator(v);
        v:=v * k;
        h:=h * k;
        k:=GCD(Append(Eltseq(v),h));
        if k ne 1 then
            v:=v / k;
            h:=h div k;
        end if;
        Append(~norms,<D ! v,h>);
    end for;
    // Return the normals
    return norms;
end function;

// extract_lattice_from_polytope_record - Returns the ambient toric lattice
// from the given polytope record.
function extract_lattice_from_polytope_record(poly)
    // First we check the sequences of points
    fields:={"vertices","generators","points","boundary_points",
             "interior_points"};
    for field in fields do
        if assigned poly``field and #poly``field ne 0 then
            return Universe(poly``field);
        end if;
    end for;
    // No luck -- check the dual points
    fields:={"inequalities","facet_normals"};
    for field in fields do
        if assigned poly``field and #poly``field ne 0 then
            return Parent(poly``field[1][1]);
        end if;
    end for;
    // Things are desperate -- if the dimension isn't defined then we're stuck
    if assigned poly`ambient_dimension then
        error if poly`ambient_dimension lt 0,
            "The ambient dimension must be a non-negative integer";
        return LatticeVector(ZeroSequence(Integers(),poly`ambient_dimension));
    end if;
    if assigned poly`dimension and poly`dimension gt 0 then
        return LatticeVector(ZeroSequence(Integers(),poly`dimension));
    end if;
    // No luck
    error "The ambient lattice could not be recovered";
end function;

// validate_polytope - Performs some very basic checks comparing the polytope
// with the polytope record. Returns true on success, false and an error
// message otherwise.
function validate_polytope(P,poly)
    // Validate the dimension data
    if assigned poly`ambient_dimension and
       Dimension(Ambient(P)) ne poly`ambient_dimension then
        return false,Sprintf("The ambient dimension (%o) does not agree with the expected dimension (%o)",Dimension(Ambient(P)),poly`ambient_dimension);
    end if;
    if assigned poly`dimension and Dimension(P) ne poly`dimension then
        return false,Sprintf("The polytope dimension (%o) does not agree with the expected dimension (%o)",Dimension(P),poly`dimension);
    end if;
    // Validate the number of vertices
    if assigned poly`number_of_vertices and
       NumberOfVertices(P) ne poly`number_of_vertices then
        return false,Sprintf("The number of vertices (%o) does not agree with the expected number (%o)",NumberOfVertices(P),poly`number_of_vertices);
    end if;
    // Validate whether the polytope is integral or not
    if assigned poly`is_integral then
        if poly`is_integral then
            if not IsIntegral(P) then
                return false,"The polytope is not integral, but is expected to be integral";
            end if;
        else
            if IsIntegral(P) then
                return false,"The polytope is integral, but is expected to not be integral";
            end if;
        end if;
    end if;
    // That's enough checking
    return true,_;
end function;

/////////////////////////////////////////////////////////////////////////
// Conversion functions
/////////////////////////////////////////////////////////////////////////

// Converts a polytope xml node to a record. Returns the record. Note that no
// validation that the record values are compatible is performed.
function xml_to_polytope_record(node)
    // Create a new record
    poly:=rec< PolytopeRec | type:="polytope" >;
    // Start populating the record
    for child in XMLNodeChildren(node) do
        case XMLNodeName(child):
            when "vertices":
                poly`vertices:=xml_child_to_point_sequence(child);
            when "generators":
                poly`generators:=xml_child_to_point_sequence(child);
            when "points":
                poly`points:=SequenceToSet(xml_child_to_point_sequence(child :
                                                               integral:=true));
            when "boundary_points":
                poly`boundary_points:=SequenceToSet(
                           xml_child_to_point_sequence(child : integral:=true));
            when "interior_points":
                poly`interior_points:=SequenceToSet(
                           xml_child_to_point_sequence(child : integral:=true));
            when "inequalities":
                poly`inequalities:=xml_child_to_normals(child);
            when "facet_normals":
                poly`facet_normals:=xml_child_to_normals(child);
            when "facet_indices":
                fidxs:=[PowerSet(Integers()) |
                            {Integers() | i + 1 : i in facet} :
                            facet in xml_child_to_int_seq_sequence(child)];
                poly`facet_indices:=fidxs;
            when "ehrhart_delta_vector":
                poly`ehrhart_delta_vector:=xml_child_to_int_sequence(child);
            when "ehrhart_coefficients":
                poly`ehrhart_coefficients:=xml_child_to_int_sequence(child);
            when "hilbert_delta_vector":
                poly`hilbert_delta_vector:=xml_child_to_int_sequence(child);
            when "hilbert_coefficients":
                poly`hilbert_coefficients:=xml_child_to_int_sequence(child);
            when "f_vector":
                poly`f_vector:=xml_child_to_int_sequence(child);
            when "volume":
                poly`volume:=xml_child_to_rational(child);
            when "boundary_volume":
                poly`boundary_volume:=xml_child_to_rational(child);
            when "dimension":
                poly`dimension:=xml_child_to_integer(child);
            when "ambient_dimension":
                poly`ambient_dimension:=xml_child_to_integer(child);
            when "number_of_points":
                poly`number_of_points:=xml_child_to_integer(child);
            when "number_of_vertices":
                poly`number_of_vertices:=xml_child_to_integer(child);
            when "number_of_boundary_points":
                poly`number_of_boundary_points:=xml_child_to_integer(child);
            when "number_of_interior_points":
                poly`number_of_interior_points:=xml_child_to_integer(child);
            when "picard_rank":
                poly`picard_rank:=xml_child_to_integer(child);
            when "gorenstein_index":
                poly`gorenstein_index:=xml_child_to_integer(child);
            when "is_integral":
                poly`is_integral:=xml_child_to_boolean(child);
            when "is_reflexive":
                poly`is_reflexive:=xml_child_to_boolean(child);
            when "is_terminal":
                poly`is_terminal:=xml_child_to_boolean(child);
            when "is_canonical":
                poly`is_canonical:=xml_child_to_boolean(child);
            when "is_smooth":
                poly`is_smooth:=xml_child_to_boolean(child);
            when "is_simplicial":
                poly`is_simplicial:=xml_child_to_boolean(child);
            when "is_fano":
                poly`is_fano:=xml_child_to_boolean(child);
        end case;
    end for;
    // We need to ensure that any untyped sequences of points are cast into
    // the correct universe
    L:=extract_lattice_from_polytope_record(poly);
    fields:={"vertices","generators"};
    for field in fields do
        if assigned poly``field and #poly``field eq 0 then
            poly``field:=[L|];
        end if;
    end for;
    fields:={"points","boundary_points","interior_points"};
    for field in fields do
        if assigned poly``field and #poly``field eq 0 then
            poly``field:={L|};
        end if;
    end for;
    // Return the record
    return poly;
end function;

// Converts the given polytope record to an XML node.
function polytope_record_to_xml(poly)
    // Sanity check
    error if not IsPolytopeRecord(poly), "Argument must be a polytope record";
    // Create the <polytope> node
    nd:=XMLCreateNode("polytope");
    // Add the data
    names:=SequenceToSet(Names(PolytopeRec));
    Exclude(~names,"type");
    for field in names do
        if assigned poly``field then
            child:=XMLCreateNode(field);
            case field:
                when "vertices":
                    XMLAppendChild(~child,sequence_to_xml(poly``field));
                when "generators":
                    XMLAppendChild(~child,sequence_to_xml(poly``field));
                when "points":
                    XMLAppendChild(~child,set_to_xml(poly``field));
                when "boundary_points":
                    XMLAppendChild(~child,set_to_xml(poly``field));
                when "interior_points":
                    XMLAppendChild(~child,set_to_xml(poly``field));
                when "inequalities":
                    vs:=[I[1] : I in poly``field];
                    hs:=[I[2] : I in poly``field];
                    XMLAppendChild(~child,sequence_to_xml(vs));
                    XMLAppendChild(~child,sequence_to_xml(hs));
                when "facet_normals":
                    vs:=[I[1] : I in poly``field];
                    hs:=[I[2] : I in poly``field];
                    XMLAppendChild(~child,sequence_to_xml(vs));
                    XMLAppendChild(~child,sequence_to_xml(hs));
                when "facet_indices":
                    fidxs:=[PowerSequence(Integers()) |
                              [Integers() | i - 1 : i in facet] :
                              facet in poly``field];
                    XMLAppendChild(~child,sequence_to_xml(fidxs));
                when "ehrhart_delta_vector":
                    XMLAppendChild(~child,sequence_to_xml(poly``field));
                when "ehrhart_coefficients":
                    XMLAppendChild(~child,sequence_to_xml(poly``field));
                when "hilbert_delta_vector":
                    XMLAppendChild(~child,sequence_to_xml(poly``field));
                when "hilbert_coefficients":
                    XMLAppendChild(~child,sequence_to_xml(poly``field));
                when "f_vector":
                    XMLAppendChild(~child,sequence_to_xml(poly``field));
                when "volume":
                    XMLAppendChild(~child,rational_to_xml(poly``field));
                when "boundary_volume":
                    XMLAppendChild(~child,rational_to_xml(poly``field));
                when "dimension":
                    XMLAppendChild(~child,integer_to_xml(poly``field));
                when "ambient_dimension":
                    XMLAppendChild(~child,integer_to_xml(poly``field));
                when "number_of_points":
                    XMLAppendChild(~child,integer_to_xml(poly``field));
                when "number_of_vertices":
                    XMLAppendChild(~child,integer_to_xml(poly``field));
                when "number_of_boundary_points":
                    XMLAppendChild(~child,integer_to_xml(poly``field));
                when "number_of_interior_points":
                    XMLAppendChild(~child,integer_to_xml(poly``field));
                when "picard_rank":
                    XMLAppendChild(~child,integer_to_xml(poly``field));
                when "gorenstein_index":
                    XMLAppendChild(~child,integer_to_xml(poly``field));
                when "is_integral":
                    XMLAppendChild(~child,boolean_to_xml(poly``field));
                when "is_reflexive":
                    XMLAppendChild(~child,boolean_to_xml(poly``field));
                when "is_terminal":
                    XMLAppendChild(~child,boolean_to_xml(poly``field));
                when "is_canonical":
                    XMLAppendChild(~child,boolean_to_xml(poly``field));
                when "is_smooth":
                    XMLAppendChild(~child,boolean_to_xml(poly``field));
                when "is_simplicial":
                    XMLAppendChild(~child,boolean_to_xml(poly``field));
                when "is_fano":
                    XMLAppendChild(~child,boolean_to_xml(poly``field));
            end case;
            XMLAppendChild(~nd,child);
        end if;
    end for;
    // Return the node
    return nd;
end function;

// Convert the given XML node to a polytope
function xml_to_polytope(node)
    return PolytopeRecordToPolytope(xml_to_polytope_record(node));
end function;

// Convert the given polytope to an XML node
function polytope_to_xml(x)
    error if not Type(x) cmpeq TorPol or not IsPolytope(x),
        "Argument must be a polytope";
    return polytope_record_to_xml(PolytopeToPolytopeRecord(x));
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic IsPolytopeRecord( x::Any ) -> BoolElt
{True iff the argument is a polytope record}
    return Type(x) cmpeq Rec and
           "type" in Names(x) and
           assigned x`type and
           x`type eq "polytope";
end intrinsic;

intrinsic PolytopeToPolytopeRecord( P::TorPol ) -> Rec
{Converts the given polytope to a polytope record}
    // Sanity check
    require IsPolytope(P): "Argument must be a polytope";
    // Create the new record
    poly:=rec< PolytopeRec | type:="polytope" >;
    // Add the dimension data
    poly`dimension:=Dimension(P);
    poly`ambient_dimension:=Dimension(Ambient(P));
    // Catch the degenerate case of the empty polytope
    if IsEmpty(P) then
        // Add the number of points
        poly`number_of_points:=0;
        poly`number_of_interior_points:=0;
        poly`number_of_boundary_points:=0;
        // Return the record
        return poly;
    end if;
    // Add the vertices
    poly`vertices:=Vertices(P);
    poly`number_of_vertices:=NumberOfVertices(P);
    // Add the inequalities if this isn't of maximum dimension
    I,n:=Inequalities(P);
    if not IsMaximumDimensional(P) then
        poly`inequalities:=I;
    end if;
    // Add the facet normals
    poly`facet_normals:=I[1..n];
    // Add the facet indices
    poly`facet_indices:=FacetIndices(P);
    // Add the f-vector
    poly`f_vector:=fVector(P);
    // Add the easy boolean data
    poly`is_integral:=IsIntegral(P);
    poly`is_reflexive:=IsReflexive(P);
    poly`is_smooth:=IsSmooth(P);
    poly`is_simplicial:=IsSimplicial(P);
    poly`is_fano:=IsFano(P);
    // Add the Gorenstein index
    if IsFano(P) then
        poly`gorenstein_index:=GorensteinIndex(P);
    end if;
    // Add the Ehrhart data
    if assigned P`Ehrhart_delta then
        poly`ehrhart_delta_vector:=EhrhartDeltaVector(P);
        poly`ehrhart_coefficients:=EhrhartCoefficients(P,19);
    end if;
    // Add the Hilbert data
    if assigned P`polar and assigned P`polar`Ehrhart_delta then
        Q:=Polar(P);
        poly`hilbert_delta_vector:=EhrhartDeltaVector(Q);
        poly`hilbert_coefficientsEhrhartCoefficients(Q,19);
    end if;
    // Add the volume and boundary volume
    if assigned P`fp_volume or assigned P`Ehrhart_delta or
       assigned P`fp_triangulation or assigned P`poly_triangulation then
        try
            poly`volume:=Volume(P);
            poly`boundary_volume:=VolumeOfBoundary(P);
        catch e;
        end try;
    end if;
    // Add the points
    if assigned P`points then
        poly`points:=Points(P);
    end if;
    if assigned P`points or assigned P`boundary_points then
        poly`boundary_points:=BoundaryPoints(P);
    end if;
    if assigned P`points or assigned P`interior_points then
        poly`interior_points:=InteriorPoints(P);
    end if;
    // Add the number of points
    if assigned P`points or assigned P`Ehrhart_coeffs or
       assigned P`Ehrhart_delta then
        poly`number_of_points:=NumberOfPoints(P);
    end if;
    if assigned P`interior_points or assigned P`num_interior_points or
       assigned P`Ehrhart_delta then
        poly`number_of_interior_points:=NumberOfInteriorPoints(P);
    end if;
    if assigned P`boundary_points or assigned P`num_boundary_points or
       assigned P`Ehrhart_delta then
        poly`number_of_boundary_points:=NumberOfBoundaryPoints(P);
    end if;
    // Add the singularity information
    if IsFano(P) and (assigned P`num_interior_points or
        assigned P`num_boundary_points) then
        // Is this terminal?
        bool:=NumberOfInteriorPoints(P) eq 1 and
              NumberOfBoundaryPoints(P) eq NumberOfVertices(P);
        poly`is_terminal:=bool;
        // Is this canonical?
        bool:=NumberOfInteriorPoints(P) eq 1;
        poly`is_canonical:=bool;
    end if;
    // Return the record
    return poly;
end intrinsic;

intrinsic PolytopeRecordToPolytope( poly::Rec ) -> TorPol
{Converts the given polytope record to a polytope}
    // Sanity check
    require IsPolytopeRecord(poly): "Argument must be a polytope record";
    // Get the empty polytope case out of the way first
    if assigned poly`dimension and poly`dimension eq -1 then
        require assigned poly`ambient_dimension:
            "An empty polytope must have the ambient dimension defined";
        zero:=LatticeVector(ZeroSequence(Integers(),poly`ambient_dimension));
        return EmptyPolyhedron(Parent(zero));
    end if;
    // Create the new polytope
    P:=false;
    if assigned poly`vertices then
        P:=Polytope(poly`vertices : areVertices:=true);
    elif assigned poly`inequalities then
        vs:=[I[1] : I in poly`inequalities];
        hs:=[I[2] : I in poly`inequalities];
        P:=PolyhedronWithInequalities(vs,hs);
        require IsPolytope(P): "The polyhedron must be a polytope";
    elif assigned poly`generators then
        P:=Polytope(poly`generators);
    elif assigned poly`facet_normals then
        vs:=[I[1] : I in poly`facet_normals];
        hs:=[I[2] : I in poly`facet_normals];
        P:=PolyhedronWithInequalities(vs,hs);
        require IsPolytope(P): "The polyhedron must be a polytope";
    end if;
    require not P cmpeq false: "Insufficient data to recover polytope";
    // Validate the polytope against the record
    bool,err:=validate_polytope(P,poly);
    require bool: err;
    // Set the attributes that we can (we don't want to break any of the
    // internal consistency)
    for field in Names(PolytopeRec) do
        if assigned poly``field then
            case field:
                when "points":
                    P`points:=poly``field;
                when "boundary_points":
                    P`boundary_points:=poly``field;
                when "interior_points":
                    P`interior_points:=poly``field;
                when "ehrhart_delta_vector":
                    P`Ehrhart_delta:=poly``field;
                when "ehrhart_coefficients":
                    if #poly``field gt 1 then
                        P`Ehrhart_coeffs:=Remove(poly``field,1);
                    end if;
                when "f_vector":
                    P`faces_f_vector:=poly``field;
                when "volume":
                    P`fp_volume:=poly``field;
                when "boundary_volume":
                    P`fp_boundary_volume:=poly``field;
                when "number_of_points":
                    if not assigned P`Ehrhart_coeffs then
                        P`Ehrhart_coeffs:=[Integers() | poly``field];
                    end if;
                when "number_of_boundary_points":
                    P`num_boundary_points:=poly``field;
                when "number_of_interior_points":
                    P`num_interior_points:=poly``field;
                when "gorenstein_index":
                    P`gorenstein_index:=poly``field;
                when "is_reflexive":
                    P`gorenstein_index:=1;
                when "is_smooth":
                    P`is_smooth:=poly``field;
                when "is_simplicial":
                    P`is_simplicial:=poly``field;
                when "is_fano":
                    P`is_fano:=poly``field;
            end case;
        end if;
    end for;
    // Return the polytope
    return P;
end intrinsic;

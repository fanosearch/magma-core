freeze;

/////////////////////////////////////////////////////////////////////////
// matrix.m
/////////////////////////////////////////////////////////////////////////
// Functions for converting integer and rational matrices to/from XML.
/////////////////////////////////////////////////////////////////////////

import "primitives.m": integer_to_xml, rational_to_xml, xml_to_integer, xml_to_rational;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Returns an appropriately typed sequence.
function create_sequence(type)
    if type eq "integer" then
        return [Integers()|];
    elif type eq "rational" then
        return [Rationals()|];
    end if;
    error "Illegal matrix type: " cat type;
end function;

// Returns a list of all the <sequence> child nodes.
function extract_sequence_nodes(node)
    return [* nd : nd in XMLNodeChildren(node) |
                   XMLNodeName(nd) eq "sequence" *];
end function;

// Attempts to deduce the type based on the given list of <sequence> nodes.
function deduce_type(rows)
    // Is the type set on any of the sequences?
    for nd in rows do
        bool,type:=XMLIsAttribute(nd,"type");
        if bool then return Trim(type); end if;
    end for;
    // Inspect the entries of the first sequence for its type
    if #rows gt 0 then
        childnds:=XMLNodeChildren(rows[1]);
        if #childnds gt 0 then
            type:=XMLNodeName(childnds[1]);
            if type eq "integer" or type eq "rational" then return type; end if;
            error "Illegal matrix type: " cat type;
        end if;
    end if;
    // No joy
    error "Illegal null sequence";
end function;

// Attempts to deduce the number of columns based on the given list of
// <sequence> nodes.
function deduce_ncols(rows)
    // Is the length set on any of the sequences?
    for nd in rows do
        bool,raw:=XMLIsAttribute(nd,"length");
        if bool then
            bool,num:=IsStringInteger(raw);
            error if not bool, "Unable to parse length: " cat raw;
            return num;
        end if;
    end for;
    // Count the entries of the first sequence
    if #rows gt 0 then
        return XMLNumberOfChildren(rows[1]);
    else
        return 0;
    end if;
end function;

// Returns true iff the <sequence> nodes match the given data. If true, also
// returns the sequence of entries.
function entries_to_sequence(rows,ncols,type)
    entries:=create_sequence(type);
    for nd in rows do
        childnds:=XMLNodeChildren(nd);
        if #childnds ne ncols then return false,_; end if;
        if type eq "integer" then
            for child in childnds do
                if XMLNodeName(child) ne "integer" then return false,_; end if;
                Append(~entries,xml_to_integer(child));
            end for;
        else
            for child in childnds do
                if XMLNodeName(child) ne "rational" then return false,_; end if;
                Append(~entries,xml_to_rational(child));
            end for;
        end if;
    end for;
    return true,entries;
end function;

/////////////////////////////////////////////////////////////////////////
// Conversion functions
/////////////////////////////////////////////////////////////////////////

// Convert the given XML node to a matrix
function xml_to_matrix(node)
    // Do we know the type?
    bool,type:=XMLIsAttribute(node,"type");
    if bool then
        type:=Trim(type);
        error if type ne "integer" and type ne "rational",
            "Illegal matrix type: " cat type;
    else
        type:=false;
    end if;
    // Do we know the number of rows?
    bool,raw:=XMLIsAttribute(node,"nrows");
    if bool then
        bool,nrows:=IsStringInteger(raw);
        error if not bool, "Unable to parse number of rows: " cat raw;
        error if nrows lt 0, Sprintf("Invalid number of rows: %o",nrows);
    else
        nrows:=false;
    end if;
    // Do we know the number of columns?
    bool,raw:=XMLIsAttribute(node,"ncolumns");
    if bool then
        bool,ncols:=IsStringInteger(raw);
        error if not bool, "Unable to parse number of columns: " cat raw;
        error if ncols lt 0, Sprintf("Invalid number of columns: %o",ncols);
    else
        ncols:=false;
    end if;
    // Extract the sequence nodes
    seq:=extract_sequence_nodes(node);
    error if #seq gt 1, "Invalid number of matrix sequence nodes";
    if #seq eq 0 then
        // Get the degenerate case out of the way
        if nrows cmpeq false then nrows:=0; end if;
        if ncols cmpeq false then ncols:=0; end if;
        error if nrows ne 0, Sprintf("Expected %o rows but found 0",nrows);
        // Check for a null sequence
        error if type cmpeq false, "Illegal null sequence";
        entries:=create_sequence(type);
    else
        // Extract the row sequences
        rows:=extract_sequence_nodes(seq[1]);
        if nrows cmpeq false then
            nrows:=#rows;
        else
            error if nrows ne #rows,
                Sprintf("Expected %o rows but found %o",nrows,#rows);
        end if;
        // Deduce the sequence type
        if type cmpeq false then type:=deduce_type(rows); end if;
        // Deduce the number of columns
        num:=deduce_ncols(rows);
        if ncols cmpeq false then
            ncols:=num;
        elif nrows gt 0 then
            error if ncols ne num,
                Sprintf("Expected %o columns but found %o",ncols,num);
        end if;
        // Extract the entries
        bool,entries:=entries_to_sequence(rows,ncols,type);
        error if not bool, "Invalid matrix data";
    end if;
    // Create a matrix
    error if #entries ne nrows * ncols,
        Sprintf("Sequence should have length %o",nrows * ncols);
    return Matrix(nrows,ncols,entries);
end function;

// Converts a matrix into an XML node
function matrix_to_xml(x)
    // Sanity check on the matrix type
    xtype:=Type(x);
    if xtype cmpeq ModMatRngElt then
        type:="integer";
    elif xtype cmpeq ModMatFldElt then
        type:="rational";
    elif xtype cmpeq AlgMatElt then
        R:=CoefficientRing(x);
        if R cmpeq Integers() then
            type:="integer";
        elif R cmpeq Rationals() then
            type:="rational";
        else
            error Sprintf("Invalid matrix coefficient ring: %o",R);
        end if;
    else
        error "Argument must be an integer or rational matrix";
    end if;
    // Create the <matrix> node
    nd:=XMLCreateNode("matrix");
    // Add the attributes
    XMLSetAttribute(~nd,"type",type);
    nrows:=IntegerToString(NumberOfRows(x));
    XMLSetAttribute(~nd,"nrows",nrows);
    ncols:=IntegerToString(NumberOfColumns(x));
    XMLSetAttribute(~nd,"ncolumns",ncols);
    // Create the main row sequence
    rows:=XMLCreateNode("sequence");
    XMLSetAttribute(~rows,"type","sequence");
    XMLSetAttribute(~rows,"length",nrows);
    // Add the rows
    for row in RowSequence(x) do
        seq:=XMLCreateNode("sequence");
        XMLSetAttribute(~seq,"type",type);
        XMLSetAttribute(~seq,"length",ncols);
        if type eq "integer" then
            for c in row do XMLAppendChild(~seq,integer_to_xml(c)); end for;
        else
            for c in row do XMLAppendChild(~seq,rational_to_xml(c)); end for;
        end if;
        XMLAppendChild(~rows,seq);
    end for;
    // Add the main row sequence and return the <matrix> node
    XMLAppendChild(~nd,rows);
    return nd;
end function;

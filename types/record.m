freeze;

/////////////////////////////////////////////////////////////////////////
// record.m
/////////////////////////////////////////////////////////////////////////
// Functions for converting records to/from XML.
/////////////////////////////////////////////////////////////////////////

import "types.m": object_to_xml;
import "error.m": error_record_to_xml;
import "polytope.m": polytope_record_to_xml;

/////////////////////////////////////////////////////////////////////////
// Conversion functions
/////////////////////////////////////////////////////////////////////////

// Convert the given record to an XML node
function record_to_xml(x)
    // Sanity checks
    error if not Type(x) cmpeq Rec, "Argument must be a record";
    error if not "type" in Names(x),
        "A record requires a \"type\" fieldname in order to convert it to XML";
    error if not assigned x`type,
        "The \"type\" fieldname is valid but unassigned";
    error if not Type(x`type) cmpeq MonStgElt,
        "The \"type\" field must contain a string";
    // Is this a special case?
    if IsXMLNode(x) then
        return x;
    elif IsErrorRecord(x) then
        return error_record_to_xml(x);
    elif IsPolytopeRecord(x) then
        return polytope_record_to_xml(x);
    end if;
    // Create the node
    nd:=XMLCreateNode(x`type);
    // Add the fields
    for field in Names(x) do
        if field ne "type" and assigned x``field then
            entry:=XMLCreateNode(field);
            XMLAppendChild(~entry,object_to_xml(x``field));
            XMLAppendChild(~nd,entry);
        end if;
    end for;
    return nd;
end function;

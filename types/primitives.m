freeze;

/////////////////////////////////////////////////////////////////////////
// primitives.m
/////////////////////////////////////////////////////////////////////////
// Functions for converting primitives to/from XML.
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Conversion functions
/////////////////////////////////////////////////////////////////////////

// Convert the given XML node to a string
function xml_to_string(node)
    error if not XMLIsTextNode(node), "Invalid string node";
    return XMLNodeValue(node);
end function;

// Convert the given XML node to an integer
function xml_to_integer(node)
    error if not XMLIsTextNode(node), "Invalid integer node";
    str:=XMLNodeValue(node);
    bool,val:=IsStringInteger(str);
    error if not bool, "Unable to parse integer: " cat str;
    return val;
end function;

// Convert the given XML node to a rational
function xml_to_rational(node)
    num:=false;
    den:=false;
    for child in XMLNodeChildren(node) do
        if XMLNodeName(child) eq "numerator" then
            error if not num cmpeq false, "Duplicate numerator node";
            error if not XMLIsTextNode(child), "Invalid numerator node";
            str:=XMLNodeValue(child);
            bool,num:=IsStringInteger(str);
            error if not bool, "Unable to parse numerator: " cat str;
        elif XMLNodeName(child) eq "denominator" then
            error if not den cmpeq false, "Duplicate denominator node";
            error if not XMLIsTextNode(child), "Invalid denominator node";
            str:=XMLNodeValue(child);
            bool,den:=IsStringInteger(str);
            error if not bool, "Unable to parse denominator: " cat str;
        end if;
    end for;
    error if num cmpeq false, "Missing numerator node";
    error if den cmpeq false, "Missing denominator node";
    error if den eq 0, "Illegal zero denominator";
    return num / den;
end function;

// Convert the given XML node to a boolean
function xml_to_boolean(node)
    error if not XMLIsTextNode(node), "Invalid boolean node";
    s:=Trim(StringToLower(XMLNodeValue(node)));
    if s eq "1" or s eq "y" or s eq "t" or s eq "yes" or s eq "true" then
        return true;
    elif s eq "0" or s eq "n" or s eq "f" or s eq "no" or s eq "false" then
        return false;
    end if;
    error "Unable to parse boolean: " cat s;
end function;

// Convert the given string to an XML node
function string_to_xml(x)
    error if not Type(x) cmpeq MonStgElt, "Argument must be a string";
    error if #x gt 65535, "String nodes can be at most 65535 characters long";
    return XMLCreateTextNode("string",x);
end function;

// Convert the given boolean to an XML node
function boolean_to_xml(x)
    error if not Type(x) cmpeq BoolElt, "Argument must be a boolean";
    str:=x select "1" else "0";
    return XMLCreateTextNode("boolean",str);
end function;

// Convert the given integer to an XML node
function integer_to_xml(x)
    if not Type(x) cmpeq RngIntElt then
        error if not IsIntegral(x), "Argument must be an integer";
        x:=Integers() ! x;
    end if;
    return XMLCreateTextNode("integer",IntegerToString(x));
end function;

// Convert the given rational to an XML node
function rational_to_xml(x)
    if not Type(x) cmpeq FldRatElt then
        error if not IsIntegral(x), "Argument must be a rational";
        x:=Rationals() ! x;
    end if;
    num:=XMLCreateTextNode("numerator",IntegerToString(Numerator(x)));
    den:=XMLCreateTextNode("denominator",IntegerToString(Denominator(x)));
    nd:=XMLCreateNode("rational");
    XMLAppendChild(~nd,num);
    XMLAppendChild(~nd,den);
    return nd;
end function;

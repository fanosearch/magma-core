freeze;

/////////////////////////////////////////////////////////////////////////
// laurent.m
/////////////////////////////////////////////////////////////////////////
// Functions for converting Laurent polynomials to/from XML.
/////////////////////////////////////////////////////////////////////////

import "term.m": has_lt_zero, polynomial_ring, xml_to_term_pair, term_pair_to_xml;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Returns true if there exists any element s in S, for any S in SS, such
// that s < 0.
function has_any_lt_zero(SS)
    for S in SS do
        if has_lt_zero(S) then return true; end if;
    end for;
    return false;
end function;

// Converts the given array of coeffs and monos into a (Laurent) polynomial
function data_to_polynomial(coeffs,monos,rk)
    R:=polynomial_ring(Universe(coeffs),rk);
    if has_any_lt_zero(monos) then
        R:=FieldOfFractions(R);
        f:=R ! 0;
        for i in [1..#coeffs] do
            coeff:=R ! coeffs[i];
            mono:=&*[R | R.j^monos[i][j] : j in [1..rk] | monos[i][j] gt 0] *
                  &*[R | 1/(R.j^-monos[i][j]) : j in [1..rk] | monos[i][j] lt 0];
            f +:= coeff * mono;
        end for;
    else
        f:=R ! 0;
        for i in [1..#coeffs] do
            coeff:=R ! coeffs[i];
            mono:=&*[R | R.j^monos[i][j] : j in [1..rk] | monos[i][j] ne 0];
            f +:= coeff * mono;
        end for;
    end if;
    return f;
end function;

// Returns an appropriately typed sequence.
function create_sequence(type)
    if type eq "integer" then
        return [Integers()|];
    elif type eq "rational" then
        return [Rationals()|];
    end if;
    error "Illegal coefficient ring type: " cat type;
end function;

/////////////////////////////////////////////////////////////////////////
// Conversion functions
/////////////////////////////////////////////////////////////////////////

// Convert the given XML node to a (Laurent) polynomial
function xml_to_laurent(node)
    // Do we know the number of variables?
    bool,str:=XMLIsAttribute(node,"nvariables");
    if bool then
        bool,rk:=IsStringInteger(str);
        error if not bool, "Unable to parse number of variables: " cat str;
        error if rk lt 0, Sprintf("Illegal number of variables: %o",rk);
    else
        rk:=false;
    end if;
    // Do we know the coefficient ring?
    bool,type:=XMLIsAttribute(node,"coefficients");
    if bool then
        coeffs:=create_sequence(type);
    else
        coeffs:=false;
    end if;
    // Start working through the terms
    monos:=[PowerSequence(Integers())|];
    for term in XMLNodeChildren(node) do
        if XMLNodeName(term) eq "term" then
            coeff,mono:=xml_to_term_pair(term);
            if coeffs cmpeq false then coeffs:=[Parent(coeff)|]; end if;
        end if;
        error if not Parent(coeff) cmpeq Universe(coeffs),
            "Incompatible term coefficient";
        if rk cmpeq false then rk:=#mono; end if;
        error if #mono ne rk,
            Sprintf("Expected monomial of rank %o, but found rank %o",rk,#mono);
        Append(~coeffs,coeff);
        Append(~monos,mono);
    end for;
    // Check for an untyped coefficient ring or rank
    error if coeffs cmpeq false, "Illegal untyped coefficient ring";
    error if rk cmpeq false, "Illegal null rank";
    error if #coeffs eq 0, "Illegal null polynomial";
    // Convert the data to a Laurent polynomial
    return data_to_polynomial(coeffs,monos,rk);
end function;

// Converts the given (Laurent) polynomial to an XML node
function laurent_to_xml(x)
    // Sanity check on the polynomial type
    xtype:=Type(x);
    error if not (xtype cmpeq RngUPolElt or xtype cmpeq RngMPolElt or
                  xtype cmpeq FldFunRatUElt or xtype cmpeq FldFunRatMElt),
        Sprintf("Argument is of an unsupported polynomial type: %o",xtype);
    R:=Parent(x);
    K:=CoefficientRing(R);
    if K cmpeq Integers() then
        type:="integer";
    elif K cmpeq Rationals() then
        type:="rational";
    else
        error Sprintf("Invalid polynomial coefficient ring: %o",K);
    end if;
    // Create the <laurent> node
    nd:=XMLCreateNode("laurent");
    // Add the attributes
    XMLSetAttribute(~nd,"nvariables",IntegerToString(Rank(R)));
    XMLSetAttribute(~nd,"coefficients",type);
    // Do we have to take into account a denominator?
    try
        den:=Denominator(x);
        x:=Numerator(x);
    catch e
        den:=false;
    end try;
    // If we have a denominator, is it a single term?
    if not den cmpeq false then
        coeffs:=Coefficients(den);
        error if #coeffs gt 1 or not coeffs[1] cmpeq K ! 1,
            "Argument must be a Laurent polynomial";
        den:=Exponents(Monomials(den)[1]);
    end if;
    // Add the terms
    coeffs:=Coefficients(x);
    monos:=Monomials(x);
    for i in [1..#coeffs] do
        if den cmpeq false then
            XMLAppendChild(~nd,term_pair_to_xml(coeffs[i],Exponents(monos[i])));
        else
            mono:=Exponents(monos[i]);
            mono:=[Integers() | mono[i] - den[i] : i in [1..#den]];
            XMLAppendChild(~nd,term_pair_to_xml(coeffs[i],mono));
        end if;
    end for;
    // Return the node
    return nd;
end function;

freeze;

/////////////////////////////////////////////////////////////////////////
// error.m
/////////////////////////////////////////////////////////////////////////
// Functions for converting errors to/from XML.
/////////////////////////////////////////////////////////////////////////

// The error record structure
ErrorRec:=recformat< type:PowerStructure(MonStgElt),
                     message:PowerStructure(MonStgElt),
                     source:PowerStructure(MonStgElt),
                     version:PowerStructure(MonStgElt),
                     hostname:PowerStructure(MonStgElt),
                     script:PowerStructure(MonStgElt),
                     date:PowerStructure(MonStgElt),
                     details:PowerStructure(MonStgElt) >;

/////////////////////////////////////////////////////////////////////////
// Conversion functions
/////////////////////////////////////////////////////////////////////////

// Convert the given XML node to an error record
function xml_to_error_record(node)
    err:=rec< ErrorRec | type:="error" >;
    names:=SequenceToSet(Names(ErrorRec));
    Exclude(~names,"type");
    for child in XMLNodeChildren(node) do
        name:=XMLNodeName(child);
        if name in names and not XMLIsEmptyNode(child) then
            error if not XMLIsTextNode(child), "Invalid error node";
            err``name:=XMLNodeValue(child);
        end if;
    end for;
    return err;
end function;

// Converts the give error record to an XML node
function error_record_to_xml(err)
    // Sanity check
    error if not IsErrorRecord(err), "Argument must be an error record";
    // Create the node
    nd:=XMLCreateNode("error");
    // Add the data
    names:=SequenceToSet(Names(ErrorRec));
    Exclude(~names,"type");
    for field in names do
        if assigned err``field and #err``field gt 0 then
            XMLAppendChild(~nd,XMLCreateTextNode(field,err``field));
        end if;
    end for;
    // Return the node
    return nd;
end function;

// Converts the given error to an XML node
function error_to_xml(err)
    // Sanity check
    error if not Type(err) cmpeq Err, "Argument must be an error";
    // Convert the error to an error record
    err:=ErrorToErrorRecord(err);
    // Convert the error record to node
    return error_record_to_xml(err);
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic IsErrorRecord( x::Any ) -> BoolElt
{True iff the argument is an error record}
    return Type(x) cmpeq Rec and
           "type" in Names(x) and
           assigned x`type and
           x`type eq "error";
end intrinsic;

intrinsic ErrorRecord( msg::MonStgElt : script:="", details:="" ) -> Rec
{Create a new error record}
    // Sanity check
    require Type(script) eq MonStgElt: "'script' must be a string";
    require Type(details) eq MonStgElt: "'details' must be a string";
    // Create the basic record
    err:=rec< ErrorRec | type:="error",
                         source:="magma",
                         version:=GetVersionString() >;
    // Set the hostname
    hostname:=Hostname();
    if #hostname gt 0 then err`hostname:=hostname; end if;
    // Set the RFC 2822 formatted time stamp
    date:=Date("%a, %d %b %Y %H:%M:%S %z");
    if #date gt 0 then err`date:=date; end if;
    // Set the remaining data
    if #script gt 0 then err`script:=script; end if;
    if #msg gt 0 then err`message:=msg; end if;
    if #details gt 0 then err`details:=details; end if;
    // Return the record
    return err;
end intrinsic;

intrinsic ErrorRecordToString( err::Rec ) -> MonStgElt
{A string description of the given error record}
    // Sanity check
    require IsErrorRecord(err): "Argument must be an error record";
    // Create the string
    msgs:=[PowerStructure(MonStgElt)|];
    if assigned err`message then
        Append(~msgs,err`message);
    end if;
    if assigned err`script then
        Append(~msgs,err`script);
    end if;
    if assigned err`source then
        if assigned err`version then
            Append(~msgs,"Source: " cat err`source cat " " cat err`version);
        else
            Append(~msgs,"Source: " cat err`source);
        end if;
    end if;
    if assigned err`hostname then
        Append(~msgs,"Hostname: " cat err`hostname);
    end if;
    if assigned err`date then
        Append(~msgs,"Date: " cat err`date);
    end if;
    if assigned err`details then
        Append(~msgs,"Details:\n" cat err`details);
    end if;
    // Return the string
    return Join(msgs,"\n");
end intrinsic;

intrinsic ErrorToErrorRecord( e::Err : script:="" ) -> Rec
{Converts the given error to an error record}
    msg:=assigned e`Object select Sprintf("%o",e`Object) else "";
    details:="";
    if assigned e`Position then
        details:=e`Position;
    end if;
    if assigned e`Traceback then
        if #details ne 0 then
            details cat:= "\n" cat e`Traceback;
        else
            details:=e`Traceback;
        end if;
    end if;
    return ErrorRecord( msg : script:=script, details:=details );
end intrinsic;

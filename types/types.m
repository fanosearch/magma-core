freeze;

/////////////////////////////////////////////////////////////////////////
// types.m
/////////////////////////////////////////////////////////////////////////
// Functions for converting objects to/from XML.
/////////////////////////////////////////////////////////////////////////

import "primitives.m": string_to_xml, integer_to_xml, rational_to_xml, boolean_to_xml, xml_to_string, xml_to_integer, xml_to_rational, xml_to_boolean;
import "sequence.m": sequence_to_xml, set_to_xml, list_to_xml, tuple_to_xml, xml_to_sequence;
import "matrix.m": matrix_to_xml, xml_to_matrix;
import "point.m": point_to_xml, xml_to_point;
import "term.m": xml_to_term;
import "laurent.m": laurent_to_xml, xml_to_laurent;
import "polytope.m": polytope_to_xml, xml_to_polytope;
import "error.m": error_to_xml, xml_to_error_record;
import "record.m": record_to_xml;

/////////////////////////////////////////////////////////////////////////
// Conversion functions
/////////////////////////////////////////////////////////////////////////

// Converts the given object to an XML node
function object_to_xml(x)
    // Match the type against the conversion function
    type:=Type(x);
    if type cmpeq MonStgElt then     return string_to_xml(x);     end if;
    if type cmpeq RngIntElt then     return integer_to_xml(x);    end if;
    if type cmpeq FldRatElt then     return rational_to_xml(x);   end if;
    if type cmpeq BoolElt then       return boolean_to_xml(x);    end if;
    if type cmpeq SeqEnum then       return sequence_to_xml(x);   end if;
    if type cmpeq SetEnum then       return set_to_xml(x);        end if;
    if type cmpeq List then          return list_to_xml(x);       end if;
    if type cmpeq Tup then           return tuple_to_xml(x);      end if;
    if type cmpeq RngUPolElt then    return laurent_to_xml(x);    end if;
    if type cmpeq RngMPolElt then    return laurent_to_xml(x);    end if;
    if type cmpeq FldFunRatUElt then return laurent_to_xml(x);    end if;
    if type cmpeq FldFunRatMElt then return laurent_to_xml(x);    end if;
    if type cmpeq ModMatRngElt then  return matrix_to_xml(x);     end if;
    if type cmpeq ModMatFldElt then  return matrix_to_xml(x);     end if;
    if type cmpeq AlgMatElt then     return matrix_to_xml(x);     end if;
    if type cmpeq TorLatElt then     return point_to_xml(x);      end if;
    if type cmpeq TorPol then        return polytope_to_xml(x);   end if;
    if type cmpeq Err then           return error_to_xml(x);      end if;
    if type cmpeq Rec then           return record_to_xml(x);     end if;
    // No luck
    error Sprintf("Unsupported object type: %o",type);
end function;

// Converts the given XML node to an object
function xml_to_object(node)
    // Sanity check
    error if not IsXMLNode(node), "Argument must be an XML node";
    // Match the type against the conversion function
    type:=XMLNodeName(node);
    if type eq "string" then   return xml_to_string(node);       end if;
    if type eq "integer" then  return xml_to_integer(node);      end if;
    if type eq "rational" then return xml_to_rational(node);     end if;
    if type eq "boolean" then  return xml_to_boolean(node);      end if;
    if type eq "sequence" then return xml_to_sequence(node);     end if;
    if type eq "matrix" then   return xml_to_matrix(node);       end if;
    if type eq "term" then     return xml_to_term(node);         end if;
    if type eq "laurent" then  return xml_to_laurent(node);      end if;
    if type eq "point" then    return xml_to_point(node);        end if;
    if type eq "polytope" then return xml_to_polytope(node);     end if;
    if type eq "error" then    return xml_to_error_record(node); end if;
    // No luck
    error "Unsupported XML node type: " cat type;
end function;

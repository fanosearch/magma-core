freeze;

/////////////////////////////////////////////////////////////////////////
// ulid.m
/////////////////////////////////////////////////////////////////////////
// Functions for working with ULIDs. See [https://github.com/ulid/spec]
/////////////////////////////////////////////////////////////////////////

// We cache the last timestamp ("t") and 80-bit random number ("rand") generated.
cache := NewStore();

// A lookup table for Crockford base-32 encoding: see [https://www.crockford.com/base32.html]
crockford_lookup := [ "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "M", "N", "P", "Q", "R", "S", "T", "V", "W", "X", "Y", "Z" ];

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Left-pads s with zeroes until it has length 26.
function left_pad(s)
    return &cat["0" : i in [1..26-#s]] cat s;
end function;

// crockford_encode returns the Crockford base 32 encoding of the integer n.
function crockford_encode(n)
    // Move n into the integers
    ZZ := Integers();
    error if not n in ZZ, "n must be an integer";
    n := ZZ!n;
    error if not n ge 0, "n must be non-negative";
    // Handle the zero case
    if n eq 0 then
        return left_pad("");
    end if;
    // Compute the base-32 expansion
    digits := [Strings()|];
    while n gt 0 do
        this := ModByPowerOf2(n,5);
        Append(~digits, crockford_lookup[this+1]);
        n := ShiftRight(n,5);
    end while;
    return left_pad(&cat Reverse(digits));
end function;

// crockford_decode returns the integer corresponding to the Crockford base 32 encoded string s.
function crockford_decode(s)
    error if not Type(s) eq MonStgElt, "s must be a string";
    error if #s eq 0, "s must be a non-empty string";
    // Make everything upper case and remove hyphens
    s := SubstituteString(ToUpper(s), "-", "");
    // Work digit-by-digit from the left-hand end
    n := 0;
    for i in [1..#s] do
        idx := Index(crockford_lookup, s[i]);
        error if idx eq 0, "illegal character";
        n := 32*n + idx - 1;
    end for;
    return n;
end function;

// new_ulid returns a new ULID.
function new_ulid()
    // Grab the cached timestamp
    ok, old_t := StoreIsDefined(cache, "t");
    // Compute the current timestamp
    t := Floor(1000*Realtime());
    // Check if we are still in the same centisecond
    if (not ok) or (t ne old_t) then
        rand := Random(2^80-1);
        StoreSet(cache, "t", t);
    else
        rand := StoreGet(cache, "rand") + 1;
        if rand eq 2^80 then
            // ULID generation failed. We try again, as eventually the centisecond will change
            return new_ulid();
        end if;
    end if;
    StoreSet(cache, "rand", rand);
    return crockford_encode(ShiftLeft(t, 80) + rand);
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic ULIDNew() -> MonStgElt
    {A new ULID.}
    return new_ulid();
end intrinsic;

intrinsic ULIDTime(u::MonStgElt) -> FldReElt
    {The time corresponding to u, in seconds since 00:00:00 GMT, Jan 1, 1970.}
    return ShiftRight(crockford_decode(u),80)/1000.0;
end intrinsic;

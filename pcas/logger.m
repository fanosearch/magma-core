freeze;

/////////////////////////////////////////////////////////////////////////
// logger.m
/////////////////////////////////////////////////////////////////////////
// This is the Magma side of the interface to the pcas logger
/////////////////////////////////////////////////////////////////////////

import "pcas.m": validate_timeout, send_JSON_request,
    send_JSON_request_with_args, malformed_JSON_missing_key, string_from_JSON;
import "../logger/logger.m": set_logger, set_logger_store_value,
    get_logger_store_value;

// Holds configuration information for a logd server.
LogdRec:=recformat<
    Address: MonStgElt  // The address of the server, as a hostname:port pair
                        // of the form "hostname[:port]" or
                        // "tcp://hostname[:port][/]", or a websocket URI of the
                        // form "ws://host/path".
>;

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// Validates the connection c. On success returns true, j, where j is the JSON
// representation of c. Returns false, error message otherwise.
function validate_connection(c)
    // Create the empty JSON object
    j:=JSONCreateNode();
    // Check that this is either 'false' or a record
    if Type(c) cmpeq BoolElt and not c then
        return true, j;
	elif not Type(c) cmpeq Rec then
		return false, "Optional parameter 'Connection' must be a record.";
	end if;
    // Add the address
    if "Address" in Names(c) and assigned c`Address then
        if not Type(c`Address) eq MonStgElt then
            return false, "Value for Address on optional parameter 'Connection' must be a string.";
        elif #c`Address ne 0 then
            JSONAppend(~j, "Address", c`Address);
        end if;
    end if;
	return true, j;
end function;

// Passes the given log message to pcas-magma.
procedure pcas_logger_handler(message,level)
    // Grab the log name, timeout, and connection
    bool,name:=get_logger_store_value("pcas_log_name");
    assert bool;
    bool,Timeout:=get_logger_store_value("pcas_log_timeout");
    assert bool;
    bool,Connection:=get_logger_store_value("pcas_log_connection");
    assert bool;
    // Create the arguments
    args:=JSONCreateNode([* "Name", name *], [* "Message", message *]);
    if JSONNumberOfKeys(Connection) ne 0 then
        JSONAppend(~args, "Connection", Connection);
    end if;
    // Send the log message
    _ := send_JSON_request_with_args("log", "log", args : Timeout:=Timeout);
end procedure;

//////////////////////////////////////////////////////////////////////
// Intrinsics
//////////////////////////////////////////////////////////////////////

intrinsic PcasMagmaLogDefaults() -> Rec
{Configuration information for the connection to the pcas-magma logging server.}
    // Query the server
    ok, j := send_JSON_request("log","defaults");
    require ok: j;
    // Extract the address and return
    ok,conn:=JSONIsDefined(j,"Connection");
    require ok: malformed_JSON_missing_key("Connection",j);
    ok,address:=string_from_JSON(conn,"Address");
    require ok: address;
    return rec<LogdRec | Address:=address>;
end intrinsic;

intrinsic SetLoggerToPcas(log::MonStgElt : Timeout:=0, Connection:=false)
{Sets log messages to be sent to the specified log on the pcas-magma logging server, and enables logging. Optional parameter 'Timeout' is the timeout for submitting log messages [default: 0]. The Timeout is measured in seconds; a Timeout of 0 means no timeout.}
    // Sanity check
    require IsPcasMagma(): "Not running pcas-magma.";
    log:=Trim(log);
    require #log gt 0: "The log name must not be empty.";
    ok,Timeout:=validate_timeout(Timeout);
    require ok: Timeout;
    ok,Connection:=validate_connection(Connection);
    require ok: Connection;
    // Save the values in the store
    set_logger_store_value("pcas_log_name",log);
    set_logger_store_value("pcas_log_timeout",Timeout);
    set_logger_store_value("pcas_log_connection",Connection);
    // Set the logger
    set_logger(pcas_logger_handler,"%s");
end intrinsic;

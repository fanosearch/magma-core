freeze;

/////////////////////////////////////////////////////////////////////////
// stream.m
/////////////////////////////////////////////////////////////////////////
// Provides a stream for key-value inserts.
/////////////////////////////////////////////////////////////////////////

import "../pcas.m": send_JSON_request, string_from_JSON, integer_from_JSON;
import "keyvalue.m": validate_table, keyvalue_JSON_request, record_to_JSON;

declare type KvDbStr;
declare attributes KvDbStr:
        db,                 // The underlying database
        table,              // The name of the underlying table
        ULID,               // The ULID for the stream
        is_closed,          // True if and only if the stream is closed
        n;                  // The total number of inserts on this stream

/////////////////////////////////////////////////////////////////////////
// Instrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic StartInsert(db::KvDb, table::MonStgElt :
    buffer_size:=64, Timeout:=0) -> KvDbStr
{Start a new insert stream to begin inserting key-value records into the given table in the key-value database db. Once created, records can be inserted via Insert. You must call Close on the stream when finished, otherwise resources may leak and buffered data may be lost. The optional parameter 'buffer_size' (default: 64) sets the size of the iterator buffer; set this to zero to disable buffering (note that setting this to 0 does not remove the need to call Close on the stream when finished) . The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
	// Sanity check
	ok, msg := validate_table(table);
	require ok: msg;
    require Type(buffer_size) cmpeq RngIntElt and buffer_size ge 0:
		"Optional parameter 'buffer_size' must be a non-negative integer.";
	// Make the request
	ok, j := keyvalue_JSON_request(db, "begin_insert",
				[* "Table", table *],
                [* "BufferSize", buffer_size *] :
				Timeout:=Timeout);
	require ok: j;
    // Extract the ULID
	ok, ULID := string_from_JSON(j, "ULID");
    require ok: ULID;
	require Trim(ULID) eq ULID and #ULID eq 26:
        Sprintf("Malformed ULID: %o.", ULID);
    // Create and return the stream
    str:=New(KvDbStr);
	str`db:=db;
    str`table:=table;
	str`ULID:=ULID;
	str`is_closed:=false;
	str`n:=0;
	return str;
end intrinsic;

intrinsic Insert(str::KvDbStr, record::Assoc : Timeout:=0)
{Inserts the given record into the the key-value stream. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    // Sanity check
    require not str`is_closed: "Attempting to insert into a closed stream";
    // Convert the record
    ok, r := record_to_JSON(record);
    require ok: r;
    // Make the request
    ok, j := send_JSON_request(str`db`endpoint, "insert_record",
                                    [* "ULID", str`ULID *],
                                    [* "Record", r *] :
                                    Timeout:=Timeout);
    require ok: j;
end intrinsic;

intrinsic Close(str::KvDbStr : Timeout:=0) -> RngIntElt
{Closes the key-value stream, preventing further inserts and releasing any resources used. Returns the total number of inserts made using this stream. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
	// Is there anything to do?
	if not str`is_closed then
        // Send the request
        ok, j := send_JSON_request(str`db`endpoint, "end_insert",
                                    [* "ULID", str`ULID *]:
                                    Timeout:=Timeout);
        require ok: j;
        // Extract the result
        ok,n:=integer_from_JSON(j, "Number");
        str`is_closed := true;
        str`n := n;
    end if;
    return str`n;
end intrinsic;

intrinsic Print(str::KvDbStr)
{Display the key-value stream str.}
	if str`is_closed then
		printf "Closed key-value insert stream for table \"%o\" in %o.", str`table, str`db;
		return;
	end if;
    printf "Key-value insert stream for table \"%o\" in %o.", str`table, str`db;
end intrinsic;

freeze;

/////////////////////////////////////////////////////////////////////////
// connection.m
/////////////////////////////////////////////////////////////////////////
// This defines Records that hold configuration information for the
// various keyvalue storage engines, and functions that convert these
// Records to JSON.
/////////////////////////////////////////////////////////////////////////

import "../pcas.m": send_JSON_request, send_JSON_request_with_args,
	malformed_JSON, malformed_JSON_missing_key, string_from_JSON,
	integer_from_JSON, bool_from_JSON, strings_from_JSON;

KvdbRec:=recformat<
	AppName: MonStgElt,	// The application name to identify ourselves by
	Address: MonStgElt	// The address of the server, as a hostname:port
						// pair of the form "hostname[:port]" or
						// "tcp://hostname[:port][/]", or a websocket URI of the
						// form "ws://host/path"
>;

MongoRec:=recformat<
	AppName: MonStgElt,			// The application name to identify ourselves by
	Hosts: SeqEnum,				// The hosts
	Username: MonStgElt,		// The username
	Password: MonStgElt,		// The password
	PasswordSet: BoolElt,		// Is a password set?
	ReadConcern: MonStgElt,		// The read concern to use ("available",
								// "linearizable", "local", or "majority")
	WriteConcern: MonStgElt,	// The write concern to use ("majority" or
								// "writeN")
	WriteN: RngIntElt,			// The number of writes to confirm
	WriteJournal: BoolElt		// Journal confirmation?
>;

PostgresRec:=recformat<
	AppName: MonStgElt,			// The application name to identify ourselves by
	Hosts: SeqEnum,				// The hosts
	Username: MonStgElt,		// The user to sign in as
	Password: MonStgElt,		// The user's password
	PasswordSet: BoolElt,		// Is a password set?
	ConnectTimeout: RngIntElt,	// Maximum wait for connection, in seconds
	RequireSSL: BoolElt,		// Whether or not to require SSL
	FastCopy: BoolElt			// Whether or not to enable fast Copy
>;

MysqlRec:=recformat<
	Host: MonStgElt,			// The host
	Username: MonStgElt,		// The user to sign in as
	Password: MonStgElt,		// The user's password
	PasswordSet: BoolElt,		// Is a password set?
	ConnectTimeout: RngIntElt,	// Maximum wait for connection, in seconds
	RequireSSL: BoolElt			// Whether or not to require SSL
>;

SqliteRec:=recformat<
	Path: MonStgElt		// The path to the database file
>;

declare type KvDb;
declare attributes KvDb:
        name, 		// The name of the database
		endpoint,	// The endpoint for this database
		config,		// Configuration data for the underlying connection
		conn_JSON,	// The JSON representation of the connection data
        hash_value; // The hash value of this object

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Returns true,config if j is a JSON representation of a kvdb configuration,
// and false, error message otherwise.
function kvdb_from_JSON(j)
	ok,addr:=string_from_JSON(j, "Address");
	if not ok then
		return false,addr;
	end if;
	return true,rec<KvdbRec | Address:=addr>;
end function;

// Returns a JSON representation of a kvdb configuration.
function kvdb_to_JSON(c)
	j:=JSONCreateNode();
	if assigned c`Address and #c`Address ne 0 then
		JSONAppend(~j,"Address",c`Address);
	end if;
	return j;
end function;

// Returns true,config if j is a JSON representation of a mongodb configuration,
// and false, error message otherwise.
function mongodb_from_JSON(j)
	// Read the AppName
	ok,app_name:=string_from_JSON(j, "AppName");
	if not ok then
		return false,app_name;
	end if;
	// Read the Hosts
	ok,hosts:=strings_from_JSON(j, "Hosts");
	if not ok then
		return false,hosts;
	end if;
	// Read the Username
	ok,username:=string_from_JSON(j, "Username");
	if not ok then
		return false, username;
	end if;
	// Read the Password
	ok,password:=string_from_JSON(j, "Password");
	if not ok then
		return false, password;
	end if;
	// Read the PasswordSet
	ok,password_set:=bool_from_JSON(j, "PasswordSet");
	if not ok then
		return false, password_set;
	end if;
	// Read the ReadConcern
	ok,read_concern:=string_from_JSON(j, "ReadConcern");
	if not ok then
		return false, read_concern;
	end if;
	// Read the WriteConcern
	ok,write_concern:=string_from_JSON(j, "WriteConcern");
	if not ok then
		return false, write_concern;
	end if;
	// Read the WriteN
	ok,write_n:=integer_from_JSON(j, "WriteN");
	if not ok then
		return false, write_n;
	end if;
	// Read the WriteJournal
	ok,write_journal:=bool_from_JSON(j, "WriteJournal");
	if not ok then
		return false, write_journal;
	end if;
	// Package up the result
	return true, rec<MongoRec |
		AppName:=app_name,
		Hosts:=hosts,
		Username:=username,
		Password:=password,
		PasswordSet:=password_set,
		ReadConcern:=read_concern,
		WriteConcern:=write_concern,
		WriteN:=write_n,
		WriteJournal:=write_journal
	>;
end function;

// Returns a JSON representation of a mongodb configuration.
function mongodb_to_JSON(c)
	j:=JSONCreateNode();
	if assigned c`AppName and #c`AppName ne 0 then
		JSONAppend(~j, "AppName", c`AppName);
	end if;
	if assigned c`Hosts and #c`Hosts ne 0 then
		JSONAppend(~j, "Hosts", c`Hosts);
	end if;
	if assigned c`Username and #c`Username ne 0 then
		JSONAppend(~j, "Username", c`Username);
	end if;
	if assigned c`PasswordSet and c`PasswordSet then
		JSONAppend(~j, "PasswordSet", true);
		if assigned c`Password then
			JSONAppend(~j, "Password", c`Password);
		else
			JSONAppend(~j, "Password", "");
		end if;
	end if;
	if assigned c`ReadConcern and #c`ReadConcern ne 0 then
		JSONAppend(~j, "ReadConcern", c`ReadConcern);
	end if;
	if assigned c`WriteConcern and #c`WriteConcern ne 0 then
		JSONAppend(~j, "WriteConcern", c`WriteConcern);
		if c`WriteConcern eq "writeN" then
			if assigned c`WriteN then
				JSONAppend(~j, "WriteN", c`WriteN);
			else
				JSONAppend(~j, "WriteN", 0);
			end if;
		end if;
	end if;
	if assigned c`WriteJournal ne 0 then
		JSONAppend(~j, "WriteJournal", c`WriteJournal);
	end if;
	return j;
end function;

// Returns true, backend if j is a JSON representation of a postgres
// configuration and false, error message otherwise.
function postgres_from_JSON(j)
	// Read the AppName
	ok, app_name := string_from_JSON(j, "AppName");
	if not ok then
		return false, app_name;
	end if;
	// Read the Hosts
	ok, hosts := strings_from_JSON(j, "Hosts");
	if not ok then
		return false, hosts;
	end if;
	// Read the Username
	ok, username := string_from_JSON(j, "Username");
	if not ok then
		return false, username;
	end if;
	// Read the Password
	ok, password := string_from_JSON(j, "Password");
	if not ok then
		return false, password;
	end if;
	// Read the PasswordSet
	ok, password_set := bool_from_JSON(j, "PasswordSet");
	if not ok then
		return false, password_set;
	end if;
	// Read the ConnectTimeout
	ok, connect_timeout := integer_from_JSON(j, "ConnectTimeout");
	if not ok then
		return false, connect_timeout;
	end if;
	// Read the RequireSSL
	ok, require_ssl := bool_from_JSON(j, "RequireSSL");
	if not ok then
		return false, require_ssl;
	end if;
	// Read the FastCopy
	ok, fast_copy := bool_from_JSON(j, "FastCopy");
	if not ok then
		return false, fast_copy;
	end if;
	// Package up the result
	return true, rec<PostgresRec |
		AppName:=app_name,
		Hosts:=hosts,
		Username:=username,
		Password:=password,
		PasswordSet:=password_set,
		ConnectTimeout:=connect_timeout,
		RequireSSL:=require_ssl,
		FastCopy:=fast_copy
	>;
end function;

// Returns a JSON representation of a postgres configuration.
function postgres_to_JSON(c)
	j:=JSONCreateNode();
	if assigned c`AppName and #c`AppName ne 0 then
		JSONAppend(~j, "AppName", c`AppName);
	end if;
	if assigned c`Hosts and #c`Hosts ne 0 then
		JSONAppend(~j, "Hosts", c`Hosts);
	end if;
	if assigned c`Username and #c`Username ne 0 then
		JSONAppend(~j, "Username", c`Username);
	end if;
	if assigned c`PasswordSet and c`PasswordSet then
		JSONAppend(~j, "PasswordSet", true);
		if assigned c`Password then
			JSONAppend(~j, "Password", c`Password);
		else
			JSONAppend(~j, "Password", "");
		end if;
	end if;
	if assigned c`ConnectTimeout then
		JSONAppend(~j, "ConnectTimeout", c`ConnectTimeout);
	end if;
	if assigned c`RequireSSL then
		JSONAppend(~j, "RequireSSL", c`RequireSSL);
	end if;
	if assigned c`FastCopy then
		JSONAppend(~j, "FastCopy", c`FastCopy);
	end if;
	return j;
end function;

// Returns true, backend if j is a JSON representation of a mysql configuration
// and false, error message otherwise.
function mysql_from_JSON(j)
	// Read the Host
	ok, host := string_from_JSON(j, "Host");
	if not ok then
		return false, host;
	end if;
	// Read the Username
	ok, username := string_from_JSON(j, "Username");
	if not ok then
		return false, username;
	end if;
	// Read the Password
	ok, password := string_from_JSON(j, "Password");
	if not ok then
		return false, password;
	end if;
	// Read the PasswordSet
	ok, password_set := bool_from_JSON(j, "PasswordSet");
	if not ok then
		return false, password_set;
	end if;
	// Read the ConnectTimeout
	ok, connect_timeout := integer_from_JSON(j, "ConnectTimeout");
	if not ok then
		return false, connect_timeout;
	end if;
	// Read the RequireSSL
	ok, require_ssl := bool_from_JSON(j, "RequireSSL");
	if not ok then
		return false, require_ssl;
	end if;
	// Package up the result
	return true, rec<MysqlRec |
		Host:=host,
		Username:=username,
		Password:=password,
		PasswordSet:=password_set,
		ConnectTimeout:=connect_timeout,
		RequireSSL:=require_ssl
	>;
end function;

// Returns a JSON representation of a mysql configuration.
function mysql_to_JSON(c)
	j:=JSONCreateNode();
	if assigned c`Host and #c`Host ne 0 then
		JSONAppend(~j, "Host", c`Host);
	end if;
	if assigned c`Username and #c`Username ne 0 then
		JSONAppend(~j, "Username", c`Username);
	end if;
	if assigned c`PasswordSet and c`PasswordSet then
		JSONAppend(~j, "PasswordSet", true);
		if assigned c`Password then
			JSONAppend(~j, "Password", c`Password);
		else
			JSONAppend(~j, "Password", "");
		end if;
	end if;
	if assigned c`ConnectTimeout then
		JSONAppend(~j, "ConnectTimeout", c`ConnectTimeout);
	end if;
	if assigned c`RequireSSL then
		JSONAppend(~j, "RequireSSL", c`RequireSSL);
	end if;
	return j;
end function;

// Returns true, backend if j is a JSON representation of an sqlite
// configuration and false, error message otherwise.
function sqlite_from_JSON(j)
	// Read the Path
	ok, path := string_from_JSON(j, "Path");
	if not ok then
		return false, path;
	end if;
	// Package up the result
	return true, rec<SqliteRec |
		Path:=path
	>;
end function;

// Returns a JSON representation of an sqlite configuration.
function sqlite_to_JSON(c)
	j:=JSONCreateNode();
	if assigned c`Path and #c`Path ne 0 then
		JSONAppend(~j, "Path", c`Path);
	end if;
	return j;
end function;

// Returns the default configuration data for the key-value connection to the
// named database system. Returns true followed by the config on success,
// false followed by an error message otherwise.
function fetch_defaults(S)
	// Sanity check
	if not IsPcasMagma() then
		return false,"Not running pcas-magma.";
	end if;
	Sorig:=S;
	S:=ToLower(S);
	if not S in {"kvdb","postgres","postgresql","mongo","mongodb",
		"mysql","sqlite"} then
		return false,Sprintf("Unknown database system \"%o\": Supported systems are \"kvdb\", \"mongo\", \"mysql\", \"postgres\", or \"sqlite\".",Sorig);
	end if;
	// Normalise the name
	if S eq "postgresql" then
		S:="postgres";
	elif S eq "mongo" then
		S:="mongodb";
	end if;
	// Query the server
    ok,j:=send_JSON_request(S,"defaults");
	if not ok then
		return false,j;
	end if;
    // Process the reply
    ok,conn:=JSONIsDefined(j,"Connection");
	if not ok then
		return false,malformed_JSON_missing_key("Connection",j);
	end if;
	case S:
		when "postgres":
			ok,defaults:=postgres_from_JSON(conn);
		when "mongodb":
			ok,defaults:=mongodb_from_JSON(conn);
		when "mysql":
			ok,defaults:=mysql_from_JSON(conn);
		when "sqlite":
			ok,defaults:=sqlite_from_JSON(conn);
		when "kvdb":
			ok,defaults:=kvdb_from_JSON(conn);
	end case;
	return ok,defaults;
end function;

// Returns true if cfg is a record giving configuration data for connection to a
// key-value database and false, error message, _ otherwise. If true, also
// returns the endpoint and a JSON representation of the corresponding
// Connection.
function validate_config(cfg)
	if Type(cfg) ne Rec then
		return false, "Optional parameter 'Config' must be a record.", _;
	end if;
	// Switch on the format
	fmt := Format(cfg);
	if fmt cmpeq KvdbRec then
		return true, "kvdb", kvdb_to_JSON(cfg);
	elif fmt cmpeq MongoRec then
		return true, "mongodb", mongodb_to_JSON(cfg);
	elif fmt cmpeq PostgresRec then
		return true, "postgres", postgres_to_JSON(cfg);
	elif fmt cmpeq MysqlRec then
		return true, "mysql", mysql_to_JSON(cfg);
	elif fmt cmpeq SqliteRec then
		return true, "sqlite", sqlite_to_JSON(cfg);
	end if;
	return false, "Optional parameter 'Config' must be a record in one of the formats returned by KeyValueDbDefaults.", _;
end function;

// Returns the set of database names available via the given config. Returns
// true followed by the set on success, false followed by an error message
// otherwise.
function list_databases(cfg : Timeout:=0)
	// Sanity check
	ok,endpoint,j:=validate_config(cfg);
	if not ok then
		return false,endpoint;
	end if;
	// Make the request
    args:=JSONCreateNode([* "Connection", j *]);
	ok,r:=send_JSON_request_with_args(endpoint, "list_databases", args :
			Timeout:=Timeout);
	if not ok then
		return false,r;
	end if;
	ok, S := strings_from_JSON(r, "Databases");
	if not ok then
		return false,S;
	end if;
	return true,SequenceToSet(S);
end function;

// Creates a database with the given name via the given config. Returns true on success and false followed by an error message otherwise.
function create_database(cfg, name : Timeout:=0)
	// Sanity check
	ok,endpoint,j:=validate_config(cfg);
	if not ok then
		return false,endpoint;
	end if;
	// Make the request
    args:=JSONCreateNode([* "Connection", j *], [* "Database", name *]);
	ok,r:=send_JSON_request_with_args(endpoint, "create_database", args :
			Timeout:=Timeout);
	return ok,r;
end function;

// Deletes the database with the given name via the given config. Returns true on success and false followed by an error message otherwise.
function delete_database(cfg, name : Timeout:=0)
	// Sanity check
	ok,endpoint,j:=validate_config(cfg);
	if not ok then
		return false,endpoint;
	end if;
	// Make the request
    args:=JSONCreateNode([* "Connection", j *], [* "Database", name *]);
	ok,r:=send_JSON_request_with_args(endpoint, "delete_database", args :
			Timeout:=Timeout);
	return ok,r;
end function;

// Returns true followed by a KvDb object described by the given config and
// database name, or false followed by an error message.
function newConnection(cfg,database)
	// Sanity check
	if not IsPcasMagma() then
		return false,"Not running pcas-magma.";
	end if;
	if Trim(database) ne database or #database eq 0 then
		return false,"Illegal database name.";
	end if;
	ok,endpoint,j:=validate_config(cfg);
	if not ok then
		return false,endpoint;
	end if;
	// Package up the result
	db:=New(KvDb);
	db`name:=database;
	db`endpoint:=endpoint;
	db`config:=cfg;
	db`conn_JSON:=j;
	return true,db;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic KeyValueDbDefaults(S::MonStgElt) -> Rec
{Default configuration data for the key-value connection to the given database system, which should be one of "kvdb", "mongo", "mysql", "postgres", or "sqlite".}
	ok,defaults:=fetch_defaults(S);
	require ok: defaults;
	return defaults;
end intrinsic;

intrinsic KeyValueDbConnect(S::MonStgElt, database::MonStgElt) -> KvDb
{Initialises a connection to the named key-value database on the database system S. The default connection to S is used, as returned by KeyValueDbDefaults.}
	ok,cfg:=fetch_defaults(S);
	require ok: cfg;
	ok,db:=newConnection(cfg,database);
	require ok: db;
	return db;
end intrinsic;

intrinsic KeyValueDbConnect(database::MonStgElt : Config:=false) -> KvDb
{Initialises a connection to the named key-value database. The optional parameter 'Config' can be used to provide configuration data for the key-value connection; if Config is not set, then the default connection to kvdb is used.}
	if Config cmpeq false then
		ok,Config:=fetch_defaults("kvdb");
		require ok: Config;
	end if;
	ok,db:=newConnection(Config,database);
	require ok: db;
	return db;
end intrinsic;

intrinsic KeyValueDbListDatabases(S::MonStgElt : Timeout:=0)
	-> SetEnum[MonStgElt]
{The set of available key-value databases on the database system S. The default connection to S is used, as returned by KeyValueDbDefaults. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
	ok,cfg:=fetch_defaults(S);
	require ok: cfg;
	ok,S:=list_databases(cfg : Timeout:=Timeout);
	require ok: S;
	return S;
end intrinsic;

intrinsic KeyValueDbListDatabases( : Config:=false, Timeout:=0)
	-> SetEnum[MonStgElt]
{The set of available key-value databases. The optional parameter 'Config' can be used to provide configuration data for the key-value connection; if Config is not set, then the default connection to kvdb is used. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
	require IsPcasMagma(): "Not running pcas-magma.";
	if Config cmpeq false then
		ok,Config:=fetch_defaults("kvdb");
		require ok: Config;
	end if;
	ok,S:=list_databases(Config : Timeout:=Timeout);
	require ok: S;
	return S;
end intrinsic;

intrinsic KeyValueDbListDatabases(db::KvDb : Timeout:=0) -> SetEnum[MonStgElt]
{The set of available key-value databases. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
	ok,S:=list_databases(db`config : Timeout:=Timeout);
	require ok: S;
	return S;
end intrinsic;

intrinsic KeyValueDbCreate(S::MonStgElt, name::MonStgElt : Timeout:=0)
{Creates a database with the given name on the database system S. The default connection to S is used, as returned by KeyValueDbDefaults. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
	ok,cfg:=fetch_defaults(S);
	require ok: cfg;
	ok,S:=create_database(cfg, name : Timeout:=Timeout);
	require ok: S;
end intrinsic;

intrinsic KeyValueDbCreate(name::MonStgElt : Config:=false, Timeout:=0)
{Creates a database with the given name. The optional parameter 'Config' can be used to provide configuration data for the key-value connection; if Config is not set, then the default connection to kvdb is used. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
	require IsPcasMagma(): "Not running pcas-magma.";
	if Config cmpeq false then
		ok,Config:=fetch_defaults("kvdb");
		require ok: Config;
	end if;
	ok,S:=create_database(Config, name : Timeout:=Timeout);
	require ok: S;
end intrinsic;

intrinsic KeyValueDbDelete(S::MonStgElt, name::MonStgElt : Timeout:=0)
{Deletes database with the given name on the database system S. If there is no such database, this does nothing and no error is returned. The default connection to S is used, as returned by KeyValueDbDefaults. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
	require false: "This operation is disabled";
	ok,cfg:=fetch_defaults(S);
	require ok: cfg;
	ok,S:=delete_database(cfg, name : Timeout:=Timeout);
	require ok: S;
end intrinsic;

intrinsic KeyValueDbDelete(name::MonStgElt : Config:=false, Timeout:=0)
{Deletes the database with the given name. If there is no such database, this does nothing and no error is returned. The optional parameter 'Config' can be used to provide configuration data for the key-value connection; if Config is not set, then the default connection to kvdb is used. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
	require IsPcasMagma(): "Not running pcas-magma.";
	require false: "This operation is disabled";	
	if Config cmpeq false then
		ok,Config:=fetch_defaults("kvdb");
		require ok: Config;
	end if;
	ok,S:=delete_database(Config, name : Timeout:=Timeout);
	require ok: S;
end intrinsic;

intrinsic Hash(db::KvDb) -> RngIntElt
{The hash value of the database connection.}
    if not assigned db`hash_value then
        db`hash_value:=Hash([db`name, db`endpoint, JSONToString(db`conn_JSON)]);
    end if;
    return db`hash_value;
end intrinsic;

intrinsic 'eq'(db1::KvDb, db2::KvDb) -> BoolElt
{True iff the database connections db1 and db2 are equal.}
    return db1`name eq db2`name and
            db1`endpoint eq db2`endpoint and
            JSONToString(db1`conn_JSON) eq JSONToString(db2`conn_JSON);
end intrinsic;

intrinsic Print(db::KvDb)
{Display the database connection db.}
    printf "[Database: %o, Endpoint: %o]", db`name, db`endpoint;
end intrinsic;

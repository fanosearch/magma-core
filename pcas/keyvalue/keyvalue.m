freeze;

/////////////////////////////////////////////////////////////////////////
// keyvalue.m
/////////////////////////////////////////////////////////////////////////
// This is the Magma side of the interface to the pcas key-value
// infrastructure
/////////////////////////////////////////////////////////////////////////

import "../pcas.m": send_JSON_request_with_args, malformed_JSON,
	malformed_JSON_missing_key, integer_from_JSON, strings_from_JSON;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// A wrapper around send_JSON_request_with_args for making keyvalue requests to
// the database db.
function keyvalue_JSON_request_with_args(db, operation, args : Timeout:=0)
	// Add the connection data and database name
    JSONAppend(~args, "Connection", db`conn_JSON);
    JSONAppend(~args, "Database", db`name);
	// Make the request
	ok,r:=send_JSON_request_with_args(db`endpoint, operation, args :
			Timeout:=Timeout);
	return ok,r;
end function;

// A wrapper around send_JSON_request_with_args for making keyvalue requests to
// the database db.
function keyvalue_JSON_request(db, operation, ... : Timeout:=0)
    // Tidy up the input
	args := operation[2..#operation];
	operation := operation[1];
	// Package up the arguments
	keys := [Strings()|];
	values := [* *];
	for a in args do
		Append(~keys, a[1]);
		Append(~values, a[2]);
	end for;
    args:=JSONCreateNode(keys, values);
	ok,r:=keyvalue_JSON_request_with_args(db,operation,args : Timeout:=Timeout);
	return ok,r;
end function;

// Returns true, j where j is a JSON representation of the record r, and
// false, error message if there is a validation error.
function record_to_JSON(r)
	// Prepare the keys and values
	keys := [Strings()|];
	values := [* *];
	for k in Keys(r) do
		if not (Type(k) eq MonStgElt and #k gt 0) then
			return false, "Keys of record must be non-empty strings.";
		end if;
		// Check the value
		v := r[k];
		if not Type(v) in {BoolElt, MonStgElt, RngIntElt, FldReElt} then
			msg:=Sprintf("Value of key \"%o\" in record is of unsupported type: %o",k,Type(v));
			return false, msg;
		end if;
		// Record the key and value
		Append(~keys, k);
		Append(~values,v);
	end for;
	return true, JSONCreateNode(keys, values);
end function;

// Returns true, L where L is a sequence containing the JSON representations of
// the sequence of records rs, or false, error message if a validation error
// occurs.
function records_to_JSON(rs)
	result := [PowerStructure(Rec)|];
	for i in [1..#rs] do
		ok, j := record_to_JSON(rs[i]);
		if not ok then
			return false, Sprintf("Unable to convert record %o: %o", i, j);
		end if;
		Append(~result, j);
	end for;
	return true, result;
end function;

// Returns true, r where j is a JSON representation of the Record r, and
// false, error message otherwise.
function record_from_JSON(j)
	keys := JSONKeys(j);
	// The keys must be distinct
	if #keys ne #SequenceToSet(keys) then
		return false, malformed_JSON(j);
	end if;
	// The values must be integers, booleans, floats, or strings
	result := AssociativeArray(Strings());
	for k in JSONKeys(j) do
		v := JSONValue(j, k);
		if not Type(v) in {RngIntElt, BoolElt, FldReElt, MonStgElt} then
			msg:=Sprintf("Value of key \"%o\" in record is of unsupported type: %o",k,Type(v));
			return false, msg;
		end if;
		result[k] = v;
	end for;
	return true, result;
end function;

// Returns true, _ if the string t is non-empty with no leading or trailing
// whitespace, and false, error message otherwise.
function validate_table(t)
	if Trim(t) ne t or #t eq 0 then
		return false, "Invalid table name.";
	end if;
	return true, _;
end function;

// Returns true, _ if the string k is non-empty and equal to Trim(k), and
// false, error message otherwise.
function validate_key(k)
	if Trim(k) ne k or #k eq 0 then
		return false, "Invalid key.";
	end if;
	return true, _;
end function;

// Returns true followed by an example record based on j, or false, error
// message on error. Here j is a record with keys that are strings and values
// that are strings in the set
//
// { "int", "int8", "int16", "int32", "int64", "uint", "uint8", "uint16",
// "uint32", "uint64", "float64", "bool", "string", "[]byte" }
//
// An error message is returned if the key "[]byte" is present. Otherwise the
// return value has the same keys, and values as follows:
//
// "int", "int8", "int16", "int32", "int64", "uint", "uint8", "uint16",
// "uint32", "uint64" : Zero(Integers())
//
// "float64": Zero(RR) where RR := RealField(52 : Bits:=true)
//
// "bool": false
//
// "string": ""
function example_from_template(j)
	// Prepare the return value
	result := AssociativeArray(Strings());
	for k in JSONKeys(j) do
		// grab the corresponding value
		vals := JSONValues(j, k);
		if #vals gt 1 then
			return false, "Duplicate key: " cat k;
		end if;
		case vals[1]:
            when "int":
                v := Zero(Integers());
            when "int8":
                v := Zero(Integers());
            when "int16":
                v := Zero(Integers());
            when "int32":
                v := Zero(Integers());
            when "int64":
                v := Zero(Integers());
            when "uint":
                v := Zero(Integers());
            when "uint8":
                v := Zero(Integers());
            when "uint16":
                v := Zero(Integers());
            when "uint32":
                v := Zero(Integers());
            when "uint64":
                v := Zero(Integers());
			when "float64":
				RR := RealField(52 : Bits:=true);
				v := Zero(RR);
            when "bool":
                v := false;
            when "string":
                v := "";
			when "[]byte":
				return false, "Byte slices in key-value records are not supported.";
            else:
                msg := Sprintf("Unknown value type for key \"%o\": %o", k, v);
                return false, msg;
        end case;
		result[k] := v;
	end for;
	return true, result;
end function;

// Returns true, j where j is the JSON representation of the condition c, or
// false, error message if a validation error occurs.
function condition_to_JSON(c)
	// c could be an SQL string
	if Type(c) eq MonStgElt then
		return true, c;
	end if;
	// Otherwise c should be a record
	if Type(c) ne Assoc then
		return false, Sprintf("Unexpected condition type: %o", Type(c));
	end if;
	ok, j := record_to_JSON(c);
	if not ok then
		return false, j;
	end if;
	return true, JSONCreateNode([* "Selector", j*]);
end function;

// Returns true, n where n is the number of elements in table in the database db
// satisfying selector, or false, error message if an error occurs. The selector
// can be false, for no condition, or an SQL string or a record.
function count(db, table, selector : Timeout:=Timeout)
	// Check the table name
	ok, msg := validate_table(table);
	if not ok then
		return false, msg;
	end if;
	// Convert the selector, if present
	has_selector := false;
	if not selector cmpeq false then
		has_selector := true;
		ok, j := condition_to_JSON(selector);
		if not ok then
			return false, j;
		end if;
	end if;
	// Make the request
	args := JSONCreateNode([* "Table", table *]);
	if has_selector then
		JSONAppend(~args, "Where", j);
	end if;
	ok, j:=keyvalue_JSON_request_with_args(db, "count", args:Timeout:=Timeout);
	if not ok then
		return false, j;
	end if;
	// Extract the result
	ok,n:=integer_from_JSON(j, "Number");
	return ok,n;
end function;

// Updates the key-value records in table in the database db that match
// selector, replacing them with replacement. Returns true, n where n is the
// number of elements updated, or false, error message if an error occurs. The
// selector can be an SQL string or a record.
function update(db, table, selector, replacement : Timeout:=Timeout)
	// Check the table name
	ok, msg := validate_table(table);
	if not ok then
		return false, msg;
	end if;
	// Convert the selector
	ok, selector := condition_to_JSON(selector);
	if not ok then
		return false, selector;
	end if;
	// Convert the replacement
	ok, replacement := record_to_JSON(replacement);
	if not ok then
		return false, replacement;
	end if;
	// Make the request
	ok, j := keyvalue_JSON_request(db, "update",
				[* "Table", table *],
				[* "Where", selector *],
				[* "Replacement", replacement *]:
				Timeout:=Timeout);
	if not ok then
		return false, j;
	end if;
	// Extract the result
	ok,n:=integer_from_JSON(j, "Number");
	return ok,n;
end function;

// Deletes the key-value records in table in the database db that match
// selector, replacing them with replacement. Returns true, n where n is the
// number of elements deleted, or false, error message if an error occurs. The
// selector can be an SQL string or a record.
function del(db, table, selector, allow_empty_selector : Timeout:=Timeout)
	// Check the table name
	ok, msg := validate_table(table);
	if not ok then
		return false, msg;
	end if;
	// Convert the selector
	ok, selector := condition_to_JSON(selector);
	if not ok then
		return false, selector;
	end if;
	// Check if the selector is empty
	if not allow_empty_selector then
		if (Type(selector) eq MonStgElt and Trim(selector) eq "") or
			(Type(selector) eq Assoc and #Keys(selector) eq 0) then
			return false, "Illegal empty selector. To allow an empty selector, set the optional parameter 'allow_empty_selector' to true.";
		end if;
	end if;
	// Make the request
	ok, j := keyvalue_JSON_request(db, "delete",
				[* "Table", table *],
				[* "Where", selector *]:
				Timeout:=Timeout);
	if not ok then
		return false, j;
	end if;
	// Extract the result
	ok,n:=integer_from_JSON(j, "Number");
	return ok,n;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic ListTables(db::KvDb : Timeout:=0) -> SetEnum[MonStgElt]
{The set of tables in the key-value database db. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
	ok, j := keyvalue_JSON_request(db, "list_tables": Timeout:=Timeout);
	require ok: j;
	ok, S := strings_from_JSON(j, "Tables");
	require ok: S;
	return SequenceToSet(S);
end intrinsic;

intrinsic DescribeTable(db::KvDb, name::MonStgElt : Timeout:=0) -> Assoc
{A best-guess template for the data in the table with given name in the key-value database db. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
	// Sanity check
	ok, msg := validate_table(name);
	require ok: msg;
	// Make the request
	ok, j := keyvalue_JSON_request(db, "describe_table",
				[* "Table", name *]:
				Timeout:=Timeout);
	require ok: j;
	// Extract the description
	ok, r := JSONIsDefined(j, "Record");
	require ok: malformed_JSON_missing_key("Record", j);
	ok, result := example_from_template(r);
	require ok: result;
	return result;
end intrinsic;

intrinsic CreateTable(db::KvDb, name::MonStgElt, template::Assoc : Timeout:=0)
{Creates the table with given name and template in the key-value database db. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
	// Sanity check
	ok, msg := validate_table(name);
	require ok: msg;
	// Convert the template
	ok, template := record_to_JSON(template);
	require ok: template;
	// Make the request
	ok, j := keyvalue_JSON_request(db, "create_table",
				[* "Table", name *],
				[* "Template", template *]:
				Timeout:=Timeout);
	require ok: j;
end intrinsic;

intrinsic RenameTable(db::KvDb,old::MonStgElt,new::MonStgElt:Timeout:=0)
{Renames a table in the key-value database db from old to new. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
	// Check the names
	ok, msg := validate_table(old);
	require ok: msg;
	ok, msg := validate_table(new);
	require ok: msg;
	// Make the request
	ok, j := keyvalue_JSON_request(db, "rename_table",
				[* "OldName", old *],
				[* "NewName", new *]:
				Timeout:=Timeout);
	require ok: j;
end intrinsic;

intrinsic DeleteTable(db::KvDb, name::MonStgElt : Timeout:=0)
{Deletes the table with given name in the key-value database db. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
	// Check the table name
	ok, msg := validate_table(name);
	require ok: msg;
	// Make the request
	ok, j := keyvalue_JSON_request(db, "delete_table",
				[* "Table", name *]:
				Timeout:=Timeout);
	require ok: j;
end intrinsic;

intrinsic Count(db::KvDb, table::MonStgElt, selector::Assoc : Timeout:=0)
	-> RngIntElt
{The number of key-value records in the given table in the key-value database db that match the key-value record selector. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
	ok, n := count(db, table, selector : Timeout:=Timeout);
	require ok: n;
	return n;
end intrinsic;

intrinsic Count(db::KvDb, table::MonStgElt, selector::MonStgElt : Timeout:=0)
	-> RngIntElt
{The number of key-value records in the given table in the key-value database db that match the SQL string selector. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
	ok, n := count(db, table, selector : Timeout:=Timeout);
	require ok: n;
	return n;
end intrinsic;

intrinsic Count(db::KvDb, table::MonStgElt : Timeout:=0) -> RngIntElt
{The number of key-value records in the given table in the key-value database db. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
	ok, n := count(db, table, false : Timeout:=Timeout);
	require ok: n;
	return n;
end intrinsic;

intrinsic Insert(db::KvDb, table::MonStgElt, record::Assoc : Timeout:=0)
	-> RngIntElt
{Inserts the given key-value record into the given table in the key-value database db. Returns the number of records inserted, which will be 1 on success. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
	// Check the table name
	ok, msg := validate_table(table);
	require ok: msg;
	// Convert the record
	ok, r := record_to_JSON(record);
	require ok: r;
	// Make the request
	ok, j := keyvalue_JSON_request(db, "insert",
				[* "Table", table *],
				[* "Record", r *]:
				Timeout:=Timeout);
	require ok: j;
	// Extract the result
	ok, n := integer_from_JSON(j, "Number");
	require ok: n;
	return n;
end intrinsic;

intrinsic Insert(db::KvDb, table::MonStgElt, records::SeqEnum[Assoc] :
	Timeout:=0) -> RngIntElt
{Inserts the given sequence of key-value records into the given table in the key-value database db. Returns the number of records inserted, which will be #records on success. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
	// Check the table name
	ok, msg := validate_table(table);
	require ok: msg;
	// Convert the record
	ok, rs := records_to_JSON(records);
	require ok: rs;
	// Make the request
	ok, j := keyvalue_JSON_request(db, "insert",
				[* "Table", table *],
				[* "Records", rs *]:
				Timeout:=Timeout);
	require ok: j;
	// Extract the result
	ok, n := integer_from_JSON(j, "Number");
	require ok: n;
	return n;
end intrinsic;

intrinsic Update(db::KvDb,table::MonStgElt,selector::Assoc,replacement::Assoc :
	Timeout:=0) -> RngIntElt
{Updates all key-value records in the given table in the key-value database db that match the key-value record selector, setting the keys in the key-value record replacement to the corresponding values. Returns the number of records updated. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
	ok, n := update(db, table, selector, replacement : Timeout:=Timeout);
	require ok: n;
	return n;
end intrinsic;

intrinsic Update(db::KvDb,table::MonStgElt, selector::MonStgElt,
	replacement::Assoc : Timeout:=0) -> RngIntElt
{Updates all key-value records in the given table in the key-value database db that match the SQL string selector, setting the keys in the key-value record replacement to the corresponding values. Returns the number of records updated. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
	ok, n := update(db, table, selector, replacement : Timeout:=Timeout);
	require ok: n;
	return n;
end intrinsic;

intrinsic Delete(db::KvDb, table::MonStgElt, selector::Assoc :
	allow_empty_selector:=false, Timeout:=0) -> RngIntElt
{Deletes all key-value records in the given table in the key-value database db that match the key-value record selector. Returns the number of records deleted. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
	require Type(allow_empty_selector) eq BoolElt:
		"Optional parameter 'allow_empty_selector' must be a boolean.";
    ok, n := del(db, table, selector, allow_empty_selector : Timeout:=Timeout);
	require ok: n;
	return n;
end intrinsic;

intrinsic Delete(db::KvDb, table::MonStgElt, selector::MonStgElt :
	allow_empty_selector:=false, Timeout:=0) -> RngIntElt
{Deletes all key-value records in the given table in the key-value database db that match the SQL string selector. Returns the number of records deleted. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    require Type(allow_empty_selector) eq BoolElt:
		"Optional parameter 'allow_empty_selector' must be a boolean.";
    ok, n := del(db, table, selector, allow_empty_selector : Timeout:=Timeout);
	require ok: n;
	return n;
end intrinsic;

intrinsic ListIndices(db::KvDb,name::MonStgElt:Timeout:=0) -> SetEnum[MonStgElt]
{The set of keys for which indices are present in the table with the given name in the key-value database db. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
	ok, msg := validate_table(name);
	require ok: msg;
	ok, j := keyvalue_JSON_request(db, "list_indices",
						[* "Table", name *]: Timeout:=Timeout);
	require ok: j;
	ok, S := strings_from_JSON(j, "Indices");
	require ok: S;
	return SequenceToSet(S);
end intrinsic;

intrinsic AddIndex(db::KvDb, name::MonStgElt, key::MonStgElt : Timeout:=0)
{Adds an index on the given key in the table with the given name in the key-value database db. If an index already exists, this index is unmodified. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
	ok, msg := validate_table(name);
	require ok: msg;
	ok, msg := validate_key(key);
	require ok: msg;
	ok, j := keyvalue_JSON_request(db, "add_index",
									[* "Table", name *],
									[* "Key", key *]:
									Timeout:=Timeout);
	require ok: j;
end intrinsic;

intrinsic DeleteIndex(db::KvDb, name::MonStgElt, key::MonStgElt : Timeout:=0)
{Deletes all indices, if present, on the given key in the table with the given name in the key-value database db. This operation does not return an error if no index is present. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
	ok, msg := validate_table(name);
	require ok: msg;
	ok, msg := validate_key(key);
	require ok: msg;
	ok, j := keyvalue_JSON_request(db, "delete_index",
									[* "Table", name *],
									[* "Key", key *]:
									Timeout:=Timeout);
	require ok: j;
end intrinsic;

intrinsic AddKeys(db::KvDb, name::MonStgElt, record::Assoc : Timeout:=0)
{Updates each record r in t, adding any keys in record that are not already present along with the corresponding values. Any keys that are already present in r will be left unmodified. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
	ok, msg := validate_table(name);
	require ok: msg;
	ok, r := record_to_JSON(record);
	require ok: r;
	ok, j := keyvalue_JSON_request(db, "add_keys",
									[* "Table", name *],
									[* "Record", r *]:
									Timeout:=Timeout);
	require ok: j;
end intrinsic;

intrinsic DeleteKeys(db::KvDb, name::MonStgElt, keys::SeqEnum[MonStgElt] : Timeout:=0)
{Updates each record r in t, deleting all the specified keys if present. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
	ok, msg := validate_table(name);
	require ok: msg;
	// Validate the keys
	for i in [1..#keys] do
		ok, msg := validate_key(keys[i]);
		require ok: Sprintf("Error validating key %o: %o", i, msg);
	end for;
	ok, j := keyvalue_JSON_request(db, "delete_keys",
									[* "Table", name *],
									[* "Keys", keys *]:
									Timeout:=Timeout);
	require ok: j;
end intrinsic;

freeze;

/////////////////////////////////////////////////////////////////////////
// iterator.m
/////////////////////////////////////////////////////////////////////////
// Provides iterators for key-value selection.
/////////////////////////////////////////////////////////////////////////

import "../pcas.m": malformed_JSON, send_JSON_request, string_from_JSON;
import "keyvalue.m": validate_table, validate_key,
	keyvalue_JSON_request_with_args, condition_to_JSON, record_to_JSON;

declare type KvDbItr;
declare attributes KvDbItr:
        db,                 // The underlying database
        table,              // The name of the underlying table
        keys,               // The keys to extract
        cfs,                // The conversion functions
        require_all_keys,   // True if and only if we require all keys
        ULID,               // The ULID for the iterator
        is_closed,          // True if and only if the iterator is closed
        has_value,          // True if and only if the iterator has a value
        value;              // The current value, if set

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Validates the conversion functions, returning true, cfs on success and
// false, msg otherwise, where cfs is an associative array of conversion
// functions (empty if conversion_funcs is false; equal to conversion_funcs
// otherwise) and msg is an error message describing the failure. Keys of
// conversion_funcs that are not in 'keys' are removed.
function validate_conversion_funcs(conversion_funcs, keys)
	result:=AssociativeArray();
	if conversion_funcs cmpeq false then
		return true, result;
	end if;
	if not Type(conversion_funcs) eq Assoc then
		return false, "Optional parameter 'conversion_funcs' must be an associative array.";
	end if;
    for key in Keys(conversion_funcs) do
        if not Type(key) eq MonStgElt then
            return false, "Optional parameter 'conversion_funcs' must be an associative array whose keys are strings.";
		end if;
        f:=conversion_funcs[key];
        if not ISA(Type(f),Program) then
            return false, Sprintf("The value for key \"%o\" in optional parameter 'conversion_funcs' must be a Program.",key);
		end if;
        if key in keys then
            result[key]:=f;
        end if;
    end for;
	return true, result;
end function;

// Returns true, j where j is the JSON representation of the given sort order,
// or false, error message if a validation error occurs.
function order_to_JSON(order)
	// Validate the input
	if Type(order) ne SeqEnum then
        return false, "The sort order must be a sequence of tuples.";
    elif #order eq 0 and IsNull(order) then
		return true, false;
    end if;
    ok, order := CanChangeUniverse(order, CartesianProduct(Strings(), Integers()));
    if not ok then
        return false, "The sort order must be a sequence of <string, integer> tuples";
	elif #order eq 0 then
		return true, false;
    end if;
    for t in order do
        if #t[1] eq 0 then
            return false, "The sort order must be a sequence of <string, integer> tuples where the strings are non-empty";
        elif not t[2] in {-1,1} then
            return false, "The sort order must be a sequence of <string, integer> tuples where the integers are 1 (for increasing order) or -1 (for decreasing order).";
        end if;
    end for;
	// Return a sequence of Orders
	return true, [PowerStructure(Assoc) |
								JSONCreateNode([* t[1], t[2]*]) : t in order];
end function;

// Returns the record with JSON representation j, restricting the keys to only
// those given and converting values using the conversion functions cfs.
function convert_record(j, keys, cfs, require_all_keys)
	// Prepare the return value
	result := AssociativeArray(Strings());
	for k in JSONKeys(j) do
		// Grab the corresponding value
		vals := JSONValues(j, k);
		if #vals gt 1 then
			return false, malformed_JSON(j);
		end if;
		v := vals[1];
		// Check if we need to apply a conversion function
		ok, f := IsDefined(cfs, k);
		if ok then
			v := f(v);
		end if;
		result[k] := v;
	end for;
	// Check that we have all the keys, if appropriate
	if require_all_keys then
		kr := Keys(result);
		for k in keys do
			if not k in kr then
				return false, Sprintf("Missing key: %o", k);
			end if;
		end for;
	end if;
	return true, result;
end function;

// Returns true, r, true where r is an element in the table in the database db
// satisfying selector, or true, r, false if no such element exists (here r
// will be an empty AssociativeArray), or false, error message, false if an
// error occurs. The selector can be false, for no condition, or an SQL string
// or a record.
function select_one(db, table, template, selector, require_all_keys, conversion_funcs, order, Timeout)
	// Validate the table name
	ok, msg := validate_table(table);
	if not ok then
		return false, msg, false;
	end if;
	// If necessary extract the keys from the template, or build the template
	if Type(template) eq SetEnum then
		keys:=template;
		template:=AssociativeArray(Strings());
		for k in keys do
			template[k]:="";
		end for;
	else
		if not Universe(template) cmpeq Strings() then
			return false,"The keys of the template must be strings.",false;
		end if;
		keys:=Keys(template);
	end if;
	// Validate the keys
	for k in keys do
		ok, msg := validate_key(k);
		if not ok then
			return false,Sprintf("Error validating key \"%o\"': %o",k,msg),false;
		end if;
	end for;
	// Convert the selector, if present
	has_selector := false;
	if not selector cmpeq false then
		has_selector := true;
		ok, selector := condition_to_JSON(selector);
		if not ok then
			return false, selector, false;
		end if;
	end if;
	// Convert the order, if present
	has_order := false;
	if not order cmpeq false then
		has_order := true;
		ok, order := order_to_JSON(order);
		if not ok then
			return false, order, false;
		end if;
	end if;
	// Validate the conversion functions, if present
	if not conversion_funcs cmpeq false then
		ok, conversion_funcs:=validate_conversion_funcs(conversion_funcs,keys);
		if not ok then
			return false, conversion_funcs, false;
		end if;
	else
		conversion_funcs:=AssociativeArray(Strings());
	end if;
	// Validate the remaining optional parameters
	if Type(require_all_keys) ne BoolElt then
		return false,"Optional parameter 'require_all_keys' must be a boolean.",false;
	end if;
	// Build the JSON template
	ok, template := record_to_JSON(template);
	if not ok then
		return false, template, false;
	end if;
	// Build the request
	args := JSONCreateNode(
				[* "Table", table *],
				[* "Template", template *]);
	if has_selector then
		// Build the query
		if Type(selector) eq MonStgElt then
			if has_order then
				return false,"Cannot set order when an SQL selector is present.",false;
			end if;
			JSONAppend(~args, "Where", selector);
		else
			if has_order then
				JSONAppend(~selector, "Order", order);
			end if;
			JSONAppend(~args, "Where", selector);
		end if;
	end if;
	// Make the request
	ok,j:=keyvalue_JSON_request_with_args(db,"select_one",args : Timeout:=Timeout);
	if not ok then
		return false, j, false;
	end if;
	// Extract the record
	ok, r := JSONIsDefined(j, "Record");
	if not ok then
		return true, AssociativeArray(Strings()), false;
	end if;
	ok, rr := convert_record(r, keys, conversion_funcs, require_all_keys);
	if not ok then
		return false, rr, false;
	end if;
	return true, rr, true;
end function;

// Returns true followed by an iterator for the specified selector for the given
// table in the key-value database db. The argument 'template' can be a set of
// keys, or an associative array defining the template. The argument 'selector'
// can be false, for no condition, or an SQL string or a record. The argument
// 'limit' can be false, for no limit, or a positive integer. The argument
// 'order' can be false, for unspecified sort order, or a sequence of pairs
// <key, n> with n in {-1,1}. The buffer size must be a non-negative integer.
function sel(db, table, template, selector, require_all_keys, conversion_funcs,
	limit, order, buffer_size, Timeout)
	// Validate the table name
	ok, msg := validate_table(table);
	if not ok then
		return false, msg;
	end if;
	// If necessary extract the keys from the template, or build the template
	if Type(template) eq SetEnum then
		keys:=template;
		template:=AssociativeArray(Strings());
		for k in keys do
			template[k]:="";
		end for;
	else
		if not Universe(template) cmpeq Strings() then
			return false,"The keys of the template must be strings.";
		end if;
		keys:=Keys(template);
	end if;
	// Validate the keys
	for k in keys do
		ok, msg := validate_key(k);
		if not ok then
			return false, Sprintf("Error validating key \"%o\"': %o", k, msg);
		end if;
	end for;
	// Convert the selector, if present
	has_selector := false;
	if not selector cmpeq false then
		has_selector := true;
		ok, selector := condition_to_JSON(selector);
		if not ok then
			return false, selector;
		end if;
	end if;
	// Convert the order, if present
	has_order := false;
	if not order cmpeq false then
		has_order := true;
		ok, order := order_to_JSON(order);
		if not ok then
			return false, order;
		end if;
	end if;
	// Validate the conversion functions, if present
	has_conversion_funcs := false;
	if not conversion_funcs cmpeq false then
		has_conversion_funcs:=true;
		ok, conversion_funcs:=validate_conversion_funcs(conversion_funcs,keys);
		if not ok then
			return false, conversion_funcs;
		end if;
	end if;
	// Validate the limit
	has_limit := false;
	if not limit cmpeq false then
		has_limit := true;
		if Type(limit) ne RngIntElt  or limit le 0 then
			return false, "Optional parameter 'limit' must be a positive integer.";
		end if;
	end if;
	// Validate the remaining optional parameters
	if Type(require_all_keys) ne BoolElt then
		return false, "Optional parameter 'require_all_keys' must be a boolean.";
	elif Type(buffer_size) ne RngIntElt or buffer_size lt 0 then
		return false, "Optional parameter 'buffer_size' must be a non-negative integer.";
	end if;
	// Build the JSON template
	ok, template := record_to_JSON(template);
	if not ok then
		return false, template;
	end if;
	// Build the request
	args := JSONCreateNode(
				[* "Table", table *],
				[* "Template", template *],
				[* "BufferSize", buffer_size *]);
	if has_selector then
		// Build the query
		if Type(selector) eq MonStgElt then
			if has_limit or has_order then
				return false, "Cannot set limit or order when an SQL selector is present.";
			end if;
			JSONAppend(~args, "Where", selector);
		else
			if has_order then
				JSONAppend(~selector, "Order", order);
			end if;
			if has_limit then
				JSONAppend(~selector, "Limit", limit);
			end if;
			JSONAppend(~args, "Where", selector);
		end if;
	end if;
	// Make the request
	ok,j:=keyvalue_JSON_request_with_args(db,"select",args : Timeout:=Timeout);
	if not ok then
		return false, j;
	end if;
	// Extract the ULID
	ok, ULID := string_from_JSON(j, "ULID");
	if not ok then
		return false, ULID;
	elif Trim(ULID) ne ULID or #ULID ne 26 then
		return false, Sprintf("Malformed ULID: %o.", ULID);
	end if;
	// Wrap up the result as an iterator and return
	itr:=New(KvDbItr);
	itr`db:=db;
	itr`table:=table;
	itr`keys:=keys;
	itr`require_all_keys:=require_all_keys;
	itr`ULID:=ULID;
	itr`has_value:=false;
	itr`is_closed:=false;
	itr`cfs:=AssociativeArray(Strings());
	if has_conversion_funcs then
		itr`cfs:=conversion_funcs;
	end if;
	return true, itr;
end function;

// Returns true followed by the next value in the process, or false if the
// process has finished. The process is assumed to be wrapping a KvDbItr
// iterator.
function next_value(P)
	data:=GetUserProcessData(P);
	itr,Timeout:=Explode(data);
	ok,A:=Next(itr : Timeout:=Timeout);
	if not ok then
		Close(itr : Timeout:=Timeout);
		return false,_;
	end if;
	return true,A;
end function;

// Converts a KvDbItr iterator into a process.
function iterator_to_process(itr : Timeout:=Timeout)
	return UserProcess("Keyvalue Iterator",next_value,next_value,
															[* itr, Timeout *]);
end function;

////////////////////////////////////////////////////////////////////////////////
// Intrinsics
////////////////////////////////////////////////////////////////////////////////

intrinsic KeyValueDbProcess(db::KvDb, table::MonStgElt, keys::SetEnum[MonStgElt] :
	require_all_keys:=false, conversion_funcs:=false, limit:=false,
	buffer_size:=64, Timeout:=0) -> Process
{}
	ok, itr := sel(db, table, keys, false, require_all_keys,
	conversion_funcs, limit, false, buffer_size, Timeout);
	require ok: itr;
	return iterator_to_process(itr : Timeout:=Timeout);
end intrinsic;

intrinsic KeyValueDbProcess(db::KvDb, table::MonStgElt, template::Assoc :
	require_all_keys:=false, conversion_funcs:=false, limit:=false,
	buffer_size:=64, Timeout:=0) -> Process
{A process containing the records in the given table in the key-value database db. The optional parameter 'limit' controls the maximum number of records in the results set. The optional parameter 'buffer_size' (default: 64) sets the size of the iterator buffer; set this to zero to disable buffering. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
	ok, itr := sel(db, table, template, false, require_all_keys,
						conversion_funcs, limit, false, buffer_size, Timeout);
	require ok: itr;
	return iterator_to_process(itr : Timeout:=Timeout);
end intrinsic;

intrinsic KeyValueDbProcess(db::KvDb, table::MonStgElt, keys::SetEnum[MonStgElt],
	selector::Assoc : require_all_keys:=false, conversion_funcs:=false,
	limit:=false, buffer_size:=64, Timeout:=0) -> Process
{}
    ok, itr := sel(db, table, keys, selector, require_all_keys,
						conversion_funcs, limit, false, buffer_size, Timeout);
	require ok: itr;
	return iterator_to_process(itr : Timeout:=Timeout);
end intrinsic;

intrinsic KeyValueDbProcess(db::KvDb, table::MonStgElt, template::Assoc,
	selector::Assoc : require_all_keys:=false, conversion_funcs:=false,
	limit:=false, buffer_size:=64, Timeout:=0) -> Process
{A process containing the records in the given table in the key-value database db that match the key-value record selector. The optional parameter 'limit' controls the maximum number of records in the results set. The optional parameter 'buffer_size' [default: 64] sets the size of the iterator buffer; set this to zero to disable buffering. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    ok, itr := sel(db, table, template, selector, require_all_keys,
						conversion_funcs, limit, false, buffer_size, Timeout);
	require ok: itr;
	return iterator_to_process(itr : Timeout:=Timeout);
end intrinsic;

intrinsic KeyValueDbProcess(db::KvDb, table::MonStgElt, keys::SetEnum[MonStgElt],
	sql::MonStgElt : require_all_keys:=false, conversion_funcs:=false,
	buffer_size:=64, Timeout:=0) -> Process
{}
	ok, itr := sel(db, table, keys, sql, require_all_keys,
						conversion_funcs, false, false, buffer_size, Timeout);
	require ok: itr;
	return iterator_to_process(itr : Timeout:=Timeout);
end intrinsic;

intrinsic KeyValueDbProcess(db::KvDb, table::MonStgElt, template::Assoc,
	sql::MonStgElt : require_all_keys:=false, conversion_funcs:=false,
	buffer_size:=64, Timeout:=0) -> Process
{A process containing the records in the given table in the key-value database db that match the given SQL string. The SQL string can also specify the maximum number of records in the results set (LIMIT) and sort order (ORDER BY). The optional parameter 'buffer_size' [default: 64] sets the size of the iterator buffer; set this to zero to disable buffering. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
	ok, itr := sel(db, table, template, sql, require_all_keys,
						conversion_funcs, false, false, buffer_size, Timeout);
	require ok: itr;
	return iterator_to_process(itr : Timeout:=Timeout);
end intrinsic;

intrinsic KeyValueDbProcess(db::KvDb, table::MonStgElt, keys::SetEnum[MonStgElt],
	selector::Assoc, order::SeqEnum : require_all_keys:=false,
	conversion_funcs:=false,limit:=false,buffer_size:=64,Timeout:=0) -> Process
{}
    ok, itr := sel(db, table, keys, selector, require_all_keys,
						conversion_funcs, limit, order, buffer_size, Timeout);
	require ok: itr;
	return iterator_to_process(itr : Timeout:=Timeout);
end intrinsic;

intrinsic KeyValueDbProcess(db::KvDb, table::MonStgElt, template::Assoc,
	selector::Assoc, order::SeqEnum : require_all_keys:=false,
	conversion_funcs:=false,limit:=false,buffer_size:=64,Timeout:=0) -> Process
{A process containing the records in the given table in the key-value database db that match the key-value record selector. The records are sorted according to order; for example if order is [<"x", 1>, <"y", -1>] then results are sorted in increasing order in x, then decreasing order in y. If order is the empty sequence then results are returned in an unspecified order. The optional parameter 'limit' controls the maximum number of records in the results set. The optional parameter 'buffer_size' [default: 64] sets the size of the iterator buffer; set this to zero to disable buffering. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    ok, itr := sel(db, table, template, selector, require_all_keys,
						conversion_funcs, limit, order, buffer_size, Timeout);
	require ok: itr;
	return iterator_to_process(itr : Timeout:=Timeout);
end intrinsic;

intrinsic Select(db::KvDb, table::MonStgElt, keys::SetEnum[MonStgElt] :
	require_all_keys:=false, conversion_funcs:=false, limit:=false,
	buffer_size:=64, Timeout:=0) -> KvDbItr
{}
	ok, itr := sel(db, table, keys, false, require_all_keys,
						conversion_funcs, limit, false, buffer_size, Timeout);
	require ok: itr;
	return itr;
end intrinsic;

intrinsic Select(db::KvDb, table::MonStgElt, template::Assoc :
	require_all_keys:=false, conversion_funcs:=false, limit:=false,
	buffer_size:=64, Timeout:=0) -> KvDbItr
{An iterator containing the records in the given table in the key-value database db. You must call Close on the iterator when finished, otherwise resources may leak. The optional parameter 'limit' controls the maximum number of records in the results set. The optional parameter 'buffer_size' (default: 64) sets the size of the iterator buffer; set this to zero to disable buffering. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
	ok, itr := sel(db, table, template, false, require_all_keys,
						conversion_funcs, limit, false, buffer_size, Timeout);
	require ok: itr;
	return itr;
end intrinsic;

intrinsic Select(db::KvDb, table::MonStgElt, keys::SetEnum[MonStgElt],
	selector::Assoc : require_all_keys:=false, conversion_funcs:=false,
	limit:=false, buffer_size:=64, Timeout:=0) -> KvDbItr
{}
    ok, itr := sel(db, table, keys, selector, require_all_keys,
						conversion_funcs, limit, false, buffer_size, Timeout);
	require ok: itr;
	return itr;
end intrinsic;

intrinsic Select(db::KvDb, table::MonStgElt, template::Assoc,
	selector::Assoc : require_all_keys:=false, conversion_funcs:=false,
	limit:=false, buffer_size:=64, Timeout:=0) -> KvDbItr
{An iterator containing the records in the given table in the key-value database db that match the key-value record selector. You must call Close on the iterator when finished, otherwise resources may leak. The optional parameter 'limit' controls the maximum number of records in the results set. The optional parameter 'buffer_size' [default: 64] sets the size of the iterator buffer; set this to zero to disable buffering. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    ok, itr := sel(db, table, template, selector, require_all_keys,
						conversion_funcs, limit, false, buffer_size, Timeout);
	require ok: itr;
	return itr;
end intrinsic;

intrinsic Select(db::KvDb, table::MonStgElt, keys::SetEnum[MonStgElt],
	sql::MonStgElt : require_all_keys:=false, conversion_funcs:=false,
	buffer_size:=64, Timeout:=0) -> KvDbItr
{}
	ok, itr := sel(db, table, keys, sql, require_all_keys,
						conversion_funcs, false, false, buffer_size, Timeout);
	require ok: itr;
	return itr;
end intrinsic;

intrinsic Select(db::KvDb, table::MonStgElt, template::Assoc,
	sql::MonStgElt : require_all_keys:=false, conversion_funcs:=false,
	buffer_size:=64, Timeout:=0) -> KvDbItr
{An iterator containing the records in the given table in the key-value database db that match the given SQL string. The SQL string can also specify the maximum number of records in the results set (LIMIT) and sort order (ORDER BY). You must call Close on the iterator when finished, otherwise resources may leak. The optional parameter 'buffer_size' [default: 64] sets the size of the iterator buffer; set this to zero to disable buffering. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
	ok, itr := sel(db, table, template, sql, require_all_keys,
						conversion_funcs, false, false, buffer_size, Timeout);
	require ok: itr;
	return itr;
end intrinsic;

intrinsic Select(db::KvDb, table::MonStgElt, keys::SetEnum[MonStgElt],
	selector::Assoc, order::SeqEnum : require_all_keys:=false,
	conversion_funcs:=false,limit:=false,buffer_size:=64,Timeout:=0) -> KvDbItr
{}
    ok, itr := sel(db, table, keys, selector, require_all_keys,
						conversion_funcs, limit, order, buffer_size, Timeout);
	require ok: itr;
	return itr;
end intrinsic;

intrinsic Select(db::KvDb, table::MonStgElt, template::Assoc,
	selector::Assoc, order::SeqEnum : require_all_keys:=false,
	conversion_funcs:=false,limit:=false,buffer_size:=64,Timeout:=0) -> KvDbItr
{An iterator containing the records in the given table in the key-value database db that match the key-value record selector. The records are sorted according to order; for example if order is [<"x", 1>, <"y", -1>] then results are sorted in increasing order in x, then decreasing order in y. If order is the empty sequence then results are returned in an unspecified order. You must call Close on the iterator when finished, otherwise resources may leak. The optional parameter 'limit' controls the maximum number of records in the results set. The optional parameter 'buffer_size' [default: 64] sets the size of the iterator buffer; set this to zero to disable buffering. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    ok, itr := sel(db, table, template, selector, require_all_keys,
						conversion_funcs, limit, order, buffer_size, Timeout);
	require ok: itr;
	return itr;
end intrinsic;

intrinsic SelectOne(db::KvDb, table::MonStgElt, keys::SetEnum[MonStgElt],
	selector::Assoc : require_all_keys:=false, conversion_funcs:=false,
	Timeout:=0) -> Assoc, BoolElt
{}
	ok, r, hasRecord := select_one(db, table, keys, selector,
						require_all_keys, conversion_funcs, false, Timeout);
	require ok: r;
	return r, hasRecord;
end intrinsic;

intrinsic SelectOne(db::KvDb, table::MonStgElt, template::Assoc,
	selector::Assoc : require_all_keys:=false, conversion_funcs:=false,
	Timeout:=0) -> Assoc, BoolElt
{The first record returned from the specified table in the key-value database db that matches the key-value record selector, followed by true. If the results set is empty then an empty Associative Array is returned, followed by false. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
	ok, r, hasRecord := select_one(db, table, template, selector,
						require_all_keys, conversion_funcs, false, Timeout);
	require ok: r;
	return r, hasRecord;
end intrinsic;

intrinsic SelectOne(db::KvDb, table::MonStgElt, keys::SetEnum[MonStgElt] :
	require_all_keys:=false,conversion_funcs:=false,Timeout:=0) -> Assoc,BoolElt
{}
	ok, r, hasRecord := select_one(db, table, keys, false,
						require_all_keys, conversion_funcs, false, Timeout);
	require ok: r;
	return r, hasRecord;
end intrinsic;

intrinsic SelectOne(db::KvDb, table::MonStgElt, template::Assoc :
	require_all_keys:=false,conversion_funcs:=false,Timeout:=0) -> Assoc,BoolElt
{The first record returned from the specified table in the key-value database db, followed by true. If the results set is empty then an empty Associative Array is returned, followed by false. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
	ok, r, hasRecord := select_one(db, table, template, false,
						require_all_keys, conversion_funcs, false, Timeout);
	require ok: r;
	return r, hasRecord;
end intrinsic;

intrinsic SelectOne(db::KvDb, table::MonStgElt, keys::SetEnum[MonStgElt],
	selector::MonStgElt : require_all_keys:=false, conversion_funcs:=false,
	Timeout:=0) -> Assoc, BoolElt
{}
	ok, r, hasRecord := select_one(db, table, keys, selector,
						require_all_keys, conversion_funcs, false, Timeout);
	require ok: r;
	return r, hasRecord;
end intrinsic;

intrinsic SelectOne(db::KvDb, table::MonStgElt, template::Assoc,
	selector::MonStgElt : require_all_keys:=false, conversion_funcs:=false,
	Timeout:=0) -> Assoc, BoolElt
{The first record returned from the specified table in the key-value database db that matches the SQL string selector, followed by true. If the results set is empty then an empty Associative Array is returned, followed by false. The SQL string can also specify the maximum number of records in the results set (LIMIT) and sort order (ORDER BY). The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
	ok, r, hasRecord := select_one(db, table, template, selector,
						require_all_keys, conversion_funcs, false, Timeout);
	require ok: r;
	return r, hasRecord;
end intrinsic;

intrinsic SelectOne(db::KvDb, table::MonStgElt, keys::SetEnum[MonStgElt],
	selector::Assoc, order::SeqEnum : require_all_keys:=false,
	conversion_funcs:=false, Timeout:=0) -> Assoc, BoolElt
{}
	ok, r, hasRecord := select_one(db, table, keys, selector,
						require_all_keys, conversion_funcs, order, Timeout);
	require ok: r;
	return r, hasRecord;
end intrinsic;

intrinsic SelectOne(db::KvDb, table::MonStgElt, template::Assoc,
	selector::Assoc, order::SeqEnum : require_all_keys:=false,
	conversion_funcs:=false, Timeout:=0) -> Assoc, BoolElt
{The first record returned from the specified table in the key-value database db that matches the key-value record selector, followed by true. If the results set is empty then an empty Associative Array is returned, followed by false. The records are sorted according to order; for example if order is [<"x", 1>, <"y", -1>] then results are sorted in increasing order in x, then decreasing order in y. If order is the empty sequence then results are returned in an unspecified order. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
	ok, r, hasRecord := select_one(db, table, template, selector,
						require_all_keys, conversion_funcs, order, Timeout);
	require ok: r;
	return r, hasRecord;
end intrinsic;

intrinsic Print(itr::KvDbItr)
{Display the iterator itr.}
	if itr`is_closed then
		printf "Closed iterator for table \"%o\" in %o.", itr`table, itr`db;
		return;
	end if;
	val:=itr`has_value select itr`value else "none";
    printf "Iterator for table \"%o\" in %o.\n\tCurrent value:\n%o",
				itr`table, itr`db, ToString(val);
end intrinsic;

intrinsic Next(itr::KvDbItr : Timeout:=0) -> BoolElt, Assoc
{Advances the key-value iterator. Returns true followed by the next entry on success, and false at the end of iteration. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
	// Sanity check
	require not itr`is_closed: "The iterator is closed.";
    // Record that we no longer have a value
	itr`has_value := false;
	itr`value := false;
    // Send the request
	ok, j := send_JSON_request(itr`db`endpoint, "advance",
								[* "ULID", itr`ULID *]:
								Timeout:=Timeout);
    require ok: j;
	// Extract the ULID
	ok, u := string_from_JSON(j, "ULID");
	require ok: u;
	require u eq itr`ULID:
		Sprintf("ULID mismatch: expected \"%o\" but got \"%o\".", itr`ULID, u);
    // Are we at the end of iteration?
	ok, r := JSONIsDefined(j, "Record");
	if not ok then
		return false, _;
	end if;
	// Convert the record
	ok, result := convert_record(r, itr`keys, itr`cfs, itr`require_all_keys);
	require ok: result;
	// Record that we now have a value
	itr`has_value := true;
	itr`value := result;
	return true, result;
end intrinsic;

intrinsic Current(itr::KvDbItr) -> Assoc
{The current entry for the key-value iterator.}
	require not itr`is_closed: "The iterator is closed.";
    require itr`has_value: "Must only be called after Next returns success.";
    return itr`value;
end intrinsic;

intrinsic Close(itr::KvDbItr : Timeout:=0)
{Closes the key-value iterator, preventing further iteration and releasing any resources used. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
	// Is there anything to do?
	if itr`is_closed then
		return;
	end if;
	// Record that we no longer have a value
	itr`has_value := false;
	itr`value := false;
    // Send the request
	ok, j := send_JSON_request(itr`db`endpoint, "close",
								[* "ULID", itr`ULID *]:
								Timeout:=Timeout);
    require ok: j;
	itr`is_closed := true;
end intrinsic;

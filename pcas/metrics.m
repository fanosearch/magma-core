freeze;

/////////////////////////////////////////////////////////////////////////
// metrics.m
/////////////////////////////////////////////////////////////////////////
// This is the Magma side of the interface to the pcas metrics database.
/////////////////////////////////////////////////////////////////////////

import "pcas.m": validate_timeout, send_JSON_request, string_from_JSON,
    send_JSON_request_with_args, malformed_JSON_missing_key;
import "../timing/timing.m": set_timing_handler, set_timing_store_value,
    get_timing_store_value;

// Holds configuration information for a metricsdb server.
MetricsRec:=recformat<
    Address: MonStgElt  // The address of the server, as a hostname:port pair
                        // of the form "hostname[:port]" or
                        // "tcp://hostname[:port][/]", or a websocket URI of the
                        // form "ws://host/path".
>;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Validates the tags T. On success, returns true, j, where j is the JSON
// representation of T. Returns false, error message otherwise.
function validate_tags(T)
    // Create the empty JSON object
    j:=JSONCreateNode();
    // Check that this is either 'false' or an associative array
    if Type(T) cmpeq BoolElt and not T then
        return true, j;
    elif not Type(T) cmpeq Assoc then
        return false, "Optional parameter 'Tags' must be an associative array.";
    end if;
    // Add the tags
    for k in Keys(T) do
        if not Type(k) cmpeq MonStgElt then
            return false, "Optional parameter 'Tags' must be an associative array whose keys are strings.";
        end if;
        v:=T[k];
        if not Type(v) cmpeq MonStgElt then
            return false, Sprintf("The value for key \"%o\" in optional parameter 'Tags' must be a string.",k);
        end if;
        JSONAppend(~j,k,v);
    end for;
    return true,j;
end function;

// Validates the connection c. On success returns true, j, where j is the JSON
// representation of c. Returns false, error message otherwise.
function validate_connection(c)
    // Create the empty JSON object
    j:=JSONCreateNode();
    // Check that this is either 'false' or a record
    if Type(c) cmpeq BoolElt and not c then
        return true, j;
	elif not Type(c) cmpeq Rec then
		return false, "Optional parameter 'Connection' must be a record.";
	end if;
    // Add the address
    if "Address" in Names(c) and assigned c`Address then
        if not Type(c`Address) eq MonStgElt then
            return false, "Value for Address on optional parameter 'Connection' must be a string.";
        elif #c`Address ne 0 then
            JSONAppend(~j, "Address", c`Address);
        end if;
    end if;
	return true, j;
end function;

// A simple wrapper around send_JSON_request_with_args for making requests
// from the endpoint.
function metrics_JSON_request(operation, ... : Timeout:=0, Connection:=false)
    // Sanity check
    ok, Connection := validate_connection(Connection);
    if not ok then
        return false, Connection;
    end if;
    // Tidy up the input
	args := operation[2..#operation];
	operation := operation[1];
	// Package up the arguments
	keys := [Strings()|];
	values := [* *];
	for a in args do
		Append(~keys, a[1]);
		Append(~values, a[2]);
	end for;
    args:=JSONCreateNode(keys, values);
    // Add the connection
    if JSONNumberOfKeys(Connection) ne 0 then
        JSONAppend(~args, "Connection", Connection);
    end if;
	// Make the request
	ok,r:=send_JSON_request_with_args("metrics", operation, args :
			Timeout:=Timeout);
	return ok,r;
end function;

// Submits a point with the given name and fields. Returns true on success,
// false followed by an error message otherwise.
function submit_point(name, field, ... :
    Tags:=false, Timeout:=0, Connection:=false)
    // Sanity check
    ok, Tags := validate_tags(Tags);
    if not ok then
        return false, Tags;
    end if;
    // Submit the point
    ok,msg:=metrics_JSON_request("submit",
        [* "Name", name *], [* "Tags", Tags *], [* "Fields", field *] :
        Timeout:=Timeout, Connection:=Connection);
    if not ok then
        return false,msg;
    end if;
    return true,_;
end function;

// Passes the given timing data to pcas-magma.
procedure pcas_metrics_handler(name,t)
    // Grab the timeout, connection, and tags
    bool,Timeout:=get_timing_store_value("pcas_metrics_timeout");
    assert bool;
    bool,Connection:=get_timing_store_value("pcas_metrics_connection");
    assert bool;
    bool,Tags:=get_timing_store_value("pcas_metrics_tags");
    assert bool;
    // Create the field
    t:=Ceiling(10^9 * t); // Nanoseconds
    field:=JSONCreateNode([* "Key", "duration" *], [* "Duration", t *]);
    // Create the arguments
    args:=JSONCreateNode(
        [* "Name", name *], [* "Tags", Tags *], [* "Fields", [* field *] *]);
    // Add the connection
    if JSONNumberOfKeys(Connection) ne 0 then
        JSONAppend(~args, "Connection", Connection);
    end if;
	// Make the request
    _:=send_JSON_request_with_args("metrics", "submit", args :
			Timeout:=Timeout);
end procedure;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic MetricsDefaults() -> Rec
{Configuration information for the pcas-magma metrics server.}
    // Query the server
    ok, j := send_JSON_request("metrics","defaults");
    require ok: j;
    // Extract the address and return
    ok,conn:=JSONIsDefined(j,"Connection");
    require ok: malformed_JSON_missing_key("Connection",j);
    ok,address:=string_from_JSON(conn,"Address");
    require ok: address;
    return rec<MetricsRec | Address:=address>;
end intrinsic;

intrinsic SetTimingToPcas( : Tags:=false, Timeout:=0, Connection:=false)
{Sets timing data to be sent to the pcas-magma metrics server, and enables the collection of timing data. The optional parameter 'Timeout' is the timeout for submitting timing data, measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    // Sanity check
    require IsPcasMagma(): "Not running pcas-magma.";
    ok,Timeout:=validate_timeout(Timeout);
    require ok: Timeout;
    ok,Connection:=validate_connection(Connection);
    require ok: Connection;
    ok,Tags:=validate_tags(Tags);
    require ok: Tags;
    // Save the values in the store
    set_timing_store_value("pcas_metrics_timeout",Timeout);
    set_timing_store_value("pcas_metrics_connection",Connection);
    set_timing_store_value("pcas_metrics_tags",Tags);
    // Set the handler
    set_timing_handler(pcas_metrics_handler);
end intrinsic;

intrinsic MetricsClearDefaultTags()
{Clears the default tags that are sent with each metrics point.}
    ok,msg:=send_JSON_request("metrics","clear_default_tags");
    require ok: msg;
end intrinsic;

intrinsic MetricsAddDefaultTags(T::Assoc)
{Adds the given tags T to the default tags that are sent with each metrics point. The keys and values of the associative array T must be strings.}
    // Sanity check
    for k in Keys(T) do
        require Type(k) cmpeq MonStgElt: "Argument keys must be strings.";
        require Type(T[k]) cmpeq MonStgElt:
            Sprintf("Argument value for key \"%o\" must be a string.",k);
    end for;
    // Update the tags
    ok,msg:=send_JSON_request("metrics","add_default_tags",[* "Tags", T *]);
    require ok: msg;
end intrinsic;

intrinsic MetricsSubmitDuration(name::MonStgElt, key::MonStgElt, t::FldReElt :
    Tags:=false, Timeout:=0, Connection:=false)
{Submits a point with the given name to the metrics server. The point contains a single field with given key and duration t, measured in seconds. The optional parameter 'Tags' can be used to define tags to associate with this point (in addition to any default tags already set). The optional parameter 'Timeout' is the timeout for the submission of the point, measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    t:=Ceiling(10^9 * t); // Nanoseconds
    field:=JSONCreateNode([* "Key", key *], [* "Duration", t *]);
    ok,msg:=submit_point(name, field :
        Tags:=Tags, Timeout:=Timeout, Connection:=Connection);
    require ok: msg;
end intrinsic;

intrinsic MetricsSubmit(name::MonStgElt, key::MonStgElt, value::BoolElt :
    Tags:=false, Timeout:=0, Connection:=false)
{}
    field:=JSONCreateNode([* "Key", key *], [* "Boolean", value *]);
    ok,msg:=submit_point(name, field :
        Tags:=Tags, Timeout:=Timeout, Connection:=Connection);
    require ok: msg;
end intrinsic;

intrinsic MetricsSubmit(name::MonStgElt, key::MonStgElt, value::FldReElt :
    Tags:=false, Timeout:=0, Connection:=false)
{}
    field:=JSONCreateNode([* "Key", key *], [* "Float", value *]);
    ok,msg:=submit_point(name, field :
        Tags:=Tags, Timeout:=Timeout, Connection:=Connection);
    require ok: msg;
end intrinsic;

intrinsic MetricsSubmit(name::MonStgElt, key::MonStgElt, value::RngIntElt :
    Tags:=false, Timeout:=0, Connection:=false)
{}
    field:=JSONCreateNode([* "Key", key *], [* "Integer", value *]);
    ok,msg:=submit_point(name, field :
        Tags:=Tags, Timeout:=Timeout, Connection:=Connection);
    require ok: msg;
end intrinsic;

intrinsic MetricsSubmit(name::MonStgElt, key::MonStgElt, value::MonStgElt :
    Tags:=false, Timeout:=0, Connection:=false)
{Submits a point with the given name to the metrics server. The point contains a single field with given key and value. The optional parameter 'Tags' can be used to define tags to associate with this point (in addition to any default tags already set). The optional parameter 'Timeout' is the timeout for the submission of the point, measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    field:=JSONCreateNode([* "Key", key *], [* "String", value *]);
    ok,msg:=submit_point(name, field :
        Tags:=Tags, Timeout:=Timeout, Connection:=Connection);
    require ok: msg;
end intrinsic;

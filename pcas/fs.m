freeze;

/////////////////////////////////////////////////////////////////////////
// fs.m
/////////////////////////////////////////////////////////////////////////
// This is the Magma side of the interface to the pcas filesystem.
/////////////////////////////////////////////////////////////////////////

import "pcas.m": send_JSON_request, send_JSON_request_with_args,
    malformed_JSON, malformed_JSON_missing_key, string_from_JSON,
    integer_from_JSON, bool_from_JSON, strings_from_JSON;

// Holds configuration information for an fs server.
FsRec:=recformat<
    Address: MonStgElt  // The address of the server, as a hostname:port pair
                        // of the form "hostname[:port]" or
                        // "tcp://hostname[:port][/]", or a websocket URI of
                        // the form "ws://host/path"
>;

// Metadata for a file or directory.
FsMetadata:=recformat<
    Path: MonStgElt,                // The path of the file or directory
    Size: RngIntElt,                // The size of the file; undefined for
                                    // directories
    ModificationTime: RngIntElt,    // The time that the file was last modified,
                                    // expressed as the number of seconds since
                                    // January 1, 1970 UTC
    IsDir: BoolElt                  // True if and only if this path is a
                                    // directory
>;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Validates the connection c. On success returns true, j, where j is the JSON
// representation of c. Returns false, error message otherwise.
function validate_connection(c)
    // Create the empty JSON object
    j:=JSONCreateNode();
    // Check that this is either 'false' or a record
    if Type(c) cmpeq BoolElt and not c then
        return true, j;
	elif not Type(c) cmpeq Rec then
		return false, "Optional parameter 'Connection' must be a record.";
	end if;
    // Add the address
    if "Address" in Names(c) and assigned c`Address then
        if not Type(c`Address) eq MonStgElt then
            return false, "Value for Address on optional parameter 'Connection' must be a string.";
        elif #c`Address ne 0 then
            JSONAppend(~j, "Address", c`Address);
        end if;
    end if;
	return true, j;
end function;

// A simple wrapper around send_JSON_request_with_args for making requests
// from the endpoint.
function fs_JSON_request_with_args(operation,args:Timeout:=0,Connection:=false)
    // Sanity check
    ok, Connection := validate_connection(Connection);
    if not ok then
        return false, Connection;
    end if;
    // Add the connection
    if JSONNumberOfKeys(Connection) ne 0 then
        JSONAppend(~args, "Connection", Connection);
    end if;
	// Make the request
	ok,r:=send_JSON_request_with_args("fs", operation, args : Timeout:=Timeout);
	return ok,r;
end function;

// A simple wrapper around send_JSON_request_with_args for making requests
// from the endpoint.
function fs_JSON_request(operation, ... : Timeout:=0, Connection:=false)
    // Tidy up the input
	args := operation[2..#operation];
	operation := operation[1];
	// Package up the arguments
	keys := [Strings()|];
	values := [* *];
	for a in args do
		Append(~keys, a[1]);
		Append(~values, a[2]);
	end for;
	args:=JSONCreateNode(keys,values);
	// Make the request
	ok,r:=fs_JSON_request_with_args(operation, args :
                Timeout:=Timeout, Connection:=Connection);
	return ok,r;
end function;

// Parses the JSON representation j of file metadata. Returns true followed by
// an FsMetadata object on success, and false, error message otherwise.
function metadata_from_JSON(j)
    ok, path := string_from_JSON(j, "Path");
    if not ok then
        return false, path;
    end if;
    ok, size := integer_from_JSON(j, "Size");
    if not ok then
        return false, size;
    end if;
    ok, mod_time := integer_from_JSON(j, "ModificationTime");
    if not ok then
        return false, mod_time;
    end if;
    ok, is_dir := bool_from_JSON(j, "IsDir");
    if not ok then
        return false, is_dir;
    end if;
    return true, rec<FsMetadata |
        Path:=path,
        Size:=size,
        ModificationTime:=Ceiling(mod_time/10^9), // Convert to seconds
        IsDir:=is_dir>;
end function;

// Returns true followed by the directory listing for p on success, false
// followed by an error messahe otherwise.
function list_dir(p : Timeout:=0, Connection:=false)
    // Send the request
    ok,j:=fs_JSON_request("list_dir", [* "Path",p *] :
                Timeout:=Timeout, Connection:=Connection);
    if not ok then
        return false,j;
    end if;
    // Extract the names
    ok,names:=strings_from_JSON(j,"Listing");
    if not ok then
        return false,names;
    end if;
    Sort(~names);
    return true,names;
end function;

// Performs an "exists-type" operation with path p. Returns true, b on success,
// where b is the value of the given boolean field in the result, and false,
// error message otherwise.
function fs_is_thing(operation,p,field:Timeout:=Timeout,Connection:=Connection)
    // Send the request
    ok,j:=fs_JSON_request(operation, [* "Path", p *]:
                Timeout:=Timeout, Connection:=Connection);
    if not ok then
        return false,j;
    end if;
    // Extract the result
    ok,b:=bool_from_JSON(j,field);
    if not ok then
        return false,b;
    end if;
    return true,b;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic FsDefaults() -> Rec
{Configuration information for the pcas-magma file system.}
    // Query the server
    ok,j:=send_JSON_request("fs","defaults");
    require ok: j;
    // Extract the address and return
    ok,conn:=JSONIsDefined(j,"Connection");
    require ok: malformed_JSON_missing_key("Connection",j);
    ok,address:=string_from_JSON(conn,"Address");
    require ok: address;
    return rec<FsRec | Address:=address>;
end intrinsic;

intrinsic FsWorkingDir( : Connection:=false) -> MonStgElt
{The absolute pathname of the current working directory.}
    // Query the server
    ok,j:=fs_JSON_request("get_working_dir" : Connection:=Connection);
    require ok: j;
    // Extract the working directory
    ok,p:=string_from_JSON(j,"Path");
    require ok: p;
    return p;
end intrinsic;

intrinsic FsSetWorkingDir(p::MonStgElt : Timeout:=0, Connection:=false)
{Set the current working directory. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    ok,j:=fs_JSON_request("set_working_dir", [* "Path",p *] :
                Timeout:=Timeout, Connection:=Connection);
    require ok: j;
end intrinsic;

intrinsic FsExists(p::MonStgElt : Timeout:=0, Connection:=false) -> BoolElt
{True if and only if a file or directory with the given path exists. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    ok,b:=fs_is_thing("exists",p,"Exists" :
                Timeout:=Timeout, Connection:=Connection);
    require ok: b;
    return b;
end intrinsic;

intrinsic FsIsFile(p::MonStgElt : Timeout:=0, Connection:=false) -> BoolElt
{True if and only if the given path exists and is a file. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    ok,b:=fs_is_thing("is_file",p,"IsFile" :
                Timeout:=Timeout, Connection:=Connection);
    require ok: b;
    return b;
end intrinsic;

intrinsic FsIsDir(p::MonStgElt : Timeout:=0, Connection:=false) -> BoolElt
{True if and only if the given path exists and is a directory. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    ok,b:=fs_is_thing("is_dir",p,"IsDir" :
                Timeout:=Timeout, Connection:=Connection);
    require ok: b;
    return b;
end intrinsic;

intrinsic FsIsEmptyDir(p::MonStgElt : Timeout:=0, Connection:=false) -> BoolElt
{True if and only if the given path exists and is an empty directory. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    ok,b:=fs_is_thing("is_empty_dir",p,"IsEmptyDir" :
                Timeout:=Timeout, Connection:=Connection);
    require ok: b;
    return b;
end intrinsic;

intrinsic FsMetadata(p::MonStgElt : Timeout:=0, Connection:=false) -> Rec
{The metadata for the given path. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    ok,j:=fs_JSON_request("metadata",[* "Path", p *] :
                Timeout:=Timeout, Connection:=Connection);
    require ok: j;
    ok,m:=metadata_from_JSON(j);
    require ok: m;
    return m;
end intrinsic;

intrinsic FsMakeDir(p::MonStgElt :
    Recursive:=false, Timeout:=0, Connection:=false)
{Makes an empty directory with given path. If the optional parameter 'Recursive' is set to true [default: false] then intermediate subdirectories are also created. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    require Type(Recursive) eq BoolElt:
        "Optional parameter 'Recursive' must be a boolean.";
    args:=JSONCreateNode([* "Path",p *]);
    if Recursive then
        JSONAppend(~args,"Recursive",true);
    end if;
    ok,j:=fs_JSON_request_with_args("mkdir",args :
                Timeout:=Timeout, Connection:=Connection);
    require ok: j;
end intrinsic;

intrinsic FsRead(p::MonStgElt :
    MaxSize:=false, Timeout:=0, Connection:=false) -> MonStgElt
{Open the file p and return its complete contents as a string. The optional parameter 'MaxSize' can be used to restrict the number of bytes read; if the file exceeds MaxSize then a runtime error will be raised. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    require (Type(MaxSize) eq BoolElt and MaxSize eq false) or
        (Type(MaxSize) eq RngIntElt and MaxSize gt 0):
        "Optional parameter 'MaxSize' must be a positive integer.";
    args:=JSONCreateNode([* "Path",p *]);
    if Type(MaxSize) eq RngIntElt then
        JSONAppend(~args,"MaxSize",MaxSize);
    end if;
    ok,j:=fs_JSON_request_with_args("cat",args :
                Timeout:=Timeout, Connection:=Connection);
    require ok: j;
    ok,d:=string_from_JSON(j,"Content");
    require ok: d;
    return d;
end intrinsic;

intrinsic FsReplace(p::MonStgElt, S::MonStgElt : Timeout:=0, Connection:=false)
{Writes the string S to the file p. If p already exists then its contents will be overwritten. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    ok,j:=fs_JSON_request("replace",[* "Path",p *],[* "Data",S *] :
                Timeout:=Timeout, Connection:=Connection);
    require ok: j;
end intrinsic;

intrinsic FsUpload(src::MonStgElt, dst::MonStgElt :
    Timeout:=0, Connection:=false)
{Uploads the file or directory src on the local file system to the path dst. If src is a file then dst must not already exist, and will be created. If src is a directory then dst must either not exist, in which case it will be created, or must be an empty directory; in either case the contents of src are uploaded recursively to dst. In the case that the source path points to a directory then symlinks within the source directory will be ignored. Similarly, any paths within the source directory whose final element begins with a '.' will be ignored. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    // Send the request
    ok,j:=fs_JSON_request("upload",[* "Source",src *],[* "Destination",dst *] :
                Timeout:=Timeout, Connection:=Connection);
    require ok: j;
end intrinsic;

intrinsic FsDownload(src::MonStgElt, dst::MonStgElt :
    Timeout:=0, Connection:=false)
{Downloads the file or directory src to the path dst on the local file system. If src is a file then dst must not already exist, and will be created. If src is a directory then dst must either not exist, in which case it will be created, or must be an empty directory; in either case the contents of src are downloaded recursively to dst. Any paths within the source directory whose final element begins with a '.' will be ignored. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    ok,j:=fs_JSON_request("download",[* "Source",src *],[* "Destination",dst *]:
                Timeout:=Timeout, Connection:=Connection);
    require ok: j;
end intrinsic;

intrinsic FsDelete(p::MonStgElt :
    Recursive:=false, Timeout:=0, Connection:=false)
{Deletes the file or directory with given path p. If p is a directory and the optional parameter 'Recursive' is set to true [default: false], the contents of p are recursively removed; otherwise, if p is a directory then it must be empty. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    require Type(Recursive) eq BoolElt:
        "Optional parameter 'Recursive' must be a boolean.";
    args:=JSONCreateNode([* "Path",p *]);
    if Recursive then
        JSONAppend(~args,"Recursive",true);
    end if;
    ok,j:=fs_JSON_request_with_args("delete",args :
                Timeout:=Timeout, Connection:=Connection);
    require ok: j;
end intrinsic;

intrinsic FsCopy(src::MonStgElt, dst::MonStgElt :
    Recursive:=false, Timeout:=0, Connection:=false)
{Copies the file at the path src to the path dst. If the optional parameter 'Recursive' is true [default: false] then, in the case where the source path points to a directory, the contents of that directory will recursively copied. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    require Type(Recursive) eq BoolElt:
        "Optional parameter 'Recursive' must be a boolean.";
    args:=JSONCreateNode([* "Source",src *],[* "Destination",dst *]);
    if Recursive then
        JSONAppend(~args,"Recursive",true);
    end if;
    ok,j:=fs_JSON_request_with_args("copy",args :
                Timeout:=Timeout, Connection:=Connection);
    require ok: j;
end intrinsic;

intrinsic FsListDir(p::MonStgElt : Timeout:=0, Connection:=false)
    -> SeqEnum[MonStgElt]
{A sequence containing the names of files and directories in the given directory. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    ok,names:=list_dir(p : Timeout:=0, Connection:=false);
    require ok: names;
    return names;
end intrinsic;

intrinsic FsListDir( : Timeout:=0, Connection:=false) -> SeqEnum[MonStgElt]
{A sequence containing the names of files and directories in the working directory. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    ok,names:=list_dir("" : Timeout:=0, Connection:=false);
    require ok: names;
    return names;
end intrinsic;

intrinsic FsHash(p::MonStgElt, hash::MonStgElt :
    Timeout:=0, Connection:=false) -> MonStgElt
{Computes the hash of the file with given path. Here "hash" is the (case insensitive) name of the hash to use. The following hash names are supported: "MD5", "SHA1", "SHA224", "SHA256", "SHA384", "SHA512", "SHA512/224", "SHA512/256", "SHA3-224", "SHA3-256", "SHA3-384", "SHA3-512", "BLAKE2s-256", "BLAKE2b-256", "BLAKE2b-384", and "BLAKE2b-512". The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    // Sanity check on the hash name
    require ToUpper(hash) in {"MD5","SHA1","SHA224","SHA256","SHA384","SHA512",
        "SHA512/224","SHA512/256","SHA3-224","SHA3-256","SHA3-384","SHA3-512",
        "BLAKE2S-256","BLAKE2B-256","BLAKE2B-384","BLAKE2B-512"}:
        Sprintf("Argument 2 (%o) is not a valid hash name.",hash);
    // Send the request
    ok,j:=fs_JSON_request("hash", [* "Path",p *], [* "HashName",hash *] :
                Timeout:=Timeout, Connection:=Connection);
    require ok: j;
    // Extract the hash
    ok,h:=string_from_JSON(j,"Hash");
    require ok: h;
    return h;
end intrinsic;

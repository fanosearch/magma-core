freeze;

/////////////////////////////////////////////////////////////////////////
// gobencode.m
/////////////////////////////////////////////////////////////////////////
// Intrinsics for gobencoding and gobdecoding data
/////////////////////////////////////////////////////////////////////////

import "pcas.m": send_JSON_request, malformed_JSON, malformed_JSON_missing_key,
    string_from_JSON, strings_from_JSON;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Returns true followed by the gob encoding of an object in the given universe,
// or false and an error message on failure. The object should be a sequence of
// key-value pairs [* "key", value *].
function encode(universe, ...)
    // Tidy up the input
    args := universe[2..#universe];
    universe := universe[1];
    // Package up the Value object
    keys := [Strings()|];
    values := [* *];
    for x in args do
        Append(~keys, x[1]);
        Append(~values, x[2]);
    end for;
    value := JSONCreateNode(keys, values);
    // Make the request
    ok, j := send_JSON_request("gob","encode",
                [* "Universe", universe *],
                [* "Value", value *]);
    if not ok then
        return false, j;
    end if;
    // Check the universe
    ok,u:=string_from_JSON(j,"Universe");
    if not ok then
        return false, u;
    elif u ne universe then
        return false, Sprintf("Expected universe \"%o\" but received universe \"%o\".", universe, u);
    end if;
    // Extract the encoding
    ok,e:=JSONIsDefined(j,"Encoding");
    if not ok then
        return false, malformed_JSON_missing_key("Encoding",j);
    end if;
    return true, e;
end function;

// Decodes the gob-encoded string e into the given universe, returning true, j
// on success and false, error message otherwise, where j is the JSON
// representation of the value encoded by e.
function decode(universe, e)
    // Make the request
    ok, j := send_JSON_request("gob","decode",
                [* "Universe", universe *],
                [* "Encoding", e *]);
    if not ok then
        return false, j;
    end if;
    // Check the universe
    ok,u:=string_from_JSON(j,"Universe");
    if not ok then
        return false, u;
    elif u ne universe then
        return false, Sprintf("Expected universe \"%o\" but received universe \"%o\".", universe, u);
    end if;
    // Extract the value
    ok,v:=JSONIsDefined(j,"Value");
    if not ok then
        return false, malformed_JSON_missing_key("Value",j);
    end if;
    return true, v;
end function;

// Returns [[S[1],...,S[k]],[S[k+1],...,S[2k]],...]. Raises an error if #S is not divisible by k.
function unflatten(S, k)
    error if not IsDivisibleBy(#S, k), "unexpected sequence length";
    m := ExactQuotient(#S,k);
    return [[S[i] : i in [(n-1)*k+1..n*k]] : n in [1..m]];
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic ToGob(x::SeqEnum[RngIntElt]) -> MonStgElt
{A base64-encoded string representing the gob encoding of x.}
    ok, e := encode("[integers]",[* "Array",[IntegerToString(n) : n in x] *]);
    require ok: e;
    return e;
end intrinsic;

intrinsic GobToIntegerSequence(s::MonStgElt) -> SeqEnum[RngIntElt]
{Decodes s, a base64-encoded gob encoding of a sequence of integers.}
    ok, j := decode("[integers]", s);
    require ok: j;
    // Extract the sequence
    ok, S := strings_from_JSON(j,"Array");
    require ok: S;
    // Convert the sequence to integers
    T:=[Integers()|];
    for i in [1..#S] do
        ok,n:=IsStringInteger(S[i]);
        require ok: Sprintf("Unable to convert entry %o (\"%o\") to an integer.",i,S[i]);
        Append(~T,n);
    end for;
    return T;
end intrinsic;

intrinsic ToGob(x::SeqEnum[FldRatElt]) -> MonStgElt
{A base64-encoded string representing the gob encoding of x.}
    ok, e := encode("[rationals]",[* "Array",[ToString(n) : n in x] *]);
    require ok: e;
    return e;
end intrinsic;

intrinsic GobToRationalSequence(s::MonStgElt) -> SeqEnum[FldRatElt]
{Decodes s, a base64-encoded gob encoding of a sequence of rationals.}
    ok, j := decode("[rationals]", s);
    require ok: j;
    // Extract the sequence
    ok, S := strings_from_JSON(j,"Array");
    require ok: S;
    // Convert the sequence to rationals
    T:=[Rationals()|];
    for i in [1..#S] do
        ok,q:=IsStringRational(S[i]);
        require ok: Sprintf("Unable to convert entry %o (\"%o\") to a rational.",i,S[i]);
        Append(~T,q);
    end for;
    return T;
end intrinsic;

/* 
To encode a Laurent polynomial with integer coefficients, f, first convert it to parallel sequences cs, es of coefficients and exponents. Then construct the sequence E that concatenates [Eltseq(e) : e in es], and encode f by 

F := [r, #cs] cat cs cat E. 

The encoding of f is the encoding of the integer sequence F.
*/

intrinsic ToGob(f::FldFunRatMElt) -> MonStgElt
{A base64-encoded string representing the gob encoding of the Laurent polynomial f with integer coefficients.}
    require IsLaurent(f): "The argument must be a Laurent polynomial.";
    cs, es := CoefficientsAndExponents(f);
    ok, cs := CanChangeUniverse(cs, Integers());
    require ok: "The coefficients of the argument must be integers.";
    r := Rank(Parent(f));
    E := &cat[Eltseq(e) : e in es];
    return ToGob([Integers()|r, #cs] cat cs cat E);
end intrinsic;

intrinsic GobToLaurent(s::MonStgElt) -> FldFunRatMElt
{Decodes s, a base64-encoded gob encoding of a Laurent polynomial with integer coefficients.}
    // Recover the integer sequence
    S := GobToIntegerSequence(s);
    // Extract the rank etc.
    r := S[1];
    n := S[2];
    cs := S[3..n+2];
    es := unflatten(S[n+3..#S], r);
    // Handle the zero case
    if n eq 0 then
        return Zero(RationalFunctionField(Rationals(), r));
    end if;
    // Reconstruct the Laurent polynomial
    return LaurentPolynomial(es, cs);
end intrinsic;

/* 
To encode an element L of the Weyl algebra over the rationals with integer coefficients, first convert it to parallel sequences cs, es of coefficients and exponents, then construct the sequence E that flattens es, and then set

F := [#cs] cat cs cat E. 

The encoding of L is the encoding of the integer sequence F.
*/

intrinsic ToGob(L::AlgFPElt) -> MonStgElt
{A base64-encoded string representing the gob encoding of the element W of the Weyl algebra over the rationals with integer coefficients.}
    require L in WeylAlgebra(): "The argument must be an element of the Weyl algebra over the rational numbers.";
    cs, es := CoefficientsAndExponents(L);
    ok, cs := CanChangeUniverse(cs, Integers());
    require ok: "The coefficients of the argument must be integers.";
    E := &cat[e : e in es];
    return ToGob([#cs] cat cs cat E);
end intrinsic;

intrinsic GobToWeyl(s::MonStgElt) -> AlgFPElt
{Decodes s, a base64-encoded gob encoding of an element W of the Weyl algebra over the rationals with integer coefficients.}
    // Recover the integer sequence
    S := GobToIntegerSequence(s);
    // Extract the coefficients and exponents
    n := S[1];
    cs := S[2..n+1];
    es := unflatten(S[n+2..#S], 2);
    // Reconstruct the Weyl algebra element
    W := WeylAlgebra();
    return &+[W| cs[i]*W.2^es[i][2]*W.1^es[i][1] : i in [1..#cs]];
end intrinsic;
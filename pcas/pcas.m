freeze;

/////////////////////////////////////////////////////////////////////////
// pcas.m
/////////////////////////////////////////////////////////////////////////
// This is the Magma side of the go-magma wrapper that allows Magma
// to access our pcas infrastructure (logging, etc.)
/////////////////////////////////////////////////////////////////////////

import "../core.m": save_data, fetch_data;

PcasDetails:=recformat<
	Major: RngIntElt,		// The major version number
	Minor: RngIntElt,		// The minor version number
	Patch: RngIntElt,		// The patch version number
	GitCommit: MonStgElt,	// The git commit version
	GitDirty: BoolElt,		// Was the git repo dirty?
	GoArch: MonStgElt,		// The CPU architecture
	GoOS: MonStgElt,		// The OS
	Date: RngIntElt			// The build date (in seconds since Jan 1, 1970 UTC)
>;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Returns true followed by an IOSocket connected to the go-magma server, or
// false follwed by an error message. The port to listen on will be read from
// the enviroment variable PCAS_MAGMA_PORT.
function get_wrapper_port()
    // Grab the port number from the environment
    s:=GetEnv("PCAS_MAGMA_PORT");
    if #s eq 0 then
		return false, "The environment variable PCAS_MAGMA_PORT is not assigned.";
	end if;
    ok,port:=IsStringInteger(s);
	if not ok then
		return false, "The environment variable PCAS_MAGMA_PORT must be an integer.";
	elif port le 0 or port gt 65535 then
		msg:=Sprintf("The environment variable PCAS_MAGMA_PORT (%o) must be in the range 1..65535.",port);
		return false, msg;
	end if;
    // Open a socket connected to this port
    count := 0;
	ok:=false;
	while not ok do
		try
			S:=Socket("localhost", port);
			ok:=true;
		catch e
			// pass
			;
		end try;
		if not ok then
			count +:= 1;
			if count eq 5 then
				msg:=Sprintf("Unable to open socket to localhost:%o.",port);
				return false, msg;
			end if;
			Sleep(1);
		end if;
	end while;
	// The socket has been opened, so return success
    return true,S;
end function;

// Sends the handshake block down the socket S to the pcas-magma wrapper.
// Returns true on success, false followed by an error message otherwise.
function send_handshake(S)
    j := JSONCreateNode(["SendLengthPrefix"], [* true *]);
	ok:=false;
	try
    	Write(S, JSONToString(j));
		ok:=true;
	catch e
		// pass
		;
	end try;
	if not ok then
		return false,"Unable to write handshake to socket.";
	end if;
	return true,_;
end function;

// Returns true followed by the socket used to communicate with pcas-magma,
// false followed by an error message otherwise.
function get_socket()
	// Is there anything to do?
	ok,S:=fetch_data("_pcas_magma_socket");
	if ok then
		return true,S;
	end if;
	// Open the socket and send the handshake
	ok,S:=get_wrapper_port();
	if not ok then
		return false,S;
	end if;
	ok,msg:=send_handshake(S);
	if not ok then
		return false,msg;
	end if;
	// Record and return the socket
    save_data("_pcas_magma_socket",S);
	return true,S;
end function;

// If necessary, augments the given error message with the JSON. Returns the new
// message.
function augment_error_msg(msg,j)
	if #msg eq 0 then
		msg:="Malformed JSON";
	elif #msg lt 14 or msg[1..14] ne "Malformed JSON" then
		msg:="Malformed JSON: " cat msg;
	end if;
	if FanosearchIsVerbose() then
		jstr:=JSONToString(j);
		if #jstr gt 80 then
			jstr:=jstr[1..77] cat "...";
		end if;
		msg cat:= ": " cat jstr;
	elif msg[#msg] ne "." then
		msg cat:= ".";
	end if;
	return msg;
end function;

// Returns an error string explaining that the given JSON is malformed.
function malformed_JSON(j)
	return augment_error_msg("",j);
end function;

// Returns an error string explaining that the given key is missing from the
// given JSON.
function malformed_JSON_missing_key(key,j)
	msg:=Sprintf("Missing \"%o\" key",key);
	return augment_error_msg(msg,j);
end function;

// Returns true followed by the specified value in the JSON element j, if that
// value is of the specified type, and false, error message otherwise.
function type_from_JSON(j, key, type)
	ok,v:=JSONIsDefined(j,key);
	if not ok then
		return false,malformed_JSON_missing_key(key, j);
	elif Type(v) ne type then
		msg:=Sprintf("Value for key \"%o\" must be of type %o, but found type %o",key,type,Type(v));
		return false,augment_error_msg(msg,j);
	end if;
	return true, v;
end function;

// Returns true followed by the specified value in the JSON element j, if that
// value is a string, and false, error message otherwise.
function string_from_JSON(j, key)
	ok, v := type_from_JSON(j, key, MonStgElt);
	return ok, v;
end function;

// Returns true followed by the specified value in the JSON element j, if that
// value is an integer, and false, error message otherwise.
function integer_from_JSON(j, key)
	ok, v := type_from_JSON(j, key, RngIntElt);
	return ok, v;
end function;

// Returns true followed by the specified value in the JSON element j, if that
// value is a boolean, and false, error message otherwise.
function bool_from_JSON(j, key)
	ok, v := type_from_JSON(j, key, BoolElt);
	return ok, v;
end function;

// Returns true followed by the specified value in the JSON element j, if that
// value is a sequence of strings, and false, error message otherwise.
function strings_from_JSON(j, key)
	ok, S := type_from_JSON(j, key, List);
	if not ok then
		return false, S;
	end if;
	result := [Strings()|];
	for i in [1..#S] do
		s:=S[i];
		if Type(s) ne MonStgElt then
			msg:=Sprintf("Entry %o for key \"%o\" must be of type MonStgElt, but found type %o",i,key,Type(s));
			return false, augment_error_msg(msg,j);
		end if;
		Append(~result, s);
	end for;
	return true, result;
end function;

// Validates the timeout t, returning true, t on success and false, error
// message otherwise.
function validate_timeout(t)
    if not (Type(t) cmpeq RngIntElt) then
        ok, t := IsCoercible(RealField(),t);
        if not ok then
            return false, "Optional parameter 'Timeout' must be a non-negative number.";
        end if;
    end if;
    if t lt 0 then
        return false, "Optional parameter 'Timeout' must be a non-negative number.";
    end if;
    return true, t;
end function;

// Validates the maximum wait time t, returning true, t on success and false, error
// message otherwise.
function validate_wait(t)
    if not (Type(t) cmpeq RngIntElt) then
        ok, t := IsCoercible(RealField(),t);
        if not ok then
            return false, "Optional parameter 'MaxWait' must be a non-negative number.";
        end if;
    end if;
    if t lt 0 then
        return false, "Optional parameter 'MaxWait' must be a non-negative number.";
    end if;
    return true, t;
end function;

// Returns the smallest of two timeouts, handling zeroes (which mean "no timeout") appropriately.
function combine_timeouts(t1, t2)
	if t1 eq 0 then
		return t2;
	elif t2 eq 0 then
		return t1;
	end if;
	return Min(t1,t2);
end function;

// Validates the given request data and returns true followed by the request
// JSON on success, or false followed by an error message otherwise.
function create_request(u,endpoint,operation,args : Timeout:=0)
	// Sanity check
	if not IsPcasMagma() then
		return false, "Not running pcas-magma.";
	elif #endpoint eq 0 and #operation ne 0 then
		return false, "Missing endpoint.";
	elif #operation eq 0 and JSONNumberOfKeys(args) ne 0 then
		return false, "Missing operation.";
	end if;
	ok,Timeout:=validate_timeout(Timeout);
	if not ok then
		return false, Timeout;
	end if;
	// Create the JSON
	req := JSONCreateNode([* "ID", u *]);
	if #endpoint ne 0 then
		JSONAppend(~req, "Endpoint", endpoint);
	end if;
	if #operation ne 0 then
		JSONAppend(~req, "Operation", operation);
	end if;
	if JSONNumberOfKeys(args) ne 0 then
		JSONAppend(~req, "Arguments", args);
	end if;
	if Timeout ne 0 then
		JSONAppend(~req, "Timeout", Ceiling(10^9*Timeout)); // Nanoseconds
	end if;
	return true, req;
end function;

// Returns true iff the given response JSON has ID set and equal to u.
function is_ulid_equal_to(resp, u)
	ok, s := string_from_JSON(resp,"ID");
	return ok and s eq u;
end function;

// Validates the given response JSON against the given expected values. Returns
// true on success, false followed by an error message otherwise.
function validate_response(resp, endpoint, operation)
	if #endpoint eq 0 then
		if JSONIsDefined(resp, "Endpoint") then
			ok, s := string_from_JSON(resp, "Endpoint");
			if not ok then
				return false, s;
			elif #s ne 0 then
				msg:=Sprintf("Unexpected endpoint in response: %o",s);
				return false, augment_error_msg(msg,resp);
			end if;
		end if;
	else
		ok, s := string_from_JSON(resp, "Endpoint");
		if not ok then
			return false, s;
		elif s ne endpoint then
			msg:=Sprintf("Response endpoint (%o) does not match the request endpoint (%o)", s, endpoint);
			return false, augment_error_msg(msg,resp);
		end if;
	end if;
	if #operation eq 0 then
		if JSONIsDefined(resp, "Operation") then
			ok, s := string_from_JSON(resp, "Operation");
			if not ok then
				return false, s;
			elif #s ne 0 then
				msg:=Sprintf("Unexpected operation in response: %o",s);
				return false, augment_error_msg(msg,resp);
			end if;
		end if;
	else
		ok, s := string_from_JSON(resp, "Operation");
		if not ok then
			return false, s;
		elif s ne operation then
			msg:=Sprintf("Response operation (%o) does not match the request operation (%o)", s, operation);
			return false, augment_error_msg(msg,resp);
		end if;
	end if;
	return true,_;
end function;

// Reads from the socket S until the first match of
//	[1-9][0-9]*\n{
// Once matched, returns true followed by the corresponding integer N
// represented by base-10 string given by the integers. Returns false followed
// by an error message otherwise. Note that this will consume the initial '{'.
function read_size_and_open_brace(S)
	// Note: You need to know the following bytes:
	//  * The new-line '\n' byte code is 10;
	//  * The integers '0'-'9' map to byte codes 48-57, respectively;
	//  * The open brace '{' maps to the byte code 123.
	// We loop until we recover a block of the form
	//	[1-9][0-9]*\n{
	// or until the socket closes.
	while true do
		// Read in the sequence of integers up to '\n', discarding junk as
		// we go
		bytes:=[];
		repeat
			msg:=ReadBytes(S,1);
			if #msg eq 0 then
				return false,"Socket closed unexpectedly.";
			end if;
			b:=msg[1];
			if b ge 48 and b le 57 then
				if b eq 48 and #bytes eq 0 then
					// This is junk, so discard
					bytes:=[];
				else
					Append(~bytes,b);
				end if;
			elif b ne 10 then
				// This is junk, so discard
				bytes:=[];
			end if;
		until b eq 10 and #bytes ne 0;
		// The next byte of data should be an open brace
		msg:=ReadBytes(S,1);
		if #msg eq 0 then
			return false,"Socket closed unexpectedly.";
		end if;
		b:=msg[1];
		if b eq 123 then
			// Looks good -- recover the integer encoded by the bytes and return
			return true,StringToInteger(BytesToString(bytes));
		end if;
	end while;
end function;

// Reads a block of JSON from the pcas-magma socket. Returns true followed by
// the block on success, false followed by an error message otherwise.
function read_JSON()
	// Get the communication socket
	ok,S:=get_socket();
	if not ok then
		return false,S;
	end if;
	// Read the length of the JSON block -- note that this consumes the
	// opening curly brace '{'
	ok,N:=read_size_and_open_brace(S);
	if not ok then
		return false,N;
	end if;
	// Now read the JSON
	msg:=ReadBytes(S,N-1);
	if #msg lt N-1 then
		return false,"Short read from socket.";
	end if;
	// Looks good -- remember to add the opening curly brace '{'
	j:=JSONParseString("{" cat BytesToString(msg));
	save_data("_pcas_magma_last_read",j);
	return true,j;
end function;

// Writes the JSON object j to the pcas-magma wrapper. Returns true on success,
// false followed by an error message otherwise.
function write_JSON(j)
	ok,S:=get_socket();
	if not ok then
		return false,S;
	end if;
	Write(S,JSONToString(j));
	save_data("_pcas_magma_last_write",j);
	return true,_;
end function;

// send_JSON_request_with_args sends a request with the given operation,
// JSON-formatted arguments, and timeout to the specified endpoint. Returns
// true, result on success, where result is the JSON from the Result field.
// Otherwise returns false, error message. The timeout is given in seconds.
function send_JSON_request_with_args(endpoint, operation, args : Timeout:=0)
	// Make an ID for the request
	u:=ULIDNew();
	// Create the request
	ok,req:=create_request(u,endpoint,operation,args : Timeout:=Timeout);
	if not ok then
		return false,req;
	end if;
	// Query the server
    ok,msg:=write_JSON(req);
    if not ok then
		return false,msg;
	end if;
    // Read the response -- because there may be stale data in the socket, we
	// keep reading until the JSON block has the expected ID
	repeat
		ok,resp:=read_JSON();
		if not ok then
			return false,resp;
		end if;
	until is_ulid_equal_to(resp, u);
	// Is the response what we are expecting?
	ok,msg:=validate_response(resp, endpoint, operation);
	if not ok then
		return false,msg;
	end if;
	// Was there an error?
	ok,e:=JSONIsDefined(resp, "Error");
	if ok then
		return false,e;
	end if;
	// Result may or may not be set
	ok,r:=JSONIsDefined(resp, "Result");
	if not ok then
		r:=JSONCreateNode();
	end if;
	return true,r;
end function;

// send_JSON_request sends a request with the given operation, arguments and
// timeout to the specified endpoint. Each element of args should be a key-value
// pair [* "key", value *]. Returns true, result on success, where result is the
// JSON from the Result field. Otherwise returns false, error message. The
// timeout is given in seconds.
function send_JSON_request(endpoint, operation, ... : Timeout:=0)
	// Tidy up the input
	args := operation[2..#operation];
	operation := operation[1];
	// Package up the arguments
	keys := [Strings()|];
	values := [* *];
	for a in args do
		Append(~keys, a[1]);
		Append(~values, a[2]);
	end for;
	args:=JSONCreateNode(keys,values);
	// Make the call
	ok,r:=send_JSON_request_with_args(endpoint, operation, args :
			Timeout:=Timeout);
	return ok,r;
end function;

/////////////////////////////////////////////////////////////////////////
// Internal Intrinsics
/////////////////////////////////////////////////////////////////////////

/*
The following intrinsics may be useful when debugging. They are not intended to
be used as part of your production code.
*/

intrinsic InternalPcasMagmaLastWrite() -> Rec
{The last JSON document written to the pcas-magma socket.}
	require IsPcasMagma(): "Not running pcas-magma.";
	ok,j:=fetch_data("_pcas_magma_last_write");
	require ok: "No JSON has been written to pcas-magma.";
	return j;
end intrinsic;

intrinsic InternalPcasMagmaLastRead() -> Rec
{The last JSON document read from the pcas-magma socket.}
	require IsPcasMagma(): "Not running pcas-magma.";
	ok,j:=fetch_data("_pcas_magma_last_read");
	require ok: "No JSON has been read from pcas-magma.";
	return j;
end intrinsic;

intrinsic InternalPcasEndpoints() -> SetEnum
{The endpoints available over pcas-magma.}
	ok,j:=send_JSON_request("","");
	require ok: j;
	ok, endpoints := JSONIsDefined(j,"Endpoints");
	require ok: malformed_JSON_missing_key("Endpoints",j);
	require Type(endpoints) cmpeq List: malformed_JSON(j);
	return {Strings() | x : x in endpoints};
end intrinsic;

intrinsic InternalPcasOperations(endpoint::MonStgElt) -> SetEnum
{The operations available over pcas-magma for the given endpoint.}
	require #endpoint ne 0: "Illegal empty endpoint";
	ok,j:=send_JSON_request(endpoint,"");
	require ok: j;
	ok, operations := JSONIsDefined(j,"Operations");
	require ok: malformed_JSON_missing_key("Operations",j);
	require Type(operations) cmpeq List: malformed_JSON(j);
	return {Strings() | x : x in operations};
end intrinsic;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic IsPcasMagma() -> BoolElt
{True if and only if this is pcas-magma.}
    return #GetEnv("PCAS_MAGMA_PORT") ne 0;
end intrinsic;

intrinsic PcasMagmaSetAppName(name::MonStgElt)
{Set the default name by which we identify ourselves.}
	save_data("_pcas_magma_app_name",name);
end intrinsic;

intrinsic PcasMagmaGetAppName() -> MonStgElt
{The default name by which we identify ourselves.}
	ok,name:=fetch_data("_pcas_magma_app_name");
	return ok select name else "pcas-magma";
end intrinsic;

intrinsic PcasMagmaGetVersion() -> RngIntElt, RngIntElt, RngIntElt
{Integers x, y and z such that the current version of pcas-magma is "vx.y.z".}
	// Make the request
	ok,j:=send_JSON_request("version","get_version");
	require ok: j;
	// Extract the version values
	v:=[];
	for name in ["Major","Minor","Patch"] do
		ok,n:=integer_from_JSON(j,name);
		require ok: n;
		Append(~v,n);
	end for;
	// Return the values
	return v[1],v[2],v[3];
end intrinsic;

intrinsic PcasMagmaGetVersionDetails() -> Rec
{A record containing details for the current version of pcas-magma. The record contains the version number (Major, Minor, and Patch), the git commit string and status (GitCommit and GitDirty), the system architecture and operating system (GoArch and GoOS), and the build time (Date) given in seconds since Jan 1, 1970 UTC.}
	// Make the request
	ok,j:=send_JSON_request("version","get_version");
	require ok: j;
	// Extract the version values
	v:=[];
	for name in ["Major","Minor","Patch"] do
		ok,n:=integer_from_JSON(j,name);
		require ok: n;
		Append(~v,n);
	end for;
	// Extract the git commit string
	ok,commit:=string_from_JSON(j,"GitCommit");
	require ok: commit;
	// Was the git repo dirty?
	ok,dirty:=bool_from_JSON(j,"GitDirty");
	require ok: dirty;
	// Extract the architecture
	ok,arch:=string_from_JSON(j,"GoArch");
	require ok: arch;
	// Extract the operating system
	ok,os:=string_from_JSON(j,"GoOS");
	require ok: os;
	// Extract the build date
	ok,date:=integer_from_JSON(j,"Date");
	require ok: date;
	// Return the details in a record
	return rec<PcasDetails |
		Major:=v[1],
		Minor:=v[2],
		Patch:=v[3],
		GitCommit:=commit,
		GitDirty:=dirty,
		GoArch:=arch,
		GoOS:=os,
		Date:=Ceiling(date / 10^9) // Convert from nanoseconds to seconds
	>;
end intrinsic;

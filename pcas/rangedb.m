freeze;

/////////////////////////////////////////////////////////////////////////
// rangedb.m
/////////////////////////////////////////////////////////////////////////
// This is the Magma side of the interface to the pcas rangedb server.
/////////////////////////////////////////////////////////////////////////

import "pcas.m": send_JSON_request, send_JSON_request_with_args,
    malformed_JSON, malformed_JSON_missing_key, validate_timeout, validate_wait, combine_timeouts, string_from_JSON, integer_from_JSON, strings_from_JSON;

// Holds configuration information for a rangedb server.
RangeDbRec:=recformat<
    Address: MonStgElt  // The address of the server, as a hostname:port pair
                        // of the form "hostname[:port]" or
                        // "tcp://hostname[:port][/]", or a websocket URI of the
                        // form "ws://host/path".
>;

// An iterator for ranges stored in a rangedb server.
declare type RgeDbItr;
declare attributes RgeDbItr:
	name,       // The range name
    app_name,   // The name by which we identify ourselves to the rangedb server
    connection, // The connection to use
    value,      // The current value
    deadline,   // The time at which this entry will go stale, expressed as a
                // floating point number of seconds since 1 Jan 1970 UTC
    id,         // A ULID that identifies this value in the range
    failures,   // The number of times that this entry has failed
    has_value;  // True if and only if we currently hold a value

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Erases any value held by the RgeDbItr itr.
procedure zero_out(itr)
    itr`id := "";
    itr`has_value := false;
    itr`value := 0;
    itr`failures := 0;
    itr`deadline := 0;
end procedure;

// Parses a JSON representation of a range. Returns true followed by a list
// with entries of the form n or [a,b] on success, false followed by an error
// message otherwise.
function range_from_JSON(j)
    // j should be of the form [Interval, ..., Interval]
    if Type(j) ne List then
        return false,malformed_JSON(j);
    end if;
    // Parse the intervals
    intervals:=[* *];
    for x in j do
        if Type(x) cmpeq RngIntElt then
            Append(~intervals, x);
        elif Type(x) cmpeq List and #x eq 2 then
            if Type(x[1]) ne RngIntElt or Type(x[2]) ne RngIntElt then
                return false,malformed_JSON(j);
            end if;
            Append(~intervals,[Integers() | x[1],x[2]]);
        else
            return false,malformed_JSON(j);
        end if;
    end for;
    return true,intervals;
end function;

// Converts the given range list to a string.
function range_to_string(I)
    // Create the string for each interval in the range
    intervals:=[Strings()|];
    for x in I do
        if Type(x) cmpeq RngIntElt then
            Append(~intervals,IntegerToString(x));
        else
            s:=IntegerToString(x[1]) cat ".." cat IntegerToString(x[2]);
            Append(~intervals,s);
        end if;
    end for;
    // Assemble the result and return
    return "[" cat Join(intervals,",") cat "]";
end function;

// Attempts to parse the string S to a range list. Returns true followed by the
// list on success, false followed by an error message otherwise.
function string_to_range(S)
    I:=[* *];
    // Get the empty string out of the way
    S:=Trim(S);
    if #S eq 0 then
        return true,I;
    end if;
    // If there are surrounding square brackets, remove them
    if S[1] eq "[" then
        if S[#S] eq "]" then
            S:=S[2..#S-1];
        else
            return false,"Mismatched brackets.";
        end if;
    elif S[#S] eq "]" then
        return false,"Mismatched brackets.";
    end if;
    // Work through the components
    for s in Split(S,",") do
        ss:=Trim(s);
        if #ss eq 0 then
            return false,"Invalid range.";
        end if;
        idx:=Index(ss,"..");
        if idx eq 0 then
            ok,n:=IsStringInteger(ss);
            if not ok then
                return false,"Invalid range.";
            end if;
            Append(~I,n);
        elif idx eq 1 or idx ge #ss-1 then
            return false,"Invalid range.";
        else
            s1:=Trim(ss[1..idx-1]);
            ok,a:=IsStringInteger(s1);
            if not ok then
                return false,"Invalid range.";
            end if;
            s2:=Trim(ss[idx+2..#ss]);
            ok,b:=IsStringInteger(s2);
            if not ok then
                return false,"Invalid range.";
            end if;
            Append(~I,[a,b]);
        end if;
    end for;
    // Looks good
    return true,I;
end function;

// Converts the deadline d to a timeout. If d is in the past, returns a timeout
// of one nanosecond.
function deadline_to_timeout(d)
    if d cmpeq false then
        return 0;
    end if;
    t := d - Realtime();
    // The deadline could be in the past
    if t le 0 then
        t := 10.0^-9;
    end if;
    return t;
end function;

// Performs the iterative step for RangeDbForAll.
function iterative_step(itr,n,f : Deadline:=false)
    // Call the user function
    ok:=false;
    success:=false;
    try
        ok:=f(n);
        success:=true;
    catch e
        m:=Sprintf("Error calling user function in RangeDbForAll: %o",e`Object);
        Logger(m);
    end try;
    // Did the user function succeed?
    if success then
        // Attempt to mark the iterative step as a success
        try
            Success(itr : Timeout:=deadline_to_timeout(Deadline));
        catch e
            m:=Sprintf("Error calling Success in RangeDbForAll: %o",e`Object);
            Logger(m);
        end try;
    else
        // Attempt to mark the iterative step as a failure
        try
            Error(itr : Timeout:=deadline_to_timeout(Deadline));
        catch e
            m:=Sprintf("Error calling Error in RangeDbForAll: %o",e`Object);
            Logger(m);
        end try;
        ok:=true;
    end if;
    // Check whether the deadline has passed
    if (not Deadline cmpeq false) and (Realtime() gt Deadline) then
        ok := false;
    end if;
    return ok;
end function;

// Validates the connection c. On success returns true, j, where j is the JSON
// representation of c. Returns false, error message otherwise.
function validate_connection(c)
    // Create the empty JSON object
    j:=JSONCreateNode();
    // Check that this is either 'false' or a record
    if Type(c) cmpeq BoolElt and not c then
        return true, j;
	elif not Type(c) cmpeq Rec then
		return false, "Optional parameter 'Connection' must be a record.";
	end if;
    // Add the address
    if "Address" in Names(c) and assigned c`Address then
        if not Type(c`Address) eq MonStgElt then
            return false, "Value for Address on optional parameter 'Connection' must be a string.";
        elif #c`Address ne 0 then
            JSONAppend(~j, "Address", c`Address);
        end if;
    end if;
	return true, j;
end function;

// A simple wrapper around send_JSON_request_with_args for making requests
// from the endpoint.
function rangedb_JSON_request(operation, ... : Timeout:=0, Connection:=false)
    // Sanity check
    ok, Connection := validate_connection(Connection);
    if not ok then
        return false, Connection;
    end if;
    // Tidy up the input
	args := operation[2..#operation];
	operation := operation[1];
	// Package up the arguments
	keys := [Strings()|];
	values := [* *];
	for a in args do
		Append(~keys, a[1]);
		Append(~values, a[2]);
	end for;
    args:=JSONCreateNode(keys, values);
    // Add the connection
    if JSONNumberOfKeys(Connection) ne 0 then
        JSONAppend(~args, "Connection", Connection);
    end if;
	// Make the request
	ok,r:=send_JSON_request_with_args("rangedb", operation, args :
			Timeout:=Timeout);
	return ok,r;
end function;

// Sends the given operation for the given iterator. Returns true on success,
// otherwise returns false followed by an error message.
function send_for_value(itr,operation : Timeout:=0)
    if not itr`has_value then
        return false,"No value for iterator.";
    end if;
    if Realtime() gt itr`deadline then
        return false,"Deadline exceeded.";
    end if;
    ok,j:=rangedb_JSON_request(operation,[* "ID", itr`id *] :
            Timeout:=Timeout, Connection:=itr`connection);
    if not ok then
        return false,j;
    end if;
    zero_out(itr);
    return true,_;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic RangeDbDefaults( : Timeout:=0) -> Rec
{Configuration information for the pcas-magma range server. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    // Query the server
    ok, j := send_JSON_request("rangedb","defaults" : Timeout:=Timeout);
    require ok: j;
    // Extract the address and return
    ok,conn:=JSONIsDefined(j,"Connection");
    require ok: malformed_JSON_missing_key("Connection",j);
    ok,address:=string_from_JSON(conn,"Address");
    require ok: address;
    return rec<RangeDbRec | Address:=address>;
end intrinsic;

intrinsic RangeDbCreate(name::MonStgElt,entries::MonStgElt,lifetime::RngIntElt :
    MaxRetries:=7, MaxConcurrency:=2^15-1, Timeout:=0, Connection:=false)
{}
    // Sanity check
    require Type(MaxRetries) cmpeq RngIntElt and MaxRetries ge 0:
        "Optional parameter 'MaxRetries' must be a non-negative integer.";
    require Type(MaxConcurrency) cmpeq RngIntElt and MaxConcurrency gt 0:
        "Optional parameter 'MaxConcurrency' must be a positive integer.";
    // Convert the entries to a list
    ok,I:=string_to_range(entries);
    require ok: "Unable to parse argument 2: " cat I;
    // Convert the lifetime to a string
    s:=Sprintf("%os",lifetime);
    // Query the server
    ok, j := rangedb_JSON_request("create",
        [* "Name", name *],
        [* "Range", I *],
        [* "Lifetime", s *],
        [* "MaxRetries", MaxRetries *],
        [* "MaxConcurrency", MaxConcurrency *] :
        Timeout:=Timeout, Connection:=Connection);
    require ok: j;
end intrinsic;

intrinsic RangeDbCreate(name::MonStgElt,entries::MonStgElt,lifetime::FldReElt :
    MaxRetries:=7, MaxConcurrency:=2^15-1, Timeout:=0, Connection:=false)
{Creates the range with the given name and entries. Entries should be a string specifying the range, for example "[-12,-5..3,5,6,9..11]". This string should be of the form "[I_1,...,I_n]" where each of the interval specifiers I_j is either a 64-bit integer n or a substring "a..b" where a and b are 64-bit integers; in the first case I_j represents the interval [n..n] and in the second case it represents [a..b]. The intervals may be empty, and may overlap. Lifetime is the time after which an entry in the range goes stale, given by the number of seconds. The optional parameter 'MaxRetries' is the maximum number of times that an entry in the range may be retried [default: 7]. The optional parameter 'MaxConcurrency' is the maximum number of simultaneously active elements of a range [default: 32767]. The optional parameter 'Timeout' is the timeout for the creation of the range, measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    // Sanity check
    require Type(MaxRetries) cmpeq RngIntElt and MaxRetries ge 0:
        "Optional parameter 'MaxRetries' must be a non-negative integer.";
    require Type(MaxConcurrency) cmpeq RngIntElt and MaxConcurrency gt 0:
        "Optional parameter 'MaxConcurrency' must be a positive integer.";
    // Convert the entries to a list
    ok,I:=string_to_range(entries);
    require ok: "Unable to parse argument 2: " cat I;
    // Convert the lifetime to a string
    s:=Sprintf("%os",lifetime);
    // Query the server
    ok, j := rangedb_JSON_request("create",
        [* "Name", name *],
        [* "Range", I *],
        [* "Lifetime", s *],
        [* "MaxRetries", MaxRetries *],
        [* "MaxConcurrency", MaxConcurrency *] :
        Timeout:=Timeout, Connection:=Connection);
    require ok: j;
end intrinsic;

intrinsic RangeDbList(: Timeout:=0, Connection:=false) -> SetEnum[MonStgElt]
{A set containing the names of the known ranges. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    // Query the server
    ok, j := rangedb_JSON_request("list" :
                Timeout:=Timeout, Connection:=Connection);
    require ok: j;
    // Extract the range names
    ok,names:=strings_from_JSON(j,"Names");
    require ok: names;
    return SequenceToSet(names);
end intrinsic;

intrinsic RangeDbDelete(name::MonStgElt : Timeout:=0, Connection:=false)
{Deletes the range with the given name. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    ok, j := rangedb_JSON_request("delete", [* "Name", name *] :
                Timeout:=Timeout, Connection:=Connection);
    require ok: j;
end intrinsic;

intrinsic RangeDbStatus(name::MonStgElt:Timeout:=0, Connection:=false) -> Assoc
{An associative array containing status information about the range with the given name. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    // Query the server
    ok, j := rangedb_JSON_request("status", [* "Name", name *] :
                Timeout:=Timeout, Connection:=Connection);
    require ok: j;
    // Create the associative array
    A:=AssociativeArray();
    A["Name"]:=name;
    // Collect together the results
    for k in ["Pending","Active","Succeeded","Failed"] do
        ok,val:=JSONIsDefined(j,k);
        require ok: malformed_JSON_missing_key(k,j);
        ok,I:=range_from_JSON(val);
        require ok: I;
        A[k]:=range_to_string(I);
    end for;
    return A;
end intrinsic;

intrinsic RangeDbStatus(name::MonStgElt, n::RngIntElt :
    Timeout:=0, Connection:=false) -> Assoc
{An associative array containing status information for the entry n in the range with the given name. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    // Query the server
    ok, j := rangedb_JSON_request("info",[* "Name", name *],[* "Entry", n *] :
                Timeout:=Timeout, Connection:=Connection);
    require ok: j;
    // Extract the "State" field
    ok,val:=string_from_JSON(j,"State");
    require ok: val;
    // Compute which keys to extract
    case val:
    when "uninitialised":
        keys := [];
    when "pending":
        keys:=["Value","Failures"];
    when "active":
        keys:=["Value","AppName","Start","Deadline","Failures"];
    when "succeeded":
        keys:=["Value"];
    when "failed":
        keys:=["Value"];
    else:
        require false: Sprintf("Unknown state: %o.",val);
    end case;
    // Create the associative array
    A:=AssociativeArray();
    A["Name"]:=name;
    A["State"]:=val;
    // Collect together the results
    for k in keys do
        if k eq "Value" or k eq "Failures" then
            ok,val:=integer_from_JSON(j,k);
            require ok: val;
        elif k eq "Deadline" or k eq "Start" then
            ok,val:=integer_from_JSON(j,k);
            require ok: val;
            val:=val / 10.0^9; // Convert from nanoseconds to seconds
        else
            ok,val:=string_from_JSON(j,k);
            require ok: val;
        end if;
        A[k]:=val;
    end for;
    return A;
end intrinsic;

intrinsic RangeDbForAll(name::MonStgElt, f::UserProgram :
    AppName:=false, Timeout:=0, Connection:=false, MaxWait:=0)
{Executes the function f, which should be a function of one integer variable that returns a boolean, over each element of the range with the given name. f should return true to indicate successful completion and that iteration should continue; false to indicate successful completion and that iteration should stop; and raise an error if evaluation of f on that element of the range should be considered to have failed. Iteration will also stop if Timeout has been exceeded; the Timeout is measured in seconds [default: 0], and a Timeout of 0 means no timeout. MaxWait is a time in seconds [default: 0]. Combined with Timeout, MaxWait is used to bound the time to wait for the next range element. AppName is the name by which we identify ourselves to the rangedb server.}
    // Sanity check
    require IsPcasMagma(): "Not running pcas-magma.";
    require AppName cmpeq false or Type(AppName) cmpeq MonStgElt:
        "Optional parameter 'AppName' must be a string.";
    ok,msg:=validate_connection(Connection);
    require ok: msg;
    ok,msg:=validate_timeout(Timeout);
    require ok: msg;
    ok,msg:=validate_wait(MaxWait);
    require ok: msg;
    // If necessary, set the default app name
    if AppName cmpeq false then
        AppName:=PcasMagmaGetAppName();
    end if;
    // Compute the deadline
    Deadline := false;
    if Timeout gt 0 then
        Deadline := Realtime() + Timeout;
    end if;
    // Get an iterator for the range
    itr:=RangeDbIterator(name : AppName:=AppName, Connection:=Connection,
            Timeout:=combine_timeouts(deadline_to_timeout(Deadline),MaxWait));
    // Start iterating
    ok:=true;
    while Next(itr : Timeout:=combine_timeouts(deadline_to_timeout(Deadline),MaxWait)) and ok do
        ok:=iterative_step(itr,Current(itr),f : Deadline:=Deadline);
    end while;
end intrinsic;

/////////////////////////////////////////////////////////////////////////
// RgeDbItr intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic RangeDbIterator(name::MonStgElt :
    AppName:=false, Timeout:=0, Connection:=false) -> RgeDbItr
{An iterator for the range with the given name. AppName is the name by which we identify ourselves to the rangedb server. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    // Sanity checks
    require IsPcasMagma(): "Not running pcas-magma.";
    require AppName cmpeq false or Type(AppName) cmpeq MonStgElt:
        "Optional parameter 'AppName' must be a string.";
    ok,msg:=validate_connection(Connection);
    require ok: msg;
    ok,msg:=validate_timeout(Timeout);
    require ok: msg;
    require name in RangeDbList(: Timeout:=Timeout, Connection:=Connection):
        Sprintf("Unknown range: %o.",name);
    // If necessary, set the default app name
    if AppName cmpeq false then
        AppName:=PcasMagmaGetAppName();
    end if;
    // Initialise a new iterator
    itr := New(RgeDbItr);
    itr`name := name;
    itr`app_name := AppName;
    itr`connection := Connection;
    zero_out(itr);
    return itr;
end intrinsic;

intrinsic Print(itr::RgeDbItr)
{Display the iterator itr.}
    val:="none";
    d:="none";
    if itr`has_value then
        val:=itr`value;
        d:=itr`deadline;
    end if;
    printf "Iterator for range \"%o\".\n\tCurrent value: %o\n\tDeadline: %o",
            itr`name,val,d;
end intrinsic;

intrinsic Next(itr::RgeDbItr : Timeout:=0) -> BoolElt, RngIntElt
{Advances the range iterator itr and returns true followed by the next entry on success, and false at the end of iteration. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    // Sanity check
    require not itr`has_value:
        "Success, Error, Requeue, or Fatal must be called between successive calls to Next.";
    // Send the "next" request
    ok, j := rangedb_JSON_request("next",
        [* "Name", itr`name *],
        [* "AppName", itr`app_name *] :
        Timeout:=Timeout, Connection:=itr`connection);
    require ok: j;
    // Are we at the end of iteration?
    if JSONNumberOfKeys(j) eq 0 then
        return false, _;
    end if;
    // Extract and store the ID
    ok, u := string_from_JSON(j, "ID");
    require ok: u;
    // Extract the deadline
    ok, d := integer_from_JSON(j, "Deadline");
    require ok: d;
    // Extract the number of failures
    ok, f := integer_from_JSON(j, "Failures");
    require ok: f;
    // Extract the value
    ok, n := integer_from_JSON(j, "Value");
    require ok: n;
    // Save the values
    itr`id := u;
    itr`deadline := d/10.0^9; // Convert from nanoseconds to seconds
    itr`failures := f;
    itr`value := n;
    itr`has_value := true;
    // Return the value
    return true, n;
end intrinsic;

intrinsic Current(itr::RgeDbItr) -> RngIntElt
{The current entry for the range iterator.}
    require itr`has_value: "Must only be called after Next returns success.";
    return itr`value;
end intrinsic;

intrinsic Deadline(itr::RgeDbItr) -> FldReElt
{The deadline for the current entry for the range iterator.}
    require itr`has_value: "Must only be called after Next returns success.";
    return itr`deadline;
end intrinsic;

intrinsic Failures(itr::RgeDbItr) -> RngIntElt
{The number of failures for the current entry for the range iterator.}
    require itr`has_value: "Must only be called after Next returns success.";
    return itr`failures;
end intrinsic;

intrinsic Success(itr::RgeDbItr : Timeout:=0)
{Informs the pcas rangedb server that the current entry in the range iterator itr has been processed successfully. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    ok,msg:=send_for_value(itr,"success" : Timeout:=Timeout);
    require ok: msg;
end intrinsic;

intrinsic Error(itr::RgeDbItr : Timeout:=0)
{Informs the pcas rangedb server that an error occurred when processing the current entry in the range iterator itr. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    ok,msg:=send_for_value(itr,"error" : Timeout:=Timeout);
    require ok: msg;
end intrinsic;

intrinsic Requeue(itr::RgeDbItr : Timeout:=0)
{Informs the pcas rangedb server that the current entry in the range iterator itr should be requeued. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    ok,msg:=send_for_value(itr,"requeue" : Timeout:=Timeout);
    require ok: msg;
end intrinsic;

intrinsic Fatal(itr::RgeDbItr : Timeout:=0)
{Informs the pcas rangedb server that an unrecoverable error occurred when processing the current entry in the range iterator itr. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    ok,msg:=send_for_value(itr,"fatal" : Timeout:=Timeout);
    require ok: msg;
end intrinsic;

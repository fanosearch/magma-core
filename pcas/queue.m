/////////////////////////////////////////////////////////////////////////
// queue.m
/////////////////////////////////////////////////////////////////////////
// This is the Magma side of the interface to the pcas queue server.
/////////////////////////////////////////////////////////////////////////

import "pcas.m": send_JSON_request, send_JSON_request_with_args,
    malformed_JSON, malformed_JSON_missing_key, validate_timeout,
    string_from_JSON, integer_from_JSON;

// Holds configuration information for a queue server.
QueueRec:=recformat<
    Address: MonStgElt  // The address of the server, as a hostname:port pair
                        // of the form "hostname[:port]" or
                        // "tcp://hostname[:port][/]", or a websocket URI of the
                        // form "ws://host/path".
>;

// Represents a queue on a pcas queue server.
declare type Queue;
declare attributes Queue:
	name,       // The queue name
    app_name,   // The name by which we identify ourselves to the queue server
    connection; // The connection to use

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Converts the deadline d to a timeout. If d is in the past, returns a timeout
// of one nanosecond.
function deadline_to_timeout(d)
    if d cmpeq false then
        return 0;
    end if;
    t := d - Realtime();
    // The deadline could be in the past
    if t le 0 then
        t := 10.0^-9;
    end if;
    return t;
end function;

// Validates the connection c. On success returns true, j, where j is the JSON
// representation of c. Returns false, error message otherwise.
function validate_connection(c)
    // Create the empty JSON object
    j:=JSONCreateNode();
    // Check that this is either 'false' or a record
    if Type(c) cmpeq BoolElt and not c then
        return true, j;
	elif not Type(c) cmpeq Rec then
		return false, "Optional parameter 'Connection' must be a record.";
	end if;
    // Add the address
    if "Address" in Names(c) and assigned c`Address then
        if not Type(c`Address) eq MonStgElt then
            return false, "Value for Address on optional parameter 'Connection' must be a string.";
        elif #c`Address ne 0 then
            JSONAppend(~j, "Address", c`Address);
        end if;
    end if;
	return true, j;
end function;

// Validates the queue name s. Returns true, s on success and false, error message otherwise.
function validate_name(s)
    if s ne Trim(s) or #s eq 0 then
        return false, "illegal queue name";
    end if;
    return true, s;
end function;

// A simple wrapper around send_JSON_request_with_args for making requests
// from the endpoint.
function queue_JSON_request(operation, ... : AppName:=false, Timeout:=0, Connection:=false)
    // Sanity check
    ok, Connection := validate_connection(Connection);
    if not ok then
        return false, Connection;
    end if;
    // Tidy up the input
	args := operation[2..#operation];
	operation := operation[1];
	// Package up the arguments
	keys := [Strings()|];
	values := [* *];
	for a in args do
		Append(~keys, a[1]);
		Append(~values, a[2]);
	end for;
    args:=JSONCreateNode(keys, values);
    // Add the connection
    if JSONNumberOfKeys(Connection) ne 0 then
        JSONAppend(~args, "Connection", Connection);
    end if;
    // Add the application name, if provided
    if not AppName cmpeq false then
        JSONAppend(~args, "AppName", AppName);
    end if;
	// Make the request
	ok,r:=send_JSON_request_with_args("queue", operation, args :
			Timeout:=Timeout);
	return ok,r;
end function;

// Returns true, _ if the handler functions are valid and false, error_message otherwise.
function validate_handlers(handlers)
    if not Type(handlers) eq Assoc then
        return false, "Argument 'handlers' should be an associative array";
    elif Universe(Keys(handlers)) ne Strings() then
        return false, "Argument 'handlers' should be an associative array with string keys";
    end if;
    for k in Keys(handlers) do
        if not ISA(Type(handlers[k]),Program) then
            return false, "The values in the argument 'handlers' must be programs";
        end if;
    end for;
    return true, _;
end function;


// Returns true, _ if the conversion functions are valid and false, error_message otherwise.
function validate_task_conversion_funcs(conversion_funcs)
    if not conversion_funcs cmpeq false then
        if Universe(Keys(conversion_funcs)) ne Strings() then
            return false, "The keys of 'conversion_funcs' must be strings";
        end if;
        for k in Keys(conversion_funcs) do
            v := conversion_funcs[k];
            if Type(v) ne Assoc then
                return false, "The values of 'conversion_funcs' must be associative arrays";
            elif Universe(Keys(v)) ne Strings() then
                return false, "The keys in each value of 'conversion_funcs' must be strings"; 
            end if;
            for k in Keys(v) do
                if not ISA(Type(v[k]),Program) then
                    return false, "The values in eacn value of 'conversion_funcs' must be programs";
                end if;
            end for;
        end for;
    end if;
    return true, _;
end function;

// Returns true, _ if the preflight and postflight callback functions are valid and false, error_message otherwise.
function validate_callbacks(preflight, postflight)
    if not (preflight cmpeq false or ISA(Type(preflight),Program)) then
        return false, "Argument 'preflight' must be a program";
    elif not (postflight cmpeq false or ISA(Type(postflight),Program)) then
        return false, "Argument 'postflight' must be a program";
    end if;
    return true, _;
end function;

// Returns true followed by the task name, ULID, and payload from a JSON representation j of a task, or false, msg, _ if the task is invalid, where msg is an error message.
function get_task_details(j : conversion_funcs:=false)
    // Extract the task, ULID, and payload
    ok, task := JSONIsDefined(j, "task");
    if not ok then 
        return false,  "Missing task name", _, _;
    end if;
    ok, ulid := JSONIsDefined(j, "ulid");
    if not ok then
        return false, "Missing task ULID", _, _;
    end if;
    ok, p := JSONIsDefined(j, "payload");
    if not ok then
        return false, "Missing task payload", _, _;
    end if;
    cfs := false;
    if not conversion_funcs cmpeq false then
        ok, x := IsDefined(conversion_funcs, task);
        if ok then
            cfs := x;
        end if;
    end if;
    payload := JSONToAssociativeArray(p : conversion_funcs:=cfs);
    return true, task, ulid, payload;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic QueueDefaults( : Timeout:=0) -> Rec
{Configuration information for the pcas-magma queue server. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    // Query the server
    ok, j := send_JSON_request("queue","defaults" : Timeout:=Timeout);
    require ok: j;
    // Extract the address and return
    ok,conn:=JSONIsDefined(j,"Connection");
    require ok: malformed_JSON_missing_key("Connection",j);
    ok,address:=string_from_JSON(conn,"Address");
    require ok: address;
    return rec<QueueRec | Address:=address>;
end intrinsic;

intrinsic QueueConnect(name::MonStgElt : AppName:=false, Timeout:=0, Connection:=false) -> Queue
{Initialises a connection to the named queue. The optional parameter 'AppName' is the name by which we identify ourselves to the queue server. The optional parameter 'Connection' can be used to provide configuration data for the connection to the queue server; if Connection is not set, then the default connection is used.}
    require AppName cmpeq false or Type(AppName) cmpeq MonStgElt:
        "Optional parameter 'AppName' must be a string.";
    // Validate the connection
    ok, Connection := validate_connection(Connection);
    require ok: Connection;
    // Validate the queue name
    ok, name := validate_name(name);
    require ok: name;
    // Create the queue and return
    q := New(Queue);
    q`name := name;
    q`app_name := AppName;
    q`connection := Connection;
    return q;
end intrinsic;

intrinsic QueueDelete(name::MonStgElt : AppName:=false, Timeout:=0, Connection:=false)
{Deletes the queue with the given name. The optional parameter 'AppName' is the name by which we identify ourselves to the queue server. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout. The optional parameter 'Connection' can be used to provide configuration data for the connection to the queue server; if Connection is not set, then the default connection is used.}
    ok, j := queue_JSON_request("delete", [* "Name", name *] :
                AppName:=AppName, Timeout:=Timeout, Connection:=Connection);
    require ok: j;
end intrinsic;

intrinsic Get(q::Queue : Timeout:=0) -> MonStgElt, MonStgElt
{Get the next message from q, returning id, msg where id is the ID of the message and msg is the content of the message. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    ok, j := queue_JSON_request("get", [* "Name", q`name *] :
            AppName:=q`app_name, Timeout:=Timeout, Connection:=q`connection);
    require ok: j;
    ok, id := string_from_JSON(j, "ID");
    require ok: id;
    ok, msg := string_from_JSON(j, "Message");
    require ok: msg;
    return id, msg;
end intrinsic;

intrinsic Acknowledge(q::Queue, id::MonStgElt : Timeout:=0) 
{Acknowledge the message from q with the given ID. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    ok, j := queue_JSON_request("acknowledge", [* "ID", id *] :
            AppName:=q`app_name, Timeout:=Timeout, Connection:=q`connection);
    require ok: j;
end intrinsic;

intrinsic Requeue(q::Queue, id::MonStgElt : Timeout:=0) 
{Requeue the message from q with the given ID. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    ok, j := queue_JSON_request("requeue", [* "ID", id *] :
            AppName:=q`app_name, Timeout:=Timeout, Connection:=q`connection);
    require ok: j;
end intrinsic;

intrinsic Put(q::Queue, msg::MonStgElt : Timeout:=0) 
{Put the given message on the queue q. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    ok, j := queue_JSON_request("put", [* "Name", q`name *], [* "Message", msg *] :
            AppName:=q`app_name, Timeout:=Timeout, Connection:=q`connection);
    require ok: j;
end intrinsic;

intrinsic Length(q::Queue : Timeout:=0) -> RngIntElt
{The number of messages in q. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    ok, j := queue_JSON_request("length", [* "Name", q`name *] :
            AppName:=q`app_name, Timeout:=Timeout, Connection:=q`connection);
    require ok: j;
    ok, n := integer_from_JSON(j, "Length");
    require ok: n;
    return n;
end intrinsic;

intrinsic Print(q::Queue)
{Display the queue q.}
    // Recover the connection
    ok, a := string_from_JSON(q`connection, "Address");
    if ok then
        conn := Sprintf("connection \"%o\"", a);
    else
        conn := "the default connection";
    end if;
    printf "Queue with %o and name \"%o\".",
            conn, q`name;
end intrinsic;

intrinsic PutTask(q::Queue, name::MonStgElt, payload::Assoc : Timeout:=0)
{Put the task with the given name and payload onto the queue q. The optional parameter 'Timeout' is measured in seconds [default: 0]; a Timeout of 0 means no timeout.}
    // Package up the task
    j := JSONCreateNode(
        [* "task", name *],
        [* "ulid", ULIDNew() *],
        [* "payload", ToJSON(payload) *]
    );
    s := JSONToString(j);
    Put(q, s : Timeout:=Timeout);
end intrinsic;

intrinsic DoTasks(q::Queue, handlers::Assoc : Timeout:=0, conversion_funcs:=false, preflight:=false, postflight:=false)
    {Process tasks from the queue q, using handler functions specified by handlers. The keys in handlers should be task names and the values should be functions that are passed the payload for the task, which is an associative array as in PutTask. The optional parameter 'Timeout' is the timeout when waiting for a new task and is measured in seconds [default: 0]; a Timeout of 0 means no timeout. The optional parameter conversion_funcs specifies conversion functions for each task; this should be an associative array with keys equal to task names and values equal to associative arrays that specify a conversion function for each key in the payload. The optional parameter preflight is a procedure which will be called before the handler function, and passed the arguments task_name, ulid, payload. The optional parameter postflight is a function which will be called after the handler function completes, and passed the arguments task_name, ulid, payload. It should return true if iteration should continue and false otherwise.}
    // Sanity checks
    ok, msg := validate_handlers(handlers);
    require ok: msg;
    ok, msg := validate_task_conversion_funcs(conversion_funcs);
    require ok: msg;
    ok, msg := validate_callbacks(preflight, postflight);
    require ok: msg;
    // Main loop
    while true do
        // Grab a task from the queue
        id, msg := Get(q : Timeout:=Timeout);
        // Recover the JSON
        j := JSONParseString(msg);
        // Extract the task, ULID, and payload
        ok, task, ulid, payload := get_task_details(j);
        require ok: task;
        // Recover the handler function
        ok, h := IsDefined(handlers, task);
        require ok: Sprintf("Unknown task name %o", task);
        // Execute the preflight procedure, if defined
        if not preflight cmpeq false then
            preflight(task, ulid, payload);
        end if;
        // Execute the handler function
        h(payload);
        // Execute the postflight procedure, if defined
        if not postflight cmpeq false then
            if not postflight(task, ulid, payload) then
                // We should terminate iteration
                return;
            end if;
        end if;
        // Record the task as done and move on
        Acknowledge(q, id);
    end while;
end intrinsic;

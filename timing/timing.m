freeze;

/////////////////////////////////////////////////////////////////////////
// timing.m
/////////////////////////////////////////////////////////////////////////
// Intrinsics to help with profiling code.
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Data Store
/////////////////////////////////////////////////////////////////////////

// The store for the timing settings. Do not access directly!
timing_store:=NewStore();

/////////////////////////////////////////////////////////////////////////
// Functions for general use
/////////////////////////////////////////////////////////////////////////

// Sets the timing store for the given key to the given value.
forward initialize_timing_store;
procedure set_timing_store_value(key,value)
    // Sanity check
    error if Type(key) ne MonStgElt,
        "set_timing_store_value: The key must be a string";
    // Set the value
    if not StoreIsDefined(timing_store,"init") then
        initialize_timing_store();
    end if;
    StoreSet(timing_store,key,value);
end procedure;

// Returns true followed by the timing store value for the given key if defined,
// false otherwise.
function get_timing_store_value(key)
    // Sanity check
    error if Type(key) ne MonStgElt,
        "get_timing_store_value: The key must be a string";
    // Recover the value
    if not StoreIsDefined(timing_store,"init") then
        initialize_timing_store();
    end if;
    bool,value:=StoreIsDefined(timing_store,key);
    if bool then
        return true,value;
    else
        return false,_;
    end if;
end function;

// Sets the handler of the timing data, and ensures that collection of timing
// data is enabled.
//  * The handler must be a procedure accepting two arguments: the name of the
//    timing metric, and the duration (in seconds, as a FldReElt).
procedure set_timing_handler(handler)
    // Sanity check
    error if Type(handler) ne UserProgram,
        "set_timing_handler: The handler must be a procedure accepting two arguments";
    // Set the values
    set_timing_store_value("handler",handler);
    set_timing_store_value("enabled",true);
end procedure;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Initializes the timing store ready for use.
forward magma_timing_handler;
procedure initialize_timing_store()
    StoreSet(timing_store,"init",true);
    StoreSet(timing_store,"enabled",false);
end procedure;

// Returns a sequence of strings describing the given timing data.
function timing_data_to_string(name,I)
    return [name,                           // The name
            IntegerToString(I[1]),          // The total number of times called
            Sprintf("%o",I[2]),             // The total cpu time
            Sprintf("%o",I[2] / I[1])];     // The average cpu time
end function;

// Prints the given string, with padding to length l.
procedure print_with_padding(S,l)
    printf "%o%o",S,&cat[" " : i in [1..l - #S]];
end procedure;

// Prints the given row, with paddings L.
procedure print_row(S,L)
    for i in [1..#L - 1] do
        print_with_padding(S[i],L[i] + 1);
    end for;
    printf "%o\n",S[#S];
end procedure;

// Passes the given timing data to the appropriate handler.
procedure timing_to_handler(name,t)
    bool,handler:=get_timing_store_value("handler");
    assert bool;
    handler(name,t);
end procedure;

// The default "magma" timing handler.
procedure magma_timing_handler(name,t)
    // Does nothing
end procedure;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic DisableTiming()
{Disables the collection of timing data.}
    set_timing_store_value("enabled",false);
end intrinsic;

intrinsic EnableTiming()
{Enables the collection of timing data.}
    set_timing_store_value("enabled",true);
end intrinsic;

intrinsic IsTimingEnabled() -> BoolElt
{Is the collection of timing data enabled?}
    bool,enabled:=get_timing_store_value("enabled");
    assert bool;
    return enabled;
end intrinsic;

intrinsic SetTimingToMagma()
{Sets timing data to be stored within Magma, and enables the collection of timing data.}
    set_timing_handler(magma_timing_handler);
end intrinsic;

intrinsic TimingPush( name::MonStgElt )
{Push a timing with the given name onto the stack. Every call to TimingPush must be balanced with a call to TimingPop.}
    // Is there anything to do?
    if not IsTimingEnabled() then
        return;
    end if;
    // Fetch the stack from the store
    bool,stack:=get_timing_store_value("stack");
    // Append the timing data to the end of the stack
    if bool then
        Append(~stack,<name,Cputime()>);
    else
        stack:=[<name,Cputime()>];
    end if;
    // Update the store with the new stack
    set_timing_store_value("stack",stack);
end intrinsic;

intrinsic TimingPop()
{Pop a timing off the stack. Every call to TimingPush must be balanced with a call to TimingPop.}
    // Is there anything to do?
    if not IsTimingEnabled() then
        return;
    end if;
    // Fetch the stack from the store and check that it's non-empty
    bool,stack:=get_timing_store_value("stack");
    require bool and #stack gt 0: "The timing stack is empty.";
    // Extract the timing data from the stack
    I:=stack[#stack];
    t:=Cputime(I[2]);
    // Pass the timing data to the handler
    name:=I[1];
    timing_to_handler(name,t);
    // Update the store with the new stack
    Prune(~stack);
    set_timing_store_value("stack",stack);
    // Fetch the totals data from the store
    bool,total:=get_timing_store_value("total");
    if not bool then
        total:=AssociativeArray(PowerStructure(MonStgElt));
    end if;
    // Update the entry for this name
    bool,I:=IsDefined(total,name);
    I:=bool select <I[1] + 1,I[2] + t> else <1,t>;
    total[name]:=I;
    // Update the store with the new totals data
    set_timing_store_value("total",total);
end intrinsic;

intrinsic TimingReset()
{Reset the current timing information.}
    StoreRemove(timing_store,"stack");
    StoreRemove(timing_store,"total");
end intrinsic;

intrinsic TimingWrite( F::MonStgElt )
{Writes the timing data to the file specified by F. The timing data will be outputed as a comma-separated list.}
    // Fetch the totals data from the store
    bool,total:=get_timing_store_value("total");
    if not bool then return; end if;
    // Output the data ordered by name
    for name in Sort(Keys(total)) do
        I:=timing_data_to_string(name,total[name]);
        fprintf F,"%o,%o,%o,%o\n",I[1],I[2],I[3],I[4];
    end for;
end intrinsic;

intrinsic TimingPrint()
{Prints a table of timing data.}
    // Fetch the totals data from the store
    bool,total:=get_timing_store_value("total");
    if not bool then
        total:=AssociativeArray(PowerStructure(MonStgElt));
    end if;
    // Convert the data to strings
    strs:=[timing_data_to_string(name,total[name]) : name in Sort(Keys(total))];
    // Calculate the column widths
    titles:=["Name","Num","Total","Avrg"];
    cols:=[#I : I in titles];
    for row in strs do
        for i in [1..#cols] do
            if #row[i] gt cols[i] then
                cols[i]:=#row[i];
            end if;
        end for;
    end for;
    // Output the header and rule off
    print_row(titles,cols);
    print_row([&cat["=" : i in [1..l]] : l in cols],cols);
    // Output the rows of timing data
    for row in strs do
        print_row(row,cols);
    end for;
end intrinsic;

freeze;

/////////////////////////////////////////////////////////////////////////
// core.m
/////////////////////////////////////////////////////////////////////////
// The core Fanosearch intrinsics.
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Fanosearch verbose flag
/////////////////////////////////////////////////////////////////////////

// The Fanosearch verbose flag (max level: 1)
declare verbose Fanosearch, 1;

/////////////////////////////////////////////////////////////////////////
// Data Store
/////////////////////////////////////////////////////////////////////////

// The shared Fanosearch data store -- do not access directly!
fs_store:=NewStore();

// Deletes the given key from the data store.
procedure delete_data(key)
    StoreRemove(fs_store,key);
end procedure;

// Stores the given key/value in the data store.
procedure save_data(key,value)
    StoreSet(fs_store,key,value);
end procedure;

// Fetches the value for the given key from the data store. Returns true
// followed by the value on success, false otherwise.
function fetch_data(key)
    bool,value:=StoreIsDefined(fs_store,key);
    if bool then
        return true,value;
    else
        return false,_;
    end if;
end function;

/////////////////////////////////////////////////////////////////////////
// Internal Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic InternalFanosearchStoreClear()
{Removes all entries from the internal Fanosearch data store. Intended for debugging purposes only.}
    StoreClear(fs_store);
end intrinsic;

intrinsic InternalFanosearchStoreKeys() -> SetEnum[MonStgElt]
{The set of fields with defined values in the internal Fanosearch data store. Intended for debugging purposes only.}
    return StoreKeys(fs_store);
end intrinsic;

intrinsic InternalFanosearchStoreGet( field::MonStgElt ) -> .
{The value associated to the field in the internal Fanosearch data store. Intended for debugging purposes only.}
    bool,value:=fetch_data(field);
    require bool: "No such value present in the store";
    return value;
end intrinsic;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic FanosearchGetRoot() -> MonStgElt
{The path to the Fanosearch root directory. This can be specified via the FSROOT environment variable.}
    bool,path:=fetch_data("fs_root_dir");
    if not bool then
        // If the environment variable is set, this is easy
        path:=Trim(GetEnv("FSROOT"));
        while #path gt 0 and path[#path] eq "/" do Prune(~path); end while;
        if #path eq 0 then
            // Magma doesn't currently provide a way for a package file to
            // discover its path directly -- instead we force an error and parse
            // the error string to extract the path.
            try
                a:=0;
                a:=1/a;
            catch e;
                idx:=Index(e`Position,"\"");
                path:=e`Position[idx+1..#e`Position];
            end try;
            idx:=Index(path,"\"");
            path:=path[1..idx-1];
            bool,path:=PipeCommand("dirname \"" cat path cat "\"");
            require bool:
                "Unable to deduce the path to the Fanosearch root directory";
            idx:=Index(path,"\n");
            if idx ne 0 then path:=path[1..idx-1]; end if;
            path cat:= "/..";
        end if;
        require #path gt 0:
            "Unable to deduce the path to the Fanosearch root directory";
        save_data("fs_root_dir",path);
    end if;
    return path;
end intrinsic;

intrinsic FanosearchSetRoot( path::MonStgElt )
{Set the path to the Fanosearch root directory}
    while #path gt 0 and path[#path] eq "/" do Prune(~path); end while;
    require #path gt 0: "The path must not be empty";
    save_data("fs_root_dir",path);
end intrinsic;

intrinsic FanosearchGetDbRoot() -> MonStgElt
{The path to the Fanosearch database directory. This can be specified via the FSDB environment variable.}
    bool,path:=fetch_data("fs_db_dir");
    if not bool then
        // Has this been set via the environment variable?
        path:=Trim(GetEnv("FSDB"));
        if #path eq 0 then
            // No luck -- we deduce it from the Fanosearch root directory
            try
                path:=FanosearchGetRoot() cat "/db";
                success:=true;
            catch e
                success:=false;
            end try;
            require success:
                "Unable to deduce the path to the Fanosearch root directory";
        end if;
        save_data("fs_db_dir",path);
    end if;
    return path;
end intrinsic;

intrinsic FanosearchSetDbRoot( path::MonStgElt )
{Set the path to the Fanosearch database directory}
    while #path gt 0 and path[#path] eq "/" do Prune(~path); end while;
    require #path gt 0: "The path must not be empty";
    save_data("fs_db_dir",path);
end intrinsic;

intrinsic FanosearchSetVerbose( v::BoolElt )
{Set the verbose flag to: off, if v is false; minimum value if v is true.}
    SetVerbose("Fanosearch",v);
end intrinsic;

intrinsic FanosearchIsVerbose() -> BoolElt
{True iff the verbose flag is currently non-zero}
    return IsVerbose("Fanosearch");
end intrinsic;

intrinsic FanosearchTempDir() -> MonStgElt
{Return the directory used for temporary files. This can be specified via the FSTMP environment variable.}
    bool,tmp:=fetch_data("tempdir");
    if not bool then
        // Is the environment variable set? If not, fall back on Magma's temp
        // directory.
        tmp:=Trim(GetEnv("FSTMP"));
        if #tmp eq 0 then tmp:=GetTempDir(); end if;
        // Cache the result
        save_data("tempdir",tmp);
    end if;
    return tmp;
end intrinsic;

intrinsic FanosearchGetVersion()
{Print version information.}
    // Get Magma version information
    a, b, c := GetVersion();
    // Build the Magma version string
    magma := Sprintf("magma: %o-%o-%o", a, b, c);
    // Get git information
    tag_cmd := Sprintf("cd %o/magma-core; git rev-parse --short HEAD", FanosearchGetRoot());
    x := Pipe(tag_cmd, "");
    tag := Split(x)[1];
    // Build the magma-core version string
    magma_core := Sprintf("magma-core: %o", tag);
    // Build the pcas-magma version string
    if IsPcasMagma() then
        v := PcasMagmaGetVersionDetails();
        pcas_magma := Sprintf("pcas-magma: %o %o %o %o", v`Version, v`Commit, v`GoOs, v`GoArch);
    else
        pcas_magma := "pcas-magma: not present";
    end if;
    // Print the version information
    printf "%o\n%o\n%o\n", magma, magma_core, pcas_magma;
end intrinsic;

intrinsic FanosearchTimeout() -> Err
{Return an error object that should (only) be used to signal that a timeout has occurred.}
    /* 
    This returns an error e with e`Object equal to a sentinel error err. So the pattern is
    
    try
        // code that might throws a timeout
        ...
        error FanosearchTimeout();
        ...
    catch e
        if e`Object cmpeq err then
            // handle the timeout
            ...
        end if;
    end try;
    
    At user level, this looks like:

    try 
        // code that might throw a timeout
        ...
        error FanosearchTimeout();
        ...
    catch e
        if not FanosearchIsTimeout(e) then
            error e;
        end if;
        // handle the timeout
        ...
    end try;

    */
    bool,err:=fetch_data("timeout");
    if not bool then
        err := Error("A timeout has occurred");
        save_data("timeout",err);
    end if;
    return Error(err);
end intrinsic;

intrinsic FanosearchIsTimeout(e:Err) -> BoolElt
{True if and only if e is a timeout.}
    bool,err:=fetch_data("timeout");
    if not bool then
        err := Error("A timeout has occurred");
        save_data("timeout",err);
    end if;    return e`Object cmpeq err;
end intrinsic;

/////////////////////////////////////////////////////////////////////////
// assoc.m
/////////////////////////////////////////////////////////////////////////
// Functions for converting associative arrays to and from JSON objects.
// Note that this does not currently handle nested associative arrays: 
// each value must be a boolean, string, integer, sequence of booleans,
// sequence of strings, or sequence of integers. All integers must fit
// into an int64.
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Returns true if v is a boolean, string, 64-bit integer, sequence of booleans, sequence of strings, or sequence of 64-bit integers. 
function validate_value(v)
    t := Type(v);
    if t eq BoolElt or t eq MonStgElt then
        return true;
    elif t eq RngIntElt and IsInt64(v) then
        return true;
    elif t eq SeqEnum then
        if Universe(v) eq BoolElt or Universe(v) eq MonStgElt then
            return true;
        elif Universe(v) eq RngIntElt and &and[IsInt64(x): x in v] then
            return true;
        end if;
    end if;
    return false;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic ToJSON(x::Assoc) -> Rec
{Attempts to convert an associative array into a JSON node}
    keys := Keys(x);
    require Universe(keys) eq Strings(): "The keys must be strings";
    // Add the key-value pairs one at a time
    j := JSONCreateNode();
    for k in Sort(Keys(x)) do
        v := x[k];
        require validate_value(v): "Values must be booleans, strings, integers, or sequences of such. All integers must fit in an int64";
        JSONAppend(~j, k, v);
    end for;
    return j;
end intrinsic;

intrinsic JSONToAssociativeArray( x::Rec : conversion_funcs:=false ) -> Assoc
{Outputs x as an associative array. }
    // Sanity check
    require IsJSONNode(x): "Argument must be a JSON node";
    // Validate the conversion functions
    cfs:=AssociativeArray();
    if not conversion_funcs cmpeq false then
        for k in Keys(conversion_funcs) do
            require Type(k) eq MonStgElt:
                "The keys of 'conversion_funcs' must be strings";
            to_obj:=conversion_funcs[k];
            require ISA(Type(to_obj),Program): "The values of 'conversion_funcs' must be programs";
            cfs[k]:=to_obj;
        end for;
    end if;
    // Build the result, one key at a time
    a := AssociativeArray();
    for k in JSONKeys(x) do
        vs := JSONValues(x, k);
        require #vs eq 1: "There must be a unique value for every key";
        // Convert the key if necessary
        v := vs[1];
        ok, f := IsDefined(cfs, k);
        if ok then
            v := f(v);
        end if;
        // Store the key-value pair
        a[k] := v;
    end for;
    return a;
end intrinsic;


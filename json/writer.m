freeze;

/////////////////////////////////////////////////////////////////////////
// writer.m
/////////////////////////////////////////////////////////////////////////
// Functions for outputting basic JSON documents.
/////////////////////////////////////////////////////////////////////////

import "reader.m": create_JSON_node, append_to_JSON_node,
                    RR, INT64_MIN, INT64_MAX, FLOAT64_MAX;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Returns true, _ iff the given value can be interpreted as a JSON object. Otherwise returns false followed by error message.
forward is_json_list;
function is_json_value(x)
    t:=Type(x);
    if t cmpeq RngIntElt then
        if not IsInt64(x) then
            return false, "Integer must fit in an int64";
        end if;
        return true, _;
    elif t cmpeq FldRatElt then
        if not IsIntegral(x) then
            return false, "Unsupported type";
        elif not IsInt64(x) then
            return false, "Integer must fit in an int64";
        end if;
        return true, _;
    elif t cmpeq FldReElt then
        if Abs(RR!x) gt FLOAT64_MAX then
            return false, "Real number must fit in a float64";
        end if;
        return true, _;
    elif t cmpeq SeqEnum or t cmpeq List then
        ok, i, msg := is_json_list(x);
        if not ok then
            return false, Sprintf("Error validating entry %o: %o",i,msg);
        end if;
        return true, _;
    elif t cmpeq MonStgElt or
        (t cmpeq Rec and IsJSONNode(x)) or
        t cmpeq BoolElt then
        return true, _;
    end if;
    return false, "Unsupported type";
end function;

// Returns true, _, _ iff the given list can be interpreted as a list of JSON objects.
// If false, also returns the index of the first object in the list that is not
// a JSON object along with an error message.
function is_json_list(list)
    for i in [1..#list] do
        ok, msg := is_json_value(list[i]);
        if not ok then
            return false,i,msg;
        end if;
    end for;
    return true,_,_;
end function;

// Escapes any of the special characters ",\,/,\b,\n,\r,\t.
function escape_string(str)
    str:=SubstituteString(str,"\\","\\\\");
    str:=SubstituteString(str,"\"","\\\"");
    str:=SubstituteString(str,"/","\\/");
    str:=SubstituteString(str,"\b","\\b");
    str:=SubstituteString(str,"\n","\\n");
    str:=SubstituteString(str,"\r","\\r");
    str:=SubstituteString(str,"\t","\\t");
    return str;
end function;

// Returns a string description of the given list.
forward value_to_string;
function list_to_string(list : formatted:=false, indent:="")
    // Get the empty list out of the way
    if #list eq 0 then
        return "[]";
    end if;
    // Start constructing the list
    str:="[";
    first:=true;
    for value in list do
        if first then
            first:=false;
        else
            str cat:= ",";
            if formatted then
                str cat:= " ";
            end if;
        end if;
        str cat:= value_to_string(value : formatted:=formatted, indent:=indent);
    end for;
    return str cat "]";
end function;

// Returns a string description of the given value.
forward node_to_string;
function value_to_string(value : formatted:=false, indent:="")
    t:=Type(value);
    if t cmpeq MonStgElt then
        // This is a string
        return "\"" cat escape_string(value) cat "\"";
    elif t cmpeq Rec and IsJSONNode(value) then
        // This is a node
        return node_to_string(value : formatted:=formatted,
                                      indent:=indent cat "  ");
    elif t cmpeq List then
        // This is a list
        return list_to_string(value : formatted:=formatted,indent:=indent);
    elif t cmpeq SeqEnum then
        // This is a list
        return list_to_string(value : formatted:=formatted,indent:=indent);
    elif t cmpeq RngIntElt then
        // This is an integer
        return IntegerToString(value);
    elif t cmpeq FldRatElt and IsIntegral(value) then
        // This is an integer
        return IntegerToString(Numerator(value));
    elif t cmpeq BoolElt then
        // This is a boolean
        return value select "true" else "false";
    elif t cmpeq FldReElt then
        // This is a float
        return Sprintf("%o", RR!value);
    end if;
    // If we're here then we don't support this value type
    error "Unsupported type for value";
end function;

// Converts the given node to a string
function node_to_string(node : formatted:=false, indent:="")
    // Handle the empty node
    if not assigned node`keys or #node`keys eq 0 then
        return "{}";
    end if;
    // Open the node
    str:="{";
    if formatted then
        str cat:= "\n";
    end if;
    // Add the key: value pairs
    len:=#node`keys;
    for i in [1..len] do
        // Make a note of the key and value
        key:=node`keys[i];
        value:=node`values[i];
        // Write out the key
        if formatted then
            str cat:= indent cat "  ";
        end if;
        str cat:= "\"" cat escape_string(key) cat "\":";
        if formatted then
            str cat:= " ";
        end if;
        // Write out the entry
        str cat:= value_to_string(value : formatted:=formatted, indent:=indent);
        // Finish off this entry
        if i lt len then
            str cat:= ",";
        end if;
        if formatted then
            str cat:= "\n";
        end if;
    end for;
    // Close the node
    if formatted then
        str cat:= indent;
    end if;
    return str cat "}";
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic IsInt64( x::FldRatElt ) -> BoolElt
{}
    return IsIntegral(x) and x ge INT64_MIN and x le INT64_MAX;
end intrinsic;

intrinsic IsInt64( x::RngIntElt ) -> BoolElt
{True iff the integer fits in an int64}
    return x ge INT64_MIN and x le INT64_MAX;
end intrinsic;

intrinsic JSONToString( x::Rec : formatted:=false ) -> MonStgElt
{}
    // Sanity check
    require IsJSONNode(x): "Argument must be a JSON node";
    require Type(formatted) cmpeq BoolElt: "Optional parameter 'formatted' must be a boolean";
    // Create the JSON string
    return node_to_string(x : formatted:=formatted);
end intrinsic;

intrinsic JSONToString( x::List : formatted:=false ) -> MonStgElt
{Outputs x as a JSON-formatted string}
    // Sanity check
    ok,idx,msg:=is_json_list(x);
    require ok: Sprintf("Error validating entry %o: %o",idx,msg);
    require Type(formatted) cmpeq BoolElt: "Optional parameter 'formatted' must be a boolean";
    // Create the JSON string
    return list_to_string(x : formatted:=formatted);
end intrinsic;

intrinsic JSONCreateNode() -> Rec
{}
    return create_JSON_node();
end intrinsic;

intrinsic JSONCreateNode( keys::SeqEnum[MonStgElt], values::List ) -> Rec
{Create a new JSON node with the given key-value pairs.}
    // Sanity check
    require #keys eq #values: "Argument 1 and argument 2 must be of the same length";
    ok,idx,msg:=is_json_list(values);
    require ok: Sprintf("Error validating entry %o of argument 2: %o",idx,msg);
    // Create the node
    node:=create_JSON_node();
    for i in [1..#keys] do
        append_to_JSON_node(~node,keys[i],values[i]);
    end for;
    return node;
end intrinsic;

intrinsic JSONCreateNode( pairs::Any,... ) -> Rec
{Create a new JSON node with the given key-value pairs. Each element of pairs must be of the form [* key, value *] where key is a string.}
    // Create the node, validating as we go
    node:=create_JSON_node();
    for i in [1..#pairs] do
        x:=pairs[i];
        require Type(x) cmpeq List: Sprintf("Argument %o must be a List.",i);
        require #x eq 2: Sprintf("Argument %o must be a List of length 2.",i);
        k:=x[1];
        require Type(k) cmpeq MonStgElt: Sprintf("The first element of argument %o must be a string.",i);
        v:=x[2];
        ok, msg := is_json_value(v);
        require ok: Sprintf("Error validating the second element of argument %o: %o",i,msg);
        append_to_JSON_node(~node,k,v);
    end for;
    return node;
end intrinsic;

intrinsic JSONCreateNode( A::Assoc ) -> Rec
{Create a new JSON node with the given key-value pairs. The keys of the associative array must be strings.}
    // Create the node, validating as we go
    node:=create_JSON_node();
    for k in Keys(A) do
        require Type(k) cmpeq MonStgElt: "Argument keys must be strings.";
        v:=A[k];
        ok, msg := is_json_value(v);
        require ok: Sprintf("Error validating the value for key \"%o\": %o",k,msg);
        append_to_JSON_node(~node,k,v);
    end for;
    return node;
end intrinsic;

intrinsic JSONAppend( ~node::Rec, key::MonStgElt, value::MonStgElt )
{}
    // Sanity check
    require IsJSONNode(node): "Argument 1 must be a JSON node";
    // Append the key: value pair
    append_to_JSON_node(~node,key,value);
end intrinsic;

intrinsic JSONAppend( ~node::Rec, key::MonStgElt, value::SeqEnum[MonStgElt] )
{}
    // Sanity check
    require IsJSONNode(node): "Argument 1 must be a JSON node";
    // Append the key: value pair
    append_to_JSON_node(~node,key,[* x : x in value *]);
end intrinsic;

intrinsic JSONAppend( ~node::Rec, key::MonStgElt, value::RngIntElt )
{}
    // Sanity check
    require IsJSONNode(node): "Argument 1 must be a JSON node";
    // Append the key: value pair
    append_to_JSON_node(~node,key,value);
end intrinsic;

intrinsic JSONAppend( ~node::Rec, key::MonStgElt, value::SeqEnum[RngIntElt] )
{}
    // Sanity check
    require IsJSONNode(node): "Argument 1 must be a JSON node";
    // Append the key: value pair
    append_to_JSON_node(~node,key,[* x : x in value *]);
end intrinsic;

intrinsic JSONAppend( ~node::Rec, key::MonStgElt, value::BoolElt )
{}
    // Sanity check
    require IsJSONNode(node): "Argument 1 must be a JSON node";
    // Append the key: value pair
    append_to_JSON_node(~node,key,value);
end intrinsic;

intrinsic JSONAppend( ~node::Rec, key::MonStgElt, value::SeqEnum[BoolElt] )
{}
    // Sanity check
    require IsJSONNode(node): "Argument 1 must be a JSON node";
    // Append the key: value pair
    append_to_JSON_node(~node,key,[* x : x in value *]);
end intrinsic;

intrinsic JSONAppend( ~node::Rec, key::MonStgElt, value::Rec )
{}
    // Sanity check
    require IsJSONNode(node): "Argument 1 must be a JSON node";
    require IsJSONNode(value): "Argument 3 must be a JSON node";
    // Append the key: value pair
    append_to_JSON_node(~node,key,value);
end intrinsic;

intrinsic JSONAppend( ~node::Rec, key::MonStgElt, value::List )
{Appends the key: value pair to the end of the JSON node}
    // Sanity check
    require IsJSONNode(node): "Argument 1 must be a JSON node";
    ok,idx,msg:=is_json_list(value);
    require ok: Sprintf("Error validating entry %o of argument 3: %o",idx,msg);
    // Append the key: value pair
    append_to_JSON_node(~node,key,value);
end intrinsic;

intrinsic JSONInsert( ~node::Rec, i::RngIntElt, key::MonStgElt, value::MonStgElt )
{}
    // Sanity check
    require IsJSONNode(node): "Argument 1 must be a JSON node";
    require i gt 0: "Argument 2 must be positive";
    N:=#node`keys+1;
    require i le N: Sprintf("Argument 2 must be in the range [1..%o]",N);
    // Insert the key: value pair
    Insert(~node`keys,i,key);
    Insert(~node`values,i,value);
end intrinsic;

intrinsic JSONInsert( ~node::Rec, i::RngIntElt, key::MonStgElt, value::SeqEnum[MonStgElt] )
{}
    // Sanity check
    require IsJSONNode(node): "Argument 1 must be a JSON node";
    require i gt 0: "Argument 2 must be positive";
    N:=#node`keys+1;
    require i le N: Sprintf("Argument 2 must be in the range [1..%o]",N);
    // Insert the key: value pair
    Insert(~node`keys,i,key);
    Insert(~node`values,i,[* x : x in value *]);
end intrinsic;

intrinsic JSONInsert( ~node::Rec, i::RngIntElt, key::MonStgElt, value::RngIntElt )
{}
    // Sanity check
    require IsJSONNode(node): "Argument 1 must be a JSON node";
    require i gt 0: "Argument 2 must be positive";
    N:=#node`keys+1;
    require i le N: Sprintf("Argument 2 must be in the range [1..%o]",N);
    // Insert the key: value pair
    Insert(~node`keys,i,key);
    Insert(~node`values,i,value);
end intrinsic;

intrinsic JSONInsert( ~node::Rec, i::RngIntElt, key::MonStgElt, value::SeqEnum[RngIntElt] )
{}
    // Sanity check
    require IsJSONNode(node): "Argument 1 must be a JSON node";
    require i gt 0: "Argument 2 must be positive";
    N:=#node`keys+1;
    require i le N: Sprintf("Argument 2 must be in the range [1..%o]",N);
    // Insert the key: value pair
    Insert(~node`keys,i,key);
    Insert(~node`values,i,[* x : x in value *]);
end intrinsic;

intrinsic JSONInsert( ~node::Rec, i::RngIntElt, key::MonStgElt, value::BoolElt )
{}
    // Sanity check
    require IsJSONNode(node): "Argument 1 must be a JSON node";
    require i gt 0: "Argument 2 must be positive";
    N:=#node`keys+1;
    require i le N: Sprintf("Argument 2 must be in the range [1..%o]",N);
    // Insert the key: value pair
    Insert(~node`keys,i,key);
    Insert(~node`values,i,value);
end intrinsic;

intrinsic JSONInsert( ~node::Rec, i::RngIntElt, key::MonStgElt, value::SeqEnum[BoolElt] )
{}
    // Sanity check
    require IsJSONNode(node): "Argument 1 must be a JSON node";
    require i gt 0: "Argument 2 must be positive";
    N:=#node`keys+1;
    require i le N: Sprintf("Argument 2 must be in the range [1..%o]",N);
    // Insert the key: value pair
    Insert(~node`keys,i,key);
    Insert(~node`values,i,[* x : x in value *]);
end intrinsic;

intrinsic JSONInsert( ~node::Rec, i::RngIntElt, key::MonStgElt, value::Rec )
{}
    // Sanity check
    require IsJSONNode(node): "Argument 1 must be a JSON node";
    require i gt 0: "Argument 2 must be positive";
    N:=#node`keys+1;
    require i le N: Sprintf("Argument 2 must be in the range [1..%o]",N);
    require IsJSONNode(value): "Argument 4 must be a JSON node";
    // Insert the key: value pair
    Insert(~node`keys,i,key);
    Insert(~node`values,i,value);
end intrinsic;

intrinsic JSONInsert( ~node::Rec, i::RngIntElt, key::MonStgElt, value::List )
{Inserts the key: value pair into position i in the JSON node}
    // Sanity check
    require IsJSONNode(node): "Argument 1 must be a JSON node";
    require i gt 0: "Argument 2 must be positive";
    N:=#node`keys+1;
    require i le N: Sprintf("Argument 2 must be in the range [1..%o]",N);
    ok,idx,msg:=is_json_list(value);
    require ok: Sprintf("Error validating entry %o of argument 4: %o",idx,msg);
    // Insert the key: value pair
    Insert(~node`keys,i,key);
    Insert(~node`values,i,value);
end intrinsic;

intrinsic JSONDelete( ~node::Rec, idx::RngIntElt )
{Removes the key: value pair at index idx from the given JSON node}
    // Sanity check
    require IsJSONNode(node): "Argument 1 must be a JSON node";
    N:=#node`keys;
    require idx ge 1 and idx le N: Sprintf("Argument 2 must be in the range [1..%o]",N);
    // Remove the key: value pair
    Remove(~node`keys,idx);
    Remove(~node`values,idx);
end intrinsic;

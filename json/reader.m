freeze;

/////////////////////////////////////////////////////////////////////////
// reader.m
/////////////////////////////////////////////////////////////////////////
// Functions for reading basic JSON documents.
// Known limitations:
//   - Unicode escape sequences of the forn \u + four integers are not
//     unescaped, since Magma does not support unicode.
//   - Only integer numbers are supported; floats and scientific notation
//     will cause an error.
//   - There is not equivalent to the null value in Magma, so this will
//     be interpreted as an empty string.
/////////////////////////////////////////////////////////////////////////

// IEEE-754 64-bit floats have 52 bits of precision.
RR := RealField(52 : Bits:=true);

INT64_MIN := -9223372036854775808;
INT64_MAX :=  9223372036854775807;
FLOAT64_MAX := RR!1.79769313486231570814527423731704356798070e+308;

// The set of digits
digits:={"0","1","2","3","4","5","6","7","8","9"};

// The set of valid numeric characters
numeric:=Include(digits,"-");

// The JSON node record structure
JSONNodeRec:=recformat< type:PowerStructure(MonStgElt),
                        keys:PowerSequence(Strings()),
                        values:PowerStructure(List) >;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Returns a snippet of the string S, centred around the given index.
function create_snippet(S,idx)
    start:=Max(1,idx-25);
    finish:=Min(#S,idx+25);
    snippet:=S[start..finish];
    if start ne 1 then
        snippet:="..." cat snippet;
    end if;
    if finish ne #S then
        snippet cat:= "...";
    end if;
    return snippet;
end function;

// Returns a new, empty JSON node.
function create_JSON_node()
    return rec< JSONNodeRec | type:="jsonnode",
                             keys:=[Strings()|],
                             values:=[* *] >;
end function;

// Appends the give key: value pair to the JSON node.
procedure append_to_JSON_node(~node,key,value)
    Append(~node`keys,key);
    Append(~node`values,value);
end procedure;

// Starting at the given index, skips over any white space until the first
// non-white-space character. Returns the index if the character is c, 0
// otherwise.
function skip_to_char(S,idx,c)
    idx:=IndexOfNonWhiteSpace(S,idx);
    return idx eq 0 or S[idx] ne c select 0 else idx;
end function;

// Extracts the quoted string from S, starting at offset idx, and returning
// true,string,idx on success, where idx is the index of the final quote in the
// string. Returns false,errstring,idx on failure, where errstring is a
// description of what went wrong, and idx is the index of the character
// causing the error.
// Note: We do not support unicode encoding.
function read_string(S,idx)
    // The first character had better be a quote
    startidx:=skip_to_char(S,idx,"\"");
    if startidx eq 0 then
        return false,"Expected opening quote '\"'",idx;
    end if;
    idx:=startidx;
    // Extract the string
    str:="";
    is_escaped:=false;
    for i in [idx+1..#S] do
        if is_escaped then
            // Only ",\,/,b,f,n,r,t, and unicodes are valid escaped sequences
            c:=S[i];
            if c eq "\"" or c eq "\\" or c eq "/" then
                str cat:= c;
            elif c eq "b" then
                str cat:= "\b";
            elif c eq "f" then
                str cat:= "\f";
            elif c eq "n" then
                str cat:= "\n";
            elif c eq "r" then
                str cat:= "\r";
            elif c eq "t" then
                str cat:= "\t";
            elif c eq "u" then
                // We don't support unicode sequences, so leave it encoded
                str cat:= "\\u";
            else
                msg:=Sprintf("Invalid escape sequence '\\%o'",c);
                return false,msg,i;
            end if;
            is_escaped:=false;
        elif S[i] eq "\\" then
            is_escaped:=true;
        elif S[i] eq "\"" then
            return true,str,i;
        else
            str cat:=S[i];
        end if;
    end for;
    // If we're here then the string didn't terminate
    return false,"Expected closing quote '\"'",#S;
end function;

// Extracts a number x from S, starting at offset idx, and returning true,x,idx
// on success, where idx is the index of the last character in the string that
// forms the number. Returns false,errstring,idx on failure, where errstring is
// a description of what went wrong, and idx is the index of the character
// causing the error.
function read_number(S,idx)
    // Skip over any white space
    idx:=IndexOfNonWhiteSpace(S,idx);
    if idx eq 0 then
        return false,"Unable to parse number",#S;
    end if;
    // Does this begin with a "-"?
    hasMinus:=false;
    if S[idx] eq "-" then
        hasMinus:=true;
        if idx eq #S then
            return false,"Unable to parse number",idx;
        end if;
        idx +:= 1;
    end if;
    // Read in decimal digits
    start:=idx;
    while idx le #S and S[idx] in digits do
        idx +:= 1;
    end while;
    if idx eq start then
        return false,"Unable to parse number",idx;
    end if;
    n:=StringToInteger(S[start..idx - 1]);
    if hasMinus then
        n *:= -1;
    end if;
    // Have we reached the end?
    if idx gt #S then
        return true,n,#S;
    end if;
    // Is the next character a period?
    if S[idx] eq "." then
        // Read in the number after the period
        idx +:= 1;
        start:=idx;
        while idx le #S and S[idx] in digits do
            idx +:= 1;
        end while;
        if idx eq start then
            return false,"Unable to parse number",idx;
        end if;
        m:=StringToInteger(S[start..idx - 1]);
        if hasMinus then
            m *:= -1;
        end if;
        // Build the number
        x:=RR!n + (RR!m)/10^(idx-start);
        // Have we reached the end?
        if idx gt #S then
            return true,x,#S;
        end if;
    else
        // So far, our number is an integer
        x := n;
    end if;
    // Is there an exponent?
    if S[idx] eq "e" or S[idx] eq "E" then
        // Read in the exponent
        if idx eq #S then
            return false,"Unable to parse number",idx;
        end if;
        idx +:= 1;
        // Does it begin with a "-"?
        eHasMinus:=false;
        if S[idx] eq "-" then
            eHasMinus:=true;
            if idx eq #S then
                return false,"Unable to parse number",idx;
            end if;
            idx +:= 1;
        end if;
        // Read in decimal digits
        start:=idx;
        while idx le #S and S[idx] in digits do
            idx +:= 1;
        end while;
        if idx eq start then
            return false,"Unable to parse number",idx;
        end if;
        e:=StringToInteger(S[start..idx - 1]);
        if eHasMinus then
            e *:= -1;
        end if;
        // Take account of the exponent
        x *:= (RR!10.0)^e;
        // Have we reached the end?
        if idx gt #S then
            return true,x,#S;
        end if;
    end if;
    // Skip over any remaining white space before returning
    idx:=IndexOfNonWhiteSpace(S,idx);
    if idx eq 0 then
        return true,x,#S;
    end if;
    return true,x,idx-1;
end function;

// Extracts a boolean b from S, starting at offset idx, and returning
// true,b,idx on success, where idx is the index of the character in the string
// that forms the boolean. Returns false,errstring,idx on failure, where
// errstring is a description of what went wrong, and idx is the index of the
// character causing the error.
function read_boolean(S,idx)
    // Move to the first non-white space character
    startidx:=IndexOfNonWhiteSpace(S,idx);
    if startidx eq 0 then
        return false,"Invalid formatted value",idx;
    end if;
    idx:=startidx;
    // This determines whether the value is "true" or "false"
    c:=S[idx];
    if c eq "t" and idx+3 le #S and S[idx..idx+3] eq "true" then
        return true,true,idx+3;
    elif c eq "f" and idx+4 le #S and S[idx..idx+4] eq "false" then
        return true,false,idx+4;
    end if;
    // No luck
    return false,"Invalid formatted value",idx;
end function;

// Extracts a null value from S, starting at offset idx, and returning
// true,"",idx on success, where idx is the index of the character in the string
// that forms the null value. Returns false,errstring,idx on failure, where
// errstring is a description of what went wrong, and idx is the index of the
// character causing the error.
// Note: Magma has no concept of null, so we used an empty string.
function read_null(S,idx)
    // The first character had better be "n"
    startidx:=skip_to_char(S,idx,"n");
    if startidx eq 0 then
        return false,"Invalid formatted value",idx;
    end if;
    idx:=startidx;
    // The string should be "null"
    if idx+3 le #S and S[idx..idx+3] eq "null" then
        return true,"",idx+3;
    end if;
    // No luck
    return false,"Invalid formatted value",idx;
end function;

// Parses the list block "[ ... ]" from the string S, starting at offset idx,
// and returning true,list,idx on success, where idx is the index of the final
// closing square bracket "]" in the list. Returns false,errstring,idx on
// failure, where errstring is a description of what went wrong, and idx is the
// index of the character causing the error.
forward read_value;
function read_list(S,idx)
    // The first character must be a open-square bracket
    startidx:=skip_to_char(S,idx,"[");
    if startidx eq 0 then
        return false,"Expected opening square bracket '['",idx;
    end if;
    idx:=startidx;
    // Create an empty list
    list:=[* *];
    // Move to the first non-white space character
    startidx:=IndexOfNonWhiteSpace(S,idx+1);
    if startidx eq 0 then
        return false,"Expected closing square bracket ']'",idx;
    end if;
    idx:=startidx;
    // If this is a close-square bracket then the list is empty
    if S[idx] eq "]" then
        return true,list,idx;
    end if;
    idx -:= 1;
    // Loop until done
    done:=false;
    while not done do
        // Parse the value
        ok,value,idx:=read_value(S,idx+1);
        if not ok then
            return false,value,idx;
        end if;
        // Append the value to the list
        Append(~list,value);
        // The next character should either be a comma or a close-square bracket
        startidx:=IndexOfNonWhiteSpace(S,idx+1);
        if startidx eq 0 then
            return false,"Expected closing square bracket ']'",idx;
        end if;
        idx:=startidx;
        c:=S[idx];
        if c eq "]" then
            done:=true;
        elif c ne "," then
            return false,"Expected comma ',' or closing square bracket ']'",idx;
        end if;
    end while;
    // Success -- return the list
    return true,list,idx;
end function;

// Extracts the value from S, starting at offset idx, and returning
// true,value,idx on success, where idx is the index of the final character in
// the string that determines the value. Returns false,errstring,idx on
// failure, where errstring is a description of what went wrong, and idx is the
// index of the character causing the error.
forward parse_JSON;
function read_value(S,idx)
    // Move to the first non-white space character
    startidx:=IndexOfNonWhiteSpace(S,idx);
    if startidx eq 0 then
        return false,"Expected value",idx;
    end if;
    idx:=startidx;
    // How we proceed depends on the next character
    ok:=false;
    value:="Invalid formatted value";
    c:=S[idx];
    if c eq "\"" then
        // This is a string
        ok,value,idx:=read_string(S,idx);
    elif c eq "{" then
        // This is a node
        ok,value,idx:=parse_JSON(S,idx);
    elif c eq "[" then
        // This is a list
        ok,value,idx:=read_list(S,idx);
    elif c in numeric then
        // This is a number
        ok,value,idx:=read_number(S,idx);
    elif c eq "t" or c eq "f" then
        // This is a boolean
        ok,value,idx:=read_boolean(S,idx);
    elif c eq "n" then
        // This is a null value
        ok,value,idx:=read_null(S,idx);
    end if;
    // Return the value
    return ok,value,idx;
end function;

// Parses the JSON block "{ ... }" from the string S, starting at offset idx,
// and returning true,node,idx on success, where idx is the index of the final
// closing curly brace "}" in the block. Returns false,errstring,idx on
// failure, where errstring is a description of what went wrong, and idx is the
// index of the character causing the error.
function parse_JSON(S,idx)
    // The first character must be a open-curly brace
    startidx:=skip_to_char(S,idx,"{");
    if startidx eq 0 then
        return false,"Expected opening curly brace '{'",idx;
    end if;
    idx:=startidx;
    // Create an empty node
    node:=create_JSON_node();
    // Move to the first non-white space character
    startidx:=IndexOfNonWhiteSpace(S,idx+1);
    if startidx eq 0 then
        return false,"Expected closing curly brace '}'",idx;
    end if;
    idx:=startidx;
    // If this is a close-curly brace then the node is empty
    if S[idx] eq "}" then
        return true,node,idx;
    end if;
    idx -:= 1;
    // Loop until done
    done:=false;
    while not done do
        // Extract the key
        ok,key,idx:=read_string(S,idx+1);
        if not ok then
            return false,key,idx;
        end if;
        // The next character has to be a colon
        startidx:=skip_to_char(S,idx+1,":");
        if startidx eq 0 then
            return false,"Expected colon ':'",idx;
        end if;
        idx:=startidx;
        // Parse the value
        ok,value,idx:=read_value(S,idx+1);
        if not ok then
            return false,value,idx;
        end if;
        // Append the key: value to the node
        append_to_JSON_node(~node,key,value);
        // The next character should either be a comma or a close-curly brace
        startidx:=IndexOfNonWhiteSpace(S,idx+1);
        if startidx eq 0 then
            return false,"Expected closing curly brace '}'",idx;
        end if;
        idx:=startidx;
        c:=S[idx];
        if c eq "}" then
            done:=true;
        elif c ne "," then
            return false,"Expected comma ',' or closing curly brace '}'",idx;
        end if;
    end while;
    // Success -- return the node
    return true,node,idx;
end function;

/////////////////////////////////////////////////////////////////////////
// Main functions
/////////////////////////////////////////////////////////////////////////

// True iff this is a JSON node
intrinsic IsJSONNode( x::Any ) -> BoolElt
{True iff the argument is a JSON node}
    return Type(x) cmpeq Rec and
           "type" in Names(x) and
           assigned x`type and
           x`type eq "jsonnode";
end intrinsic;

intrinsic JSONParseString( str::MonStgElt ) -> Rec
{Attempts to parse the JSON string into a JSON node}
    // Parse the string into a JSON node
    ok,node,idx:=parse_JSON(str,1);
    require ok: Sprintf("Not valid JSON: %o.\nError at or around: %o",node,create_snippet(str,idx));
    // The rest of the string should only consist of white space
    endidx:=IndexOfNonWhiteSpace(str,idx+1);
    require endidx eq 0: Sprintf("Not valid JSON: String has unexpected trailing content.\nError at or around: %o",create_snippet(str,endidx));
    return node;
end intrinsic;

intrinsic JSONParseStringToList( str::MonStgElt ) -> List
{Attempts to parse the JSON string into a list of JSON nodes}
    // Parse the string into a list of JSON nodes
    ok,list,idx:=read_list(str,1);
    require ok: Sprintf("Not valid JSON: %o.\nError at or around: %o",list,create_snippet(str,idx));
    // The rest of the string should only consist of white space
    endidx:=IndexOfNonWhiteSpace(str,idx+1);
    require endidx eq 0: Sprintf("Not valid JSON: String has unexpected trailing content.\nError at or around: %o",create_snippet(str,endidx));
    return list;
end intrinsic;

intrinsic JSONNumberOfKeys( node::Rec ) -> RngIntElt
{The number of keys in the given JSON node}
    // Sanity check
    require IsJSONNode(node): "Argument must be a JSON node";
    // Return the number of keys
    return assigned node`keys select #node`keys else 0;
end intrinsic;

intrinsic JSONKeys( node::Rec ) -> SeqEnum[MonStgElt]
{The keys in the given JSON node}
    // Sanity check
    require IsJSONNode(node): "Argument must be a JSON node";
    // Return the keys
    return assigned node`keys select node`keys
                                else [Strings()|];
end intrinsic;

intrinsic JSONIsDefined( node::Rec, key::MonStgElt ) -> BoolElt, Any
{Return whether a value is set for the given key in the given JSON node, and if so, return the first value with given key}
    // Sanity check
    require IsJSONNode(node): "Argument 1 must be a JSON node";
    // Is this a key?
    if assigned node`keys then
        idx:=Index(node`keys,key);
        if idx ne 0 then
            return true,node`values[idx];
        end if;
    end if;
    return false,_;
end intrinsic;

intrinsic JSONValues( node::Rec ) -> List
{The values in the given JSON node}
    // Sanity check
    require IsJSONNode(node): "Argument must be a JSON node";
    // Return the values
    return assigned node`values select node`values else [* *];
end intrinsic;

intrinsic JSONValues( node::Rec, key::MonStgElt ) -> List
{The values in the given JSON node with given key}
    // Sanity check
    require IsJSONNode(node): "Argument must be a JSON node";
    // Return the values
    if assigned node`keys then
        return [* node`values[i] : i in [1..#node`keys] | node`keys[i] eq key*];
    end if;
    return [* *];
end intrinsic;

intrinsic JSONValue( node::Rec, key::MonStgElt ) -> Any
{The first value associated to the key in the given JSON node}
    // Sanity check
    require IsJSONNode(node): "Argument 1 must be a JSON node";
    // Compute the index
    require assigned node`keys: "Argument 2 is not a key of argument 1";
    idx:=Index(node`keys,key);
    require idx ne 0: "Argument 2 is not a key of argument 1";
    return node`values[idx];
end intrinsic;

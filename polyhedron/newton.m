freeze;

/////////////////////////////////////////////////////////////////////////
// newton.m
/////////////////////////////////////////////////////////////////////////
// Intrinsic to compute the Newton polytope of a multivariate polynomial
/////////////////////////////////////////////////////////////////////////

intrinsic NewtonPolytope(f::RngMPolElt : return_coeffs:=false) -> TorPol, SeqEnum
{The Newton polytope P of the rational function f (regarded as a Laurent polynomial). Set 'return_coeffs' to true to also return the coefficients associated with the points of P. The coefficients are placed in bijection with Sort(SetToSequence(Points(P))).}
    require not IsZero(f): "Argument must be non-zero";
    require Rank(Parent(f)) ne 0: "Argument must lie in a non-trivial polynomial ring";
    require Type(return_coeffs) cmpeq BoolElt: "Optional parameter 'return_coeffs' must be a boolean";
    cs, es := CoefficientsAndExponents(f);
    P := Polytope(es);
    if not return_coeffs then
        return P, _;
    end if;
    pts := Sort([Eltseq(v) : v in Points(P)]);
    coeffs := [Universe(cs)|];
    for p in pts do
        idx := Index(es, p);
        if idx eq 0 then
            Append(~coeffs, 0);
        else
            Append(~coeffs, cs[idx]);
        end if;
    end for;
    return P, coeffs;
end intrinsic;

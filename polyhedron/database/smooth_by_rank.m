freeze;

/////////////////////////////////////////////////////////////////////////
// smooth_by_rank.m
/////////////////////////////////////////////////////////////////////////
// Access functions for the database of smooth Fano polytopes,
// partitioned by Picard rank.
/////////////////////////////////////////////////////////////////////////
// * The bound on the Picard rank is from:
//    C. Casagrande, "The number of vertices of a Fano polytope",
//    Ann. Inst. Fourier (Grenoble) (2006) 121-130.
/////////////////////////////////////////////////////////////////////////

import "../../core.m": save_data, fetch_data;
import "read.m": read_line;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Returns the number of smooth polygons of dimension n, rank r. Assumes that
// the values for n and r are sensible.
function number_of_polygons(n,r)
    num := case <n |
        2 : [1,2,1,1],
        3 : [1,4,7,4,2],
        4 : [1,9,28,47,27,10,1,1],
        5 : [1,15,91,268,312,137,35,5,2],
        6 : [1,26,257,1318,2807,2204,771,186,39,11,1,1],
        7 : [1,40,643,5347,19516,26312,14758,4362,1013,214,43,5,2],
        8 : [1,62,1511,19453,114697,253950,226867,98657,26831,6281,1286,243,
             40,11,1,1],
        default: 0>;
    return num[r];
end function;

// Given the dimension n and rank r, returns the corresponding block id.
// Assumes that the values for n and r are sensible.
function nr_to_block(n,r)
    return &+[Integers() | IsEven(i) select 2 * i else 2 * i - 1 :
                                                        i in [2..n - 1]] + r;
end function;

// Extracts the k-th id from the (n,r) block file, where n is the dimension,
// and r the rank. Returns true followed by the id on success, false followed
// by an error message otherwise. We assume that the values for n, r, and k are
// sensible.
function fetch_id(n,r,k)
    // Compute the line number and section number
    line:=((k - 1) div 100) + 1;
    sec:=((k - 1) mod 100) + 1;
    // Perhaps the line is in the cache?
    bool,data:=fetch_data("smooth_by_rank_last_line");
    if bool and data[1] eq n and data[2] eq r and data[3] eq line then
        S:=data[4..#data];
        if #S lt sec then
            return false,"The database \"smooth_by_rank\" is corrupted";
        end if;
        return true,&+S[1..sec];
    end if;
    // We missed the cache...
    bool,L:=read_line("smooth_by_rank",nr_to_block(n,r),line);
    if not bool then return false,L; end if;
    // Try to convert the line into a sequence of integers
    try
        S:=[Integers() | StringToInteger(c) : c in Split(L," ")];
    catch e
        return false,"The database \"smooth_by_rank\" is corrupted";
    end try;
    if #S lt sec then
        return false,"The database \"smooth_by_rank\" is corrupted";
    end if;
    // Looks good -- cache the line and return the id
    save_data("smooth_by_rank_last_line",[n,r,line] cat S);
    return true,&+S[1..sec];
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic PolytopeSmoothFanoPicardRank(n::RngIntElt,r::RngIntElt) -> RngIntElt
{The number of smooth Fano polytopes of dimension n with Picard rank r in the database of smooth Fano polytopes}
    // Sanity check
    require n ge 2 and n le 8: "The dimension must be in the range 2..8";
    max_r:=IsEven(n) select 2 * n else 2 * n - 1;
    require r ge 1 and r le max_r:
        Sprintf("The Picard rank must be in the range 1..%o",max_r);
    // Return the number of smooth polytopes
    return number_of_polygons(n,r);
end intrinsic;

intrinsic PolytopeSmoothFanoPicardRank(n::RngIntElt,r::RngIntElt,k::RngIntElt)
    -> TorPol,RngIntElt
{The k-th smooth Fano polytope of dimension n with Picard rank r in the database of smooth Fano polytopes. Also gives the ID of the polytope in the smooth Fano polytopes database.}
    // Sanity check
    require n ge 2 and n le 8: "The dimension must be in the range 2..8";
    max_r:=IsEven(n) select 2 * n else 2 * n - 1;
    require r ge 1 and r le max_r:
        Sprintf("The Picard rank must be in the range 1..%o",max_r);
    max_id:=number_of_polygons(n,r);
    require k ge 1 and k le max_id:
        Sprintf("Argument 3 must be in the range 1..%o",max_id);
    // Fetch the ID
    bool,id:=fetch_id(n,r,k);
    require bool: id;
    // Return the data
    return PolytopeSmoothFano(id),id;
end intrinsic;

freeze;

/////////////////////////////////////////////////////////////////////////
// minkowski_dim3_buckets.m
/////////////////////////////////////////////////////////////////////////
// Functions and intrinsics for accessing the lattice Minkowski database
// of 3-dimensional reflexive buckets.
/////////////////////////////////////////////////////////////////////////

import "../../core.m": save_data, fetch_data;
import "read.m": open_db_file;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Atempts to convert a name into an id. Returns true followed by the id on
// success. If the name is valid, but there is no corresponding Minkowski
// period, then returns an id of 0. If the name is invalid, returns false.
function minkowski_3d_name_to_id(name)
    // Begin by normalising the name
    s:="";
    last_c:="";
    for i in [1..#name] do
        c:=name[i];
        if c eq "b" then
            c:="B";
        elif c eq "p" then
            c:="P";
        elif c eq "q" then
            c:="Q";
        elif c eq "v" then
            c:="V";
        end if;
        if (c ne "{" and c ne "}" and c ne " " and c ne "\n" and c ne "\t" and
           c ne "-" and c ne "P") or (c eq "-" and last_c ne "-") or
           (c eq "P" and last_c ne "P") then
            s cat:= c;
            last_c:=c;
        end if;
    end for;
    // Extract the names from the cache (or create them)
    bool,names:=fetch_data("minkowski3d_buckets_names");
    if not bool then
        names:=["P3","P^3","2-33","Q3","Q^3","2-30","2-28","2-36","2-35","3-29",
                "2-34","3-30","3-26","3-22","3-31","2-31","3-25","3-23","3-19",
                "2-27","3-14","3-9","2-32","3-28","4-13","3-24","4-12","2-29",
                "4-10","3-20","3-17","3-18","3-16","2-25","2-24","3-27","B5",
                "B_5","4-11","3-21","4-9","4-8","2-26","5-2","4-7","3-15","4-5",
                "2-22","3-13","3-11","2-18","B4","B_4","5-3","2-23","4-6","4-4",
                "2-21","3-12","2-19","2-20","4-3","3-10","5-1","2-17","3-7",
                "2-16","B3","B_3","6-1","2-15","4-2","4-1","3-8","V22","V_22",
                "3-6","2-12","2-13","2-11","2-14","V18","V_18","3-3","7-1",
                "3-5","2-9","B2","B_2","3-4","V16","V_16","2-8","2-10","V14",
                "V_14","2-7","2-6","V12","V_12","3-1","8-1","3-2","2-5","V10",
                "V_10","2-4","V8","V_8","V6","V_6","V4","V_4","V2","V_2","B1",
                "B_1","2-1","2-2","2-3","9-1","10-1"];
        save_data("minkowski3d_buckets_names",names);
    end if;
    // Do we recognise this name?
    idx:=Index(names,s);
    if idx eq 0 then return false,_; end if;
    // Extract the ids from the cache (or create them)
    bool,ids:=fetch_data("minkowski3d_buckets_ids");
    if not bool then
        ids:=[Integers() | 1,1,2,3,3,4,5,6,7,8,10,11,12,13,14,15,16,17,18,19,21,
                22,24,28,29,31,34,35,37,38,39,41,42,43,44,45,46,46,48,49,54,57,
                58,64,65,67,68,69,70,72,74,75,75,76,78,81,83,84,85,86,87,88,99,
                100,101,103,104,106,106,107,109,110,111,112,113,113,117,118,119,
                120,122,124,124,135,136,138,139,140,140,142,143,143,144,145,147,
                147,148,149,150,150,154,155,157,158,160,160,161,163,163,164,164,
                165,165,0,0,0,0,0,0,0,0,0];
        save_data("minkowski3d_buckets_ids",ids);
    end if;
    // Return success, followed by the id
    return true,ids[idx];
end function;

// Returns true along with the bucket data upon success, false and an error
// string otherwise.
function load_buckets()
    // Attempt to open the file for reading
    bool,fh:=open_db_file("minkowski3d.txt");
    if not bool then return false,fh; end if;
    // Start importing the contents
    buckets:=[];
    L:=Gets(fh);
    while not IsEof(L) do
        b:=eval "[" cat L cat "]";
        for I in b do
            if Type(I) ne Tup or #I ne 2 or Type(I[1]) ne RngIntElt or
               I[1] le 0 or I[1] gt 4319 or Type(I[2]) ne SeqEnum then
                return false,"The database file \"minkowski3d.txt\" is corrupted.";
            end if;
        end for;
        Append(~buckets,b);
        L:=Gets(fh);
    end while;
    // Perform a quick sanity check before returning success
    if #buckets ne 165 then
        return false,"The database file \"minkowski3d.txt\" is corrupted.";
    end if;
    return true,buckets;
end function;

// Returns true followed by the bucket data on success, false followed by an
// error message otherwise.
function fetch_buckets()
    // Are the buckets in the cache?
    bool,buckets:=fetch_data("minkowski3d_buckets");
    if not bool then
        // No -- we need to load the bucket database
        bool,buckets:=load_buckets();
        if not bool then return false,buckets; end if;
        // Update the cache
        save_data("minkowski3d_buckets",buckets);
    end if;
    // Return the buckets
    return true,buckets;
end function;

// Returns true followed by the data for the given bucket id on success, false
// followed by an error message otherwise.
function fetch_bucket(id)
    bool,buckets:=fetch_buckets();
    if not bool then return false,buckets; end if;
    return true,buckets[id];
end function;

// Returns true followed by the Laurent polynomials for the given bucket, false
// followed by an error message otherwise. The Laurent polynomials are given
// in Laurent normal form.
function fetch_bucket_laurents(id)
    // Fetch the bucket data
    bool,bucket:=fetch_bucket(id);
    if not bool then return false,bucket; end if;
    // Work through the buckets creating the Laurent polynomials
    fs:={};
    for I in bucket do
        P:=Polytope(NormalForm(PolytopeReflexiveFanoDim3(I[1])) :
                                                        areVertices:=true);
        Include(~fs,PolytopeToLaurent(P,I[2]));
    end for;
    // Return the Laurent polynomials
    return true,fs;
end function;

// Returns true followed by the set of possible lattice Minkowski ansatz
// coefficients for the reflexive 3-tope with given id. The coefficients are
// given in Laurent normal form, and are in bijection with
// Sort(SetToSequence(PALPNormalForm(P))).
function fetch_lattice_minkowski_coefficients(pid)
    // Fetch the buckets
    bool,buckets:=fetch_buckets();
    if not bool then return false,buckets; end if;
    // Extract all coefficients for the given polytope id
    return true,&join[{PowerSequence(Integers()) | I[2] :
                                          I in b | I[1] eq pid} : b in buckets];
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic LatticeMinkowskiBucketDim3( id::RngIntElt ) -> SetEnum[FldFunRatMElt]
{The lattice Minkowski Laurent polynomials in dimension 3 with given bucket ID. The bucket ID is an integer in the range 1..165.}
    // Sanity check
    require id ge 1 and id le 165: "The bucket ID must be in the range 1..165";
    // Fetch the Laurent polynomials
    bool,fs:=fetch_bucket_laurents(id);
    require bool: fs;
    return fs;
end intrinsic;

intrinsic LatticeMinkowskiBucketDim3( name::MonStgElt ) -> SetEnum[FldFunRatMElt]
{The lattice Minkowski Laurent polynomials in dimension 3 corresponding to the given Mori-Mukai name}
    // Recover the id
    bool,id:=minkowski_3d_name_to_id(name);
    require bool: Sprintf("Unknown name: \"%o\"",name);
    require id gt 0:
        Sprintf("No mirror Minkowski Laurent polynomial exists for %o.",name);
    // Fetch the Laurent polynomials
    bool,fs:=fetch_bucket_laurents(id);
    require bool: fs;
    return fs;
end intrinsic;

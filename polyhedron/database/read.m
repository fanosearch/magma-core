freeze;

/////////////////////////////////////////////////////////////////////////
// read.m
/////////////////////////////////////////////////////////////////////////
// Utility functions for reading from the databases of polytopes.
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Given a string representing a lattice point, returns the point (as a
// sequence of integers).
function str_to_point(L)
    // Remove the braces
    if #L ge 1 and L[1] eq "[" then
        L:=L[2..#L];
    end if;
    if #L ge 1 and L[#L] eq "]" then
        L:=L[1..#L - 1];
    end if;
    // Convert the string into a sequence of integers
    return [Integers() | StringToInteger(c) : c in Split(L,",")];
end function;

// Given a string representing a sequence of lattice points, returns the points
// (as a sequence of sequences of integers).
function str_to_points(L)
    // First we normalise the string
    L:=SubstituteString(L," ","");
    if #L ge 2 and L[1] eq "[" and L[2] eq "[" then
        L:=L[2..#L];
    end if;
    if #L ge 2 and L[#L] eq "]" and L[#L - 1] eq "]" then
        L:=L[1..#L - 1];
    end if;
    // Split the string into points
    pts:=[PowerSequence(Integers())|];
    idx:=Index(L,"]");
    while idx ne 0 do
        // Extract the point
        Append(~pts,str_to_point(L[1..idx]));
        // Move on
        if idx eq #L then
            return pts;
        else
            L:=L[idx + 2..#L];
            idx:=Index(L,"]");
        end if;
    end while;
    // We shouldn't be here
    assert false;
end function;

/////////////////////////////////////////////////////////////////////////
// Database access functions
/////////////////////////////////////////////////////////////////////////

// Returns the block and line number for the given ID and block size.
function ID_to_block(ID,blocksize)
    block:=(ID - 1) div blocksize;
    line:=(ID - 1) mod blocksize;
    return block + 1,line + 1;
end function;

// Returns true along with a file pointer to the requested file (relative to
// the database directory) upon success, false and an error string otherwise.
function open_db_file(file)
    try
        root:=FanosearchGetDbRoot();
    catch e
        return false,e`Object;
    end try;
    file:=Sprintf("%o/%o",root,file);
    try
        fh:=Open(file,"r");
    catch e
        // The real work is to create a sensible error message
        err:="";
        if assigned e`Object and Type(e`Object) eq MonStgElt then
            idx:=Index(e`Object,":");
            if idx gt 0 then
                err:=e`Object[idx + 1..#e`Object];
                err:=SubstituteString(err,"\\\n","");
                err:=SubstituteString(err,"\n"," ");
                err:=Trim(err);
                err:=SubstituteString(err,"  "," ");
                if #err ne 0 and err[#err] eq "." then Prune(~err); end if;
            end if;
        end if;
        if #err eq 0 then
            err:="Unable to open database file " cat file;
        end if;
        return false,err cat ".";
    end try;
    return true,fh;
end function;

// Returns true along with a file pointer to the requested block file upon
// success, false and an error string otherwise.
function open_database_block(dbname,block)
    file:=Sprintf("%o/%o.txt",dbname,block);
    bool,fh:=open_db_file(file);
    if not bool then
        err:=fh cat "\n\nThe database \"" cat dbname cat
             "\" should be installed in your Fanosearch database directory.";
        return false,err;
    end if;
    return true,fh;
end function;

// Returns true along with the line of the given block file upon success, false
// and an error string otherwise.
function read_line(dbname,block,line)
    // Open the requested block file
    bool,fh:=open_database_block(dbname,block);
    if not bool then return false,fh; end if;
    // Read the requested line
    while line gt 0 do
        L:=Gets(fh);
        if IsEof(L) then
            line := -1;
            break;
        end if;
        line -:= 1;
    end while;
    // Were we successful?
    if line ne 0 then
        return false,"The database \"" cat dbname cat "\" is corrupted";
    end if;
    // Return the line
    return true,L;
end function;

// Returns true along with the vertices (as a sequence of sequence of integers)
// of the given block file upon success, false and an error string otherwise.
function read_vertices(dbname,block,line)
    // Fetch the line
    bool,L:=read_line(dbname,block,line);
    if not bool then return false,L; end if;
    // For reasons connected with how data was created, some of the databases
    // prefix the vertices with the database id -- we strip that off
    idx:=Index(L,":");
    if idx ne 0 then L:=L[idx + 1..#L]; end if;
    // Convert the string to a sequence of points
    try
        pts:=str_to_points(L);
    catch e
        return false,"The database \"" cat dbname cat "\" is corrupted";
    end try;
    // Assert that the vertices are all of the same length
    if #pts ne 0 then
        n:=#pts[1];
        if not &and[#pt eq n : pt in pts] then
            return false,"The database \"" cat dbname cat "\" is corrupted";
        end if;
    end if;
    // Looks good
    return true,pts;
end function;

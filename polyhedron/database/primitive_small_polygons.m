freeze;

/////////////////////////////////////////////////////////////////////////
// primitive_small_polygons.m
/////////////////////////////////////////////////////////////////////////
// The database of IDs of small polygons whose edges are of unit length.
/////////////////////////////////////////////////////////////////////////

import "read.m": open_db_file;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Returns true along with the small polygons ID for the given primitive small polygon ID upon success, false and an error string otherwise.
function convert_ID(id)
    // Compute the file number k and line number N for this ID
    k:=((id - 1) div 5000) + 1;
    N:=((id - 1) mod 5000) + 1;
    // Make a note of the file name
    name:="primitive_small_polygons/" cat IntegerToString(k) cat ".txt";
    // Open the file
    ok,fh:=open_db_file(name);
    if not ok then return false, fh; end if;
    // Extract the required line
    L:=Gets(fh);
    while not IsEof(L) do
        if N eq 1 then
            return true,StringToInteger(L);
        end if;
        N -:= 1;
        L:=Gets(fh);
    end while;
    // We shouldn't be here
    return false,"The database file \"" cat name cat "\" is corrupted.";
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic PolytopePrimitiveSmallPolygon(id::RngIntElt) -> TorPol,RngIntElt,RngIntElt
{The polygon P, with given database ID in the range 1..95789, whose edges are all of unit length. Also gives the smallest value of k such that P fits inside a [0,k] x [0,k] box (after possible translation and change of basis), along with the corresponding ID in the small polytope database (see PolytopeSmallPolygon). The database contains all polygons for 1 <= k <= 7.}
    // Sanity check
    require id ge 1 and id le 95789: "The ID must be in the range 1..95789";
    // Convert the ID to a small polygons ID
    ok,id:=convert_ID(id);
    require ok: id;
    // Return the data
    P,k:=PolytopeSmallPolygon(id);
    return P, k, id;
end intrinsic;

freeze;

/////////////////////////////////////////////////////////////////////////
// dim4.m
/////////////////////////////////////////////////////////////////////////
// Database of four-dimensional reflexive polytopes.
/////////////////////////////////////////////////////////////////////////
// * M. Kreuzer and H. Skarke,
//   "Complete classification of reflexive polyhedra in four dimensions",
//   Adv. Theor. Math. Phys. 4(6) (2000) 1209-1230.
/////////////////////////////////////////////////////////////////////////

import "stdread.m": ID_to_packed_block, read_packed_vertices;
import "read.m": ID_to_block, read_vertices;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Returns the vertices for the reflexive 4-fold with given ID.
function vertices_of_reflexive_fourfold(ID)
    // If the ID is within range, try the mini database first
    if ID le 500000 then
        block,line:=ID_to_packed_block(ID,5000);
        bool,verts:=read_packed_vertices("ks4mini",block,line);
        if bool then
            return true,verts;
        end if;
    end if;
    // No luck -- try the complete database
    block,line:=ID_to_block(ID,10000);
    bool,verts:=read_vertices("ks4",block,line);
    return bool,verts;
end function;

// Apply function of two arguments cumulatively to the items of sequence, from
// left to right, so as to reduce the sequence to a single value.
function reduce(f,S)
    p:=S[1];
    for i in [2..#S] do
        p:=f(p,S[i]);
    end for;
    return p;
end function;

// Returns \prod x[i]^Abs(y[i]) mod 16777213.
function mult_pow_red(x,y)
    p:=1;
    for i in [1..#x] do
        q:=(x[i]^Abs(y[i])) mod 16777213;
        p:=(p * q) mod 16777213;
    end for;
    return p;
end function;

// Returns the primary and secondary hash values for the given 4-dim reflexive
// polytope. The hash values are integers in the range 0..16777212. (Note that
// 16777213 is the largest prime less than 16777216 = 2^24.)
function hash_values(P)
    // Sanity check
    assert Dimension(P) eq 4 and IsReflexive(P);
    // Calculate the normal form
    nf:=[PowerSequence(Integers()) | Eltseq(v) : v in PALPNormalForm(P)];
    // Calculate the primary hash value
    f:=func<x,y | ((1 + x) * y) mod 16777213>;
    primary:=reduce(f,[Integers() | mult_pow_red([11,13,17,19],v) : v in nf]);
    // Calculate the secondary hash value
    f:=func<x,y | ((2 + x) * y) mod 16777213>;
    offset:=[Integers() | Abs(Min([v[i] : v in nf])) : i in [1..4]];
    secondary:=reduce(f,[Integers() |
       mult_pow_red([23,29,31,37],[v[i] + offset[i] : i in [1..4]]) : v in nf]);
    // Return the hash values
    return primary,secondary;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic PolytopeReflexiveFanoDim4( ID::RngIntElt ) -> TorPol
{The reflexive Fano 4-dimensional polytope with given database ID in the range 1..473800776}
    // Is the ID within the database range?
    require ID ge 1 and ID le 473800776:
        "The ID must be in the range 1..473800776";
    // Create the list of vertices
    bool,vertices:=vertices_of_reflexive_fourfold(ID);
    require bool: vertices;
    // Return the polytope
    return Polytope(vertices : areVertices:=true);
end intrinsic;

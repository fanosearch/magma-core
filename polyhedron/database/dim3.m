freeze;

/////////////////////////////////////////////////////////////////////////
// dim3.m
/////////////////////////////////////////////////////////////////////////
// Database of three-dimensional polytopes.
/////////////////////////////////////////////////////////////////////////

import "read.m": ID_to_block, read_vertices;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Returns the vertices for the reflexive facet with given ID.
function vertices_of_reflexive_facet(ID)
    block,line:=ID_to_block(ID,10000);
    bool,verts:=read_vertices("dim4_facets",block,line);
    return bool,verts;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic PolytopeReflexiveFanoDim4Facet( ID::RngIntElt ) -> TorPol
{The (3-dimensional) facets of the reflexive Fano 4-dimensional polytopes, up to affine equivalence. The facets are indexed 1..18888927.}
    // Is the ID within the database range?
    require ID ge 1 and ID le 18888927:
        "The ID must be in the range 1..18888927";
    // Create the list of vertices
    bool,vertices:=vertices_of_reflexive_facet(ID);
    require bool: vertices;
    // Create and return the facet
    return Polytope(vertices : areVertices:=true);
end intrinsic;

// Note: Don't remove this intrinsic (required by external code).
intrinsic InternalPolytopeReflexiveFanoDim4Facet( ID::RngIntElt )
    -> SeqEnum[SeqEnum[RngIntElt]]
{The vertices of the (3-dimensional) facets of the reflexive Fano 4-dimensional polytopes, up to affine equivalence. The facets are indexed 1..18888927. By construction, the vertices are given in affine normal form.}
    // Is the ID within the database range?
    require ID ge 1 and ID le 18888927:
        "The ID must be in the range 1..18888927";
    // Create the list of vertices
    bool,vertices:=vertices_of_reflexive_facet(ID);
    require bool: vertices;
    return vertices;
end intrinsic;

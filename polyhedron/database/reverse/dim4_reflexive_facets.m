freeze;

/////////////////////////////////////////////////////////////////////////
// dim4_reflexive_facets.m
/////////////////////////////////////////////////////////////////////////
// Reverse look-up for the database of facets of reflexive 4-topes.
/////////////////////////////////////////////////////////////////////////

import "../read.m": open_database_block;
import "../dim3.m": vertices_of_reflexive_facet;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Returns the hash values for the given sequence of vertices. These are two
// integers in the range 0..16777212. (Note that 16777213 is the largest prime
// less than 16777216 = 2^24.)
function hash_vertices(verts)
    // Sanity check
    assert #verts ne 0 and #verts[1] eq 3;
    // Calculate the first hash value
    hash1:=0;
    for v in verts do
        c1:=11^Abs(v[1]) mod 16777213;
        c2:=13^Abs(v[2]) mod 16777213;
        c3:=17^Abs(v[3]) mod 16777213;
        c:=(c1 * c2) mod 16777213;
        c:=(c * c3) mod 16777213;
        hash1:=((1 + hash1) * c) mod 16777213;
    end for;
    // Calculate the second hash value
    offset:=[Integers() | Abs(Min([v[i] : v in verts])) : i in [1..3]];
    hash2:=0;
    for v in verts do
        c1:=23^(v[1] + offset[1]) mod 16777213;
        c2:=29^(v[2] + offset[2]) mod 16777213;
        c3:=31^(v[3] + offset[3]) mod 16777213;
        c:=(c1 * c2) mod 16777213;
        c:=(c * c3) mod 16777213;
        hash2:=((2 + hash2) * c) mod 16777213;
    end for;
    // Return the hash values
    return hash1,hash2;
end function;

// Extracts the ids from the given string and returns true followed by the ids.
// Otherwise returns false.
function extract_ids(S)
    // Skip over the hashes
    idx:=Index(S,":");
    if idx eq 0 then return false,_; end if;
    idx:=Index(S,":",idx + 1);
    if idx eq 0 then return false,_; end if;
    // Extract the id string and convert it to a sequence of integers
    S:=S[idx + 1..#S];
    try
        ids:=[Integers() | StringToInteger(c) : c in Split(S,",")];
    catch e
        return false,_;
    end try;
    return true,ids;
end function;

// Reads in the next 1000 lines from the given file and returns them as a
// sequence of strings.
function fetch_data(fh)
    data:=[];
    done:=false;
    S:=Gets(fh);
    while not IsEof(S) and not done do
        Append(~data,S);
        if #data eq 1000 then
            done:=true;
        else
            S:=Gets(fh);
        end if;
    end while;
    return data;
end function;

// Compares this line of hash data (as a string) against the given hashes.
// On success returns true followed by an integer c, where c = -1 if this
// line < hashes, c = 0 if they are equal, and c = +1 if this line > hashes.
// Otherwise returns false.
function cmp_hashes(S,hash1,hash2)
    // Extract the first hash value...
    idx1:=Index(S,":");
    if idx1 eq 0 then return false,_; end if;
    h1:=StringToInteger(S[1..idx1 - 1]);
    // ...and check it against hash1
    if h1 lt hash1 then return true,-1; end if;
    if h1 gt hash1 then return true,1; end if;
    // Extract the second hash value...
    idx2:=Index(S,":",idx1 + 1);
    if idx2 eq 0 then return false,_; end if;
    h2:=StringToInteger(S[idx1 + 1..idx2 - 1]);
    // ...and check it against hash2
    if h2 lt hash2 then return true,-1; end if;
    if h2 gt hash2 then return true,1; end if;
    // If we're here then the hashes are equal
    return true,0;
end function;

// Performs a binary search in the given sequence of data for the index of the
// hash values. Returns true followed by the index (or 0 if the hashes are
// not in the data). Otherwise returns false.
function binary_search(data,hash1,hash2)
    // Initialize the values
    start:=1;
    finish:=#data;
    // Get looping
    while start lt finish do
        // Calculate the mid point
        mid:=Floor((start + finish) / 2);
        // Adjust the range
        bool,cmp:=cmp_hashes(data[mid],hash1,hash2);
        if not bool then return false,_; end if;
        if cmp lt 0 then
            start:=mid + 1;
        elif cmp gt 0 then
            finish:=mid - 1;
        else
            return true,mid;
        end if;
    end while;
    // Did we find a match?
    if start eq finish then
        bool,cmp:=cmp_hashes(data[start],hash1,hash2);
        if not bool then return false,_; end if;
        if cmp eq 0 then return true,start; end if;
    end if;
    return true,0;
end function;

// Searches the given file for the hash values. Returns true followed by the
// sequence of ids on success, false followed by an error message otherwise.
function search_for_hash_values(fh,hash1,hash2)
    // Start hunting through the file for the hash values
    data:=fetch_data(fh);
    while #data ne 0 do
        // Are the hash values in this block of data?
        bool,cmp:=cmp_hashes(data[#data],hash1,hash2);
        if not bool then
            return false,"The database \"dim4_facet_hashes\" is corrupted";
        end if;
        if cmp ge 0 then
            // Find the index of the hashes in the data
            if cmp eq 0 then
                idx:=#data;
            else
                bool,idx:=binary_search(data,hash1,hash2);
                if not bool then
                    return false,
                        "The database \"dim4_facet_hashes\" is corrupted";
                end if;
            end if;
            if idx eq 0 then
                return false,
                    "Argument must be the facet of a reflexive Fano 4-tope";
            end if;
            // Extract and return the ids
            bool,ids:=extract_ids(data[idx]);
            if not bool then
                return false,"The database \"dim4_facet_hashes\" is corrupted";
            end if;
            return true,ids;
        end if;
        // Move on
        data:=fetch_data(fh);
    end while;
    // If we're here then the hashes are invalid
    return false,"Argument must be the facet of a reflexive Fano 4-tope";
end function;

// Returns true followed by the id of the facet P (where P is assumed to be the
// facet of a reflexive Fano 4-tope). Otherwise returns false followed by an
// error message.
function reflexive_dim4_facet_to_id(P)
    // Sanity check
    if Dimension(P) ne 3 or not IsIntegral(P) then
        return false,"Argument must be the facet of a reflexive Fano 4-tope";
    end if;
    // Place the facet in affine normal form
    verts:=[PowerSequence(Integers()) | Eltseq(v) :
                              v in AffineNormalForm(PolyhedronInSublattice(P))];
    // Calculate the hash values and block file
    hash1,hash2:=hash_vertices(verts);
    block:=(hash1 mod 5000) + 1;
    // Open the block file
    bool,fh:=open_database_block("dim4_facet_hashes",block);
    if not bool then return false,fh; end if;
    // Search through the file for the ids we want
    bool,ids:=search_for_hash_values(fh,hash1,hash2);
    if not bool then return false,ids; end if;
    // Validate and return the corresponding id
    if #ids eq 1 then
        bool,dbverts:=vertices_of_reflexive_facet(ids[1]);
        if not bool then return false,dbverts; end if;
        if dbverts eq verts then return true,ids[1]; end if;
    else
        for id in ids do
            bool,dbverts:=vertices_of_reflexive_facet(id);
            if not bool then return false,dbverts; end if;
            if dbverts eq verts then return true,id; end if;
        end for;
    end if;
    // If we're here then the facet isn't valid
    return false,"Argument must be the facet of a reflexive Fano 4-tope";
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic PolytopeReflexiveFanoDim4FacetID( F::TorPol ) -> RngIntElt
{Given a facet F of a reflexive four-dimensional polytope, returns the index of the facet. The facets are indexed (up to equivalence) 1..18888927.}
    // Sanity check
    require IsPolytope(F) and Dimension(F) eq 3 and IsIntegral(F):
        "Argument must be the facet of a reflexive Fano 4-tope";
    // Recover the ID for this facet
    bool,id:=reflexive_dim4_facet_to_id(F);
    require bool: id;
    return id;
end intrinsic;

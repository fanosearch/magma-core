freeze;

////////////////////////////////////////////////////////////////////////////////
// this defines an intrinsic MightSmooth
// which takes a Fano polytope P and returns false if Altmann's work on
// deforming isolated Gorenstein singularities implies that the toric variety
// defined by the spanning fan of P will not smooth
// and returns true otherwise
////////////////////////////////////////////////////////////////////////////////

intrinsic MightSmooth(P::TorPol) -> BoolElt
{
Given a Fano polytope P, return false if any facet F of P is such that F is Minkowski-irreducible and the toric singularity defined by the cone over F is isolated and non-smooth.  In this case, we know by work of Klaus Altmann that the toric variety defined by the spanning fan of P will not smooth.  Otherwise, return true.
}
    require IsFano(P): "P must be a Fano polytope";
    facet_idxs := FacetIndices(P);
    vertices := Vertices(P);
    dim := Dimension(P);
    probably_decomposables := [];
    ////////////////////////////////////////////////////////////////////////////////
    // on the first pass, check the simplices and a
    ////////////////////////////////////////////////////////////////////////////////
    for fidx in facet_idxs do
	if #fidx ne dim then
	    // we skip non-simplices
	    continue;
	end if;
	F := Polytope([vertices[i] : i in fidx] : areVertices:=true);
	C := Cone(F);
	if IsSmooth(C) or not IsGorenstein(C) or not IsIsolated(C) then
	    // Altmann has nothing to say
	    continue;
	end if;
	if InternalIsMinkowskiIndecomposable(F) then
	    return false;
	else
	    Append(~probably_decomposables,F);
	end if;
    end for;
    ////////////////////////////////////////////////////////////////////////////////
    // on the second pass, check the non-simplices
    ////////////////////////////////////////////////////////////////////////////////
    for facet_num in [1..#facet_idxs] do
	if #facet_idxs[facet_num] eq dim then
	    // we skip simplices
	    continue;
	end if;
	F := Facets(P)[facet_num];
	C := Cone(F);
	if not IsGorenstein(C) or not IsIsolated(C) then
	    // Altmann has nothing to say
	    continue;
	end if;
	if InternalIsMinkowskiIndecomposable(F) then
	    return false;
	else
	    Append(~probably_decomposables,F);
	end if;
    end for;
    // now check that all the probably-decomposable facets are, in fact, decomposable
    // if so then return true, otherwise return false
    for F in probably_decomposables do
	non_trivial_decomps := [ xx : xx in MinkowskiDecomposition(F) | #[Q : Q in xx | Dimension(Q) gt 0] gt 1 ];
	if #non_trivial_decomps eq 0 then
	    return false;
	end if;
    end for;
    return true;
end intrinsic;


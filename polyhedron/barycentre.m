freeze;

/////////////////////////////////////////////////////////////////////////
// barycentre.m
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic Barycentre(P::TorPol) -> TorLatElt
{The barycentre of the polytope P}
	// Sanity check
	require IsPolytope(P): "The polyhedron must be a polytope";
	require IsMaximumDimensional(P):
		"The polytope must be of maximum dimension in the ambient lattice";
	// Calculate the barycentre
	d:=Dimension(P) + 1;
	Ts:=Triangulation(P);
	barys:=[&+Vertices(T) / d : T in Ts];
	vols:=[Volume(T) : T in Ts];
	return &+[Ambient(P) | vols[i] * barys[i] : i in [1..#Ts]] / &+vols;
end intrinsic;

intrinsic IsZeroBarycentre(P::TorPol) -> BoolElt
{True iff the polytope P has barycentre zero}
	require IsPolytope(P): "The polyhedron must be a polytope";
	require IsMaximumDimensional(P):
		"The polytope must be of maximum dimension in the ambient lattice";
	return IsZero(Barycentre(P));
end intrinsic;

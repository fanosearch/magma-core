freeze;

/////////////////////////////////////////////////////////////////////////
// smallest.m
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Returns the lattice width of P with respect to the form u
function compute_width(P,u)
    heights:=[Rationals() | v * u : v in Vertices(P)];
    return Maximum(heights) - Minimum(heights);
end function;

// Given a polytope P and a basis element e such that width_e(P) >= maxw,
// returns the set of all primitive dual lattice points u such that
// width_u(P) <= maxw. Only one of u and -u will be included.
function forms_of_width(P,e,maxw)
    // Sanity check
    D:=Dual(Ambient(P));
    error if not e in Basis(D), "The form must be a dual basis element";
    // If the polytope doesn't contain the origin then we translate
    if not ContainsZero(P) then
        if assigned P`interior_points and #P`interior_points ne 0 then
            bary:=Representative(P`interior_points);
        else
            bary:=&+Vertices(P) / NumberOfVertices(P);
        end if;
        P:=P - bary;
    end if;
    // Sanity check on the form
    c:=compute_width(P,e);
    error if c lt maxw,
        Sprintf("The form must give width greater than %o",maxw);
    // Compute the norm of the largest sphere contained in P
    r:=Minimum([Rationals() | I[2]^2 / Norm(I[1]) : I in Inequalities(P)]);
    // Compute all forms of width at most maxw
    S:=[D|];
    LL:=Lattice(IdentityMatrix(Integers(),Dimension(Ambient(P))));
    PP:=ShortVectorsProcess(LL,c^2 / (4 * r));
    while not IsEmpty(PP) do
        u:=Eltseq(NextVector(PP));
        if GCD(u) eq 1 then
            u:=D ! u;
            c:=compute_width(P,u);
            if c le maxw then
                Append(~S,u);
            end if;
        end if;
    end while;
    // Return the result
    return SequenceToSet(S);
end function;

// Returns true iff the given collection of lattice points contains a basis for
// the lattice. If true, also returns an possible basis.
function contains_basis(pts)
    // Sanity check
    error if IsNull(pts), "Illegal null sequence";
    // Get the zero dimensional case out of the way
    L:=Universe(pts);
    d:=Dimension(L);
    if #pts eq 0 then
        if d eq 0 then
            return true,[L|];
        else
            return false,_;
        end if;
    end if;
    // Begin by throwing away all unwanted points
    newpts:={L|};
    for v in pts do
        if not IsZero(v) and IsIntegral(v) and not -v in newpts then
            Include(~newpts,v);
        end if;
    end for;
    pts:=SetToSequence(newpts);
    // Is there anything to do?
    if #pts lt d then return false,_; end if;
    // Start hunting for a basis
    for idxs in Subsets({1..#pts},d) do
        subpts:=[L | pts[i] : i in idxs];
        if AreGenerators(subpts) then return true,subpts; end if;
    end for;
    // If we're here then no luck
    return false,_;
end function;

// Returns the index of the smallest entry in S at least k.
function index_of_smallest(S,k)
    min:=false;
    idx:=0;
    for i in [1..#S] do
        if S[i] ge k and (min cmpeq false or min gt S[i]) then
            min:=S[i];
            idx:=i;
        end if;
    end for;
    return idx;
end function;

// Given a lattice polytope P, returns the smallest value of k such that, after
// possible change of basis and translation, P fits into a [0,k] x [0,k] box.
// Also returns the change of basis of the dual lattice.
function smallest_box_for_polytope(P)
    // Begin by computing the minimum width
    minw,pts:=Width(P);
    assert IsIntegral(minw);
    minw:=Integers() ! minw;
    bool,B:=contains_basis(pts);
    if bool then return minw,B; end if;
    // OK, no luck -- we'll compute the largest box guaranteed to work
    _,min,max:=BoundingBox(P);
    min:=Eltseq(min);
    max:=Eltseq(max);
    widths:=[Rationals() | max[i] - min[i] : i in [1..#min]];
    maxw:=Maximum(widths);
    // Start the computation
    L:=Dual(Ambient(P));
    for w in [minw + 1..maxw] do
        i:=index_of_smallest(widths,w);
        pts:=forms_of_width(P,L.i,w);
        bool,B:=contains_basis(pts);
        if bool then return w,B; end if;
    end for;
    // We should never get here
    error "No box was found, which is impossible!";
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic SmallestRepresentative( P::TorPol ) -> TorPol,RngIntElt,Map,TorLatElt
{Given a d-dimensional lattice polytope P, returns an equivalent polytope Q which fits into a [0,k]^d box, such that P and Q are equivalent, and k is as small as possible. Note that there might be many possible choices of Q, but clearly k is well-defined. Also returns the value of k, along with a lattice map phi and translation u such that phi(P) + u = Q.}
    // Sanity check
    require IsPolytope(P): "The polyhedron must be a polytope";
    require IsMaximumDimensional(P):
        "The polytope must be of maximum dimension in the ambient lattice";
    require IsIntegral(P): "The polytope must be a lattice polytope";
    // Compute the value of k and a change of basis (of the dual lattice)
    k,B:=smallest_box_for_polytope(P);
    // Perform the transformation on P
    phi:=Dual(LatticeMap(Universe(B),B));
    Q:=Image(phi,P);
    // Compute the translation
    _,u:=BoundingBox(Q);
    // Return the result
    return Q - u,k,phi,-u;
end intrinsic;

freeze;

/////////////////////////////////////////////////////////////////////////
// polytope.m
/////////////////////////////////////////////////////////////////////////
// Useful intrinsics for working with polytopes.
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic BoundingBox(f::FldFunRatMElt) -> TorPol, TorLatElt, TorLatElt
{The smallest polytope Q that is a hyper-box and contains the Newton polytope of f. Also gives the bottom-left and top-right corners of the box.}
    require not IsZero(f): "Argument must be non-zero";
    require Rank(Parent(f)) ne 0: "Argument must lie in a non-trivial polynomial ring";
    require IsLaurent(f): "Argument must be a Laurent polynomial";
    _,es:=CoefficientsAndExponents(f);
    S:=RowSequence(Transpose(Matrix(es)));
    topright:=[Max(I) : I in S];
    bottomleft:=[Min(I) : I in S];
    return BoundingBox(Polytope([topright,bottomleft]));
end intrinsic;

intrinsic DualFace(P::TorPol, F::TorPol) -> TorPol
{The face in the polytope dual to P which is dual to the face F of P. Here P must be of maximum dimension containing the origin in its strict interior.}
    // Sanity check
    require IsPolytope(P): "Argument 1 must be a polytope";
    require IsMaximumDimensional(P): "Argument 1 must be of maximum dimension in its ambient lattice";
    require ContainsZero(P): "Argument 1 must contain the origin strictly in its interior";
    require IsFace(P,F): "Argument 2 must be a face of argument 1";
    // Compute the vertex indices and return the polytope
    Qverts:=Vertices(Dual(P));
    verts:=[Universe(Qverts) | Qverts[i] : i in DualFaceIndices(P,F)];
    return Polytope(verts : areVertices:=true);
end intrinsic;

intrinsic DualFaceIndices(P::TorPol, F::TorPol) -> TorPol
{A set describing the indices of the vertices of the polytope dual to P which evaluate to -1 on F. Here P must be of maximum dimension containing the origin in its strict interior.}
    // Sanity check
    require IsPolytope(P): "Argument 1 must be a polytope";
    require IsMaximumDimensional(P): "Argument 1 must be of maximum dimension in its ambient lattice";
    require ContainsZero(P): "Argument 1 must contain the origin strictly in its interior";
    require IsFace(P,F): "Argument 2 must be a face of argument 1";
    // Compute the vertices of F
    Fverts:=Vertices(F);
    // Calculate the indices of the vertices of the dual face to F
    Qverts:=Vertices(Dual(P));
    return {Integers() | i : i in [1..#Qverts] |
                                        &and[u*Qverts[i] eq -1 : u in Fverts]};
end intrinsic;

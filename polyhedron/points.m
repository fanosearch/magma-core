freeze;

/////////////////////////////////////////////////////////////////////////
// points.m
/////////////////////////////////////////////////////////////////////////

// Returns the number of points contained in the edges of a lattice polytope.
function num_edge_points_int(P)
	verts:=Vertices(P);
	num:=#verts;
	for E in EdgeIndices(P) do
		idx1,idx2:=Explode(SetToSequence(E));
		num +:= GCD(Eltseq(verts[idx1] - verts[idx2])) - 1;
	end for;
	return num;
end function;

// Searches for the first lattice point on the edge from v1 to v2. Returns true
// followed by the point on success, false if no such point exists. Assumes
// that neither v1 nor v2 are lattice.
function first_point(v1,v2)
	d:=LCM([Denominator(v) : v in Eltseq(v1)] cat
		   [Denominator(v) : v in Eltseq(v2)]);
    pt:=d * v1;
	u:=d * v2 - pt;
	v:=PrimitiveLatticeVector(u);
	for i in [1..Floor(u / v)] do
		pt +:= v;
		if &and[IsDivisibleBy(c,d) : c in Eltseq(pt)] then
			return true,pt / d;
		end if;
	end for;
	return false,_;
end function;

// Returns the number of points contained in the edges of a rational polytope.
function num_edge_points_rat(P)
	verts:=Vertices(P);
	num:=#[v : v in verts | IsIntegral(v)];
	for E in EdgeIndices(P) do
		idx1,idx2:=Explode(SetToSequence(E));
		b1:=IsIntegral(verts[idx1]);
		b2:=IsIntegral(verts[idx2]);
		if b1 and b2 then
			num +:= GCD(Eltseq(verts[idx1] - verts[idx2])) - 1;
		elif b1 or b2 then
			u:=verts[idx1] - verts[idx2];
			num +:= Floor(u / PrimitiveLatticeVector(u));
		else
			bool,v1:=first_point(verts[idx1],verts[idx2]);
			if bool then
				u:=verts[idx2] - v1;
				num +:= Floor(u / PrimitiveLatticeVector(u)) + 1;
			end if;
		end if;
	end for;
	return num;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic NumberOfEdgePoints( P::TorPol ) -> RngIntElt
{The number of lattice points lying on the edges of the polytope P}
    // Sanity check
    require IsPolytope(P): "Polyhedron must be a polytope";
	// Get the degenerate cases out of the way
	d:=Dimension(P);
	if d lt 1 then
		return 0;
	elif d eq 1 then
		return NumberOfPoints(P);
	elif d eq 2 then
		return NumberOfBoundaryPoints(P);
	end if;
	// How we proceed depends on whether P is integral
	return IsIntegral(P) select num_edge_points_int(P)
						   else num_edge_points_rat(P);
end intrinsic;

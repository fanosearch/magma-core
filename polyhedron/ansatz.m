freeze;

/////////////////////////////////////////////////////////////////////////
// ansatz.m
/////////////////////////////////////////////////////////////////////////
// Intrinsics for assigning coefficients to the points of a polytope.
/////////////////////////////////////////////////////////////////////////

import "database/minkowski_dim3_buckets.m": fetch_lattice_minkowski_coefficients;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Returns true iff the given lattice Minkowski decomposition is admissible.
function is_admissible(decomp)
    // Check each component is of the correct type
    for P in decomp do
        dim:=Dimension(P);
        assert dim le 2;
        if Dimension(P) eq 2 then
            if NumberOfVertices(P) eq 3 and
               NumberOfInteriorPoints(P) eq 0 then
                edges:=Sort([NumberOfPoints(E) - 2 : E in Edges(P)]);
                if edges[1] ne 0 or edges[2] ne 0 then
                    return false;
                end if;
            else
                return false;
            end if;
        end if;
    end for;
    // If we're here then it looks good
    return true;
end function;

// Returns the ansatz coefficients for a line segment, along with the sequence
// of points (in bijective correspondence).
function dim_1_ansatz_coeffs(L)
    // Sanity check
    assert Dimension(L) eq 1 and IsIntegral(L);
    // Return the result
    pts:=Sort(Points(L));
    return [Integers() | Binomial(#pts - 1,i) : i in [0..#pts - 1]],pts;
end function;

// Returns the ansatz coefficients for an A_n triangle, along with the sequence
// of points (in bijective correspondence).
function dim_2_ansatz_coeffs(T)
    // Sanity check
    assert Dimension(T) eq 2 and NumberOfVertices(T) eq 3 and
           NumberOfInteriorPoints(T) eq 0;
    // Two edges should be free of interior points -- check this
    edges:=[Integers() | NumberOfPoints(E) - 2 : E in Edges(T)];
    idx1:=Index(edges,0);
    assert idx1 ne 0;
    idx2:=Index(edges,0,idx1 + 1);
    assert idx2 ne 0;
    // The remaining edge might have interior points
    idx3:=Representative({1..3} diff {idx1,idx2});
    E:=Edges(T)[idx3];
    // Compute the coefficients for this edge
    pts:=Sort(Points(E));
    coeffs:=[Integers() | Binomial(#pts - 1,i) : i in [0..#pts - 1]];
    // We simply need to add on an extra point for the remaining vertex
    pt:=Representative(SequenceToSet(Vertices(T)) diff
                       SequenceToSet(Vertices(E)));
    Append(~coeffs,1);
    Append(~pts,pt);
    return coeffs,pts;
end function;

// Returns the ansatz coefficients for the given admissible decomposition.
// The coefficients are in bijective correspndence with 'sortpts'.
function decomp_ansatz_coeffs(sortpts,decomp)
    sumcoeffs:=[Integers()|];
    sumpts:=[Ambient(Representative(decomp))|];
    for P in decomp do
        // Fetch the coefficients and points for this component
        assert Dimension(P) le 2;
        if Dimension(P) eq 2 then
            coeffs,pts:=dim_2_ansatz_coeffs(P);
        elif Dimension(P) eq 1 then
            coeffs,pts:=dim_1_ansatz_coeffs(P);
        else
            coeffs:=[Integers() | 1];
            pts:=Vertices(P);
        end if;
        // Update the sum
        if #sumcoeffs eq 0 then
            newcoeffs:=coeffs;
            newpts:=pts;
        else
            newcoeffs:=[Integers()|];
            newpts:=[Ambient(P)|];
            for i in [1..#coeffs] do
                coeff1:=coeffs[i];
                pt1:=pts[i];
                for j in [1..#sumcoeffs] do
                    pt:=pt1 + sumpts[j];
                    coeff:=coeff1 * sumcoeffs[j];
                    idx:=Index(newpts,pt);
                    if idx eq 0 then
                        Append(~newpts,pt);
                        Append(~newcoeffs,coeff);
                    else
                        newcoeffs[idx] +:= coeff;
                    end if;
                end for;
            end for;
        end if;
        // Move on
        sumcoeffs:=newcoeffs;
        sumpts:=newpts;
    end for;
    // Sort the points and return the coefficients
    coeffs:=ZeroSequence(Integers(),#sortpts);
    for i in [1..#sumcoeffs] do
        idx:=Index(sortpts,sumpts[i]);
        assert idx ne 0;
        coeffs[idx]:=sumcoeffs[i];
    end for;
    return coeffs;
end function;

// The set of possible coefficients that can be assigned to the polygon P using
// the lattice Minkowski ansatz. The coefficients are placed in bijection with
// 'sortpts'.
function dim_2_lattice_minkowski_ansatz(P,sortpts)
    // Move to the two-dimensional embedding
    PP,emb,v:=PolyhedronInSublattice(P);
    // Compute the possible lattice Minkowski decompositions
    pts:=[(pt - v) @@ emb : pt in sortpts];
    return {PowerSequence(Integers()) | decomp_ansatz_coeffs(pts,decomp) :
                                   decomp in LatticeMinkowskiDecomposition(PP) |
                                   is_admissible(decomp)};
end function;

// Returns true followed by the set of possible coefficients that can be
// assigned to the reflexive 3-tope P using the lattice Minkowski ansatz. The
// coefficients are placed in bijection with 'sortpts'. Returns false followed
// by an error message if the database can't be accessed.
function dim_3_reflexive_lattice_minkowski_ansatz(P,sortpts)
    // Recover the database ID
    pid:=[Integers() | I[2] : I in DatabaseID(P) | I[1] eq "reflexive3"];
    if #pid ne 1 then
        return false,"Argument 1 must be a reflexive 3-tope";
    end if;
    pid:=pid[1];
    // Fetch the lattice Minkowski coefficients from the database
    bool,cs:=fetch_lattice_minkowski_coefficients(pid);
    if not bool then return false,cs; end if;
    // We need to recover the isomorphism mapping P to the normal form
    verts,perm:=NormalForm(P);
    M:=Matrix(PermuteSequence(Vertices(P),perm));
    phi:=Transpose(Solution(Transpose(M),Transpose(Matrix(verts))));
    // Map the points of sortpts across and recover the permutation
    mappedpts:=RowSequence(Matrix(sortpts) * phi);
    Sort(~mappedpts,~perm);
    perm:=perm^-1;
    // Reorder the coefficients
    cs:={PowerSequence(Integers()) | PermuteSequence(c,perm) : c in cs};
    // Finally, we need to apply the automorphism group of P to all
    // coefficients. In practice -- since this is a pre-computed database --
    // we know a priori what cases we need to worry about
    newcs:={Universe(cs)|};
    if pid in {Integers() | 1903,2329,2697,2760,2817,3008,3189,3447,3505,3591,
               3705,3791,3844,3845,3846,3868,3874,3875,3927,4058,4075,4076,
               4169,4182,4183} then
        sortpts:=RowSequence(Matrix(sortpts));
        Sort(~sortpts,~perm1);
        sortpts:=Matrix(sortpts);
        for g in AutomorphismGroup(P) do
            newpts:=RowSequence(sortpts * g);
            Sort(~newpts,~perm2);
            perm:=perm1^-1 * perm2 * perm1;
            newcs join:= {PermuteSequence(c,perm) : c in cs};
        end for;
    end if;
    // Return what we have
    return true,cs join newcs;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic LatticeMinkowskiAnsatz( P::TorPol : sortpts:=false )
    -> SetEnum[SeqEnum[RngIntElt]]
{The set of possible coefficients that can be assigned to the polytope P using the lattice Minkowski ansatz. The coefficients are placed in bijection with 'sortpts', which defaults to Sort(SetToSequence(Points(P))).}
    // Sanity check
    require IsPolytope(P) and IsIntegral(P):
        "The polyhedron must be a lattice polytope";
    require Dimension(P) eq 2 or (Dimension(P) eq 3 and IsReflexive(P)):
        "The polyhedron must be a two-dimensional lattice polytope, or a 3-dimensional reflexive polytope";
    if sortpts cmpeq false then
        sortpts:=Sort(Points(P));
    else
        require Type(sortpts) eq SeqEnum and Universe(sortpts) eq Ambient(P):
            "Parameter 'sortpts' should be a sequence of all distinct lattice points in the polytope";
        tmp:=SequenceToSet(sortpts);
        require #tmp eq #sortpts and tmp eq Points(P):
            "Parameter 'sortpts' should be a sequence of all distinct lattice points in the polytope";
    end if;
    // How we proceed depends on the dimension
    if Dimension(P) eq 2 then
        return dim_2_lattice_minkowski_ansatz(P,sortpts);
    elif Dimension(P) eq 3 and IsReflexive(P) then
        bool,cs:=dim_3_reflexive_lattice_minkowski_ansatz(P,sortpts);
        require bool: cs;
        return cs;
    end if;
    // We should never get here
    assert false;
end intrinsic;

intrinsic BinomialEdgeCoefficients( P::TorPol : sortpts:=false )
    -> SeqEnum[RngIntElt]
{Assigns binomial coefficients to the edges of the given polytope P. The coefficients are placed in bijection with 'sortpts', which defaults to Sort(SetToSequence(Points(P))).}
    // Sanity check
    require IsPolytope(P) and IsIntegral(P):
        "The polyhedron must be an integral polytope";
    if sortpts cmpeq false then
        sortpts:=Sort(Points(P));
    else
        require Type(sortpts) eq SeqEnum and Universe(sortpts) eq Ambient(P):
            "Parameter 'sortpts' should be a sequence of distinct points in the polytope containing all edge points";
        tmp:=SequenceToSet(sortpts);
        require #tmp eq #sortpts and tmp subset Points(P) and
          &join[Points(E) : E in Edges(P)] subset tmp:
            "Parameter 'sortpts' should be a sequence of distinct points in the polytope containing all edge points";
    end if;
    // Assign the binomial coefficients to the edges
    coeffs:=ZeroSequence(Integers(),#sortpts);
    for E in Edges(P) do
        pts:=Sort(Points(E));
        for i in [1..#pts] do
            coeffs[Index(sortpts,pts[i])]:=Binomial(#pts - 1,i - 1);
        end for;
    end for;
    // Return the coefficients
    return coeffs;
end intrinsic;

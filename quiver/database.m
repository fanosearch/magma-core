freeze;

/////////////////////////////////////////////////////////////////////////
// database.m
/////////////////////////////////////////////////////////////////////////
// Intrinsics for accessing quivers that define Fano quiver flag
// varieties in the sense of A. Craw, "Quiver flag varieties and
// multigraded linear series", 	Duke Mathematical Journal 156 (2011)
// pp. 469--500
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

function sequence_to_quiver(S)
    dim := S[1];  // the dimension vector
    A := Transpose(Matrix(Integers(), S[2..#S]));   // the adjacency matrix
    return QuiverFlagVariety(A, dim);
end function;

//////////////////////////////////////////////////////////////////////
// Al's functions for reading packed vertices.  These are from
// read.m and write.m in the polytopes databases directory
//////////////////////////////////////////////////////////////////////

// Returns the block and line number for the given ID and block size.
function ID_to_block( ID, blocksize )
    block := (ID - 1) div blocksize;
    line := (ID - 1) mod blocksize;
    return block, line + 1;
end function;

function get_database_dir( dbname )
    return FanosearchGetDbRoot() cat "/quiver/";
end function;

// Returns true along with a file pointer to the requested file upon success,
// false and an error string otherwise.
function open_database_block( dbname, block : name:=false )
    root := get_database_dir( dbname );
    if #root eq 0 then
        return false, "MAGMA_LIBRARY_ROOT not set";
    end if;
    file := root cat "block" cat IntegerToString( block );
    try
        fh := Open( file, "r" );
    catch e
        err := "Unable to open database file " cat file;
        return false, err;
    end try;
    return true, fh;
end function;

// Unpacks the integer into a sequence of vertices.
function unpack_vertices( pack, base )
    // First unpack the sequence
    coeffs := IntegerToSequence( pack, base );
    // The first entry is the dimension, the second entry is the shift
    dim := coeffs[1];
    shift := coeffs[2];
    // Shift the coefficients and regroup them as vertices
    coeffs := [Integers() | c - shift : c in coeffs[3..#coeffs]];
    return [PowerSequence(Integers()) |
                    [Integers() | coeffs[dim * i + j] : j in [1..dim]] :
                    i in [0..#coeffs div dim - 1]];
end function;

// Returns true along with the vertices corresponding to the given line of the
// given block file upon success, false and an error string otherwise.
function read_packed_vertices( dbname, block, line : name:=false )
    // Open the requested block file
    bool, fh := open_database_block( dbname, block : name:=name );
    if not bool then return false, fh; end if;
    // The first line of the file contains the base the data is encoded in
    base := Gets( fh );
    if IsEof( base ) then line := -1; end if;
    // Read the requested line
    while line gt 0 do
        s := Gets( fh );
        if IsEof( s ) then
            line := -1;
            break;
        end if;
        line -:= 1;
    end while;
    // Were we successful?
    if line ne 0 then
        if name cmpeq false then name:=dbname; end if;
        return false, "The database \"" cat name cat "\" is corrupted";
    end if;
    // Try to convert the data we've read
    try
        base := StringToInteger( base );
        s := StringToInteger( s );
        verts := unpack_vertices( s, base );
    catch e
        if name cmpeq false then name:=dbname; end if;
        return false, "The database \"" cat name cat "\" is corrupted";
    end try;
    // Return the vertices
    return true, verts;
end function;

//////////////////////////////////////////////////////////////////////
// Intrinsics
//////////////////////////////////////////////////////////////////////

intrinsic QuiverFlagVarietyFano( dim::RngIntElt, ID::RngIntElt ) -> QuvFlg
    {The Fano quiver flag variety with given dimension and database ID.  The dimension dim must be in the range 1..8.  If dim is 1 then the ID must be in the range 1..1.  If dim is 2 then the ID must be in the range 1..5.  If dim is 3 then the ID must be in the range 1..21.  If dim is 4 then the ID must be in the range 1..112.  If the dimension is 5 then the ID must be in the range 1..640.  If the dimension is 6 then the ID must be in the range 1..4061.  If the dimension is 7 then the ID must be in the range 1..27202.  If the dimension is 8 then the ID must be in the range 1..191002.  The database contains all Fano quiver flag varieties of dimension at most 8 in normal form: see NormalForm.  They are ordered by dimension, then by Picard rank, then lexicographically by dimension vector, then lexicographically by the columns of the adjacency matrix.}
    //////////////////////////////////////////////////////////////////////
    // sanity checks
    //////////////////////////////////////////////////////////////////////
    // Is the dimension in the range 1..8?
    require dim ge 1 and dim le 8: "The dimension must be in the range 1..8";
    // Is the ID within the database range?
    number_of_quivers := [ 1, 5, 21, 112, 640, 4061, 27202, 191002 ];
    err := Sprintf("If dim is %o then the ID must be in the range 1..%o", dim, number_of_quivers[dim]);
    require ID ge 1 and ID le number_of_quivers[dim]: err;
    //////////////////////////////////////////////////////////////////////
    // Retrieve the quiver as a sequence of integer sequences
    //////////////////////////////////////////////////////////////////////
    offset := &+[Integers() | number_of_quivers[i] : i in [1..dim-1]];
    block, line := ID_to_block(ID+offset,5000);
    bool, seq := read_packed_vertices("", block, line);
    require bool: seq;
    // Return the quiver flag variety
    return sequence_to_quiver(seq);
end intrinsic;

intrinsic QuiverFlagVarietyFano( ID::RngIntElt ) -> AlgMatElt, SeqEnum
{The Fano quiver flag variety with given database ID.  The dimension ID must be in the range 1..223044.  The database contains all Fano quiver flag varieties of dimension at most 8 in normal form: see NormalForm.  They are ordered by dimension, then by Picard rank, then lexicographically by dimension vector, then lexicographically by the columns of the adjacency matrix.}
    //////////////////////////////////////////////////////////////////////
    // sanity checks
    //////////////////////////////////////////////////////////////////////
    require ID ge 1 and ID le 223044: "The ID must be in the range 1..223044";
    //////////////////////////////////////////////////////////////////////
    // Retrieve the quiver as a sequence of integer sequences
    //////////////////////////////////////////////////////////////////////
    block, line := ID_to_block(ID,5000);
    bool, seq := read_packed_vertices("", block, line);
    require bool: seq;
    // Return the quiver as a pair dimension_vector, adjacency_matrix
    return sequence_to_quiver(seq);
end intrinsic;

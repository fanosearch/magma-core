freeze;

//////////////////////////////////////////////////////////////////////
// quiver.m
//////////////////////////////////////////////////////////////////////
// Functions for creating and printing quiver flag varieties
//////////////////////////////////////////////////////////////////////

// internally we represent a quiver Q by a pair A, dim where A is a
// (rho+1) x (rho+1) adjacency matrix and dim is a dimension vector
// of length rho+1 with first entry equal to 1.
//
// In A Craw, Quiver flag varieties and multigraded linear series,
// Duke Mathematical Journal 156 (2011) 469-500
//
// Craw indexes vertices from zero, and the zero-th vertex is always
// the unique source of the quiver.  We always index vertices from 1
// and we do not demand that the source vertex is the first vertex.
// If the quiver is in normal form (see NormalForm below) then the
// source is the first vertex, however.


declare type QuvFlg;
declare attributes QuvFlg:
	A,                   // the adjacency matrix of the underlying quiver
	r,                   // the dimension vector
	hash_value,          // the hash value of the quiver flag variety
	normal_form,         // the normal form of the quiver flag variety (see NormalForm below)
	normal_form_reorder, // the vertex reordering that puts Q into normal form (see NormalForm below)
	abelianization,      // the abelianization of the quiver flag variety (see abelianization.m).  This is a toric quiver flag variety.
	dimension,           // the dimension of the quiver flag variety
	Picard_lattice,      // the Picard lattice of the quiver flag variety
	nef_cone,            // the nef cone of the quiver flag variety
	Mori_cone,           // the Mori cone of the quiver flag variety
	minus_K;             // the anticanonical class of the quiver flag variety.  This is an element of the Picard lattice.

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// return the directed graph specified by the adjacency matrix A.
// This has an arrow from vertex i to vertex j if and only if
// A[i,j] gt 0
function directed_graph_from_adjacency_matrix(A)
    // sanity checks
    error if NumberOfRows(A) ne NumberOfColumns(A), "A must be a square matrix";
    bool, A := CanChangeRing(A, Integers());
    error if not bool, "A must be a matrix of integers";
    // make another adjacency matrix which amalgamates multiple edges
    n := NumberOfRows(A);
    B := ZeroMatrix(Integers(), n, n);
    for i in [1..n] do
        for j in [1..n] do
            if A[i,j] gt 0 then
                B[i,j] := 1;
            end if;
        end for;
    end for;
    G := Digraph< n | B >;
    // label the edges with their multiplicity
    for e in Edges(G) do
	i := Index(Vertices(G), InitialVertex(e));
	j := Index(Vertices(G), TerminalVertex(e));
	AssignLabel(~G, e, A[i,j]);
    end for;
    return G;
end function;

// returns true if and only if the j-th vertex is a source for the
// quiver with adjacency matrix A
function is_source(A, j)
    return &and[A[i,j] eq 0 : i in [1..NumberOfRows(A)]];
end function;

// returns true, i if the quiver with adjacency matrix A has vertex i
// as its unique source, and false, _ otherwise
function has_unique_source(A)
    sources := [ Integers()|];
    for j in [1..NumberOfColumns(A)] do
	if is_source(A,j) then
	    Append(~sources, j);
	end if;
    end for;
    // was there a unique source?
    if #sources ne 1 then
	return false, _;
    end if;
    return true, sources[1];
end function;

 // returns true if the quiver with adjacency matrix A is connected and
// acyclic with unique source, and false otherwise.  A is assumed to
// be non-empty and square, with non-negative integer entries.
function is_connected_and_acyclic_with_unique_source(A)
    // check that there are no self-loops
    for j in [1..NumberOfColumns(A)] do
	if A[j,j] ne 0 then
	    return false;
	end if;
    end for;
    // check if there is a unique source
    if not has_unique_source(A) then
	return false;
    end if;
    // build the quiver as a directed graph
    G := directed_graph_from_adjacency_matrix(A);
    // check connectedness
    if not IsWeaklyConnected(G) then
	return false;
    end if;
    // check acyclicity
    return #StronglyConnectedComponents(G) eq NumberOfVertices(G);
end function;


// canonicalize takes a pair A, dim where A is the adjacency matrix of a
// quiver Q and dim is the dimension vector, and returns the corresponding
// pair A_can, dim_can for an isomorphic quiver such that:
// * the entries of dim_can are non-decreasing; and
// * with this constraint, the columns of A_can are lex-minimal.
// These constraints single out a unique quiver isomorphic to Q.
// Note that the source vertex is always the first vertex of the canonicalized quiver.
function canonicalize(A, dim)
    // sort the dimension vector and record the reordering
    // this satisfies dim_can[i] eq dim[reorder[i]]
    dim_can := dim;
    reorder := [1..#dim];
    if dim_can ne Sort(dim_can) then
	// this looks redundant -- why not just ParallelSort it
	// anyway -- but this ensures that the reordering is the
	// identity permutation if possible
	ParallelSort(~dim_can, ~reorder);
    end if;
    // build the new adjacency matrix
    new_A := ZeroMatrix(Integers(),#dim,#dim);
    for i in [1..#dim] do
	for j in [1..#dim] do
	    new_A[i,j] := A[reorder[i], reorder[j]];
	end for;
    end for;
    // now walk over the possible reorderings which preserve dim_can
    // and look for the adjacency matrix with lex-minimal columns
    blocks := [];
    for entry in Sort(SequenceToSet(dim_can)) do
	Append(~blocks, Sym(#[d: d in dim_can | d eq entry]));
    end for;
    reorderings := [ [i^g : i in [1..#dim]] : g in DirectProduct(blocks)];
    A_can := new_A;
    reorder_can := reorder;
    for r in reorderings do
	// build the new adjacency matrix
	this_A := ZeroMatrix(Integers(),#dim,#dim);
	for i in [1..#dim] do
	    for j in [1..#dim] do
		this_A[i,j] := new_A[r[i], r[j]];
	    end for;
	end for;
	if ColumnSequence(this_A) lt ColumnSequence(A_can) then
	    // this is the new minimum
	    A_can := this_A;
	    reorder_can := [reorder[r[i]] : i in [1..#dim]];
	end if;
    end for;
    return A_can, dim_can, reorder_can;
end function;

//////////////////////////////////////////////////////////////////////
// Creation of quiver flag varieties
//////////////////////////////////////////////////////////////////////

intrinsic QuiverFlagVariety(A::AlgMatElt, r::SeqEnum) -> QuvFlg
{The quiver flag variety defined by the adjacency matrix A and the dimension vector r.  The quiver defined by A must be acyclic with unique source.  The i-th entry in the dimension vector, where the source vertex is the i-th vertex, must be 1.}
    // sanity checks
    require NumberOfRows(A) gt 0 and NumberOfColumns(A) gt 0:
	    "The adjacency matrix A must be non-empty.";
    require NumberOfRows(A) eq NumberOfColumns(A):
	    "The adjacency matrix A must be square.";
    bool, A := CanChangeRing(A, Integers());
    require bool and &and[x ge 0 : x in &cat RowSequence(A)]:
	    "The adjacency matrix must contain only non-negative integer entries.";
    require is_connected_and_acyclic_with_unique_source(A):
	    "The quiver defined by A must be connected and acyclic with a unique source.";
    require NumberOfRows(A) eq #r:
	    "The adjacency matrix A and the dimension vector r are of incompatible sizes.";
    bool, r := CanChangeUniverse(r, Integers());
    require &and[x gt 0 : x in r]:
	    "The dimension vector r must have non-negative integer entries.";
    source_idx := [j : j in [1..NumberOfColumns(A)] | is_source(A, j)][1];
    require r[source_idx] eq 1:
	    "The entry in the dimension vector corresponding to the source vertex must be 1.";
    // build the quiver flag variety and return
    Q := New(QuvFlg);
    Q`A := A;
    Q`r := r;
    return Q;
end intrinsic;

//////////////////////////////////////////////////////////////////////
// Functions to display a quiver flag variety
//////////////////////////////////////////////////////////////////////

intrinsic Print(Q::QuvFlg)
    {Display the quiver flag variety Q.}
    printf "The quiver flag variety with adjacency matrix:\n%o\nand dimension vector:\n%o.", Q`A, Q`r;
end intrinsic;

//////////////////////////////////////////////////////////////////////
// Normal form functions
//////////////////////////////////////////////////////////////////////

intrinsic NormalForm(Q::QuvFlg) -> QuvFlg, SeqEnum
    {Returns Q in normal form, that is, with vertices reordered such that the dimension vector is increasing and, subject to this constraint, the columns of the adjacency matrix of the quiver are lex-minimal.  The normal form of a quiver flag variety is unique and has the source as the initial vertex.  Also returns the vertex reordering that puts Q into normal form.}
    if not assigned Q`normal_form then
	A_can, dim_can, reorder_can := canonicalize(Q`A, Q`r);
	NF := QuiverFlagVariety(A_can, dim_can);
	Q`normal_form := NF;
	Q`normal_form_reorder := reorder_can;
	NF`normal_form := NF;
	NF`normal_form_reorder := [ i : i in [1..#reorder_can]];
    end if;
    return Q`normal_form, Q`normal_form_reorder;
end intrinsic;

//////////////////////////////////////////////////////////////////////
// Hash and comparison functions
//////////////////////////////////////////////////////////////////////

intrinsic Hash(Q::QuvFlg) -> RngIntElt
{The hash value of the quiver flag variety}
    if not assigned Q`hash_value then
        Q`hash_value:=Hash(RowSequence(Q`A) cat [Q`r]);
    end if;
    return Q`hash_value;
end intrinsic;


intrinsic 'eq'(Q1::QuvFlg, Q2::QuvFlg) -> BoolElt
{True iff the quiver flag varieties Q1 and Q2 are equal.}
    return Q1`r eq Q2`r and Q1`A eq Q2`A;
end intrinsic;

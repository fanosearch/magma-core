freeze;

//////////////////////////////////////////////////////////////////////
// picard.m
//////////////////////////////////////////////////////////////////////
// Functions for computing the Picard group, nef cone, etc. of
// a quiver flag variety
//////////////////////////////////////////////////////////////////////

import "quiver.m": directed_graph_from_adjacency_matrix, has_unique_source;
import "basic.m": si, siprime;

//////////////////////////////////////////////////////////////////////
// local functions
//////////////////////////////////////////////////////////////////////

// return the directed graph obtained from the quiver Q by removing
// all arrows in and out of vertex i
function graph_with_arrows_removed(Q,i)
   A:=AdjacencyMatrix(Q);
   for j in [1..NumberOfRows(A)] do
    A[i,j]:=0;
    A[j,i]:=0;
   end for;
   return directed_graph_from_adjacency_matrix(A);
end function;

// For a vertex i, find all j such that all paths from the source to j pass through
// i, assuming that we can always go around a vertex with rj>1.
function downstream(Q,i)
    G:=Quiver(Q);
    ok, source := has_unique_source(AdjacencyMatrix(Q));
    error if not ok, "The quiver should have a unique source";
    if DimensionVector(Q)[i] gt 1 then
	return [i];
    else
	Gi:=graph_with_arrows_removed(Q,i);
	V := Vertices(Gi);
	return [j : j in [1..#V] | not Reachable(V[source],V[j])];
    end if;
end function;

// compute_divisor_data computes the Picard lattice, nef cone, and
// Mori cone of the quiver flag variety Q, and caches them on Q
procedure compute_divisor_data(Q)
    // sanity check
    error if Type(Q) ne QuvFlg, "Q must be a quiver flag variety";
    ok, source := has_unique_source(AdjacencyMatrix(Q));
    error if (not ok) or (source ne 1), "The first vertex of Q must be the unique source";
    // Build the lattices
    L := ToricLattice(#Q`r-1);
    L`name := "Pic";
    M := Dual(L);
    M`name := "Pic^*";
    // Build the nef cone
    ineqs:=[];
    for i in [2..#Q`r] do
	mi:=[0 : k in [2..#Q`r]];
	for j in downstream(Q,i) do
	    mi[j-1]:=Q`r[j];
	end for;
	Append(~ineqs, M ! mi);
    end for;
    Q`nef_cone := ConeWithInequalities(ineqs);
    // build the anticanonical class
    Q`minus_K := L![si(Q,i) - siprime(Q,i) : i in [1..#Q`r-1]];
    // cache the rest of the data
    Q`Mori_cone := Dual(Q`nef_cone);
    Q`Picard_lattice := L;
end procedure;

//////////////////////////////////////////////////////////////////////
// Intrinsics
//////////////////////////////////////////////////////////////////////

intrinsic NefCone(Q::QuvFlg) -> TorCon
{The nef cone of the quiver flag variety Q.}
    if not assigned Q`nef_cone then
	compute_divisor_data(Q);
    end if;
    return Q`nef_cone;
end intrinsic;

intrinsic MoriCone(Q::QuvFlg) -> TorCon
{The Mori cone of the quiver flag variety Q.}
    if not assigned Q`Mori_cone then
	compute_divisor_data(Q);
    end if;
    return Q`Mori_cone;
end intrinsic;

intrinsic PicardLattice(Q::QuvFlg) -> TorLat
{The Picard lattice of the quiver flag variety Q.}
    if not assigned Q`Picard_lattice then
	compute_divisor_data(Q);
    end if;
    return Q`Picard_lattice;
end intrinsic;

intrinsic CanonicalClass(Q::QuvFlg) -> TorLatElt
{The canonical class of the quiver flag variety Q.}
    if not assigned Q`minus_K then
	compute_divisor_data(Q);
    end if;
    return -Q`minus_K;
end intrinsic;

intrinsic IsFano(Q::QuvFlg) -> Bool
    {True iff the quiver flag variety Q is Fano.}
    if not assigned Q`minus_K then
	compute_divisor_data(Q);
    end if;
    return IsInInterior(Q`minus_K, Q`nef_cone);
end intrinsic;



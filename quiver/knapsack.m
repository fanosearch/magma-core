freeze;

//////////////////////////////////////////////////////////////////////
// vector_bundles.m
//////////////////////////////////////////////////////////////////////
// Functions for finding Fano quiver flag zero loci
//////////////////////////////////////////////////////////////////////

import "irreducible.m": irreducible_vector_bundles;
import "../ci/ci.m": first_Chern_classes_for_nef_ci;
import "flags.m": largest_rank_flag;

declare attributes QuvFlg:

	all_vector_bundles;    // an associative array as follows

/*

all_vector_bundles is an associative array indexed by rank r, with the
r-th entry equal to an associative array indexed by subsets of
Q`irreducible_vector_bundles.  The S-th entry of this array is the set
of all direct sums E = E_1 + ... + E_k of vector bundles such that
rank(E) = r, -K_Q - c_1(E) is ample on Q, and for each i:
(1) E_i is in the set Q`irreducible_vector_bundles; and
(2) none of the E_i are in S

*/

/*
timing data for the first 10000 quiver flag varieties on the Imperial HPC,
on 22 Nov 2017.

Q := QuiverFlagVarietyFano(N);
_ := QuiverFlagBundles(Q,4);

box_and_check:  takes roughly V/90000 seconds, where V is the box volume

the plot looks roughly linear -- or at least, in a cone through the
origin -- and the best fit line is T = 1.05897E-05 * V + 3.234023207.
The best-fit line for the top of the cone -- i.e. the worst-case
runtimes -- is T = 1.85685E-05 * V - 9.293546, so the worst-case
runtime is around V/53858.

polyhedron performs very poorly, in general

hilbert basis beats two-phase hilbert basis, on average, and the
differences involved are small anyway (the timing difference was
almost always very small, and ranged from -5s to 1.85s over the first
10000 quiver flag varieties)

hilbert basis beats box_and_check basically always.  Also, the biggest
win of box-and-check over hilbert_basis over the first 10000 quiver
flag varieties -- other than for a handful of cases where the hilbert
basis algorithm doesn't terminate in a reasonable time -- is 0.1s.  So
we should use hilbert basis by default, and enable the other
algorithms via an optional argument.

*/


//////////////////////////////////////////////////////////////////////
// local functions
//////////////////////////////////////////////////////////////////////

// validate returns true if and only if all of the following are true: Q is a quiver flag variety in normal form, rank is a non-negative integer, include is a quiver flag bundle,
function validate(Q, rank, include, exclude)
    // validate Q
    error if not Type(Q) eq QuvFlg, "Q must be a quiver flag variety.";
    error if not Q eq NormalForm(Q), "The quiver flag variety must be in normal form.";
    // validate rank
    error if not rank in Integers(), "The rank must be an integer.";
    error if not rank ge 0, "The rank must be non-negative.";
    // validate 'include'
    error if not Type(include) eq QuvFlgBun, "The parameter 'include' must be of type QuvFlgBun.";
    // validate 'exclude'
    error if not (Type(exclude) eq SetEnum and Universe(exclude) cmpeq PowerStructure(QuvFlgBun)), "The parameter 'exclude' must be a set of elements of type QuvFlgBun.";
    error if not &and[QuiverFlagVariety(E) eq Q : E in exclude], "The elements of 'exclude' must be quiver flag bundles on Q.";
    error if not &and[IsIrreducible(E) : E in exclude], "The elements of 'exclude' must be irreducible quiver flag bundles.";
    // validation succeeded
    return true;
end function;


// cache_insert stores the pair [{include}, exclude] in the cache Q`all_vector_bundles[rank].  It initialises the cache if necessary.
procedure cache_insert(Q, include, exclude, rank, result);
    // initialise the cache if necessary
    if not assigned Q`all_vector_bundles then
		Q`all_vector_bundles := AssociativeArray(Integers());
    end if;
    if not IsDefined(Q`all_vector_bundles, rank) then
		Q`all_vector_bundles[rank] := AssociativeArray(SeqEnum(PowerSet(PowerStructure(QuvFlgBun))));
    end if;
    // do the insert
    Q`all_vector_bundles[rank][[{include}, exclude]] := result;
end procedure;


// cache_lookup looks up the pair [{include}, exclude] in the cache Q`all_vector_bundles[rank], returning true, value on success and false, _ otherwise.  It initialises the cache if necessary.
function cache_lookup(Q, include, exclude, rank)
    // initialise the cache if necessary
    if not assigned Q`all_vector_bundles then
		Q`all_vector_bundles := AssociativeArray(Integers());
    end if;
    if not IsDefined(Q`all_vector_bundles, rank) then
		Q`all_vector_bundles[rank] := AssociativeArray(PowerSequence(PowerSet(PowerStructure(QuvFlgBun))));
    end if;
    // look up the data
    key := [{include}, exclude];
    if not IsDefined(Q`all_vector_bundles[rank], key) then
		// look for smaller 'include' bundles and smaller exclude sets
		success := false;
		for k in Keys(Q`all_vector_bundles[rank]) do
		    // grab the corresponding include bundle and exclude set
		    assert #k[1] eq 1;
		    k_include := Representative(k[1]);
		    k_exclude := k[2];
		    if k_exclude subset exclude and IsSummand(k_include, include) then
				Q`all_vector_bundles[rank][key] := {E : E in Q`all_vector_bundles[rank][k] | IsSummand(include, E) and IsEmpty(SequenceToSet(Summands(E)) meet exclude)};
				success := true;
				break;
		    end if;
		end for;
		// did we find it?
		if not success then
		    return false, _;
		end if;
    end if;
    return true, Q`all_vector_bundles[rank][key];
end function;

// is_ample returns true if and only if L is ample on Q, that is, if and only if L lies in the interior of the nef cone of Q.  L should be an element of the Picard lattice of the quiver flag variety Q.
function is_ample(L, Q)
    error if not Type(Q) eq QuvFlg, "Q should be a quiver flag variety";
    error if not L in PicardLattice(Q), "L should be an element of the Picard lattice of Q";
    return IsInInterior(L, NefCone(Q));
end function;

// is_nef returns true if and only if L is a nef class on Q.  L should be an element of the Picard lattice of the quiver flag variety Q.
function is_nef(L, Q)
    error if not Type(Q) eq QuvFlg, "Q should be a quiver flag variety";
    error if not L in PicardLattice(Q), "L should be an element of the Picard lattice of Q";
    return L in NefCone(Q);
end function;


// max_times returns a sequence of integers [n_1,...,n_k] where n_i is maximal such that A-n_i c_1(E_i) is ample on Q and n_i rank(E_i) le rank.  Here E = [E_1,...,E_k] is a non-empty sequence of non-zero irreducible quiver flag bundles on Q such that c_1(E_i) is nef, Q is a Fano quiver flag variety, rank is a non-negative integer, and A is an ample class on Q.
function max_times(Q, A, rank, E)
    result := [Integers()|];
    for e in E do
		error if IsZero(e), "Each bundle in E should be non-zero.";
		error if not &and[is_nef(FirstChernClass(e), Q): e in E], "Each bundle in E should have nef first Chern class.";
		n := 0;
		while is_ample(A - n*FirstChernClass(e), Q) and n*Rank(e) le rank do
		    n +:=1;
		end while;
		Append(~result, n-1);
    end for;
    return result;
end function;

// box_and_check_validate validates the arguments for box_and_check, returning true on success.
function box_and_check_validate(Q, A, d, E, n)
    error if not Type(Q) eq QuvFlg, "Q must be a quiver flag variety.";
    error if not Q eq NormalForm(Q), "Q must be in normal form.";
    error if not IsFano(Q), "Q must be Fano.";
    error if not is_ample(A, Q), "A must be an ample class on Q.";
    error if not (d in Integers() and d ge 0), "d must be a non-negative integer.";
    error if not (Type(E) eq SeqEnum and Universe(E) cmpeq PowerStructure(QuvFlgBun)), "E must be a sequence of quiver flag bundles";
    error if not &and[QuiverFlagVariety(e) eq Q : e in E], "E must be a sequence of quiver flag bundles on Q.";
    error if not &and[is_nef(FirstChernClass(e), Q) : e in E], "Each element of E must have nef first Chern class";
    error if not &and[r in Integers() and r ge 0 : r in n], "Each element of n must be a non-negative integer.";
    error if not #E eq #n, "E and n must have the same length.";
    // validation succeeded
    return true;
end function;

// All decompositions of n into k non-negative parts (in any order)
function non_negative_decompositions(n, k)
    partitions:=[[xx - 1 : xx in yy] : yy in Partitions(n + k, k)];
    return SetToSequence(&join[{[xx[j^g] : j in [1..#xx]] : xx in partitions} : g in Sym(k)]);
end function;

// box_and_check returns the set of all a_1 E_1 + ... + a_k E_k where each a_i is an integer in [0..n_i] and A - a_1 c_1(E_1) - ... - a_k c_1(E_k) is ample on the quiver flag variety Q and a_1 rank(E_1) + ... + a_k rank(E_k) le d.  Q must be a Fano quiver flag variety in normal form.  A must be an ample class on Q.  d must be a non-negative integer.  E must be a sequence of quiver flag bundles on Q.  Each element of E must have nef first Chern class.  Each element of n must be a non-negative integer.  n and E must be of the same positive length.
function box_and_check(Q, A, d, E, n)
    // sanity check
    error if not box_and_check_validate(Q, A, d, E, n), "Bad arguments";
    // deal with trivial cases
    if #E eq 0 or d eq 0 then
		return {PowerStructure(QuvFlgBun)| ZeroBundle(Q)};
    end if;
    // do the computation
    c1s := [PicardLattice(Q)| FirstChernClass(e): e in E];
    ranks := [Integers()| Rank(e) : e in E];
    k := #n;
    result := {PowerStructure(QuvFlgBun)|};
    for x in CartesianProduct([PowerSequence(Integers())|[0..r] : r in n]) do
		if &+[x[i] * ranks[i] : i in [1..k]] le d and is_ample(A - &+[x[i] * c1s[i] : i in [1..k]], Q) then
		    Include(~result, &+[PowerStructure(QuvFlgBun)| x[i]*E[i] : i in [1..#x]]);
		end if;
    end for;
    return result;
end function;

// can_score returns false if it finds a contradiction to finding n line bundles L_1,...,L_n from lb (not necessarily distinct) such that A - c_1(E) is ample.  NB this function CAN return true even if it is not possible to find such line bundles: it may just not have found a cheap contradiction.
function can_score(Q, A, n, lb)
	assert n ge 0;
    // deal with the trivial case
    if n eq 0 then
		return is_ample(A, Q);
    end if;
    // check constraints from the inequalities of the nef cone
    ineq := Rays(Dual(NefCone(Q)));
    c1s := [PicardLattice(Q)| FirstChernClass(L) : L in lb];
    for f in ineq do
		min := Min([f*c : c in c1s]);
		if n*min gt f*A then
		    return false;
		end if;
	end for;
    // check 5 random constraints that lie strictly inside the dual to the nef cone
    for count in [1..5] do
		f := &+[(1+Random(10))*c : c in ineq];
		min := Min([f*c : c in c1s]);
		if n*min gt f*A then
		    return false;
		end if;
    end for;
    // check the sum of the inequalities.  This is particularly useful in the flag manifold case.
    f := &+ineq;
    min := Min([f*c : c in c1s]);
    if n*min gt f*A then
	    return false;
	end if;
    // we didn't find a contradiction
    return true;
end function;

// Given a non-empty sequence S = [u1,...,uk] of elements in a toric
// lattice L and a polytope P in L, return a polytope Q in a
// k-dimensional ambient space such that the points of Q are those
// tuples (a1,...,ak) of non-negative integers such that
// a1*u1+...+ak*uk lies in P
function toric_knapsack_for_polyhedron(S, P)
    // sanity checks
    error if #S eq 0, "The sequence S must not be empty";
    L := Universe(S);
    error if not Ambient(P) eq L, "The sequence S and the polytope P live in different lattices.";
    // do the computation
    k := #S;
    positive_orthant := PositiveQuadrant(k);
    LL := Ambient(positive_orthant);
    phi := LatticeMap(LL, S);
    preimage := P @@ phi;
    return preimage meet ConeToPolyhedron(positive_orthant);
end function;

// Given a non-empty sequence S = [u1,...,uk] of a toric lattice L and an
// element P in L, return the set of tuples (a1,...,ak) of non-negative
// integers such that a1*u1+...+ak*uk eq p
function toric_knapsack_for_point(S, p)
    // sanity checks
    error if #S eq 0, "The sequence S must not be empty";
    L := Universe(S);
    error if not p in L, "The sequence S and the element p live in different lattices.";
    // do the computation
    return ToricKnapsack([Eltseq(s) : s in S], Eltseq(p));
end function;

// points_in_polyhedron_validate validates the arguments for points_in_polyhedron, returning true on success.
function points_in_polyhedron_validate(Q, A, target_rank, E, strict)
    error if not Type(Q) eq QuvFlg, "Q must be a quiver flag variety.";
    error if not Q eq NormalForm(Q), "Q must be in normal form.";
    error if not is_ample(A, Q), "A must be an ample class on Q.";
    error if not (target_rank in Integers() and target_rank ge 0), "target_rank must be a non-negative integer.";
    error if not (Type(E) eq SeqEnum and Universe(E) cmpeq PowerStructure(QuvFlgBun)), "E must be a sequence of quiver flag bundles.";
    error if not &and[QuiverFlagVariety(e) eq Q : e in E], "E must be a sequence of quiver flag bundles on Q.";
    error if not Type(strict) eq BoolElt, "strict must be a boolean.";
    // validation succeeded
    return true;
end function;

// points_in_polyhedron returns the set of non-negative integer linear combinations F = a_1 E_1 + ... + a_k E_k of elements of E such that A - c_1(F) is ample and either rank(F) le target_rank (if strict is false) or rank(F) eq target_rank (if target_rank is true).  Q must be a quiver flag variety in normal form.  A must be an ample class on Q.  target_rank must be a non-negative integer.  E must be a sequence of quiver flag bundles on Q.   strict must be a boolean.
function points_in_polyhedron(Q, A, target_rank, E, strict)
    // sanity check
    error if not points_in_polyhedron_validate(Q, A, target_rank, E, strict), "Bad arguments";
    // deal with trivial cases
    if target_rank eq 0 then
		return {PowerStructure(QuvFlgBun)| ZeroBundle(Q)};
    elif #E eq 0 then
		if strict then
		    return {PowerStructure(QuvFlgBun)| };
		else
		    return {PowerStructure(QuvFlgBun)| ZeroBundle(Q)};
		end if;
    end if;
    // do the computation
    c1s := [PicardLattice(Q) | FirstChernClass(e) : e in E];
    ranks := [Integers()| Rank(e) : e in E];
    P := ConeToPolyhedron(NefCone(Q)) meet (A-ConeToPolyhedron(NefCone(Q)));
    PP := toric_knapsack_for_polyhedron(c1s, P);
    // slice PP with the hyperplane or halfspace that implements the rank constraint
    if strict then
		PP := PP meet HyperplaneToPolyhedron(Dual(Ambient(PP))!ranks, target_rank);
    else
		PP := PP meet HalfspaceToPolyhedron(-Dual(Ambient(PP))!ranks, -target_rank);
    end if;
    // reassemble the quiver flag bundles
    results := {PowerStructure(QuvFlgBun)|};
    for x in Points(PP) do
		x_seq := Eltseq(x);
		// skip linear combinations E such that A - c1(E) is strictly nef
		if not is_ample(A - &+[PicardLattice(Q)| x_seq[i]*c1s[i] : i in [1..#x_seq]], Q) then
		    continue;
		end if;
		// reassemble the quiver flag bundle and record it
		F :=  &+[PowerStructure(QuvFlgBun)| x_seq[i] * E[i] : i in [1..#x_seq]];
		if strict then
		    assert Rank(F) eq target_rank;
		else
		    assert Rank(F) le target_rank;
		end if;
		Include(~results, F);
    end for;
    return results;
end function;

// cone_walks returns the set of multisets {* e_1,...,e_k *} such that e_1,...,e_k are non-zero lattice points in C that sum to A and k is at most K.  C should be a strictly convex cone.  A should lie in the same lattice as C.  K should be a positive integer.  This does a knapsack on the elements of the Hilbert basis of C.
function cone_walks(C, A, K)
    // sanity checks
    error if not (Type(C) eq TorCon and IsStrictlyConvex(C)), "C should be a strictly convex cone.";
    L := Ambient(C);
    error if not (Type(A) eq TorLatElt and A in L), "A and C should lie in the same lattice.";
    error if not (K in Integers() and K gt 0), "K should be a positive integer.";
    // get the easy cases out of the way
    if (not A in C) then
		return {PowerMultiset(L)|};
    elif IsZero(A) then
    	return {PowerMultiset(L)| {* L| *} };
    elif K eq 1 then
		return {PowerMultiset(L)| {* A *}} ;
    end if;
    // find all possible ways of writing A as a linear combination of Hilbert basis elements
    hilbert_basis:=HilbertBasis(C : algorithm:="Hemmecke");
    decompositions:=[Eltseq(pt) : pt in Points(ToricKnapsack([Eltseq(b) : b in hilbert_basis], Eltseq(A)))];
    // This is all ways of writing A as a LC of hilbert basis generators
    // Now divide each of them up in all possible ways
    results := {PowerMultiset(L)| {* A *}};
    for dec in decompositions do
		for k in [2..K] do
	            poss:=[PowerSequence(PowerSequence((Integers())))| non_negative_decompositions(u,k) : u in dec];
		    results join:= { {* &+[hilbert_basis[i] * x[i][j] : i in [1..#x]]  : j in [1..k] *} : x in CartesianProduct(poss) };
		end for;
    end for;
    return results;
end function;

// integer_knapsack returns the set of all sequences of elements taken from S (with repeats allowed) which sum to n.  S must be a non-empty set of positive integers and n must be an integer.
function integer_knapsack(S, n)
    // sanity checks
    error if not (Type(S) eq SetEnum and Universe(S) eq Integers() and #S gt 0 and &and[ s gt 0 : s in S]), "S must be a set of positive integers.";
    error if not n in Integers(), "n must be an integer.";
    // deal with the trivial case
    if n le 0 then
		return {PowerSequence(Integers())|};
    end if;
    // do the computation
    m := Min(S);
    return &join[PowerSet(PowerSequence(Integers()))| Knapsack(S, n, l) : l in [1..Floor(n/m)] ];
end function;

// knapsack_on_trivial_bundles returns the set of all multisets of elements taken from T with ranks that sum to exactly (if strict eq true) or at most (if strict eq false) target_rank.  Q should be a quiver flag variety in normal form. T should be a non-empty set of trivial quiver flag bundles on Q.  T should not contain the zero bundle.  target_rank should be an integer.  strict should be a boolean.
function knapsack_on_trivial_bundles(Q, T, target_rank, strict)
    // sanity checks
    error if not Type(Q) eq QuvFlg, "Q must be a quiver flag variety.";
    error if not Q eq NormalForm(Q), "Q must be in normal form.";
    error if not (Type(T) eq SeqEnum and Universe(T) cmpeq PowerStructure(QuvFlgBun) and #T gt 0), "T must be a non-empty sequence of quiver flag bundles.";
    error if not &and[QuiverFlagVariety(t) eq Q and IsTrivial(t): t in T], "T must be a sequence of trivial quiver flag bundles on Q.";
    error if ZeroBundle(Q) in T, "T must not contain the zero bundle.";
    error if not target_rank in Integers(), "target_rank should be a non-negative integer.";
    error if not Type(strict) eq BoolElt, "strict should be a boolean.";
    // handle the trivial cases
    if target_rank le 0 then
		return {PowerMultiset(PowerStructure(QuvFlgBun))|};
    elif not strict then
		return &join[PowerSequence(PowerMultiset(PowerStructure(QuvFlgBun)))| knapsack_on_trivial_bundles(Q, T, k, true) : k in [1..target_rank]];
    end if;
    // do the computation
    ranks := {Integers()| Rank(t) : t in T};
    solutions := [PowerMultiset(Integers())| SequenceToMultiset(x) : x in integer_knapsack(ranks, target_rank)];
    return {PowerMultiset(PowerStructure(QuvFlgBun))| {* PowerStructure(QuvFlgBun)| TrivialBundle(Q, r) : r in s *} : s in solutions};
end function;


// hilbert_basis_knapsack_validate validates the arguments for hilbert_basis_knapsack, returning true on success.
function hilbert_basis_knapsack_validate(Q, A, target_rank, E, strict)
    error if not Type(Q) eq QuvFlg, "Q must be a quiver flag variety.";
    error if not Q eq NormalForm(Q), "Q must be in normal form.";
    error if not is_nef(A, Q), "A must be a nef class on Q.";
    error if not (target_rank in Integers() and target_rank ge 0), "target_rank must be a non-negative integer.";
    error if not (Type(E) eq SeqEnum and Universe(E) cmpeq PowerStructure(QuvFlgBun)), "E must be a sequence of quiver flag bundles.";
    error if not &and[QuiverFlagVariety(e) eq Q : e in E], "E must be a sequence of quiver flag bundles on Q.";
    error if not &and[is_nef(FirstChernClass(e), Q) : e in E], "Each element of E must have nef first Chern class.";
    error if ZeroBundle(Q) in E, "Each element of E must be non-zero.";
    error if not Type(strict) eq BoolElt, "strict must be a boolean.";
    // validation succeeded
    return true;
end function;

// hilbert_basis_knapsack returns the set of multisets {* E_1,...,E_k *} where c_1(E_1) + ... + c_1(E_k) = A, each E_i lies in E, and rank(E_1) + ... + rank(E_k) is either equal to target rank (if strict is true) or is less than or equal to the target rank (if strict is false).  Q should be a quiver flag variety in normal form; A should be a nef class on Q; target_rank should be a non-negative integer; E should be a sequence of non-zero quiver flag bundles on Q, each of which has nef first Chern class; and strict should be a boolean.
function hilbert_basis_knapsack(Q, A, target_rank, E, strict)
    // sanity check
    error if not hilbert_basis_knapsack_validate(Q, A, target_rank, E, strict), "Bad arguments";
    // deal with trivial cases
    if target_rank eq 0 then
		if IsZero(A) then
		    return {PowerMultiset(PowerStructure(QuvFlgBun))| {*PowerStructure(QuvFlgBun)| *}};
		else
		    return {PowerMultiset(PowerStructure(QuvFlgBun))| };
		end if;
	elif #E eq 0 then
		if (not strict) and IsZero(A) then
		    return {PowerMultiset(PowerStructure(QuvFlgBun))| {*PowerStructure(QuvFlgBun)| *}};
		else
		    return {PowerMultiset(PowerStructure(QuvFlgBun))|};
		end if;
    end if;
    // strip out any trivial bundles.  NB these are the only quiver flag bundles with zero first Chern class.
    trivial := [PowerStructure(QuvFlgBun)| e : e in E | IsTrivial(e)];
    E := [PowerStructure(QuvFlgBun)| e : e in E | not IsTrivial(e)];
    // find all possible ways of writing A as a linear combination of k non-zero elements of the nef cone
    // the bound on k comes from thinking about the rank of the bundles
    cw := cone_walks(NefCone(Q), A, Floor(target_rank/Min([Rank(e) : e in E])));
    // for each such linear combination, find all ways of realising it with elements of E
    results := {PowerMultiset(PowerStructure(QuvFlgBun))|};
    for x in cw do
		x_seq := MultisetToSequence(x);
		poss := CartesianProduct( [PowerSequence(PowerStructure(QuvFlgBun))| [PowerStructure(QuvFlgBun)| e : e in E | FirstChernClass(e) eq c1] : c1 in x_seq ] );
		results join:= {PowerMultiset(PowerStructure(QuvFlgBun))| SequenceToMultiset([PowerStructure(QuvFlgBun)| xx : xx in p]) : p in poss };
	end for;
    // do we need to add back some trivial factors?
    if #trivial gt 0 then
		padded_results := {PowerMultiset(PowerStructure(QuvFlgBun))|};
		for m in results do
		    this_rank := &+[Integers()| Rank(e) : e in m];
		    padded_results join:= { m join p : p in knapsack_on_trivial_bundles(Q, trivial, target_rank - this_rank, strict)};
		end for;
		return padded_results;
    elif strict then
		return {PowerMultiset(PowerStructure(QuvFlgBun))| m : m in results | &+[Integers()| Rank(e) : e in m] eq target_rank};
    else
		return {PowerMultiset(PowerStructure(QuvFlgBun))| m : m in results | &+[Integers()| Rank(e) : e in m] le target_rank};
    end if;
end function;

//////////////////////////////////////////////////////////////////////
// wrapper functions for testing
//////////////////////////////////////////////////////////////////////

// box_and_check_wrapper wraps box-and-check
function box_and_check_wrapper(Q, A, target_rank, irreducibles, include, strict)
    m := max_times(Q, A, target_rank, irreducibles);
    if strict then
		return {PowerStructure(QuvFlgBun)| E + include : E in box_and_check(Q, A, target_rank, irreducibles, m) | Rank(E) eq target_rank};
    else
		return {PowerStructure(QuvFlgBun)| E + include : E in box_and_check(Q, A, target_rank, irreducibles, m)};
    end if;
end function;

// polyhedron_wrapper wraps points-in-polyhedron
function polyhedron_wrapper(Q, A, target_rank, irreducibles, include, strict)
    return {PowerStructure(QuvFlgBun)| include + E : E in points_in_polyhedron(Q, A, target_rank, irreducibles, strict)};
end function;

// hb_knapsack_wrapper wraps the Hilbert basis knapsack
function hb_knapsack_wrapper(Q, A, target_rank, irreducibles, include, strict)
    // compute the possible first Chern classes
    vprintf Fanosearch: "hb_knapsack_wrapper: computing first Chern classes...";
    c1s :=  first_Chern_classes_for_nef_ci(NefCone(Q), A);
    vprintf Fanosearch: "done (%o classes)\n", #c1s;
    results := {PowerStructure(QuvFlgBun)| };
    // walk over the possible first Chern classes
    for c1 in c1s do
		// walk over the possible ways of scoring c1
		for E in hilbert_basis_knapsack(Q, c1, target_rank, irreducibles, strict) do
		    // assemble the bundle
		    F := include;
		    for x in MultisetToSequence(E) do
				F +:= x;
		    end for;
		    // record the result
		    Include(~results, F);
		end for;
    end for;
    return results;
end function;

// compute_all_vector_bundles returns the set of vector bundles E on the quiver flag variety Q as specified in the docstring for the intrinsic QuiverFlagBundles below, using the specified algorithm.  We assume that Q is a quiver flag variety in normal form, rank is a non-negative integer, include is a quiver flag bundle on Q, and exclude is a set of irreducible quiver flag bundles on Q.  We assume furthermore that the rank of include is strictly less than rank.  Possible values for algorithm are "bc", "p", "hb", or "X,Y" where X and Y are in { "bc", "p", "hb" }.  These mean: bc = box-and-check, p = points-in-polyhedron, hb = Hilbert basis knapsack, X,Y = two-phase treating the higher-rank bundles first and the line bundles second, using X for the higher-rank computation and Y for the line-bundle computation.  See the timing notes at the top of the file.
function compute_all_vector_bundles(Q, rank, include, exclude, algorithm)
    // check our assumptions about the input
    error if not validate(Q, rank, include, exclude), "Invalid input data";
    error if not Rank(include) lt rank, "The rank of 'include' should be strictly less than the rank.";
    // do the computation
    // if our target first Chern class is not ample, there is nothing to do
    A := -CanonicalClass(Q) - FirstChernClass(include);
    if not is_ample(A, Q) then
		return {PowerStructure(QuvFlgBun)|};
    end if;
    target_rank := rank - Rank(include);
    // if we are on a flag manifold, check if we obviously can't find a vector bundle of this rank
    // if IsFlagManifold(Q) then
   //  	vprintf Fanosearch: "compute_all_vector_bundles: this is a flag manifold.\n";
   //  	vprintf Fanosearch: "compute_all_vector_bundles: checking rank bounds...";
   //  	if target_rank gt largest_rank_flag(DimensionVector(Q), A, TautologicalBundle(Q,NumberOfVertices(Q)-1) in exclude) then
   //  	    vprintf Fanosearch: "failed\n";
   //  	    return {PowerStructure(QuvFlgBun)|};
   //  	end if;
   //  	vprintf Fanosearch: "passed\n";
   //  else
   //  	vprintf Fanosearch: "compute_all_vector_bundles: this is not a flag manifold\n";
   //  end if;
    // if there are no permitted irreducible factors, there is nothing to do
    irreducibles := SetToSequence(irreducible_vector_bundles(Q, target_rank) diff exclude);
    if #irreducibles eq 0 then
    	return {PowerStructure(QuvFlgBun)|};
    end if;

    // compute ingredients for heuristics
    //    t := Cputime();
    //    Logger(Sprintf("Number of bundles: %o\n", #irreducibles));
    //    Logger(Sprintf("Number of line bundles: %o\n", #[i : i in [1..#irreducibles] | Rank(irreducibles[i]) eq 1]));
    //    Logger(Sprintf("Number of higher-rank bundles: %o\n", #[i : i in [1..#irreducibles] | Rank(irreducibles[i]) gt 1]));
    //    Logger(Sprintf("Number of rays of the nef cone: %o\n", #Rays(NefCone(Q))));
    //    Logger(Sprintf("Number of facets of the nef cone: %o\n", #Rays(Dual(NefCone(Q)))));
    // compute the maximum number of times that each irreducible factor could occur
    //    m := max_times(Q, A, target_rank, irreducibles);
    //    box_size := &*[Integers()| x+1 : x in m];
    //    Logger(Sprintf("Box volume: %o\n", box_size));
    //    lb_box_size := &*[Integers()| m[i]+1 : i in [1..#irreducibles] | Rank(irreducibles[i]) eq 1];
    //    Logger(Sprintf("Line bundle box volume: %o\n", lb_box_size));
    //    hd_box_size := &*[Integers()| m[i]+1 : i in [1..#irreducibles] | Rank(irreducibles[i]) gt 1];
    //    Logger(Sprintf("Higher-rank box volume: %o\n", hd_box_size));
    //    hb := HilbertBasis(NefCone(Q) : algorithm:="Hemmecke");
    //    Logger(Sprintf("Hilbert basis size: %o\n", #hb));
    // make sure we have cached data for all the methods
    //    dummy := Rays(Dual(NefCone(Q)));
    //    heuristic_t := Cputime(t);
    //    Logger(Sprintf("Compute heuristics: %o\n", heuristic_t));


    // choose the algorithm
    if algorithm eq "hb" then
		return hb_knapsack_wrapper(Q, A, target_rank, irreducibles, include, true);
    elif algorithm eq "bc" then
		return box_and_check_wrapper(Q, A, target_rank, irreducibles, include, true);
    elif algorithm eq "p" then
		return polyhedron_wrapper(Q, A, target_rank, irreducibles, include, true);
    end if;
    // we must have a two-phase algorithm
    phases := Split(algorithm, ",");
    error if #phases ne 2, "Unknown algorithm";
    methods := [];
    for a in phases do
		if a eq "hb" then
		    Append(~methods, hb_knapsack_wrapper);
		elif a eq "p" then
		    Append(~methods, polyhedron_wrapper);
		elif a eq "bc" then
		    Append(~methods, box_and_check_wrapper);
		else
		    error "Unknown algorithm";
		end if;
    end for;
    f, g := Explode(methods);
    // break irreducibles into line bundles and higher-rank bundles
    lb := [PowerStructure(QuvFlgBun)| E : E in irreducibles | Rank(E) eq 1];
    hd := [PowerStructure(QuvFlgBun)| E : E in irreducibles | Rank(E) gt 1];
    // walk over the possible higher-dimensional parts
    results := {PowerStructure(QuvFlgBun)|};
    for hd_part in f(Q, A, target_rank, hd, include, false) do
		// note that hd_part includes include here
    	// check for a cheap contradiction to using this higher-dimensional part
		if not can_score(Q, -CanonicalClass(Q) - FirstChernClass(hd_part), rank - Rank(hd_part), lb) then
		    continue;
		end if;
		// now walk over the line bundle parts
		results join:= g(Q, -CanonicalClass(Q) - FirstChernClass(hd_part), rank - Rank(hd_part), lb, hd_part, true);
    end for;
    return results;
end function;


//////////////////////////////////////////////////////////////////////
// Intrinsics
//////////////////////////////////////////////////////////////////////

intrinsic QuiverFlagBundles(Q::QuvFlg, rank::RngIntElt : include := false, exclude := false, algorithm := "hb,hb") -> SetEnum
    {The set of vector bundles E on the quiver flag variety Q having the rank specified and the following properties.  Suppose that the dimension vector of Q is [r_0,...,r_rho].  E must be a direct sum of zero or more vector bundles, each of which arises from a representation of GL(r_1) x ... x GL(r_n) specified by a sequence  [\alpha_1,...,\alpha_rho] where, if r_i gt 1, \alpha_i is a weakly decreasing sequence of non-negative integers of length at most r_i (representing a partition with at most r_i parts) and, if r_i eq 1, \alpha_i is a length-1 sequence of integers.  Such a sequence represents the tensor product \otimes_i S^\alpha_i(W_i) of Schur powers of the tautological vector bundles on Q; we demand that each summand of E is of the form L \otimes (\otimes_(i: r_i gt 1) S^\alpha_i(W_i)) where the line bundle L is nef, and that  -K_Q - E is ample on Q.  The quiver flag variety Q must be in normal form: see NormalForm.  The optional parameter 'include' (respectively 'exclude') can be a quiver flag bundle F (respectively a set of irreducible quiver flag bundles G_1,...,G_k) on Q; in this case only quiver flag bundles that contain F as a summand (respectively that do not contain any of the G_i as summands) will be returned.  The default is to set 'include' to the zero bundle and 'exclude' to the set containing the trivial line bundle on Q.}
    // set any missing parameters to their default values
    if include cmpeq false then
		include := ZeroBundle(Q);
    end if;
    if exclude cmpeq false then
		exclude := {TrivialBundle(Q,1)};
    end if;
    // validate the input
    require validate(Q, rank, include, exclude): "Invalid arguments";
    // if Q is not Fano, then there are no such bundles
    if not IsFano(Q) then
		return {PowerStructure(QuvFlgBun)|};
    end if;
    // deal with trivial cases
    if rank lt Rank(include) then
		return {PowerStructure(QuvFlgBun)|};
    elif rank eq Rank(include) then
		if is_ample(-CanonicalClass(Q)-FirstChernClass(include), Q) then
		    return {PowerStructure(QuvFlgBun)|include};
		else
		    return {PowerStructure(QuvFlgBun)|};
		end if;
    end if;
    // did we compute this already?
    ok, result := cache_lookup(Q, include, exclude, rank);
    if not ok then
		// the result isn't in the cache, so compute it and cache it
		result := compute_all_vector_bundles(Q, rank, include, exclude, algorithm);
		cache_insert(Q, include, exclude, rank, result);
    end if;
	// sanity checks
	for E in result do
		assert Rank(E) eq rank;
		assert IsInInterior(-CanonicalClass(Q) - FirstChernClass(E), NefCone(Q));
	end for;
    return result;
end intrinsic;

freeze;

//////////////////////////////////////////////////////////////////////
// quantum_period.m
//////////////////////////////////////////////////////////////////////
// Functions for computing quantum periods of quiver flag varieties
// and quiver flag zero loci
//////////////////////////////////////////////////////////////////////

declare attributes QuvFlg:
	Chow_A_tr,          // the ring in which we do the I-function
	                    // computation.  This is the quotient of
	                    // the Chow ring Q`Chow_A of the
	                    // abelianisation A by the ideal of all
	                    // monomials of sufficiently high degree
	                    // to be irrelevant
	Pic_A_to_Chow_A_tr, // the map from the Picard lattice of A to
	                    // the truncated Chow ring Chow_A_tr
	omega,              // the fundamental anti-invariant, as an
	                    // element of Chow_A_tr
	I_cache;            // a cache of elements of Chow_A_tr which
	                    // occur in the computation of I-functions
                      	    // for quiver flag bundles on Q

declare attributes QuvFlgBun:
	I_terms;            // the sequence of terms in the I-function

import "cohomology.m": compute_Chow_data, compute_bundle_data, toric_divisors_A;

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// Computes various data that we will need to compute the I-function
// of quiver flag bundles on the quiver flag variety Q, and caches
// them on Q.
procedure initialise_cache(Q)
    // ensure that the Chow ring etc. are computed
    if not assigned Q`Chow_A then
	compute_Chow_data(Q);
    end if;
    // build the truncated Chow ring
    ch := Q`Chow_A;
    R := OriginalRing(ch);
    I := DivisorIdeal(ch);
    degree_of_omega := #Q`omega_bundles;
    J := Ideal([R| m : m in MonomialsOfDegree(R,degree_of_omega+1)]);
    ch_tr := R/(I+J);
    Q`Chow_A_tr := ch_tr;
    // build the map from the Chow ring to the truncated Chow ring
    psi := hom<ch->ch_tr|[ch_tr| ch_tr.i : i in [1..Rank(R)]]>;
    // build the map from the Picard lattice to the truncated Chow ring
    f := Q`Pic_A_to_Chow_A;
    phi := func<x|psi(f(x))>;
    Q`Pic_A_to_Chow_A_tr := phi;
    // build the fundamental anti-invariant
    omega := &*[ch_tr | phi(x) : x in Q`omega_bundles];
    Q`omega := omega;
   // initialise the cache
    if not assigned Q`I_cache then
	Q`I_cache := AssociativeArray();
    end if;
end procedure;

// computes and caches the element of the truncated Chow ring
// \prod_{m \leq 0} (x + m) / \prod_{m \leq M} (x + m)
// that occurs in the I-function.  Here x is an element of the
// truncated Chow ring.
function I_divisor_term_raw(Q, x, M)
    // sanity checks
    error if not M in Integers(), "M must be an integer";
    ch_tr := Q`Chow_A_tr;
    error if not x in ch_tr, "x must be an element of the truncated Chow ring";
    // handle the trivial case
    if M eq 0 then
	return One(ch_tr);
    end if;
    // check the cache
    Ix_exists, Ix_cache := IsDefined(Q`I_cache, x);
    if Ix_exists then
	M_exists, result := IsDefined(Ix_cache, M);
	if M_exists then
	    return result;
	end if;
    else
	Q`I_cache[x] := AssociativeArray(Integers());
    end if;
    // we did not compute this term yet, so compute it
    if M lt 0 then
	result := I_divisor_term_raw(Q, x, M+1) * (x + M + 1);
    else
	result := I_divisor_term_raw(Q, x, M-1) / (x + M);
    end if;
    // cache the result and return
    Q`I_cache[x][M] := result;
    return result;
end function;

// computes and caches the element of the truncated Chow ring
// \prod_{m \leq 0} (x + m) / \prod_{m \leq M} (x + m)
// that occurs in the I-function.  Here x is an element of the
// truncated Chow ring.
function I_divisor_term(Q, x, M)
    // sanity checks
    error if not M in Integers(), "M must be an integer";
    ch_tr := Q`Chow_A_tr;
    error if not x in ch_tr, "x must be an element of the truncated Chow ring";
    // do the computation
    return I_divisor_term_raw(Q, x, M);
end function;

// computes the n-th term in the I-function of the quiver flag bundle
// E
function compute_I_term(E,n)
    Q := E`Q;
    A := Abelianization(Q);
    // grab the cached data
    ch_tr := Q`Chow_A_tr;
    phi := Q`Pic_A_to_Chow_A_tr;
    point_class := Q`point_class_A;
    omega_bundles := Q`omega_bundles;
    omega := &*[ch_tr | phi(E) : E in omega_bundles];
    line_bundles := E`line_bundles_A;
    minus_KX := E`minus_KX_A;
    // do the sum over degrees
    t := Cputime();
    vprintf Fanosearch: "Computing the degree %o coefficient of the I-function...", n;
    result := Zero(ch_tr);
    for d in Points(MoriCone(A), minus_KX, n) do
    	if IsEven(&+[Integers()|E*d : E in omega_bundles]) then
    	    sign := 1;
    	else
    	    sign := -1;
    	end if;
    	result +:= sign *
    		   &*[ch_tr| I_divisor_term(Q, phi(D), D*d) : D in toric_divisors_A(Q)] *
    		   &*[ch_tr| 1/I_divisor_term(Q, phi(E), E*d) : E in line_bundles] *
    		   &*[ch_tr| phi(E) + E*d : E in omega_bundles];
    end for;
    vprintf Fanosearch: "done (%os)\n", Cputime(t);
    return Rationals()!ExactQuotient(result, omega);
end function;

// computes and caches the first n+1 terms in the I-function of the
// quiver flag bundle E. Uses computations in the Chow ring.
function compute_I_series_Chow(E, n)
    Q := E`Q;
    // initialise the caches if necessary
    if not assigned Q`I_cache then
	initialise_cache(Q);
    end if;
    if not assigned E`I_terms then
	E`I_terms := [Rationals()| 1];
    end if;
    // do we know the answer already?
    N := #E`I_terms;
    if N lt n+1 then
	// compute and cache the terms that we need
	for k in [N..n] do
	    Append(~E`I_terms, compute_I_term(E,k));
	end for;
    end if;
    // return the correct number of terms
    return E`I_terms[1..n+1];
end function;

// computes the first n+1 terms of the regularised quantum period for
// the zero locus X of a regular section of the quiver flag bundle E.
// Assumes that X is Fano.
function compute_quantum_period(E, n : algorithm:="gamma")
    // Sanity check
    error if not algorithm in {"gamma", "Chow"}, "unknown algorithm";
    T<x> := PowerSeriesRing(Rationals(): Precision:=n+1);
    if algorithm eq "Chow" then
        I_series := compute_I_series_Chow(E,n);
    else
        I_series := [IFunctionCoefficient(E,k) : k in [0..n]];
    end if;
    I_function := T!I_series;
    // apply the mirror map
    if #I_series ge 2 then
	c := I_series[2];
    else
	c := Zero(Rationals());
    end if;
    J_function := Exp(-c*x)*I_function;
    // regularise
    ps := [Factorial(k) * Coefficient(J_function,k) : k in [0..n]];
    // sanity check
    bool, result := CanChangeUniverse(ps, Integers());
    error if not bool, "Something is wrong.  The regularised quantum period sequence should consist of integers.";
    return result;
end function;

//////////////////////////////////////////////////////////////////////
// Intrinsics
//////////////////////////////////////////////////////////////////////

intrinsic PeriodSequence(E::QuvFlgBun, l::RngIntElt : algorithm:="gamma") -> SeqEnum
{The first l+1 coefficients of the regularised quantum period sequence for the zero locus X of a generic section of E. Algorithm can be either "gamma", for an explicit formula based on derivatives of the Gamma function, or "Chow" for a Chow ring computation.}
    // sanity checks
    require l ge 0: "l must be non-negative";
    require algorithm in {"gamma", "Chow"}: "unknown algorithm";
    // initialise the caches if necessary
    Q := E`Q;
    if not assigned Q`I_cache then
	initialise_cache(Q);
    end if;
    if not assigned E`minus_KX_A then
	compute_bundle_data(E);
    end if;
    if not assigned E`I_terms then
	E`I_terms := [Rationals()| 1];
    end if;
    // check that the zero locus is Fano
    error if not IsInInterior(E`minus_KX_A, NefCone(Abelianization(Q))), "X must be Fano";
    // do the computation
    return compute_quantum_period(E,l : algorithm:=algorithm);
end intrinsic;

intrinsic PeriodSequence(Q::QuvFlg, l::RngIntElt : algorithm:="gamma") -> SeqEnum
    {The first l+1 coefficients of the regularised quantum period sequence for the quiver flag variety Q. Algorithm can be either "gamma", for an explicit formula based on derivatives of the Gamma function, or "Chow" for a Chow ring computation.}
    return PeriodSequence(ZeroBundle(Q),l : algorithm:=algorithm);
end intrinsic;

intrinsic UnregularisedPeriodCoefficient(E::QuvFlgBun, k::RngIntElt) -> FldRatElt
    {The k-th coefficient of the unregularised quantum period sequence for the zero locus X of a generic section of E.}
    // sanity check
    require k ge 0: "k must be non-negative";
    // initialise the caches if necessary
    Q := E`Q;
    if not assigned Q`I_cache then
	initialise_cache(Q);
    end if;
    if not assigned E`minus_KX_A then
	compute_bundle_data(E);
    end if;
    // check that the zero locus is Fano
    error if not IsInInterior(E`minus_KX_A, NefCone(Abelianization(Q))), "X must be Fano";
    // do the computation
    return compute_I_term(E,k);
end intrinsic;

intrinsic UnregularisedPeriodCoefficient(Q::QuvFlg, k::RngIntElt) -> FldRatElt
    {The k-th coefficient of the unregularised quantum period sequence for the quiver flag variety Q.}
    return UnregularisedPeriodCoefficient(ZeroBundle(Q),k);
end intrinsic;


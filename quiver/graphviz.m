freeze;

//////////////////////////////////////////////////////////////////////
// graphviz.m
//////////////////////////////////////////////////////////////////////
// Functions for visualizing quiver flag varieties using graphviz
//////////////////////////////////////////////////////////////////////

import "basic.m": ri, si, siprime;

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// quiver_to_graphviz returns a string representing the quiver, in dot format
// -- see [http://www.graphviz.org] for details of the format and how to run graphviz.
// label_strings is a sequence of strings, one for each vertex, that will be included in the
// output as labels for the vertices.  For example, the string "1" (including the quotes) will
// label the vertex with 1, whereas the string "none" (without the quotes) will omit that
// vertex label.  Setting the parameter vertex_labels to true will in addition label the
// vertices with their number.
function quiver_to_graphviz(Q, label_strings : vertex_labels:=false)
    // sanity check
    error if Type(Q) ne QuvFlg, "Q must be a quiver flag variety\n";
    // grab the adjacency matrix and dimension vector
    A := Q`A;
    dim := Q`r;
    // more sanity checks
    error if #label_strings eq 0, "Need a non-zero number of labels\n";
    error if not Universe(label_strings) eq Strings(), "The labels need to be strings\n";
    error if not #dim eq #label_strings, "Wrong number of labels\n";
    // start building the graphviz output
    lines := ["digraph G {", "rankdir=\"LR\";"];
    // add the vertices
    for i in [1..#dim] do
	if vertex_labels then
	    vertex_label := Sprintf(", xlabel=<<FONT COLOR=\"blue\" POINT-SIZE=\"10\">%o</FONT>>", i);
	else
	    vertex_label := "";
	end if;
	Append(~lines, Sprintf("%o [shape=circle, label=%o%o];", i-1, label_strings[i], vertex_label));
    end for;
    // add the edges
    for i in [1..#dim] do
	for j in [1..#dim] do
	    if A[i,j] eq 1 then
		Append(~lines, Sprintf("%o -> %o;", i-1, j-1));
	    elif A[i,j] gt 1 then
	    	Append(~lines, Sprintf("%o -> %o [label=\"%o\"];", i-1, j-1, A[i,j]));
	    end if;
	end for;
    end for;
    Append(~lines, "}");
    return Join(lines, "\n");
end function;


// quiver_flag_bundle_to_graphviz returns a string representing a
// bundle on a quiver flag variety, in dot format -- see
// [http://www.graphviz.org] for details of the format and how to
// run graphviz.  generalized_partitions is a generalized
// partition for each vertex except the source.  Setting the optional
// parameter vertex_labels to true will in addition label the vertices
// with their number.
function quiver_flag_bundle_to_graphviz(Q, generalized_partitions : vertex_labels:=false)
    // sanity check
    error if Type(Q) ne QuvFlg, "Q must be a quiver flag variety.";
    // now build up the label strings, vertex by vertex.  The source vertex is unlabelled.
    label_strings := ["\"\""];
    dimension_vector := Q`r;
    for i in [1..#dimension_vector-1] do
	this_generalized_partition := generalized_partitions[i];
	if ri(Q, i) eq 1 then
	    // this is a toric vertex.  Its label is a 1x1 box with an integer in it.
	    error if #this_generalized_partition ne 1, "Dimension mismatch.";
	    this_label := Sprintf("<<TABLE BORDER=\"0\" CELLBORDER=\"1\" CELLSPACING=\"0\" CELLPADDING=\"1\" COLOR=\"lightgrey\"><TR><TD>%o</TD></TR></TABLE>>", generalized_partitions[i][1]);
	else
	    this_label := "<<TABLE BORDER=\"0\" CELLBORDER=\"1\" CELLSPACING=\"2\" CELLPADDING=\"1\" COLOR=\"lightgrey\">";
	    for k in [1..ri(Q, i)] do
		this_label *:= "<TR>";
		for l in [1..Max(this_generalized_partition[1],1)] do
		    if l le this_generalized_partition[k] then
			this_label *:= "<TD BGCOLOR=\"black\" COLOR=\"black\"></TD>";
		    else
			this_label *:= "<TD></TD>";
		    end if;
		end for;
		this_label *:= "</TR>";
	    end for;
	    this_label *:= "</TABLE>>";
	end if;
	Append(~label_strings, this_label);
    end for;
    return quiver_to_graphviz(Q, label_strings : vertex_labels:=vertex_labels);
end function;

//////////////////////////////////////////////////////////////////////
// Intrinsics
//////////////////////////////////////////////////////////////////////

intrinsic Graphviz(Q::QuvFlg : vertex_labels:=false) -> MonStgElt
    {Returns a string representing the quiver flag variety Q, in dot format.  See [http:\/\/www.graphviz.org] for details of the format and how to run graphviz.}
    label_strings := [Sprintf("\"%o\"", d) : d in Q`r];
    return quiver_to_graphviz(Q, label_strings : vertex_labels:=vertex_labels);
end intrinsic;


intrinsic Graphviz(E::QuvFlgBun : vertex_labels:=false) -> SeqEnum
    {Returns a sequence of strings representing the quiver flag bundle E, in dot format.  See [http:\/\/www.graphviz.org] for details of the format and how to run graphviz.  The first string in the output represents the quiver flag variety on which E is defined.  The subsequent strings represent the irreducible summands in E (if any).}
    results := [Graphviz(E`Q : vertex_labels:=vertex_labels)];
    for gps in E`gps do
	Append(~results, quiver_flag_bundle_to_graphviz(E`Q, gps: vertex_labels:=vertex_labels));
    end for;
    return results;
end intrinsic;


intrinsic ViewWithGraphviz(Q::QuvFlg : vertex_labels:=false)
    {Views the quiver flag variety Q as a PDF file.  Requires graphviz -- see [http:\/\/www.graphviz.org].  Works on OS X only.}
    temp_file := Sprintf("%o/%o", GetTempDir(), Tempname(""));
    second_temp_file := Sprintf("%o/%o", GetTempDir(), Tempname(""));
    fprintf temp_file, Graphviz(Q : vertex_labels:=vertex_labels);
    command_string := Sprintf("dot -Tpdf %o -o %o && open %o", temp_file, second_temp_file, second_temp_file);
    System(command_string);
end intrinsic;


intrinsic ViewWithGraphviz(E::QuvFlgBun : vertex_labels:=false)
    {Views the quiver flag bundle E as a PDF file.  Requires graphviz -- see [http:\/\/www.graphviz.org].  Works on OS X only.}
    // pages holds the temporary filenames for the individual pages
    pages := [];
    for s in Graphviz(E: vertex_labels:=vertex_labels) do
	// build the input file
	temp_input_file := Sprintf("%o/%o", GetTempDir(), Tempname(""));
	fprintf temp_input_file, "%o\n", s;
        // build the PDF file for this page
	temp_output_file := Sprintf("%o/%o", GetTempDir(), Tempname(""));
	command_string := Sprintf("dot -Tpdf %o -o %o", temp_input_file, temp_output_file);
	System(command_string);
        // record the name of the page file
	Append(~pages, temp_output_file);
    end for;
    // combine the pages
    temp_output_file := Sprintf("%o/%o", GetTempDir(), Tempname(""));
    page_files := Join(pages, " ");
    command_string := Sprintf("\"/System/Library/Automator/Combine PDF Pages.action/Contents/Resources/join.py\" -o%o %o", temp_output_file, page_files);
    System(command_string);
    // open the PDF file
    command_string := Sprintf("open %o", temp_output_file);
    System(command_string);
end intrinsic;

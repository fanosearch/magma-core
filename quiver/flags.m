freeze;

//////////////////////////////////////////////////////////////////////
// flags.m
//////////////////////////////////////////////////////////////////////
// Special-case functions for quiver flag varieties that are
// actually flag manifolds
//////////////////////////////////////////////////////////////////////

declare attributes QuvFlg:
    is_flag; // true if and only if the quiver flag variety is a flag manifold

// Gr_cache is a data cache for irreducible vector bundles on Grassmannians.
Gr_cache := NewStore();

// is_flag returns true if and only if the quiver flag manifold Q is a flag manifold.  Q should be in normal form.
function is_flag(Q)
    error if not Q eq NormalForm(Q), "Q should be in normal form";
    A := Q`A;
    r := Q`r;
    // check that the dimension vector is strictly increasing
    for i in [3..#r] do
	if r[i] le r[i-1] then
	    return false;
	end if;
    end for;
    // check the adjacency matrix, row by row
    k := #r-1;
    if not (&and[IsZero(A[1,j]) : j in [1..k]] and A[1,k+1] gt r[#r]) then
	return false;
    elif not &and[IsZero(A[2,j]) : j in [1..k+1]] then
	return false;
    else
	for i in [3..k+1] do
	    if not (&and[IsZero(A[i,j]) : j in [1..k+1] | j ne i-1] and A[i,i-1] eq 1) then
		return false;
	    end if;
	end for;
    end if;
    return true;
end function;

// largest_rank_knapsack returns the rank of the largest
// representation of GL(r) such that the Chern class of the
// corresponding bundle on Gr(N,r) is strictly less than c.  (Note
// that this Chern class does not depend on N.)  If omit_tautological
// is true then the standard representation is not permitted as a
// summand.
function largest_rank_knapsack(r, c, omit_tautological)
    // we record the possible c1s and dimensions of irreducible factors.
    // for line bundles, the most efficient choice is det
    c1s := [Integers()|1];
    dims := [Integers()|1];
    // add the standard representation, unless we are supposed to omit it
    if not omit_tautological then
	Append(~c1s, 1);
	Append(~dims, r);
    end if;
    // all other irreps come from partitions of length gt 1
    for l in [2..r-1] do
	// l is the length of the partition indexing the Schur functor
	// wlog we can assume we have removed all tensor factors of
	// det, so l can be at most r-1
	// the smaller the size |p| of the partition, the more efficient. As
	// the determinant line bundle has size r and rank and first Chern clas
	// 1, we don't need to consider partitions with larger size.
	for p in &cat[Partitions(k,l) : k in [l..r-1]] do
	    d := NumberOfTableauxOnAlphabet(p,r);
	    Append(~dims, d);
	    Append(~c1s, d*(&+p)/r);
	end for;
    end for;
    // there is no point using smaller representations with the same c1
    temp_c1s := SetToSequence(SequenceToSet(c1s));
    temp_dims := [ Max([dims[i] : i in [1..#dims] | c1s[i] eq x]) : x in temp_c1s ];
    // if we have c1 = 1 and rank e then there is no point in using c1 = k and rank le k*e
    max_ratio := temp_dims[Index(temp_c1s,1)];
    these_c1s := [Integers()| 1];
    these_dims := [Integers()| max_ratio];
    for i in [1..#temp_dims] do
    	if temp_dims[i]/temp_c1s[i] gt max_ratio then
    	    Append(~these_c1s, temp_c1s[i]);
    	    Append(~these_dims, temp_dims[i]);
    	end if;
    end for;
    // now we want to find non-negative integers a1,...,aN that maximise
    // a1*d1+...+aN*dN subject to the constraint a1*c1_1+...+aN*c1_N = c-1
    C := PositiveQuadrant(#these_c1s);
    h := Dual(Ambient(C))!these_c1s;
    d := Dual(Ambient(C))!these_dims;
    P := ConeToPolyhedron(C) meet HyperplaneToPolyhedron(h, c-1);
    return Max([d*v : v in Points(P)]);
end function;

// make_partition returns the partition with m[i] columns of length i
function make_partition(m)
    error if not Type(m) eq SeqEnum and Universe(m) eq Integers(), "m should be an integer sequence";
    p := [Integers()|];
    for i in [1..#m] do
        p cat:= [i : j in [1..m[i]]];
    end for;
    Reverse(~p);
    assert IsPartition(p);
    return ChangeUniverse(TransposePartition(p), Integers());
end function;


// Gr_irreducibles returns the set of partitions that define irreducible vector
// bundles on Gr(N, r) with first Chern class exactly c and dimension exactly d.
function Gr_irreducibles(r, c, d)
    // check the cache
    ok, cache := StoreIsDefined(Gr_cache, "cache");
    if ok then
       ok, result := IsDefined(cache, [r,c,d]);
       if ok then
           return result;
       end if;
   else
       cache := AssociativeArray();
   end if;
    // bounds[i] will be a bound on the number of columns of length i that can occur
    // in the partition
    bounds := [Integers()|];
    for i in [1..r] do
        // we try k columns of length i
        k := 0;
        repeat
            k := k + 1;
            p := [ k : j in [1..i] ];
            dim := NumberOfTableauxOnAlphabet(p, r);
            c1 := k*i/r*dim;
        until dim gt d or c1 gt c;
        Append(~bounds, k-1);
    end for;
    // initialise an associative array to hold our results
    // we will cache everything that we can -- that is, the result for all
    // c1 le c and all dim le d
    result := AssociativeArray();
    for c1 in [1..c] do
        for dim in [1..d] do
            result[[r,c1,dim]] := {PowerSequence(Integers())|};
        end for;
    end for;
    // now walk over the possible partitions and check the c1 and dimension constraints
    for x in CartesianProduct([ [0..b] : b in bounds ]) do
        if IsZero([y : y in x]) then
            continue;
        end if;
        p := make_partition([Integers()| n : n in x]);
        dim := NumberOfTableauxOnAlphabet(p, r);
        c1 := (&+p)/r*dim;
        if c1 le c and dim le d then
            Include(~result[[r,c1,dim]], p);
        end if;
    end for;
    // update the cache and return
    for k in Keys(result) do
        if not IsDefined(cache, k) then
            cache[k] := result[k];
        end if;
    end for;
    StoreSet(Gr_cache, "cache", cache);
    return result[[r,c,d]];
end function;


// largest_rank_flag returns the rank of the largest representation on
// the flag manifold with dimension vector dim and first Chern class
// strictly less, entrywise, than c1.  We assume that dim and c1 are
// sorted such that dim is in increasing order; this will be the case
// if the quiver involved is in normal form.  If omit_last_W is true
// then the tautological bundle on the last vertex (which is W_1 in
// Craw's notation) will be omitted from the calculation.
function largest_rank_flag(dim, c1, omit_last_W)
    result := 0;
    for i in [1..#dim-1] do
	// the dimension vector starts with r_0=1.  We want r=r_i.
	r := dim[i+1];
	c := c1.i;
	// the largest dimension, for a given Chern class, is where
	// the bundle is a direct sum of factors, each of which is
	// a Schur functor applied to a single tautological bundle.
	if r eq 1 then
	    if i eq #dim-1 and omit_last_W then
		// we use sums of the line bundle det(W_i)^{\otimes 2}
		result +:= Floor((c-1)/2);
	    else
		// we use sums of the line bundle det(W_i)
		result +:= c-1;
	    end if;
	else
	    omit_tautological := omit_last_W and (i eq #dim-1);
	    result +:= largest_rank_knapsack(r,c,omit_tautological);
	end if;
    end for;
    return result;
end function;

//////////////////////////////////////////////////////////////////////
// Intrinsics
//////////////////////////////////////////////////////////////////////

intrinsic FlagManifold(rs::SeqEnum[RngIntElt]) -> QuvFlg
{The partial flag variety Fl(r1,r2,...,rk) as a quiver flag variety, where rs = [r1,r2,...,rk].}
    require &and[r gt 0 : r in rs]: "rs must be a sequence of positive integers";
    require &and[rs[i] gt rs[i+1] : i in [1..#rs-1]]: "rs must be a strictly decreasing sequence";
    require #rs gt 1: "rs must contain at least two elements";
    // build the quiver flag variety
    n := rs[1];
    dim := [1] cat rs[2..#rs];
    A := ZeroMatrix(Integers(), #dim, #dim);
    A[1,2] := n;
    for i in [2..#rs-1] do
        A[i,i+1] := 1;
    end for;
    // return the normal form, discarding the vertex permutation
    // information returned by NormalForm
    NF := NormalForm(QuiverFlagVariety(A,dim));
    return NF;
end intrinsic;

intrinsic FlagManifold(r1::Any,...) -> QuvFlg
{The partial flag variety Fl(r1,r2,...,rk) as a quiver flag variety.}
    // sanity checks
    require &and[ x in Integers() : x in r1]: "The argument must be a sequence of integers";
    // convert the arguments to a sequence and hand off
    rs := [Integers()| x : x in r1 ];
    return FlagManifold(rs);
end intrinsic;

intrinsic IsFlagManifold(Q::QuvFlg) -> BoolElt
{True if and only if Q is a flag manifold.}
    if not assigned Q`is_flag then
        Q`is_flag := is_flag(NormalForm(Q));
    end if;
    return Q`is_flag;
end intrinsic;

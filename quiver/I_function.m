freeze;

//////////////////////////////////////////////////////////////////////
// I_function.m
//////////////////////////////////////////////////////////////////////
// Functions for computing the I-function for quiver flag varieties
// and quiver flag zero loci
//////////////////////////////////////////////////////////////////////

declare attributes QuvFlg:
    D_A,    // the toric divisors of the abelianisation A
    period_cone,    // the cone of divisors relevant to the I-function
                    // computation
    derivs, // a sequence containing the number of partial derivatives
            // that we need to take to compute the I-function
    all_derivs; // a CartesianProduct containing sequences that index
                // all multiple partial derivatives of order at most
                // derivs

declare attributes QuvFlgBun:
    E_A,    // the toric divisors defined by the Abelianisation of E
    I_cache;    // the cache of I-function coefficients

import "cohomology.m": toric_divisors_A, compute_bundle_data, positive_root_bundles;

// define a store to hold generalised harmonic numbers
harm_cache := NewStore();

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// Returns the generalised harmonic number \sum_{i=1}^n 1/i^m
function gen_harm(m,n)
    // Sanity checks
    error if not m in Integers() or not n in Integers(), "Both arguments must be integers";
    error if m le 0 or n le 0, "Both arguments must be positive";
    // grab the cache
    must_update := false;
    ok, cache  := StoreIsDefined(harm_cache, "cache");
    if not ok then
        cache := AssociativeArray();
        must_update := true;
    end if;
    // check the cache
    key := [m,n];
    ok, r := IsDefined(cache, key);
    if not ok then
        must_update := true;
        // do the computation
        if n eq 1 then
            r := 1;
        else
            r := gen_harm(m, n-1) + 1/n^m;
        end if;
        // store the result
        cache[key] := r;
    end if;
    // update the cache, if necessary
    if must_update then
        StoreSet(harm_cache, "cache", cache);
    end if;
    return r;
end function;

// Returns the multiple partial derivative, with exponents u, of
// logGamma(1+D) - logGamma(1+D+D*d)
// u is assumed to be non-zero
function deriv_of_log_gamma_term(D, d, u, shift)
    // is the term identically zero?
    if IsZero(D*d+shift) then
        return Zero(Rationals());
    end if;
    // is the coefficient of this derivative zero?
    if &and[IsZero(D.i) or IsZero(u[i]) : i in [1..#u]] then
        return Zero(Rationals());
    end if;
    // Calculate the coefficient
    c := &*[Rationals()| (D.i)^u[i] : i in [1..#u]];
    // Which polygamma function is this?
    m := &+u-1;
    return (-1)^(m+1)*c*Factorial(Integers()!m)*gen_harm(m+1,D*d+shift);
end function;

// Returns the multiple partial derivative, with exponents u, of the logarithm
// of the gamma function term (i.e. ratio of factorials) that occurs in the
// I-function
function deriv_term(positive_D, negative_D, E_A, d, u)
    // handle the trivial case
    if IsZero(u) then
        return One(Rationals());
    end if;
    return &+[Rationals()| deriv_of_log_gamma_term(D, d, u, 0) : D in positive_D ] - &+[Rationals()| deriv_of_log_gamma_term(E, d, u, 0) : E in E_A ] - &+[Rationals()| deriv_of_log_gamma_term(-D, d, u, -1) : D in negative_D ];
end function;

// Returns the ratio of factorials in the I-function
function gamma_term(positive_D, negative_D, E_A, d)
    numerator_factorials := [Integers()| E*d : E in E_A] cat [Integers()| -1-D*d : D in negative_D];
    denominator_factorials := [Integers()| D*d : D in positive_D];
    // cancel complete factorials
    new_nfs := [Integers()|];
    Sort(~denominator_factorials);
    Reverse(~denominator_factorials);
    for n in numerator_factorials do
        // skip 0!
        if n eq 0 then
            continue;
        end if;
        idx := Index(denominator_factorials, n);
        if idx eq 0 then
            // we can't cancel it
            Append(~new_nfs, n);
        else
            Remove(~denominator_factorials, idx);
        end if;
    end for;
    // cancel incomplete factorials
    Sort(~new_nfs);
    Reverse(~new_nfs);
    if #denominator_factorials gt #new_nfs then
        flip := true;
        short := new_nfs;
        long := denominator_factorials;
    else
        flip := false;
        short := denominator_factorials;
        long := new_nfs;
    end if;
    short_prod := Rationals()!1;
    long_prod := Rationals()!1;
    for i in [1..#short] do
        s := short[i];
        l := long[i];
        if s gt l then
            short_prod *:= &*[l+1..s];
        else
            long_prod *:= &*[s+1..l];
        end if;
    end for;
    long_prod *:= &*[ Factorial(long[i]) : i in [#short+1..#long] ];
    if flip then
        return short_prod/long_prod;
    end if;
    return long_prod/short_prod;
end function;

// Given an integer vector u defining the exponents of a multiple partial
// derivative, return the sequence of sequences of such exponents expressing all
// ways of partitioning this multiple partial derivative into blocks.
function split_derivatives(u)
    // Handle the trivial case
    if IsZero(u) then
        return [[u]] ;
    end if;
    n := &+u;
    u_seq := &cat[ [i : j in [1..u[i]]] : i in [1..#u]];
    result := [];
    for S in Partitions({1..n}) do
        seq_from_partition := [ [ u_seq[i] : i in T ] : T in S ];
        Append(~result, [ [Multiplicity(x, i) : i in [1..#u]] : x in seq_from_partition ]);
    end for;
    return result;
end function;

// Returns the contribution to the I-function at degree d from the multiple
// partial derivative, with exponents u, of the gamma term.
function deriv_of_gamma(positive_D, negative_D, E_A, d, u)
    // create a cache to hold partial derivatives of log of the gamma term
    pds := AssociativeArray(PowerSequence(Integers()));
    // do the computation
    contrib := Zero(Rationals());
    for S in split_derivatives(u) do
        block_derivs := [Rationals()|];
        for v in S do
            ok, deriv_v := IsDefined(pds, v);
            if not ok then
                deriv_v := deriv_term(positive_D, negative_D, E_A, d, v);
                pds[v] := deriv_v;
            end if;
            Append(~block_derivs, deriv_v);
        end for;
        contrib +:= &*block_derivs;
    end for;
    return gamma_term(positive_D, negative_D, E_A, d) * contrib;
end function;

// Returns an associative array c such that c[[i_1,i_2,..., i_r]] is the
// coefficient of H_1^{i_1} H_2^{i_2} ... H_r^{i_r} omega * &* negative_D where
// omega is the antisymmetrising term.  Here dim_vector is the dimension vector
// of Q and d is the degree (in the Mori cone of the Abelianisation of Q) that
// we are considering.
function omega_coeffs(dim_vector, negative_D, d)
    r := Remove(dim_vector, 1);
    R := PolynomialRing(Rationals(), &+r);
    omega := One(R);
    offset := 0;
    for ri in r do
        omega *:= &*[R| R.(offset+i)-R.(offset+j)+d.(offset+i)-d.(offset+j) : j in [i+1..ri], i in [1..ri] ];
        offset +:= ri;
    end for;
    cs, es := CoefficientsAndExponents(omega * &*[R| &+[R| D.i*R.i : i in [1..Rank(R)]] : D in negative_D]);
    result := AssociativeArray();
    for i in [1..#cs] do
        result[es[i]] := cs[i];
    end for;
    return result;
end function;

// Returns the contribution to the I-function from degree d, where d is assumed
// to lie in Q`period_cone. D_A is the sequence of toric divisors on the
// Abelianisation, E_A is the sequence of line bundles defined by the
// Abelianisation of E, dim_vector is the dimension vector of Q, positive roots
// are the line bundles on the Abelianisation corresponding to positive roots,
// derivs is the sequence containing the exponents of the partial derivatives
// that we need to take, and all_derivs is a Cartesian product containing
// exponents of all smaller partial derivatives.
function I_coefficient_summand(D_A, E_A, dim_vector, positive_roots, derivs, all_derivs, d)
    // record which toric divisors are positive and negative on d
    positive_D := [D : D in D_A | D*d gt 0 ];
    negative_D := [D : D in D_A | D*d lt 0 ];
    // compute the coefficients from the antisymmetrising term
    c := omega_coeffs(dim_vector, negative_D, d);
    // compute the sign
	if IsEven(&+[Integers()|E*d : E in positive_roots]) then
	    sign := 1;
	else
	    sign := -1;
	end if;
    sign *:= (-1)^(&+[Integers()| 1+D*d : D in negative_D]);
    // do the sum over partial derivatives
    result := 0;
    for x in all_derivs do
        u := [x[i] : i in [1..#x]];
        v := [derivs[i] - u[i] : i in [1..#u]];
        ok, coeff := IsDefined(c,v);
        if ok then
            result +:= sign * deriv_of_gamma(positive_D, negative_D, E_A, d, u) * coeff / &*[Integers()| Factorial(r) : r in u];
        end if;
    end for;
    return result;
end function;


// Returns D_A, E_A where D_A is the sequence of toric divisors on the
// Abelianisation of Q and E_A is the sequence of toric divisors defining the
// Abelianisation of E.
function divisors_and_line_bundles(E)
    Q := QuiverFlagVariety(E);
    D_A := toric_divisors_A(Q);
    if not assigned E`line_bundles_A then
        compute_bundle_data(E);
    end if;
    E_A := E`line_bundles_A;
    return D_A, E_A;
end function;

// Returns d, all_d where d is a sequence giving the exponents of the partial
// derivatives that we need to take in order to compute the I-function, and
// all_d is a CartesianProduct containing exponents of all partial derivatives
// of order at most all_d.
function all_derivatives(Q)
    r := Remove(DimensionVector(Q),1);
    if not assigned Q`derivs then
        Q`derivs := &cat[ [x-j : j in [1..x]] : x in r ];
        Q`all_derivs := CartesianProduct([[0..d] : d in Q`derivs]);
    end if;
    return Q`derivs, Q`all_derivs;
end function;


// Returns the n-th coefficient of the I-function for the quiver flag zero locus
// defined by E. Assumes that n is non-negative.
function I_coefficient(E,n)
    Q := QuiverFlagVariety(E);
    // grab the toric divisors and line bundles on the Abelianisation
    D_A, E_A := divisors_and_line_bundles(E);
    // grab the first Chern class on the Abelianisation
    minus_KX := E`minus_KX_A;
    // grab the dimension vector
    dim_vector := DimensionVector(Q);
    // grab the degrees relevant to the computation of this coefficient
    MC := MoriCone(Abelianisation(Q));
    relevant_degrees := PointProcess(ConeToPolyhedron(MC) meet HyperplaneToPolyhedron(minus_KX, n));
     // grab the derivatives to take
    derivs, all_derivs := all_derivatives(Q);
    // grab the line bundles on A corresponding to positive roots
    positive_roots := positive_root_bundles(Q);
    // do the computation
    result := Zero(Rationals());
    for d in relevant_degrees do
        result +:= I_coefficient_summand(D_A, E_A, dim_vector, positive_roots, derivs, all_derivs, d);
    end for;
    return result;
end function;

//////////////////////////////////////////////////////////////////////
// Intrinsics
//////////////////////////////////////////////////////////////////////

intrinsic IFunctionCoefficient(E::QuvFlgBun, n::RngIntElt) -> FldRatElt
{The n-th coefficient of the I-function for the quiver flag zero locus defined by E.}
    // Get the trivial cases out of the way
    if n lt 0 then
        return 0;
    elif n eq 0 then
        return 1;
    end if;
    // Initialise the cache, if necessary
    if not assigned E`I_cache then
        E`I_cache := AssociativeArray(Integers());
    end if;
    // Check the cache
    ok, c := IsDefined(E`I_cache, n);
    if not ok then
        // Do the computation and store the result
        c := I_coefficient(E,n);
        E`I_cache[n] := c;
    end if;
    return c;
end intrinsic;

intrinsic IFunctionCoefficient(Q::QuvFlg, n::RngIntElt) -> FldRatElt
{The n-th coefficient of the I-function for the quiver flag variety Q.}
    return IFunctionCoefficient(ZeroBundle(Q), n);
end intrinsic;

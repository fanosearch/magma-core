freeze;

//////////////////////////////////////////////////////////////////////
// bundle.m
//////////////////////////////////////////////////////////////////////
// Functions related to bundles on quiver flag varieties
//////////////////////////////////////////////////////////////////////

// we consider bundles E on a quiver flag variety Q such that E is
// a direct sum of bundles, each of which is of the form
//
// L \otimes \bigotimes_{i : r_i > 1} S^{\alpha_i} W_i       (1)
//
// where L is a nef line bundle on Q, [r_0,...,r_rho] is the
// dimension vector of Q, W_i is the i-th tautological bundle on
// Q, and S^{\alpha_i} is a non-negative Schur functor.
//
// Each summand is represented as a sequence of generalised partitions
// (\alpha_1,...,\alpha_\rho) where, if r_i gt 1, \alpha_i is a
// partition with at most r_i parts and, if r_i eq 1, \alpha_i is
// a length-one integer sequence [ m ]; note that m may be negative.
//
// We allow the case where there are no summands (the empty bundle).
// Note that not every length-one sequence of negative integers is
// allowed, because we demand that the line bundle factor L in (1) is
// nef.
//
// Internally, we represent each summand by a sequence
// [\alpha_1,...,\alpha_\rho] where, if r_i gt 1, \alpha_i is a
// weakly decreasing sequence of non-negative integers of length r_i       (2)
// (that is, \alpha_i may have trailing zeroes) and, if r_i eq 1,
// \alpha_i is a length-1 sequence of integers.
//
// Two bundles are considered to be equal if and only if they are
// defined on the same quiver flag variety and have the same
// summands up to reordering.

/*
Lemma: bundles E of the form (1) are convex

Proof: E is convex if and only if the splitting type of f^*(E), where
[f:C->E is a stable map from an irreducible rational curve, is O(a_1)
\oplus...\oplus O(a_k) with all the a_i \geq -1.  This condition is
preserved by tensoring with a nef line bundle. QED
*/


declare type QuvFlgBun;
declare attributes QuvFlgBun:
	Q,                 // the underlying quiver
	gps,               // the sequence of generalised partitions representing the bundle
	rank,              // the rank of the bundle
	c1,                // the first Chern class of the bundle
	hash_value;        // the hash value of the bundle

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// validate_gps returns true if and only if gps is a sequence of
// generalised partitions satisfying (2) above.  If true, it also returns
// the sequence of generalised partitions obtained by padding the
// elements of gps with zeroes so that the i-th part has length
// num_parts[i].  Here num_parts[i] is r_i above, so is the (i+1)st entry
// of the dimension vector.
function validate_gps(num_parts, gps)
    // sanity checks
    error if Type(num_parts) ne SeqEnum or
	  Universe(num_parts) ne Integers() or
	  not (&and[x gt 0: x in num_parts]), "The first argument must be a sequence of positive integers.";
    error if Type(gps) ne SeqEnum or
	  Universe(gps) ne PowerSequence(Integers()), "The second argument must be a sequence of integer sequences.";
    // check the lengths
    if #num_parts ne #gps then
	return false, _;
    end if;
    // check the generalised partitions
    padded := [PowerSequence(Integers())|];
    for i in [1..#num_parts] do
	ri := num_parts[i];
	gp := gps[i];
	if ri eq 1 then
	    // we should have a length-1 integer sequence, or a length-zero integer sequence if we are going to zero-pad it
	    if #gp eq 0 then
		Append(~padded, [0]);
	    elif #gp eq 1 then
		Append(~padded, gp);
	    else
		return false, _;
	    end if;
	else
	    // we should have a weakly decreasing sequence of non-negative integers of length at most r_i
	    if #gp gt ri then
		return false, _;
	    end if;
	    sorted := Reverse(Sort(gp));
	    if sorted ne gp then
		return false, _;
	    end if;
	    Append(~padded, gp cat [0 : i in [1..ri-#gp]]);
	end if;
    end for;
    return true, padded;
end function;

// validate_line_bundle returns true if and only if the line bundle L on Q defined, via (2) above, by the sequence of generalized partitions gps is nef.  The pair (Q, gps) is assumed to have been validated; that is, gps is assumed to determine a well-defined vector bundle on Q.
function validate_line_bundle(Q, gps)
    // grab basic geometric data about Q
    NC := NefCone(Q);
    r := DimensionVector(Q);
    dims := r[2..#r];
    rho := #dims;
    // sanity check: the partitions are zero-padded (if necessary) to have length dim[i]
    assert &and[#gps[i] eq dims[i] : i in [1..#dims]];
    // build the line bundle.  L is the tensor product of all powers of det(W_i)
    L_seq := [Min(p) : p in gps];
    L := Ambient(NC)!L_seq;
    return L in NC;
end function;

//////////////////////////////////////////////////////////////////////
// Creation of quiver flag varieties
//////////////////////////////////////////////////////////////////////

intrinsic QuiverFlagBundle(Q::QuvFlg, S::SeqEnum : skip_bundle_validation:=false) -> QuvFlgBun
    {The bundle E on the quiver flag variety Q determined by the sequence S of generalised partitions.  Suppose that the dimension vector of Q is [r_0,...,r_rho].  Each element of S should be a sequence [\alpha_1,...,\alpha_rho] where, if r_i gt 1, \alpha_i is a weakly decreasing sequence of non-negative integers of length at most r_i (representing a partition with at most r_i parts) and, if r_i eq 1, \alpha_i is a length-1 sequence of integers.  The bundle E is then the tensor product of Schur powers of the tautological vector bundles on Q: E = \otimes_i S^\alpha_i(W_i).  We demand that this is of the form L \otimes (\otimes_(i: r_i gt 1) S^\alpha_i(W_i)) where the line bundle L is nef.  The quiver Q should be in normal form: see NormalForm.  The optional parameter skip_bundle_validation can be set to true to turn off some (usually cheap) internal sanity checks.
}
    // Q should be in normal form
    require Q eq NormalForm(Q): "The quiver Q should be in normal form.";
    // the universe of S should be correct
    ok, S := CanChangeUniverse(S, PowerSequence(PowerSequence(Integers())));
    require ok: "S should be a sequence of sequences of integer sequences.";
    // validate the entries in S
    r := DimensionVector(Q);
    dims := r[2..#r];
    padded_S := [PowerSequence(PowerSequence(Integers()))|];
    for gps in S do
	// check that the sequence gps has the correct lengths, and zero-pad the entries if necessary
	ok, padded_gps := validate_gps(dims,gps);
	require ok: Sprintf("The element %o of S does not define a vector bundle on Q.", gps);
	Append(~padded_S, padded_gps);
	// check that the line bundle L defined by gps is nef
	if not skip_bundle_validation then
	    ok := validate_line_bundle(Q, padded_gps);
	    require ok: Sprintf("The line bundle on Q defined by the element %o of S is not nef.", gps);
	end if;
    end for;
    // construct the quiver flag bundle
    E := New(QuvFlgBun);
    E`Q := Q;
    E`gps := Sort(padded_S);
    return E;
end intrinsic;


intrinsic Summands(E::QuvFlgBun) -> SeqEnum[QuvFlgBun]
    {The irreducible direct summands of E.}
    return [QuiverFlagBundle(E`Q, [gps] : skip_bundle_validation:=true) : gps in E`gps];
end intrinsic;


intrinsic ZeroBundle(Q::QuvFlg) -> QuvFlgBun
    {The trivial bundle of rank zero on Q.}
    return QuiverFlagBundle(Q,[PowerSequence(PowerSequence(Integers()))|]);
end intrinsic;


intrinsic TrivialBundle(Q::QuvFlg, r::RngIntElt) -> QuvFlgBun
    {The trivial bundle on Q of rank r.}
    require r ge 0: "The rank r must be non-negative.";
    // make the trivial line bundle
    rho := #DimensionVector(Q)-1;
    gps := [ [Integers()|] : i in [1..rho]];
    triv_line := QuiverFlagBundle(Q, [gps]);
    // return the appropriate number of copies
    result := ZeroBundle(Q);
    for i in [1..r] do
	result +:= triv_line;
    end for;
    return result;
end intrinsic;


intrinsic TautologicalBundle(Q::QuvFlg, i::RngIntElt) -> QuvFlgBun
    {The i-th tautological vector bundle on the quiver flag variety Q.}
    rho := #DimensionVector(Q)-1;
    require i ge 1 and i le rho: "The index i must be in the range [1..rho] where rho is the Picard rank of Q.";
    gps := [ [] : j in [1..rho]];
    gps[i] := [1];
    return QuiverFlagBundle(Q, [gps]);
end intrinsic;


//////////////////////////////////////////////////////////////////////
// Functions to display a quiver flag variety
//////////////////////////////////////////////////////////////////////

intrinsic Print(E::QuvFlgBun)
    {Display the quiver flag bundle E.}
    printf "The quiver flag bundle on (%o) defined by the sequences of generalized partitions %o.", E`Q, E`gps;
end intrinsic;

//////////////////////////////////////////////////////////////////////
// Hash and comparison functions
//////////////////////////////////////////////////////////////////////

intrinsic Hash(E::QuvFlgBun) -> RngIntElt
    {The hash value of the quiver flag bundle E.}
    if not assigned E`hash_value then
        E`hash_value:= Hash([[[Hash(E`Q)]]] cat E`gps);
    end if;
    return E`hash_value;
end intrinsic;


intrinsic 'eq'(E1::QuvFlgBun, E2::QuvFlgBun) -> BoolElt
{True iff the quiver flag bundles E1 and E2 are equal.}
    return E1`Q eq E2`Q and E1`gps eq E2`gps;
end intrinsic;

//////////////////////////////////////////////////////////////////////
// Direct sum
//////////////////////////////////////////////////////////////////////

intrinsic '+'(E1::QuvFlgBun, E2::QuvFlgBun) -> QuvFlgBun
{The direct sum of the quiver flag bundles E1 and E2.}
    require E1`Q eq E2`Q: "The quiver flag bundles must have the same underlying quiver flag variety.";
    return QuiverFlagBundle(E1`Q, E1`gps cat E2`gps : skip_bundle_validation:=true);
end intrinsic;

intrinsic '*'(n::RngIntElt, E::QuvFlgBun) -> QuvFlgBun
{The direct sum of n copies of the quiver flag bundle E.}
    require n ge 0: "The number of bundles must be non-negative.";
    if n eq 0 then
	return ZeroBundle(E`Q);
    else
	return QuiverFlagBundle(E`Q, &cat[E`gps : k in [1..n]] : skip_bundle_validation:=true);
    end if;
end intrinsic;

intrinsic '*'(E::QuvFlgBun, n::RngIntElt) -> QuvFlgBun
{The direct sum of n copies of the quiver flag bundle E.}
    return n*E;
end intrinsic;

//////////////////////////////////////////////////////////////////////
// Rank
//////////////////////////////////////////////////////////////////////

intrinsic Rank(E::QuvFlgBun) -> RngIntElt
    {The rank of the quiver flag bundle E.}
    if not assigned E`rank then
	r := DimensionVector(E`Q);
	dims := r[2..#r];
	E`rank:= &+[Integers()|&*[Integers()|NumberOfTableauxOnAlphabet(gps[i], dims[i]) : i in [1..#dims] | dims[i] gt 1] : gps in E`gps];
    end if;
    return E`rank;
end intrinsic;

//////////////////////////////////////////////////////////////////////
// simple properties
//////////////////////////////////////////////////////////////////////

intrinsic IsIrreducible(E::QuvFlgBun) -> BoolElt
    {True if and only if the quiver flag bundle E is irreducible.}
    return #E`gps le 1;
end intrinsic;

intrinsic IsTrivial(E::QuvFlgBun) -> BoolElt
    {True if and only if the quiver flag bundle E is trivial.}
    return &and[IsZero(gps) : gps in E`gps];
end intrinsic;

intrinsic IsZero(E::QuvFlgBun) -> BoolElt
    {True if and only if the quiver flag bundle E is the trivial bundle of rank zero.}
    return #E`gps eq 0;
end intrinsic;

intrinsic IsSummand(E::QuvFlgBun, F::QuvFlgBun) -> BoolElt
    {True if and only if the quiver flag bundle E is a summand of F.}
    require E`Q eq F`Q: "The quiver flag bundles must have the same underlying quiver flag variety.";
    E_summands := Summands(E);
    F_summands := Summands(F);
    for Ei in E_summands do
	j := Index(F_summands, Ei);
	if j eq 0 then
	    return false;
	end if;
	Remove(~F_summands, j);
    end for;
    return true;
end intrinsic;

//////////////////////////////////////////////////////////////////////
// first Chern class
//////////////////////////////////////////////////////////////////////

intrinsic FirstChernClass(E::QuvFlgBun) -> TorLatElt
    {The first Chern class of E, as an element of the Picard lattice of the underlying quiver flag variety.}
    L := PicardLattice(E`Q);
    if not assigned E`c1 then
	if IsZero(E) then
	    E`c1 := Zero(L);
	elif IsIrreducible(E) then
	    d := Rank(E);
	    r := DimensionVector(E`Q);
	    dim := r[2..#r];
	    rho := #dim;
	    gps := E`gps[1];
	    E`c1 := L![ d*(&+gps[i])/dim[i] : i in [1..rho]];
	else
	    E`c1 := &+[L|FirstChernClass(F) : F in Summands(E)];
	end if;
    end if;
    return E`c1;
end intrinsic;

//////////////////////////////////////////////////////////////////////
// the underlying quiver flag variety
//////////////////////////////////////////////////////////////////////

intrinsic QuiverFlagVariety(E::QuvFlgBun) -> QuvFlg
    {The quiver flag variety on which E is defined.}
    return E`Q;
end intrinsic;

//////////////////////////////////////////////////////////////////////
// the generalised partitions defining E
//////////////////////////////////////////////////////////////////////

intrinsic Partitions(E::QuvFlgBun) -> SeqEnum[SeqEnum[SeqEnum[RngIntElt]]]
    {The sequence of sequences of generalised partitions that defines E.}
    return E`gps;
end intrinsic;

//////////////////////////////////////////////////////////////////////
// Tests and examples
//////////////////////////////////////////////////////////////////////

/*
Q := QuiverFlagVariety(Matrix([[0,2,3,0],[0,0,0,0],[0,0,0,3],[0,0,0,0]]),[1,1,2,4]);
gp1 := [[2],[2,1],[1]];
gp2 := [[1],[1,1],[1,1,1,1]];
gp3 := [[-1],[1,1],[1,1,1,1]];
gp4 := [[-1],[5,0,0],[1,1,1,1]];
E1 := QuiverFlagBundle(Q,[gp1]);   // succeeds
E2 := QuiverFlagBundle(Q,[gp2]);   // succeeds
E3 := QuiverFlagBundle(Q,[gp3]);   // fails the nef-ness check
E4 := QuiverFlagBundle(Q,[gp4]);   // fails the "defines a vector bundle on Q" check
E := QuiverFlagBundle(Q,[gp1,gp2,gp1]);   // succeeds
Summands(E) eq [E1,E2,E1];    // false because of sorting the summands
Summands(E) eq [E2,E1,E1];    // true
SequenceToSet(Summands(E)) eq {E1,E2};    // true
Rank(E1);  // 8
Rank(E2);  // 1
Rank(E);   // 17
IsIrreducible(E1); // true
IsIrreducible(E2); // true
IsIrreducible(E);  // false
IsTrivial(E1); // false
IsTrivial(E2); // false
IsTrivial(E);  // false
IsZero(E1); // false
IsZero(E2); // false
IsZero(E);  // false
IsZero(TrivialBundle(Q,3)); // false
IsZero(TrivialBundle(Q,0)); // true
IsTrivial(TrivialBundle(Q,3)); // true
IsTrivial(TrivialBundle(Q,0)); // true
E1 + ZeroBundle(Q) eq E1; // true;
ZeroBundle(Q) + E1 eq E1; // true;
ZeroBundle(Q) + ZeroBundle(Q) eq ZeroBundle(Q); // true
E1 + TrivialBundle(Q,1) eq E1; // false;
TrivialBundle(Q,1) + E1 eq E1; // false;
TrivialBundle(Q,1) + TrivialBundle(Q,1) eq TrivialBundle(Q,1); // false
TrivialBundle(Q,1) + TrivialBundle(Q,1) eq TrivialBundle(Q,2); // true
FirstChernClass(E1);  // (16, 12, 2)
FirstChernClass(E2);  // (1, 1, 1)
FirstChernClass(E);  // (33, 25, 5)
IsSummand(E1, E2); // false
IsSummand(E1, E1); // true
IsSummand(E1, E); // true
IsSummand(E2, E); // true
IsSummand(E1+E1, E); // true
IsSummand(E1+E1+E2, E); // true
IsSummand(E2+E2, E); // false
IsSummand(ZeroBundle(Q),E); // true
IsSummand(E, ZeroBundle(Q)); // false
*/

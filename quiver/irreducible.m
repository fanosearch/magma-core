freeze;

//////////////////////////////////////////////////////////////////////
// irreducible.m
//////////////////////////////////////////////////////////////////////
// Functions for finding irreducible quiver flag bundles on a quiver
// flag variety Q that are not too positive
//////////////////////////////////////////////////////////////////////

import "../ci/ci.m": non_negative_decompositions, first_Chern_classes_for_nef_ci;
import "flags.m": Gr_irreducibles;

declare attributes QuvFlg:

	possible_first_Chern_classes,        // the set of nef classes n such that -K_Q - n is ample on Q

	irreducible_vector_bundles_max_rank, // the maximum rank for which we have computed irreducible
	                                     // vector bundles

	irreducible_vector_bundles;          // the set of all irreducible quiver flag bundles E on Q of
	                                     // the form (*) below, such that they satisfy the positivity
	                                     // condition (**) below and that the rank of E is at most
	                                     // irreducible_vector_bundles_max_rank


/*

Let Q be a quiver flag variety with dimension vector [r_0,...,r_rho].
We would like to search for vector bundles on Q that (a) are convex;
and (b) come from representations of GL(r_1) x ... x GL(r_rho).

We don't understand how to characterise convexity, but we do know a
good supply of convex vector bundles.  Globally generated vector
bundles are convex, so in particular the vector bundles on the quiver
flag variety corresponding to non-negative Schur powers of the
tautological vector bundles are convex.

In view of the Lemma in the file bundle.m, we seek vector bundles of
the form:

E = L \otimes \bigotimes_{i : r_i > 1} S^{\alpha_i} W_i      (*)

where \alpha_i are partitions and L is a nef line bundle.  Later on we
will need direct sums of these bundles, but in this file we
concentrate on irreducible vector bundles, hence seek precisely vector
bundles on Q of the form (*) such that

-K_X - c_1(E)                                              (**)

is ample on X.

Suppose that E is of the form (*) and that the second tensor factor
has first Chern class c and rank d.  Then we need

-K_X - d c_1(L) - c

to be ample on X.  Since (d-1) c_1(L) + c is nef on X, this implies
that -K_X - c_1(L) is ample on X.  That is, c_1(L) lies in the set A
consisting of nef classes n such that -K_X - n is ample on X.

So now, for each element c1_L in the set A, we seek partitions
\alpha_i such that

-K_X - d c1_L - c

is ample on X and that the vector bundle has rank strictly smaller
than the dimension of the quiver flag variety.

For details of the internal representation of bundles (*), see the
file bundle.m

 */



//////////////////////////////////////////////////////////////////////
// local functions
//////////////////////////////////////////////////////////////////////


// compute_possible_first_Chern_classes returns the set of nef classes
// n such that -K_Q - n is ample on Q
function compute_possible_first_Chern_classes(Q)
    // sanity checks
    error if not Type(Q) eq QuvFlg, "Q must be a quiver flag variety";
    // compute the data, if necessary
    if not assigned Q`possible_first_Chern_classes then
	t := Cputime();
	vprintf Fanosearch: "compute_possible_first_Chern_classes: Starting...";
	Q`possible_first_Chern_classes := first_Chern_classes_for_nef_ci(NefCone(Q), -CanonicalClass(Q));
	vprintf Fanosearch: "done (%os)\n", Cputime(t);
    end if;
    return Q`possible_first_Chern_classes;
end function;

// tuples_of_partitions returns the set of tuples of partitions of the entries of N with maximum numbers of parts given by the entries of K
function tuples_of_partitions(N, K)
    // sanity checks
    error if not (Type(N) eq SeqEnum and Universe(N) eq Integers() and &and[n ge 0 : n in N]), "N must be a sequence of non-negative integers";
    error if not (Type(K) eq SeqEnum and Universe(K) eq Integers() and &and[k ge 0: k in K]), "K must be a sequence of non-negative integers";
    error if not #N eq #K, "N and K must have the same length";
    // do the computation
    partitions_list := [ ];
    for i in [1..#N] do
	if N[i] eq 0 then
	    Append(~partitions_list, [[]]);
	else
	    Append(~partitions_list, &cat[Partitions(N[i],k) : k in [1..K[i]]]);
	end if;
    end for;
    return CartesianProduct(partitions_list);
end function;

// irreducible_vector_bundles_with_specified_data returns the set of irreducible quiver flag bundles E on Q of the form (*) such that the rank of E is d, the first Chern class of L is c1_L, and the first Chern class of the other tensor factor of E is c.
function irreducible_vector_bundles_with_specified_data(Q, c1_L, c, d)
    r := DimensionVector(Q);
    dim := r[2..#r];
    rho := #dim;
    c_seq := Eltseq(c);
    c1_L_seq := Eltseq(c1_L);
    // sanity check: the entries of c corresponding to vertices
    // with dimension 1 are zero
    assert &and[ c_seq[i] eq 0 : i in [1..rho] | dim[i] eq 1];
    // the remaining entries of c are d |\alpha_i|/r_i where
    // |\alpha_i| is the length of the partition \alpha_i and
    // the dimension vector is (1,r_1,...,r_rho)
    // we use this to reconstruct the lengths of the partitions
    lengths := [ dim[i]*c_seq[i]/d : i in [1..rho] ];
    ok, lengths := CanChangeUniverse(lengths, Integers());
    if not ok then
	// there are no such quiver flag bundles
	return {PowerStructure(QuvFlgBun)|};
    end if;
    // walk through the all possible partitions of these lengths,
    // checking if they give a vector bundle of dimension d
    results := {PowerStructure(QuvFlgBun)|};
    for p in tuples_of_partitions(lengths, dim) do
    	// does this give the correct dimension for the representation?
    	if d ne &*[NumberOfTableauxOnAlphabet(p[i], dim[i]) : i in [1..rho]] then
    	    // the dimensions don't match, so skip this partition
    	    continue;
    	end if;
    	// zero-pad the partitions where necessary, and tensor with c1_L
    	gps := [];
    	for i in [1..rho] do
    	    padded_partition := p[i] cat [0 : j in [1..dim[i]-#(p[i])]];
    	    Append(~gps, [ v + c1_L_seq[i] : v in padded_partition]);
    	end for;
    	// record the result and continue
    	E := QuiverFlagBundle(Q,[gps]);
    	assert FirstChernClass(E) eq c + d*c1_L;
    	Include(~results, E);
    end for;
    return results;
end function;

// compute_more_irreducible_vector_bundles_flag returns data exactly as for
// compute_more_irreducible_vector_bundles below, under the additional
// hypothesis that Q is a flag manifold in normal form.  Any irreducible here is
// of the form
//
// det(W_1)^{a_1} \otimes ... \otimes det(W_{i-1})^{a_{i-1}) \otimes
// S^\alpha(W_i)
//
// with a_i ge 0 for all i.  This is before taking normal form, so in this
// context it is
//
//  S^\alpha(W_i) \otimes \bigotimes_{j=i+1}^\rho det(W_j)^{a_j}
//
// for some i ge 1, with a_j ge 0 for all j.
function compute_more_irreducible_vector_bundles_flag(Q, min_rank, max_rank)
    vprintf Fanosearch: "compute_more_irreducible_vector_bundles: Computing irreducible vector bundles (min_rank=%o, max_rank=%o), flag case...", min_rank, max_rank;
    t := Cputime();
    if min_rank eq 1 then
        results := {TrivialBundle(Q,1)};
    else
        results:= {PowerStructure(QuvFlgBun)|};
    end if;
    // grab basic geometric data about Q
    NC := NefCone(Q);
    minus_K := -CanonicalClass(Q);
    r := DimensionVector(Q);
    dim := r[2..#r];
    rho := #dim;
    for i in [1..rho] do
        // get the first Chern class bound on \alpha
        for c in [1..minus_K.i-1] do
            for d in [min_rank..max_rank] do
                for alpha in Gr_irreducibles(dim[i],c,d) do
                    // fill in tail
                    bounds := [Integers()| Ceiling(minus_K.j/d)-1 : j in [i+1..rho]];
                    tails := CartesianProduct([PowerSequence(Integers())| [0..b] : b in bounds ]);
                    for t in tails do
                        tail := [x : x in t];
                        p := [ PowerSequence(Integers()) | [Integers()| ] : j in [1..i-1] ];
                        Append(~p, alpha);
                        for j in [1..#tail] do
                            Append(~p, [ tail[j] : k in [1..dim[i+j]] ]);
                        end for;
                        E := QuiverFlagBundle(Q,[p]);
                        assert IsInInterior(minus_K - FirstChernClass(E), NC);
                        assert Rank(E) eq d;
                        Include(~results, E);
                    end for;
                end for;
            end for;
        end for;
    end for;
    vprintf Fanosearch: "done (%os)\n", Cputime(t);
    return results;
end function;

// compute_more_irreducible_vector_bundles returns the set containing
// all irreducible quiver flag bundles E on Q of the form (*) that
// satisfy the positivity condition (**) such that the rank of E lies
// in [min_rank..max_rank].  It does not update the cache
// Q`irreducible_vector_bundles
function compute_more_irreducible_vector_bundles(Q, min_rank, max_rank)
    if IsFlagManifold(Q) then
        return compute_more_irreducible_vector_bundles_flag(Q, min_rank, max_rank);
    end if;
    results:= {PowerStructure(QuvFlgBun)|};
    // grab basic geometric data about Q
    NC := NefCone(Q);
    minus_K := -CanonicalClass(Q);
    r := DimensionVector(Q);
    dim := r[2..#r];
    rho := #dim;
    // last_orthant is the positive orthant in the subspace
    // corresponding to vertices of dimension gt 1
    last_orthant := Cone([Ambient(NC)| Basis(Ambient(NC))[i] : i in [1..rho] | dim[i] gt 1]);
    // find all nef classes n such that -K - n is ample on Q
    possible_first_Chern_classes := compute_possible_first_Chern_classes(Q);
    // now walk over possible first Chern classes and ranks
    vprintf Fanosearch: "compute_more_irreducible_vector_bundles: Computing irreducible vector bundles (min_rank=%o, max_rank=%o)...", min_rank, max_rank;
    t := Cputime();
    for c1_L in possible_first_Chern_classes do
    	for d in [min_rank..max_rank] do
    	    if not IsInInterior(minus_K - d*c1_L, NC) then
    		continue;
    	    end if;
    	    // we now find the possible values for c (see the discussion at the top of the file)
    	    candidates := first_Chern_classes_for_nef_ci(NC, minus_K - d*c1_L);
    	    for c in candidates do
    		// skip any points that are not in the last orthant
    		if not c in last_orthant then
    		    continue;
    		end if;
    		// add the corresponding irreducible vector bundles to results
    		results join:= irreducible_vector_bundles_with_specified_data(Q, c1_L, c, d);
    	    end for;
    	end for;
    end for;
    vprintf Fanosearch: "done (%os)\n", Cputime(t);
    return results;
end function;


//////////////////////////////////////////////////////////////////////
// public functions
//////////////////////////////////////////////////////////////////////

// irreducible_vector_bundles returns the set containing all
// irreducible quiver flag bundles E on Q of the form (*) that satisfy
// the positivity condition (**)
function irreducible_vector_bundles(Q, max_rank)
    t := Cputime();
    vprintf Fanosearch: "irreducible_vector_bundles (max_rank:=%o): Sanity checks...", max_rank;
    // sanity checks
    error if not Type(Q) eq QuvFlg, "Q must be a quiver flag variety";
    error if not max_rank in Integers() and max_rank ge 0, "max_rank must be a non-negative integer";
    // if Q is not Fano, there is nothing to do
    if (not IsFano(Q)) or max_rank eq 0 then
    	return {PowerStructure(QuvFlgBun)|};
    end if;
    vprintf Fanosearch: "done (%os)\n", Cputime(t);
    // if the cache does not exist yet, initialise it
    if not assigned Q`irreducible_vector_bundles then
    	Q`irreducible_vector_bundles_max_rank := 0;
    	Q`irreducible_vector_bundles := {PowerStructure(QuvFlgBun)|};
    end if;
    // did we already compute everything that we need?
    if max_rank eq Q`irreducible_vector_bundles_max_rank then
    	return Q`irreducible_vector_bundles;
    elif max_rank lt Q`irreducible_vector_bundles_max_rank then
    	return {E : E in Q`irreducible_vector_bundles | Rank(E) le max_rank};
    end if;
    // do the computation and update the cache
    Q`irreducible_vector_bundles join:= compute_more_irreducible_vector_bundles(Q, Q`irreducible_vector_bundles_max_rank+1,max_rank);
    Q`irreducible_vector_bundles_max_rank := max_rank;
    return Q`irreducible_vector_bundles;
end function;

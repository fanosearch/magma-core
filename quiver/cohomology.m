freeze;

//////////////////////////////////////////////////////////////////////
// cohomology.m
//////////////////////////////////////////////////////////////////////
// Functions for computing cohomological data for quiver flag
// varieties and quiver flag zero loci
//////////////////////////////////////////////////////////////////////

declare attributes QuvFlg:
    Chow_A,         // the Chow ring of the abelianisation A
    H2_basis_A,     // a sequence containing the first Chern
                    // classes of the tautological line bundles
                    // on A, regarded as elements of Chow_A
    point_class_A,  // the element of Chow_A that is Poincar\'e
                    // dual to a point on A
    omega_bundles,  // a sequence containing the classes, in the
                    // Picard lattice of A, of the line bundles
                    // corresponding to positive roots
    order_of_W,     // the order of the Weyl group
    Pic_A_to_Chow_A,    // the map from the Picard lattice of A to
                        // Chow_A
    toric_divisors_A,   // the toric divisors on the Abelianisation
    degree;         // the degree of Q


declare attributes QuvFlgBun:
    line_bundles_A,     // a sequence containing the first Chern
                        // classes of the line bundles that sum to
                        // give the abelianised bundle E_T,
                        // regarded as elements of Chow_A where A is
                        // the abelianisation of the underlying
                        // quiver
    minus_KX_A,         // the anticanonical class of the zero locus
                        // of a generic section of E, as an element
                        // of Chow_A
    degree,             // the degree of the zero locus of a generic
                        // section X of E
    Todd_term,          // Todd class of
                        // T_A - E_T - Omega - Omega^*   (*)
                        // as an element of Chow_A.  Here Omega is
                        // the sum of line bundles on A defined by
                        // the positive roots.  Quantity (*) is a
                        // K-theory lift of TX.
    Euler_term,     // the Euler class of E_T + Omega + Omega^*
    num_components, // the number of components of X
    Hilbert_coeffs, // the first few coefficients of the Hilbert
                    // series for -K_X
    Euler_num;      // the Euler number (=the topological Euler
                    // characteristic) of X

//////////////////////////////////////////////////////////////////////
// Local functions
//////////////////////////////////////////////////////////////////////

// Returns true if and only if x lies in the cone spanned by the
// elements of D indexed by S.  D should be a subset of {1..#D}.  D
// should be a sequence of elements of a toric lattice L, which also
// contains x.
function covers(S, D, x)
    return x in Cone([Universe(D)|D[i] : i in S]);
end function;

// Returns ch, RR, phi where:
// ch is the Chow ring for the toric quiver flag variety A with weight
// matrix wts
// R is the polynomial ring on the toric divisors of A; and
// phi is the map from R to ch (which is a quotient ring of R).
// Note that this is specific to toric quiver flag varieties, rather
// than toric varieties in general, because we assume that the
// stability condition (called "ample" below) is [1,1,...,1].
function toric_Chow_ring(wts)
    // Sanity check
    error if IsZero(wts), "Need a non-zero weight matrix";
    // we take the columns of the weight matrix, without repetitions
    // keeping track of how many repeats of each column there are
    D := [Universe(Columns(wts))|];
    repeats := [Integers()|];
    for col in Columns(wts) do
	idx := Index(D, col);
	if idx eq 0 then
	    Append(~D, col);
	    Append(~repeats, 1);
	else
	    repeats[idx] +:=1;
	end if;
    end for;
    // Create the ambient ring
    ChangeUniverse(~D, ToricLattice(NumberOfRows(wts)));
    R:=PolynomialRing(Rationals(), #D, "grevlex");
    // we need to mod out by the ideal "a" of non-cones
    // we can read this off the subsets of S that do not cover the ample cone
    ample := Universe(D)![1 : i in [1..NumberOfRows(wts)]];
    nonCovers := &cat[[S : S in Subsets({1..#D}, j) | not covers(S, D, ample)] : j in [0..#D]];
    a:=Ideal([R | &*[R | (R.i)^repeats[i] : i in [1..#D] | not i in S] : S in nonCovers]);
    // Create the ideal "b" of kernels
    bs:=Basis(Kernel(Matrix(D)));
    b:=Ideal([R | &+[b[i] * R.i : i in [1..#D]] : b in bs]);
    // Return the Chow ring, and the embedding map
    ch, emb:=R / (a + b);
    RR := PolynomialRing(Rationals(), NumberOfColumns(wts), "grevlex");
    phi := hom<RR->ch | [ emb(R.Index(D,col)): col in Columns(wts)]>;
    return ch, RR, phi;
end function;

// Returns B, [pt] where B is a sequence of elements of the Chow ring
// of the abelianised quiver A and [pt] is the class Poincar\'e dual
// to a point in A.  The sequence B consists of the first Chern
// classes of the tautological line bundles on A.  ch, R, phi here
// should be the result of calling toric_Chow_ring on Gradings(A).
function basis_and_point_class(A, ch, R, phi)
    wts := Matrix(Gradings(A));
    // build the basis B
    choice_of_columns := [];
    for i in [1..NumberOfRows(wts)] do
	Append(~choice_of_columns, Min([j : j in [1..NumberOfColumns(wts)] | wts[i][j] eq 1]));
    end for;
    subMatrix := Submatrix(wts, [1..NumberOfRows(wts)], choice_of_columns);
    // the inverse of this matrix expresses the standard basis in terms of the toric divisors
    assert Determinant(subMatrix) ne 0;
    invSubMatrix := subMatrix^(-1);
    // express the standard basis vectors for H^2 in the Chow ring
    B := [ &+[ invSubMatrix[k][i]*phi(R.(choice_of_columns[k])) : k in [1..NumberOfRows(wts)]] : i in [1..NumberOfRows(wts)]];
    // build the class Poincar\'e dual to a point
    class_of_a_point := &*[ch| phi(R.i) : i in [1..Rank(R)] | not i in choice_of_columns];
    return B, class_of_a_point;
end function;

// Returns the sequence of elements of the Picard lattice of the
// abelianisation A of Q that correspond to positive roots.
function positive_root_bundles(Q)
    A := Abelianization(Q);
    result := [PicardLattice(A)|];
    standard_basis := Basis(PicardLattice(A));
    dim := DimensionVector(Q);
    for i in [2..#dim] do
	start_index := &+dim[2..i-1] + 1;
	end_index := &+dim[2..i];
	result cat:= [standard_basis[j] - standard_basis[k]: j in [start_index..end_index], k in [start_index..end_index] | j lt k];
    end for;
    return result;
end function;

// Computes cohomological data about the abelianisation A of Q and
// caches it on Q.
procedure compute_Chow_data(Q)
    A := Abelianization(Q);
    // compute the Chow ring
    t := Cputime();
    vprintf Fanosearch: "Computing Chow ring...";
    ch, R, phi := toric_Chow_ring(Matrix(Gradings(A)));
    Q`Chow_A := ch;
    vprintf Fanosearch: "done (%os)\n", Cputime(t);
    // compute the standard basis for H^2, and the class Poincar\'e dual to a point
    t := Cputime();
    vprintf Fanosearch: "Computing standard basis for H^2...";
    basis, Q`point_class_A := basis_and_point_class(A, ch, R, phi);
    Q`H2_basis_A := basis;
    vprintf Fanosearch: "done (%os)\n", Cputime(t);
    Q`omega_bundles := positive_root_bundles(Q);
    // compute the order of the Weyl group
    Q`order_of_W := &*[Integers()| Factorial(r) : r in DimensionVector(Q)];
    // build a function to map the Picard lattice of A into the Chow ring of A
    Q`Pic_A_to_Chow_A := function(x)
	error if not x in PicardLattice(A), "x should be an element of the Picard lattice of the Abelianisation";
	x_as_seq := Eltseq(x);
	return &+[ch| basis[i]*x_as_seq[i] : i in [1..#basis]];
    end function;
end procedure;

// Computes cohomological data about the quiver flag bundle E and
// caches it on E
procedure compute_bundle_data(E)
    t := Cputime();
    vprintf Fanosearch: "Computing abelianizations of the bundles...";
    //////////////////////////////////////////////////////////////////////
    // recall that E_T is a direct sum of line bundles.  We now compute
    // the first Chern classes of these line bundles.  Let Q denote the
    // quiver flag variety over which E is a quiver flag bundle.
    //
    // Our group is G = GL_{r_1} \times \cdots \times GL_{r_rho}, with
    // diagonal torus T = T^{r_1} \times \cdots \times T^{r_rho}.
    // Our representation of G (and thus of T) is specified by a sequence
    // (\alpha_1,...,\alpha_rho) of generalized partitions:
    //
    // \otimes_{i=1}^{rho} S^{\alpha_i} [std_i]
    //
    // where std_i is the standard representation of GL_{r_i}.
    //
    // The standard representation std_i defines a representation of T
    // (via projection to T^{r_i}), and this splits as the direct sum of
    // 1-dimensional irreducible representations std_{i,j} where j runs
    // from 1 to r_i.  These representations induce the tautological line
    // bundles over the abelianization; the first Chern classes of these
    // line bundles are the divisor classes L[k], where L = Q`H2_basis_A
    // was computed using compute_Chow_data().  Here k runs from
    // 1+sum_{j<i} r_j to sum_{j \leq i} r_j.  We will refer to L[k] as
    // L[i,j].
    //
    // S^{\alpha_i} [std_i] breaks into one-dimensional representations
    // of T, which are determined by the Schur polynomial
    // s_{\alpha_i}(x_1,...,x_{r_i}) as the evaluation
    // s_{\alpha_i}([std_{i,1}], ..., [std_{i,r_i}]) in the representation
    // ring of T.  (That is, product is tensor product of representations
    // and sum is direct sum of representations.)
    // So the vector bundle on the abelianization induced by our
    // representation is
    //
    // E_T = \otimes_{i=1}^{rho} s_{\alpha_i}(W_{i,1}, ..., W_{i,r_i})
    //
    // where the W_{i,j}, 1 \leq i \leq rho, 1 \leq j \leq r_i are the
    // tautological line bundles on the abelianization.  We see that E_T
    // is the direct sum of line bundles, determined as follows.
    // Take the product of Schur polynomials
    // s_{\alpha_i}(x_{i,1},...,x_{i,r_i}) over i
    // Express this product as a sum of monomials in x_{i,j}.  Each
    // monomial corresponds to a line bundle factor, and the monomial
    // \prod_{i,j} x_{i,j}^{m_{i,j}}
    // (which automatically has coefficient 1) corresponds to the line
    // bundle with first Chern class
    // \sum_{i,j} m_{i,j} L[i,j]
    //
    // We compute these first Chern classes for each irreducible factor
    // in our vector bundle.
    //////////////////////////////////////////////////////////////////////
    // grab the dimension vector and the abelianisation of Q
    Q := E`Q;
    dim := DimensionVector(Q);
    A := Abelianisation(Q);
    // build the line bundles L[i,j] described in the comment above
    standard_basis := Basis(PicardLattice(A));
    function L(i, j)
	error if j lt 1 or j gt dim[i+1], "j is out of range";
	return standard_basis[(&+dim[2..i])+j];
    end function;
    // build the line bundles we want
    line_bundles := [PicardLattice(A)|];
    for p in E`gps do
	line_bundle_factors := [];
	for i in [1..#dim-1] do
	    alpha_i := p[i];
	    r_i := #alpha_i;
	    if r_i eq 1 then
		// the i-th factor has r_i eq 1
		Append(~line_bundle_factors, [alpha_i[1]*L(i,1)] );
	    elif IsZero(alpha_i) then
		Append(~line_bundle_factors, [Zero(PicardLattice(A))] );
	    else
		// we strip off the trailing zeroes, as Magma requires
		// partitions to be sequences of positive integers
		alpha_i := [xx : xx in alpha_i | xx gt 0];
		// we compute the Schur polynomial directly, as a sum
		// over semi-standard Young tableaux
		contents := [Content(tab) : tab in TableauxOfShape(alpha_i,r_i)];
		padded_contents := [ xx cat [0 : i in [#xx+1..r_i]] : xx in contents];
		Append(~line_bundle_factors, [ &+[PicardLattice(A)|exponent[j]*L(i,j) : j in [1..r_i]] : exponent in padded_contents ]);
	    end if;
	end for;
	line_bundles cat:= [&+[yy : yy in xx] : xx in CartesianProduct(line_bundle_factors)];
    end for;
    E`line_bundles_A := line_bundles;
    vprintf Fanosearch: "done (%os)\n", Cputime(t);
    E`minus_KX_A := -CanonicalClass(A) - &+line_bundles;
end procedure;

// returns the following data, after computing and caching it if
// necessary:
//
// ch                the Chow ring of the abelianisation A of the
//                   quiver underlying E
// phi               the map from the Picard lattice of A to ch
// class_of_a_point  the element of ch Poincar\'e dual to a point
//                   in A
// order_of_W        the order of the Weyl group
// line_bundles      the first Chern classes of the line bundles
//                   on A defined by the abelianisation of E
// omega_bundles     the first Chern classes of the line bundles
//                   on A corresponding to positive roots
// dim_X             the dimension of the zero locus X of a
//                   generic section of E
// minus_KX          the anticanonical class of X, as an element
//                   of ch
function get_cached_data(E)
    if not assigned E`Q`Chow_A then
	compute_Chow_data(E`Q);
    end if;
    if not assigned E`line_bundles_A then
	compute_bundle_data(E);
    end if;
    ch := E`Q`Chow_A;
    phi := E`Q`Pic_A_to_Chow_A;
    class_of_a_point := E`Q`point_class_A;
    order_of_W := E`Q`order_of_W;
    line_bundles := E`line_bundles_A;
    omega_bundles := E`Q`omega_bundles;
    dim_X := Dimension(E`Q) - Rank(E);
    minus_KX := E`minus_KX_A;
    return ch, phi, class_of_a_point, order_of_W, line_bundles, omega_bundles, dim_X, minus_KX;
end function;

// computes the degree of the zero locus of a generic section of the
// quiver flag bundle E
function compute_degree(E)
    // grab cached data (or compute it)
    ch, phi, class_of_a_point,
    order_of_W, line_bundles,
    omega_bundles, dim_X, minus_KX := get_cached_data(E);
    t := Cputime();
    vprintf Fanosearch: "Computing the degree...\n";
    vprintf Fanosearch: "order_of_W: %o\n", order_of_W;
    // do the computation
    c_1_power := phi(minus_KX)^dim_X;
    integrand := c_1_power *
		 &*[ch| phi(E) : E in line_bundles] *
		 &*[ch | -phi(rho)^2 : rho in omega_bundles] / order_of_W;
    vprintf Fanosearch: "integrand: %o\n", integrand;
    vprintf Fanosearch: "class_of_a_point: %o\n", class_of_a_point;
    degree := ExactQuotient(integrand, class_of_a_point);
    return degree;
end function;

// computes the Todd class x/(1-e^(-x)), discarding terms of degree K+1 and higher in x
function Todd(x, K)
    return &+[Parent(x)| (-1)^i*BernoulliNumber(i)*x^i/Factorial(i) : i in [0..K]];
end function;

// computes the Chern class e^x, discarding terms of degree K+1 and higher in x.
function Chern(x, K)
    return &+[Parent(x)| x^i/Factorial(i) : i in [0..K]];
end function;

// returns the toric divisors on the toric quiver variety A, as
// elements of the PicardLattice of A
function toric_divisors_A(Q)
    if not assigned Q`toric_divisors_A then
        A := Abelianisation(Q);
        Q`toric_divisors_A := ChangeUniverse(ColumnSequence(Matrix(Gradings(A))),PicardLattice(A));
    end if;
    return Q`toric_divisors_A;
end function;

// computes the Todd term described above (see the comments where we
// define the new attributes for QuvFlgBun)
function compute_Todd(E)
    if not assigned E`Todd_term then
	t := Cputime();
	vprintf Fanosearch: "Computing the Todd class and Euler class...";
	A := Abelianization(E`Q);
	dim_A := Dimension(A);
	// grab cached data (or compute it)
	ch, phi, _, _, line_bundles,
	omega_bundles, _, _ := get_cached_data(E);
	E`Todd_term := &*[ch| Todd(phi(D), dim_A) : D in toric_divisors_A(E`Q)] /
		       &*[ch| Todd(phi(E), dim_A) : E in line_bundles] /
		       &*[ch| Todd(phi(rho), dim_A) : rho in omega_bundles] /
		       &*[ch| Todd(phi(-rho), dim_A) : rho in omega_bundles] ;
        vprintf Fanosearch: "done (%os)\n", Cputime(t);
    end if;
    return E`Todd_term;
end function;

// computes the Euler term described above (see the comments where we
// define the new attributes for QuvFlgBun)
function compute_Euler(E)
    if not assigned E`Euler_term then
	t := Cputime();
	vprintf Fanosearch: "Computing the Euler class...";
	// grab cached data (or compute it)
	ch, phi, _, _, line_bundles,
	omega_bundles, _, _ := get_cached_data(E);
	E`Euler_term := &*[ch| phi(E) : E in line_bundles] *
			&*[ch | -phi(rho)^2 : rho in omega_bundles];
        vprintf Fanosearch: "done (%os)\n", Cputime(t);
    end if;
    return E`Euler_term;
end function;

// computes h_0(-k K_X) using Grothendieck--Riemann--Roch, where X is
// the zero locus of a generic section of the quiver flag bundle E
function compute_Hilbert_coefficient(E, k)
    t := Cputime();
    vprintf Fanosearch: "Computing h_0(-%o K_X)...\n", k;
    // grab cached data (or compute it)
    _, phi, class_of_a_point, order_of_W, _, _, _, minus_KX := get_cached_data(E);
    Todd_term := compute_Todd(E);
    Euler_term := compute_Euler(E);
    // do the computation
    top_degree := TotalDegree(class_of_a_point);
    Chern_term := Chern(phi(k*minus_KX), top_degree);
    integrand := Chern_term * Todd_term * Euler_term / order_of_W;
    top_degree_part := &+[Parent(integrand) | xx : xx in Terms(integrand) | TotalDegree(xx) eq top_degree];
    vprintf Fanosearch: "done (%os)\n", Cputime(t);
    return ExactQuotient(top_degree_part, class_of_a_point);
end function;

// computes the Euler number of X, where X is the zero locus of a
// generic section of E.  This is the Euler class of TX integrated
// over X; it coincides with the Euler characteristic of X as a
// topological space.
function compute_Euler_number(E)
    A := Abelianization(E`Q);
    // grab cached data (or compute it)
    ch, phi, class_of_a_point, order_of_W,
    line_bundles, omega_bundles, _, _ := get_cached_data(E);
    Todd_term := compute_Todd(E);
    Euler_term := compute_Euler(E);
    total_Chern_term := &*[ch| 1+ phi(D) : D in toric_divisors_A(E`Q)] /
			&*[ch| 1+ phi(E) : E in line_bundles] /
			&*[ch| 1 + phi(rho): rho in omega_bundles] /
			&*[ch| 1 + phi(-rho) : rho in omega_bundles] ;
    integrand := total_Chern_term * Euler_term / order_of_W;
    top_degree := TotalDegree(class_of_a_point);
    top_degree_part := &+[ch | xx : xx in Terms(integrand) | TotalDegree(xx) eq top_degree];
    return ExactQuotient(top_degree_part, class_of_a_point);
end function;


//////////////////////////////////////////////////////////////////////
// Intrinsics
//////////////////////////////////////////////////////////////////////

intrinsic Degree(E::QuvFlgBun) -> RngIntElt
    {The degree of the zero locus of a generic section of E.}
    if not assigned E`degree then
	E`degree := compute_degree(E);
    end if;
    return E`degree;
end intrinsic;

intrinsic Degree(Q::QuvFlg) -> RngIntElt
    {The degree of Q.}
    if not assigned Q`degree then
	Q`degree := compute_degree(ZeroBundle(Q));
    end if;
    return Q`degree;
end intrinsic;

intrinsic NumberOfComponents(E::QuvFlgBun) -> RngIntElt
    {The number of components of the zero locus of a generic section of E.}
    if not assigned E`num_components then
	// the number of components is h^0(X,O_X)
	E`num_components := compute_Hilbert_coefficient(E, 0);
    end if;
    return E`num_components;
end intrinsic;

intrinsic HilbertCoefficients(E::QuvFlgBun, l::RngIntElt) -> SeqEnum
    {The first l+1 coefficients of the Hilbert series for the anticanonical class of X, where X is the zero locus of a generic section of E.}
    // sanity check
    require l ge 0: "l must be non-negative";
    // if necessary, initialise the cache
    if not assigned E`Hilbert_coeffs then
	E`Hilbert_coeffs := [ Integers()| ];
    end if;
    // do we need to compute more terms?
    N := #E`Hilbert_coeffs;
    if N lt l+1 then
	E`Hilbert_coeffs cat:= [ compute_Hilbert_coefficient(E,k) : k in [N..l] ];
    end if;
    return E`Hilbert_coeffs[1..l+1];
end intrinsic;

intrinsic EulerNumber(E::QuvFlgBun) -> RngIntElt
    {The Euler number of the zero locus X of a generic section of E.  This is the integral of the Euler class of TX over X; it coincides with the Euler characteristic of X as a topological space.}
    if not assigned E`Euler_num then
	E`Euler_num := compute_Euler_number(E);
    end if;
    return E`Euler_num;
end intrinsic;

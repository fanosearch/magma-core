freeze;

//////////////////////////////////////////////////////////////////////
// abelianization.m
//////////////////////////////////////////////////////////////////////
// Functions to compute the abelianization X//T of a quiver flag
// variety X//G.
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// local functions
//////////////////////////////////////////////////////////////////////

// is_toric_quiver returns true if and only if Q is a toric quiver
// flag variety
function is_toric_quiver(Q)
    error if not Type(Q) eq QuvFlg, "Q must be a quiver flag variety";
    return SequenceToSet(Q`r) eq {Integers()|1};
end function;

// unique_source returns the unique source of the quiver with
// adjacency matrix A.  A runtime error will occur if there is not
// exactly one source.
function unique_source(A)
    sources := [j : j in [1..NumberOfColumns(A)] | &and[A[i,j] eq 0 : i in [1..NumberOfRows(A)]]];
    error if not #sources eq 1, "Something is wrong.  The quiver should have a unique source.";
    return sources[1];
end function;

// weight_matrix_of_toric_quiver returns the weight matrix defining a toric quiver flag variety.  A is the adjacency matrix, dim is the dimension vector, as usual; so in particular dim must be (1,1,...,1) otherwise an error is raised.
function weight_matrix_of_toric_quiver(A, dim)
    error if not SequenceToSet(dim) eq {1}, "The quiver must be toric\n";
    // record the source vertex
    source := unique_source(A);
    // build the weight matrix, column by column
    weight_cols := [];
    for i in [1..NumberOfRows(A)] do
	for j in [1..NumberOfColumns(A)] do
	    col := [0 : l in [1..NumberOfColumns(A)]];
	    col[i] := -1;
	    col[j] := 1;
	        for k in [1..A[i,j]] do
		    Append(~weight_cols, col);
		end for;
	    end for;
	end for;
    // now get the weight matrix by transposing and removing the row corresponding to the source vertex
    weight_matrix := Transpose(Matrix(weight_cols));
    weight_matrix := Submatrix(weight_matrix, [i : i in [1..NumberOfRows(weight_matrix)] | i ne source], [1..NumberOfColumns(weight_matrix)]);
    return weight_matrix;
end function;

//////////////////////////////////////////////////////////////////////
// Intrinsics
//////////////////////////////////////////////////////////////////////

intrinsic Abelianization(Q::QuvFlg) -> QuvFlg
    {Returns the abelianization of Q, that is, the toric quiver flag variety A=X\/\/T where Q=X\/\/G.  The dimension vector of A is (1,1,...,1).  There are r_i vertices of A for each vertex of Q, where (r_1,...,r_rho) is the dimension vector of Q.  There is one arrow in A from i to j for each arrow in Q between the corresponding vertices i' and j'.}
    if not assigned Q`abelianization then
	dim := Q`r;
	ab_vertices := &cat [ [i : k in [1..dim[i]]] : i in [1..#dim] ];
	ab_dim := [1 : i in [1..#ab_vertices]];
	ab_A := ZeroMatrix(Integers(),#ab_vertices, #ab_vertices);
	for i in [1..#ab_vertices] do
	    for j in [1..#ab_vertices] do
		ab_A[i,j] := Q`A[ab_vertices[i], ab_vertices[j]];
	    end for;
	end for;
	A := QuiverFlagVariety(ab_A, ab_dim);
	A`abelianization := A;
	Q`abelianization := A;
    end if;
    return Q`abelianization;
end intrinsic;

intrinsic Abelianisation(Q::QuvFlg) -> QuvFlg
    {Returns the abelianisation of Q, that is, the toric quiver flag variety A=X\/\/T where Q=X\/\/G.  The dimension vector of A is (1,1,...,1).  There are r_i vertices of A for each vertex of Q, where (r_1,...,r_rho) is the dimension vector of Q.  There is one arrow in A from i to j for each arrow in Q between the corresponding vertices i' and j'.}
    return Abelianization(Q);
end intrinsic;

intrinsic Gradings(Q::QuvFlg) -> SeqEnum
    {A sequence of sequences giving the gradings defining the toric quiver flag variety Q as a toric variety.}
    // sanity check
    error if not is_toric_quiver(Q), "The argument must be a toric quiver flag variety.";
    return RowSequence(weight_matrix_of_toric_quiver(Q`A, Q`r));
end intrinsic;

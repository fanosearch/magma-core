freeze;

//////////////////////////////////////////////////////////////////////
// basic.m
//////////////////////////////////////////////////////////////////////
// Basic functions for working with quiver flag varieties
//////////////////////////////////////////////////////////////////////

import "quiver.m": directed_graph_from_adjacency_matrix;

//////////////////////////////////////////////////////////////////////
// local functions
//////////////////////////////////////////////////////////////////////

// ri returns r_i in Craw's notation, that is, the (i+1)-st entry in
// the dimension vector of Q
function ri(Q, i)
    return Q`r[i+1];
end function;

// nij returns n_{ij} in Craw's notation, that is, the number of
// arrows in Q from vertex i to vertex j, where we index vertices
// from zero
function nij(Q, i, j)
    return Q`A[i+1,j+1];
end function;

// si returns s_i in Craw's notation, that is, the weighted sum of
// the number of arrows in to the i-th vertex of the quiver, where
// we index the vertices from zero
function si(Q, i)
    return &+[Integers()| ri(Q, j) * nij(Q, j, i) : j in [0..NumberOfVertices(Q)-1]];
end function;

// siprime returns s'_i in Craw's notation, that is, the weighted sum
// of the number of arrows out of the i-th vertex of Q, where we index
// the vertices from zero
function siprime(Q, i)
    return &+[Integers()| nij(Q, i, j) * ri(Q, j) : j in [0..NumberOfVertices(Q)-1]];
end function;

//////////////////////////////////////////////////////////////////////
// Intrinsics
//////////////////////////////////////////////////////////////////////

intrinsic NumberOfVertices(Q::QuvFlg) -> RngIntElt
    {The number of vertices of Q.}
    return #(Q`r);
end intrinsic;

intrinsic Dimension(Q::QuvFlg) -> RngIntElt
    {The dimension of Q.}
    if not assigned Q`dimension then
	Q`dimension := &+[Integers()| ri(Q,i)*(si(Q,i) - ri(Q,i)) : i in [1..NumberOfVertices(Q)-1] ];
    end if;
    return Q`dimension;
end intrinsic;

intrinsic DimensionVector(Q::QuvFlg) -> SeqEnum
    {The dimension vector of Q}
    return Q`r;
end intrinsic;

intrinsic AdjacencyMatrix(Q::QuvFlg) -> AlgMatElt
    {The adjacency matrix of the quiver Q}
    return Q`A;
end intrinsic;

intrinsic Quiver(Q::QuvFlg) -> Grph
    {The quiver defining Q, as a labelled directed graph}
    G := directed_graph_from_adjacency_matrix(Q`A);
    AssignVertexLabels(~G,Q`r);
    return G;
end intrinsic;

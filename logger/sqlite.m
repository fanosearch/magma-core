freeze;

/////////////////////////////////////////////////////////////////////////
// sqlite.m
/////////////////////////////////////////////////////////////////////////
// Allows logging to an sqlite3 database.
/////////////////////////////////////////////////////////////////////////

import "logger.m": set_logger, set_logger_store_value, get_logger_store_value;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Logs the given message to the database.
procedure sqlite_logger_handler(message,level)
    bool,db:=get_logger_store_value("sqlite_db");
    assert bool;
    bool,table_name:=get_logger_store_value("sqlite_table_name");
    assert bool;
    message:=SubstituteString(message,"\"","\"\"");
    cmd:="INSERT INTO " cat table_name cat "(level, message) VALUES (" cat
         IntegerToString(level) cat ", \"" cat message cat "\");";
    try
        SqliteExecute(db,cmd);
    catch e
        print "Warning: Unable to log message with sqlite: " cat e`Object;
    end try;
end procedure;

// Creates the table.
procedure create_table(db,table_name)
    cmd:="CREATE TABLE " cat table_name cat " (" cat
            "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " cat
            "stamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP, " cat
            "level INTEGER NOT NULL, " cat
            "message TEXT); " cat
         "CREATE INDEX idx_stamp ON " cat table_name cat "(stamp); " cat
         "CREATE INDEX idx_level ON " cat table_name cat "(level);";
    SqliteExecute(db,cmd);
end procedure;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic SetLoggerToSqlite(database::MonStgElt :
    table_name:=false, sqlite:=false )
{Sets log messages to be sent to an sqlite database, and enables logging.}
    // Sanity checks
    require not InternalIsPC(): "Operation not supported on Windows";
    database:=Trim(database);
    require #database gt 0: "The path to the database must not be empty";
    if table_name cmpeq false then table_name:="error_log"; end if;
    require Type(table_name) eq MonStgElt:
        "'table_name' must be a non-empty string";
    table_name:=Trim(table_name);
    require #table_name gt 0:
        "'table_name' must be a non-empty string";
    if sqlite cmpeq false then sqlite:="sqlite3"; end if;
    require Type(sqlite) eq MonStgElt and #sqlite gt 0:
        "'sqlite' must be a non-empty string";
    // Establish a connection to the database
    db:=SqliteConnect(database : sqlite:=sqlite);
    // Do we need to create the table?
    if not SqliteTableExists(db,table_name) then
        create_table(db,table_name);
    end if;
    // Set the logger
    set_logger_store_value("sqlite_db",db);
    set_logger_store_value("sqlite_table_name",table_name);
    set_logger(sqlite_logger_handler,"%s");
end intrinsic;

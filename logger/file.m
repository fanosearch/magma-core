freeze;

/////////////////////////////////////////////////////////////////////////
// file.m
/////////////////////////////////////////////////////////////////////////
// Allows logging to a file.
/////////////////////////////////////////////////////////////////////////

import "logger.m": set_logger_store_value, get_logger_store_value, set_logger;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Logs the given message to the file.
procedure file_logger_handler(message,level)
    if #message eq 0 then return; end if;
    bool,path:=get_logger_store_value("file_path");
    assert bool;
    if message[#message] ne "\n" then message cat:= "\n"; end if;
    try
        fprintf path,message;
    catch e;
        print "Warning: Unable to log message to " cat path;
    end try;
end procedure;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic SetLoggerToFile(path::MonStgElt)
{Sets log messages to be saved to the indicated file, and enables logging.}
    set_logger_store_value("file_path",path);
    set_logger(file_logger_handler,"%d %h[%p]: %s");
end intrinsic;

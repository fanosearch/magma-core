freeze;

/////////////////////////////////////////////////////////////////////////
// mysql.m
/////////////////////////////////////////////////////////////////////////
// Allows logging to a MySQL database.
/////////////////////////////////////////////////////////////////////////

import "logger.m": set_logger, set_logger_store_value, get_logger_store_value;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Attempts to convert the string to an integer. The first return value is true
// iff the entire string was successfully parsed. The second return value is the
// integer parsed so far. The third return value is the index of the first non-
// parsable character in the string. (If the string was successfully parsed
// through to the end, then the returned index will be zero.)
function is_string_integer( str )
    // Initialization
    idx:=IndexOfNonWhiteSpace(str);
    if idx eq 0 then return false,0,0; end if;
    numbers:=["0","1","2","3","4","5","6","7","8","9"];
    // Is there a negative sign?
    negative:=false;
    if str[idx] eq "-" then
        negative:=true;
        idx:=IndexOfNonWhiteSpace(str,idx+1);
    end if;
    // The next character had better be a number
    if idx eq 0 or not str[idx] in numbers then
        return false,0;
    end if;
    // Read the numbers
    val:=0;
    while idx le #str do
        num:=Index(numbers,str[idx]) - 1;
        if num eq -1 then
            break;
        end if;
        val *:= 10;
        val +:= num;
        idx +:= 1;
    end while;
    // Apply the negation
    if negative then val *:= -1; end if;
    // Check that what follows is white-space only
    if IndexOfNonWhiteSpace(str,idx) eq 0 then
        return true,val,0;
    else
        return false,val,idx;
    end if;
end function;

// Logs the given message to the database.
procedure mysql_logger_handler(message,level)
    bool,base:=get_logger_store_value("mysql_base");
    assert bool;
    bool,table_name:=get_logger_store_value("mysql_table_name");
    assert bool;
    message:=SubstituteString(message,"\"","\\\"");
    cmd:="INSERT INTO " cat table_name cat "(level, message) VALUES (" cat
         IntegerToString(level) cat ", \"" cat message cat "\");";
    if not PipeCommand(base cat "\"" cat EscapeString(cmd) cat "\"") then
        bool,dsn:=get_logger_store_value("mysql_dsn");
        if bool then
            print "Warning: Unable to log message with MySQL:\n  " cat dsn;
        else
            print "Warning: Unable to log message with MySQL";
        end if;
    end if;
end procedure;

// Returns true iff the given table name can be found amongst the given list.
function table_exists(res,table_name)
    for name in Split(res,"\n") do
        if name eq table_name then return true; end if;
    end for;
    return false;
end function;

// Creates the table.
function create_table(base,table_name)
    cmd:="CREATE TABLE " cat table_name cat " (" cat
            "id INT NOT NULL AUTO_INCREMENT, " cat
            "stamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP, " cat
            "level TINYINT NOT NULL, " cat
            "message TEXT, " cat
         "PRIMARY KEY(id), INDEX(stamp), INDEX(level));";
    return PipeCommand(base cat "'" cat cmd cat "'");
end function;

// Creates a dsn describing this connection.
function create_dsn(database,user,socket,host,port)
    dsn:="mysql://";
    if not user cmpeq false then
        dsn cat:= user cat "@";
    end if;
    if not socket cmpeq false then
        dsn cat:= socket;
    else
        if host cmpeq false then
            dsn cat:= "localhost";
        else
            dsn cat:= host;
        end if;
        if not port cmpeq false then
            dsn cat:= ":" cat IntegerToString(port);
        end if;
    end if;
    dsn cat:= "/" cat database;
    return dsn;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic SetLoggerToMySQL(database::MonStgElt :
    table_name:=false, mysql:=false, user:=false, password:=false,
    socket:=false, host:=false, port:=false)
{Sets log messages to be sent to a MySQL database, and enables logging.}
    // Sanity checks
    require not InternalIsPC(): "Operation not supported on Windows";
    database:=Trim(database);
    require #database gt 0: "The database name must not be empty";
    if table_name cmpeq false then table_name:="error_log"; end if;
    require Type(table_name) eq MonStgElt:
        "'table_name' must be a non-empty string";
    table_name:=Trim(table_name);
    require #table_name gt 0:
        "'table_name' must be a non-empty string";
    if mysql cmpeq false then mysql:="mysql"; end if;
    require Type(mysql) eq MonStgElt: "'mysql' must be a string";
    require user cmpeq false or Type(user) eq MonStgElt:
        "'user' must be a string";
    require password cmpeq false or Type(password) eq MonStgElt:
        "'password' must be a string";
    require socket cmpeq false or Type(socket) eq MonStgElt:
        "'socket' must be a string";
    require host cmpeq false or Type(host) eq MonStgElt:
        "'host' must be a string";
    if not port cmpeq false then
        if Type(port) eq MonStgElt then
            bool,port:=is_string_integer(port);
            require bool: "'port' must be an integer in the range 1..65535";
        end if;
        require Type(port) eq RngIntElt and port ge 1 and port le 65535:
            "'port' must be an integer in the range 1..65535";
    end if;
    require password cmpeq false or Type(password) eq MonStgElt:
        "'password' must be a string";
    // Create a dsn for this connection
    dsn:=create_dsn(database,user,socket,host,port);
    // Create the base command
    base:=mysql cat " -bqsN";
    if not user cmpeq false then
        base cat:= " --user=\"" cat EscapeString(user) cat "\"";
    end if;
    if not password cmpeq false then
        base cat:= " --password=\"" cat EscapeString(password) cat "\"";
    end if;
    if not socket cmpeq false then
        base cat:= " --socket=\"" cat EscapeString(socket) cat "\"";
    else
        if not host cmpeq false then
            base cat:= " --host=\"" cat EscapeString(host) cat "\"";
        end if;
        if not port cmpeq false then
            base cat:= " -P " cat IntegerToString(port);
        end if;
    end if;
    base cat:= " --database=\"" cat EscapeString(database) cat "\" -e ";
    // Can we talk to the database?
    bool,res:=PipeCommand(base cat "\"show tables;\"");
    require bool: "Unable to access MySQL database:\n  " cat dsn;
    // Do we need to create the table?
    if not table_exists(res,table_name) then
        require create_table(base,table_name):
            "Unable to create the table " cat table_name cat " on:\n  " cat dsn;
    end if;
    // Set the logger
    set_logger_store_value("mysql_dsn",dsn);
    set_logger_store_value("mysql_base",base);
    set_logger_store_value("mysql_table_name",table_name);
    set_logger(mysql_logger_handler,"%s");
end intrinsic;

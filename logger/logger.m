freeze;

/////////////////////////////////////////////////////////////////////////
// logger.m
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Data Store
/////////////////////////////////////////////////////////////////////////

// The store for the logger settings. Do not access directly!
logger_store:=NewStore();

/////////////////////////////////////////////////////////////////////////
// Functions for general use
/////////////////////////////////////////////////////////////////////////

// Sets the logger store for the given key to the given value.
forward initialize_logger_store;
procedure set_logger_store_value(key,value)
    // Sanity check
    error if Type(key) ne MonStgElt,
        "set_logger_store_value: The key must be a string";
    // Set the value
    if not StoreIsDefined(logger_store,"init") then
        initialize_logger_store();
    end if;
    StoreSet(logger_store,key,value);
end procedure;

// Returns true followed by the logger store value for the given key if defined,
// false otherwise.
function get_logger_store_value(key)
    // Sanity check
    error if Type(key) ne MonStgElt,
        "get_logger_store_value: The key must be a string";
    // Recover the value
    if not StoreIsDefined(logger_store,"init") then
        initialize_logger_store();
    end if;
    bool,value:=StoreIsDefined(logger_store,key);
    if bool then
        return true,value;
    else
        return false,_;
    end if;
end function;

// Sets the handler of the logger to use and the line format, and ensures that
// logging is enabled.
//  * The handler must be a procedure accepting two arguments: the message to
//    log (a string), and the log level (integer).
//  * The log line format is a string describing how the messages are formatted.
//    The supported tokens are documented in the REAME.txt file.
//    The log line format will only be used if the user hadn't set a custom
//    format.
procedure set_logger(handler,format)
    // Sanity check
    error if Type(handler) ne UserProgram,
        "set_logger: The handler must be a procedure accepting two arguments";
    error if Type(format) ne MonStgElt,
        "set_logger: The line format must be a string";
    // Set the values
    set_logger_store_value("handler",handler);
    bool,default_format:=get_logger_store_value("default_format");
    if not bool or default_format then
        set_logger_store_value("format",format);
    end if;
    set_logger_store_value("enabled",true);
end procedure;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Initializes the logger store ready for use.
forward magma_logger_handler;
procedure initialize_logger_store()
    StoreSet(logger_store,"init",true);
    StoreSet(logger_store,"handler",magma_logger_handler);
    StoreSet(logger_store,"level",1);
    StoreSet(logger_store,"format","%d: %s");
    StoreSet(logger_store,"enabled",false);
end procedure;

// Returns the handler for the current logger
function get_logger_handler()
    bool,handler:=get_logger_store_value("handler");
    assert bool;
    return handler;
end function;

// Returns a string description of the log level
function get_level_string(level)
    return case<level |
        0: "Debug",
        1: "Info",
        2: "Notice",
        3: "Warning",
        4: "Error",
        default: IntegerToString(level)>;
end function;

// Returns the username
function get_username()
    bool,uname:=get_logger_store_value("username");
    if not bool then
        bool,uname:=PipeCommand("whoami");
        if not bool then uname:=""; end if;
        set_logger_store_value("username",uname);
    end if;
    return uname;
end function;

// Returns the appropriate substitution for the given token
function token_to_string(token)
    return case<token |
        "%": "%",
        "d": Date("%Y-%m-%d %H:%M:%S"),
        "t": Sprintf("%o",Cputime()),
        "m": Sprintf("%.2o", 1.0 * GetMemoryUsage() / 2^20),
        "p": IntegerToString(Getpid()),
        "u": IntegerToString(Getuid()),
        "w": get_username(),
        "h": Hostname(),
        "v": GetVersionString(),
        default: token>;
end function;

// Performs the token substitutions on the log line format
function perform_substitutions(format,message,level)
    message := Trim(message);
    idx:=Index(message,"%s");
    while idx ne 0 do
        if idx eq 1 or message[idx - 1] ne "%" then
            message:=message[1..idx - 1] cat "%" cat message[idx..#message];
            idx +:= 1;
        end if;
        idx:=Position(message,"%s",idx + 2);
    end while;
    tokens:={"%","d","t","m","p","u","w","h","v"};
    res:="";
    idx:=Index(format,"%");
    while idx ne 0 do
        if idx eq #format then
            res cat:= format;
            format:="";
        elif format[idx + 1] eq "s" then
            res cat:= format[1..idx - 1];
            format:=message cat format[idx + 2..#format];
        elif format[idx + 1] eq "l" then
            res cat:= format[1..idx - 1] cat get_level_string(level);
            format:=format[idx + 2..#format];
        elif format[idx + 1] in tokens then
            res cat:= format[1..idx - 1] cat token_to_string(format[idx + 1]);
            format:=format[idx + 2..#format];
        else
            res cat:= format[1..idx];
            format:=format[idx + 1..#format];
        end if;
        idx:=Index(format,"%");
    end while;
    return res cat format;
end function;

// Performs the token substitution on the input string and sends the
// result to the appropriate logger.
procedure message_to_handler(message,level)
    message:=perform_substitutions(GetLoggerLineFormat(),message,level);
    handler:=get_logger_handler();
    handler(message,level);
end procedure;

// The default "magma" logger handler.
procedure magma_logger_handler(message,level)
    print message;
end procedure;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic DisableLogger()
{Disables the logger.}
    set_logger_store_value("enabled",false);
end intrinsic;

intrinsic EnableLogger()
{Enables the logger.}
    set_logger_store_value("enabled",true);
end intrinsic;

intrinsic IsLoggerEnabled() -> BoolElt
{Is the logger enabled?}
    bool,enabled:=get_logger_store_value("enabled");
    assert bool;
    return enabled;
end intrinsic;

intrinsic SetLoggerToMagma()
{Sets log messages to be displayed on the Magma command line, and enables logging.}
    set_logger(magma_logger_handler,"%d: %s");
end intrinsic;

intrinsic SetLoggerLineFormat(format::MonStgElt) -> MonStgElt
{Specifies the log line format used by the logger. Also gives an example log line to illustrate this format.}
    set_logger_store_value("format",format);
    set_logger_store_value("default_format",false);
    return perform_substitutions(format,"This is an example log entry",1);
end intrinsic;

intrinsic GetLoggerLineFormat() -> MonStgElt
{The currently used log line format.}
    bool,format:=get_logger_store_value("format");
    assert bool;
    return format;
end intrinsic;

intrinsic SetLoggerLevel(level::RngIntElt)
{Specifies the minimum level for which logging will occur.}
    require level ge 0 and level le 4:
        "The log level must be between 0 (least importance) and 4 (highest importance)";
    set_logger_store_value("level",level);
end intrinsic;

intrinsic GetLoggerLevel() -> RngIntElt
{The minimum level for which logging will occur.}
    bool,level:=get_logger_store_value("level");
    assert bool;
    return level;
end intrinsic;

intrinsic Logger(message::MonStgElt)
{Log the message with the logger.}
    Logger(message,2);
end intrinsic;

intrinsic Logger(message::MonStgElt,level::RngIntElt)
{Log the message with the logger at the specified level.}
    require level ge 0 and level le 4:
        "The log level must be between 0 (least importance) and 4 (highest importance)";
    if IsLoggerEnabled() and level ge GetLoggerLevel() then
        message_to_handler(message,level);
    end if;
end intrinsic;

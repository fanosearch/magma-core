freeze;

/////////////////////////////////////////////////////////////////////////
// asymptotics.m
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Given the relations returned by RecurrenceRelation, a partially completed period sequence S, and the next index k, extends S by one place. Adapted from magma-core/picardfuchs/picardfuchs.m
function extend_sequence_using_relation(rel,k,S)
    K:=CoefficientRing(Universe(rel));
    rel:=[Evaluate(r,k) : r in rel];
    val:=[Integers()|];
    for i in [1..#rel] do
        if rel[i] eq 0 then
            val[i]:=0;
        else
            idx:=k - i + 2;
            if idx lt 1 then
                val[i]:=0;
            elif IsDefined(S,idx) then
                val[i]:=rel[i] * S[idx];
            end if;
        end if;
    end for;
    idxs:=[Integers() | i : i in [1..#rel] | not IsDefined(val,i)];
    // Sanity check
    error if #idxs ne 1, "Expected to solve for only one variable";
    idx:=idxs[1];
    c:=&+[Integers() | val[i] : i in [1..#val] | i ne idx];
    d:=-c / rel[idx];
    error if not IsIntegral(d), "Expected integer-valued entries";
    S[k - idx + 2]:=Integers() ! d;
    return S;
end function;

// Extends the period sequence S using the Picard--Fuchs operator L until it is at least length len and callback returns true, x. Then returns x.
function slide_window(L, len, S, callback)
    // Compute the relations
    rel:=RecurrenceRelation(L);
    // Begin by extending S out to the correct length
    i:=#S;
    while #S lt len do
        S:=extend_sequence_using_relation(rel,i,S);
        i +:= 1;
    end while;
    // Create the initial associative array -- note that we index from 1
    min:=1;
    if #S gt len then
        min:=#S - len + 1;
    end if;
    A:=AssociativeArray(Integers());
    for i in [min..#S] do
        A[i]:=S[i];
    end for;
    i:=#A;
    // Does A satisfy callback? If not, slide A to the right.
    while true do
        ok,res:=callback(A);
        if ok then
            return res;
        end if;
        A:=extend_sequence_using_relation(rel,i,A);
        Remove(~A, min);
        min +:= 1;
        i +:= 1;
    end while;
end function;

// l_infinity computes the l_infinity norm between two sequences. They should have the same length.
function l_infinity(x, y)
    error if Type(x) ne SeqEnum or Type(y) ne SeqEnum, "expected sequences";
    error if Universe(x) ne RealField() or Universe(y) ne RealField(), "expected real sequences";
    error if #x ne #y, "length mismatch";
    return Max([Abs(x[i] - y[i]) : i in [1..#x]]);
end function;

// Returns a callback for linear regression
function callback(dim, idxs, epsilon, repeats)
    // We use a store to cache the most recent values of the coefficients
    cache := NewStore();
    return function(S)
        // Only regress every 1000 steps
        max := Max(Keys(S));
        if not IsDivisibleBy(max, 1000) then
            return false, _;
        end if;
        // Do the regression
        xs := [PowerSequence(RealField())|];
        ys := [RealField()|];
        for k in Keys(S) do
            v := S[k];
            if v ne 0 and k gt 1 then
                Append(~xs, [(k-1)^d : d in idxs]);
                Append(~ys, Log(v) + dim/2 * Log(k-1));
            end if;
        end for;
        A := LinearRegression(xs, ys);
        // Has the computation stabilised?
        n := 0;
        if StoreIsDefined(cache,"A") then
            A_old := StoreGet(cache, "A");
            n_old := StoreGet(cache, "n");
            if l_infinity(A, A_old) lt epsilon then
                n := n_old + 1;
                if n ge repeats then
                    return true, A;
                end if;
            end if;
        end if;
        // Keep going
        StoreSet(cache,"A",A);
        StoreSet(cache,"n",n);
        return false,_;
    end function;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic LogAsymptotics(L::AlgFPElt, dim::RngIntElt, ps::SeqEnum[RngIntElt], k::RngIntElt : epsilon:=0.0001, repeats:=5, min_length:=10000) -> SeqEnum[FldReElt]
{
    The first k asymptotic growth coefficients for Log(c_d) as d grows, where c_d is the period sequence with Picard--Fuchs operator L and initial terms of the period sequence ps. These are the coefficients [A_1,A_2,...,A_k] in an asymptotic expansion

    Log(c_d) ~ A_1 d + A_2 - (dim/2) Log(d) + A_3/d + A_4/d^3 + ... + A_k/d^(2k-5)

    Note that there are no odd negative powers of d in the expansion. The calculation is performed by extending ps using L until the asymptotic coefficients stabilise. The optional parameters 'epsilon' and 'repeats' specify respectively the maximum size of allowed changes in the A_i and the number of consecutive allowed changes before the sequence is deemed to have stabilised. The optional parameter 'min_length' specifies a minimum length for the period sequence before stabilisation is checked.
}
    // Sanity checks
    require dim gt 0: "the argument 'dim' must be positive";
    require k gt 1: "the argument 'k' must be at least 2";
    require IsZero(ApplyPicardFuchsOperator(L, ps)): "the operator 'L' must annihilate the period sequence 'ps'";
    require Type(epsilon) eq FldReElt and epsilon gt 0: "the parameter 'epsilon' must be a positive real number";
    require Type(repeats) eq RngIntElt and repeats gt 0: "the parameter 'repeats' must be a positive integer";
    require Type(min_length) eq RngIntElt and min_length gt 1: "the parameter 'min_length' must be an integer and must be at least 2";
    // Compute the indices in the expansion
    idxs := [1,0] cat [5-2*j : j in [3..k]];
    // Prepare the callback 
    f := callback(dim, idxs, epsilon, repeats);
    // Extend the sequence until the asymptotics stabilise
    return slide_window(L, min_length, ps, f);
end intrinsic;
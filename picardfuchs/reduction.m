freeze;

/////////////////////////////////////////////////////////////////////////
// reduction.m
/////////////////////////////////////////////////////////////////////////
// implements Lairez's generalized Griffiths--Dwork reduction
// see Algorithm 3 of Lairez, "Computing Periods of Rational Integrals"
/////////////////////////////////////////////////////////////////////////

cache := NewStore();

/////////////////////////////////////////////////////////////////////////
// This function should be called once, before calling any of the other
// functions in this file
//
// f is a homogeneous element of QQ[x_0,x_1,...,x_n]
// we cache the following data:
// * the degree of f
// * the Jacobian ideal of f
// * the syzygies of f
// * the trivial szyzgies of f
// * a basis of leading monomials of the trivial syzygies
// * maps to and from the ring in which all these data live
// (this has two "extra" variables u,v : see the discussion just after
// Lemma 35 in Lairez's paper)
/////////////////////////////////////////////////////////////////////////
procedure initialize_Jacobian_data(f)
    Qx:=Parent(f);     // QQ[x_0,...,x_n]
    n := Rank(Qx) - 1;
    // We implement the trick just following Lemma 35 in Lairez
    R := PolynomialRing(CoefficientRing(Qx),n+3,"grevlex");
    // the distinguished variables in R
    u := R.1;
    v := R.2;
    // the variables x_0,...,x_n, in the ring R
    vars := [R| R.i : i in [3..Rank(R)]];
    // the natural map Qx -> R
    toR := hom< Qx->R | vars >;
    // the map R -> Qx which sets u = v = 1
    fromR := hom< R->Qx | [1,1] cat [Qx.i : i in [1..Rank(Qx)]] >;
    // the map R -> Qx which extracts the coefficient of omega
    get_omega := hom< R->Qx | [Qx|1,0] cat [Qx.i : i in [1..Rank(Qx)]] >;
    // the partial derivatives of f, in the ring R
    df := [R| toR(Derivative(f,i)) : i in [1..Rank(Qx)]];
    // the monomials which represent omega and \chi^i, 0 le i le n
    omega := u^(n+1);
    chi := [R| u^(n-i)*v^(i+1) : i in [0..n]];
    // monomials of degree n+2 in u and v
    high_degree_monomials := [R| u^i*v^(n+2-i) : i in [0..n+2] ];
    // the Jacobian ideal and the syzygies
    jac := ideal < R| [ df[i]*omega - chi[i] : i in [1..Rank(Qx)]] cat high_degree_monomials >;
    syz := [R| p : p in GroebnerBasis(jac) | Degree(p,u) lt n+1];
    // the trivial szyzgies
    triv_syz := ideal <R| [ df[i]*chi[j] - df[j]*chi[i] : i, j in [1..n+1] ] cat high_degree_monomials >;
    // THINK we might not use this below
    triv_syz_lm := Basis(LeadingMonomialIdeal(tsyz));
    // cache the data
    StoreSet(cache,"N",TotalDegree(f));
    StoreSet(cache,"jac",jac);
    StoreSet(cache,"syz",syz);
    StoreSet(cache,"triv_syz",triv_syz);
    StoreSet(cache,"triv_syz_lm",triv_syz_lm);
    StoreSet(cache,"toR",toR);
    StoreSet(cache,"fromR",fromR);
    StoreSet(cache,"get_omega",get_omega);
end procedure;

// clear the data cache
// This function should be called once, after you have finished calling other functions in this file
procedure clear_Jacobian_data()
    StoreClear(cache);
end procedure;


// a is in the ring R defined in initialize_Jacobian_data()
// fromR is the map defined in initialize_Jacobian_data()
// we extract the coefficients of chi_i, 0 leq i le n, from
// the element a
// returns the coefficients as a sequence of elements of Qx
// (Qx is also defined in initialize_Jacobian_data())
function get_chis(a,fromR)
    R := Parent(a);
    coeffs_we_want := ZeroSequence(R,Rank(R) - 2);
    for t in Terms(a) do
	deg:=Degree(t,2);
	if deg gt 0 then
	    coeffs_we_want[deg] +:= t;
	end if;
    end for;
    return [Codomain(fromR) | fromR(g) : g in coeffs_we_want];
end function;

// given a polynomial f, return the part of total degree deg
function part_of_degree(f,deg)
    return &+[Parent(f) | t : t in Terms(f) | TotalDegree(t) eq deg];
end function;


// This is RedStep in Lairez's Algorithm 3
// alpha is homogeneous in QQ[x_0,x_1,...,x_n]
// q is a non-negative integer, representing a power of f in
//    the denominator of the rational differential form represented by alpha
// N,jac,toR,fromR,get_omega are as defined in initialize_Jacobian_data()
// returns a homogeneous element of QQ[x_0,...,x_n], representing red_q^{GD}(alpha)
function reduction_step(alpha,q,N,jac,toR,fromR,get_omega)
     // grab the part of alpha of degree q*N, and the rest of alpha
     alpha_q := part_of_degree(alpha,q*N);
     alpha_prime := alpha - alpha_q;
    // take the normal form with respect to the Jacobian ideal, and use it to reduce alpha
    normal_form := NormalForm(toR(alpha_q),jac);
    omega_part := get_omega(normal_form);
    chi_parts := get_chis(normal_form,fromR);
    return alpha_prime + omega_part +
	   &+[Universe(chi_parts)|Derivative(chi_parts[i],i) : i in [1..#chi_parts]];
end function;

// This is Echelon from Lairez algorithm 3 (or rather, from the discussion
// just before algorithm 3).
// fs is a sequence of homogeneous elements of R, where R is QQ[x_0,...,x_n] equipped
// with some term order
// Returns a basis in echelon form, with respect to our chosen term order, of the
// linear subspace spanned by the fs
function echelon_basis(fs)
    K:=CoefficientRing(Universe(fs));
    monos:=Sort(&join[{PowerSequence(Integers()) | Exponents(m) : m in Monomials(f)} : f in fs]);
    Reverse(~monos);
    mat:=[PowerSequence(PowerSequence(K))|];
    for f in fs do
	row:=ZeroSequence(K,#monos);
	cs,ex:=CoefficientsAndExponents(f);
	for i in [1..#cs] do
	    idx:=Index(monos,ex[i]);
	    row[idx]:=cs[i];
	end for;
	Append(~mat,row);
    end for;
    echelon_mat := EchelonForm(mat);
    RemoveZeroRows(~echelon_mat);
    return [Universe(fs)| &+[echelon_mat[i,j]*monos[j] : j in [1..#monos]] :
	    i in [1..NumberOfRows(echelon_mat)]];
end function;

// this is BasisW in Lairez algorithm 3
// r and q are non-negative integers
// N,jac,toR,fromR,get_omega are as defined in initialize_Jacobian_data()
function basis_W(r,q,N,jac,toR,fromR,get_omega)
    if r eq 1 then
	// THINK
    else
	Qx := Domain(toR); // QQ[x_0,...,x_n]
	U := [Qx| alpha : alpha in $$(r-1,q,N,jac,toR,fromR,get_omega) | TotalDegree(alpha) eq q*N];
	W := $$(r-1,q+1,N,jac,toR,fromR,get_omega);
	// THINK
        // now echelonize U cat [reduction_step(alpha,q,...) : alpha in W | deg alpha eq q*N]
    end if;
end function;

// this is BasisU in Lairez algorithm 3
// r and q are non-negative integers
// N,jac,toR,fromR,get_omega are as defined in initialize_Jacobian_data()
function basis_U(r,q,N,jac,toR,fromR,get_omega)
    Qx := Domain(toR); // QQ[x_0,...,x_n]
    return [Qx | alpha : alpha in basis_W(r,q,N,jac,toR,fromR,get_omega) | TotalDegree(alpha) eq q*N];
end function;

// this is Reduction in Lairez algorithm 3
// alpha is a homogeneous element of QQ[x_0,...,x_n]
// r is a non-negative integer
// N,jac,toR,fromR,get_omega are as defined in initialize_Jacobian_data()
function reduction_internal(alpha,r,N,jac,toR,fromR,get_omega)
    if IsZero(alpha) then
	return alpha;
    end if;
    q := ExactQuotient(TotalDegree(alpha),N);
    // grab the part of alpha of degree q*N, and the rest of alpha
    alpha_q := part_of_degree(alpha,q*N);
    alpha_prime := alpha - alpha_q;
    rho := NormalForm(reduction_step(alpha_q,q,N,jac,toR,fromR,get_omega),
		      basis_U(r,q,N,jac,toR,fromR,get_omega));
    rho_q := part_of_degree(rho,q*N);
    rho_q_minus_1 := part_of_degree(rho,(q-1)*N);
    return rho_q + $$(alpha_prime+rho_q_minus_1,r,N,jac,toR,fromR,get_omega);
end function;

// this is Reduction in Lairez algorithm 3
// alpha is a homogeneous element of QQ[x_0,...,x_n]
// r is a non-negative integer
// it wraps reduction_internal(alpha,r,N,jac,toR,fromR,get_omega)
function reduction(alpha,r)
    N := StoreGet(cache,"N");
    jac := StoreGet(cache,"jac");
    toR := StoreGet(cache,"toR");
    fromR := StoreGet(cache,"fromR");
    get_omega := StoreGet(cache,"get_omega");
    return reduction_internal(alpha,r,N,jac,toR,fromR,get_omega);
end function;

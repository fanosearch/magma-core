freeze;

/////////////////////////////////////////////////////////////////////////
// lairez.m
/////////////////////////////////////////////////////////////////////////
// Implements Lairez's algorithm for computing Picard--Fuchs operators
// associated to Laurent polynomials. See:
//  "Computing Periods of Rational Integrals", arXiv:1404.5069v2 [cs.SC]
//
// The extension of the algorithm for certificating the Picard--Fuchs
// operators was discussed when Lairez visited Imperial in May 2014,
// and is also (at least partially) in his code on GitHub
/////////////////////////////////////////////////////////////////////////

import "gauss_manin.m": gauss_manin;
import "picardfuchs.m": normalise_operator_coeffs;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Given a rational function F in QQ[t](x_1,...,x_n), returns a, f where
// a, f are homogeneous in QQ[t][x_0,x_1,...,x_n] such that a/f restricts
// to F when x_0 = 1, and deg a = deg f - n - 1, and f is square free.
function prepare_fraction(F)
    // Build QQ[t][x_0,x_1,...,x_n] and map F over
    R:=Parent(Numerator(F));
    n:=Rank(R);
    RR:=PolynomialRing(CoefficientRing(R),n + 1);
    phi:=hom<R -> RR | [RR.i : i in [2..n + 1]]>;
    num:=phi(Numerator(F));
    den:=phi(Denominator(F));
    // Homogenize the numerator and denominator
    homogNum:=Homogenization(num,RR.1);
    homogDen:=Homogenization(den,RR.1);
    // Calculate the total degree of homogNum / homogDen and adjust
    // as appropriate
    deg:=TotalDegree(homogNum) - TotalDegree(homogDen);
    if deg lt -n - 1 then
	homogNum *:= RR.1^(-deg - n - 1);
    else
	homogDen *:= RR.1^(deg + n + 1);
    end if;
    // We want homogDen to be square free, and this is always true
    // when we've got here via a Laurent polynomial
    assert IsSquarefree(homogDen);
    // THINK take this out, just fiddling with the variable order whilst debugging (to match with Lairez)
    psi := hom<RR->RR|[RR.(n+1)] cat [RR.i : i in [1..n]]>;
    homogNum := psi(homogNum);
    homogDen := psi(homogDen);
    return homogNum, homogDen;
end function;

// given an NxN matrix M in QQ[t] and a non-zero sequence of length N of elements of QQ[t]
// regard that sequence as a vector v
// and return an element L of the WeylAlgebra() W such that Lv = 0, where
// D = t d/dt in W acts as t d/dt + M, i.e. acts via a connection with connection
// matrix M
// THINK might want to do this over F_p instead, inside the reduce-mod-p part of
// the reduce-mod-p-then-rationally-reconstruct loop
// But in practice, timing data suggests that this is not a bottleneck so probably we
// don't need to change anything
function cyclic_equation(M,v)
    ////////////////////////////////////////////////////////////////////////////////
    TimingPush("cyclic_equation");
    ////////////////////////////////////////////////////////////////////////////////
    N := NumberOfRows(M);
    QQt := BaseRing(M);
    t := QQt.1;
    QQ := CoefficientRing(QQt);
    ////////////////////////////////////////////////////////////////////////////////
    // sanity checking
    ////////////////////////////////////////////////////////////////////////////////
    error if NumberOfColumns(M) ne N, "M must be a square matrix.\n";
    error if #v ne N, "v must have the same number of rows as M.\n";
    error if IsZero(v), "v must be non-zero.\n";
    error if QQ ne Rationals(), "M must be a matrix over QQ[t].\n %o\n",QQ;
    ////////////////////////////////////////////////////////////////////////////////
    // initialize everything
    ////////////////////////////////////////////////////////////////////////////////
    cyclic_vector := Matrix(QQt,N,1,v);
    cyclic_matrix := cyclic_vector;
    ////////////////////////////////////////////////////////////////////////////////
    // now add derivatives one at a time, as matrix columns, until the rank
    // of the matrix doesn't change
    ////////////////////////////////////////////////////////////////////////////////
    repeat
	// we choose a random evaluation point to calculate rank
	random_eval := hom<QQt->QQ|Random(QQ,2^32)>;
	cyclic_matrix_eval := ChangeRing(cyclic_vector,random_eval);
	repeat
	    // we loop until adding another column D^k v doesn't change the rank
	    cyclic_vector := t*Matrix(QQt,N,1,[Derivative(xx) : xx in Eltseq(cyclic_vector)])
			     + t*M*cyclic_vector;
	    cyclic_matrix := HorizontalJoin(cyclic_matrix,cyclic_vector);
	    cyclic_matrix_eval := HorizontalJoin(cyclic_matrix_eval,ChangeRing(cyclic_vector,random_eval));
	until Rank(cyclic_matrix_eval) lt NumberOfColumns(cyclic_matrix_eval);
	// now get the kernel: this should be one-dimensional, and define a differential operator L that annihilates v
	ker := Kernel(Transpose(cyclic_matrix));
    until Dimension(ker) eq 1; // this will almost always be true,
                               // but if it isn't then we were very unlucky with our
			       // evaluation point and so choose another one
    coeffs := Eltseq(Basis(ker)[1]);
    // the coefficients are in QQ(t), so we will clear denominators
    lcm := LCM([Denominator(xx) : xx in coeffs]);
    // now build the operator inside the Weyl algebra
    W,_,phi := WeylAlgebra(QQ);
    QQtD := Domain(phi);
    D := QQtD.2;
    psi := hom<QQt->QQtD|[QQtD.1]>;
    L := &+[ psi(lcm*coeffs[i])*D^(i-1) : i in [1..#coeffs]];
    TimingPop();
    return normalise_operator_coeffs(phi(L));
end function;


// Given a and f are homogeneous in QQ[t][x_0,x_1,...,x_n], and a value for r, attempts to
// construct the Picard-Fuchs operator L for a/f in W<D,t>. Note that if g was the original
// Laurent polynomial such that a/f = homog(1/(1-tg) 1/x_1...x_n) then L is the
// Picard-Fuchs operator for the classical period of g. Returns true,L on success, false,_
// otherwise.
function calculate_picard_fuchs_for_r(a,f,r : certificate:=false, tolerance:=10)
    // Compute the Picard-Fuchs operator
    // Here bool is true iff success. If it is false then (with very high probability) r is too small.
    // gm is the connection matrix with entries in QQ(t)
    // rho_0 is [a]_r.  It lies in QQ(t)
    // certificate is FIX ME THINK
    bool,gm,rho_0,M_basis,cert:=gauss_manin(f,r,a : certificate:=certificate, tolerance:=tolerance);
    if not bool then
	    // r was too small
	    return false,_,_;
    end if;
    // everything looks good, so return the annihilating operator of rho_0, as an element of
    // the Weyl algebra and the certificate (which will be undefined if we were passed certificate:=false)
    // FIX ME THINK need to turn cert from a certificate for gm into a certificate for L
    return true, cyclic_equation(gm, rho_0), cert;
end function;

// Given a rational function F living in QQ[t](x_1,...,x_n), computes an
// annihilating operator in the Weyl algebra. Returns true,L on success,
// false,errormsg otherwise (which shouldn't ever happen).
function calculate_picard_fuchs(F : r:=1, certificate:=false, tolerance:=10)
    // Rewrite F in the form a/f where a and f are homogeneous in QQ[t][x_0,x_1,...,x_n]
    a,f:=prepare_fraction(F);
    n:=Rank(Parent(a)) - 1;
    // We start working through values of r until success
    while r le n + 1 do
	bool,L,cert:=calculate_picard_fuchs_for_r(a,f,r : certificate:=certificate, tolerance:=tolerance);
	if bool then
	    return true,L,cert;
	end if;
        r +:= 1;
    end while;
    return false,"The value of 'r' has exceeded n + 1, which is a counter-example to the Dimca conjecture.",_;
end function;

// Returns true, f, r if the arguments are valid and false, msg, _ otherwise, where F is f cast into QQ(x_1,...,x_n). Valid means f is a non-degenerate Laurent polynomial in n variables over ZZ or QQ and r is a postive integer. If r is greater than n+1 then it will be set to n+1 and a warning message printed. In the invalid case, msg is a message describing the failure.
function validate_picard_fuchs_args(f, r)
    if not (Type(r) eq RngIntElt and r gt 0) then
        return false, "Parameter 'r' must be a positive integer", _;
    end if;
    // f had better be a Laurent polynomial
    if not IsLaurent(f) then
        return false, "Argument must be a Laurent polynomial defined over the rationals.", _;
    end if;
    // Check the coefficient ring
    R:=Parent(f);
    K:=CoefficientRing(R);
    if not (K cmpeq Rationals() or K cmpeq Integers()) then
	    return false, "Argument must be a Laurent polynomial defined over the rationals.", _;
    end if;
    // Make sure we are actually over the rationals
    if K cmpeq Integers() then
	    R := RationalFunctionField(Rationals(),Rank(Parent(f)));
	    f := R!f;
    end if;
    // We require that f is non-degenerate
    P:=NewtonPolytope(f);
    if not (IsMaximumDimensional(P) and ContainsZero(P)) then
	    return false, "Argument must be non-degenerate", _;
    end if;
    // If the user has specified r > n + 1 then we lower it
    if r gt Rank(R) + 1 then
	    printf "Warning: Parameter 'r' should be at most %o, so is being lowered to that value.\n",Rank(R) + 1;
	    r:=Rank(R) + 1;
    end if;
    return true, f, r;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic PicardFuchsOperator(f : r:=1, certificate:=false, tolerance:=10) -> AlgFPElt
    {A differential operator L which annihilates the classical period of f.  L is an element of the Weyl algebra W<D,t>, where D = t*d/dt.  This is not guaranteed to be minimal, that is, the Picard--Fuchs operator might be a right-divisor of L.  It is not guaranteed to be correct, because the underlying algorithm (from Lairez, "Computing Periods of Rational Integrals", arXiv: 1404.5069v2 [cs.SC]) uses interpolation over finite fields rather than working with QQ(t) directly, but it is correct with very high probability.  The optional argument `r' controls the order of the "Griffiths--Dwork-style reduction" performed; larger values of the integer r may give lower-order L, or succeed where lower values of r fail, but at a cost of increased computation.  If the optional argument `certificate' is set to true then a certificate that L is correct is returned as a second argument.  This may substantially increase the computation time; the default setting for `certificate' is false. The optional argument tolerance controls the number of reconstruction failures that are allowed before we assume that one of the random primes chosen is a bad prime and restart the computation. If tolerance is too low then PicardFuchsOperator may loop forever.}
    // Sanity check
    require Type(certificate) eq BoolElt: "Parameter 'certificate' must be a boolean";
    // Validate arguments and move f into QQ(x1,...,xn) from ZZ(x1,...,xn) if necessary
    ok, f, r := validate_picard_fuchs_args(f,r);
    require ok: f;
    // Cast f into QQ[t](x_1,...,x_n)
    R:=Parent(f);
    K:=CoefficientRing(R);
    Qt:=PolynomialRing(K);
    RR:=RationalFunctionField(Qt,Rank(R));
    f:=RR ! f;
    // Calculate 1/(1-tf(x1,...,xn))dx_1/x_1...dx_n/x_n
    F:=1/((1-Qt.1 * f) * &*Generators(RR));
    bool,L,cert:=calculate_picard_fuchs(F : r:=r, certificate:=certificate, tolerance:=tolerance);
    require bool: L;
    if certificate then
        return L, cert;
    else
        return L;
    end if;
end intrinsic;

intrinsic InternalLairezCyclicEquation(gm, rho_0) -> AlgFPElt
{Given the connection matrix gm with entries in QQ(t) and the reduction rho_0 = [a]_r as in [Lairez] S7.2.2, returns the corresponding Picard--Fuchs operator as an element of the Weyl algebra.}
    // THINK eventually need to handle certificates here
    return cyclic_equation(gm, rho_0);
end intrinsic;

freeze;

/////////////////////////////////////////////////////////////////////////
// picardfuchs.m
/////////////////////////////////////////////////////////////////////////
// Attempts to compute the Picard-Fuchs operator for the given period
// sequence.
/////////////////////////////////////////////////////////////////////////

import "../periodsequence/weyl.m": polynomial_ring_2;

// The period sequence must be longer than the margin we place around the
// boundary cases.
MARGIN:=5;

/////////////////////////////////////////////////////////////////////////
// Validation functions
/////////////////////////////////////////////////////////////////////////

// Validates the values of N and r against the length of then given period
// sequence S (as used by, for example, "annihilator" below). Returns true,_
// on success, false followed by an error message otherwise.
function validate_N_and_r_values(S,N,r)
    if N le 0 or r le 0 then
        return false,"Bounds must be positive integers.";
    end if;
    minlen:=(N + 1) * (r + 1) + MARGIN;
    if #S lt minlen then
        return false,Sprintf("Bounds incompatible with length: the sequence must be of length at least %o.",minlen);
    end if;
    return true,_;
end function;

// Validates the given data (as used by, for example, "calculate_annihilator"
// below). Returns true,maximum_height,maximum_N,maximum_r on success,
// false,errmsg,_,_ otherwise (where errmsg is an appropriate error message).
function validate_calculate_annihilator(S :
  extend:=false,
  minimum_N:=1,
  maximum_N:=false,
  minimum_r:=1,
  maximum_r:=false,
  maximum_height:=false)
    // Sanity check
    if Type(extend) ne BoolElt then
        return false,"Parameter 'extend' must be a boolean.",_,_;
    end if;
    if Type(minimum_N) ne RngIntElt or minimum_N lt 1 then
        return false,"Parameter 'minimum_N' must be a positive integer.",_,_;
    end if;
    if Type(minimum_r) ne RngIntElt or minimum_r lt 1 then
        return false,"Parameter 'minimum_r' must be a positive integer.",_,_;
    end if;
    if #S le MARGIN then
        return false,Sprintf("The sequence must be of length at least %o.",MARGIN + 1),_,_;
    end if;
    if maximum_height cmpeq false then
        maximum_height:=#S - MARGIN - 1;
    elif Type(maximum_height) ne RngIntElt or maximum_height lt 1 then
        return false,"Parameter 'maximum_height' must be a positive integer.",_,_;
    end if;
    if maximum_N cmpeq false then
        maximum_N:=maximum_height;
    elif Type(maximum_N) ne RngIntElt or maximum_N lt 1 then
        return false,"Parameter 'maximum_N' must be a positive integer.",_,_;
    end if;
    if maximum_r cmpeq false then
        maximum_r:=maximum_height;
    elif Type(maximum_r) ne RngIntElt or maximum_r lt 1 then
        return false,"Parameter 'maximum_r' must be a positive integer.",_,_;
    end if;
    if maximum_r lt minimum_r then
        return false,"The minimum and maximum range for r are incompatible.",_,_;
    end if;
    if maximum_N lt minimum_N then
        return false,"The minimum and maximum range for N are incompatible.",_,_;
    end if;
    return true,maximum_height,maximum_N,maximum_r;
end function;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Clears out the denominators of the given Picard-Fuchs operator L and ensures
// that the sign of the leading coefficient is positive.
function normalise_operator_coeffs(L)
    // Clear out the denominators
    L:=LCM([Denominator(c) : c in Coefficients(L)]) * L;
    // Remove any common factors
    L:=L / GCD([Numerator(c) : c in Coefficients(L)]);
    // Fix the sign of the leading coefficient
    return Sign(LeadingCoefficient(L)) * L;
end function;

// The result of applying D^N to S
function apply_D(S,N)
    error if N lt 0, "Argument 2 must be a non-negative integer.";
    return [Universe(S) | (i - 1)^N * S[i] : i in [1..#S]];
end function;

// The result of multiplying S by t^r
function multiply_by_t(S,r)
    error if r lt 0, "Argument 2 must be a non-negative integer.";
    r:=Minimum(#S,r);
    return [Universe(S) | 0 : i in [1..r]] cat S[1..#S - r];
end function;

// Multiply every element in S by k
function multiply_sequence(k,S)
    return [Universe(S) | k * s : s in S];
end function;

// Adds the sequences in S term by term
function add_sequences(S)
    // Sanity check
    error if #S eq 0, "Argument must not be empty.";
    n:=#S[1];
    R:=Universe(S[1]);
    error if not &and[Universe(s) cmpeq R : s in S],
        "Sequences must all have the same universe.";
    error if &or[#s ne n : s in S], "Sequences must all be of the same length.";
    // Return the sum
    return [R | &+[R | s[i] : s in S] : i in [1..n]];
end function;

// Attempts to apply the differential operator implied by the given sequences
// of coefficients and exponents to the given sequence S. Returns true followed
// by the result on success, false followed by an error message otherwise.
function apply_operator_to_sequence(cs,es,S)
    // Apply the operator
    try
        S:=add_sequences([multiply_sequence(cs[k],
                            multiply_by_t(apply_D(S,es[k][2]),es[k][1])) :
                            k in [1..#cs]]);
    catch e
        return false,e`Object;
    end try;
    // Return the result
    return true,S;
end function;

// Given the relations returned by Relation, a partially completed period
// sequence S, and the next index k, extends S by one place.
function extend_sequence_using_relation(rel,k,S)
	K:=CoefficientRing(Universe(rel));
    rel:=[Evaluate(r,k) : r in rel];
    val:=[Universe(S)|];
    for i in [1..#rel] do
        if rel[i] eq 0 then
            val[i]:=0;
        else
            idx:=k - i + 2;
            if idx lt 1 then
                val[i]:=0;
            elif IsDefined(S,idx) then
                val[i]:=rel[i] * S[idx];
            end if;
        end if;
    end for;
    idxs:=[Integers() | i : i in [1..#rel] | not IsDefined(val,i)];
    if #idxs eq 0 then
		bool,SS:=CanChangeUniverse(S,K);
        if bool then
            R:=PolynomialRing(K);
            S:=ChangeUniverse(SS,R);
            S[k + 1]:=R.1;
        else
            rk:=Rank(Universe(S));
            R:=PolynomialRing(K,rk + 1);
            phi:=hom<Universe(S) -> R | [R.i : i in [1..rk]]>;
            S:=[R | phi(c) : c in S];
            S[k + 1]:=R.(rk + 1);
        end if;
    else
        error if #idxs ne 1, "Unable to compute period sequence for operator.";
        idx:=idxs[1];
        c:=&+[Universe(val) | val[i] : i in [1..#val] | i ne idx];
        S[k - idx + 2]:=-c / rel[idx];
    end if;
    return S;
end function;

// Attempts to compute the first k + 1 terms of the period sequence S for the
// differential operator f in W<D,t>. Returns true followed by the sequence S
// on success, false followed by an error message otherwise.
function construct_sequence(f,k,S)
    // Compute the relations
    rel:=RecurrenceRelation(f);
    // Extend the sequence out to the desired length
    try
        i:=#S;
        while #S le k do
            S:=extend_sequence_using_relation(rel,i,S);
            i +:= 1;
        end while;
    catch e
        return false,e`Object;
    end try;
    // Return the result
    return true,S;
end function;

// Attempts to extend the period sequence S using the differential operators
// diffs. Returns true followed by an extension of S on success, false
// otherwise.
function extend_sequence_using_operators(S,diffs)
    // Map the differential operators in the Weyl algebra
    W,_,phi:=WeylAlgebra(CoefficientRing(Universe(diffs)));
    diffs:=[W | phi(f) : f in diffs];
    // Compute the extension for each of the given operators
    SS:=[PowerSequence(Universe(S))|];
    len:=#S + 50;
    for f in diffs do
        newS:=PeriodSequenceForPicardFuchsOperator(f,len - 1,S);
        if not Universe(newS) cmpeq Universe(S) then
            return false,_;
        end if;
        Append(~SS,newS);
    end for;
    // How far out do the extensions agree?
    for i in [#S + 1..len] do
        c:=SS[1][i];
        if &or[SS[j][i] ne c : j in [2..#SS]] then
            if i eq #S + 1 then return false,_; end if;
            vprintf Fanosearch: "-> Extended sequence to length %o.\n",i - 1;
            return true,SS[1][1..i - 1];
        end if;
    end for;
    // If we're here then they agree all the way out
    vprintf Fanosearch: "-> Extended sequence to length %o.\n",#SS[1];
    return true,SS[1];
end function;

// Given a sequence S of coefficients, and search bounds N and r (where N is the
// maximum power of D, and r is the maximum power of r), attempts to find a
// differential operator annihilating S. Returns a sequence of candidate
// operators as polynomials in \QQ[t,D].
function annihilator(S,N,r)
    // Sanity checks
    bool,msg:=validate_N_and_r_values(S,N,r);
    error if not bool, msg;
    // We need to work over the rationals
    ChangeUniverse(~S,Rationals());
    // Calculate the kernel
    SS:=[PowerSequence(Universe(S))|];
    for i in [0..N] do
        SS cat:=[multiply_by_t(S,j) : j in [0..r]];
        S:=apply_D(S,1);
    end for;
    // Convert each of the possible solutions into equations
    R:=polynomial_ring_2();
    diffs:=[R|];
    for k in Basis(Kernel(Matrix(SS))) do
        // "cs" is the array of coefficients, where cs[i * (m + 1) + j + 1] is
        // the coefficient of t^j D^i
        cs:=Eltseq(k);
        // We multiply through by the lcm to remove any denominators and keep
        // the coeffs integral
        if Universe(cs) cmpeq Rationals() then
            mult:=LCM([Integers() | Denominator(c) : c in cs]);
            cs:=[Rationals() | mult * c : c in cs];
        else
            mult:=LCM([Integers() | Denominator(Numerator(c)) : c in cs]) *
                  LCM([Denominator(c) : c in cs]);
            cs:=[Universe(S) | mult * c : c in cs];
        end if;
        // Finally, turn the array of coeffs into a function in t and D
        d:=&+[cs[i * (r + 1) + j + 1] * R.1^j * R.2^i :
                                        i in [0..N], j in [0..r]];
        Append(~diffs,d);
    end for;
    // Return the sequence of results
    return diffs;
end function;

// given an element PF of the Weyl algebra, returns N, r where
// N is the order in D and r is maximum such that PF contains a
// monomial t^r D^N
function N_and_r(PF)
    assert Type(PF) eq AlgFPElt;
    _, es:=CoefficientsAndExponents(PF);
    result:=Max(es);
    return Explode(result);
end function;

// Find the smallest value of [N,r] with a differential operator, taking care
// to avoid the boundary cases. Returns true followed by the differential
// operator and period sequence used on success, false otherwise. Smallest
// means smallest (N + 1)(r + 1), breaking ties by smallest N. Recall that the
// number of free parameters in the differential operator is (N+1)(r+1).
function calculate_annihilator(S :
  extend:=false,
  minimum_N:=1,
  maximum_N:=false,
  minimum_r:=1,
  maximum_r:=false,
  maximum_height:=false)
    // Sanity check -- on failure the second return value will be an error msg
    bool,maximum_height,maximum_N,maximum_r:=validate_calculate_annihilator(S :
        extend:=extend,
        minimum_N:=minimum_N,
        minimum_r:=minimum_r,
        maximum_height:=maximum_height,
        maximum_N:=maximum_N,
        maximum_r:=maximum_r);
    error if not bool, maximum_height;
    // Calculate the initial range of heights to search
    start_height:=Max(4,(1 + minimum_N) * (1 + minimum_r));
    end_height:=Min(maximum_height,#S - MARGIN - 1);
    // To begin with we have no candidate differential operator
    have_candidate:=false;
    candidate:=0;
    candidate_N:=-1;
    candidate_r:=-1;
    // Create all the pairs to try, sorted in order of height
    pairs_to_try:=&cat[[[N_plus_one-1,(height div N_plus_one) - 1] : N_plus_one in Divisors(height)] : height in [start_height..end_height]];
    // Extract only the pairs where N and r are large enough
    pairs_to_try:=[I : I in pairs_to_try | I[1] ge minimum_N and I[2] ge minimum_r and I[1] le maximum_N and I[2] le maximum_r];
    // Recover the map QQ[t,D] -> W<D,t>
    _,_,psi:=WeylAlgebra();
    // Now compute annihilators, in increasing order of height. If we find
    // exactly one annihilating operator, compare that to our candidate and
    // replace the candidate if it is better, and then adjust the search so
    // that we only look for operators with strictly lower N. If we find more
    // than one annihilating operator then we know for sure that N is too high,
    // so adjust the search to look for operators with strictly lower N.
    while #pairs_to_try gt 0 do
        // Grab the first pair and delete it from the list
        N,r:=Explode(pairs_to_try[1]);
        Remove(~pairs_to_try,1);
        vprintf Fanosearch: "Trying height %o (N=%o, r=%o)\n",(N+1)*(r+1),N,r;
        diffs:=annihilator(S,N,r);
        if #diffs ne 0 then
            // We might be able to extend the period sequence
            if extend then
                bool,new_S:=extend_sequence_using_operators(S,diffs);
                if bool then
        		    // Success! Extend S
                    S:=new_S;
                    // Add the new possible (N,r) pairs
                    new_start_height:=(N + 1) * (r + 1) + 1;
                    new_end_height:=Min(maximum_height,#S - MARGIN - 1);
                    new_pairs:=&cat[[[N,r] : N in [1..height],r in [1..height] |
                            (N + 1) * (r + 1) eq height and N ge minimum_N and
                            r ge minimum_r and N le maximum_N and
                            r le maximum_r] :
                            height in [new_start_height..new_end_height]];
        		    if have_candidate then
            			pairs_to_try cat:= [I : I in new_pairs |
                            (not I in pairs_to_try) and I[1] lt candidate_N];
        		    else
            			pairs_to_try cat:= [I : I in new_pairs |
                            (not I in pairs_to_try)];
        		    end if;
                end if;
            end if;
            // How many operators have we found? If it's only one, great.
            // Otherwise we need to do more work.
    	    if #diffs eq 1 then
                vprintf Fanosearch: "-> Found 1 operator.\n";
                // Is it better than our candidate operator?
                if have_candidate then
                    if N lt candidate_N or
                       N eq candidate_N and r lt candidate_r then
                        candidate:=psi(diffs[1]);
                        candidate_N:=N;
                        candidate_r:=r;
                    end if;
                else
                    have_candidate:=true;
                    candidate:=psi(diffs[1]);
                    candidate_N:=N;
                    candidate_r:=r;
                end if;
        		// Now delete pairs (N,r) that are worse than our current
                // candidate
        		vprintf Fanosearch: "-> Search restricted to N<%o or N=%o and r<%o.\n",candidate_N,candidate_N,candidate_r;
                pairs_to_try:=[I : I in pairs_to_try | I[1] lt candidate_N or
                        (I[1] eq candidate_N and I[2] lt candidate_r)];
    	    else
        		// We have more than one operator, so we know for sure that N
                // is too high
                vprintf Fanosearch: "-> Found %o operators.\n-> Search restricted to N<%o.\n",#diffs,N;
        		// Delete pairs (N',r') with N' ge N
                pairs_to_try:=[I : I in pairs_to_try | I[1] lt N];
            end if;
            vprintf Fanosearch: "-> %o more pairs to try.\n",#pairs_to_try;
        end if;
    end while;
    // Did we find a candidate?
    if have_candidate then return true,candidate; end if;
    return false,_;
end function;

// Given a P-F operator L, returns true if the factors of L(0) are all linear.
// If true, also returns true if L is of manifold type, and false if L is of
// orbifold type.
function is_of_type(L)
    // Extract L(0) from L
    R:=PolynomialRing(Rationals());
    L0:=Evaluate(L,[R.1,0]);
    // Check that the factors are all linear
    facts:=Factorization(L0);
    if not &and[Degree(F[1]) le 1 : F in facts] then return false,_; end if;
    // If the roots are integers, it's of manifold type, otherwise it's of
    // orbifold type.
    bool:=&and[&and[IsIntegral(c) : c in Coefficients(F[1])] : F in facts];
    return true,bool;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic PicardFuchsOperator( S::SeqEnum[RngIntElt] :
    extend:=false,
    minimum_N:=1,
    maximum_N:=false,
    minimum_r:=1,
    maximum_r:=false,
    maximum_height:=false ) -> AlgFPElt
{}
    ChangeUniverse(~S, Rationals());
    return PicardFuchsOperator(S:
        extend:=extend,
        minimum_N:=minimum_N,
        maximum_N:=maximum_N,
        minimum_r:=minimum_r,
        maximum_r:=maximum_r,
        maximum_height:=maximum_height
        );
end intrinsic;

intrinsic PicardFuchsOperator( S::SeqEnum[FldRatElt] :
    extend:=false,
    minimum_N:=1,
    maximum_N:=false,
    minimum_r:=1,
    maximum_r:=false,
    maximum_height:=false ) -> AlgFPElt
{}
    // Sanity check
    require IndexOfFirstUndefined(S) eq 0:
        "Argument must not contain undefined elements.";
    bool,maximum_height,maximum_N,maximum_r:=validate_calculate_annihilator(S :
        extend:=extend,
        minimum_N:=minimum_N,
        minimum_r:=minimum_r,
        maximum_height:=maximum_height,
        maximum_N:=maximum_N,
        maximum_r:=maximum_r);
    require bool: maximum_height;   // (second return value is an error msg)
    // Let calculate_annihilator do the work
    bool,L:=calculate_annihilator(S :
        extend:=extend,
        minimum_N:=minimum_N,
        minimum_r:=minimum_r,
        maximum_height:=maximum_height,
        maximum_N:=maximum_N,
        maximum_r:=maximum_r);
    // Any luck?
    require bool: "Unable to find a Picard-Fuchs operator. There may not be enough terms in the period sequence, or you may need to adjust the parameters 'minimum_N', 'minimum_r', or 'maximum_height'.";
    // Normalise the coefficients and return
    return normalise_operator_coeffs(L);
end intrinsic;

intrinsic PicardFuchsOperator( S::SeqEnum[RngIntElt],  N::RngIntElt,
r::RngIntElt ) -> AlgFPElt
{}
    ChangeUniverse(~S, Rationals());
    return PicardFuchsOperator(S, N, r);
end intrinsic;

intrinsic PicardFuchsOperator( S::SeqEnum[FldRatElt], N::RngIntElt,
    r::RngIntElt ) -> AlgFPElt
{Attempts to compute the Picard-Fuchs operator (as an element of the Weyl algebra W<D,t>) for the given period sequence S.}
    // Sanity check
    require IndexOfFirstUndefined(S) eq 0:
        "Argument 1 must not contain undefined elements.";
    require N ge 1: "Argument 2 must be a positive integer.";
    require r ge 1: "Argument 3 must be a positive integer.";
    bool,msg:=validate_calculate_annihilator(S :
        minimum_N:=N, maximum_N:=N,
        minimum_r:=r, maximum_r:=r);
    require bool: msg;
    // Let calculate_annihilator do the work
    bool,L:=calculate_annihilator(S :
        minimum_N:=N, maximum_N:=N,
        minimum_r:=r, maximum_r:=r);
    // Any luck?
    require bool: "Unable to find a Picard-Fuchs operator. There may not be enough terms in the period sequence, or you may need to adjust the values of N and r.";
    // Normalise the coefficients and return
    return normalise_operator_coeffs(L);
end intrinsic;

intrinsic PicardFuchsOperatorUsingGroebnerBasis( S::SeqEnum[RngIntElt],
    N::RngIntElt, r::RngIntElt : use_sage:=false ) -> AlgFPElt
{Attempts to use Groebner basis techniques to compute the Picard-Fuchs operator (as an element of the Weyl algebra W<D,t>) for the given period sequence S. The optional parameter 'use_sage' can be used to perform the Groenber basis computation using Sage (default: false).}
    // Sanity check
    require IndexOfFirstUndefined(S) eq 0:
        "Argument 1 must not contain undefined elements.";
    require N ge 1: "Argument 2 must be a positive integer.";
    require r ge 1: "Argument 3 must be a positive integer.";
    bool,msg:=validate_N_and_r_values(S,N,r);
    require bool: msg;
    require Type(use_sage) eq BoolElt:"Parameter 'use_sage' must be a boolean.";
    // Provide some feedback
    vprintf Fanosearch: "Computing annihilator...\n";
	cpu_t:=Cputime();
    // Compute the operator
    diffs:=annihilator(S,N,r);
    require #diffs ne 0:
        "No Picard-Fuchs operator found with these values of N and r.";
    // Provide some feedback
    vprintf Fanosearch: "Time: %os\nFound %o operators.\nComputing Groebner basis...\n",Cputime(cpu_t),#diffs;
	cpu_t:=Cputime();
    // Be careful!  At this point elements of diffs are polynomials, not
    // elements of the Weyl algebra
    if use_sage then
    	// Call out to Sage to compute the Groebner basis
    	_<t,D>:=Universe(diffs);
    	cmd:=Sprintf("sage -c \"F.<t,D> = FreeAlgebra(QQ,2);G = F.g_algebra({D*t:t*D+t});G.inject_variables(verbose=False);I = G.ideal(%o);print [L for L in I.std().gens()]\"",diffs);
        bool,res:=PipeCommand(cmd);
        require bool: "Error calling out to Sage";
        // Convert the result into the Weyl algebra
    	W:=WeylAlgebra(CoefficientRing(Universe(diffs)));
        D:=W.1;
        t:=W.2;
    	G:=eval res;
        // Provide some feedback
    	vprintf Fanosearch: "Time: %os\n",Cputime(cpu_t);
    else
        // Compute the Groebner basis
    	_,_,psi:=WeylAlgebra(CoefficientRing(Universe(diffs)));
    	G:=GroebnerBasisInWeylAlgebra([psi(L) : L in diffs]);
        // Provide some feedback
        vprintf Fanosearch: "Time: %os\n",Cputime(cpu_t);
    end if;
    // Assert that the ideal was principal
    require #G eq 1:
        Sprintf("Very interesting! A non-principal Picard-Fuchs ideal: %o",G);
    // Normalise the coefficients and return
    return normalise_operator_coeffs(G[1]);
end intrinsic;

intrinsic RecurrenceRelation( f::AlgFPElt ) -> SeqEnum[RngUPolElt]
{For a differential operator f in W<D,t>, gives the sequence of polynomials c_i such that \\sum c_i(n) a_\{n+2-i\} = 0, where a_1,a_2,... are the coefficients of the period sequence annihilated by f. We use the convention that a_j = 0 when j < 1.}
	// Sanity check
	bool,phi:=InternalIsWeylAlgebra(Parent(f));
	require bool: "Argument 1 must be an element of the Weyl algebra W<D,t>.";
	// Build the polynomial ring in which the relations live
    f:=phi(f);
    cs,es:=CoefficientsAndExponents(f);
    R:=PolynomialRing(Universe(cs));
    RR:=PolynomialRing(R);
	// Calculate the relations
    rel:=RR ! 0;
    for i in [1..#cs] do
        rel +:= cs[i] * (R.1 - es[i][1])^es[i][2] * RR.1^(es[i][1] + 1);
    end for;
    return [Coefficient(rel,i) : i in [1..Degree(rel)]];
end intrinsic;

intrinsic ApplyPicardFuchsOperator( f::AlgFPElt, S::SeqEnum ) -> SeqEnum
{Apply the differential operator f to the sequence S, where f is assumed to be an element of the Weyl algebra W<D,t>.}
    // Sanity check
    bool,phi:=InternalIsWeylAlgebra(Parent(f));
    require bool: "Argument 1 must be an element of the Weyl algebra W<D,t>.";
    require IndexOfFirstUndefined(S) eq 0:
        "Argument 2 must not contain undefined elements.";
    require not IsNull(S): "Illegal null sequence.";
    // Is there anything to do?
    if #S eq 0 then return S; end if;
    // Extract the coefficients and exponents of f
    cs,es:=CoefficientsAndExponents(phi(f));
    // Record the universe of S and try to coerce into W's coefficient ring
	uni:=Universe(S);
	bool,SS:=CanChangeUniverse(S,Universe(cs));
	if bool then S:=SS; end if;
    // Perform the appropriate shifts to S
    bool,S:=apply_operator_to_sequence(cs,es,S);
    require bool: S;
    // Try to coerce back to the original universe, and return
    bool,SS:=CanChangeUniverse(S,uni);
	return bool select SS else S;
end intrinsic;

intrinsic PeriodSequenceForPicardFuchsOperator( f::AlgFPElt, k::RngIntElt,
    S::SeqEnum ) -> SeqEnum
{}
    // Sanity check
    require InternalIsWeylAlgebra(Parent(f)):
        "Argument 1 must be an element of the Weyl algebra W<D,t>.";
    require k ge 1: "Argument 2 must be a non-negative integer.";
    require not IsNull(S): "Illegal null sequence.";
    // Record the universe of S and try to coerce into W's coefficient ring
	uni:=Universe(S);
	bool,SS:=CanChangeUniverse(S,CoefficientRing(Parent(f)));
	if bool then S:=SS; end if;
    // Compute the period sequence
    bool,S:=construct_sequence(f,k,S);
    require bool: S;
    // Try to coerce back to the original universe, and return
    bool,SS:=CanChangeUniverse(S,uni);
	return bool select SS[1..k + 1] else S[1..k + 1];
end intrinsic;

intrinsic PeriodSequenceForPicardFuchsOperator( f::AlgFPElt, k::RngIntElt )
    -> SeqEnum
{The first k + 1 terms of the period sequence associated with the Picard-Fuchs operator f, where f is assumed to be an element of the Weyl algebra W<D,t>.}
    // Sanity check
    require InternalIsWeylAlgebra(Parent(f)):
        "Argument 1 must be an element of the Weyl algebra W<D,t>.";
    require k ge 1: "Argument 2 must be a non-negative integer.";
    // Compute the period sequence
    bool,S:=construct_sequence(f,k,[CoefficientRing(Parent(f)) | 1]);
    require bool: S;
    // Can we coerce down to the integers?
    bool,SS:=CanChangeUniverse(S,Integers());
	return bool select SS else S;
end intrinsic;

intrinsic IsManifoldType( f::AlgFPElt ) -> BoolElt
{True iff the Picard-Fuchs operator f is of manifold type (f is assumed to be an element of the Weyl algebra W<D,t>).}
    // Sanity check
    require Parent(f) cmpeq WeylAlgebra(): "Argument must be an element of the Weyl algebra W<D,t> defined over the rationals.";
    // Calculate the type
    bool,type:=is_of_type(f);
    return bool and type;
end intrinsic;

intrinsic IsOrbifoldType( f::AlgFPElt ) -> BoolElt
{True iff the Picard-Fuchs operator f is of orbifold type (f is assumed to be an element of the Weyl algebra W<D,t>).}
    // Sanity check
    require Parent(f) cmpeq WeylAlgebra(): "Argument must be an element of the Weyl algebra W<D,t> defined over the rationals.";
    // Calculate the type
    bool,type:=is_of_type(f);
    return bool and not type;
end intrinsic;

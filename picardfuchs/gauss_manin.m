freeze;

/////////////////////////////////////////////////////////////////////////
// gauss_manin.m
/////////////////////////////////////////////////////////////////////////

import "../utilities/polynomial.m": get_polynomial_ring;
import "lairez.m": validate_picard_fuchs_args, prepare_fraction;

procedure timing_push(S)
//    printf "Entering: %o\n",S;
    TimingPush(S);
end procedure;

procedure timing_pop()
//    printf "Exiting\n";
    TimingPop();
end procedure;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Here R = K(t) for K a field (which in practice will be either QQ or F_p)
// and pts and vals are parallel sequences of elements of K such that g(pts[i]) = vals[i]
// for some rational function g. Attempts to reconstruct g in R = K(t). Returns true
// on success, false otherwise. If true, also returns g.
function reconstruct_by_interpolation(pts,vals,R)
    timing_push("reconstruct_by_interpolation");
    // Sanity check
    assert #pts eq #vals;
    // The maximum degree that we can recover (common to numerator and denominator)
    maxdeg:=Floor((#pts - 2) / 2);
    // Solve the linear algebra problem
    K := CoefficientRing(R);
    lhs:=[[pts[i]^j : j in [0..maxdeg]] cat
	  [-pts[i]^j * vals[i] : j in [0..maxdeg]] : i in [1..#pts]];
    ker:=Kernel(Transpose(Matrix(K,lhs)));
    if Dimension(ker) eq 0 then
	timing_pop();
	return false, _;
    end if;
    // we can use any element of the kernel to rebuild, so use the first one
    rhs:=Eltseq(ker.1);
    // Rebuild the numerator and denominator and return
    t:=R.1;
    num:=&+[R | rhs[i] * t^(i - 1) : i in [1..maxdeg + 1]];
    den:=&+[R | rhs[maxdeg + 1 + i] * t^(i - 1) : i in [1..maxdeg + 1]];
    ////////////////////////////////////////////////////////////////////////////////
    timing_pop();
    ////////////////////////////////////////////////////////////////////////////////
    return true,num / den;
end function;

// Here R = K(t) where K is a field (in practice either K=QQ or K=F_p)
// and pts and vectors are parallel sequences of element of K and "vectors in K^n" (actually sequences)
// such that g_j(pts[i]) = vectors[i][j]. Attempts to reconstruct the g_j in R = K(t).
// Returns true on success, false otherwise. If true, also returns the sequence [g_1,...,g_k].
function reconstruct_by_interpolation_for_vector(pts,vectors,R)
    // Sanity check
    assert #pts eq #vectors;
    len:=#Representative(vectors);
    assert &and[#S eq len : S in vectors];
    // We reconstruct by interpolation, entrywise
    result:=[R|];
    for i in [1..len] do
	bool,entry:=reconstruct_by_interpolation(pts,[S[i] : S in vectors],R);
	if not bool then return false,_; end if;
	Append(~result,entry);
    end for;
    return true,result;
end function;

// Here R = K(t) where K is a field (in practice either K=QQ or K=F_p)
// and pts and mats are parallel sequences of elements of K and matrices over K such that
// M(pts[i]) = mats[i], where M is a matrix with entries in R = K(t). Attempts to
// reconstruct the entries of M. Returns true on success, false otherwise. If
// true, also returns M.
function reconstruct_by_interpolation_for_matrix(pts,mats,R)
    // Sanity check
    assert #pts eq #mats;
    nrows:=NumberOfRows(mats[1]);
    assert &and[NumberOfRows(mat) eq nrows : mat in mats];
    ncols:=NumberOfColumns(mats[1]);
    assert &and[NumberOfColumns(mat) eq ncols : mat in mats];
    // We reconstruct by interpolation, entrywise
    K := CoefficientRing(R);
    M:=ZeroMatrix(R,nrows,ncols);
    for i in [1..nrows] do
	for j in [1..ncols] do
	    vals:=[K | mat[i,j] : mat in mats];
	    bool,entry:=reconstruct_by_interpolation(pts,vals,R);
	    if not bool then return false,_; end if;
	    M[i,j]:=entry;
	end for;
    end for;
    return true,M;
end function;

// given a sequence [* r_1,...,r_k *] of elements of F_{p_1},...,F_{p_k} respectively
// where p_1,...,p_k are primes
// use the Chinese Remainder Theorem to lift to an element of ZZ/mZZ where m is
// the product p_1*...*p_k
// and then use rational reconstruction to find the element n/d in QQ with n and d
// less than sqrt(m) which reduces to r_i mod p_i
// returns true, n/d on success; false, _ otherwise
function rational_reconstruction(reductions)
    TimingPush("rational reconstruction (scalar)");
    error if #reductions eq 0, "need a non-empty list of reductions";
    // deal with the easy case
    if &and[IsZero(r) : r in reductions] then
	return true, Zero(Rationals());
    end if;
    primes := [Characteristic(Parent(r)) : r in reductions];
    ZZ_lifts := [Integers()!r : r in reductions];
    crt_lift := ChineseRemainderTheorem(ZZ_lifts,primes);
    R := ResidueClassRing(&*primes);
    TimingPop();
    return RationalReconstruction(R!crt_lift);
end function;

// argument QQt should be QQ(t) or QQ[t]
// given a sequence [* f_1,...,f_k *] of elements of F_{p_1}[t],...,F_{p_k}[t] respectively
// where p_1,...,p_k are primes
// use the Chinese Remainder Theorem and rational reconstruction (coefficientwise) to try
// to find f in QQt such that f reduces to f_i mod p_i
// returns true, f on success; false, _ otherwise
function rational_reconstruction_for_polynomial(reductions,QQt)
    TimingPush("rational reconstruction (polynomial)");
    error if #reductions eq 0, "need a non-empty list of reductions";
    // special case the zero sequence, as otherwise the fact that 0 has negative degree will break things below
    if &and[IsZero(r) : r in reductions] then
	return true, Zero(QQt);
    end if;
    degree := Max([Degree(f) : f in reductions]);
    css := [* Coefficients(f) cat [Zero(CoefficientRing(Parent(f))) : k in [1..Degree(f)-degree]] : f in reductions *];
    lifted_coeffs := ZeroSequence(Rationals(),degree);
    for id in [1..degree+1] do
	bool, coeff := rational_reconstruction([* cs[id] : cs in css *]);
	if bool then
	    lifted_coeffs[id] := coeff;
	else
//	    printf "rational reconstruction (polynomial) failed for: %o\n", reductions;
	    return false, _;
	end if;
    end for;
    TimingPop();
    return true, QQt!lifted_coeffs;
end function;


// argument QQt should be QQ(t)
// given a sequence [* f_1,...,f_k *] of elements of F_{p_1}(t),...,F_{p_k}(t) respectively
// where p_1,...,p_k are primes
// use the Chinese Remainder Theorem and rational reconstruction (coefficientwise) to try
// to find f in QQt such that f reduces to f_i mod p_i
// returns true, f on success; false, _ otherwise
function rational_reconstruction_for_rational_function(reductions,QQt)
    TimingPush("rational reconstruction (rational function)");
    error if #reductions eq 0, "need a non-empty list of reductions";
    numerators := [* Numerator(f) : f in reductions *];
    denominators := [* Denominator(f) : f in reductions *];
    bool, num := rational_reconstruction_for_polynomial(numerators,RingOfIntegers(QQt));
    if not bool then TimingPop(); return false, _; end if;
    bool, den := rational_reconstruction_for_polynomial(denominators,RingOfIntegers(QQt));
    TimingPop();
    if bool and not IsZero(den) then
	return true, num/den;
    else
	return false, _;
    end if;
end function;

// argument QQt should be QQ(t)
// given a sequence [* S_1,...,S_k *] of sequences of elements of F_{p_1}(t),...,F_{p_k}(t) respectively
// where p_1,...,p_k are primes
// use the Chinese Remainder Theorem and rational reconstruction (coefficientwise) to try
// to find a sequence S of elements of QQt such that S reduces to S_i mod p_i
// returns true, S on success; false, _ otherwise
function rational_reconstruction_for_sequence_of_rational_functions(reductions,QQt)
    TimingPush("rational reconstruction (sequence)");
    error if #reductions eq 0, "need a non-empty list of reductions";
    len := #reductions[1];
    error if not &and[#S eq len : S in reductions], "The sequences must all have be same length";
    result := ZeroSequence(QQt,len);
    for idx in [1..len] do
	bool, entry := rational_reconstruction_for_rational_function([* S[idx] : S in reductions *],QQt);
	if bool then
	    result[idx] := entry;
	else
//	    printf "rational reconstruction (sequence) failed for entry: %o\n", [* S[idx] : S in reductions *];
	    return false, _;
	end if;
    end for;
    TimingPop();
    return true, result;
end function;

// argument QQt should be QQ(t)
// given a sequence [* M_1,...,M_k *] of matrices over F_{p_1}(t),...,F_{p_k}(t) respectively
// where p_1,...,p_k are primes
// use the Chinese Remainder Theorem and rational reconstruction (coefficientwise) to try
// to find a matrix M over QQt such that M reduces to M_i mod p_i
// returns true, M on success; false, _ otherwise
function rational_reconstruction_for_matrix_of_rational_functions(reductions,QQt)
    TimingPush("rational reconstruction (matrix)");
    error if #reductions eq 0, "need a non-empty list of reductions";
    num_rows := NumberOfRows(reductions[1]);
    num_cols := NumberOfColumns(reductions[1]);
    error if (not &and[NumberOfRows(M) eq num_rows : M in reductions]) or
	  (not &and[NumberOfColumns(M) eq num_cols : M in reductions]), "The matrices must all have be same size";
    result := ZeroMatrix(QQt,num_rows,num_cols);
    for i in [1..num_rows] do
	for j in [1..num_cols] do
	    bool, entry := rational_reconstruction_for_rational_function([* M[i,j] : M in reductions *],QQt);
	    if bool then
		result[i,j] := entry;
	    else
//		printf "rational reconstruction (matrix) failed for entry: %o\n", [* M[i,j] : M in reductions *];
		return false, _;
	    end if;
	end for;
    end for;
    TimingPop();
    return true, result;
end function;


/* // THINK we don't need this any more, because now we are working over F_p[t] and F_p */
/* // not QQ[t] and QQ.  (We reconstruct the matrices over QQ[t] right at the end.) */
/* // */
/* // given a matrix M over the rationals, computes a candidate for the echelon form */
/* // by using a combination of Chinese Remainder Theorem and rational reconstruction */
/* // we return once adding another prime doesn't change the reconstruction */
/* function echelon_rebuild(M) */
/*     // the primes and the echelonized reductions mod p */
/*     used_primes:={Integers()|}; */
/*     primes := [Integers()|]; */
/*     reductions := [ ]; */
/*     stabilised := false; */
/*     last_lift := false; */
/*     repeat */
/* 	p := RandomPrime(32); */
/* 	if p in used_primes then */
/* 	    // get another prime, then */
/* 	    continue; */
/* 	end if; */
/* 	Include(~used_primes,p); */
/* 	E:=EchelonForm(ChangeRing(M,GF(p))); */
/* 	RemoveZeroRows(~E); */
/* 	if #reductions eq 0 or NumberOfRows(E) eq NumberOfRows(reductions[1]) then */
/* 	    Append(~reductions,ChangeRing(E,Integers())); */
/* 	    Append(~primes, p); */
/* 	    printf "Current prime: %o.  Using %o primes.\n",p,#primes; */
/* 	elif NumberOfRows(E) gt NumberOfRows(reductions[1]) then */
/* 	    reductions:=[ ChangeRing(E,Integers()) ]; */
/* 	    primes:=[p]; */
/* 	    printf "Oops!  Bad reduction.  Resetting.\n"; */
/* 	else */
/* 	    continue; */
/* 	end if; */
/* 	// build a lift of E using the Chinese Remainder Theorem */
/* 	lift_E := ChineseRemainderTheorem(reductions,primes); */
/* 	entries:=[Rationals()|]; */
/* 	success:=true; */
/* 	R:=ResidueClassRing(&*primes); */
/* 	for c in Eltseq(lift_E) do */
/* 	    bool,d:=RationalReconstruction(R ! c); */
/* 	    if not bool then success:=false; break; end if; */
/* 	    Append(~entries,d); */
/* 	end for; */
/* 	if not success then continue; end if; */
/* 	reconstruct_E := Matrix(Rationals(),Nrows(lift_E),Ncols(lift_E),entries); */
/* 	stabilised := last_lift cmpeq reconstruct_E; */
/* 	last_lift := reconstruct_E; */
/*     until stabilised; */
/*     return last_lift; */
/* end function; */

// polys is a sequence of elements of K[u,v,x_0,...,x_n] where K is a field (in practice, K=F_p)
// return them in "reduced row echelon form", that is, write them as
// a matrix using polys_to_matrix above, put that matrix into reduced
// row echelon form, and rewrite the reduced matrix as a sequence of
// polynomials.  This replaces each element of polys by a LC of elements of
// polys such that the corresponding matrix is in reduced row-echelon form.
// Zero elements of the resulting sequence are omitted.
function echelonize(polys)
    ////////////////////////////////////////////////////////////////////////////////
    // remove duplicates and zero rows
    ////////////////////////////////////////////////////////////////////////////////
    polys := SetToSequence(Exclude(SequenceToSet(polys),Zero(Universe(polys))));
    if #polys eq 0 then
	return polys;
    end if;
    ////////////////////////////////////////////////////////////////////////////////
    timing_push("echelonize");
    ////////////////////////////////////////////////////////////////////////////////
    // now find M, monomials where M is a matrix and monomials
    // is a sequence of monomials such that the j-th element of
    // polys is sum_k M[j,k] * monomials[k]
    R := CoefficientRing(Universe(polys));
    // note the Reverse() here, so echelonization has the effect of zeroing highest-order terms first
    monomials := Reverse(Sort(SetToSequence(&join[SequenceToSet(Monomials(g)) : g in polys])));
    M := ZeroMatrix(R,#polys,#monomials);
//    print "echelonize", Characteristic(R), #polys, #monomials;
    for row_idx in [1..#polys] do
	cs, mons := CoefficientsAndMonomials(polys[row_idx]);
	for mon_idx in [1..#mons] do
	    M[row_idx,Index(monomials,mons[mon_idx])] := cs[mon_idx];
	end for;
    end for;
    // now echelonize and remove trailing zero rows
//    timing_push("EchelonForm via mod p reduction");
//    M1 := echelon_rebuild(M);
//    timing_pop();
//    TimingPrint();

    timing_push("EchelonForm");
    M := EchelonForm(M);
    timing_pop();
    RemoveZeroRows(~M);
    timing_pop();
    return [Universe(polys)| Polynomial(row,monomials) : row in RowSequence(M)];
end function;


// Given evaluated_f homogeneous in K[u,v,x_0,x_1,...,x_n] where K is a field
// (in practice K=F_p)
// returns the jacobian ideal and the ideal of trivial syzygies
function jacobian_and_syzygies(evaluated_f)
    timing_push("jacobian_and_syzygies");
    Suv := Parent(evaluated_f);
    m := Rank(Suv)-2;  // m=n+1
    u := Suv.1;
    v := Suv.2;
    // make the Jacobian ideal
    derivatives := [Suv| Derivative(evaluated_f, i+2) : i in [1..m]];
    variables := [Suv|Suv.(i+2) : i in [1..m]];
    generators := [Suv| u^(m-i)*v^i : i in [1..m] ];
    high_order := [Suv| u^i*v^(m+1-i) : i in [0..m+1] ];  // extra monomials to kill all forms of degree m+1
    jacobian_ideal := ideal<Suv| [ derivatives[i]*u^m - generators[i] : i in [1..m]] cat high_order >;
    // compute a basis of trivial syzygies
    trivial_syz_ideal := ideal<Suv| [Suv|derivatives[i]*generators[j] - derivatives[j]*generators[i] : i, j in [1..m]]
			       cat high_order>;
    timing_pop();
    return jacobian_ideal, trivial_syz_ideal;
end function;

// a is monomial, B is a list of monomials, vars is a list of variables
// deg is an integer
// returns the set of monomials of degree d which are products of a with a monomial
// in vars but which are not divisible by an element of B
function diff_stairs( a, B, vars, deg : first:=true)
    if first then timing_push("diff_stairs"); end if;
    Suv := Parent(a);
    // the trivial cases
    if deg lt Degree(a) then
	if first then timing_pop(); end if;
	return { Suv | };
    elif deg eq Degree(a) then
	normal_form := NormalForm(a,B);
	if first then timing_pop(); end if;
	return IsZero(normal_form) select { Suv | } else {normal_form};
    end if;
    // so recurse
    lower_degree_monomials := $$(a, B, vars, deg-1 : first:=false);
    this_degree_monomials := {Suv| v*e : v in vars, e in lower_degree_monomials };
    normal_forms := NormalForm(this_degree_monomials, B);
    Exclude(~normal_forms,Zero(Suv));
    if first then timing_pop(); end if;
    return normal_forms;
end function;

// A is a list of polynomials, B is a list of monomials, vars is a list of variables, deg is a degree
//
// returns a maximal set of polynomials p of the indicated degree which are a product of a monomial
// with an element of A  such that the leading monomial of p is not divisible by any element of
// B, and such that no two elements of A have the same leading monomial
//
// put differently: A generates a module M_A, B generates a module M_B, in the circumstances
// where this is called, M_B subset M_A, and this function returns a
// basis for M_A/M_B at degree deg
//
// recall that we are encoding modules as rings, so we also pass in the variables
// vars so as to know which monomials we are "really allowed" to multiply by
function basis_at_degree( A , B, vars, deg )
  result := [ Universe(A) | ];
  for a in A do
      lm := LeadingMonomial(a);
      result cat:= [ (d div lm)*a : d in diff_stairs(lm, B, vars, deg) ];
      Append(~B, lm);
  end for;
  return Sort(result);
end function;

// internal helper procedure to set the basis for U_r_q
procedure set_basis_U_r_q(r,q,basis,cache)
    timing_push("set_basis_U_r_q");
    U_basis_defined, U_basis := StoreIsDefined(cache,"U_basis");
    if not U_basis_defined then
	U_basis := AssociativeArray(PowerSequence(Integers()));
    end if;
    U_basis[[r,q]] := basis;
    StoreSet(cache,"U_basis",U_basis);
    timing_pop();
end procedure;

// internal helper function to return the basis for U_r_q if known
// returns true, basis if known; false, _ otherwise
function fetch_basis_U_r_q(r,q,cache)
    timing_push("fetch_basis_U_r_q");
    U_basis_defined, U_basis := StoreIsDefined(cache,"U_basis");
    if U_basis_defined then
	result_defined, result := IsDefined(U_basis,[r,q]);
	if result_defined then
	    timing_pop();
	    return true, result;
	end if;
    end if;
    timing_pop();
    return false, _;
end function;

// internal helper procedure to set the basis for Wdown_r_q
procedure set_basis_Wdown_r_q(r,q,basis,cache)
    timing_push("set_basis_Wdown_r_q");
    Wdown_basis_defined, Wdown_basis := StoreIsDefined(cache,"Wdown_basis");
    if not Wdown_basis_defined then
	Wdown_basis := AssociativeArray(PowerSequence(Integers()));
    end if;
    Wdown_basis[[r,q]] := basis;
    StoreSet(cache,"Wdown_basis",Wdown_basis);
    timing_pop();
end procedure;

// internal helper function to return the basis for Wdown_r_q if known
// returns true, basis if known; false, _ otherwise
function fetch_basis_Wdown_r_q(r,q,cache)
    timing_push("fetch_basis_Wdown_r_q");
    Wdown_basis_defined, Wdown_basis := StoreIsDefined(cache,"Wdown_basis");
    if Wdown_basis_defined then
	result_defined, result := IsDefined(Wdown_basis,[r,q]);
	if result_defined then
	    timing_pop();
	    return true, result;
	end if;
    end if;
    timing_pop();
    return false, _;
end function;

// here alpha in K[u,v,x_0,...,x_n] where K is a field (in practice K=F_p) is
// of the form sum_{i=0}^{i=n} b_i*xi_i where xi_i = v^{i+1} u^{n-i}
// returns omega*sum_{i=0}^{i=n} partial_i b_i where omega = u^(n+1)
function ext_diff(alpha)
    ////////////////////////////////////////////////////////////////////////////////
    // deal with the zero case quickly
    ////////////////////////////////////////////////////////////////////////////////
    if IsZero(alpha) then
	return alpha;
    end if;
    timing_push("ext_diff");
    ////////////////////////////////////////////////////////////////////////////////
    // grab the parent ring Suv = QQ[u,v,x_0,...,x_n]
    ////////////////////////////////////////////////////////////////////////////////
    Suv := Parent(alpha);
    u := Suv.1;
    v := Suv.2;
    m := Rank(Suv)-2; // this is n+1
    ////////////////////////////////////////////////////////////////////////////////
    // sanity checks
    ////////////////////////////////////////////////////////////////////////////////
    cs := Coefficients(alpha,v);
    cs cat:= ZeroSequence(Universe(cs),m+1-#cs);
    assert cs[1] eq 0;
    assert Degree(Evaluate(alpha,[u,v] cat [Suv|1 : i in [1..m]])) eq m or IsZero(Evaluate(alpha,[u,v] cat [Suv|1 : i in [1..m]]));
    ////////////////////////////////////////////////////////////////////////////////
    // return d alpha
    ////////////////////////////////////////////////////////////////////////////////
    // THINK remove this (debugging)
    g := &+[Suv| u^(i+1)*Derivative(cs[i+2],Suv.(i+3)) : i in [0..m-1]];
    assert Degree(Evaluate(g,[u,v] cat [Suv|1 : i in [1..m]])) eq m or IsZero(Evaluate(g,[u,v] cat [Suv|1 : i in [1..m]]));
    timing_pop();
    return g;
end function;

// alpha lies in K[u,v,x_0,...,x_n] where K is a field (in practice K=F_p)
// jacobian is the jacobian ideal from jacobian_and_syzygies() above
// the normal form of alpha against the jacobian ideal
// is of the form a*omega + sum_{i=0}^{i=n} b_i*xi_i where
// omega = u^{n+1} and xi_i = v^{i+1} u^{n-i}
// we return omega*(a + sum_{i=0}^{i=n} partial_i b_i)
function normal_form_and_diff(alpha,jacobian)
    ////////////////////////////////////////////////////////////////////////////////
    // deal with the zero case quickly
    ////////////////////////////////////////////////////////////////////////////////
    if IsZero(alpha) then
	return alpha;
    end if;
    ////////////////////////////////////////////////////////////////////////////////
    // grab the parent ring Suv = K[u,v,x_0,...,x_n]
    ////////////////////////////////////////////////////////////////////////////////
    timing_push("normal_form_and_diff");
    Suv := Parent(alpha);
    u := Suv.1;
    v := Suv.2;
    n := Rank(Suv)-3;
    ////////////////////////////////////////////////////////////////////////////////
    // sanity check
    ////////////////////////////////////////////////////////////////////////////////
    assert Degree(alpha,1) eq n+1 or IsZero(alpha);
    // assert Degree(Evaluate(alpha,[u,v] cat [Suv|1 : i in [0..n]])) eq n+1 or IsZero(Evaluate(alpha,[u,v] cat [Suv|1 : i in [0..n]]));
    ////////////////////////////////////////////////////////////////////////////////
    // do the calculation
    ////////////////////////////////////////////////////////////////////////////////
    timing_push("NormalForm");
    alpha := NormalForm(alpha,jacobian);
    timing_pop();
    // THINK remove this (debugging)
    assert Degree(Evaluate(alpha,[u,v] cat [Suv|1 : i in [0..n]])) eq n+1 or IsZero(Evaluate(alpha,[u,v] cat [Suv|1 : i in [0..n]]));

    cs := Coefficients(alpha,v);
    cs cat:= ZeroSequence(Universe(cs),n+2-#cs);
    // THINK remove this (debugging)
    g := cs[1] + &+[Suv| u^(i+1)*Derivative(cs[i+2],Suv.(i+3)) : i in [0..n]];
    assert Degree(g,1) eq n+1 or IsZero(g);
    // assert Degree(Evaluate(g,[u,v] cat [Suv|1 : i in [0..n]])) eq n+1 or IsZero(Evaluate(g,[u,v] cat [Suv|1 : i in [0..n]]));
    timing_pop();
    return g;
end function;

// r and q are integers
// r is the level we are in the generalized Griffiths--Dwork process
// q is some kind of degree // THINK
// deg_f is the degree of f
// Suv is the ring K[u,v,x_0,...,x_n] where K is a field (in practice K=F_p)
// jacobian_ideal and trivial_syz_ideal are ideals in Suv, specifically the
//     return values of jacobian_and_syzygies() above
// cache is an internal cache, used because this function is recursive
function basis_U_r_q(r,q,deg_f,Suv,jacobian_ideal,trivial_syz_ideal,cache)
    // do we know this already
    bool, result := fetch_basis_U_r_q(r,q,cache);
    if bool then
	return result;
    end if;
    ////////////////////////////////////////////////////////////////////////////////
    // base case
    ////////////////////////////////////////////////////////////////////////////////
    if r eq 0 then
	timing_push("basis_U_r_q r=0");
	basis := [Suv|];
	set_basis_U_r_q(r,q,basis,cache);
	timing_pop();
	return basis;
    end if;
    ////////////////////////////////////////////////////////////////////////////////
    // r=1, the usual Griffiths--Dwork case
    ////////////////////////////////////////////////////////////////////////////////
    if r eq 1 then
	timing_push("basis_U_r_q r=1");
	// get a basis of distinguished (non-trivial) syzygies
        // between partial_i f in the correct degree
	m := Rank(Suv) - 2;  // this is n+1
	u := Suv.1;
	timing_push("basis_U_r_q r=1 GroebnerBasis");
	syzygies := [Suv| g : g in GroebnerBasis(jacobian_ideal) | Degree(g,u) lt m ];
	trivial_syz_lm := Basis(LeadingMonomialIdeal(trivial_syz_ideal));
	timing_pop();
	variables := [Suv|Suv.(i+2) : i in [1..m]];
	relevant_syz := basis_at_degree(syzygies,trivial_syz_lm,variables,q-deg_f+1);
	// W^1_q is d of the relevant syzygies
	set_basis_Wdown_r_q(r,q,[Suv| ext_diff(s) : s in relevant_syz],cache);
	// r eq 1, so U^r_q is empty
 	basis := [Suv|];
	set_basis_U_r_q(r,q,basis,cache);
	timing_pop();
	return basis;
    end if;
    /////////////////////////////////////////////////////////////////////////////////
    // we don't know it, so compute it
    ////////////////////////////////////////////////////////////////////////////////
    // grab a basis for U^{r-1}_q
    basis_U_smaller := $$(r-1,q,deg_f,Suv,jacobian_ideal,trivial_syz_ideal,cache);
    // force the computation of and grab a basis for Wdown^{r-1}_{q+1}
    _ := $$(r-1,q+deg_f,deg_f,Suv,jacobian_ideal,trivial_syz_ideal,cache);
    timing_push("basis_U_r_q r>1");
    bool, basis_Wdown_smaller := fetch_basis_Wdown_r_q(r-1,q+deg_f,cache);
    assert bool;
    rels := [Suv| normal_form_and_diff(g,jacobian_ideal) : g in basis_Wdown_smaller];
    echelon_form := echelonize(basis_U_smaller cat rels);
    // now we extract from echelon forms the bases for U^r_q and Wdown^r_q
    // which are respectively the degree q and degree less than q parts
    basis_U := [Suv| g : g in echelon_form | Degree(g) eq q];
    basis_Wdown := [Suv| g : g in echelon_form | Degree(g) lt q];
    set_basis_U_r_q(r,q,basis_U,cache);
    set_basis_Wdown_r_q(r,q,basis_Wdown,cache);
    timing_pop();
    return basis_U;
end function;

// return [alpha]_r, the r-th reduction of alpha
// where alpha lies in K[u,v,x_0,...,x_n] with K a field (in practice K=F_p)
// deg_f is the degree of f
// Suv is the ring K[u,v,x_0,...,x_n]
// jacobian_ideal and trivial_syz_ideal are ideals in Suv, specifically the
//     return values of jacobian_and_syzygies() above
// cache is a Store used internally by basis_U_r_q() above
function reduce(alpha,r,deg_f,Suv,jacobian_ideal,trivial_syz_ideal,cache)
    timing_push("reduce");
    q := Degree(alpha);
    while q gt 0 do
	alpha := normal_form_and_diff(alpha,jacobian_ideal);
	basis := basis_U_r_q(r,q,deg_f,Suv,jacobian_ideal,trivial_syz_ideal,cache);
	// now subtract off a LC of elements of basis so as to zero the
	// coefficients in alpha of the leading terms in basis
	leading_exponents := [Exponents(LeadingMonomial(b)) : b in basis];
	leading_coefficients := [LeadingCoefficient(b) : b in basis];
	timing_push("reduce CoefficientsAndExponents");
	cs, es := CoefficientsAndExponents(alpha);
	timing_pop();
	for i in [1..#basis] do
	    idx := Index(es,leading_exponents[i]);
	    if idx ne 0 then
		alpha -:= cs[idx]/leading_coefficients[i]*basis[i];
	    end if;
	end for;
	q -:= deg_f;
    end while;
    timing_pop();
    return alpha;
end function;

// We are given a and f homogeneous in K[t][x_0,x_1,...,x_n] where K is a field (in practice K=F_p)
// a value for r, and an evaluation point pt,
// f_prime equal to the derivative of f w.r.t. t,
// Kt = K[t], R = K[t][x_0,...,x_n], and S = K[x_0,...,x_n].
// We now attempt to compute the connection matrix gm_matrix (or rather, the matrix of multiplication
// by f_prime evaluated at pt, which is what is left of the connection after
// evaluation), as well as rho_0 = [a_eval]_r and a basis for what is denoted by M in [Lairez] S7.2.2.
// The basis is recorded as a sorted sequence M_basis of exponents, one for each monomial.
// rho_0 is returned as a sequence of coefficients with respect to M_basis
// Returns true,gm_matrix,rho_0,M_basis on success, false,_,_,_ otherwise.
// (What can go wrong is that r isn't large enough, or pt is not general enough.
// In either of these cases r will be too small for the calculation to succeed.)
function gauss_manin_mod_p_for_fixed_t(f,f_prime,r,a,Kt,R,S,pt : certificate:=false)
    ////////////////////////////////////////////////////////////////////////////////
    timing_push("gauss_manin_mod_p_for_fixed_t");
    ////////////////////////////////////////////////////////////////////////////////
    m := Rank(S);  // m=n+1
    // Build the evaluation map t -> pt
    ev_Kt:=hom<Kt -> Parent(pt) | pt>;
    ev_R:=hom<R -> S | ev_Kt, [S.i : i in [1..m]]>;
    // Evaluate at the point pt
    f_eval:=ev_R(f);
    f_prime_eval:=ev_R(f_prime);
    a_eval:=ev_R(a);
    // put everything into the ring Suv=K[u,v,x_0,x_1,...,x_n]
    Suv := get_polynomial_ring(CoefficientRing(S),m+2,"grevlex");
    u := Suv.1;
    v := Suv.2;
    // phi maps S=K[x_0,x_1,...,x_n] into Suv=K[u,v,x_0,x_1,...,x_n]
    // in the obvious way
    phi := hom<S->Suv|[Suv.(i+2) : i in [1..m]]>;
    // psi maps Suv into S by evaluating at u=1, v=0
    // that is, it extracts the coefficient of omega = u^{n+1}
    psi := hom<Suv->S|[S|1,0] cat [S.i : i in [1..m]]>;
    // compute the jacobian ideal and the trivial syzygies
    // these are in the ring Suv:=K[u,v,x_0,...,x_n]
    jacobian_ideal, trivial_syz_ideal := jacobian_and_syzygies(phi(f_eval));
    // we will need a data cache for the (recursive) computation of bases for U^r_q
    cache := NewStore();
    ////////////////////////////////////////////////////////////////////////////////
    // now build the Gauss--Manin connection matrix (evaluated at t=pt)
    ////////////////////////////////////////////////////////////////////////////////
    // set basis := {}
    // compute the monomials in [a_eval]_r and add them to basis
    // then for each element b of basis, compute the monomials in [-b f_prime_eval]_r
    // and add them to the basis
    // repeat until basis stabilizes
    ////////////////////////////////////////////////////////////////////////////////
    // the reduction function:
    reduce_me:=func<x | psi(reduce(phi(x)*u^m,r,Degree(f_eval),Suv,jacobian_ideal,trivial_syz_ideal,cache))>;
    // the base case
    basis := [S|];
    gm_columns := [S|];   // this is [b*f_prime_eval : b in basis]
    monomials_to_add := Monomials(a_eval);
    // now loop until the basis stabilizes
    while #monomials_to_add ne 0 do
	basis cat:= monomials_to_add;
	unreduced_new_cols := [ -f_prime_eval*mon : mon in monomials_to_add];
	reduced_new_cols := [reduce_me(col) : col in unreduced_new_cols];
	if Max([Degree(col) : col in reduced_new_cols]) gt (m-1)*Degree(f_eval) then
	    // r is too small
	    ////////////////////////////////////////////////////////////////////////////////
	    timing_pop();
	    ////////////////////////////////////////////////////////////////////////////////
	    return false,_,_,_,_;
	end if;
	gm_columns cat:= reduced_new_cols;
	monomials_to_add := SetToSequence(&join[ SequenceToSet(Monomials(col)) : col in reduced_new_cols]
					  diff SequenceToSet(basis));
    end while;
    // the return value for basis is a sorted sequence of exponents
    basis_exponents := [PowerSequence(Integers())| Exponents(b) : b in basis];
    Sort(~basis_exponents,~perm);    // perm is the permutation that sorts basis_exponents
    basis := PermuteSequence(basis,perm);
    gm_columns := PermuteSequence(gm_columns,perm);
    // build the matrix
    K := CoefficientRing(S);
    gm_matrix := Matrix(K,
			[ [K| MonomialCoefficient(col,mono) : col in gm_columns ] : mono in basis]);

    reduced_a_eval := reduce_me(a_eval);
    rho_0 := [K| MonomialCoefficient(reduced_a_eval,mono) : mono in basis];
    timing_pop();
    return true,gm_matrix,rho_0,basis_exponents,false;
end function;

// Returns the most frequently occurring element s in the sequence S, along with the
// indices of S equal to s.
function most_common(S)
    Sdistinct:=SetToSequence(SequenceToSet(S));
    frequency:=ZeroSequence(Integers(),#Sdistinct);
    for s in S do
	frequency[Index(Sdistinct,s)] +:= 1;
    end for;
    _,idx:=Max(frequency);
    s:=Sdistinct[idx];
    return s,[Integers() | i : i in [1..#S] | S[i] eq s];
end function;


// Given a and f are homogeneous in K[t][x_0,x_1,...,x_n], where K=F_p for some p, and a value for r, attempts to compute
// the connection matrix gm_matrix with entries in K(t), the reduction rho_0 = [a]_r, and a basis for
// what is denoted by M in [Lairez] S7.2.2.
// The basis is recorded as a sorted sequence M_basis of exponents, one for each monomial.
// If the optional argument 'certificate' is set to true then we also calculate a
// certificate cert for gm_matrix   FIX ME THINK describe
// otherwise cert is set to 'false'
// Returns true,gm_matrix,rho_0,M_basis,cert on success, false,_,_,_,_ otherwise.
// (What can go wrong is that r isn't large enough.)
// we work by evaluating at t = t_0 for a sequence of values of t_0, and then reconstructing the result over F_p[t] by interpolation
function gauss_manin_mod_p(f,r,a : certificate:=false)
    TimingPush("gauss_manin_mod_p");
    // Collect the data we need
    R:=Parent(f); // F_p[t][x_0,...,x_n]
    Kpoly:=CoefficientRing(R); // F_p[t]
    Krat:=FieldOfFractions(Kpoly); // F_p(t)
    K := CoefficientRing(Kpoly);
    n:=Rank(R) - 1;
    // We need to work in F_p[x_0,...,x_n]
    S:=get_polynomial_ring(K,n+1,"grevlex");
    // Differentiate f w.r.t. t
    f_prime:=Polynomial([Kpoly | Derivative(c) : c in Coefficients(f)],Monomials(f));
    // Initialise the variables
    failed:=0;
    eval_points:={K|};
    reconstruction_list:=[];
    last_used_idxs:=[Integers()|];
    last_gm:=false;
    last_rho_0:=false;
    last_M_basis:=false;
    last_cert := false;
    // Start looping until we successfully compute the data we need via interpolation
    while true do
	// Check that we haven't failed too many times -- if we have then r is probably too small
	if failed ge 4 then
	    TimingPop();
	    return false,_,_,_,_;
	end if;
        // Pick a random point for evaluation
	repeat
	    pt:=Random(K);
	until not pt in eval_points;
	Include(~eval_points,pt);
	// Attempt to construct the data we will return, but with t set equal to pt
	success,gm_matrix,rho_0,M_basis,cert:=gauss_manin_mod_p_for_fixed_t(f,f_prime,r,a,Kpoly,R,S,pt : certificate:=certificate);
	if not success then
	    // We've failed, so either r is too small or the evaluation point pt wasn't sufficiently general
	    failed +:= 1;
	    continue;
	end if;
	Append(~reconstruction_list,<pt,gm_matrix,rho_0,M_basis,cert>);
	failed:=0;
	// we have <pt_1,gm_1,(rho_0)_1,(M_basis)_1,cert_1>,...,
	// <pt_k,gm_k,(rho_0)_k,(M_basis)_k,cert_k>
	// where pt_j lies in K
	// gm_j is a matrix with entries in K
	// (rho_0)_j is a vector with entries in K
	// (M_basis)_j is a list of exponents
	// FIX ME THINK describe cert_j
	// and we want to reconstruct
	// gm a matrix with entries in K(t)
	// rho_0 a vector with entries in K(t)
	// M_basis a list of exponents
	// FIX ME THINK describe the reconstruction of cert_j
	// We should set M_basis equal to the most-frequently-occurring (M_basis)_j
	// and then discard all data with (M_basis)_j not equal to M_basis
	// Find the most commonly occurring M_basis
	M_basis,used_idxs:=most_common([I[4] : I in reconstruction_list]);
	// Is there anything to do?
	if used_idxs eq last_used_idxs then
	    continue; // We added data that didn't change the most common basis from last time
	end if;
	last_used_idxs:=used_idxs;
	if #used_idxs lt 8 or IsOdd(#used_idxs) then
	    continue; // Not enough data for reconstruction
	end if;
	// If M_basis has changed, we need to reset our previous values and continue
	if Type(last_M_basis) eq BoolElt or last_M_basis ne M_basis then
	    last_M_basis:=M_basis;
	    last_rho_0:=false;
	    last_gm:=false;
	    last_cert:=false;
	    continue;
	end if;
	// Extract the evaluation points and perform the reconstruction on the vector rho_0
	pts:=[K | reconstruction_list[idx][1] : idx in used_idxs];
	bool,rho_0:=reconstruct_by_interpolation_for_vector(pts,[reconstruction_list[idx][3] : idx in used_idxs],Krat);
        if not bool then
	    failed +:= 1;
	    continue;
	end if;
	// If rho_0 hasn't stabilised, save what we have and continue
	if Type(last_rho_0) eq BoolElt or last_rho_0 ne rho_0 then
	    last_rho_0:=rho_0;
	    last_gm:=false;
	    continue;
	end if;
	// Now we reconstruct the matrix gm
	bool,gm:=reconstruct_by_interpolation_for_matrix(pts,[reconstruction_list[idx][2] : idx in used_idxs],Krat);
	if not bool then
	    failed +:= 1;
	    continue;
	end if;
	// If the matrix hasn't stabilised, save what we have and continue
	if Type(last_gm) eq BoolElt or last_gm ne gm then
	    last_gm:=gm;
	    continue;
	end if;
	// FIX ME THINK  reconstruct the certificate and check that it has stabilised
	cert := false;
	// The result has stabilised, so we return
	TimingPop();
	return true,last_gm,last_rho_0,last_M_basis,cert;
    end while;
end function;


// Given a and f are homogeneous in QQ[t][x_0,x_1,...,x_n] and a value for r, attempts to compute
// the connection matrix gm_matrix with entries in QQ(t), the reduction rho_0 = [a]_r, and a basis for
// what is denoted by M in [Lairez] S7.2.2.
// The basis is recorded as a sorted sequence M_basis of exponents, one for each monomial.
// If the optional argument 'certificate' is set to true then we also calculate a
// certificate cert for gm_matrix   FIX ME THINK describe
// otherwise cert is set to 'false'
// The optional argument 'tolerance' controls the number of reconstruction failures that
// are permitted before we conclude that we picked a bad prime and restart the calculation.
// Returns true,gm_matrix,rho_0,M_basis,cert on success, false,_,_,_,_ otherwise.
// (What can go wrong is that r isn't large enough.)
// we work by reducing mod p for a sequence of primes p, doing the calculation over F_p[t]
// and then using Chinese Remainder and rational reconstruction to recover the result
// over QQ[t]
function gauss_manin(f,r,a : certificate:=false, tolerance:=10)
    // grab the base rings
    R:=Parent(f); // QQ[t][x_0,...,x_n]
    QQt := CoefficientRing(R);
    QQrat := FieldOfFractions(QQt);
    ////////////////////////////////////////////////////////////////////////////////
    // now loop over primes p, doing the same calculation mod p
    // we stop looping when the lift to QQ(t)  [via Chinese Remainder and rational
    // reconstruction] stabilizes
    ////////////////////////////////////////////////////////////////////////////////
    used_primes:={Integers()|};
    primes := [Integers()|];
    gm_reductions := [* *];
    rho_0_reductions := [* *];
    cert_reductions := [* *];
    last_M_basis := false;
    stabilised := false;
    last_gm_lift := false;
    last_rho_0_lift := false;
    last_cert_lift := false;
    failed := 0;
    reconstruction_failed:=0;
    repeat
        if failed ge 6 then
            // too many failures below, so r is probably too small
            return false, _, _, _, _;
        end if;
        if reconstruction_failed ge tolerance then
            // too many reconstruction failures, so we probably picked a bad prime
            // reset everything and try again:
            print "Too many reconstruction failures.  Resetting.";
            primes := [Integers()|];
            gm_reductions := [* *];
            rho_0_reductions := [* *];
            cert_reductions := [* *];
            last_M_basis := false;
            stabilised := false;
            last_gm_lift := false;
            last_rho_0_lift := false;
            last_cert_lift := false;
            reconstruction_failed := 0;
        end if;
        p := RandomPrime(32);
        if p in used_primes then
            // get another prime, then
            continue;
        end if;
        Include(~used_primes,p);
        ////////////////////////////////////////////////////////////////////////////////
        // do the calculation over F_p[t]
        ////////////////////////////////////////////////////////////////////////////////
        Fpt := PolynomialRing(GF(p));
        Rp := PolynomialRing(Fpt,Rank(R),MonomialOrder(R)[1]);
        phi := hom<R->Rp|[Rp.i : i in [1..Rank(Rp)]]>;
        bool, gm_p, rho_0_p, M_basis_p, cert_p := gauss_manin_mod_p(phi(f),r,phi(a) : certificate:=certificate);
        if not bool then
            failed +:= 1;
            continue;
        end if;
        if #gm_reductions eq 0 or last_M_basis eq M_basis_p then
            // the basis agrees with what we have (if anything) so add these reductions to the list
            Append(~gm_reductions,gm_p);
            Append(~rho_0_reductions,rho_0_p);
            if certificate then
                Append(~cert_reductions,cert_p);
            end if;
            Append(~primes, p);
            last_M_basis := M_basis_p;
            printf "Current prime: %o.  Using %o primes.\n",p,#primes;
        elif #M_basis_p gt #last_M_basis then
            // the basis is too big mod p, so we should throw away everything and use the bigger basis
            gm_reductions := [* gm_p *];
            rho_0_reductions := [* rho_0_p *];
            if certificate then
            cert_reductions := [* cert_p *];
            end if;
            last_M_basis := M_basis_p;
            primes:=[p];
            printf "Oops!  Bad reduction.  Resetting.\n";
        else
            // the basis is too small mod p, so we were unlucky and picked a bad prime
            print "This prime is bad.  Skipping.";
            continue;
        end if;
        ////////////////////////////////////////////////////////////////////////////////
        // now reconstruct the connection matrix over QQ and see if it has stabilised
        ////////////////////////////////////////////////////////////////////////////////
        bool, gm_lift := rational_reconstruction_for_matrix_of_rational_functions(gm_reductions,QQrat);
        if not bool then
            reconstruction_failed +:= 1;
            print "Couldn't reconstruct gm.  Trying more primes.";
            continue;
        end if;
        gm_stabilised := last_gm_lift cmpeq gm_lift;
        ////////////////////////////////////////////////////////////////////////////////
        // now reconstruct rho_0 over QQ and see if it has stabilised
        ////////////////////////////////////////////////////////////////////////////////
        bool, rho_0_lift := rational_reconstruction_for_sequence_of_rational_functions(rho_0_reductions,QQrat);
        if not bool then
            reconstruction_failed +:= 1;
            print "Couldn't reconstruct rho_0.  Trying more primes.";
            continue;
        end if;
        rho_0_stabilised := last_rho_0_lift cmpeq rho_0_lift;
        ////////////////////////////////////////////////////////////////////////////////
        // if we are certificating then reconstruct the certificate over QQ and
        // see if it has stabilised
        ////////////////////////////////////////////////////////////////////////////////
        // FIX ME THINK write this...
        cert_lift := false;
        last_cert_lift := false;
        // FIX ME ...down to here
        cert_stabilised := (not certificate) or (last_cert_lift cmpeq cert_lift);
        ////////////////////////////////////////////////////////////////////////////////
        if gm_stabilised and rho_0_stabilised and cert_stabilised then
            ////////////////////////////////////////////////////////////////////////////////
            // everything has stabilised, so we return
            ////////////////////////////////////////////////////////////////////////////////
            return true,last_gm_lift,last_rho_0_lift,last_M_basis,last_cert_lift;
        else
            ////////////////////////////////////////////////////////////////////////////////
            // we need more primes
            ////////////////////////////////////////////////////////////////////////////////
            last_gm_lift := gm_lift;
            last_rho_0_lift := rho_0_lift;
            last_cert_lift := cert_lift;
            if not gm_stabilised then
                print "gm hasn't stabilised.  Need more primes.";
            elif not rho_0_stabilised then
                print "rho_0 hasn't stabilised.  Need more primes.";
            elif certificate and not cert_stabilised then
                print "Certificate hasn't stabilised.  Need more primes.";
            end if;
            continue;
        end if;
    until false;
end function;

intrinsic InternalLairezGaussManinModP(p, f, r : certificate:=false) -> BoolElt, AlgMatElt, SeqEnum, SeqEnum
{Given a prime p, a Laurent polynomial f in n variables with QQ or ZZ coefficients, and a value for r, attempts to compute the connection matrix gm_matrix with entries in F_p(t), the reduction rho_0 = [a]_r, and a basis for what is denoted by M in [Lairez, "Computing periods of rational integrals"] S7.2.2. The basis is recorded as a sorted sequence M_basis of exponents, one for each monomial. Returns true,gm_matrix,rho_0,M_basis,cert on success, false,_,_,_,_ otherwise. (What can go wrong is that r isn't large enough.)}
    // Sanity checks
    require not certificate: "certificates are not yet implemented";
    require p in Integers() and IsPrime(p): "p should be prime";
    // Move f to have rational coefficient if necessary, and validate arguments.
    ok, f, a := validate_picard_fuchs_args(f,r);
    require ok: f;
    // Cast f into QQ[t](x_1,...,x_n)
    R:=Parent(f);
    K:=CoefficientRing(R);
    Qt:=PolynomialRing(K);
    RR:=RationalFunctionField(Qt,Rank(R));
    f:=RR ! f;
    // Calculate 1/(1-tf(x1,...,xn))dx_1/x_1...dx_n/x_n
    F:=1/((1-Qt.1 * f) * &*Generators(RR));
    // Find a, f homogeneous in QQ[t][x0,x1,...,xn] such that a/f restricts to F when x0=1
    a,f:=prepare_fraction(F);
    // Do the calculation
    Fpt := PolynomialRing(GF(p));
    R:=Parent(f); // QQ[t][x0,...,xn]
    Rp := PolynomialRing(Fpt,Rank(R),MonomialOrder(R)[1]);
    phi := hom<R->Rp|[Rp.i : i in [1..Rank(Rp)]]>;
    return gauss_manin_mod_p(phi(f),r,phi(a) : certificate:=certificate);
end intrinsic;

intrinsic InternalLairezReconstructGaussManin(gm_reductions, rho_0_reductions, M_basis_reductions, cert_reductions : certificate:=false) -> BoolElt, AlgMatElt, SeqEnum, SeqEnum, BoolElt
{Attempts to reconstruct from the parallel lists of mod-p reductions, the connection matrix gm_matrix with entries in QQ(t), the reduction rho_0 = [a]_r, and a basis for what is denoted by M in [Lairez] S7.2.2. If the optional argument 'certificate' is set to true then we also calculate a certificate cert for gm_matrix. Returns true,gm_matrix,rho_0,M_basis,cert on success, false,_,_,_,_ otherwise. Uses Chinese Remainder and rational reconstruction.}
    // Sanity checks
    require not certificate: "certificates are not yet implemented";
    require #gm_reductions eq #rho_0_reductions: "length mismatch";
    require #gm_reductions eq #M_basis_reductions: "length mismatch";
    require not certificate or #gm_reductions eq #cert_reductions: "length mismatch";
    // Handle the trivial case
    if #gm_reductions eq 0 then
        return false, _, _, _, _;
    end if;
    // QQrat is QQ(t)
    QQrat := FieldOfFractions(PolynomialRing(Rationals()));
    // Recover the primes
    primes := [Characteristic(BaseRing(CoefficientRing(gm_p))) : gm_p in gm_reductions];
    require #primes eq #SequenceToSet(primes): "illegal repeated prime";
    printf "Attempting reconstruction from %o mod-p reductions\n", #primes;
    // Pick out the most common shape for the basis. This singles out good primes
    m := Max([#B : B in M_basis_reductions]);
    big_M_bases := [B : B in M_basis_reductions | #B eq m];
    error if #SequenceToSet(big_M_bases) ne 1, "unexpected failure to reconstruct a basis for M";
    M_basis := big_M_bases[1];
    idxs := [i : i in [1..#M_basis_reductions] | M_basis_reductions[i] eq M_basis];
    // Do we have enough good primes to attempt reconstruction?
    if #idxs lt 2 then
        print "Not enough good primes";
        return false, _, _, _, _;
    end if;
    // Restrict to the good primes
    primes := [* primes[i] : i in idxs *];
    gm_reductions := [* gm_reductions[i] : i in idxs *];
    rho_0_reductions := [* rho_0_reductions[i] : i in idxs *];
    M_basis_reductions := [* M_basis_reductions[i] : i in idxs *];
    if certificate then
        cert_reductions := [* cert_reductions[i] : i in idxs *];
    end if;
    printf "Using %o probably-good primes: %o\n", #primes, primes;
    // Try to reconstruct
    stabilised := false;
    last_gm_lift := false;
    last_rho_0_lift := false;
    last_cert_lift := false;
    // n is the number of primes needed
    n := 1;
    repeat
        if n gt #primes then
            return false, _, _, _, _;
        end if;
        printf "Trying to reconstruct with %o primes\n", n;
        // first reconstruct the connection matrix over QQ and see if it has stabilised
        bool, gm_lift := rational_reconstruction_for_matrix_of_rational_functions(gm_reductions[1..n],QQrat);
        if not bool then
            n +:= 1;
            print "Couldn't reconstruct gm.  Trying more primes.";
            continue;
        end if;
        gm_stabilised := last_gm_lift cmpeq gm_lift;
        // now reconstruct rho_0 over QQ and see if it has stabilised
        bool, rho_0_lift := rational_reconstruction_for_sequence_of_rational_functions(rho_0_reductions[1..n],QQrat);
        if not bool then
            n +:= 1;
            print "Couldn't reconstruct rho_0.  Trying more primes.";
            continue;
        end if;
        rho_0_stabilised := last_rho_0_lift cmpeq rho_0_lift;
        // if we are certificating then reconstruct the certificate over QQ and see if it has stabilised
        // FIX ME THINK write this...
        cert_lift := false;
        last_cert_lift := false;
        // FIX ME ...down to here
        cert_stabilised := (not certificate) or (last_cert_lift cmpeq cert_lift);
        if gm_stabilised and rho_0_stabilised and cert_stabilised then
            // everything has stabilised, so we return
            return true,last_gm_lift,last_rho_0_lift,M_basis,last_cert_lift;
        else
            // we need more primes
            last_gm_lift := gm_lift;
            last_rho_0_lift := rho_0_lift;
            last_cert_lift := cert_lift;
            if not gm_stabilised then
                print "gm hasn't stabilised";
            elif not rho_0_stabilised then
                print "rho_0 hasn't stabilised";
            elif certificate and not cert_stabilised then
                print "Certificate hasn't stabilised";
            end if;
            // Increase the number of primes we use and try again
            n +:= 1;
        end if;
    until false;
end intrinsic;

freeze;

/////////////////////////////////////////////////////////////////////////
// dim3.m
/////////////////////////////////////////////////////////////////////////
// Interface functions for the 3-dimensional jigsaw pieces.
/////////////////////////////////////////////////////////////////////////
// Note: The database names are case insensitive.
/////////////////////////////////////////////////////////////////////////
/*
Minkowski ansatz
================
// Add the coefficients using the lattice Minkowski ansatz
for id in [1..4248] do
    printf "[%o / 4248]\n",id;
    F:=PolytopeCanonicalFanoDim3Facet(id);
    for cs in LatticeMinkowskiAnsatz(F) do
        JigsawDim3AddCoefficientsForFacet("minkowski",F,cs);
    end for;
end for;
// Build the cache of coefficients supported on reflexive 3-topes
for id in [1..4319] do
    printf "[%o / 4319]\n",id;
    P:=PolytopeReflexiveFanoDim3(id);
    _:=JigsawDim3CoefficientsForPolytope("minkowski",P);
end for;

Binomial edge coefficients
==========================
// Add the coefficients using the binomial edge coefficients
for id in [1..4248] do
    printf "[%o / 4248]\n",id;
    F:=PolytopeCanonicalFanoDim3Facet(id);
    cs:=BinomialEdgeCoefficients(F);
    JigsawDim3AddCoefficientsForFacet("binomial",F,cs);
end for;
// Build the cache of coefficients supported on reflexive 3-topes
for id in [1..4319] do
    printf "[%o / 4319]\n",id;
    P:=PolytopeReflexiveFanoDim3(id);
    _:=JigsawDim3CoefficientsForPolytope("binomial",P);
end for;
*/

import "../core.m": save_data, fetch_data;
import "jigsaw.m": validate_name, get_jigsaw_dir, intseq_to_string, apply_affine_group, laurent_ring, lift_from_affine_normal_form, cs_process, reflexive_polytope_data, sort_permut;

/////////////////////////////////////////////////////////////////////////
// Database access
/////////////////////////////////////////////////////////////////////////

// Create the database tables (if they don't exist).
procedure db_make_tables(db)
    // Create the Laurent polynomial tables
    if not SqliteTableExists(db,"laurent_poly") then
        SqliteExecute(db,"CREATE TABLE laurent_poly (id INTEGER PRIMARY KEY AUTOINCREMENT, facet_id INTEGER NOT NULL, coeffs TEXT NOT NULL)");
        SqliteExecute(db,"CREATE INDEX laurent_poly_id_idx ON laurent_poly(id)");
        SqliteExecute(db,"CREATE INDEX laurent_poly_facet_id_idx ON laurent_poly(facet_id)");
        SqliteExecute(db,"CREATE TABLE reflexive_poly (id INTEGER NOT NULL, coeffs TEXT NOT NULL)");
        SqliteExecute(db,"CREATE INDEX reflexive_id_idx ON reflexive_poly(id)");
        SqliteExecute(db,"CREATE TABLE reflexive_poly_stale (id INTEGER PRIMARY KEY NOT NULL)");
        SqliteExecute(db,"CREATE INDEX stale_id_idx ON reflexive_poly_stale(id)");
    end if;
end procedure;

// Returns true followed by connection to the database, false followed by an
// error message otherwise.
function db_connect(db_name)
    db_name:=StringToLower(db_name);
    key:="db_conn_" cat db_name;
    bool,db:=fetch_data(key);
    if not bool then
        if not validate_name(db_name) then
            return false,"Database name contains illegal characters.";
        end if;
        bool,base:=get_jigsaw_dir();
        if not bool then return false,base; end if;
        db:=SqliteConnect(base cat "/dim3_" cat db_name cat ".db");
        try
            db_make_tables(db);
        catch e
            return false,e`Object;
        end try;
        save_data(key,db);
    end if;
    return true,db;
end function;

// Marks the cache for all reflexive polytopes containing the given facet id as
// stale. Returns true on success, an error otherwise.
function db_mark_reflexive_as_stale(db,fid)
    // Sanity check
    if fid lt 1 or fid gt 4248 then
        return false,"The facet ID must be in the range 1..4248.";
    end if;
    // Recover the reflexive ids for this facet
    pids:=PolytopeReflexiveFanoDim3IDForFacet(fid);
    if #pids eq 0 then return true,_; end if;
    // We need to exclude the ids that are already marked as stale
    if #pids eq 1 then
        sql:=Sprintf("SELECT id FROM reflexive_poly_stale WHERE id = %o",
                                                          Representative(pids));
    else
        sql:=Sprintf("SELECT id FROM reflexive_poly_stale WHERE id IN (%o)",
                                                        intseq_to_string(pids));
    end if;
    stale:={Integers()|};
    for row in SqliteQuery(db,sql) do
        Include(~stale,StringToInteger(row[1][2]));
    end for;
    pids diff:= stale;
    if #pids eq 0 then return true,_; end if;
    // OK, add the remaining ids to the stale list
    for id in pids do
        sql:=Sprintf("INSERT INTO reflexive_poly_stale (id) VALUES (%o)",id);
        SqliteExecute(db,sql);
    end for;
    return true,_;
end function;

// Returns true followed by a set of all Laurent polynomial ids for the given
// facet id, false followed by an error message otherwise.
function db_laurent_poly_ids_for_facet_id(db_name,fid)
    // Get a connection to the database
    bool,db:=db_connect(db_name);
    if not bool then return false,db; end if;
    // Create the sql statement
    sql:=Sprintf("SELECT id FROM laurent_poly WHERE facet_id = %o",fid);
    // Extract and return the ids
    ids:={Integers()|};
    for row in SqliteQuery(db,sql) do
        Include(~ids,StringToInteger(row[1][2]));
    end for;
    return true,ids;
end function;

// Returns true followed by the set of all Laurent polynomial coefficients
// supported on the given facet id, false followed by an error message
// otherwise.
function db_fetch_laurent_coefficients(db_name,fid)
    // Get a connection to the database
    bool,db:=db_connect(db_name);
    if not bool then return false,db; end if;
    // Create the sql statement
    sql:=Sprintf("SELECT coeffs FROM laurent_poly WHERE facet_id = %o",fid);
    // Extract and return the coefficients
    csset:={PowerSequence(Integers())|};
    for row in SqliteQuery(db,sql) do
        cs:=eval "[Integers() | " cat row[1][2] cat "]";
        Include(~csset,cs);
    end for;
    return true,csset;
end function;

// Returns true followed by the id of the given Laurent polynomial, false
// followed by an error message otherwise. If the Laurent polynomial isn't in
// the database then it will be added and the new id returned.
function db_laurent_poly_id(db_name,f)
    // Recover the affine normal form and canonical facet ID
    _,P,cs:=LaurentAffineNormalForm(f : return_coeffs:=true);
    fid:=PolytopeCanonicalFanoDim3FacetID(P);
    // Get a connection to the database
    bool,db:=db_connect(db_name);
    if not bool then return false,db; end if;
    // Check whether the entry already exists
    csstr:=intseq_to_string(cs);
    sql1:=Sprintf("SELECT id FROM laurent_poly WHERE facet_id = %o AND coeffs = \"%o\" LIMIT 1",fid,csstr);
    res:=SqliteQuery(db,sql1);
    // If we don't have a match, add the Laurent polynomial
    if #res eq 0 then
        // Add the entry
        sql2:=Sprintf("INSERT INTO laurent_poly (facet_id, coeffs) VALUES (%o, \"%o\")",fid,csstr);
        SqliteExecute(db,sql2);
        // Recover the id
        res:=SqliteQuery(db,sql1);
        if #res eq 0 then
            return false,"Unable to recover ID for Laurent polynomial.";
        end if;
        // Update the list of stale reflexive polytopes
        bool,err:=db_mark_reflexive_as_stale(db,fid);
        if not bool then return false,err; end if;
    end if;
    // Extract and return the id
    return true,StringToInteger(res[1][1][2]);
end function;

// Fetches the coefficients for the given reflexive polytope P with database
// id 'id' from the cache. Returns true followed by the coefficients on success,
// false otherwise. Here M should be a matrix mapping P to the normal form of P
// (up to some order of the vertices).
function db_fetch_reflexive_cache(db_name,id,P,M)
    // Sanity check
    error if id lt 1 or id gt 4319 or Dimension(P) ne 3 or not IsReflexive(P),
        "Invalid polytope data.";
    // Get a connection to the database
    bool,db:=db_connect(db_name);
    if not bool then return false,_; end if;
    // Is the cache for this polytope stale?
    sql:=Sprintf("SELECT COUNT(*) FROM reflexive_poly_stale WHERE id = %o",id);
    res:=SqliteQuery(db,sql);
    num:=StringToInteger(res[1][1][2]);
    if num ne 0 then return false,_; end if;
    // Recover the permutation of the points of P
    perm:=sort_permut(RowSequence(Matrix(Sort(Points(P))) * M));
    perm:=perm^-1;
    // Create the sql statement
    sql:=Sprintf("SELECT coeffs FROM reflexive_poly WHERE id = %o",id);
    // Extract and return the coefficients
    csset:={PowerSequence(Integers())|};
    for row in SqliteQuery(db,sql) do
        cs:=eval "[Integers() | " cat row[1][2] cat "]";
        Include(~csset,PermuteSequence(cs,perm));
    end for;
    return true,csset;
end function;

// Updates the cache of the coefficients for the given reflexive polytope
// P with database id 'id'. Here M should be a matrix mapping P to the normal
// form of P (up to some order of the vertices).
procedure db_update_reflexive_cache(db_name,id,P,M,csset)
    // Sanity check
    error if id lt 1 or id gt 4319 or Dimension(P) ne 3 or not IsReflexive(P),
        "Invalid polytope data.";
    // Get a connection to the database
    bool,db:=db_connect(db_name);
    if not bool then return; end if;
    // Delete any existing entries for this reflexive polytope
    sql:=Sprintf("DELETE FROM reflexive_poly WHERE id = %o",id);
    SqliteExecute(db,sql);
    // Recover the permutation of the points of P
    perm:=sort_permut(RowSequence(Matrix(Sort(Points(P))) * M));
    // Start adding the coefficients
    for cs in csset do
        // Permute the coefficients and convert them into a string
        csstr:=intseq_to_string(PermuteSequence(cs,perm));
        // Create the sql statement and add the coefficients
        sql:=Sprintf("INSERT INTO reflexive_poly (id, coeffs) VALUES (%o, \"%o\")",id,csstr);
        SqliteExecute(db,sql);
    end for;
    // Mark the cache for this polytope as fresh
    sql:=Sprintf("DELETE FROM reflexive_poly_stale WHERE id = %o",id);
    SqliteExecute(db,sql);
end procedure;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Returns the sequence monomials corresponding with the given facet id.
function monomials_for_facet_id(fid)
    // Sanity check
    if fid lt 1 or fid gt 4248 then
        return false,"The ID must be in the range 1..4248.";
    end if;
    // Consult the cache
    bool,M:=fetch_data("monos");
    if not bool then
        M:=AssociativeArray();
    end if;
    bool,monos:=IsDefined(M,fid);
    if not bool then
        // We missed the cache -- extract the monomials
        monos:=[Eltseq(v) : v in Sort(Points(
                                        PolytopeCanonicalFanoDim3Facet(fid)))];
        M[fid]:=monos;
        save_data("monos",M);
    end if;
    return monos;
end function;

// A wrapper for db_laurent_poly_ids_for_facet_id to correctly catch errors.
// Returns true followed by a set of all Laurent polynomial ids for the given
// facet id, false followed by an error message otherwise.
function laurent_poly_ids_for_facet_id(db_name,fid)
    // Sanity check
    if fid lt 1 or fid gt 4248 then
        return false,"The ID must be in the range 1..4248.";
    end if;
    // Fetch the data
    try
        bool,ids:=db_laurent_poly_ids_for_facet_id(db_name,fid);
    catch e
        bool:=false;
        ids:=e`Object;
    end try;
    return bool,ids;
end function;

// Returns true followed by the monomials and set of coefficients for all known
// Laurent polynomials supported on the (affine normal form of the) facet with
// given id. May also return false followed by an error message.
function fetch_laurent_polys_for_facet(db_name,fid)
    // Sanity check
    if fid lt 1 or fid gt 4248 then
        return false,"The ID must be in the range 1..4248.",_;
    end if;
    // Fetch the Laurent ids
    bool,ids:=laurent_poly_ids_for_facet_id(db_name,fid);
    if not bool then return false,ids,_; end if;
    // Fetch the Laurent coefficients
    try
        bool,csset:=db_fetch_laurent_coefficients(db_name,fid);
    catch e
        bool:=false;
        csset:=e`Object;
    end try;
    if not bool then return false,csset,_; end if;
    // Fetch the monomials
    monos:=monomials_for_facet_id(fid);
    // Is there anything to do?
    if #csset eq 0 then return true,monos,csset; end if;
    // Recover (or construct) the (affine) automorphism group
    bool,Gs:=fetch_data("aut_groups");
    if not bool then
        Gs:=AssociativeArray(Integers());
    end if;
    bool,G:=IsDefined(Gs,fid);
    if not bool then
        verts:=[Append(v,1) : v in InternalPolytopeCanonicalFanoDim3Facet(fid)];
        F:=Polytope(verts : areVertices:=true);
        G:=AutomorphismGroup(F);
        Gs[fid]:=G;
        save_data("aut_groups",Gs);
    end if;
    // Allow the automorphism group to act by reordering the monomials
    csset:=apply_affine_group(monos,csset,G);
    // Return the data
    return true,monos,csset;
end function;

// Given a canonical 3-tope P, returns the possible coefficients that can
// be constructed from the facets.
function build_coefficients_for_poly(db_name,P)
    // Recover the coefficients for each facet
    coeffs:=[];
    for F in Facets(P) do
        csset:=JigsawDim3CoefficientsForFacet(db_name,F);
        if #csset eq 0 then return {PowerSequence(Integers())|}; end if;
        Append(~coeffs,csset);
    end for;
    // Work through all possible combinations, putting the coefficients in
    // bijection with the points
    fpts:=&cat[Sort(Points(F)) : F in Facets(P)];
    pts:=Sort(Points(P));
    return {PowerSequence(Integers()) | cs : cs in cs_process(coeffs,fpts,pts)};
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic JigsawDim3AddCoefficientsForFacet( db_name::MonStgElt, F::TorPol,
    cs::SeqEnum[RngIntElt] : sortpts:=false )
{Given a facet F of a canonical three-dimensional polytope, and a sequence of coefficients, adds the entry to the database. The coefficients should be in bijection with 'sortpts', which defaults to Sort(SetToSequence(Points(F))).}
    // Sanity check
    require Dimension(F) eq 2 and IsPolytope(F) and IsIntegral(F):
        "Argument 1 must be a facet of a 3-dimensional canonical Fano polytope.";
    if sortpts cmpeq false then
        sortpts:=Sort(Points(F));
    else
        require Type(sortpts) eq SeqEnum and Universe(sortpts) eq Ambient(F):
            "Parameter 'sortpts' should be a sequence of distinct points in the polytope.";
        tmp:=SequenceToSet(sortpts);
        require #tmp eq #sortpts and tmp subset Points(F):
            "Parameter 'sortpts' should be a sequence of distinct points in the polytope.";
    end if;
    require #cs eq #sortpts:
        "The coefficients should be in bijective correspondence with the points.";
    // We need F to be maximum dimensional in the ambient lattice before we
    // construct the Laurent polynomial
    if not IsMaximumDimensional(F) then
        F,emb,u:=PolyhedronInSublattice(F);
        sortpts:=[Ambient(F) | (pt - u) @@ emb : pt in sortpts];
    end if;
    // Create the Laurent polynomial
    R:=laurent_ring(Dimension(F));
    f:=LaurentPolynomial(R,[Eltseq(pt) : pt in sortpts],cs);
    // Add the polynomial to the database
    bool,id:=db_laurent_poly_id(db_name,f);
    require bool: id;
end intrinsic;

intrinsic JigsawDim3CoefficientsForFacet( db_name::MonStgElt, F::TorPol )
    -> SetEnum[SeqEnum[RngIntElt]]
{Given a facet F of a canonical three-dimensional polytope, returns the set of coefficients for the known Laurent polynomials supported on F. The coefficients are placed in bijection with Sort(SetToSequence(Points(F))).}
    // Sanity check
    require Dimension(F) eq 2 and IsPolytope(F) and IsIntegral(F):
        "Argument must be a facet of a 3-dimensional canonical Fano polytope.";
    // Recover the facet id
    fid:=PolytopeCanonicalFanoDim3FacetID(F);
    // Fetch the coefficients supported on the affine normal form
    bool,monos,csset:=fetch_laurent_polys_for_facet(db_name,fid);
    require bool: monos;
    if #csset eq 0 then return {PowerSequence(Integers())|}; end if;
    // Lift the monomials back to F
    monos:=lift_from_affine_normal_form(F,monos);
    Sort(~monos,~perm);
    return {PowerSequence(Integers()) | PermuteSequence(cs,perm) : cs in csset};
end intrinsic;

intrinsic JigsawDim3CoefficientsForPolytope( db_name::MonStgElt, P::TorPol )
    -> SetEnum[SeqEnum[RngIntElt]]
{Given a canonical three-dimensional polytope, returns the set of coefficients for the known Laurent polynomials supported on P. The coefficients are placed in bijection with Sort(SetToSequence(Points(P))).}
    // Sanity check
    require Dimension(P) eq 3 and IsFano(P) and NumberOfInteriorPoints(P) eq 1:
        "Argument must be a 3-dimensional canonical Fano polytope.";
    // If this is a reflexive polytope, check the cache for the result
    if IsReflexive(P) then
        id,M:=reflexive_polytope_data(P);
        bool,coeffs:=db_fetch_reflexive_cache(db_name,id,P,M);
        if bool then return coeffs; end if;
    end if;
    // Recover the coefficients for the polytope
    coeffs:=build_coefficients_for_poly(db_name,P);
    // If this is a reflexive polytope, update the cache
    if IsReflexive(P) then
        db_update_reflexive_cache(db_name,id,P,M,coeffs);
    end if;
    // Return the coefficients
    return coeffs;
end intrinsic;

intrinsic JigsawDim3LaurentPolynomialsForPolytope( db_name::MonStgElt,
    P::TorPol ) -> SetEnum[FldFunRatMElt]
{Given a canonical three-dimensional polytope, returns the set of known Laurent polynomials supported on P.}
    // Sanity check
    require Dimension(P) eq 3 and IsFano(P) and NumberOfInteriorPoints(P) eq 1:
        "Argument must be a 3-dimensional canonical Fano polytope.";
    // Fetch the parent for the Laurent polynomials and the exponents
    R:=laurent_ring(3);
    ex:=[Eltseq(pt) : pt in Sort(Points(P))];
    // If this is a reflexive polytope, check the cache for the result
    if IsReflexive(P) then
        id,M:=reflexive_polytope_data(P);
        bool,coeffs:=db_fetch_reflexive_cache(db_name,id,P,M);
        if bool then
            return {R | LaurentPolynomial(R,ex,cs) : cs in coeffs};
        end if;
    end if;
    // Recover the coefficients for the polytope
    coeffs:=build_coefficients_for_poly(db_name,P);
    // If this is a reflexive polytope, update the cache
    if IsReflexive(P) then
        db_update_reflexive_cache(db_name,id,P,M,coeffs);
    end if;
    // Return the Laurent polynomials
    return {R | LaurentPolynomial(R,ex,cs) : cs in coeffs};
end intrinsic;

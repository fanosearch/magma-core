freeze;

/////////////////////////////////////////////////////////////////////////
// jigsaw.m
/////////////////////////////////////////////////////////////////////////
// Common jigsaw access functions.
/////////////////////////////////////////////////////////////////////////

import "../core.m": delete_data, save_data, fetch_data;

/////////////////////////////////////////////////////////////////////////
// Laurent coefficients process
/////////////////////////////////////////////////////////////////////////

// Reorders the coefficients (in bijection with the points fpts) to place them
// in bijection with pts. Returns true followed by the reordering on success,
// false otherwise.
function cs_reorder_coeffs(cs,fpts,pts)
    A:=AssociativeArray(Universe(pts));
    for i in [1..#cs] do
        if not fpts[i] in pts then return false,_; end if;
        bool,c:=IsDefined(A,fpts[i]);
        if bool then
            if c ne cs[i] then return false,_; end if;
        else
            A[fpts[i]]:=cs[i];
        end if;
    end for;
    cs:=[Integers()|];
    for pt in pts do
        bool,c:=IsDefined(A,pt);
        if not bool then
            Append(~cs,0);
        else
            Append(~cs,c);
        end if;
    end for;
    return true,cs;
end function;

// Returns the data for the initial case in the coefficients process.
function cs_initial(coeffs,fpts,pts)
    // Is there anything to do?
    if #coeffs eq 0 then return <true>; end if;
    // Create the initial case
    val:=[Integers() | 1 : i in [1..#coeffs - 1]];
    Append(~val,0);
    success:=false;
    while not success do
        // Compute the next choice of coefficients
        i:=#coeffs;
        val[i] +:= 1;
        while val[i] gt #coeffs[i] do
            if i eq 1 then return <true>; end if;
            val[i]:=1;
            i -:= 1;
            val[i] +:= 1;
        end while;
        // Construct the sequence of coefficients
        cs:=&cat[coeffs[i][val[i]] : i in [1..#coeffs]];
        // Check that they're compatible
        success,cs:=cs_reorder_coeffs(cs,fpts,pts);
    end while;
    // Return the values
    return <false,coeffs,fpts,pts,val,cs,1>;
end function;

// Returns true iff the process has finished.
function cs_is_empty(info)
    return info[1];
end function;

// Increments the process.
procedure cs_advance(~info)
    // Sanity check
    error if info[1], "The process has finished.";
    // Extract the data we need
    coeffs:=info[2];
    val:=info[5];
    // Advance the value
    success:=false;
    while not success do
        // Compute the next choice of coefficients
        i:=#coeffs;
        val[i] +:= 1;
        while val[i] gt #coeffs[i] do
            if i eq 1 then
                info:=<true>;
                return;
            end if;
            val[i]:=1;
            i -:= 1;
            val[i] +:= 1;
        end while;
        // Construct the sequence of coefficients
        cs:=&cat[coeffs[i][val[i]] : i in [1..#coeffs]];
        // Check that they're compatible
        success,cs:=cs_reorder_coeffs(cs,info[3],info[4]);
    end while;
    // Update the values
    info:=<false,coeffs,info[3],info[4],val,cs,info[7] + 1>;
end procedure;

// Returns the current value.
function cs_current_value(info)
    error if info[1], "The process has finished.";
    return info[6];
end function;

// Returns a label for the current value.
function cs_current_label(info)
    error if info[1], "The process has finished.";
    return info[7];
end function;

// Returns a process for iterating over all possible combinatons from the given
// sets of coefficients, points, and target points.
function cs_process(coeffs,fpts,pts)
    // Convert the sets to sequences
    coeffs:=[SetToSequence(csset) : csset in coeffs];
    // Return the process
    info:=cs_initial(coeffs,fpts,pts);
    return InternalCreateProcess("Laurent Coefficients",info,cs_is_empty,
                                 cs_advance,cs_current_value,cs_current_label);
end function;

/////////////////////////////////////////////////////////////////////////
// Helper functions
/////////////////////////////////////////////////////////////////////////

// Returns true iff the proposed database name is valid (i.e. only contains
// certain characters).
function validate_name(db_name)
    cs:={"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
         "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
         "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "_", "-"};
    db_name:=StringToLower(db_name);
    return &and[db_name[i] in cs : i in [1..#db_name]];
end function;

// Returns the permutation that would sort the given sequence.
function sort_permut(S)
    Sort(~S,~perm);
    return perm;
end function;

// Returns the given sequence (or set) of values as a string. Opening and
// closing braces are omitted.
function intseq_to_string(S)
    if #S eq 0 then return ""; end if;
    str:="";
    for s in S do str cat:= Sprintf("%o,",s); end for;
    return str[1..#str - 1];
end function;

// Returns true followed by the path to the database base directory, false
// followed by an error message otherwise.
function get_jigsaw_dir()
    try
        path:=FanosearchGetDbRoot() cat "/jigsaw";
    catch e
        return false,"Unable to deduce the path to the Fanosearch root directory";
    end try;
    return true,path;
end function;

// The parent of our Laurent polynomials (of rank n).
function laurent_ring(n)
    bool,A:=fetch_data("laurent_ring");
    if not bool then
        A:=AssociativeArray(Integers());
    end if;
    bool,R:=IsDefined(A,n);
    if not bool then
        assert n ge 0;
        if n eq 1 then
            R:=FieldOfFractions(PolynomialRing(Rationals()));
        else
            R:=FieldOfFractions(PolynomialRing(Rationals(),n));
        end if;
        A[n]:=R;
        save_data("laurent_ring",A);
    end if;
    return R;
end function;

// Pulls back the given monomial exponents from the affine normal form of F to
// F. This works even if F isn't maximum dimensional.
function lift_from_affine_normal_form(F,monos)
    // Move to the maximum dimensional sublattice
    FF,emb,u1:=PolyhedronInSublattice(F);
    // Compute the affine normal form and recover the transformation
    Qverts,perm:=AffineNormalForm(FF);
    assert IsZero(Qverts[1]);
    u2:=Vertices(FF)[1^perm];
    vtrans:=[v - u2 : v in Vertices(FF)];
    H:=Transpose(Solution(Transpose(Matrix(PermuteSequence(vtrans,perm))),
                          Transpose(Matrix(Qverts))));
    // Pull back the monomials
    monos:=[Ambient(F) | emb(v + u2) + u1 :
             v in ChangeUniverse(RowSequence(Matrix(monos) * H^-1),Parent(u2))];
    // Return the new monomials
    return [PowerSequence(Integers()) | Eltseq(v) : v in monos];
end function;

// Returns true followed by the indicated database id for the polytope P, false
// otherwise.
function database_id(P,type)
    for I in DatabaseID(P) do
        if I[1] eq type then return true,I[2]; end if;
    end for;
    return false,_;
end function;

// Given a reflexive polytope P, return the database id and the isomorphism
// from P to the normal form (but not the permutation).
function reflexive_polytope_data(P)
    bool,id:=database_id(P,"reflexive3");
    error if not bool, "Unable to recover polytope database ID.";
    nf,perm:=NormalForm(P);
    M:=Transpose(Solution(Transpose(Matrix(PermuteSequence(Vertices(P),perm))),
                          Transpose(Matrix(nf))));
    return id,M;
end function;

// Given a sequence of monomial exponents and coefficients, allows the group G
// to act on the exponents *affinely* (i.e. embedded at height 1 in L x Z).
// Returns the resulting set of coefficients.
function apply_affine_group(monos,csset,G)
    // Is there anything to do?
    if #csset eq 0 then return {PowerSequence(Integers())|}; end if;
    if Type(csset) eq SeqEnum then
        csset:={PowerSequence(Integers()) | csset};
    end if;
    // Sanity check
    error if #Representative(csset) ne #monos,
        "apply_group: Arguments 1 and 2 must be of the same length.";
    // Apply the group
    M:=HorizontalJoin(Matrix(monos),
                      Matrix(#monos,1,ZeroSequence(Integers(),#monos)));
    perms:={sort_permut(RowSequence(M * g)) : g in G};
    return {PowerSequence(Integers()) | PermuteSequence(cs,perm) :
                                                    cs in csset, perm in perms};
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic JigsawDatabases() -> SeqEnum
{The available jigsaw databases}
    // Fetch the path to the jigsaw directory
    bool,base:=get_jigsaw_dir();
    require bool: base;
    // List the contents
    bool,res:=PipeCommand("ls \"" cat base cat "/\"");
    require bool: "Unable to read jigsaw directory contents.";
    // Extract the database names and dimensions
    dbs:=[];
    for file in Split(res) do
        bool,_,matches:=Regexp("dim([0-9]+)_([a-z0-9_-]+).db",file);
        if bool then
            d:=StringToInteger(matches[1]);
            Append(~dbs,<d,matches[2]>);
        end if;
    end for;
    // Sort and return the databases
    Sort(~dbs);
    return [<I[2],I[1]> : I in dbs];
end intrinsic;

intrinsic JigsawDatabases( d::RngIntElt ) -> SeqEnum[MonStgElt]
{The available jigsaw databases for dimension d}
    // Fetch the path to the jigsaw directory
    bool,base:=get_jigsaw_dir();
    require bool: base;
    // List the contents
    bool,res:=PipeCommand("ls \"" cat base cat "/\"");
    require bool: "Unable to read jigsaw directory contents.";
    // Extract the database names and dimensions
    dbs:=[];
    for file in Split(res) do
        bool,_,matches:=Regexp("dim([0-9]+)_([a-z0-9_-]+).db",file);
        if bool then
            if StringToInteger(matches[1]) eq d then
                Append(~dbs,matches[2]);
            end if;
        end if;
    end for;
    // Sort and return the databases
    Sort(~dbs);
    return dbs;
end intrinsic;

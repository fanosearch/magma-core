freeze;

/////////////////////////////////////////////////////////////////////////
// mutation.m
/////////////////////////////////////////////////////////////////////////
// Mutation intrinsics for Laurent polynomials.
/////////////////////////////////////////////////////////////////////////

import "../utilities/laurent.m": seq_to_monomial;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Given a polytope Laurent polynomial f, returns the slices of f in the
// direction of the "z" axis. The slices are cast into a codimension 1
// polynomial ring.
function slices(f)
    // Recover the dimension of the parent polynomial ring and create the target
    d:=Rank(Parent(f));
    R:=RationalFunctionField(CoefficientRing(Parent(f)),d - 1);
    // Split f into a numerator and denominator, and compute the height of
    // the denominator
    num:=Numerator(f);
    den:=Denominator(f);
    shift:=-Exponents(den)[d];
    // Recreate the denominator in R
    den:=seq_to_monomial(R,Prune(Exponents(den)));
    // Create the slices
    cs,ex:=CoefficientsAndExponents(num);
    A:=AssociativeArray(Integers());
    for h in {Integers() | m[d] : m in ex} do
        A[h + shift] := &+[R | cs[i] * seq_to_monomial(R,Prune(ex[i])) :
                                           i in [1..#ex] | ex[i][d] eq h] / den;
    end for;
    // Return the slices
    return A;
end function;

// Given a sequence of factorisations F = [<f,i>] (where <f,i> means that f^i
// is a factor), and given a pair I = <g,j>, returns min(i,j) if g lies in F, 0
// otherwise.
function in_factors(F,I)
    for J in F do
        if I[1] eq J[1] then return Min(I[2],J[2]); end if;
    end for;
    return 0;
end function;

// Given an associative array A of slices of a Laurent polynomial, and a set of
// slices H (given by the relevant keys of A), returns the largest factor F such
// that F^Abs(i) | Numerator(A[i]) for all i in H.
function common_factor(A,H)
    // Initially we have no candidate factors
    cands:=false;
    // Start working through the slices
    for i in H do
        // Extract the factors of appropriate power
        facts:=[];
        for I in Factorisation(Numerator(A[i])) do
            j:=Floor(I[2] / Abs(i));
            if j gt 0 then Append(~facts,<I[1],j>); end if;
        end for;
        // Is there anything to do?
        if IsEmpty(facts) then return facts; end if;
        // Adjust the candidates accordingly
        if Type(cands) eq BoolElt then
            cands:=facts;
        else
            // Restrict the candidates
            newcands:=[];
            for I in cands do
                j:=in_factors(facts,I);
                if j ne 0 then
                    Append(~newcands,<I[1],j>);
                end if;
            end for;
            // Is there anything to do?
            if IsEmpty(newcands) then return newcands; end if;
            cands:=newcands;
        end if;
    end for;
    // Return the factors
    return cands;
end function;

// Suppose that we have a sequence of objects S = [o_1,...,o_1,o_2,...,o_2,...],
// where each object o_i is repeated n_i times. These repetitions are encoded
// in a sequence N = [n_1,n_2,...]. Given N, this function will return all
// distinct ways of choosing an unordered sequence of objects from S -- this is
// returned as a set of sets of indices (from S).
function unordered_sequences(N)
    // Get the easy cases out of the way
    if IsEmpty(N) then
        return {{Integers()|}};
    elif #N eq 1 then
        return {{1..k} : k in [0..N[1]]};
    end if;
    // We now work recursively on the n_i
    n:=N[1];
    base:={{1..k} : k in [0..n]};
    idxs:={Universe(base)|};
    for I in $$(N[2..#N]) do
        J:={Integers() | i + n : i in I};
        idxs join:= {B join J : B in base};
    end for;
    // Return the sequence if indices
    return idxs;
end function;

// Given an associative array of slices, performs the mutation that moves the
// factors to the negative slices.
function move_slices_to_bottom(A)
    // Recover the common factor from the positive slices
    fact:=common_factor(A,{Integers() | h : h in Keys(A) | h gt 0});
    // Perform the mutation that moves the factor to the negative slices
    R:=Parent(A[Representative(Keys(A))]);
    K:=&*[R | I[1]^I[2] : I in fact];
    for h in Keys(A) do
        if h lt 0 then
            A[h]:=A[h] * K^-h;
        elif h gt 0 then
            A[h]:=A[h] / K^h;
        end if;
    end for;
    // Return the result
    return A;
end function;

// Input is an associative array of slices, where each slice is orthogonal to
// (0,...,0,1). Set 'return_graph' to false to return a set of all possible
// Laurent polynomials; set 'return_graph' to true to return the data encoded in
// a directed graph, whose vertex labels are the Laurent polynomials and whose
// edge labels are pairs <F,sgn> describing the corresponding factors. Here F is
// the factor, and sgn is _1 if the w-vector is (0,...,0,1), and is -1 if the
// w-vector is (0,...,0,-1).
function mutations_for_slices(RR,A,return_graph)
    // Build an embedding from the slices into the parent ring of f
    R:=Parent(A[Representative(Keys(A))]);
    emb:=hom<R -> RR | [RR.i : i in [1..Rank(R)]]>;
    d:=Rank(RR);
    z:=RR.d;
    // Recover the common factor from the negative slices
    fact:=common_factor(A,{Integers() | h : h in Keys(A) | h lt 0});
    num:=[Integers() | I[2] : I in fact];
    fact:=&cat[[I[1] : i in [1..I[2]]] : I in fact];
    // We keep track of the prime decomposition used in order to recover the
    // divisibility amongst the resulting mutations
    mutfacts:=[PowerSet(Integers())|];
    // 'maps' will contain the map from the end-point in the w-line to the
    // indexed mutation
    maps:=[RR|];
    // Start working through the possible ways of distributing the factors
    muts:=[RR|];
    for I in unordered_sequences(num) do
        // Build the component that we're distributing
        K:=&*[R | fact[i] : i in I];
        // Distribute it over the heights
        newA:=AssociativeArray(Integers());
        for h in Keys(A) do
            if h lt 0 then
                newA[h]:=A[h] / K^-h;
            elif h gt 0 then
                newA[h]:=A[h] * K^h;
            else
                newA[0]:=A[0];
            end if;
        end for;
        // Now we rebuild these slices as a Laurent polynomial
        Append(~muts,&+[RR | emb(newA[h]) * z^h : h in Keys(newA)]);
        if return_graph then
            // Calculate the map and divisibility data
            Append(~maps,emb(K));
            Append(~mutfacts,I);
        end if;
    end for;
    // If we don't need to calculate the graph, then we're done
    if not return_graph then return SequenceToSet(muts); end if;
    // Build the edges indices
    edges:={PowerSequence(Integers())|};
    for i in [1..#muts - 1] do
        for j in [i + 1..#muts] do
            if mutfacts[i] subset mutfacts[j] or
               mutfacts[j] subset mutfacts[i] then
                Include(~edges,[i,j]);
            end if;
        end for;
    end for;
    edges join:= {PowerSequence(Integers()) | Reverse(E) : E in edges};
    // Build the mutation graph
    G:=Digraph<#muts | SetToIndexedSet(edges)>;
    AssignVertexLabels(~G,muts);
    // Is there anything to do?
    if #edges eq 0 then return G; end if;
    // Create the mutation data for the edges
    labels:=[];
    for E in Edges(G) do
        // Recover the vertex indices that this is an edge between
        idx1:=Index(InitialVertex(E));
        idx2:=Index(TerminalVertex(E));
        // Build the factor and record the direction of w
        F:=maps[idx2] / maps[idx1];
        num:=Numerator(F);
        den:=Denominator(F);
        if den eq 1 then
            F:=num;
            sgn:=1;
        else
            assert num eq 1;
            F:=den;
            sgn:=-1;
        end if;
        // Add the label
        Append(~labels,<F,sgn>);
    end for;
    // Adding the labels to the edges and return
    AssignEdgeLabels(~G,labels);
    return G;
end function;

// Given a directed graph of mutations and a change of basis M used to place the
// original Laurent polynomial in standard form, undoes the change of basis.
procedure undo_base_change(~G,M)
    // Recover the data we need
    muts:=VertexLabels(G);
    RR:=Universe(muts);
    d:=Rank(RR);
    // Undo the change of basis for the Laurent polynomials
    Minv:=M^-1;
    AssignVertexLabels(~G,[RR | g^Minv : g in muts]);
    // Are there any edges to update?
    if NumberOfEdges(G) eq 0 then return; end if;
    // Build the inverse change of basis map
    phiinv:=hom<RR -> RR | [RR.i^Minv : i in [1..d]]>;
    // Undo the change of basis for the mutation data
    labels:=[<phiinv(E[1]),E[2]> : E in EdgeLabels(G)];
    AssignEdgeLabels(~G,labels);
end procedure;

// Returns one representative of each reordering class of non-empty subsequences
// of S
function subsequences(S)
    error if Type(S) ne SeqEnum, "S should be a sequence";
    return {[S[i] : i in idxs] : idxs in Subsets({1..#S}) | #idxs gt 0};
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic WLine( f::FldFunRatMElt, w::TorLatElt ) -> SetEnum[FldFunRatMElt]
{The set of all mutated Laurent polynomials in the w-line induced by f.}
    // Sanity check
    require IsLaurent(f):
        "Argument 1 must be a Laurent polynomial";
    RR:=Parent(f);
    require Rank(RR) eq Dimension(Parent(w)):
        Sprintf("Argument 2 must lie in a %o-dimensional lattice",Rank(RR));
    require IsPrimitive(w):
        "Argument 2 must be a primitive lattice vector";
    // Perform the change of basis
    M:=DefiningMatrix(ChangeBasis(w));
    f:=f^M;
    // Create the initial slices
    A:=slices(f);
    require Min(Keys(A)) lt 0 and Max(Keys(A)) gt 0:
        "The Newton polytope of the Laurent polynomial must intersect the hyperplane defined by argument 2";
    A:=move_slices_to_bottom(A);
    // Perform the mutations, undo the change of basis, and return
    Minv:=M^-1;
    return {RR | g^Minv : g in mutations_for_slices(RR,A,false)};
end intrinsic;

intrinsic WLineGraph( f::FldFunRatMElt, w::TorLatElt ) -> GrphDir
{The directed graph G of all mutated Laurent polynomials in the w-line induced by f. The vertices of G are labeled by Laurent polynomials f_i. The edges [i,j] of G are labeled by a pair <F,sgn> describing the mutations f_i --> f_j. Here F is the factor and sgn is +1 if w is then w-vector, and sgn is -1 if -w is the w-vector.}
    // Sanity check
    require IsLaurent(f):
        "Argument 1 must be a Laurent polynomial";
    RR:=Parent(f);
    require Rank(RR) eq Dimension(Parent(w)):
        Sprintf("Argument 2 must lie in a %o-dimensional lattice",Rank(RR));
    require IsPrimitive(w):
        "Argument 2 must be a primitive lattice vector";
    // Perform the change of basis
    M:=DefiningMatrix(ChangeBasis(w));
    f:=f^M;
    // Create the initial slices
    A:=slices(f);
    require Min(Keys(A)) lt 0 and Max(Keys(A)) gt 0:
        "The Newton polytope of the Laurent polynomial must intersect the hyperplane defined by argument 2";
    A:=move_slices_to_bottom(A);
    // Perform the mutations
    G:=mutations_for_slices(RR,A,true);
    // Undo the change of basis and return
    undo_base_change(~G,M);
    return G;
end intrinsic;

intrinsic Mutation( F::FldFunRatMElt, w::TorLatElt ) -> Map
{The mutation generated by the given factor F and w-vector.}
    // Sanity check
    require IsLaurent(F):
        "Argument 1 must be a Laurent polynomial";
    RR:=Parent(F);
    d:=Rank(RR);
    L:=Parent(w);
    require d eq Dimension(L):
        Sprintf("Argument 2 must lie in a %o-dimensional lattice",d);
    require IsPrimitive(w):
        "Argument 2 must be a primitive lattice vector";
    _,ex:=CoefficientsAndExponents(F);
    ChangeUniverse(~ex,Dual(L));
    require &and[IsZero(v * w) : v in ex]:
        "Argument 1 must lie in the kernel of argument 2";
    // Perform the change of basis
    M:=DefiningMatrix(ChangeBasis(w));
    F:=F^M;
    // Reconstruct the mutation defined by F
    z:=RR.d;
    mut:=hom<RR -> RR | [RR.i : i in [1..d - 1]] cat [F * z]>;
    // Build the change of basis map and its inverse
    Minv:=M^-1;
    phi:=hom<RR -> RR | [RR.i^M : i in [1..d]]>;
    phiinv:=hom<RR -> RR | [RR.i^Minv : i in [1..d]]>;
    // Return the mutation
    return phi * mut * phiinv;
end intrinsic;

intrinsic IsNormalised(f::FldFunRatMElt) -> BoolElt
{True if and only if f is a non-zero Laurent polynomial such that the coefficients associated with each vertex are 1, the coefficient of the unit monomial is zero, the Newton polytope P of f is full-dimensional, and the origin is in the strict interior of P.}
    require IsLaurent(f): "Argument must be a Laurent polynomial";
    require not IsZero(f): "Argument must be non-zero";
    P := NewtonPolytope(f);
    if not (IsMaximumDimensional(P) and ContainsZero(P)) then
        return false;
    end if;
    vs := [Eltseq(v) : v in Vertices(P)];
    cs, es := CoefficientsAndExponents(f);
    for v in vs do
        idx := Index(es, v);
        if cs[idx] ne 1 then
            return false;
        end if;
    end for;
    return Index(es, ZeroSequence(Integers(), Dimension(P))) eq 0;
end intrinsic;

intrinsic LocalMutations(f::FldFunRatMElt) -> SetEnum
{The set of one-step mutations <g,w> that are compatible with the non-zero Laurent polynomial f.}
    // Sanity check
    require IsLaurent(f): "Argument must be a Laurent polynomial";
    require not IsZero(f): "Argument must be non-zero";
    // Grab the weights
    ws:=MutationGradings(f);
    // Build the set of weights and factors
    S:={};
    for w in ws do
        factors:=[Parent(f)|];
        Ff,h:=Face(f, w);
        for I in Factorization(Numerator(Ff)) do
            n:=I[2] div h;
            if n gt 0 then
                g:=I[1]/LeadingMonomial(I[1]);
                for k in [1..n] do
                    Append(~factors, g);
                end for;
            end if;
        end for;
        for T in subsequences(factors) do
            g:=&*T;
            phi:=Mutation(g, w);
            if IsLaurent(phi(f)) then
                Include(~S, <g, w>);
            end if;
        end for;
    end for;
    return S;
end intrinsic;

freeze;

import "widths.m": possible_widths_laurent;

/////////////////////////////////////////////////////////////////////////
// mutation_graph.m
/////////////////////////////////////////////////////////////////////////
// Intrinsics for construction the mutation graph of a Laurent polynomial.
/////////////////////////////////////////////////////////////////////////

// The record associated with a Laurent polynomial f
f_rec:=recformat<
    id,         // The id of the Laurent polynomial f
    ws,         // The set of width vectors, partitioned by width
    min_h,      // The minimum width vector in ws
    depth,      // The depth of f
    G >;        // The automorphism group of f

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Returns true iff the terminate function returns true for some f in fs.
function should_terminate(terminate_func,fs)
    for f in fs do
        if terminate_func(f) then return true; end if;
    end for;
    return false;
end function;


// Given a sequence of width forms, partitioned by width h, returns the smallest
// value of h. If the sequence contains no width forms, returns 0.
function min_width(ws)
    for h in [1..#ws] do
        if IsDefined(ws,h) and not IsEmpty(ws[h]) then return h; end if;
    end for;
    return 0;
end function;

// Given a polytope P and a set of width vectors ws, returns ws partitioned by
// width.
function partition_by_width(P,ws,max_width)
    // Sanity check
    assert Dual(Ambient(P)) eq Universe(ws);
    // Partition by width on P
    parts:=[PowerSet(Universe(ws))|];
    for w in ws do
        h:=Integers() ! Width(P,w);
        if max_width eq 0 or h le max_width then
            if IsDefined(parts,h) then
                Include(~parts[h],w);
            else
                parts[h]:={w};
            end if;
        end if;
    end for;
    // Return the partition
    return parts;
end function;

// Given a Laurent polynomial f, returns the set of all possible width vectors
// on f that may give rise to non-trivial mutations. The widths are returned
// partitioned by width. The minimum width is also returned. "max_width" is
// optional and can be used to bound the maximum width permitted. The
// dimensions of the factors to consider can be specified by "factor_dims".
function partitioned_widths(f : max_width:=0, factor_dims:=false)
    poss := possible_widths_laurent(f : max_width:=max_width, factor_dims:=factor_dims);
    ws:=partition_by_width(NewtonPolytope(f),poss,max_width);
    return ws,min_width(ws);
end function;

/////////////////////////////////////////////////////////////////////////

// Given a sequence S and index h, returns the new sequence given by omitting
// S[h], but preserving all other indices.
function remove_index(S,h)
    newS:=[Universe(S)|];
    for i in [1..#S] do
        if IsDefined(S,i) and i ne h then newS[i]:=S[i]; end if;
    end for;
    return newS;
end function;

// Given the current array of Laurent data records, a target entry f, and a form
// w of width h, allows the automorphism group of f to act on both w and -w and
// removes all resulting widths from the target. Also updates the minimum width
// (if necessary).
procedure remove_from_width_data(~fs,f,h,w)
    // Is there anything to do?
    ws:=fs[f]`ws;
    if not IsDefined(ws,h) then return; end if;
    // Calculate the orbit of w. Note: We multiply by g rather than g^-1 since
    // we're iterating over a group, so in the end it doesn't matter.
    D:=Universe(ws[h]);
    orb:={D | w * Transpose(g) : g in fs[f]`G};
    // And, if necessary, the orbit of -w
    if not -w in orb then orb join:= {D | -w : w in orb}; end if;
    // Compute the difference and update the sequence
    min_h:=fs[f]`min_h;
    C:=ws[h] diff orb;
    if not IsEmpty(C) then
        ws[h]:=C;
    else
        ws:=remove_index(ws,h);
        if h eq min_h then fs[f]`min_h:=min_width(ws); end if;
    end if;
    fs[f]`ws:=ws;
end procedure;

// Given the current array of Laurent data records, and a current target width,
// picks a Laurent polynomial f and width w and returns true,f,w,h. If there's
// nothing to pick, returns false.
function fetch_next_by_width(fs,current_h)
    // Start working through the data looking for a width to return
    for f in Keys(fs) do
        if fs[f]`min_h eq current_h then
            w:=Representative(fs[f]`ws[current_h]);
            return true,f,w,current_h;
        end if;
    end for;
    // No luck -- let's calculate the width we should be using
    min_h:=0;
    found_f:=false;
    found_w:=0;
    for f in Keys(fs) do
        h:=fs[f]`min_h;
        if h ne 0 and (min_h eq 0 or h lt min_h) then
            min_h:=h;
            found_f:=f;
            found_w:=Representative(fs[f]`ws[h]);
            if min_h eq 2 then
                return true,found_f,found_w,min_h;
            end if;
        end if;
    end for;
    // Any luck?
    if min_h ne 0 then
        return true,found_f,found_w,min_h;
    else
        return false,_,_,_;
    end if;
end function;

// Given the current array of Laurent data records, and a current target depth,
// picks a Laurent polynomial f and width w and returns true,f,w,h,depth. If
// there's nothing to pick, returns false.
function fetch_next_by_depth(fs,current_depth)
    // Start working through the data looking for a width to return. Whilst we
    // do this, we also look out for the next depth down (just in case).
    next_f:=false;
    next_h:=0;
    next_w:=0;
    for f in Keys(fs) do
        depth:=fs[f]`depth;
        h:=fs[f]`min_h;
        if depth eq current_depth and h ne 0 then
            w:=Representative(fs[f]`ws[h]);
            return true,f,w,h,current_depth;
        elif next_f cmpeq false and depth eq current_depth + 1 and h ne 0 then
            next_f:=f;
            next_h:=h;
            next_w:=Representative(fs[f]`ws[h]);
        end if;
    end for;
    // No luck -- was there a result at the next depth?
    if next_f cmpeq false then
        return false,_,_,_,_;
    else
        return true,next_f,next_w,next_h,current_depth + 1;
    end if;
end function;

// Adds the given Laurent polynomial f to the data array fs. The polynomial is
// assumed to be in normal form, and assumed not to already be in the data.
procedure add_polynomial(f,~fs : max_width:=0, depth:=0, factor_dims:=false)
    // Calculate the width forms
    ws,min_h:=partitioned_widths(f : max_width:=max_width,
                                     factor_dims:=factor_dims);
    // Calculate the automorphism group
    G:=LaurentAutomorphismGroup(f);
    // Add the data to the data array
    id:=#fs + 1;
    fs[f]:=rec< f_rec | id:=id, ws:=ws, min_h:=min_h, depth:=depth, G:=G >;
    // Provide some feedback
    vprintf Fanosearch: " + New Laurent polynomial (id = %o) with %o w-lines.\n",id,&+[Integers() | #W : W in ws];
end procedure;

// Uses the given set/sequence of Laurent polynomials to initialize the data
// array and graph.
function initialize_data(gs : max_width:=0, filter_func:=false,
  factor_dims:=false)
    // First we create the data array
    fs:=AssociativeArray(Universe(gs));
    // Now we add the Laurent polynomials
    vprintf Fanosearch: "Creating initial data...\n";
    for g in gs do
        f:=LaurentNormalForm(g);
        if not IsDefined(fs,f) then
            if filter_func cmpeq false or filter_func(f) then
                add_polynomial(f,~fs : max_width:=max_width,
                                       factor_dims:=factor_dims);
            end if;
        end if;
    end for;
    // Create the graph
    G:=Digraph<#fs|>;
    // Assign the vertex labels
    S:=[<fs[f]`id,f> : f in Keys(fs)];
    Sort(~S);
    AssignVertexLabels(~G,[Universe(fs) | I[2] : I in S]);
    // Finally we return the data
    return fs,G;
end function;

// Returns true iff the given direct graph is connected as a non-directed graph.
function is_connected(G)
    H := UnderlyingGraph(G);
    vprintf Fanosearch: " - There are %o components, of sizes %o.\n", #Components(H), [#c : c in Components(H)];
    return IsConnected(H);
end function;

// Merges the graph for the w-line induced by f with the mutation graph G.
procedure merge_wline_to_graph(f,w,~G,~fs,~current_width : max_width:=0,
  depth:=0, labels_include_width:=false, filter_func:=false,
  filter_child_func:=false, factor_dims:=false)
    // Extract the ambient rational function field
    R:=Parent(f);
    rk:=Rank(R);
    // Calculate the w-line and note the current width
    WG:=WLineGraph(f,w);
    w_width:=current_width;
    // First we merge the vertices
    verts:=Vertices(WG);
    labels:=VertexLabels(WG);
    mapping:=AssociativeArray(Universe(verts));
    for i in [1..#verts] do
        // Normalise the Laurent polynomial and record the change of basis
        g,_,M:=LaurentNormalForm(labels[i]);
        // Is this Laurent polynomial new?
        if not IsDefined(fs,g) then
            // Should we add it?
            add:=(filter_func cmpeq false or filter_func(g)) and
                 (filter_child_func cmpeq false or filter_child_func(f,g));
            if add then
                AddVertex(~G,g);
                add_polynomial(g,~fs : max_width:=max_width, depth:=depth,
                                       factor_dims:=factor_dims);
                if fs[g]`min_h lt current_width then
                    current_width:=fs[g]`min_h;
                end if;
            end if;
        else
            add:=true;
        end if;
        if add then
            // Remove the width data
            Minv:=M^-1;
            remove_from_width_data(~fs,g,w_width,w * Transpose(Minv));
            // Record the normal form data
            mapping[verts[i]]:=<fs[g]`id,M,Minv>;
        end if;
    end for;
    // Is there anything more to do?
    if NumberOfEdges(WG) eq 0 then return; end if;
    // Now we merge the edges
    minus_w:=-w;
    Es:=Edges(WG);
    Elabels:=EdgeLabels(WG);
    verts:=Vertices(G);
    for i in [1..#Es] do
        // Extract the edge data
        E:=Es[i];
        bool,data1:=IsDefined(mapping,InitialVertex(E));
        if bool then
            bool,data2:=IsDefined(mapping,TerminalVertex(E));
            if bool then
                // Ensure that the edge exists
                v1:=verts[data1[1]];
                v2:=verts[data2[1]];
                AddEdge(~G,v1,v2);
                EE:=EdgeSet(G) ! [v1,v2];
                // Create the edge label describing this mutation
                I:=Elabels[i];
                F:=I[1]^data1[2];
                if I[2] eq 1 then
                    Fw:=w * Transpose(data1[3]);
                else
                    Fw:=minus_w * Transpose(data1[3]);
                end if;
                B:=data1[3] * data2[2];
                J:=labels_include_width select <F,Fw,B,w_width> else <F,Fw,B>;
                // Update the edge label
                if IsLabelled(EE) then
                    AssignLabel(~G,EE,Include(Label(EE),J));
                else
                    AssignLabel(~G,EE,{J});
                end if;
            end if;
        end if;
    end for;
end procedure;

/////////////////////////////////////////////////////////////////////////
// Width first algorithms
/////////////////////////////////////////////////////////////////////////

// Given a sequence of Laurent polynomials fs, computes the mutation graph using
// a width-first algorithm.
function mutation_graph_width_first_basic(fs : max_width:=0, max_iterations:=0,
  labels_include_width:=false, factor_dims:=false, max_size:=0, has_timeout:=false, timeout:=0.0)
    // First we initialize our data
    fs,G:=initialize_data(fs : max_width:=max_width, factor_dims:=factor_dims);
    if has_timeout then
        deadline:=Realtime() + timeout;
    end if;
    // The main loop -- we build the mutation graph width-first
    iteration:=1;
    has_next,f,w,current_width:=fetch_next_by_width(fs,2);
    while has_next and 
        (max_iterations eq 0 or iteration le max_iterations) and 
        (max_size eq 0 or NumberOfVertices(G) lt max_size) and
        ((not has_timeout) or Realtime() lt deadline) do
        // Provide some feedback
        vprintf Fanosearch: "Iteration: %o\n - Current width: %o\n - Calculating w-line for Laurent id = %o and w = %o...\n",iteration,current_width,fs[f]`id,w;
        // Merge the w-line with the mutation graph
        merge_wline_to_graph(f,w,~G,~fs,~current_width :
                                max_width:=max_width,
                                labels_include_width:=labels_include_width,
                                factor_dims:=factor_dims);
        // Provide some feedback
        vprintf Fanosearch: " - There are %o Laurent polynomials.\n - There remain %o w-values to explore.\n",#fs,&+[Integers() | &+[Integers() | #W : W in fs[f]`ws] : f in Keys(fs)];
        // Move on
        iteration +:= 1;
        has_next,f,w,current_width:=fetch_next_by_width(fs,current_width);
    end while;
    // Tidy up
    if max_size gt 0 then
        RemoveVertices(~G, [max_size+1..NumberOfVertices(G)]);
    end if;
    // Return the graph
    return G;
end function;

// This is a more feature-rich version of the basic function above.
// Given a sequence of Laurent polynomials fs, computes the mutation graph using
// a width-first algorithm.
function mutation_graph_width_first_full(fs : max_iterations:=0, max_width:=0,
  terminate_if_connected:=false, labels_include_width:=false,
  filter_func:=false, filter_child_func:=false, terminate_func:=false,
  factor_dims:=false, max_size:=0, has_timeout:=false, timeout:=0.0)
    // First we initialize our data
    fs,G:=initialize_data(fs : max_width:=max_width, filter_func:=filter_func,
                               factor_dims:=factor_dims);
    if has_timeout then
        deadline:=Realtime() + timeout;
    end if;
    // Is there anything to do?
    iteration:=1;
    if not terminate_func cmpeq false and
      should_terminate(terminate_func,Keys(fs)) then
        has_next:=false;
    else
        has_next,f,w,current_width:=fetch_next_by_width(fs,2);
    end if;
    // The main loop -- we build the mutation graph width-first
    while has_next and 
        (max_iterations eq 0 or iteration le max_iterations) and
        (max_size eq 0 or NumberOfVertices(G) lt max_size) and 
        ((not has_timeout) or Realtime() lt deadline) do
        // Provide some feedback
        vprintf Fanosearch: "Iteration: %o\n - Current width: %o\n - Calculating w-line for Laurent id = %o and w = %o...\n",iteration,current_width,fs[f]`id,w;
        // Merge the w-line with the mutation graph
        old_size:=NumberOfVertices(G);
        w_width:=current_width;
        merge_wline_to_graph(f,w,~G,~fs,~current_width :
                                max_width:=max_width,
                                labels_include_width:=labels_include_width,
                                filter_func:=filter_func,
                                filter_child_func:=filter_child_func,
                                factor_dims:=factor_dims);
        // Provide some feedback
        vprintf Fanosearch: " - There are %o Laurent polynomials.\n - There remain %o w-values to explore.\n",#fs,&+[Integers() | &+[Integers() | #W : W in fs[f]`ws] : f in Keys(fs)];
        // Have we been asked to terminate?
        if not terminate_func cmpeq false and
          should_terminate(terminate_func,
           VertexLabels(G)[old_size + 1..NumberOfVertices(G)]) then
            has_next:=false;
        // Is the graph connected?
        elif terminate_if_connected and is_connected(G) then
            has_next:=false;
            vprintf Fanosearch: "Mutation graph becomes connected at width: %o\n",w_width;
        end if;
        // Move on
        if has_next then
            iteration +:= 1;
            has_next,f,w,current_width:=fetch_next_by_width(fs,current_width);
        end if;
    end while;
    // Tidy up
    if max_size gt 0 then
        RemoveVertices(~G, [max_size+1..NumberOfVertices(G)]);
    end if;
    // Return the graph
    return G;
end function;

/////////////////////////////////////////////////////////////////////////
// Depth first algorithms
/////////////////////////////////////////////////////////////////////////

// Given a sequence of Laurent polynomials fs, computes the mutation graph using
// a breadth-first algorithm.
function mutation_graph_breadth_first_basic(fs : max_width:=0, max_depth:=0,
  max_iterations:=0, labels_include_width:=false, factor_dims:=false,  max_size:=0, has_timeout:=false, timeout:=0.0)
    // First we initialize our data
    fs,G:=initialize_data(fs : max_width:=max_width, factor_dims:=factor_dims);
    if has_timeout then
        deadline:=Realtime() + timeout;
    end if;
    // The main loop -- we build the mutation graph breadth-first
    iteration:=1;
    has_next,f,w,current_width,current_depth:=fetch_next_by_depth(fs,0);
    while has_next and 
        (max_iterations eq 0 or iteration le max_iterations) and
        (max_depth eq 0 or current_depth lt max_depth) and
        (max_size eq 0 or NumberOfVertices(G) lt max_size) and
        ((not has_timeout) or Realtime() lt deadline) do
        // Provide some feedback
        vprintf Fanosearch: "Iteration: %o\n - Current depth: %o\n - Calculating w-line for Laurent id = %o and w = %o...\n",iteration,current_depth,fs[f]`id,w;
        // Merge the w-line with the mutation graph
        merge_wline_to_graph(f,w,~G,~fs,~current_width :
                                max_width:=max_width,
                                depth:=current_depth + 1,
                                labels_include_width:=labels_include_width,
                                factor_dims:=factor_dims);
        // Provide some feedback
        vprintf Fanosearch: " - There are %o Laurent polynomials.\n - There remain %o w-values to explore.\n",#fs,&+[Integers() | &+[Integers() | #W : W in fs[f]`ws] : f in Keys(fs)];
        // Move on
        iteration +:= 1;
        has_next,f,w,current_width,current_depth:=fetch_next_by_depth(fs,
                                                                 current_depth);
    end while;
    // Tidy up
    if max_size gt 0 then
        RemoveVertices(~G, [max_size+1..NumberOfVertices(G)]);
    end if;
    // Return the graph
    return G;
end function;

// This is a more feature-rich version of the basic function above.
// Given a sequence of Laurent polynomials fs, computes the mutation graph using
// a breadth-first algorithm.
function mutation_graph_breadth_first_full(fs : max_width:=0, max_depth:=0,
  max_iterations:=0, terminate_if_connected:=false,
  labels_include_width:=false, filter_func:=false, filter_child_func:=false,
  terminate_func:=false, factor_dims:=false, max_size:=0, has_timeout:=false, timeout:=0.0)
    // First we initialize our data
    fs,G:=initialize_data(fs : max_width:=max_width, filter_func:=filter_func,
                               factor_dims:=factor_dims);
    if has_timeout then
        deadline:=Realtime() + timeout;
    end if;
    // Is there anything to do?
    iteration:=1;
    if not terminate_func cmpeq false and
      should_terminate(terminate_func,Keys(fs)) then
        has_next:=false;
    else
        has_next,f,w,current_width,current_depth:=fetch_next_by_depth(fs,0);
    end if;
    // The main loop -- we build the mutation graph breadth-first
    while has_next and (max_iterations eq 0 or iteration le max_iterations) and
      (max_depth eq 0 or current_depth lt max_depth) and
      (max_size eq 0 or NumberOfVertices(G) lt max_size) and
      ((not has_timeout) or Realtime() lt deadline) do
        // Provide some feedback
        vprintf Fanosearch: "Iteration: %o\n - Current depth: %o\n - Calculating w-line for Laurent id = %o and w = %o...\n",iteration,current_depth,fs[f]`id,w;
        // Merge the w-line with the mutation graph
        old_size:=NumberOfVertices(G);
        merge_wline_to_graph(f,w,~G,~fs,~current_width :
                                max_width:=max_width,
                                depth:=current_depth + 1,
                                labels_include_width:=labels_include_width,
                                filter_func:=filter_func,
                                filter_child_func:=filter_child_func,
                                factor_dims:=factor_dims);
        // Provide some feedback
        vprintf Fanosearch: " - There are %o Laurent polynomials.\n - There remain %o w-value to explore.\n",#fs,&+[Integers() | &+[Integers() | #W : W in fs[f]`ws] : f in Keys(fs)];
        // Have we been asked to terminate?
        if not terminate_func cmpeq false and
          should_terminate(terminate_func,
           VertexLabels(G)[old_size + 1..NumberOfVertices(G)]) then
            has_next:=false;
        // Is the graph connected?
        elif terminate_if_connected and is_connected(G) then
            has_next:=false;
            vprintf Fanosearch: "Mutation graph becomes connected at depth: %o\n",current_depth;
        end if;
        // Move on
        if has_next then
            iteration +:= 1;
            has_next,f,w,current_width,current_depth:=fetch_next_by_depth(fs,
                                                                 current_depth);
        end if;
    end while;
    // Tidy up
    if max_size gt 0 then
        RemoveVertices(~G, [max_size+1..NumberOfVertices(G)]);
    end if;
    // Return the graph
    return G;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic MutationGraph( fs::SetEnum[FldFunRatMElt] :
    max_width:=false,
    max_depth:=false,
    max_iterations:=false,
    terminate_if_connected:=false,
    breadth_first:=false,
    labels_include_width:=false,
    filter_func:=false,
    filter_child_func:=false,
    terminate_func:=false,
    factor_dims:=false,
    max_size:=false,
    timeout:=false ) -> GrphDir
{Given a set of Laurent polynomials, computes the mutation graph G using either a width-first or breadth-first algorithm. The vertices of G are labeled by the Laurent polynomials f_i, in normal form. The edges [i,j] of G are labeled by a set of triples <F,w,B> describing the mutations f_i --> f_j. Here F is the factor and w the w-vector decribing the mutation from f_i --> g_j, and B is the change of basis placing g_j in normal form f_j (so that g_j^B = f_j). The optional parameter 'max_width' can be used to specify an upper bound on the widths allowed [default: unbounded], 'max_iterations' to restrict the number of iterations to compute [default: no limit], and 'max_depth' to restrict the maximum depth to search to [default: no limit]. Set 'terminate_if_connected' to true to halt as soon as the mutation graph becomes connected. The algorithm will be selected automatically based on the values of the parameters, with a bias towards a width-first search. You can force a breadth-first search by setting 'breadth_first' to true. If 'labels_include_width' is set to true then the edge labels will be augmented with the width. One of two optional filter functions can be provided. If specified, 'filter_func' will be passed a Laurent polynomial g (in normal form) and should return true iff g should be included in the mutation graph. Alternatively, if specified, 'filter_child_func' will be passed two Laurent polynomials f and g (in normal form), where f is a Laurent polynomial already in the graph, and g is a candidate Laurent polynomial obtained from f via a mutation, and should return true iff g should be included in the mutation graph. The optional function 'terminate_func' will be passed g (in normal form) and should return true iff the mutation graph calculation should terminate and return. The dimensions of the factors to consider can be restricted via the parameter 'factor_dims'. The optional parameter 'max_size' can be used to terminate the calculation once a given size of graph has been computed. The optional parameter 'timeout' can be set to interrupt the calculation after (approximately) a given number of seconds.}
    // Sanity check
    require not IsNull(fs): "Illegal null sequence";
    if max_width cmpeq false then
        max_width:=0;
    else
        bool,max_width:=IsCoercible(Integers(),max_width);
        require bool and max_width gt 0:
            "Parameter 'max_width' must be a positive integer";
    end if;
    if max_depth cmpeq false then
        max_depth:=0;
    else
        bool,max_depth:=IsCoercible(Integers(),max_depth);
        require bool and max_depth gt 0:
            "Parameter 'max_depth' must be a positive integer";
    end if;
    if max_iterations cmpeq false then
        max_iterations:=0;
    else
        bool,max_iterations:=IsCoercible(Integers(),max_iterations);
        require bool and max_iterations gt 0:
            "Parameter 'max_iterations' must be a positive integer";
    end if;
    if max_size cmpeq false then
        max_size:=0;
    else
        bool,max_size:=IsCoercible(Integers(),max_size);
        require bool and max_size gt 0:
            "Parameter 'max_size' must be a positive integer";
    end if;
    if timeout cmpeq false then
        has_timeout:=false;
    else
        has_timeout:=true;
        bool,timeout:=IsCoercible(RealField(),timeout);
        require bool and timeout gt 0.0:
            "Parameter 'timeout' must be a positive real number";
    end if;
    require Type(terminate_if_connected) eq BoolElt:
        "Parameter 'terminate_if_connected' must be a boolean";
    require Type(breadth_first) eq BoolElt:
        "Parameter 'breadth_first' must be a boolean";
    require Type(labels_include_width) eq BoolElt:
        "Parameter 'labels_include_width' must be a boolean";
    require filter_func cmpeq false or ISA(Type(filter_func),Program):
        "Parameter 'filter_func' must be a program";
    require filter_child_func cmpeq false or
        ISA(Type(filter_child_func),Program):
        "Parameter 'filter_child_func' must be a program";
    require (filter_func cmpeq false) or (filter_child_func cmpeq false):
        "At most one of 'filter_func' or 'filter_child_func' should be specified";
    require terminate_func cmpeq false or ISA(Type(terminate_func),Program):
        "Parameter 'terminate_func' must be a program";
    require factor_dims cmpeq false or ((Type(factor_dims) eq SetEnum or Type(factor_dims) eq SeqEnum) and not IsNull(factor_dims) and Universe(factor_dims) eq Integers() and &and[d gt 0 : d in factor_dims]):
        "Parameter 'factor_dims' must be a set or sequence of positive integers";
    // Should we search breadth first or width first?
    if breadth_first or max_depth gt 0 then
        // Breadth first
        if terminate_if_connected eq false and filter_func cmpeq false and
           filter_child_func cmpeq false and terminate_func cmpeq false then
            return mutation_graph_breadth_first_basic(fs :
                        max_width:=max_width,
                        max_depth:=max_depth,
                        max_iterations:=max_iterations,
                        labels_include_width:=labels_include_width,
                        factor_dims:=factor_dims,
                        max_size:=max_size,
                        has_timeout:=has_timeout,
                        timeout:=timeout);
        else
            return mutation_graph_breadth_first_full(fs :
                        max_width:=max_width,
                        max_depth:=max_depth,
                        max_iterations:=max_iterations,
                        terminate_if_connected:=terminate_if_connected,
                        labels_include_width:=labels_include_width,
                        filter_func:=filter_func,
                        filter_child_func:=filter_child_func,
                        terminate_func:=terminate_func,
                        factor_dims:=factor_dims,
                        max_size:=max_size,
                        has_timeout:=has_timeout,
                        timeout:=timeout);
        end if;
    else
        // Width first
        if terminate_if_connected eq false and filter_func cmpeq false and
           filter_child_func cmpeq false and terminate_func cmpeq false then
            return mutation_graph_width_first_basic(fs :
                        max_width:=max_width,
                        max_iterations:=max_iterations,
                        labels_include_width:=labels_include_width,
                        factor_dims:=factor_dims,
                        max_size:=max_size,
                        has_timeout:=has_timeout,
                        timeout:=timeout);
        else
            return mutation_graph_width_first_full(fs :
                        max_width:=max_width,
                        max_iterations:=max_iterations,
                        terminate_if_connected:=terminate_if_connected,
                        labels_include_width:=labels_include_width,
                        filter_func:=filter_func,
                        filter_child_func:=filter_child_func,
                        terminate_func:=terminate_func,
                        factor_dims:=factor_dims,
                        max_size:=max_size,
                        has_timeout:=has_timeout,
                        timeout:=timeout);
        end if;
    end if;
end intrinsic;

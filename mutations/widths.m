freeze;

/////////////////////////////////////////////////////////////////////////
// widths.m
/////////////////////////////////////////////////////////////////////////
// An intrinsic to find width vectors for non-trivial mutations
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Returns true iff there exists a lattice vector v such that P = Q - v. If
// true, also returns v. Requires that P and Q lie in the same ambient.
function are_equal_after_translation(P,Q)
    // Sanity check
    assert Ambient(P) eq Ambient(Q);
    // Get the easy negatives out of the way
    if Dimension(P) ne Dimension(Q) or
       NumberOfVertices(P) ne NumberOfVertices(Q) then
        return false,_;
    end if;
    // Get the easy positives out of the way
    if P eq Q then
        return true,Zero(Ambient(P));
    end if;
    // Translate P so that one of the vertices is at the origin
    vertP:=Vertices(P);
    if &or[IsZero(u) : u in vertP] then
        vP:=Zero(Ambient(P));
        vertP:={PowerSequence(Integers()) | Eltseq(u) : u in vertP};
    else
        vP:=vertP[1];
        vertP:={PowerSequence(Integers()) | Eltseq(u - vP) : u in vertP};
    end if;
    // Now consider each translation of Q in turn looking for equality
    vertQ:=Vertices(Q);
    for i in [1..#vertQ] do
        vQ:=vertQ[i];
        if &and[Eltseq(vertQ[j] - vQ) in vertP : j in [1..#vertQ] | j ne i] then
            return true,vQ - vP;
        end if;
    end for;
    // If we're here then they're not equal
    return false,_;
end function;

// Given a sequence of polytopes Ps, a set of indices I of polytopes in Ps,
// and a polytope Q, checks to see if
//      are_equal_after_translation(Ps[i],Q)
// is true for any i in I. If true, also returns the first index i found, along
// with the translation vector.
function is_equal_after_transation_to_any(I,Q,Ps)
    for i in I do
        bool,v:=are_equal_after_translation(Ps[i],Q);
        if bool then
            return true,i,v;
        end if;
    end for;
    return false,_,_;
end function;

// Input is a sequence "decomp" of Minkowski summands as returned by, for
// example, MinkowskiDecomposition. Returns parallel sequences idxs and mults,
// along with a translation vector v. Here idxs indexes all distinct polytopes
// in decomp, up to translation, and mults[i] is the number of copies of the
// polytope decomp[idxs[i]]. The translations are amalgamated into the vector
// v. Note also that any 0-dimensional polytopes appearing in decomp are
// instead merged into v.
function collate_by_multiplicity(decomp)
    // Begin by partitioning the summands by crude invariants. Whilst we're
    // doing this we keep an eye out for any zero-dimensional summands.
    v:=Zero(Ambient(decomp[1]));
    parts:=AssociativeArray(PowerSequence(Integers()));
    for i in [1..#decomp] do
        Q:=decomp[i];
        if Dimension(Q) eq 0 then
            v +:= Vertices(Q)[1];
        else
            I:=[Integers() | Dimension(Q),NumberOfVertices(Q)];
            bool,S:=IsDefined(parts,I);
            S:=bool select Append(S,i) else [Integers() | i];
            parts[I]:=S;
        end if;
    end for;
    // We now collate each of these partitions
    idxs:=[Integers()|];
    mults:=[Integers()|];
    for I in Keys(parts) do
        copies:=AssociativeArray(Integers());
        for i in parts[I] do
            bool,j,u:=is_equal_after_transation_to_any(Keys(copies),
                                                              decomp[i],decomp);
            if bool then
                v +:= u;
                copies[j] +:= 1;
            else
                copies[i]:=1;
            end if;
        end for;
        for i in Keys(copies) do
            Append(~idxs,i);
            Append(~mults,copies[i]);
        end for;
    end for;
    // Return what we have
    return idxs,mults,v;
end function;

// Given a Laurent polynomial f, returns the set of all possible width vectors
// on f that may give rise to non-trivial mutations. The optional parameter
// "max_width" bounds the maximum width permitted. The optional parameter
// "factor_dims" restricts the dimensions of the factors to consider.
function possible_widths_laurent(f : max_width:=0, factor_dims:=false)
    // Extract the Newton polytope of f
    P:=NewtonPolytope(f);
    // If necessary, set the default factor dimensions
    if factor_dims cmpeq false then
        factor_dims:=[1..Dimension(P) - 1];
    end if;
    // We need to work with the dual, so the origin had better lie in P
    assert ContainsZero(P);
    D:=Dual(P);
    // Build the map from exponents to coefficients
    cs,pts:=CoefficientsAndExponents(f);
    ChangeUniverse(~pts,Ambient(P));
    A:=AssociativeArray(Ambient(P));
    for i in [1..#pts] do
        A[pts[i]]:=cs[i];
    end for;
    // We consider factorisations of each (non-extremal) face in turn
    ws:={Ambient(D)|};
    for d in factor_dims do
        // Start working through the dimension d faces of P
        for F in Faces(P,d) do
            // Build the Laurent polynomial Ff of f restricted to F
            Fpts:=[Universe(pts) | pt : pt in pts | pt in F];
            Ff:=LaurentPolynomial(Fpts,[A[pt] : pt in Fpts]);
            // Calculate the largest factor of Ff
            mults:={Integers() | I[2] : I in Factorisation(Numerator(Ff)) |
                                                             #Terms(I[1]) ne 1};
            Include(~mults,1);
            lF:=Max(mults);
            // Extract the possible dual vectors
            if max_width gt 0 then
                lF:=Min(lF,max_width - 1);
            end if;
            G:=DualFace(P,F);
            ws join:= &join[{Ambient(G) | u : u in Points(i*G) |
                                                IsPrimitive(u)} : i in [1..lF]];
        end for;
    end for;
    return ws;
end function;

// Given a polytope P, returns a set containing all possible width  vectors on P
// that may give rise to non-trivial mutations. (This set  may also contain
// width vectors that don't give rise to a mutation, because it doesn't check
// whether the width vector is compatible with intermediate 'slices'.) The
// optional parameter "max_width" bounds the maximum width permitted. The
// optional parameter "factor_dims" restricts the dimensions of the factors to
// consider.
function possible_widths_polytope(P : max_width:=0, factor_dims:=false)
    // If necessary, set the default factor dimensions
    if factor_dims cmpeq false then
        factor_dims:=[1..Dimension(P) - 1];
    end if;
    // We need to work with the dual, so the origin had better lie in P
    assert ContainsZero(P);
    Q:=Dual(P);
    // We consider factorisations of each (non-extremal) face in turn
    ws:={Ambient(Q)|};
    for d in factor_dims do
        // Start working through the dimension d faces of P
        for F in Faces(P,d) do
            // Calculate the largest factor of F
            lF:=1;
            for decomp in MinkowskiDecomposition(F) do
                _,mults:=collate_by_multiplicity(decomp);
                lF:=Max(Max(mults),lF);
            end for;
            // Extract the possible dual vectors
            if max_width gt 0 then
                lF:=Min(lF,max_width - 1);
            end if;
            G:=DualFace(P,F);
            ws join:= &join[{Ambient(Q) | u : u in Points(i*G) |
                                                IsPrimitive(u)} : i in [1..lF]];
        end for;
    end for;
    // Return the widths
    return ws;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic MutationGradings(f::FldFunRatMElt : max_width:=0, factor_dims:=false) -> SetEnum[TorLatElt]
{A set containing all possible weight vectors that may give rise to non-trivial mutations of f. The optional parameter 'max_width', if non-zero, bounds the width of the mutations considered; the optional_parameter 'factor_dims' restricts the dimensions of the Newton polytopes of the factors considered. }
    require IsLaurent(f): "Argument must be a Laurent polynomial";
    require ContainsZero(NewtonPolytope(f)): "The Newton polytope of the argument must contain the origin in its strict interior";
    require max_width in Integers() and max_width ge 0: "Optional parameter 'max_width' must be a non-negative integer";
    return possible_widths_laurent(f : max_width:=max_width, factor_dims:=factor_dims);
end intrinsic;

intrinsic MutationGradings(P::TorPol : max_width:=0, factor_dims:=false) -> SetEnum[TorLatElt]
{A set containing all possible weight vectors that may give rise to non-trivial mutations of P. The optional parameter 'max_width', if non-zero, bounds the width of the mutations considered; the optional_parameter 'factor_dims' restricts the dimensions of the Newton polytopes of the factors considered. }
    require IsPolytope(P): "Argument must be a polytope";
    require ContainsZero(P): "Argument must contain the origin in its strict interior";
    require max_width in Integers() and max_width ge 0: "Optional parameter 'max_width' must be a non-negative integer";
    return possible_widths_polytope(P : max_width:=max_width, factor_dims:=factor_dims);
end intrinsic;

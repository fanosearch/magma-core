freeze;

/////////////////////////////////////////////////////////////////////////
// writer.m
/////////////////////////////////////////////////////////////////////////
// A writer for "key: value" file data.
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Returns true iff the given store is a key:value writer.
function is_kvw(F)
    try
        bool,t:=StoreIsDefined(F,"type");
        success:=bool and t cmpeq "key_value_writer";
    catch e
        success:=false;
    end try;
    return success;
end function;

// Attempts to convert the given Magma object to a string. Returns true followed
// by the string on success, false followed by an error message otherwise.
function convert_to_str(key,to_str,obj)
    try
        result:=to_str(obj);
    catch e
        return false,Sprintf("Unable to convert data for '%o': %o\n%o",
                                                              key,obj,e`Object);
    end try;
    return true,result;
end function;

// Recorsively converts a set, sequence, etc. to string. 'l' and 'r' should be
// the left- and right-deliminators to use.
forward default_obj_to_str;
function list_to_str(S,l,r)
    if #S eq 0 then
        if #l ne 0 and l[#l] eq " " and #r ne 0 and r[1] eq " " then
            return l cat r[2..#r];
        end if;
        return l cat r;
    end if;
    str:=l;
    for s in S do
        str cat:= default_obj_to_str(s) cat ",";
    end for;
    return str[1..#str - 1] cat r;
end function;

// A default function to attempt to recursively convert objects to strings.
function default_obj_to_str(obj)
    t:=Type(obj);
    str:="";
    if t eq MonStgElt then
        str:=obj;
    elif t eq RngIntElt then
        str:=IntegerToString(obj);
    elif t eq SeqEnum then
        str:=list_to_str(obj,"[","]");
    elif t eq SetEnum then
        str:=list_to_str(obj,"{","}");
    elif t eq List then
        str:=list_to_str(obj,"[* "," *]");
    elif t eq Tup then
        str:=list_to_str(obj,"<",">");
    elif ISA(t,Mtrx) then
        str:=list_to_str(RowSequence(obj),"[","]");
    elif ISA(t,FldFunRatMElt) and IsLaurent(obj) then
        str:=PrintLaurent(obj);
    elif t eq TorLatElt then
        str:=list_to_str(Eltseq(obj),"[","]");
    elif t eq TorPol and IsPolytope(obj) then
        str:=list_to_str([Eltseq(v) : v in Vertices(obj)],"[","]");
    else
        str:=Sprintf("%o",obj);
    end if;
    return str;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic NewKeyValueFile( F::MonStgElt : conversion_funcs:=false) -> Rec
{Create an object to write associative arrays to the file named F as \"key: value\" pairs.}
    // Sanity check
    require conversion_funcs cmpeq false or Type(conversion_funcs) eq Assoc:
        "Parameter 'conversion_funcs' must be an associative array.";
    // Validate the conversion functions
    convert:=AssociativeArray();
    if not conversion_funcs cmpeq false then
    normal_keys:={};
        for key in Keys(conversion_funcs) do
            require Type(key) eq MonStgElt:
                "The keys of 'conversion_funcs' must be strings.";
            require Index(key,":") eq 0:
                "Keys cannot contain a colon \":\".";
            to_str:=conversion_funcs[key];
            require ISA(Type(to_str),Program):
                "The values of 'conversion_funcs' must be programs.";
            k:=StringToLower(Trim(key));
            error if k in normal_keys,
                "The associative array 'conversion_funcs' contains duplicate keys.\nNote that, for us, keys are case insensitive and any white space at the beginning or end of a key will be ignored (for example, \"  DATA  \" is equivalent to \"data\").";
            Include(~normal_keys,k);
            convert[k]:=to_str;
        end for;
    end if;
    // Open the file for writing
    fh:=Open(F,"w");
    // Initialize the data store
    fdata:=NewStore();
    StoreSet(fdata,"type","key_value_writer");
    StoreSet(fdata,"name",F);
    StoreSet(fdata,"convert",convert);
    StoreSet(fdata,"fh",fh);
    // Return the data store
    return fdata;
end intrinsic;

intrinsic KeyValueWrite( F::Rec, data::Assoc )
{Write the associative array 'data' to the key-value file F. The data will be written as \"key: value\" pairs. F should have been created using NewKeyValueFile.}
    // Sanity check
    require is_kvw(F): "Argument 1 must be a key-value file writer record.";
    keys:=Keys(data);
    require &and[Type(key) eq MonStgElt : key in keys]:
         "The keys must be strings.";
    require &and[Index(key,":") eq 0 : key in keys]:
        "Keys cannot contain a colon \":\".";
    // Validate the keys
    normal_keys:={StringToLower(Trim(key)) : key in keys};
    require #normal_keys eq #keys:
        "The keys contain duplicates.\nNote that keys are case insensitive, and any white space at the beginning or end of a key will be ignored (for example, \"  DATA  \" is equivalent to \"data\").";
    // Handle the empty case
    if #keys eq 0 then
	return;
    end if;
    // Convert the data to a string
    convert:=StoreGet(F,"convert");
    S:="";
    for key in Sort(keys) do
        k:=StringToLower(Trim(key));
        if k in Keys(convert) then
            bool,str:=convert_to_str(key,convert[k],data[key]);
            require bool: str;
        else
            str:=default_obj_to_str(data[key]);
        end if;
        require Index(str,"\n") eq 0:
            Sprintf("String representation of value for '%o' contains a newline.",key);
        S cat:= Sprintf("%o: %o\n",key,str);
    end for;
    // Write out the data
    if #S ne 0 then
        Put(StoreGet(F,"fh"),S cat "\n");
    end if;
end intrinsic;

intrinsic KeyValueFlush( F::Rec )
{Flush the key-value file F}
    require is_kvw(F): "Argument must be a key-value file writer record.";
    Flush(StoreGet(F,"fh"));
end intrinsic;

freeze;

/////////////////////////////////////////////////////////////////////////
// reader.m
/////////////////////////////////////////////////////////////////////////
// A reader for "key: value" file data.
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Formats an error message to include the current file name and line number.
function error_msg(fdata,msg)
    // Note the data we need
    name:=StoreGet(fdata,"name");
    line_num:=StoreGet(fdata,"line_num");
    // Return the formatted error message
    return Sprintf("Error whilst parsing '%o', line %o.\n%o",name,line_num,msg);
end function;

// Attempts to convert the given string to a Magma object. Returns true followed
// by the object on success, false followed by an error message otherwise.
function convert_to_obj(to_obj,S)
    try
        obj:=to_obj(S);
    catch e
        return false,Sprintf("Unable to convert value: %o\n%o",S,e`Object);
    end try;
    return true,obj;
end function;

// Attempts to apply the preparse function to the given string. Returns true
// followed by the parsed string on success, false followed by an error message
// otherwise.
function apply_preparse_func(preparse,S)
    try
        parsedS:=preparse(S);
    catch e
        return false,Sprintf("Unable to preparse line: %o\n%o",S,e`Object);
    end try;
    if Type(parsedS) ne MonStgElt then
        return false,"Preparse function did not return a string.";
    end if;
    return true,Trim(parsedS);
end function;

// Given a string of the form "key: value", splits and returns true followed
// by the key and value (as strings). The key will be returned in lower case.
// If the string is incorrectly formatted, returns false followed by an error
// message.
function split_key_value(S)
    idx:=Index(S,":");
    if idx eq 0 then
        return false,"Line is not of the form \"key: value\".",_;
    end if;
    return true,StringToLower(Trim(S[1..idx - 1])),Trim(S[idx + 1..#S]);
end function;

// Skips over white space and returns true followed by the next non-empty line
// of data from the file, false otherwise.
function next_line_skip_space(fdata)
    // Note the data we need
    fh:=StoreGet(fdata,"fh");
    line_num:=StoreGet(fdata,"line_num");
    preparse:=StoreGet(fdata,"preparse");
    // Start working through the file
    while true do
        L:=Gets(fh);
        if IsEof(L) then
            StoreSet(fdata,"line_num",line_num);
            return false,_;
        end if;
        line_num +:= 1;
        L:=Trim(L);
        if #L ne 0 and not preparse cmpeq false then
            bool,L:=apply_preparse_func(preparse,L);
            if not bool then
                StoreSet(fdata,"line_num",line_num);
                error error_msg(fdata,L);
            end if;
        end if;
        if #L ne 0 then
            StoreSet(fdata,"line_num",line_num);
            return true,L;
        end if;
    end while;
end function;

// Fetches the next file from the file. If the line is non-empty, returns true
// followed by the line. Otherwise returns false.
function next_line(fdata)
    // Note the data we need
    fh:=StoreGet(fdata,"fh");
    line_num:=StoreGet(fdata,"line_num");
    preparse:=StoreGet(fdata,"preparse");
    // Fetch the next line of data from the file
    L:=Gets(fh);
    if IsEof(L) then return false,_; end if;
    StoreSet(fdata,"line_num",line_num + 1);
    L:=Trim(L);
    if #L ne 0 and not preparse cmpeq false then
        bool,L:=apply_preparse_func(preparse,L);
        error if not bool, error_msg(fdata,L);
    end if;
    if #L eq 0 then return false,_; end if;
    return true,L;
end function;

// Fetch the next block of data from the file. Returns true followed by the
// data (as an associative array) on success, true followed by false if the end
// of the file has been reached, false followed by an error message otherwise.
function parse_block(fdata)
    // Note the data we need
    keys:=StoreGet(fdata,"keys");
    keymap:=StoreGet(fdata,"keymap");
    convert:=StoreGet(fdata,"convert");
    skip_unknown:=StoreGet(fdata,"skip_unknown");
    skip_duplicate_keys:=StoreGet(fdata,"skip_duplicate_keys");
    require_all_keys:=StoreGet(fdata,"require_all_keys");
    // Extract the first key: value pair
    bool,L:=next_line_skip_space(fdata);
    if not bool then return true,false; end if;
    // Start parsing
    A:=AssociativeArray();
    while bool do
        // Split the data into the key and value
        bool,key,value:=split_key_value(L);
        if not bool then return false,error_msg(fdata,key); end if;
        // Is the key valid?
        if key in keys then
            // Is the key a duplicate?
            if not IsDefined(A,key) then
                // Convert the value and update the array
                bool,to_obj:=IsDefined(convert,key);
                if bool then
                    bool,value:=convert_to_obj(to_obj,value);
                    if not bool then
                        return false,error_msg(fdata,value);
                    end if;
                end if;
                A[key]:=value;
            elif not skip_duplicate_keys then
                return false,error_msg(fdata,"Duplicate key: " cat key);
            end if;
        elif not skip_unknown then
            return false,error_msg(fdata,"Unknown key: " cat key);
        end if;
        // Move on
        bool,L:=next_line(fdata);
    end while;
    // If all keys are required, are they set?
    if require_all_keys then
        missing:=keys diff Keys(A);
        num:=#missing;
        if num ne 0 then
            missing:=Join(Sort(missing),", ");
            msg:=num eq 1 select "Missing the following key: "
                            else Sprintf("Missing the following %o keys: ",num);
            return false,error_msg(fdata,msg cat missing);
        end if;
    end if;
    // Remap the keys and return the array of data
    data:=AssociativeArray();
    for key in Keys(A) do
        data[keymap[key]]:=A[key];
    end for;
    return true,data;
end function;

// True iff the key-value process is empty.
function kv_is_empty(info)
    return info[1];
end function;

// Advances the key-value process to the next block.
procedure kv_advance(~info)
    error if info[1], "The process has finished.";
    fdata:=info[2];
    bool,A:=parse_block(fdata);
    error if not bool, A;
    if A cmpeq false then
        info:=<true>;
    else
        info:=<false,fdata,A,info[4] + 1>;
    end if;
end procedure;

// Returns the current block.
function kv_value(info)
    error if info[1], "The process has finished.";
    return info[3];
end function;

// Returns the current label.
function kv_label(info)
    error if info[1], "The process has finished.";
    return info[4];
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic KeyValueFileProcess( F::MonStgElt, keys::SetEnum[MonStgElt] :
     skip_unknown:=false, skip_duplicate_keys:=false, require_all_keys:=false,
     conversion_funcs:=false, preparse_func:=false ) -> Process
{Create a process to read the \"key: value\" file with filename F and given keys.}
    // Sanity check
    require &and[Index(key,":") eq 0 : key in keys]:
        "Keys cannot contain a colon \":\".";
    require Type(skip_unknown) eq BoolElt:
        "Parameter 'skip_unknown' must be a boolean.";
    require Type(skip_duplicate_keys) eq BoolElt:
        "Parameter 'skip_duplicate_keys' must be a boolean.";
    require Type(require_all_keys) eq BoolElt:
        "Parameter 'require_all_keys' must be a boolean.";
    require conversion_funcs cmpeq false or Type(conversion_funcs) eq Assoc:
        "Parameter 'conversion_funcs' must be an associative array.";
    require preparse_func cmpeq false or ISA(Type(preparse_func),Program):
        "Parameter 'preparse_func' must be a program.";
    // Normalise the keys and build the key map
    normal_keys:={StringToLower(Trim(key)) : key in keys};
    require #normal_keys eq #keys:
        "The keys contain duplicates.\nNote that, for us, keys are case insensitive, and any white space at the beginning or end of a key will be ignored (for example, \"  DATA  \" is equivalent to \"data\").";
    keymap:=AssociativeArray();
    for key in keys do
        keymap[StringToLower(Trim(key))]:=key;
    end for;
    // Validate the converstion functions
    convert:=AssociativeArray();
    if not conversion_funcs cmpeq false then
        for key in Keys(conversion_funcs) do
            require Type(key) eq MonStgElt:
                "The keys of 'conversion_funcs' must be strings.";
            to_obj:=conversion_funcs[key];
            require ISA(Type(to_obj),Program):
                "The values of 'conversion_funcs' must be programs.";
            k:=StringToLower(Trim(key));
            if k in normal_keys then
                convert[k]:=to_obj;
            end if;
        end for;
    end if;
    // Open the file for reading
    fh:=Open(F,"r");
    // Initialize the data store
    fdata:=NewStore();
    StoreSet(fdata,"name",F);
    StoreSet(fdata,"keys",normal_keys);
    StoreSet(fdata,"keymap",keymap);
    StoreSet(fdata,"skip_unknown",skip_unknown);
    StoreSet(fdata,"skip_duplicate_keys",skip_duplicate_keys);
    StoreSet(fdata,"require_all_keys",require_all_keys);
    StoreSet(fdata,"preparse",preparse_func);
    StoreSet(fdata,"convert",convert);
    StoreSet(fdata,"fh",fh);
    StoreSet(fdata,"line_num",0);
    // Initialize the process data
    bool,A:=parse_block(fdata);
    require bool: A;
    if A cmpeq false then
        info:=<true>;
    else
        info:=<false,fdata,A,1>;
    end if;
    // Create and return the process
    return InternalCreateProcess("Key-Value File",info,kv_is_empty,kv_advance,
                                                             kv_value,kv_label);
end intrinsic;

freeze;

/////////////////////////////////////////////////////////////////////////
// decomposition.m
/////////////////////////////////////////////////////////////////////////
// Functions for decomposing an integral polygons into Minkowski
// summands.
/////////////////////////////////////////////////////////////////////////

import "simplex.m": simplex_decomposition;
import "general.m": general_decomposition;
import "functions.m": distinct_unordered_partitions;
import "point.m": primitive_nonzero_lattice_vector;

declare attributes TorPol:
    decomposition;              // The Minkowski decomposition of the polytope

/////////////////////////////////////////////////////////////////////////
// Functions
/////////////////////////////////////////////////////////////////////////

// Returns of sequence of distinct Minkowski sums Q + R of P.
function minkowski_pairs(P)
    // First compute the decomposition
    M:=MinkowskiDecomposition(P);
    // We ensure that every decomposition contains a 0-dimensional component
    // by adding in the origin if necessary
    L:=Ambient(P);
    for i in [1..#M] do
        if not &or[Dimension(Q) eq 0 : Q in M[i]] then
            Append(~M[i],Polytope([L | Zero(L)] : areVertices:=true));
        end if;
    end for;
    // Assign IDs to the polytopes occuring in the Minkowski decompositions
    // and re-express them in terms of these IDs
    polys:=SetToIndexedSet(SequenceToSet(&cat M));
    M:={PowerSequence(Integers()) | Sort([Integers() |
                            Index(polys,P) : P in decomp]) : decomp in M};
    // Now, for each decomposition, create all possible pairs
    idsToPoly:=AssociativeArray(PowerSequence(Integers()));
    decompPolys:={@ PowerStructure(TorPol)| @};
    pairs:={PowerSequence(Integers())|};
    for decomp in M do
        for pair in distinct_unordered_partitions(decomp) do
            // The following code is simply an effort to reduce the number of
            // polytopes we need to construct. The end result is an id (the
            // index in decompPolys) of each polytope in the pair.
            S:=Sort(pair[1]);
            bool,P1:=IsDefined(idsToPoly,S);
            if not bool then
                P1:=&+[polys[i] : i in S];
                idsToPoly[S]:=P1;
            end if;
            S:=Sort(pair[2]);
            bool,P2:=IsDefined(idsToPoly,S);
            if not bool then
                P2:=&+[polys[i] : i in S];
                idsToPoly[S]:=P2;
            end if;
            id1:=Index(decompPolys,P1);
            if id1 eq 0 then
                Include(~decompPolys,P1);
                id1:=#decompPolys;
            end if;
            id2:=Index(decompPolys,P2);
            if id2 eq 0 then
                Include(~decompPolys,P2);
                id2:=#decompPolys;
            end if;
            // Add the pair to the list we're constructing
            Include(~pairs,Sort([Integers() | id1, id2]));
        end for;
    end for;
    // Finally, return the decompositions
    return [PowerSequence(PowerStructure(TorPol)) |
                [decompPolys[I[1]],decompPolys[I[2]]] : I in pairs];
end function;

// Computes all possible primitive decompositions of the given line.
function line_decomposition(L)
    pt1,pt2:=Explode(Vertices(L));
    if IsZero(pt1) then
        pt,k:=primitive_nonzero_lattice_vector(pt2);
        LL:=k eq 1 select L else Polytope([pt1,pt] : areVertices:=true);
        return [[LL : i in [1..k]]];
    elif IsZero(pt2) then
        pt,k:=primitive_nonzero_lattice_vector(pt1);
        LL:=k eq 1 select L else Polytope([pt2,pt] : areVertices:=true);
        return [[LL : i in [1..k]]];
    else
        pt,k:=primitive_nonzero_lattice_vector(pt2 - pt1);
        LL:=Polytope([Zero(Parent(pt)),pt] : areVertices:=true);
        return [[Polytope([pt1] : areVertices:=true)] cat [LL : i in [1..k]]];
    end if;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic MinkowskiDecomposition(P::TorPol) -> SeqEnum[SeqEnum[TorPol]]
{The possible primitive Minkowski decompositions of the integral polytope P}
    // Do we already know the answer?
    if not assigned P`decomposition then
        // Sanity checks
        require IsPolytope(P) and IsIntegral(P):
            "The polyhedron must be an integral polytope";
        // Get the dimension 0, 1, and 2 cases out of the way
        if Dimension(P) eq 0 then
            P`decomposition:=[[P]];
        elif Dimension(P) eq 1 then
            P`decomposition:=line_decomposition(P);
        elif IsSimplex(P) then
            P`decomposition:=simplex_decomposition(P);
        // See if there's a cheap reason to be indecomposable
        elif InternalIsMinkowskiIndecomposable(P) then
            P`decomposition:=[[P]];
        // Finally, use the general dimension algorithm
        else
            P`decomposition:=general_decomposition(P);
        end if;
    end if;
    return P`decomposition;
end intrinsic;

freeze;

/////////////////////////////////////////////////////////////////////////
// point.m
/////////////////////////////////////////////////////////////////////////
// Describes the elements (points) of the toric lattice and how to
// manipulate them.
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Local Functions
/////////////////////////////////////////////////////////////////////////

// Converts the row matrix M into a lattice point in L. Note that NO checks
// are performed on the size of M to ensure that it is compatible with L.
function row_matrix_to_lattice_vector(L,M)
    // Create the point and assign its data
    v:=New(TorLatElt);
    v`lattice:=L;
    if Type(M) eq ModMatRngElt then
        v`is_integral:=true;
        v`row_matrix:=ChangeRing(M,Rationals());
    else
        v`row_matrix:=M;
    end if;
    return v;
end function;

// Given a non-zero lattice vector v, returns a primitive vector u and a
// positive scalar k such that v = k * u.
// Note: This does NOT check whether v is zero.
function primitive_nonzero_lattice_vector(v)
    u_mat:=Denominator(v) * v`row_matrix;
    k:=GCD(ChangeUniverse(Eltseq(u_mat),Integers()));
    if not assigned v`primitive then
        u:=row_matrix_to_lattice_vector(Parent(v),u_mat / k);
        u`is_integral:=true;
        u`is_primitive:=true;
        v`primitive:=u;
    end if;
    return v`primitive,k;
end function;

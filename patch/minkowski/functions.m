freeze;

/////////////////////////////////////////////////////////////////////////
// functions.m
/////////////////////////////////////////////////////////////////////////
// A collection of useful utility functions and intrinsics.
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// Functions
/////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////
// distinct_unordered_partitions
/////////////////////////////////////////////////////////////////////////
// Input: A sequence S.
// Output: All distinct, non-empty, partitions S1,S2 of S, where the order
// of the elements in S1 and S2 doesn't matter.
/////////////////////////////////////////////////////////////////////////

function distinct_unordered_partitions_compliment(S,S1)
    for s in S1 do
        Remove(~S,Index(S,s));
    end for;
    return S;
end function;

procedure distinct_unordered_partitions_recurse(i,vals,nums,S1,~S1s)
    if i eq 0 then
        Append(~S1s,S1);
    elif #nums gt 0 then
        sum:=&+nums;
        if sum - nums[1] ge i then
            $$(i,vals[2..#vals],nums[2..#nums],S1,~S1s);
        end if;
        if sum ge i then
            Append(~S1,vals[1]);
            if nums[1] eq 1 then
                vals:=vals[2..#vals];
                nums:=nums[2..#nums];
            else
                nums[1] -:= 1;
            end if;
            $$(i - 1,vals,nums,S1,~S1s);
        end if;
    end if;
end procedure;

function distinct_unordered_partitions(S)
    vals:=SetToSequence(SequenceToSet(S));
    nums:=[Integers() | #[Integers() | 1 : s in S | s eq val] : val in vals];
    S1s:=[PowerSequence(Universe(S))|];
    for i in [1..Floor(#S / 2)] do
        subS1s:=[Universe(S1s)|];
        distinct_unordered_partitions_recurse(i,vals,nums,
                                                    [Universe(S)|],~subS1s);
        S1s cat:= subS1s;
    end for;
    return [PowerSequence(Universe(S1s)) |
               [S1,distinct_unordered_partitions_compliment(S,S1)] : S1 in S1s];
end function;

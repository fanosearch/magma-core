freeze;

/////////////////////////////////////////////////////////////////////////
// sqlite.m
/////////////////////////////////////////////////////////////////////////
// Utilities for interfacing with sqlite.
/////////////////////////////////////////////////////////////////////////

// The database record
DatabaseRec:=recformat< base : PowerStructure(MonStgElt),
                        database : PowerStructure(MonStgElt) >;

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// Returns an error string for the given query.
function sqlite_error_string(sql,db)
    return "Unable to issue command\n\t" cat sql cat
           "\nto sqlite database \"" cat db`database cat "\".";
end function;

// Splits the row into [key,value] pairs.
function sqlite_split_row(res)
    res:=Trim(res);
    row:=[PowerSequence(PowerStructure(MonStgElt))|];
    idx:=Index(res," = ");
    while idx ne 0 do
        key:=Trim(res[1..idx - 1]);
        value:="";
        if idx + 3 gt #res then
            idx:=0;
        else
            done:=false;
            res:=res[idx + 3..#res];
            idx:=Index(res,"\n");
            while done eq false do
                if idx eq 0 then
                    if #value eq 0 then
                        value:=res;
                    else
                        value cat:= "\n" cat res;
                    end if;
                    done:=true;
                elif idx eq #res then
                    if #value eq 0 then
                        value:=res[1..#res - 1];
                    elif idx ne 1 then
                        value cat:= "\n" cat res[1..#res - 1];
                    end if;
                    idx:=0;
                    done:=true;
                else
                    if #value eq 0 then
                        value:=res[1..idx - 1];
                    elif idx ne 1 then
                        value cat:= "\n" cat res[1..idx - 1];
                    end if;
                    res:=res[idx + 1..#res];
                    tmpidx1:=Index(res," = ");
                    tmpidx2:=Index(res,"\n");
                    if tmpidx1 lt tmpidx2 or
                       (tmpidx1 gt 0 and tmpidx2 eq 0) then
                        done:=true;
                        idx:=tmpidx1;
                    else
                        idx:=tmpidx2;
                    end if;
                end if;
            end while;
        end if;
        Append(~row,[key,value]);
    end while;
    return row;
end function;

// Splits the result string into rows.
function sqlite_split_rows(res)
    rows:=[PowerSequence(PowerSequence(PowerStructure(MonStgElt)))|];
    idx:=Index(res,"\n\n");
    while idx ne 0 do
        if idx + 2 gt #res then
            idx:=0;
        else
            row:=sqlite_split_row(res[1..idx - 1]);
            if #row ne 0 then
                Append(~rows,row);
            end if;
            res:=res[idx + 2..#res];
            idx:=Index(res,"\n\n");
        end if;
    end while;
    row:=sqlite_split_row(res);
    if #row ne 0 then
        Append(~rows,row);
    end if;
    return rows;
end function;

/////////////////////////////////////////////////////////////////////////
// Intrinsics
/////////////////////////////////////////////////////////////////////////

intrinsic IsSqliteDatabase(x::Any) -> BoolElt
{True iff the argument is an Sqlite database connection}
    return Type(x) cmpeq Rec and
           Names(x) eq Names(DatabaseRec);
end intrinsic;

intrinsic SqliteConnect(database::MonStgElt : sqlite:=false) -> Rec
{Initialises a connection to the given sqlite database}
    // Sanity checks
    require not InternalIsPC(): "Operation not supported on Windows";
    database:=Trim(database);
    require #database gt 0: "The path to the database must not be empty";
    if sqlite cmpeq false then sqlite:="sqlite3"; end if;
    require Type(sqlite) eq MonStgElt and #sqlite gt 0:
        "'sqlite' must be a non-empty string";
    // Create the base command
    base:=sqlite cat " -line " cat QuotePath(database) cat " ";
    // Can we talk to the database?
    bool:=PipeCommand(base cat ".table");
    require bool: "Unable to access sqlite database \"" cat database cat "\"";
    // Store the data in a record and return it
    return rec< DatabaseRec | base:=base, database:=database >;
end intrinsic;

intrinsic SqliteDatabase(db::Rec) -> MonStgElt
{The name of the database}
    require IsSqliteDatabase(db): "Argument must be an Sqlite database connection";
    return db`database;
end intrinsic;

intrinsic SqliteTables(db::Rec) -> SeqEnum
{The tables in the given database}
    require IsSqliteDatabase(db): "Argument must be an Sqlite database connection";
    bool,res:=PipeCommand(db`base cat "\".table\"");
    require bool: sqlite_error_string(".table",db);
    return Split(CollateWhiteSpace(res)," ");
end intrinsic;

intrinsic SqliteTableExists(db::Rec, table::MonStgElt) -> BoolElt
{True iff the given table exists}
    require IsSqliteDatabase(db): "Argument 1 must be an Sqlite database connection";
    bool,res:=PipeCommand(db`base cat "\".table\"");
    require bool: sqlite_error_string(".table",db);
    for name in Split(CollateWhiteSpace(res)," ") do
        if name eq table then return true; end if;
    end for;
    return false;
end intrinsic;

intrinsic SqliteExecute(db::Rec, sql::MonStgElt)
{Executes the given SQL statement}
    require IsSqliteDatabase(db): "Argument 1 must be an Sqlite database connection";
    bool,res:=PipeCommand(db`base cat "\"" cat EscapeString(sql) cat "\"");
    require bool: sqlite_error_string(sql,db);
end intrinsic;

intrinsic SqliteQuery(db::Rec, sql::MonStgElt) -> SeqEnum[SeqEnum[MonStgElt]]
{Performs the given SQL query}
    require IsSqliteDatabase(db): "Argument 1 must be an Sqlite database connection";
    bool,res:=PipeCommand(db`base cat "\"" cat EscapeString(sql) cat "\"");
    require bool: sqlite_error_string(sql,db);
    return sqlite_split_rows(res);
end intrinsic;
